function base_url(data) {
    var pathparts = location.pathname.split('/')
    var url = location.origin;
    if (location.host == 'localhost') {
        var target = 0
        for(let i=1; i <= pathparts.length; i++){
            if(pathparts[i] == 'bank_sampah_v2' || pathparts[i] == 'superadmin'){
                target = i
            }
        }
        if(target != 0 ){
            for(let i=1; i<= target; i++){
                url = url+'/'+pathparts[i];
            }
        } else {
            url = url+'/'+pathparts[1].trim('/');
        }
    } else if(location.host == 'kompis.online' || location.host == 'sandboxku.com'){
        var url = location.origin+'/'+pathparts[1];
    } else {
        var url = location.origin;
    }

    if(data == '' || data == null){
        return url;
    } else {
        return url+'/'+data;
    }
}

function formatAngka(angka, prefix) {
    var number_string = angka.replace(/[^,\d]/g, '').toString(),
        split = number_string.split(','),
        sisa = split[0].length % 3,
        rupiah = split[0].substr(0, sisa),
        ribuan = split[0].substr(sisa).match(/\d{3}/gi);

    if (ribuan) {
        separator = sisa ? '.' : '';
        rupiah += separator + ribuan.join('.');
    }

    rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
    return prefix == undefined ? rupiah : (rupiah ? rupiah + '' : '');
}

function formatKoin(angka, prefix) {
    var number_string = angka.replace(/[^,\d]/g, '').toString(),
        split = number_string.split(','),
        sisa = split[0].length % 3,
        rupiah = split[0].substr(0, sisa),
        ribuan = split[0].substr(sisa).match(/\d{3}/gi);

    if (ribuan) {
        separator = sisa ? '.' : '';
        rupiah += separator + ribuan.join('.');
    }

    rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
    return prefix == undefined ? rupiah + ' <b>Koin</b>' : (rupiah ? rupiah + ' <b>Koin</b>' : '');
}

// Date filter
function setDateFilter(id_akhir,id_awal){
    $('#'+id_akhir).bootstrapMaterialDatePicker({
        format: 'YYYY-MM-DD',
        weekStart: 0,
        time: false,
        setMaxDate: new Date()
    }).on('change', function(e, date) {
        $('#'+id_awal).bootstrapMaterialDatePicker('setMaxDate', date);
        btnClearAkhir()
    }); 

    $('#'+id_awal).bootstrapMaterialDatePicker({
        format: 'YYYY-MM-DD',
        weekStart: 0,
        time: false
    }).on('change', function(e, date) {
        $('#'+id_akhir).bootstrapMaterialDatePicker('setMinDate', date);
        btnClearAwal()
    });

    dateValidation(id_awal, id_akhir)
}

function dateValidation(id_awal, id_akhir){
    if($('#'+id_awal).val() != ''){
		$('#'+id_akhir).bootstrapMaterialDatePicker('setMinDate', $('#'+id_awal).val());
        btnClearAwal()
	}
	if($('#'+id_akhir).val() != ''){
		$('#'+id_awal).bootstrapMaterialDatePicker('setMaxDate', $('#'+id_akhir).val());
        btnClearAkhir()
	}
}

function btnClearAwal(){
    $('#icon-awal').removeClass('fa-calendar-alt')
    $('#icon-awal').addClass('fa-times-circle')
    $('#icon-awal').prop('title', 'Clear')
    $('#icon-awal').attr('onClick', 'resetAwal()')
}

function btnClearAkhir(){
    $('#icon-akhir').removeClass('fa-calendar-alt')
    $('#icon-akhir').addClass('fa-times-circle')
    $('#icon-akhir').prop('title', 'Clear')
    $('#icon-akhir').attr('onClick', 'resetAkhir()')
}

function resetAwal(){
    $('#awal').val('')
    $('#icon-awal').removeClass('fa-times-circle')
    $('#icon-awal').addClass('fa-calendar-alt')
    $('#icon-awal').removeAttr('onClick')
    $('#akhir').bootstrapMaterialDatePicker('setMinDate', '01-01-1990');
    $('#awal').bootstrapMaterialDatePicker({setMinDate: false});
}

function resetAkhir(){
    $('#akhir').val('')
    $('#icon-akhir').removeClass('fa-times-circle')
    $('#icon-akhir').addClass('fa-calendar-alt')
    $('#icon-akhir').removeAttr('onClick')
    let date = new Date(new Date().setFullYear(new Date().getFullYear() + 1))
    $('#akhir').bootstrapMaterialDatePicker({setMaxDate: false});
    $('#awal').bootstrapMaterialDatePicker('setMaxDate', date);
}

function hanyaAngka(evt) {
    var charCode = (evt.which) ? evt.which : event.keyCode
      if (charCode > 31 && (charCode < 48 || charCode > 57))
          return false;
    return true;
}

function startloading(id){
    $(id).ploading({
        action: 'show'
    })
}

function stoploading(id){
    $(id).ploading({
        action: 'hide'
    })
}

function delConf(link){
    swal({
            title: 'Yakin menghapus data?',
            text: "Data yang sudah terhapus tidak dapat dikembalikan!",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#CD5C5C',
            cancelButtonColor: '#d33',
            cancelButtonText: 'Batal',
            confirmButtonText: 'Hapus',
            closeOnConfirm: false,
        },
        function(isConfirm) {
            if (isConfirm) {
                window.location.href = link;
            } else {

            }
        });
}


function CekKonfirmasi(gagal, berhasil){
    if(gagal != ""){
        swal({
            position  : 'top-end',
            type      : 'error',
            title     : 'Gagal!',
            text      : gagal,
            showConfirmButton: false,
            timer: 1500
        });
    }
    if(berhasil != ""){
        swal({
            position  : 'top-end',
            type      : 'success',
            title     : 'Success!',
            text      : berhasil,
            showConfirmButton: false,
            timer: 1500
        });
    }
}