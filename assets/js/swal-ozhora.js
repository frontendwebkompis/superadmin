
        window.onload=function(){
            <?php
                if(!empty($this->session->flashdata('berhasil'))) {
                    ?>
                        swal({
                          position  : 'top-end',
                          type      : 'success',
                          title     : 'Success!',
                          text      : '<?php echo $this->session->flashdata('berhasil'); ?>',
                          showConfirmButton: false,
                          timer: 1500
                        });
                    <?php
                }
                if(!empty($this->session->flashdata('gagal'))) {
                    $pesan = $this->session->flashdata('gagal');
                    ?>
                        swal({
                          position  : 'top-end',
                          type      : 'error',
                          title     : 'Gagal!',
                          text      : '<?php echo $this->session->flashdata('gagal'); ?>',
                          showConfirmButton: false,
                          timer: 1500
                        });
                    <?php
                }

                if(!empty($this->session->flashdata('cek'))) {
                
                    $this->db->where('id_bank_sampah', $this->session->userdata('uid'));
                    $query = $this->db->get('bank_sampah');
                    foreach($query->result() as $row){
                      $status = $row->id_status_bank_sampah;
                    }
                    if($status == 1){
                      $pesan = $this->session->flashdata('cek');
                    } else {
                      $pesan = 'Berkas masih dalam proses, Silahkan menunggu konfirmasi dari admin Kompis.';
                    }
                    ?>
                        swal({
                          position  : 'top-end',
                          type      : 'info',
                          title     : 'Info!',
                          text      : '<?php echo $pesan; ?>',
                          showConfirmButton: true,
                        });
                    <?php
                }

            ?>
        } 