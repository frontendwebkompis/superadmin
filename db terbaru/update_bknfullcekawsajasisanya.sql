-- --------------------------------------------------------
-- Host:                         localhost
-- Versi server:                 10.1.32-MariaDB - mariadb.org binary distribution
-- OS Server:                    Win32
-- HeidiSQL Versi:               10.2.0.5599
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- membuang struktur untuk table db_kompis_baru.bank_sampah
CREATE TABLE IF NOT EXISTS `bank_sampah` (
  `id_bank_sampah` varchar(10) NOT NULL,
  `id_status_bank_sampah` varchar(5) NOT NULL,
  `nama_bank_sampah` varchar(50) DEFAULT NULL,
  `alamat_bank_sampah` varchar(50) DEFAULT NULL,
  `id_provinsi` int(5) DEFAULT NULL,
  `id_kabupaten` int(5) DEFAULT NULL,
  `id_kecamatan` int(5) DEFAULT NULL,
  `lat_bank_sampah` decimal(10,8) DEFAULT NULL,
  `long_bank_sampah` decimal(11,8) DEFAULT NULL,
  `email_bank_sampah` varchar(50) DEFAULT NULL,
  `no_hp_bank_sampah` varchar(15) DEFAULT NULL,
  `password_bank_sampah` varchar(30) DEFAULT NULL,
  `kode_verifikasi_bank_sampah` varchar(50) DEFAULT NULL,
  `last_login` datetime DEFAULT NULL,
  `tgl_join` datetime DEFAULT NULL,
  PRIMARY KEY (`id_bank_sampah`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Membuang data untuk tabel db_kompis_baru.bank_sampah: ~8 rows (lebih kurang)
/*!40000 ALTER TABLE `bank_sampah` DISABLE KEYS */;
INSERT INTO `bank_sampah` (`id_bank_sampah`, `id_status_bank_sampah`, `nama_bank_sampah`, `alamat_bank_sampah`, `id_provinsi`, `id_kabupaten`, `id_kecamatan`, `lat_bank_sampah`, `long_bank_sampah`, `email_bank_sampah`, `no_hp_bank_sampah`, `password_bank_sampah`, `kode_verifikasi_bank_sampah`, `last_login`, `tgl_join`) VALUES
	('003', '1', '1', '1', 1, 3, 30, 1.00000000, 2.00000000, 'nnn@gmail.com', '39393', 'keke', '1', '2020-02-12 11:33:11', NULL),
	('12', '0', 'mem', 'keke', 12, 196, 3009, 3.00000000, 4.00000000, 'mk@gmail.com', '0383838', 'ekkeke', '1', '2020-02-12 11:32:02', NULL),
	('3', '0', 'mem', 'eme', 1, 1, 1, 1.00000000, 1.00000000, '1mm@gmail.com', '3333', '33mm', 'meme', '2020-02-12 11:30:38', NULL),
	('KMP01001', '1', '1', '1', 1, 1, 1, 1.00000000, 1.00000000, 'mk@gmail.com', '1929', '1kkwk', '1', '2020-02-12 11:37:11', NULL),
	('KMP010013', '1', '1', '1', 1, 1, 1, 1.00000000, 1.00000000, '1mm@gmail.com', '1', 'mmm1', '1', '2020-02-12 12:08:46', NULL),
	('KMP02010', '1', '1', '1', 2, 10, 151, 1.00000000, 1.00000000, '1mm@gmail.com', '19191', 'mmwm', '1', '2020-02-12 11:39:06', NULL),
	('KMP0506500', '1', '1', 'mmm', 5, 65, 1244, 1.00000000, 1.00000000, 'mk@gmail.com', '1', '1', '1', '2020-02-12 11:40:25', NULL),
	('KMP1928600', '2', '1', 's', 19, 286, 4091, 1.00000000, 1.00000000, '1mm@gmail.com', '1', '1', '1', '2020-02-12 12:11:52', NULL);
/*!40000 ALTER TABLE `bank_sampah` ENABLE KEYS */;

-- membuang struktur untuk table db_kompis_baru.bank_sampah_detail
CREATE TABLE IF NOT EXISTS `bank_sampah_detail` (
  `id_bank_sampah` varchar(10) NOT NULL,
  `no_telp_bank_sampah` varchar(15) DEFAULT NULL,
  `foto_bangunan_bank_sampah` varchar(50) DEFAULT NULL,
  `sk_bank_sampah` varchar(50) DEFAULT NULL,
  `tanggal_berdiri_bank_sampah` date DEFAULT NULL,
  `slogan_bank_sampah` varchar(100) DEFAULT NULL,
  `website_bank_sampah` varchar(100) DEFAULT NULL,
  `nama_penanggungjawab` varchar(100) DEFAULT NULL,
  `no_ktp_penanggungjawab` int(16) DEFAULT NULL,
  `foto_ktp_penanggungjawab` varchar(50) DEFAULT NULL,
  `selfie_ktp_penanggungjawab` varchar(50) DEFAULT NULL,
  `alamat_penanggungjawab` varchar(100) DEFAULT NULL,
  `no_hp_penanggungjawab` varchar(15) DEFAULT NULL,
  `email_penanggungjawab` varchar(50) DEFAULT NULL,
  `saldo_bank_sampah` int(11) DEFAULT NULL,
  `point_bank_sampah` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Membuang data untuk tabel db_kompis_baru.bank_sampah_detail: ~2 rows (lebih kurang)
/*!40000 ALTER TABLE `bank_sampah_detail` DISABLE KEYS */;
INSERT INTO `bank_sampah_detail` (`id_bank_sampah`, `no_telp_bank_sampah`, `foto_bangunan_bank_sampah`, `sk_bank_sampah`, `tanggal_berdiri_bank_sampah`, `slogan_bank_sampah`, `website_bank_sampah`, `nama_penanggungjawab`, `no_ktp_penanggungjawab`, `foto_ktp_penanggungjawab`, `selfie_ktp_penanggungjawab`, `alamat_penanggungjawab`, `no_hp_penanggungjawab`, `email_penanggungjawab`, `saldo_bank_sampah`, `point_bank_sampah`) VALUES
	('KMP02010', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	('003', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
/*!40000 ALTER TABLE `bank_sampah_detail` ENABLE KEYS */;

-- membuang struktur untuk table db_kompis_baru.nasabah_online
CREATE TABLE IF NOT EXISTS `nasabah_online` (
  `id_nasabah_online` int(11) NOT NULL AUTO_INCREMENT,
  `id_status_nasabah_online` varchar(5) NOT NULL,
  `nama_nasabah_online` varchar(50) DEFAULT NULL,
  `email_nasabah_online` varchar(50) DEFAULT NULL,
  `no_hp_nasabah_online` varchar(15) DEFAULT NULL,
  `password_nasabah_online` varchar(100) DEFAULT NULL,
  `kode_verifikasi_bank_sampah` varchar(50) DEFAULT NULL,
  `last_login` datetime DEFAULT NULL,
  `tgl_join` datetime DEFAULT NULL,
  PRIMARY KEY (`id_nasabah_online`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- Membuang data untuk tabel db_kompis_baru.nasabah_online: ~3 rows (lebih kurang)
/*!40000 ALTER TABLE `nasabah_online` DISABLE KEYS */;
INSERT INTO `nasabah_online` (`id_nasabah_online`, `id_status_nasabah_online`, `nama_nasabah_online`, `email_nasabah_online`, `no_hp_nasabah_online`, `password_nasabah_online`, `kode_verifikasi_bank_sampah`, `last_login`, `tgl_join`) VALUES
	(1, '1', 'mm', 'mm@gmail.com', '12344', '1111', NULL, NULL, NULL),
	(2, '3', 'mamam', 'lele@gmail.com', '12334441', '1', NULL, '2020-02-13 11:19:39', '2020-02-13 11:19:39'),
	(3, '3', 'nasabaha', 'nasabaha@gmail.com', '122222', '11111', NULL, '2020-02-13 14:43:18', '2020-02-13 14:43:18');
/*!40000 ALTER TABLE `nasabah_online` ENABLE KEYS */;

-- membuang struktur untuk table db_kompis_baru.nasabah_online_detail
CREATE TABLE IF NOT EXISTS `nasabah_online_detail` (
  `id_nasabah_online_detail` int(11) NOT NULL AUTO_INCREMENT,
  `id_nasabah_online` varchar(8) DEFAULT NULL,
  `alamat_nasabah_online` varchar(100) DEFAULT NULL,
  `lat_nasabah_online` decimal(10,8) DEFAULT NULL,
  `long_nasabah_online` decimal(11,8) DEFAULT NULL,
  `id_provinsi` int(5) DEFAULT NULL,
  `id_kabupaten` int(5) DEFAULT NULL,
  `id_kecamatan` int(5) DEFAULT NULL,
  `no_ktp_nasabah_online` int(16) DEFAULT NULL,
  `foto_ktp_nasabah_online` varchar(50) DEFAULT NULL,
  `saldo_nasabah_online` int(11) DEFAULT NULL,
  `point_nasabah_online` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_nasabah_online_detail`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- Membuang data untuk tabel db_kompis_baru.nasabah_online_detail: ~1 rows (lebih kurang)
/*!40000 ALTER TABLE `nasabah_online_detail` DISABLE KEYS */;
INSERT INTO `nasabah_online_detail` (`id_nasabah_online_detail`, `id_nasabah_online`, `alamat_nasabah_online`, `lat_nasabah_online`, `long_nasabah_online`, `id_provinsi`, `id_kabupaten`, `id_kecamatan`, `no_ktp_nasabah_online`, `foto_ktp_nasabah_online`, `saldo_nasabah_online`, `point_nasabah_online`) VALUES
	(1, '1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
/*!40000 ALTER TABLE `nasabah_online_detail` ENABLE KEYS */;

-- membuang struktur untuk table db_kompis_baru.pengepul
CREATE TABLE IF NOT EXISTS `pengepul` (
  `id_pengepul` int(11) NOT NULL AUTO_INCREMENT,
  `id_status_pengepul` int(11) NOT NULL DEFAULT '0',
  `nama_pengepul` varchar(50) DEFAULT NULL,
  `email_pengepul` varchar(50) DEFAULT NULL,
  `no_hp_pengepul` varchar(15) DEFAULT NULL,
  `password_pengepul` varchar(100) DEFAULT NULL,
  `kode_verifikasi_bank_sampah` varchar(50) DEFAULT NULL,
  `last_login` datetime DEFAULT NULL,
  `tgl_join` datetime DEFAULT NULL,
  PRIMARY KEY (`id_pengepul`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- Membuang data untuk tabel db_kompis_baru.pengepul: ~3 rows (lebih kurang)
/*!40000 ALTER TABLE `pengepul` DISABLE KEYS */;
INSERT INTO `pengepul` (`id_pengepul`, `id_status_pengepul`, `nama_pengepul`, `email_pengepul`, `no_hp_pengepul`, `password_pengepul`, `kode_verifikasi_bank_sampah`, `last_login`, `tgl_join`) VALUES
	(1, 1, 'mmm', 'mm@gmail.com', '202020', '202020', '2020', '2020-02-14 10:21:06', '2020-02-14 10:21:06'),
	(2, 3, 'mamam', 'maman@gmail.com', '1020202', '010101', '11', '2020-02-14 11:07:41', '2020-02-14 11:07:41'),
	(3, 1, 'mamamkk', 'keke@gmail.com', '18181', '1233', '1', '2020-02-14 14:06:51', '2020-02-14 14:06:51');
/*!40000 ALTER TABLE `pengepul` ENABLE KEYS */;

-- membuang struktur untuk table db_kompis_baru.pengepul_detail
CREATE TABLE IF NOT EXISTS `pengepul_detail` (
  `id_pengepul` varchar(8) DEFAULT NULL,
  `alamat_pengepul` varchar(100) DEFAULT NULL,
  `id_provinsi` int(5) DEFAULT NULL,
  `id_kabupaten` int(5) DEFAULT NULL,
  `id_kecamatan` int(5) DEFAULT NULL,
  `lat_pengepul` decimal(10,8) DEFAULT NULL,
  `long_pengepul` decimal(11,8) DEFAULT NULL,
  `no_ktp_pengepul` int(16) DEFAULT NULL,
  `foto_ktp_pengepul` varchar(50) DEFAULT NULL,
  `saldo_pengepul` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Membuang data untuk tabel db_kompis_baru.pengepul_detail: ~2 rows (lebih kurang)
/*!40000 ALTER TABLE `pengepul_detail` DISABLE KEYS */;
INSERT INTO `pengepul_detail` (`id_pengepul`, `alamat_pengepul`, `id_provinsi`, `id_kabupaten`, `id_kecamatan`, `lat_pengepul`, `long_pengepul`, `no_ktp_pengepul`, `foto_ktp_pengepul`, `saldo_pengepul`) VALUES
	('2', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	('1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	('3', 'clp', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
/*!40000 ALTER TABLE `pengepul_detail` ENABLE KEYS */;

-- membuang struktur untuk table db_kompis_baru.reward
CREATE TABLE IF NOT EXISTS `reward` (
  `id_reward` int(11) NOT NULL AUTO_INCREMENT,
  `id_tipe_bank_sampah` int(11) DEFAULT NULL,
  `nama_reward` varchar(50) DEFAULT NULL,
  `detail_reward` varchar(50) DEFAULT NULL,
  `poin_reward` int(11) DEFAULT NULL,
  `status_reward` varchar(50) DEFAULT NULL COMMENT 'Aktif dan Non Aktif',
  PRIMARY KEY (`id_reward`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- Membuang data untuk tabel db_kompis_baru.reward: ~2 rows (lebih kurang)
/*!40000 ALTER TABLE `reward` DISABLE KEYS */;
INSERT INTO `reward` (`id_reward`, `id_tipe_bank_sampah`, `nama_reward`, `detail_reward`, `poin_reward`, `status_reward`) VALUES
	(1, 2, 'mantap', 'Mantaaaapnnn', 10, '1'),
	(2, 1, 'nene', 'mmee', 19, '91');
/*!40000 ALTER TABLE `reward` ENABLE KEYS */;

-- membuang struktur untuk table db_kompis_baru.status_bank_sampah
CREATE TABLE IF NOT EXISTS `status_bank_sampah` (
  `id_status_bank_sampah` varchar(5) NOT NULL,
  `ket_status_bank_sampah` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id_status_bank_sampah`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Membuang data untuk tabel db_kompis_baru.status_bank_sampah: ~4 rows (lebih kurang)
/*!40000 ALTER TABLE `status_bank_sampah` DISABLE KEYS */;
INSERT INTO `status_bank_sampah` (`id_status_bank_sampah`, `ket_status_bank_sampah`) VALUES
	('0', 'Belum Verifikasi Email'),
	('1', 'Belum Upload Berkas'),
	('2', 'Sudah Terverifikasi'),
	('3', 'Blokir');
/*!40000 ALTER TABLE `status_bank_sampah` ENABLE KEYS */;

-- membuang struktur untuk table db_kompis_baru.status_nasabah_online
CREATE TABLE IF NOT EXISTS `status_nasabah_online` (
  `id_status_nasabah_online` varchar(5) NOT NULL,
  `ket_status_nasabah_online` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id_status_nasabah_online`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Membuang data untuk tabel db_kompis_baru.status_nasabah_online: ~4 rows (lebih kurang)
/*!40000 ALTER TABLE `status_nasabah_online` DISABLE KEYS */;
INSERT INTO `status_nasabah_online` (`id_status_nasabah_online`, `ket_status_nasabah_online`) VALUES
	('0', 'Belum Verifikasi Email'),
	('1', 'Belum Upload Berkas'),
	('2', 'Sudah Terverifikasi'),
	('3', 'Blokir');
/*!40000 ALTER TABLE `status_nasabah_online` ENABLE KEYS */;

-- membuang struktur untuk table db_kompis_baru.status_pengepul
CREATE TABLE IF NOT EXISTS `status_pengepul` (
  `id_status_pengepul` varchar(5) NOT NULL,
  `ket_status_pengepul` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id_status_pengepul`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Membuang data untuk tabel db_kompis_baru.status_pengepul: ~4 rows (lebih kurang)
/*!40000 ALTER TABLE `status_pengepul` DISABLE KEYS */;
INSERT INTO `status_pengepul` (`id_status_pengepul`, `ket_status_pengepul`) VALUES
	('0', 'Belum Verifikasi Email'),
	('1', 'Belum Upload Berkas'),
	('2', 'Sudah Terverifikasi'),
	('3', 'Blokir');
/*!40000 ALTER TABLE `status_pengepul` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
