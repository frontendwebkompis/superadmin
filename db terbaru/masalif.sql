-- --------------------------------------------------------
-- Host:                         localhost
-- Versi server:                 10.1.32-MariaDB - mariadb.org binary distribution
-- OS Server:                    Win32
-- HeidiSQL Versi:               10.2.0.5599
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- membuang struktur untuk table db_kompis_baru.bank
CREATE TABLE IF NOT EXISTS `bank` (
  `id_bank` int(11) NOT NULL AUTO_INCREMENT,
  `nama_bank` varchar(50) DEFAULT NULL,
  `kode_bank` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`id_bank`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=latin1;

-- Membuang data untuk tabel db_kompis_baru.bank: ~0 rows (lebih kurang)
/*!40000 ALTER TABLE `bank` DISABLE KEYS */;
/*!40000 ALTER TABLE `bank` ENABLE KEYS */;

-- membuang struktur untuk table db_kompis_baru.bank_sampah
CREATE TABLE IF NOT EXISTS `bank_sampah` (
  `id_bank_sampah` varchar(10) NOT NULL,
  `id_status_bank_sampah` varchar(5) NOT NULL,
  `nama_bank_sampah` varchar(50) DEFAULT NULL,
  `alamat_bank_sampah` varchar(50) DEFAULT NULL,
  `id_provinsi` int(5) DEFAULT NULL,
  `id_kabupaten` int(5) DEFAULT NULL,
  `id_kecamatan` int(5) DEFAULT NULL,
  `lat_bank_sampah` decimal(10,8) DEFAULT NULL,
  `long_bank_sampah` decimal(11,8) DEFAULT NULL,
  `email_bank_sampah` varchar(50) DEFAULT NULL,
  `no_hp_bank_sampah` varchar(15) DEFAULT NULL,
  `password_bank_sampah` varchar(30) DEFAULT NULL,
  `kode_verifikasi_bank_sampah` varchar(50) DEFAULT NULL,
  `last_login` datetime DEFAULT NULL,
  `tgl_join` datetime DEFAULT NULL,
  PRIMARY KEY (`id_bank_sampah`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Membuang data untuk tabel db_kompis_baru.bank_sampah: ~0 rows (lebih kurang)
/*!40000 ALTER TABLE `bank_sampah` DISABLE KEYS */;
/*!40000 ALTER TABLE `bank_sampah` ENABLE KEYS */;

-- membuang struktur untuk table db_kompis_baru.bank_sampah_detail
CREATE TABLE IF NOT EXISTS `bank_sampah_detail` (
  `id_bank_sampah` varchar(10) NOT NULL,
  `no_telp_bank_sampah` varchar(15) DEFAULT NULL,
  `foto_bangunan_bank_sampah` varchar(50) DEFAULT NULL,
  `sk_bank_sampah` varchar(50) DEFAULT NULL,
  `tanggal_berdiri_bank_sampah` date DEFAULT NULL,
  `slogan_bank_sampah` varchar(100) DEFAULT NULL,
  `website_bank_sampah` varchar(100) DEFAULT NULL,
  `nama_penanggungjawab` varchar(100) DEFAULT NULL,
  `no_ktp_penanggungjawab` int(16) DEFAULT NULL,
  `foto_ktp_penanggungjawab` varchar(50) DEFAULT NULL,
  `selfie_ktp_penanggungjawab` varchar(50) DEFAULT NULL,
  `alamat_penanggungjawab` varchar(100) DEFAULT NULL,
  `no_hp_penanggungjawab` varchar(15) DEFAULT NULL,
  `email_penanggungjawab` varchar(50) DEFAULT NULL,
  `saldo_bank_sampah` int(11) DEFAULT NULL,
  `point_bank_sampah` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Membuang data untuk tabel db_kompis_baru.bank_sampah_detail: ~0 rows (lebih kurang)
/*!40000 ALTER TABLE `bank_sampah_detail` DISABLE KEYS */;
/*!40000 ALTER TABLE `bank_sampah_detail` ENABLE KEYS */;

-- membuang struktur untuk table db_kompis_baru.bookmark
CREATE TABLE IF NOT EXISTS `bookmark` (
  `id_bookmark` int(11) NOT NULL AUTO_INCREMENT,
  `id_nasabah_online` varchar(8) DEFAULT NULL,
  `id_bank_sampah` varchar(8) DEFAULT NULL,
  PRIMARY KEY (`id_bookmark`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Membuang data untuk tabel db_kompis_baru.bookmark: ~0 rows (lebih kurang)
/*!40000 ALTER TABLE `bookmark` DISABLE KEYS */;
/*!40000 ALTER TABLE `bookmark` ENABLE KEYS */;

-- membuang struktur untuk table db_kompis_baru.brand
CREATE TABLE IF NOT EXISTS `brand` (
  `id_brand` int(11) NOT NULL AUTO_INCREMENT,
  `nama_brand` varchar(50) DEFAULT NULL,
  `logo_brand` varchar(100) DEFAULT NULL,
  `detail_brand` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id_brand`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;

-- Membuang data untuk tabel db_kompis_baru.brand: ~0 rows (lebih kurang)
/*!40000 ALTER TABLE `brand` DISABLE KEYS */;
/*!40000 ALTER TABLE `brand` ENABLE KEYS */;

-- membuang struktur untuk table db_kompis_baru.jabatan_pengurus
CREATE TABLE IF NOT EXISTS `jabatan_pengurus` (
  `id_jabatan_pengurus` int(11) NOT NULL AUTO_INCREMENT,
  `nama_jabatan_pengurus` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id_jabatan_pengurus`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;

-- Membuang data untuk tabel db_kompis_baru.jabatan_pengurus: ~0 rows (lebih kurang)
/*!40000 ALTER TABLE `jabatan_pengurus` DISABLE KEYS */;
/*!40000 ALTER TABLE `jabatan_pengurus` ENABLE KEYS */;

-- membuang struktur untuk table db_kompis_baru.jadwal_bank_sampah
CREATE TABLE IF NOT EXISTS `jadwal_bank_sampah` (
  `id_jadwal_bank_sampah` int(11) NOT NULL AUTO_INCREMENT,
  `id_bank_sampah` varchar(8) DEFAULT NULL,
  `id_status_buka_bank_sampah` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_jadwal_bank_sampah`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Membuang data untuk tabel db_kompis_baru.jadwal_bank_sampah: ~0 rows (lebih kurang)
/*!40000 ALTER TABLE `jadwal_bank_sampah` DISABLE KEYS */;
/*!40000 ALTER TABLE `jadwal_bank_sampah` ENABLE KEYS */;

-- membuang struktur untuk table db_kompis_baru.jadwal_bank_sampah_detail
CREATE TABLE IF NOT EXISTS `jadwal_bank_sampah_detail` (
  `id_jadwal_bank_sampah` int(11) DEFAULT NULL,
  `id_status_buka_bank_sampah` int(11) DEFAULT NULL,
  `hari_buka` varchar(50) DEFAULT NULL,
  `jam_buka` time DEFAULT NULL,
  `jam_tutup` time DEFAULT NULL,
  `jam_jemput` time DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Membuang data untuk tabel db_kompis_baru.jadwal_bank_sampah_detail: ~0 rows (lebih kurang)
/*!40000 ALTER TABLE `jadwal_bank_sampah_detail` DISABLE KEYS */;
/*!40000 ALTER TABLE `jadwal_bank_sampah_detail` ENABLE KEYS */;

-- membuang struktur untuk table db_kompis_baru.jenis_sampah
CREATE TABLE IF NOT EXISTS `jenis_sampah` (
  `id_jenis_sampah` varchar(14) NOT NULL COMMENT 'JNS<id_kategori_sampah><4 digit auto inc>',
  `id_kategori_sampah` varchar(7) DEFAULT NULL COMMENT 'KAS<4 digit auto inc>',
  `nama_jenis_sampah` varchar(50) CHARACTER SET latin1 DEFAULT NULL,
  `foto_jenis_sampah` varchar(50) CHARACTER SET latin1 DEFAULT NULL,
  `deskripsi_jenis_sampah` varchar(100) CHARACTER SET latin1 DEFAULT NULL,
  `id_superadmin` varchar(7) NOT NULL COMMENT 'SPR<kode 4 digit auto>',
  `status_jenis_sampah_aktif` char(1) NOT NULL COMMENT '0 = Non Aktif, 1 = Aktif',
  `status_jenis_sampah_remove` char(1) NOT NULL COMMENT '0 = Hidden, 1 = Visible',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `update_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id_jenis_sampah`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Membuang data untuk tabel db_kompis_baru.jenis_sampah: ~9 rows (lebih kurang)
/*!40000 ALTER TABLE `jenis_sampah` DISABLE KEYS */;
INSERT INTO `jenis_sampah` (`id_jenis_sampah`, `id_kategori_sampah`, `nama_jenis_sampah`, `foto_jenis_sampah`, `deskripsi_jenis_sampah`, `id_superadmin`, `status_jenis_sampah_aktif`, `status_jenis_sampah_remove`, `created_at`, `update_at`) VALUES
	('1', 'KAS0002', 'Blowing', NULL, 'Plastik jenis botol oli dll', 'SPR2104', '1', '1', '2020-03-12 07:35:49', '0000-00-00 00:00:00'),
	('10', 'KAS0001', 'PET Biru Muda Bersih', NULL, 'PET botol minuman plastik warna bening biru lepas semua', 'SPR2104', '1', '1', '2020-03-12 07:37:32', '0000-00-00 00:00:00'),
	('11', 'KAS0001', 'PET Biru Muda Kotor', NULL, 'PET botol minuman plastik warna bening biru masih lengkap (tutup dan label)', 'SPR2104', '1', '1', '2020-03-12 07:37:35', '0000-00-00 00:00:00'),
	('12', 'KAS0001', 'PET Warna Bersih', NULL, 'PET botol minuman plastik berwarna masih lengkap (tutup dan label)', 'SPR2104', '1', '1', '2020-03-12 07:37:39', '0000-00-00 00:00:00'),
	('13', 'KAS0001', 'PET Warna Kotor	', NULL, 'PET botol minuman plastik berwarna masih lengkap (tutup dan label)', 'SPR2104', '1', '1', '2020-03-12 07:37:46', '0000-00-00 00:00:00'),
	('14', 'KAS0005', 'PP Bersih', NULL, 'Sampah plastik polimer termoplastik berwarna dengan kondisi bersih', 'SPR2104', '1', '1', '2020-03-12 07:37:49', '0000-00-00 00:00:00'),
	('15', 'KAS0005', 'PP Kotor', NULL, 'Sampah plastik polimer termoplastik berwarna dengan kondisi kotor', 'SPR2104', '1', '1', '2020-03-12 07:37:52', '0000-00-00 00:00:00'),
	('16', 'KAS0001', 'Plastik Campur', NULL, 'Berbagai macam sampah berbahan dasar plastik', 'SPR2104', '1', '1', '2020-03-12 07:37:54', '0000-00-00 00:00:00'),
	('17', 'KAS0016', 'Emberan', NULL, 'Botol bekas sampo, sabun, tutup botol minuman dll', 'SPR2104', '1', '1', '2020-03-12 07:37:56', '0000-00-00 00:00:00'),
	('18', 'KAS0002', 'HDPE-2 Jerigen Plastik (Blowing)', NULL, 'Sampah plastik tiup atau derigen bekas', 'SPR2104', '1', '1', '2020-03-12 07:37:58', '0000-00-00 00:00:00'),
	('19', 'KAS0011', 'Aluminium Bersih', NULL, 'Kaleng minuman bekas berbahan aluminium dengan kondisi bersih', 'SPR2104', '1', '1', '2020-03-12 07:38:01', '0000-00-00 00:00:00'),
	('2', 'KAS0011', 'Seng', NULL, 'Seng bekas', 'SPR2104', '1', '1', '2020-03-12 07:36:51', '0000-00-00 00:00:00'),
	('20', 'KAS0011', 'Aluminium Kotor', NULL, 'Sampah berbahan dasar alumunium dengan kondisi kotor', 'SPR2104', '1', '1', '2020-03-12 07:38:07', '0000-00-00 00:00:00'),
	('21', 'KAS0004', 'CD Kaset', NULL, 'CD bekas (film, lagu, data, dll)', 'SPR2104', '1', '1', '2020-03-12 07:38:09', '0000-00-00 00:00:00'),
	('22', 'KAS0002', 'Sedotan, Gelas Plastik Putih', NULL, 'Plastik dalam bentuk sedotan, minuman gelas mount tea, dll', 'SPR2104', '1', '1', '2020-03-12 07:38:13', '0000-00-00 00:00:00'),
	('23', 'KAS0004', 'PP-5 Botol Sampo dan Sejenis	', NULL, 'Sampah plastik produk tertentu', 'SPR2104', '1', '1', '2020-03-12 07:38:21', '0000-00-00 00:00:00'),
	('24', 'KAS0002', 'LD Tutup Galon', NULL, 'Plastik dalam bentuk tutup galon aqua, dan galon air minum isi ulang', 'SPR2104', '1', '1', '2020-03-12 07:38:24', '0000-00-00 00:00:00'),
	('25', 'KAS0005', 'Cover CD Mika', NULL, 'Cover CD bekas bening dari mika (film, lagu, data, dll)', 'SPR2104', '1', '1', '2020-03-12 07:38:28', '0000-00-00 00:00:00'),
	('26', 'KAS0016', 'Cover CD Hitam', NULL, 'Cover CD bekas hitam (film, lagu, data, dll)', 'SPR2104', '1', '1', '2020-03-12 07:38:30', '0000-00-00 00:00:00'),
	('27', 'KAS0007', 'Kertas Koran', NULL, 'Sampah kertas koran', 'SPR2104', '1', '1', '2020-03-12 07:38:33', '0000-00-00 00:00:00'),
	('28', 'KAS0007', 'Kertas Karton', NULL, 'Sampah kertas berbahan karton', 'SPR2104', '1', '1', '2020-03-12 07:38:35', '0000-00-00 00:00:00'),
	('29', 'KAS0007', 'Kertas Buku', NULL, 'Buku tulis dan sejenis', 'SPR2104', '1', '1', '2020-03-12 07:38:37', '0000-00-00 00:00:00'),
	('3', 'KAS0002', 'Jerigen', NULL, 'Jerigen kemasan minyak bekas dan sejenis', 'SPR2104', '1', '1', '2020-03-12 07:37:16', '0000-00-00 00:00:00'),
	('30', 'KAS0007', 'Kertas HVS Putih', NULL, 'Sampah kertas HVS berwarna putih', 'SPR2104', '1', '1', '2020-03-12 07:38:41', '0000-00-00 00:00:00'),
	('31', 'KAS0011', 'Rak Piring, dll', NULL, 'Rak piring bekas berbahan dasar alumunium', 'SPR2104', '1', '1', '2020-03-12 07:38:43', '0000-00-00 00:00:00'),
	('32', 'KAS0011', 'Besi Campur Padat', NULL, 'Besi bekas', 'SPR2104', '1', '1', '2020-03-12 07:38:46', '0000-00-00 00:00:00'),
	('33', 'KAS0011', 'Aki Bekas', NULL, 'Baterai/aki basah/kering bekas', 'SPR2104', '1', '1', '2020-03-12 07:38:48', '0000-00-00 00:00:00'),
	('34', 'KAS0007', 'Kertas Campur/Dupleks', NULL, 'Sampah kertas campuran/dupleks', 'SPR2104', '1', '1', '2020-03-12 07:38:51', '0000-00-00 00:00:00'),
	('35', 'KAS0011', 'Tembaga', NULL, 'Sampah berbahan dasar tembaga', 'SPR2104', '1', '1', '2020-03-12 07:38:57', '0000-00-00 00:00:00'),
	('36', 'KAS0008', 'Kain Bekas', NULL, 'Kain bekas berbagai corak', 'SPR2104', '1', '1', '2020-03-12 07:38:59', '0000-00-00 00:00:00'),
	('37', 'KAS0008', 'Baju Bekas	', NULL, 'Baju bekas dengan varian corak bebas', 'SPR2104', '1', '1', '2020-03-12 07:39:02', '0000-00-00 00:00:00'),
	('38', 'KAS0008', 'Pot Bunga Kain Bekas	', NULL, 'Pot bunga berbahan dasar kain bekas', 'SPR2104', '1', '1', '2020-03-12 07:39:05', '0000-00-00 00:00:00'),
	('39', 'KAS0004', 'Gelas Bening Kotor	', NULL, 'Gelas plastik bersih dengan label', 'SPR2104', '1', '1', '2020-03-12 07:39:09', '0000-00-00 00:00:00'),
	('4', 'KAS0001', 'KW3', NULL, 'PET-1 dengan label kertas (Botol Kecap, Botol Minyak Goreng,dll)	', 'SPR2104', '1', '1', '2020-03-12 07:37:19', '0000-00-00 00:00:00'),
	('40', 'KAS0004', 'Gelas Bening Bersih', NULL, 'Gelas plastik bersih tanpa label', 'SPR2104', '1', '1', '2020-03-12 07:39:12', '0000-00-00 00:00:00'),
	('41', 'KAS0001', 'Tali', NULL, 'Sampah tali bekas berbahan dasar kain', 'SPR2104', '1', '1', '2020-03-12 07:39:15', '0000-00-00 00:00:00'),
	('42', 'KAS0014', 'Minyak Jelantah', NULL, 'Minyak jelantah', 'SPR2104', '1', '1', '2020-03-12 07:39:18', '0000-00-00 00:00:00'),
	('43', 'KAS0013', 'PCB HP Nokia, Samsung', NULL, 'Sampah elektronik (cpu, laptop, server, dll)', 'SPR2104', '1', '1', '2020-03-12 07:39:22', '0000-00-00 00:00:00'),
	('44', 'KAS0013', 'Mpeg Cd-rom', NULL, 'Sampah elektronik (cpu, laptop, server, dll)', 'SPR2104', '1', '1', '2020-03-12 07:39:25', '0000-00-00 00:00:00'),
	('45', 'KAS0013', 'Panel A', NULL, 'Sampah elektronik (cpu, laptop, server, dll)', 'SPR2104', '1', '1', '2020-03-12 07:39:27', '0000-00-00 00:00:00'),
	('46', 'KAS0013', 'VGA A', NULL, 'Sampah elektronik (cpu, laptop, server, dll)', 'SPR2104', '1', '1', '2020-03-12 07:39:32', '0000-00-00 00:00:00'),
	('47', 'KAS0013', 'VGA Chipset	', NULL, 'Sampah elektronik (cpu, laptop, server, dll)', 'SPR2104', '1', '1', '2020-03-12 07:39:35', '0000-00-00 00:00:00'),
	('48', 'KAS0013', 'VGA Lepas AI	', NULL, 'Sampah elektronik (cpu, laptop, server, dll)', 'SPR2104', '1', '1', '2020-03-12 07:39:37', '0000-00-00 00:00:00'),
	('49', 'KAS0013', 'Non Chip', NULL, 'Sampah elektronik (cpu, laptop, server, dll)', 'SPR2104', '1', '1', '2020-03-12 07:39:40', '0000-00-00 00:00:00'),
	('5', 'KAS0007', 'Kertas Campur', NULL, 'Mix', 'SPR2104', '1', '1', '2020-03-12 07:36:49', '0000-00-00 00:00:00'),
	('50', 'KAS0013', 'Chip 3	', NULL, 'Sampah elektronik (cpu, laptop, server, dll)', 'SPR2104', '1', '1', '2020-03-12 07:39:43', '0000-00-00 00:00:00'),
	('51', 'KAS0013', 'Chip 2', NULL, 'Sampah elektronik (cpu, laptop, server, dll)', 'SPR2104', '1', '1', '2020-03-12 07:39:47', '0000-00-00 00:00:00'),
	('52', 'KAS0013', 'Chip 1	', NULL, 'Sampah elektronik (cpu, laptop, server, dll)', 'SPR2104', '1', '1', '2020-03-12 07:39:57', '0000-00-00 00:00:00'),
	('6', 'KAS0011', 'Kaleng', NULL, 'Kaleng susu, mentega, cat,rokok', 'SPR2104', '1', '1', '2020-03-12 07:37:21', '0000-00-00 00:00:00'),
	('7', 'KAS0007', 'Gardus', NULL, 'Kertas gardus untuk kemasan	', 'SPR2104', '1', '1', '2020-03-12 07:37:14', '0000-00-00 00:00:00'),
	('8', 'KAS0001', 'PET Bening Bersih', NULL, 'PET botol minuman plastik warna bening tanpa gelang', 'SPR2104', '1', '1', '2020-03-12 07:37:25', '0000-00-00 00:00:00'),
	('9', 'KAS0001', 'PET Bening Kotor	', NULL, 'PET botol minuman plastik warna bening masih lengkap (tutup dan label)', 'SPR2104', '1', '1', '2020-03-12 07:37:28', '0000-00-00 00:00:00');
/*!40000 ALTER TABLE `jenis_sampah` ENABLE KEYS */;

-- membuang struktur untuk table db_kompis_baru.jenis_sampah_perbanksampah
CREATE TABLE IF NOT EXISTS `jenis_sampah_perbanksampah` (
  `id_jenis_perbanksampah` int(11) NOT NULL,
  `id_bank_sampah` varchar(8) DEFAULT NULL,
  `id_kategori_sampah` int(11) DEFAULT NULL,
  `id_jenis_sampah` int(11) DEFAULT NULL,
  `harga_sampah` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_jenis_perbanksampah`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Membuang data untuk tabel db_kompis_baru.jenis_sampah_perbanksampah: ~0 rows (lebih kurang)
/*!40000 ALTER TABLE `jenis_sampah_perbanksampah` DISABLE KEYS */;
/*!40000 ALTER TABLE `jenis_sampah_perbanksampah` ENABLE KEYS */;

-- membuang struktur untuk table db_kompis_baru.jenis_tarik_saldo_offline
CREATE TABLE IF NOT EXISTS `jenis_tarik_saldo_offline` (
  `id_jenis_tarik_saldo_offline` int(11) NOT NULL AUTO_INCREMENT,
  `nama_jenis_tarik_saldo_offline` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id_jenis_tarik_saldo_offline`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;

-- Membuang data untuk tabel db_kompis_baru.jenis_tarik_saldo_offline: ~0 rows (lebih kurang)
/*!40000 ALTER TABLE `jenis_tarik_saldo_offline` DISABLE KEYS */;
/*!40000 ALTER TABLE `jenis_tarik_saldo_offline` ENABLE KEYS */;

-- membuang struktur untuk table db_kompis_baru.jenis_tarik_saldo_online
CREATE TABLE IF NOT EXISTS `jenis_tarik_saldo_online` (
  `id_jenis_tarik_saldo_online` int(11) NOT NULL AUTO_INCREMENT,
  `nama_jenis_tarik_saldo_online` varchar(50) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id_jenis_tarik_saldo_online`)
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=latin1;

-- Membuang data untuk tabel db_kompis_baru.jenis_tarik_saldo_online: ~0 rows (lebih kurang)
/*!40000 ALTER TABLE `jenis_tarik_saldo_online` DISABLE KEYS */;
/*!40000 ALTER TABLE `jenis_tarik_saldo_online` ENABLE KEYS */;

-- membuang struktur untuk table db_kompis_baru.jenis_transaksi
CREATE TABLE IF NOT EXISTS `jenis_transaksi` (
  `id_jenis_transaksi` int(11) NOT NULL AUTO_INCREMENT,
  `nama_jenis_transaksi` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id_jenis_transaksi`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- Membuang data untuk tabel db_kompis_baru.jenis_transaksi: ~0 rows (lebih kurang)
/*!40000 ALTER TABLE `jenis_transaksi` DISABLE KEYS */;
/*!40000 ALTER TABLE `jenis_transaksi` ENABLE KEYS */;

-- membuang struktur untuk table db_kompis_baru.jenjang_pendidikan
CREATE TABLE IF NOT EXISTS `jenjang_pendidikan` (
  `id_jenjang_pendidikan` int(11) NOT NULL AUTO_INCREMENT,
  `nama_jenjang_pendidikan` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id_jenjang_pendidikan`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

-- Membuang data untuk tabel db_kompis_baru.jenjang_pendidikan: ~0 rows (lebih kurang)
/*!40000 ALTER TABLE `jenjang_pendidikan` DISABLE KEYS */;
/*!40000 ALTER TABLE `jenjang_pendidikan` ENABLE KEYS */;

-- membuang struktur untuk table db_kompis_baru.kabupaten
CREATE TABLE IF NOT EXISTS `kabupaten` (
  `id_kabupaten` int(5) NOT NULL,
  `nama_kabupaten` varchar(50) NOT NULL,
  `id_provinsi` int(5) NOT NULL,
  PRIMARY KEY (`id_kabupaten`)
) ENGINE=MyISAM AUTO_INCREMENT=476 DEFAULT CHARSET=latin1;

-- Membuang data untuk tabel db_kompis_baru.kabupaten: 0 rows
/*!40000 ALTER TABLE `kabupaten` DISABLE KEYS */;
/*!40000 ALTER TABLE `kabupaten` ENABLE KEYS */;

-- membuang struktur untuk table db_kompis_baru.kategori_sampah
CREATE TABLE IF NOT EXISTS `kategori_sampah` (
  `id_kategori_sampah` varchar(7) NOT NULL COMMENT 'KAS<4 digit auto inc>',
  `nama_kategori_sampah` varchar(50) CHARACTER SET latin1 DEFAULT NULL,
  `id_superadmin` varchar(7) NOT NULL COMMENT 'SPR<kode 4 digit auto>',
  `id_treatment_sampah` int(11) NOT NULL,
  `status_kategori_sampah_aktif` char(1) NOT NULL DEFAULT '1' COMMENT '0 = Non Aktif, 1 = Aktif',
  `status_kategori_sampah_remove` char(1) NOT NULL DEFAULT '1' COMMENT '0 = Hidden, 1 = Visible',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `update_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id_kategori_sampah`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Membuang data untuk tabel db_kompis_baru.kategori_sampah: ~19 rows (lebih kurang)
/*!40000 ALTER TABLE `kategori_sampah` DISABLE KEYS */;
INSERT INTO `kategori_sampah` (`id_kategori_sampah`, `nama_kategori_sampah`, `id_superadmin`, `id_treatment_sampah`, `status_kategori_sampah_aktif`, `status_kategori_sampah_remove`, `created_at`, `update_at`) VALUES
	('', 'nn', 'SPR', 0, '', '', '2020-03-13 15:34:14', '0000-00-00 00:00:00'),
	('KAS0001', 'Plastik - PET - 1', 'SPR2104', 2, '1', '1', '2020-03-12 03:15:08', '0000-00-00 00:00:00'),
	('KAS0002', 'Plastik - HDPE - 2', 'SPR2104', 2, '1', '1', '2020-03-12 03:23:21', '0000-00-00 00:00:00'),
	('KAS0003', 'Plastik - LDPE - 4', 'SPR2104', 2, '1', '1', '2020-03-12 03:23:24', '0000-00-00 00:00:00'),
	('KAS0004', 'Plastik - PP - 5', 'SPR2104', 2, '1', '1', '2020-03-12 03:23:29', '0000-00-00 00:00:00'),
	('KAS0005', 'Plastik - PS - 6', 'SPR2104', 2, '1', '1', '2020-03-12 03:23:34', '0000-00-00 00:00:00'),
	('KAS0006', 'Plastik B3', 'SPR2104', 3, '1', '1', '2020-03-12 03:23:10', '0000-00-00 00:00:00'),
	('KAS0007', 'Kertas', 'SPR2104', 2, '1', '1', '2020-03-12 03:23:07', '0000-00-00 00:00:00'),
	('KAS0008', 'Kain', 'SPR2104', 2, '1', '1', '2020-03-12 03:23:04', '0000-00-00 00:00:00'),
	('KAS0009', 'Kaca', 'SPR2104', 2, '1', '1', '2020-03-12 03:22:59', '0000-00-00 00:00:00'),
	('KAS0010', 'Kaca B3', 'SPR2104', 3, '1', '1', '2020-03-12 03:22:56', '0000-00-00 00:00:00'),
	('KAS0011', 'Logam', 'SPR2104', 2, '1', '1', '2020-03-12 03:22:50', '0000-00-00 00:00:00'),
	('KAS0012', 'Logam B3', 'SPR2104', 3, '1', '1', '2020-03-12 03:22:47', '0000-00-00 00:00:00'),
	('KAS0013', 'Elektronik', 'SPR2104', 2, '1', '1', '2020-03-12 03:22:44', '0000-00-00 00:00:00'),
	('KAS0014', 'Cair', 'SPR2104', 1, '1', '1', '2020-03-12 03:22:39', '0000-00-00 00:00:00'),
	('KAS0015', 'Cair B3', 'SPR2104', 3, '1', '1', '2020-03-12 03:22:36', '0000-00-00 00:00:00'),
	('KAS0016', 'Lain-lainnya', 'SPR2104', 2, '1', '1', '2020-03-12 03:22:33', '0000-00-00 00:00:00'),
	('KAS0017', 'nono', 'SPR', 0, '1', '0', '2020-03-13 15:45:58', '0000-00-00 00:00:00'),
	('KAS0018', 'nnn', 'SPR', 0, '1', '1', '2020-03-13 15:47:59', '0000-00-00 00:00:00');
/*!40000 ALTER TABLE `kategori_sampah` ENABLE KEYS */;

-- membuang struktur untuk table db_kompis_baru.kecamatan
CREATE TABLE IF NOT EXISTS `kecamatan` (
  `id_kecamatan` int(5) NOT NULL,
  `nama_kecamatan` varchar(50) NOT NULL,
  `id_kabupaten` int(5) NOT NULL,
  `id_provinsi` int(5) NOT NULL,
  PRIMARY KEY (`id_kecamatan`)
) ENGINE=MyISAM AUTO_INCREMENT=6643 DEFAULT CHARSET=latin1;

-- Membuang data untuk tabel db_kompis_baru.kecamatan: 0 rows
/*!40000 ALTER TABLE `kecamatan` DISABLE KEYS */;
/*!40000 ALTER TABLE `kecamatan` ENABLE KEYS */;

-- membuang struktur untuk table db_kompis_baru.kondisi_sampah
CREATE TABLE IF NOT EXISTS `kondisi_sampah` (
  `id_kondisi_sampah` int(11) NOT NULL AUTO_INCREMENT,
  `ket_kondisi_sampah` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id_kondisi_sampah`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- Membuang data untuk tabel db_kompis_baru.kondisi_sampah: ~0 rows (lebih kurang)
/*!40000 ALTER TABLE `kondisi_sampah` DISABLE KEYS */;
/*!40000 ALTER TABLE `kondisi_sampah` ENABLE KEYS */;

-- membuang struktur untuk table db_kompis_baru.lelang
CREATE TABLE IF NOT EXISTS `lelang` (
  `id_lelang` int(11) NOT NULL,
  `id_bank_sampah` varchar(8) DEFAULT NULL,
  `id_pengepul` varchar(8) DEFAULT NULL,
  `id_jenis_sampah` int(11) DEFAULT NULL,
  `berat_sampah` int(11) DEFAULT NULL,
  `tanggal_mulai_lelang` datetime DEFAULT NULL,
  `tanggal_selesai_lelang` datetime DEFAULT NULL,
  `harga_awal_penawaran` int(11) DEFAULT NULL,
  `bin_lelang` int(11) DEFAULT NULL,
  `kelipatan_lelang` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_lelang`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Membuang data untuk tabel db_kompis_baru.lelang: ~0 rows (lebih kurang)
/*!40000 ALTER TABLE `lelang` DISABLE KEYS */;
/*!40000 ALTER TABLE `lelang` ENABLE KEYS */;

-- membuang struktur untuk table db_kompis_baru.merk_sampah
CREATE TABLE IF NOT EXISTS `merk_sampah` (
  `id_merk` int(11) NOT NULL AUTO_INCREMENT,
  `id_kategori_sampah` int(11) DEFAULT NULL,
  `nama_merk` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id_merk`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

-- Membuang data untuk tabel db_kompis_baru.merk_sampah: ~0 rows (lebih kurang)
/*!40000 ALTER TABLE `merk_sampah` DISABLE KEYS */;
/*!40000 ALTER TABLE `merk_sampah` ENABLE KEYS */;

-- membuang struktur untuk table db_kompis_baru.merk_sampahs
CREATE TABLE IF NOT EXISTS `merk_sampahs` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `merek_sampah` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Membuang data untuk tabel db_kompis_baru.merk_sampahs: ~0 rows (lebih kurang)
/*!40000 ALTER TABLE `merk_sampahs` DISABLE KEYS */;
/*!40000 ALTER TABLE `merk_sampahs` ENABLE KEYS */;

-- membuang struktur untuk table db_kompis_baru.migrations
CREATE TABLE IF NOT EXISTS `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Membuang data untuk tabel db_kompis_baru.migrations: ~0 rows (lebih kurang)
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;

-- membuang struktur untuk table db_kompis_baru.nabung_online
CREATE TABLE IF NOT EXISTS `nabung_online` (
  `id_setoran` int(11) NOT NULL AUTO_INCREMENT,
  `id_nasabah_online` varchar(8) DEFAULT NULL,
  `id_bank_sampah` varchar(8) DEFAULT NULL,
  `id_status_setoran` int(11) DEFAULT NULL,
  `id_jenis_transaksi` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_setoran`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Membuang data untuk tabel db_kompis_baru.nabung_online: ~0 rows (lebih kurang)
/*!40000 ALTER TABLE `nabung_online` DISABLE KEYS */;
/*!40000 ALTER TABLE `nabung_online` ENABLE KEYS */;

-- membuang struktur untuk table db_kompis_baru.nabung_online_detail
CREATE TABLE IF NOT EXISTS `nabung_online_detail` (
  `id_setoran` int(11) NOT NULL,
  `id_nasabah_online` varchar(8) DEFAULT NULL,
  `id_nasabah_offline` varchar(8) DEFAULT NULL,
  `id_kategori_sampah` int(11) DEFAULT NULL,
  `id_jenis_sampah` int(11) DEFAULT NULL,
  `id_merk` int(11) DEFAULT NULL,
  `id_kondisi_sampah` int(11) DEFAULT NULL,
  `berat_setoran` float DEFAULT NULL,
  `harga_setoran` int(11) DEFAULT NULL,
  `datetime_setoran` datetime DEFAULT NULL,
  PRIMARY KEY (`id_setoran`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Membuang data untuk tabel db_kompis_baru.nabung_online_detail: ~0 rows (lebih kurang)
/*!40000 ALTER TABLE `nabung_online_detail` DISABLE KEYS */;
/*!40000 ALTER TABLE `nabung_online_detail` ENABLE KEYS */;

-- membuang struktur untuk table db_kompis_baru.nasabah_offline
CREATE TABLE IF NOT EXISTS `nasabah_offline` (
  `id_nasabah_offline` varchar(8) NOT NULL,
  `nama_nasabah_offline` varchar(50) DEFAULT NULL,
  `email_nasabah_offline` varchar(50) DEFAULT NULL,
  `no_hp_nasabah_offline` varchar(15) DEFAULT NULL,
  PRIMARY KEY (`id_nasabah_offline`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Membuang data untuk tabel db_kompis_baru.nasabah_offline: ~0 rows (lebih kurang)
/*!40000 ALTER TABLE `nasabah_offline` DISABLE KEYS */;
/*!40000 ALTER TABLE `nasabah_offline` ENABLE KEYS */;

-- membuang struktur untuk table db_kompis_baru.nasabah_offline_detail
CREATE TABLE IF NOT EXISTS `nasabah_offline_detail` (
  `id_nasabah_offline` varchar(8) NOT NULL,
  `id_bank_sampah` varchar(8) NOT NULL DEFAULT '0',
  `alamat_nasabah_offline` varchar(100) DEFAULT NULL,
  `id_provinsi` int(5) DEFAULT NULL,
  `id_kabupaten` int(5) DEFAULT NULL,
  `id_kecamatan` int(5) DEFAULT NULL,
  `no_ktp_nasabah_offline` int(16) NOT NULL DEFAULT '0',
  `foto_ktp_nasabah_offline` varchar(50) NOT NULL DEFAULT '0',
  `saldo_nasabah_offline` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_nasabah_offline`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Membuang data untuk tabel db_kompis_baru.nasabah_offline_detail: ~0 rows (lebih kurang)
/*!40000 ALTER TABLE `nasabah_offline_detail` DISABLE KEYS */;
/*!40000 ALTER TABLE `nasabah_offline_detail` ENABLE KEYS */;

-- membuang struktur untuk table db_kompis_baru.nasabah_online
CREATE TABLE IF NOT EXISTS `nasabah_online` (
  `id_nasabah_online` int(11) NOT NULL AUTO_INCREMENT,
  `id_status_nasabah_online` varchar(5) NOT NULL,
  `nama_nasabah_online` varchar(50) DEFAULT NULL,
  `email_nasabah_online` varchar(50) DEFAULT NULL,
  `no_hp_nasabah_online` varchar(15) DEFAULT NULL,
  `password_nasabah_online` varchar(100) DEFAULT NULL,
  `kode_verifikasi_bank_sampah` varchar(50) DEFAULT NULL,
  `last_login` datetime DEFAULT NULL,
  `tgl_join` datetime DEFAULT NULL,
  PRIMARY KEY (`id_nasabah_online`)
) ENGINE=InnoDB AUTO_INCREMENT=28 DEFAULT CHARSET=latin1;

-- Membuang data untuk tabel db_kompis_baru.nasabah_online: ~0 rows (lebih kurang)
/*!40000 ALTER TABLE `nasabah_online` DISABLE KEYS */;
/*!40000 ALTER TABLE `nasabah_online` ENABLE KEYS */;

-- membuang struktur untuk table db_kompis_baru.nasabah_online_detail
CREATE TABLE IF NOT EXISTS `nasabah_online_detail` (
  `id_nasabah_online_detail` int(11) NOT NULL AUTO_INCREMENT,
  `id_nasabah_online` varchar(8) DEFAULT NULL,
  `alamat_nasabah_online` varchar(100) DEFAULT NULL,
  `lat_nasabah_online` decimal(10,8) DEFAULT NULL,
  `long_nasabah_online` decimal(11,8) DEFAULT NULL,
  `id_provinsi` int(5) DEFAULT NULL,
  `id_kabupaten` int(5) DEFAULT NULL,
  `id_kecamatan` int(5) DEFAULT NULL,
  `no_ktp_nasabah_online` int(16) DEFAULT NULL,
  `foto_ktp_nasabah_online` varchar(50) DEFAULT NULL,
  `saldo_nasabah_online` int(11) DEFAULT NULL,
  `point_nasabah_online` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_nasabah_online_detail`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- Membuang data untuk tabel db_kompis_baru.nasabah_online_detail: ~0 rows (lebih kurang)
/*!40000 ALTER TABLE `nasabah_online_detail` DISABLE KEYS */;
/*!40000 ALTER TABLE `nasabah_online_detail` ENABLE KEYS */;

-- membuang struktur untuk table db_kompis_baru.notifikasi
CREATE TABLE IF NOT EXISTS `notifikasi` (
  `id_notifikasi` int(11) NOT NULL AUTO_INCREMENT,
  `id_nasabah_online` varchar(8) DEFAULT NULL,
  `id_bank_sampah` varchar(8) DEFAULT NULL,
  `id_pengepul` varchar(8) DEFAULT NULL,
  `detail_notifikasi` varchar(100) DEFAULT NULL,
  `url_notifikasi` varchar(100) DEFAULT NULL COMMENT 'khusus website',
  `activity` varchar(100) DEFAULT NULL COMMENT 'khusus apps ',
  `foto_notifikasi` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id_notifikasi`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Membuang data untuk tabel db_kompis_baru.notifikasi: ~0 rows (lebih kurang)
/*!40000 ALTER TABLE `notifikasi` DISABLE KEYS */;
/*!40000 ALTER TABLE `notifikasi` ENABLE KEYS */;

-- membuang struktur untuk table db_kompis_baru.pengepul
CREATE TABLE IF NOT EXISTS `pengepul` (
  `id_pengepul` int(11) NOT NULL AUTO_INCREMENT,
  `id_status_pengepul` int(11) NOT NULL DEFAULT '0',
  `nama_pengepul` varchar(50) DEFAULT NULL,
  `email_pengepul` varchar(50) DEFAULT NULL,
  `no_hp_pengepul` varchar(15) DEFAULT NULL,
  `password_pengepul` varchar(100) DEFAULT NULL,
  `kode_verifikasi_bank_sampah` varchar(50) DEFAULT NULL,
  `last_login` datetime DEFAULT NULL,
  `tgl_join` datetime DEFAULT NULL,
  PRIMARY KEY (`id_pengepul`)
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=latin1;

-- Membuang data untuk tabel db_kompis_baru.pengepul: ~0 rows (lebih kurang)
/*!40000 ALTER TABLE `pengepul` DISABLE KEYS */;
/*!40000 ALTER TABLE `pengepul` ENABLE KEYS */;

-- membuang struktur untuk table db_kompis_baru.pengepul_detail
CREATE TABLE IF NOT EXISTS `pengepul_detail` (
  `id_pengepul` varchar(8) DEFAULT NULL,
  `alamat_pengepul` varchar(100) DEFAULT NULL,
  `id_provinsi` int(5) DEFAULT NULL,
  `id_kabupaten` int(5) DEFAULT NULL,
  `id_kecamatan` int(5) DEFAULT NULL,
  `lat_pengepul` decimal(10,8) DEFAULT NULL,
  `long_pengepul` decimal(11,8) DEFAULT NULL,
  `no_ktp_pengepul` int(16) DEFAULT NULL,
  `foto_ktp_pengepul` varchar(50) DEFAULT NULL,
  `saldo_pengepul` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Membuang data untuk tabel db_kompis_baru.pengepul_detail: ~0 rows (lebih kurang)
/*!40000 ALTER TABLE `pengepul_detail` DISABLE KEYS */;
/*!40000 ALTER TABLE `pengepul_detail` ENABLE KEYS */;

-- membuang struktur untuk table db_kompis_baru.pengurus_bank_sampah
CREATE TABLE IF NOT EXISTS `pengurus_bank_sampah` (
  `id_pengurus_bank_sampah` int(11) NOT NULL,
  `id_bank_sampah` varchar(8) NOT NULL,
  `id_jabatan_pengurus` int(11) DEFAULT NULL,
  `nama_pengurus` varchar(100) DEFAULT NULL,
  `no_ktp_pengurus` int(11) DEFAULT NULL,
  `alamat_pengurus` varchar(100) DEFAULT NULL,
  `id_provinsi` int(5) DEFAULT NULL,
  `id_kabupaten` int(5) DEFAULT NULL,
  `id_kecamatan` int(5) DEFAULT NULL,
  `no_hp_pengurus` int(11) DEFAULT NULL,
  `email_pengurus` varchar(50) DEFAULT NULL,
  `foto_ktp_pengurus` varchar(50) DEFAULT NULL,
  `foto_selfie_ktp_pengurus` varchar(50) DEFAULT NULL,
  `status_pengurus_bank_sampah` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id_pengurus_bank_sampah`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Membuang data untuk tabel db_kompis_baru.pengurus_bank_sampah: ~0 rows (lebih kurang)
/*!40000 ALTER TABLE `pengurus_bank_sampah` DISABLE KEYS */;
/*!40000 ALTER TABLE `pengurus_bank_sampah` ENABLE KEYS */;

-- membuang struktur untuk table db_kompis_baru.posts
CREATE TABLE IF NOT EXISTS `posts` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `body` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Membuang data untuk tabel db_kompis_baru.posts: ~0 rows (lebih kurang)
/*!40000 ALTER TABLE `posts` DISABLE KEYS */;
/*!40000 ALTER TABLE `posts` ENABLE KEYS */;

-- membuang struktur untuk table db_kompis_baru.promo
CREATE TABLE IF NOT EXISTS `promo` (
  `id_promo` int(11) NOT NULL AUTO_INCREMENT,
  `judul_promo` varchar(50) DEFAULT NULL,
  `gambar_promo` varchar(50) DEFAULT NULL,
  `detail_promo` varchar(100) DEFAULT NULL,
  `tipe_promo` int(11) DEFAULT NULL,
  `created_at_promo` timestamp NULL DEFAULT NULL,
  `status_promo` varchar(50) DEFAULT NULL COMMENT 'status promo: promo bank sampah atau promo nasabah',
  PRIMARY KEY (`id_promo`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=latin1;

-- Membuang data untuk tabel db_kompis_baru.promo: ~1 rows (lebih kurang)
/*!40000 ALTER TABLE `promo` DISABLE KEYS */;
INSERT INTO `promo` (`id_promo`, `judul_promo`, `gambar_promo`, `detail_promo`, `tipe_promo`, `created_at_promo`, `status_promo`) VALUES
	(16, 'nnnn', 'd07fc614dc29bff0eb4a9f9e55ff1ce5.png', 'mantap', 0, '2020-03-13 14:03:00', 'Aktif');
/*!40000 ALTER TABLE `promo` ENABLE KEYS */;

-- membuang struktur untuk table db_kompis_baru.provinsi
CREATE TABLE IF NOT EXISTS `provinsi` (
  `id_provinsi` int(5) NOT NULL,
  `nama_provinsi` varchar(50) NOT NULL,
  PRIMARY KEY (`id_provinsi`)
) ENGINE=MyISAM AUTO_INCREMENT=35 DEFAULT CHARSET=latin1;

-- Membuang data untuk tabel db_kompis_baru.provinsi: 0 rows
/*!40000 ALTER TABLE `provinsi` DISABLE KEYS */;
/*!40000 ALTER TABLE `provinsi` ENABLE KEYS */;

-- membuang struktur untuk table db_kompis_baru.redeem_bank_sampah
CREATE TABLE IF NOT EXISTS `redeem_bank_sampah` (
  `id_redeem_bank_sampah` int(11) NOT NULL AUTO_INCREMENT,
  `id_bank_sampah` int(11) DEFAULT NULL,
  `id_reward` int(11) DEFAULT NULL,
  `id_merchant` int(11) DEFAULT NULL,
  `datetime_redeem_bank_sampah` datetime DEFAULT NULL,
  PRIMARY KEY (`id_redeem_bank_sampah`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Membuang data untuk tabel db_kompis_baru.redeem_bank_sampah: ~0 rows (lebih kurang)
/*!40000 ALTER TABLE `redeem_bank_sampah` DISABLE KEYS */;
/*!40000 ALTER TABLE `redeem_bank_sampah` ENABLE KEYS */;

-- membuang struktur untuk table db_kompis_baru.redeem_nasabah
CREATE TABLE IF NOT EXISTS `redeem_nasabah` (
  `id_redeem_nasabah` int(11) NOT NULL AUTO_INCREMENT,
  `id_nasabah_online` varchar(8) DEFAULT NULL,
  `id_reward` int(11) DEFAULT NULL,
  `id_merchant` int(11) DEFAULT NULL,
  `datetime_redeem_nasabah` datetime DEFAULT NULL,
  PRIMARY KEY (`id_redeem_nasabah`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Membuang data untuk tabel db_kompis_baru.redeem_nasabah: ~0 rows (lebih kurang)
/*!40000 ALTER TABLE `redeem_nasabah` DISABLE KEYS */;
/*!40000 ALTER TABLE `redeem_nasabah` ENABLE KEYS */;

-- membuang struktur untuk table db_kompis_baru.review
CREATE TABLE IF NOT EXISTS `review` (
  `id_review` int(11) NOT NULL AUTO_INCREMENT,
  `id_nasabah_online` varchar(8) DEFAULT NULL,
  `id_bank_sampah` varchar(8) DEFAULT NULL,
  `id_pengepul` varchar(8) DEFAULT NULL,
  `nilai_nasabah_online` int(11) DEFAULT NULL,
  `nilai_bank_sampah` int(11) DEFAULT NULL COMMENT 'bintang 1 sampai 5',
  `nilai_pengepul` int(11) DEFAULT NULL,
  `ulasan_review` varchar(100) DEFAULT NULL,
  `datetime_review` datetime DEFAULT NULL,
  PRIMARY KEY (`id_review`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Membuang data untuk tabel db_kompis_baru.review: ~0 rows (lebih kurang)
/*!40000 ALTER TABLE `review` DISABLE KEYS */;
/*!40000 ALTER TABLE `review` ENABLE KEYS */;

-- membuang struktur untuk table db_kompis_baru.reward
CREATE TABLE IF NOT EXISTS `reward` (
  `id_reward` int(11) NOT NULL AUTO_INCREMENT,
  `id_tipe_bank_sampah` int(11) DEFAULT NULL,
  `nama_reward` varchar(50) DEFAULT NULL,
  `detail_reward` varchar(50) DEFAULT NULL,
  `poin_reward` int(11) DEFAULT NULL,
  `status_reward` varchar(50) DEFAULT NULL COMMENT 'Aktif dan Non Aktif',
  PRIMARY KEY (`id_reward`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;

-- Membuang data untuk tabel db_kompis_baru.reward: ~0 rows (lebih kurang)
/*!40000 ALTER TABLE `reward` DISABLE KEYS */;
/*!40000 ALTER TABLE `reward` ENABLE KEYS */;

-- membuang struktur untuk table db_kompis_baru.riwayat_penawaran
CREATE TABLE IF NOT EXISTS `riwayat_penawaran` (
  `id_lelang` int(11) DEFAULT NULL,
  `id_pengepul` varchar(8) DEFAULT NULL,
  `id_bank_sampah` varchar(8) DEFAULT NULL,
  `jumlah_penawaran` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Membuang data untuk tabel db_kompis_baru.riwayat_penawaran: ~0 rows (lebih kurang)
/*!40000 ALTER TABLE `riwayat_penawaran` DISABLE KEYS */;
/*!40000 ALTER TABLE `riwayat_penawaran` ENABLE KEYS */;

-- membuang struktur untuk table db_kompis_baru.sekolah
CREATE TABLE IF NOT EXISTS `sekolah` (
  `id_lembaga_pendidikan` int(11) NOT NULL,
  `id_jenjang_pendidikan` int(11) DEFAULT NULL,
  `id_bank_sampah` varchar(8) DEFAULT NULL,
  PRIMARY KEY (`id_lembaga_pendidikan`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Membuang data untuk tabel db_kompis_baru.sekolah: ~0 rows (lebih kurang)
/*!40000 ALTER TABLE `sekolah` DISABLE KEYS */;
/*!40000 ALTER TABLE `sekolah` ENABLE KEYS */;

-- membuang struktur untuk table db_kompis_baru.siswa
CREATE TABLE IF NOT EXISTS `siswa` (
  `nis` varchar(50) NOT NULL,
  `nama_siswa` varchar(100) DEFAULT NULL,
  `email_siswa` varchar(50) DEFAULT NULL,
  `no_hp_siswa` varchar(15) DEFAULT NULL,
  PRIMARY KEY (`nis`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Membuang data untuk tabel db_kompis_baru.siswa: ~0 rows (lebih kurang)
/*!40000 ALTER TABLE `siswa` DISABLE KEYS */;
/*!40000 ALTER TABLE `siswa` ENABLE KEYS */;

-- membuang struktur untuk table db_kompis_baru.smartbin
CREATE TABLE IF NOT EXISTS `smartbin` (
  `id_smartbin` int(11) NOT NULL AUTO_INCREMENT,
  `id_brand` int(11) NOT NULL DEFAULT '0',
  `kode_smartbin` varchar(10) DEFAULT NULL,
  `lokasi_smartbin` varchar(50) DEFAULT NULL,
  `detail_smartbin` varchar(50) DEFAULT NULL,
  `code_versi_smartbin` varchar(10) DEFAULT NULL,
  `api_check_smartbin` varchar(50) DEFAULT NULL,
  `api_flush_smartbin` varchar(50) DEFAULT NULL,
  `status_smartbin` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id_smartbin`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Membuang data untuk tabel db_kompis_baru.smartbin: ~0 rows (lebih kurang)
/*!40000 ALTER TABLE `smartbin` DISABLE KEYS */;
/*!40000 ALTER TABLE `smartbin` ENABLE KEYS */;

-- membuang struktur untuk table db_kompis_baru.smartbin_counter
CREATE TABLE IF NOT EXISTS `smartbin_counter` (
  `id_smartbin` int(11) NOT NULL,
  `id_nasabah_online` varchar(8) DEFAULT NULL,
  `kode_smartbin` varchar(10) DEFAULT NULL,
  `jumlah_botol` int(11) DEFAULT NULL,
  `point` int(11) DEFAULT NULL,
  `datetime` datetime DEFAULT NULL,
  `status` varchar(50) DEFAULT NULL,
  `createsd_at` date DEFAULT NULL,
  PRIMARY KEY (`id_smartbin`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Membuang data untuk tabel db_kompis_baru.smartbin_counter: ~0 rows (lebih kurang)
/*!40000 ALTER TABLE `smartbin_counter` DISABLE KEYS */;
/*!40000 ALTER TABLE `smartbin_counter` ENABLE KEYS */;

-- membuang struktur untuk table db_kompis_baru.status_bank_sampah
CREATE TABLE IF NOT EXISTS `status_bank_sampah` (
  `id_status_bank_sampah` varchar(5) NOT NULL,
  `ket_status_bank_sampah` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id_status_bank_sampah`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Membuang data untuk tabel db_kompis_baru.status_bank_sampah: ~0 rows (lebih kurang)
/*!40000 ALTER TABLE `status_bank_sampah` DISABLE KEYS */;
/*!40000 ALTER TABLE `status_bank_sampah` ENABLE KEYS */;

-- membuang struktur untuk table db_kompis_baru.status_buka_bank_sampah
CREATE TABLE IF NOT EXISTS `status_buka_bank_sampah` (
  `id_status_buka_bank_sampah` int(11) NOT NULL AUTO_INCREMENT,
  `ket_status_buka_bank_sampah` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id_status_buka_bank_sampah`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

-- Membuang data untuk tabel db_kompis_baru.status_buka_bank_sampah: ~0 rows (lebih kurang)
/*!40000 ALTER TABLE `status_buka_bank_sampah` DISABLE KEYS */;
/*!40000 ALTER TABLE `status_buka_bank_sampah` ENABLE KEYS */;

-- membuang struktur untuk table db_kompis_baru.status_nasabah_online
CREATE TABLE IF NOT EXISTS `status_nasabah_online` (
  `id_status_nasabah_online` varchar(5) NOT NULL,
  `ket_status_nasabah_online` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id_status_nasabah_online`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Membuang data untuk tabel db_kompis_baru.status_nasabah_online: ~0 rows (lebih kurang)
/*!40000 ALTER TABLE `status_nasabah_online` DISABLE KEYS */;
/*!40000 ALTER TABLE `status_nasabah_online` ENABLE KEYS */;

-- membuang struktur untuk table db_kompis_baru.status_pengepul
CREATE TABLE IF NOT EXISTS `status_pengepul` (
  `id_status_pengepul` varchar(5) NOT NULL,
  `ket_status_pengepul` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id_status_pengepul`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Membuang data untuk tabel db_kompis_baru.status_pengepul: ~0 rows (lebih kurang)
/*!40000 ALTER TABLE `status_pengepul` DISABLE KEYS */;
/*!40000 ALTER TABLE `status_pengepul` ENABLE KEYS */;

-- membuang struktur untuk table db_kompis_baru.status_setoran
CREATE TABLE IF NOT EXISTS `status_setoran` (
  `id_status_setoran` int(11) NOT NULL AUTO_INCREMENT,
  `nama_status_setoran` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id_status_setoran`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

-- Membuang data untuk tabel db_kompis_baru.status_setoran: ~0 rows (lebih kurang)
/*!40000 ALTER TABLE `status_setoran` DISABLE KEYS */;
/*!40000 ALTER TABLE `status_setoran` ENABLE KEYS */;

-- membuang struktur untuk table db_kompis_baru.superadmin
CREATE TABLE IF NOT EXISTS `superadmin` (
  `id_superadmin` varchar(7) NOT NULL COMMENT 'SPR<kode 4 digit auto>',
  `username` varchar(30) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `nama_superadmin` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `email_superadmin` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `no_tlp_superadmin` varchar(50) DEFAULT NULL,
  `password` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `status_superadmin_aktif` char(1) NOT NULL COMMENT '0 = Non Aktif, 1 = Aktif',
  `status_superadmin_remove` char(1) NOT NULL COMMENT '0 = Hidden, 1 = Visible',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `update_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id_superadmin`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Membuang data untuk tabel db_kompis_baru.superadmin: ~3 rows (lebih kurang)
/*!40000 ALTER TABLE `superadmin` DISABLE KEYS */;
INSERT INTO `superadmin` (`id_superadmin`, `username`, `nama_superadmin`, `email_superadmin`, `no_tlp_superadmin`, `password`, `status_superadmin_aktif`, `status_superadmin_remove`, `created_at`, `update_at`) VALUES
	('SPR', 'mm', NULL, 'ulin@mm.com', NULL, '8d969eef6ecad3c29a3a629280e686cf0c3f5d5a86aff3ca12020c923adc6c92', '', '', '2020-03-13 15:22:28', '0000-00-00 00:00:00'),
	('SPR2101', 'admin', 'Manila Kristin', 'maniilakr@kompiscreative.tech', '087839911121', '', '1', '1', '2020-03-13 14:43:18', '0000-00-00 00:00:00'),
	('SPR2104', 'admin', 'Ulinnuha', 'ulin@gmail.com', '081992929292', 'b0b5afc924c6f892bacc3ea6b492ec5223f0815ec5284336be81504efb4d019a', '1', '1', '2020-03-13 14:46:31', '0000-00-00 00:00:00');
/*!40000 ALTER TABLE `superadmin` ENABLE KEYS */;

-- membuang struktur untuk table db_kompis_baru.tabel_harga
CREATE TABLE IF NOT EXISTS `tabel_harga` (
  `id_tabel_harga` int(11) NOT NULL AUTO_INCREMENT,
  `id_kategori_sampah` int(11) DEFAULT NULL,
  `id_jenis_sampah` int(11) DEFAULT NULL,
  `harga_nasional` int(11) NOT NULL,
  PRIMARY KEY (`id_tabel_harga`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;

-- Membuang data untuk tabel db_kompis_baru.tabel_harga: ~0 rows (lebih kurang)
/*!40000 ALTER TABLE `tabel_harga` DISABLE KEYS */;
/*!40000 ALTER TABLE `tabel_harga` ENABLE KEYS */;

-- membuang struktur untuk table db_kompis_baru.tabel_harga_detail
CREATE TABLE IF NOT EXISTS `tabel_harga_detail` (
  `id_tbl_harga_detail` int(11) NOT NULL AUTO_INCREMENT,
  `id_tabel_harga` int(11) DEFAULT NULL,
  `id_provinsi` int(5) DEFAULT NULL,
  `harga_provinsi` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_tbl_harga_detail`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- Membuang data untuk tabel db_kompis_baru.tabel_harga_detail: ~0 rows (lebih kurang)
/*!40000 ALTER TABLE `tabel_harga_detail` DISABLE KEYS */;
/*!40000 ALTER TABLE `tabel_harga_detail` ENABLE KEYS */;

-- membuang struktur untuk table db_kompis_baru.tarik_saldo_offline
CREATE TABLE IF NOT EXISTS `tarik_saldo_offline` (
  `id_tarik_saldo_offline` int(11) NOT NULL AUTO_INCREMENT,
  `id_jenis_tarik_saldo_offline` int(11) DEFAULT NULL,
  `id_nasabah_offline` varchar(8) DEFAULT NULL,
  `id_bank_sampah` varchar(8) DEFAULT NULL,
  `datetime_tarik_saldo_offline` datetime DEFAULT NULL,
  PRIMARY KEY (`id_tarik_saldo_offline`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Membuang data untuk tabel db_kompis_baru.tarik_saldo_offline: ~0 rows (lebih kurang)
/*!40000 ALTER TABLE `tarik_saldo_offline` DISABLE KEYS */;
/*!40000 ALTER TABLE `tarik_saldo_offline` ENABLE KEYS */;

-- membuang struktur untuk table db_kompis_baru.tarik_saldo_online
CREATE TABLE IF NOT EXISTS `tarik_saldo_online` (
  `id_tarik_saldo_online` int(11) NOT NULL AUTO_INCREMENT,
  `id_jenis_tarik_saldo_online` int(11) DEFAULT NULL,
  `id_nasabah_online` varchar(8) DEFAULT NULL,
  `id_bank_sampah` varchar(8) DEFAULT NULL,
  `datetime_transaksi_saldo` datetime DEFAULT NULL,
  PRIMARY KEY (`id_tarik_saldo_online`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Membuang data untuk tabel db_kompis_baru.tarik_saldo_online: ~0 rows (lebih kurang)
/*!40000 ALTER TABLE `tarik_saldo_online` DISABLE KEYS */;
/*!40000 ALTER TABLE `tarik_saldo_online` ENABLE KEYS */;

-- membuang struktur untuk table db_kompis_baru.tb_superadmin
CREATE TABLE IF NOT EXISTS `tb_superadmin` (
  `id_super` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `password` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nama_admin` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `last_login` datetime DEFAULT NULL,
  `dibuat_tgl` datetime DEFAULT NULL,
  PRIMARY KEY (`id_super`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Membuang data untuk tabel db_kompis_baru.tb_superadmin: ~1 rows (lebih kurang)
/*!40000 ALTER TABLE `tb_superadmin` DISABLE KEYS */;
INSERT INTO `tb_superadmin` (`id_super`, `username`, `password`, `email`, `nama_admin`, `last_login`, `dibuat_tgl`) VALUES
	(2, 'nn', '8d969eef6ecad3c29a3a629280e686cf0c3f5d5a86aff3ca12020c923adc6c92', 'ulin@gmail.com', 'joss', '2020-03-13 16:07:01', NULL);
/*!40000 ALTER TABLE `tb_superadmin` ENABLE KEYS */;

-- membuang struktur untuk table db_kompis_baru.tipe_bank_sampah
CREATE TABLE IF NOT EXISTS `tipe_bank_sampah` (
  `id_tipe_bank_sampah` int(11) NOT NULL AUTO_INCREMENT,
  `nama_tipe_bank_sampah` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id_tipe_bank_sampah`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- Membuang data untuk tabel db_kompis_baru.tipe_bank_sampah: ~0 rows (lebih kurang)
/*!40000 ALTER TABLE `tipe_bank_sampah` DISABLE KEYS */;
/*!40000 ALTER TABLE `tipe_bank_sampah` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
