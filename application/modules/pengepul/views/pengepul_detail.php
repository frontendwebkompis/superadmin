<script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>

<div class="row">
  <section class="col-lg connectedSortable ui-sortable">
    <div class="card card-info">
      <div class="card-header">
        <div class="card-title">
          <?= $title_card; ?>
        </div>
      </div>
      <div class="card-body">
        <div class="tab-content p-0">
          <div class="row">
            <div class="col-md-8">
              <table class="table table-bordered table-striped table-condensed table-sm" style="margin-bottom: 10px">
                <tr>
                  <td class="font-weight-bold px-3">Nama Pengepul</td>
                  <td><?php
                      if ($nama_pengepul == null) {
                        echo '<span class="badge badge-danger">Kosong</span>';
                      } else {
                        echo $nama_pengepul;
                      }  ?></td>
                </tr>
                <tr>
                  <td class="font-weight-bold px-3">Email Pengepul</td>
                  <td><?php
                      if ($email_pengepul == null) {
                        echo '<span class="badge badge-danger">Kosong</span>';
                      } else {
                        echo $email_pengepul;
                      }  ?></td>
                </tr>
                <tr>
                  <td class="font-weight-bold px-3">No Hp Pengepul</td>
                  <td><?php
                      if ($no_hp_pengepul == null) {
                        echo '<span class="badge badge-danger">Kosong</span>';
                      } else {
                        echo $no_hp_pengepul;
                      }  ?></td>
                </tr>
                <tr>
                  <td class="font-weight-bold px-3">Kode Verifikasi Pengepul</td>
                  <td><?php
                      if ($kode_verifikasi_pengepul == null) {
                        echo '<span class="badge badge-danger">Kosong</span>';
                      } else {
                        echo $kode_verifikasi_pengepul;
                      }  ?></td>
                </tr>
                <tr>
                  <td class="font-weight-bold px-3">Tanggal Join</td>
                  <td><?php
                      if ($tgl_daftar_pengepul == null) {
                        echo '<span class="badge badge-danger">Kosong</span>';
                      } else {
                        echo fulldate($tgl_daftar_pengepul);
                      }  ?></td>
                </tr>
                <tr>
                  <td class="font-weight-bold px-3">Last Login</td>
                  <td><?php
                      if ($last_login == null) {
                        echo '<span class="badge badge-danger">Kosong</span>';
                      } else {
                        echo fulldate($last_login);
                      }  ?></td>
                </tr>
                <tr>
                  <td class="font-weight-bold px-3">Alamat Pengepul</td>
                  <td><?php
                      if ($alamat_pengepul == null) {
                        echo '<span class="badge badge-danger">Kosong</span>';
                      } else {
                        echo $alamat_pengepul;
                      }  ?></td>
                </tr>
                <tr>
                  <td class="font-weight-bold px-3">Provinsi</td>
                  <td><?php
                      if ($nama_provinsi == null) {
                        echo '<span class="badge badge-danger">Kosong</span>';
                      } else {
                        echo $nama_provinsi;
                      }  ?></td>
                </tr>
                <tr>
                  <td class="font-weight-bold px-3">Kabupaten</td>
                  <td><?php
                      if ($nama_kabupaten == null) {
                        echo '<span class="badge badge-danger">Kosong</span>';
                      } else {
                        echo $nama_kabupaten;
                      }  ?></td>
                </tr>
                <tr>
                  <td class="font-weight-bold px-3">Kecamatan</td>
                  <td><?php
                      if ($nama_kecamatan == null) {
                        echo '<span class="badge badge-danger">Kosong</span>';
                      } else {
                        echo $nama_kecamatan;
                      }  ?></td>
                </tr>
                <tr>
                  <td class="font-weight-bold px-3">lat</td>
                  <td><?php
                      if ($lat_pengepul == null) {
                        echo '<span class="badge badge-danger">Kosong</span>';
                      } else {
                        echo $lat_pengepul;
                      }  ?></td>
                </tr>
                <tr>
                  <td class="font-weight-bold px-3">long</td>
                  <td><?php
                      if ($long_pengepul == null) {
                        echo '<span class="badge badge-danger">Kosong</span>';
                      } else {
                        echo $long_pengepul;
                      }  ?></td>
                </tr>
                <tr>
                  <td class="font-weight-bold px-3">Nomor Ktp</td>
                  <td><?php
                      if ($no_ktp_pengepul == null) {
                        echo '<span class="badge badge-danger">Kosong</span>';
                      } else {
                        echo $no_ktp_pengepul;
                      }  ?>
                        <button id="cekktp" onclick="cekktpasli('<?= $no_ktp_pengepul ?>','<?= $nama_pengepul ?>')" class="btn-sm btn-primary" 
                        href=""
                        >Cek Ktp validasi ktp</button>
                      </td>
                </tr>
                <tr>
                  <td class="font-weight-bold px-3">Saldo Pengepul</td>
                  <td><?php
                      if ($saldo_pengepul == null) {
                        echo '<span class="badge badge-danger">Kosong</span>';
                      } else {
                        echo $saldo_pengepul;
                      }  ?></td>
                </tr>
              </table>
            </div>
            <div class="col-md-4">
              <div class="text-center font-weight-bold">Foto KTP Pengepul</div>
              <div class="text-center">
                <?php
                if ($foto_ktp_pengepul == null) {
                  echo '<span class="badge badge-danger">Kosong</span>';
                } else { ?>
                  <a href="<?= base_url() ?>pengepul/get_by_ktp?img=<?= $foto_ktp_pengepul ?>" data-toggle="lightbox" data-max-width="800" data-title="Foto KTP Pengepul">
                    <img src="<?= base_url() ?>pengepul/get_by_ktp?img=<?= $foto_ktp_pengepul ?>" class="img-fluid img-thumbnail mb-2" style="max-width: 250px; max-height: 130px;" alt="Foto KTP Pengepul">
                  </a>
                <?php  }
                ?>
              </div>
              <div class="text-center font-weight-bold">Foto Selfie KTP</div>
              <div class="text-center">
                <?php
                if ($foto_apsi == null) {
                  echo '<span class="badge badge-danger">Kosong</span>';
                } else { ?>
                  <a href="<?= base_url() ?>pengepul/get_image?img=<?= $foto_ktp_selfie_pengepul ?>" data-toggle="lightbox" data-max-width="800" data-title="Foto Apsi">
                    <img src="<?= base_url() ?>pengepul/get_image?img=<?= $foto_ktp_selfie_pengepul ?>" class="img-fluid img-thumbnail mb-2" style="max-width: 250px; max-height: 130px;" alt="Foto Apsi">
                  </a>
                <?php  }
                ?>
              </div>
              <div class="text-center font-weight-bold">Foto Apsi</div>
              <div class="text-center">
                <?php
                if ($foto_apsi == null) {
                  echo '<span class="badge badge-danger">Kosong</span>';
                } else { ?>
                  <a href="<?= base_url() ?>pengepul/get_by_apsi?img=<?= $foto_apsi ?>" data-toggle="lightbox" data-max-width="800" data-title="Foto Apsi">
                    <img src="<?= base_url() ?>pengepul/get_by_apsi?img=<?= $foto_apsi ?>" class="img-fluid img-thumbnail mb-2" style="max-width: 250px; max-height: 130px;" alt="Foto Apsi">
                  </a>
                <?php  }
                ?>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-sm-12">
              <div class="btn-group btn-group-sm">
                <a href="<?php echo site_url('pengepul/premium') ?>" title="Kembali" class="btn btn-primary" style="width: 120px;"><i class="far fa-arrow-alt-circle-left"> Kembali</i></a>
                <?php if ($id_status_pengepul != 4) { ?>
                  <?php if ($id_status_pengepul != 3 && $id_status_pengepul != 1) { ?>
                    <a href="#" class="btn btn-warning" title="Jadikan Premium" onclick="premium('<?= $id_pengepul ?>')" style="width: 120px;"><i class="fas fa-info-circle"></i> Premium</a>

                    <button type="button" class="btn btn-default" data-toggle="modal" title="Tolak berkas" data-target="#modal-default" style="width: 120px;">Tolak Berkas</button>
                  <?php } ?>
                  <a href="<?php echo site_url('pengepul/blokir/' . $id_pengepul); ?>" title="Blokir" class="btn btn-danger" style="width: 120px;"><i class="fas fa-power-off"></i> Blokir</a>
                <?php } else { ?>
                  <a href="#" class="btn btn-success" title="Aktifkan Pengepul" onclick="unblokir('<?= $id_pengepul ?>')" style="width: 120px;"><i class="fas fa-check"></i> Aktifkan</a>
                <?php } ?>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
</div>



<div class="modal fade" id="modal-default">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Tolak Berkas</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form method="post" action="<?php echo site_url('pengepul/tolak_berkas') ?>">
        <div class="modal-body">

          <div class="form-group">
            <label for="exampleFormControlSelect1">Alasan Tolak</label>
            <select name="reason" class="form-control">
              <option value="">Pilih Alasan Tolak</option>
              <?php foreach ($reason as $res) : ?>
                <option value="<?php echo $res->id_reason; ?>"><?php echo $res->desc_reason; ?></option>
              <?php endforeach ?>
            </select>
          </div>
          <input type="hidden" name="id_pengepul" value="<?= $id_pengepul ?>">

        </div>
        <div class="modal-footer justify-content-between">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          <button class="btn btn-danger">Tolak Berkas</button>
        </div>
      </form>

    </div>
  </div>
</div>

<!-- Ekko Lightbox -->
<script src="<?= base_url('assets/plugins/ekko-lightbox/ekko-lightbox.min.js') ?>"></script>
<script>
  $(function() {
    $(document).on('click', '[data-toggle="lightbox"]', function(event) {
      event.preventDefault();
      $(this).ekkoLightbox({
        alwaysShowClose: true
      });
    });

    $('.filter-container').filterizr({
      gutterPixels: 3
    });
    $('.btn[data-filter]').on('click', function() {
      $('.btn[data-filter]').removeClass('active');
      $(this).addClass('active');
    });
  })
</script>
<script>
  function cekktpasli(noktp,nama) {
     // alert(''+noktp+'  '+nama);
      $.ajax({
                url: '<?php echo base_url() ?>pengepul/cek_ktp_asli?ktp='+noktp+'&nama='+nama,
                  dataType: "json",
                cache: false,
                success: function(res) {
                  if(res.message=='failed'){
                     alert('Nama dan NIK tidak Sesuai');
                  }else{
                     alert('Nama dan NIK Sesuai');
                  }
                }
              });
  }
   function confirm(res) {
    Swal.fire({
      title: 'Anda Yakin Mau Blokir Pengepul?',
      text: "Akun akan segera di Blokir!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Ya, Blokir!',
      cancelButtonText: 'Batal'
    }).then((result) => {
      if (result.value) {
        Swal.fire(
          'Terblokir!',
          'Akun sudah diblokir.',
          'success'
        )
        window.location = '<?php echo base_url() . 'pengepul/ubahstatus/'; ?>' + res;
      }
    });
  }

  function premium(res) {
    Swal.fire({
      title: 'Anda Yakin mau Premium Akun?',
      text: "Akan segera di Premium kan!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Ya, Aktifkan!',
      cancelButtonText: 'Batal'
    }).then((result) => {
      if (result.value) {
        Swal.fire(
          'Premium!',
          'Akun sudah Premium.',
          'success'
        )
        window.location = '<?php echo base_url() . 'pengepul/ubahpremium/'; ?>' + res;
      }
    });
  }

  function unblokir(res) {
    Swal.fire({
      title: 'Anda Yakin Mau Unblokir Pengepul?',
      text: "Akun akan segera di Unblokir!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Ya, Aktifkan!',
      cancelButtonText: 'Batal'
    }).then((result) => {
      if (result.value) {
        Swal.fire(
          'Aktif!',
          'Akun sudah di Unblokir.',
          'success'
        )
        window.location = '<?php echo base_url() . 'pengepul/ubahbanned/'; ?>' + res;
      }
    });
  }
</script>