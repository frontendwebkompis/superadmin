<script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>

<div class="row">
	<!-- Left col -->
	<section class="col-lg connectedSortable ui-sortable">
		<!-- Custom tabs (Charts with tabs)-->
		<div class="card">
			<div class="card-body">
				<div class="tab-content p-0">
					
					<table class="table table-bordered table-striped table-condensed" style="margin-bottom: 20px">
						<tr>	
							<td>Status Pengepul</td>
							<td><?php echo $status_pengepul; ?></td>
						</tr>
						<tr>
                            <td>Nama Pengepul</td>
                            <td><?php echo $nama_pengepul; ?></td>
						</tr>
						<tr>
                            <td>Alamat Pengepul</td>
                            <td><?php echo $alamat_pengepul; ?></td>
						</tr>
                        <tr>
                            <td>Email Pengepul</td>
                            <td><?php echo $email_pengepul; ?></td>
						</tr>
                        <tr>
                            <td>No Hp Pengepul</td>
                            <td><?php echo $no_hp_pengepul; ?></td>
						</tr>
						<tr>
                            <td>Last Login</td>
                            <td><?php echo $last_login; ?></td>
						</tr>
					</table>
                    <div class="form-group">
                            <label for="exampleFormControlSelect1">Alasan Blokir</label>
                            <select name="reason" id="reason" class="form-control">
                            <option value="">Pilih Alasan Blokir</option>
                             <?php foreach ($reason as $res): ?>
                                 <option value="<?php echo $res->id_reason; ?>"><?php echo $res->desc_reason; ?></option>
                            <?php endforeach ?>
                            </select>
                        </div>
					<div class="btn-group">
						<a href="<?php echo site_url('pengepul') ?>" class="btn btn-primary btn-sm">Batal</a></td>
                        <a href="#" id="blokir" class="btn btn-danger btn-sm" data-toggle="tooltip" data-placement="top" title="Non Aktif" 
                        onclick="confirm('<?=  $id_pengepul ?>')" >Blokir</a>
					</div>

				</div>
			</div><!-- /.card-body -->
		</div>
		<!-- /.card -->
	</section>
	<!-- /.Left col -->
</div>



<script>
    $("#reason"). change(function(){
var cek = $(this). children("option:selected"). val();
// alert(cek);
if(cek>0){
    $('#blokir').css('display','block');
}else{
$('#blokir').css('display','none');
}
});
function confirm(res) {
    Swal.fire({
    title: 'Anda Yakin Mau Blokir Pengepul?',
    text: "Akun akan segera di Blokir!",
    icon: 'warning',
    showCancelButton: true,
    confirmButtonColor: '#3085d6',
    cancelButtonColor: '#d33',
    confirmButtonText: 'Ya, Blokir!',
    cancelButtonText: 'Batal'
    }).then((result) => {
    if (result.value) {
        Swal.fire(
        'Terblokir!',
        'Akun sudah diblokir.',
        'success'
        )
        window.location='<?php echo base_url().'pengepul/ubahstatus/'; ?>'+res+'?reason='+$('#reason').val();
    }
    });
}

function premium(res) {
    Swal.fire({
    title: 'Anda Yakin mau Premium Akun?',
    text: "Akan segera di Premium kan!",
    icon: 'warning',
    showCancelButton: true,
    confirmButtonColor: '#3085d6',
    cancelButtonColor: '#d33',
    confirmButtonText: 'Ya, Aktifkan!',
    cancelButtonText: 'Batal'
    }).then((result) => {
    if (result.value) {
        Swal.fire(
        'Premium!',
        'Akun sudah Premium.',
        'success'
        )
        window.location='<?php echo base_url().'pengepul/ubahpremium/'; ?>'+res;
    }
    });
}

function unblokir(res) {
    Swal.fire({
    title: 'Anda Yakin Mau Unblokir Pengepul?',
    text: "Akun akan segera di Unblokir!",
    icon: 'warning',
    showCancelButton: true,
    confirmButtonColor: '#3085d6',
    cancelButtonColor: '#d33',
    confirmButtonText: 'Ya, Aktifkan!',
    cancelButtonText: 'Batal'
    }).then((result) => {
    if (result.value) {
        Swal.fire(
        'Aktif!',
        'Akun sudah di Unblokir.',
        'success'
        )
        window.location='<?php echo base_url().'pengepul/ubahbanned/'; ?>'+res;
    }
    });
}
</script>