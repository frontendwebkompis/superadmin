<script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>

<div class="row">
	<!-- Left col -->
	<section class="col-lg connectedSortable ui-sortable">
		<!-- Custom tabs (Charts with tabs)-->
		<div class="card card-info">
            <div class="card-header">
                <div class="card-title">
                  <?php echo $title_card?>
                </div>
            </div>
			<div class="card-body">
				<div class="tab-content p-0">

        <div class="row">
            
            <?php $this->load->view('pengepul/menu_atas'); ?>

            <div class="col-md-3 offset-md-4 mb-2">
                <?= search(site_url('pengepul/index'), site_url('pengepul'), $q) ?>
            </div>
        </div>
        <div class="tab-content p-0" style="overflow:auto">
        <table class="table table-bordered table-striped table-condensed table-hover table-sm" style="margin-bottom: 10px">
            <tr>
                <th class="text-center" width="50px">No</th>
                <th class="text-center">Nama</th>
                <th class="text-center">Email</th>
                <th class="text-center">No Hp</th>
                <th class="text-center">Kode Verifikasi</th>
                <th class="text-center">Last Login</th>
                <th class="text-center">Tanggal Join</th>
                <th class="text-center">Status </th>
                <th class="text-center">Aksi</th>
            </tr>
            <?php 
            if($total_rows == 0){
                echo '<tr><td colspan="10" bgcolor="grey" style="color: white; font-weight: bold; text-align: center;">Data Tidak Ditemukan</td></tr>';
            } else {
            foreach ($pengepul_data as $pengepul) { ?>
            <tr>
                <td class="text-center"><?php echo ++$start ?></td>
                <td><?php echo $pengepul->nama_pengepul ?></td>
                <td><?php echo $pengepul->email_pengepul ?></td>
                <td><?php echo $pengepul->no_hp_pengepul ?></td>
                <td class="text-center">
                    <?php if(empty($pengepul->kode_verifikasi_pengepul)){
                        echo '<span class="badge badge-danger">Kosong</span>';
                    } else {
                        echo $pengepul->kode_verifikasi_pengepul;
                    }?>
                </td>
                <td class="text-center">
                    <?php if(empty($pengepul->last_login)){
                        echo '<span class="badge badge-danger">Kosong</span>';
                    } else {
                        echo fulldate($pengepul->last_login);
                    }?>
                </td>
                <td class="text-center"><?php echo fulldate($pengepul->tgl_daftar_pengepul) ?></td>
                <td class="text-center">
                    <?php if($pengepul->ket_status_pengepul == '0'){
                        echo '<span class="badge badge-danger">Non Aktif</span>';
                    } else {
                        echo '<span class="badge badge-primary">Sudah Verifikasi (Premium)</span>';
                    }
                    ?>
                </td>
                <td style="text-align:center" width="100px">
                    <div class="btn-group">
                        <a href="<?php echo site_url('pengepul/detail/'.$pengepul->id_pengepul); ?>"
                        data-toogle="tooltip" title="Lihat Detail">
                        <button type="button" class="btn btn-info btn-sm"><i class="fas fa-info-circle fa-sm"></i></button></a>
                        <a href="<?php echo site_url('pengepul/blokir/'.$pengepul->id_pengepul); ?>"
                        data-toogle="tooltip" title="Lihat Detail">
                        <button type="button" class="btn btn-danger btn-sm"><i class="fas fa-power-off fa-sm"></i></button></a>
                    </div>
                </td>
		    </tr>
            <?php }
                }     
             ?>
        </table>
        </div>
        <?= footer($total_rows, $pagination, site_url('pengepul/exportxl?q=' . $q)) ?>
        </div>
			</div>
		</div>
	</section>
</div>

<script>
function confirm(res) {
    Swal.fire({
    title: 'Anda Yakin Mau Blokir Pengepul?',
    text: "Akun akan segera di Blokir!",
    icon: 'warning',
    showCancelButton: true,
    confirmButtonColor: '#3085d6',
    cancelButtonColor: '#d33',
    confirmButtonText: 'Ya, Blokir!',
    cancelButtonText: 'Batal'
    }).then((result) => {
    if (result.value) {
        Swal.fire(
        'Terblokir!',
        'Akun sudah diblokir.',
        'success'
        )
        window.location='<?php echo base_url().'pengepul/ubahstatus/'; ?>'+res;
    }
    });
}
</script>