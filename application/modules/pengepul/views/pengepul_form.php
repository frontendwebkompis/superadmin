<div class="row">
	<!-- Left col -->
	<section class="col-lg connectedSortable ui-sortable">
		<!-- Custom tabs (Charts with tabs)-->
		<div class="card">
			<div class="card-body">
				<div class="tab-content p-0">

        <form action="<?php echo $action; ?>" method="post">
            <div class="form-group">
                <label for="varchar">Nama Pengepul <?php echo form_error('nama_pengepul') ?></label>
                <input type="text" class="form-control" name="nama_pengepul" id="nama_pengepul" placeholder="Nama Pengepul" value="<?php echo $nama_pengepul; ?>" />
            </div>
            <div class="form-group">
                <label for="varchar">Email Pengepul <?php echo form_error('email_pengepul') ?></label>
                <input type="text" class="form-control" name="email_pengepul" id="email_pengepul" placeholder="Email Pengepul" value="<?php echo $email_pengepul; ?>" />
            </div>
            <div class="form-group">
                <label for="varchar">No Hp Pengepul <?php echo form_error('no_hp_pengepul') ?></label>
                <input type="text" onkeypress="return Angkasaja(event)" class="form-control" name="no_hp_pengepul" id="no_hp_pengepul" placeholder="No Hp Pengepul" value="<?php echo $no_hp_pengepul; ?>" />
            </div>
            <!-- <div class="form-group">
                <label for="varchar">Password Pengepul <?php echo form_error('password_pengepul') ?></label>
                <input type="text" class="form-control" name="password_pengepul" id="password_pengepul" placeholder="Password Pengepul" value="<?php echo $password_pengepul; ?>" />
            </div> -->
            <div class="form-group">
                <label for="varchar">Kode Verifikasi Bank Sampah <?php echo form_error('kode_verifikasi_bank_sampah') ?></label>
                <input type="text" onkeypress="return Angkasaja(event)" class="form-control" name="kode_verifikasi_bank_sampah" id="kode_verifikasi_bank_sampah" placeholder="Kode Verifikasi Bank Sampah" value="<?php echo $kode_verifikasi_bank_sampah; ?>" />
            </div>
            <!-- <div class="form-group">
                <label for="datetime">Last Login <?php echo form_error('last_login') ?></label>
                <input type="text" class="form-control" name="last_login" id="last_login" placeholder="Last Login" value="<?php echo $last_login; ?>" />
            </div>
            <div class="form-group">
                <label for="datetime">Tgl Join <?php echo form_error('tgl_join') ?></label>
                <input type="text" class="form-control" name="tgl_join" id="tgl_join" placeholder="Tgl Join" value="<?php echo $tgl_join; ?>" />
            </div> -->
            <input type="hidden" name="id_pengepul" value="<?php echo $id_pengepul; ?>" /> 
            <button type="submit" class="btn btn-primary">Simpan</button> 
            <a href="<?php echo site_url('pengepul') ?>" class="btn btn-default">Batal</a>
	    </form>

        
			</div><!-- /.card-body -->
		</div>
		<!-- /.card -->
	</section>
	<!-- /.Left col -->
</div>

<script type="text/javascript">
    function Angkasaja(evt) {
    var charCode = (evt.which) ? evt.which : event.keyCode
    if (charCode > 31 && (charCode < 48 || charCode > 57))
    return false;
    return true;
    }
</script>