<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Pengepul_model extends CI_Model
{

    public $table = 'pengepul';
    public $id = 'id_pengepul';
    public $order = 'DESC';

    function __construct()
    {
        parent::__construct();
    }

    function get_all()
    {
        $this->db->order_by($this->id, $this->order);
        return $this->db->get($this->table)->result();
    }

    function get_by_ktp($id)
    {
        $this->db->where('foto_ktp_pengepul', $id);
        return $this->db->get('pengepul_detail')->row();
    }

    function get_by_apsi($id)
    {
        $this->db->where('foto_apsi', $id);
        return $this->db->get('pengepul_detail')->row();
    }

    function get_reason($cek=0)
    {
        if ($cek==1) {
            $this->db->where('id_proses', 1);
        } else {
            $this->db->where('id_proses', 10);
        }
        return $this->db->get('reason')->result();
    }

    function get_by_id($id)
    {
        $this->db->where($this->id, $id);
        return $this->db->get($this->table)->row();
    }

    // get data by id
    function get_by_id_detail($id)
    {
        $query = $this->db->query("SELECT pengepul.id_pengepul, pengepul.id_status_pengepul,pengepul.email_pengepul,status_pengepul.ket_status_pengepul,pengepul.nama_pengepul,pengepul.no_hp_pengepul,pengepul.kode_verifikasi_pengepul,pengepul.last_login,pengepul.tgl_daftar_pengepul,pengepul_detail.alamat_pengepul,provinsi.nama_provinsi,kabupaten.nama_kabupaten,kecamatan.nama_kecamatan,pengepul_detail.* FROM pengepul
        LEFT JOIN status_pengepul ON status_pengepul.id_status_pengepul=pengepul.id_status_pengepul
        RIGHT JOIN pengepul_detail ON pengepul_detail.id_pengepul=pengepul.id_pengepul
        LEFT JOIN provinsi ON provinsi.id_provinsi=pengepul_detail.id_provinsi
        LEFT JOIN kabupaten ON kabupaten.id_provinsi=pengepul_detail.id_kabupaten
        LEFT JOIN kecamatan ON kecamatan.id_kecamatan=pengepul_detail.id_kecamatan
         where pengepul.id_pengepul='" . $id . "'");

        return $query->row();
    }

    // get total rows
    function total_rows($q = NULL)
    {
        $this->db->like($this->table . '.id_pengepul', $q);
        $this->db->where($this->table . '.id_status_pengepul', 3);
        $this->db->group_start();
        $this->db->or_like($this->table . '.nama_pengepul', $q);
        
        $this->db->group_end();    $this->db->join('status_pengepul', 'pengepul.id_status_pengepul=status_pengepul.id_status_pengepul');
        $this->db->from($this->table);
        return $this->db->count_all_results();
    }

    // get data with limit and search
    function get_limit_data($limit, $start = 0, $q = NULL)
    {
        $this->db->order_by($this->id, $this->order);
        $this->db->where($this->table . '.id_status_pengepul', 3);
        $this->db->group_start();
        $this->db->or_like($this->table . '.nama_pengepul', $q);
        
        $this->db->group_end();
        $this->db->limit($limit, $start);
        $this->db->join('status_pengepul', 'pengepul.id_status_pengepul=status_pengepul.id_status_pengepul');
        return $this->db->get($this->table)->result();
    }

    // get total rows
    function total_rows_approve($q = NULL)
    {
        $this->db->where($this->table . '.id_status_pengepul', 0);
        $this->db->group_start();
        $this->db->or_like('nama_pengepul', $q);
        $this->db->group_end();
        $this->db->join('status_pengepul', 'pengepul.id_status_pengepul=status_pengepul.id_status_pengepul');
        $this->db->from($this->table);
        return $this->db->count_all_results();
    }

    // get data with limit and search
    function get_limit_data_approve($limit, $start = 0, $q = NULL)
    {
        $this->db->order_by($this->id, $this->order);
        $this->db->where($this->table . '.id_status_pengepul', 0);
        $this->db->group_start();
        $this->db->or_like('nama_pengepul', $q);
        $this->db->group_end();
        $this->db->limit($limit, $start);
        $this->db->join('status_pengepul', 'pengepul.id_status_pengepul=status_pengepul.id_status_pengepul');
        return $this->db->get($this->table)->result();
    }
    // get total rows
    function total_rows_premium($q = NULL)
    {
        $this->db->where($this->table . '.id_status_pengepul', 2);
        $this->db->group_start();
        $this->db->or_like('nama_pengepul', $q);
        $this->db->group_end();
        $this->db->from($this->table);
        $this->db->join('status_pengepul', 'pengepul.id_status_pengepul=status_pengepul.id_status_pengepul');
        return $this->db->count_all_results();
    }

    // get data with limit and search
    function get_limit_data_premium($limit, $start = 0, $q = NULL)
    {
        $this->db->order_by($this->id, $this->order);
        $this->db->where($this->table . '.id_status_pengepul', 2);
        $this->db->group_start();
        $this->db->or_like('nama_pengepul', $q);
        $this->db->group_end();
         $this->db->limit($limit, $start);
        $this->db->join('status_pengepul', 'pengepul.id_status_pengepul=status_pengepul.id_status_pengepul');
        return $this->db->get($this->table)->result();
    }
    // get total rows
    // get total rows
    function total_rows_banned($q = NULL)
    {
        $this->db->where($this->table . '.id_status_pengepul', 4);
        $this->db->group_start();
        $this->db->or_like('nama_pengepul', $q);
        $this->db->group_end();
        $this->db->from($this->table);
        $this->db->join('status_pengepul', 'pengepul.id_status_pengepul=status_pengepul.id_status_pengepul');
        return $this->db->count_all_results();
    }

    // get data with limit and search
    function get_limit_data_banned($limit, $start = 0, $q = NULL)
    {
        $this->db->order_by($this->id, $this->order);
        $this->db->where($this->table . '.id_status_pengepul', 4);
        $this->db->group_start();
        $this->db->or_like('nama_pengepul', $q);
        $this->db->group_end();
        $this->db->limit($limit, $start);
        $this->db->join('status_pengepul', 'pengepul.id_status_pengepul=status_pengepul.id_status_pengepul');
        return $this->db->get($this->table)->result();
    }
    // get total rows
    function total_rows_berkas($q = NULL)
    {
        $this->db->where($this->table . '.id_status_pengepul', 1);
        $this->db->group_start();
        $this->db->or_like('nama_pengepul', $q);
        $this->db->group_end();
        $this->db->from($this->table);
        $this->db->join('status_pengepul', 'pengepul.id_status_pengepul=status_pengepul.id_status_pengepul');
        return $this->db->count_all_results();
    }

    // get data with limit and search
    function get_limit_data_berkas($limit, $start = 0, $q = NULL)
    {
        $this->db->order_by($this->id, $this->order);
        $this->db->where($this->table . '.id_status_pengepul', 1);
        $this->db->group_start();
        $this->db->or_like('nama_pengepul', $q);
        $this->db->group_end();
        $this->db->limit($limit, $start);
        $this->db->join('status_pengepul', 'pengepul.id_status_pengepul=status_pengepul.id_status_pengepul');
        return $this->db->get($this->table)->result();
    }

    // insert data
    function insertdetailpengepul($id)
    {
        $data = array(
            'id_pengepul' => $id,
        );
        $this->db->insert('pengepul_detail', $data);
    }

    // insert data
    function insert($data)
    {
        $this->db->insert($this->table, $data);
    }

    // update data
    function update($id, $data)
    {
        $this->db->where($this->id, $id);
        $this->db->update($this->table, $data);
    }

    // delete data
    function delete($id)
    {
        $this->db->where($this->id, $id);
        $this->db->delete($this->table);
    }

     public function get_ktp_ori($no_ktp, $nama)
    {
        $dt = new DateTime();
        try{
          // $apiHost ='http://api.shipping.esoftplay.com';
        
         $apiHost ='http://api.binderbyte.com/cekktp';
        $key ='e4f44aaa99629e4947818b511afb35aadc3d70959f2c8b02498e2c6e4f364311';
       $client = new GuzzleHttp\Client(['base_uri' => $apiHost,]);

        $data = $client->request('GET', '?api_key=' . $key.'&nik='.$no_ktp.'&nama='.$nama ,[
              
            'http_errors' => false
        ]);
        // if($data->getStatusCode()==200){
        $datas = $data->getBody();
        $data= json_decode($datas->getContents());
       
          return $data;
    
      
       } catch(RequestException $e) {
            $response_s = $e->getResponse()->getBody();
            return $response_s;
        }

        catch(ClientException $ce){
            $response_s = $e->getResponse()->getBody();
            return $response_s;
}
      
    } 
}

/* End of file Pengepul_model.php */
/* Location: ./application/models/Pengepul_model.php */
/* Please DO NOT modify this information : */
/* Generated by Harviacode Codeigniter CRUD Generator 2020-02-14 04:12:17 */
/* http://harviacode.com */
