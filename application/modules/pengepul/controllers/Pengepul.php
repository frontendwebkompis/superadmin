<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Pengepul extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        if (cek_token()) {
            $this->load->model('Pengepul_model');
            $this->load->library('form_validation');
        } else {
            logout();
        }
    }

    public function index()
    {
        $q = urldecode($this->input->get('q', TRUE));
        $start = intval($this->input->get('start'));
        
        if ($q <> '') {
            $config['base_url'] = base_url() . 'pengepul?q=' . urlencode($q);
            $config['first_url'] = base_url() . 'pengepul?q=' . urlencode($q);
        } else {
            $config['base_url'] = base_url() . 'pengepul';
            $config['first_url'] = base_url() . 'pengepul';
        }

        $config['per_page']             = 10;
        $config['page_query_string']    = TRUE;
        $config['total_rows']           = $this->Pengepul_model->total_rows($q);
        $pengepul = $this->Pengepul_model->get_limit_data($config['per_page'], $start, $q);

        $this->load->library('pagination');
        $this->pagination->initialize($config);

        $data = array(
            'tittle'            => 'Pengepul',
            'judul'             => 'Pengepul',
            'title_card'        => 'List Pengepul',
            'tombol_aktif'      => 'pengepul',
            'pengepul_data'     => $pengepul,
            'q'                 => $q,
            'pagination'        => $this->pagination->create_links(),
            'total_rows'        => $config['total_rows'],
            'start'             => $start,
            'pengepul_aktif'    => 'pengepul',
            'menu_aktif'        => 'pengepul'
        );
        $res['datakonten'] = $this->load->view('pengepul/pengepul_list', $data,true);
        $this->load->view('layouts/main_view', $res);
    }


    public function approve()
    {
        $q = urldecode($this->input->get('q', TRUE));
        $start = intval($this->input->get('start'));
        
        if ($q <> '') {
            $config['base_url'] = base_url() . 'pengepul/approve?q=' . urlencode($q);
            $config['first_url'] = base_url() . 'pengepul/approve?q=' . urlencode($q);
        } else {
            $config['base_url'] = base_url() . 'pengepul/approve';
            $config['first_url'] = base_url() . 'pengepul/approve';
        }

        $config['per_page'] = 10;
        $config['page_query_string'] = TRUE;
        $config['total_rows'] = $this->Pengepul_model->total_rows_approve($q);
        $pengepul = $this->Pengepul_model->get_limit_data_approve($config['per_page'], $start, $q);

        $this->load->library('pagination');
        $this->pagination->initialize($config);

        $data = array(
            'tittle'            => 'Pengepul',
            'judul'             => 'Pengepul',
            'title_card'        => 'List Pengepul Approve',
            'tombol_aktif'      => 'approve',
            'pengepul_data'     => $pengepul,
            'q'                 => $q,
            'pagination'        => $this->pagination->create_links(),
            'total_rows'        => $config['total_rows'],
            'start'             => $start,
            'pengepul_aktif'    => 'pengepul',
            'menu_aktif'        => 'pengepul'
        );
        $res['datakonten'] = $this->load->view('pengepul/pengepul_list_approve', $data,true);
        $this->load->view('layouts/main_view', $res);
    }
    
    public function premium()
    {
        $q = urldecode($this->input->get('q', TRUE));
        $start = intval($this->input->get('start'));
        
        if ($q <> '') {
            $config['base_url'] = base_url() . 'pengepul/premium?q=' . urlencode($q);
            $config['first_url'] = base_url() . 'pengepul/premium?q=' . urlencode($q);
        } else {
            $config['base_url'] = base_url() . 'pengepul/premium';
            $config['first_url'] = base_url() . 'pengepul/premium';
        }

        $config['per_page'] = 10;
        $config['page_query_string'] = TRUE;
        $config['total_rows'] = $this->Pengepul_model->total_rows_premium($q);
        $pengepul = $this->Pengepul_model->get_limit_data_premium($config['per_page'], $start, $q);

        $this->load->library('pagination');
        $this->pagination->initialize($config);

        $data = array(
            'tittle'            => 'Pengepul',
            'judul'             => 'Pengepul',
            'title_card'        => 'List Pengepul Premium',
            'tombol_aktif'      => 'premium',
            'pengepul_data'     => $pengepul,
            'q'                 => $q,
            'pagination'        => $this->pagination->create_links(),
            'total_rows'        => $config['total_rows'],
            'start'             => $start,
            'pengepul_aktif'    => 'pengepul',
            'menu_aktif'        => 'pengepul'
        );
        $res['datakonten'] = $this->load->view('pengepul/pengepul_list_premium', $data,true);
        $this->load->view('layouts/main_view', $res);
    }

    public function berkas()
    {
        $q = urldecode($this->input->get('q', TRUE));
        $start = intval($this->input->get('start'));
        
        if ($q <> '') {
            $config['base_url'] = base_url() . 'pengepul/berkas?q=' . urlencode($q);
            $config['first_url'] = base_url() . 'pengepul/berkas?q=' . urlencode($q);
        } else {
            $config['base_url'] = base_url() . 'pengepul/berkas';
            $config['first_url'] = base_url() . 'pengepul/berkas';
        }

        $config['per_page'] = 10;
        $config['page_query_string'] = TRUE;
        $config['total_rows'] = $this->Pengepul_model->total_rows_berkas($q);
        $pengepul = $this->Pengepul_model->get_limit_data_berkas($config['per_page'], $start, $q);

        $this->load->library('pagination');
        $this->pagination->initialize($config);

        $data = array(
            'tittle'            => 'Pengepul',
            'judul'             => 'Pengepul',
            'title_card'        => 'List Pengepul Berkas',
            'tombol_aktif'      => 'berkas',
            'pengepul_data'     => $pengepul,
            'q'                 => $q,
            'pagination'        => $this->pagination->create_links(),
            'total_rows'        => $config['total_rows'],
            'start'             => $start,
            'pengepul_aktif'    => 'pengepul',
            'menu_aktif'        => 'pengepul'
        );
        $res['datakonten'] = $this->load->view('pengepul/pengepul_list_berkas', $data,true);
        $this->load->view('layouts/main_view', $res);
    }

    public function banned()
    {
        $q = urldecode($this->input->get('q', TRUE));
        $start = intval($this->input->get('start'));
        
        if ($q <> '') {
            $config['base_url'] = base_url() . 'pengepul/banned?q=' . urlencode($q);
            $config['first_url'] = base_url() . 'pengepul/banned?q=' . urlencode($q);
        } else {
            $config['base_url'] = base_url() . 'pengepul/banned';
            $config['first_url'] = base_url() . 'pengepul/banned';
        }

        $config['per_page'] = 10;
        $config['page_query_string'] = TRUE;
        $config['total_rows'] = $this->Pengepul_model->total_rows_banned($q);
        $pengepul = $this->Pengepul_model->get_limit_data_banned($config['per_page'], $start, $q);

        $this->load->library('pagination');
        $this->pagination->initialize($config);

        $data = array(
            'tittle'            => 'Pengepul',
            'judul'             => 'Pengepul',
            'title_card'        => 'List Pengepul Banned',
            'tombol_aktif'      => 'banned',
            'pengepul_data'     => $pengepul,
            'q'                 => $q,
            'pagination'        => $this->pagination->create_links(),
            'total_rows'        => $config['total_rows'],
            'start'             => $start,
            'pengepul_aktif'    => 'pengepul',
            'menu_aktif'        => 'pengepul'
        );
        $res['datakonten'] = $this->load->view('pengepul/pengepul_list_banned', $data,true);
        $this->load->view('layouts/main_view', $res);
    }

    public function detail($id) 
    {
        $row = $this->Pengepul_model->get_by_id_detail($id);
           $reason = $this->Pengepul_model->get_reason(1);
        if ($row) {
            $data = array(
            'tittle'                    => 'Pengepul',
            'judul'                     => 'Pengepul',
            'title_card'                => 'Detail Pengepul',
            'id_pengepul'               => $row->id_pengepul,
            'reason'                    => $reason,
            'id_status_pengepul'        => $row->id_status_pengepul,
            'nama_pengepul'             => $row->nama_pengepul,
            'email_pengepul'            => $row->email_pengepul,
            'no_hp_pengepul'            => $row->no_hp_pengepul,
            'kode_verifikasi_pengepul'  => $row->kode_verifikasi_pengepul,
            'last_login'                => $row->last_login,
            'tgl_daftar_pengepul'       => $row->tgl_daftar_pengepul,
            'alamat_pengepul'           => $row->alamat_pengepul,
            'nama_provinsi'             => $row->nama_provinsi,
            'nama_kabupaten'            => $row->nama_kabupaten,
            'nama_kecamatan'            => $row->nama_kecamatan,
            'lat_pengepul'              => $row->lat_pengepul,
            'long_pengepul'             => $row->long_pengepul,
            'no_ktp_pengepul'           => $row->no_ktp_pengepul,
            'foto_ktp_pengepul'         => $row->foto_ktp_pengepul,
            'foto_ktp_selfie_pengepul'  => $row->foto_ktp_selfie_pengepul,
            'saldo_pengepul'            => $row->saldo_pengepul,
            'foto_apsi'                 => $row->foto_apsi,
            'pengepul_aktif'            => '1',
            'menu_aktif'                => 'pengepul'
            );
            $res['datakonten'] = $this->load->view('pengepul/pengepul_detail', $data,true);
            $this->load->view('layouts/main_view', $res);
        } else {
            $this->session->set_flashdata('gagal', 'Data Tidak Di Temukan');
            redirect(site_url('pengepul'));
        }
    }

    public function blokir($id)
    {
         $row = $this->Pengepul_model->get_by_id_detail($id);
         $reason = $this->Pengepul_model->get_reason(0);
        if ($row) {
        $data = array(
            'tittle'                => 'Pengepul',
            'judul'                 => 'Pengepul Blokir Detail',
            'id_pengepul'           => $row->id_pengepul,
            'nama_pengepul'         => $row->nama_pengepul,
            'reason'                => $reason,
            'status_pengepul'       => $row->ket_status_pengepul,
            'email_pengepul'        => $row->email_pengepul,
            'no_hp_pengepul'        => $row->no_hp_pengepul,
            'last_login'            => $row->last_login,
            'tgl_daftar_pengepul'   => $row->tgl_daftar_pengepul,
            'alamat_pengepul'       => $row->alamat_pengepul,
            'menu_aktif'            => 'pengepul'
        );
        $res['datakonten'] = $this->load->view('pengepul/pengepul_blokir', $data,true);
        $this->load->view('layouts/main_view', $res);
         } else {
            $this->session->set_flashdata('gagal', 'Data Tidak Di Temukan');
            redirect(site_url('pengepul'));
        }
    }

    public function create() 
    {
        $data = array(
            'tittle'                        => 'Pengepul',
            'judul'                         => 'Tambah Pengepul',
            'button'                        => 'Create',
            'action'                        => site_url('pengepul/create_action'),
            'id_pengepul'                   => set_value('id_pengepul'),
            'nama_pengepul'                 => set_value('nama_pengepul'),
            'email_pengepul'                => set_value('email_pengepul'),
            'no_hp_pengepul'                => set_value('no_hp_pengepul'),
            'password_pengepul'             => set_value('password_pengepul'),
            'kode_verifikasi_bank_sampah'   => set_value('kode_verifikasi_bank_sampah'),
            'last_login'                    => set_value('last_login'),
            'tgl_join'                      => set_value('tgl_join'),
            'menu_aktif'                    => 'pengepul'
	    );
        $res['datakonten'] = $this->load->view('pengepul/pengepul_form', $data,true);
        $this->load->view('layouts/main_view', $res);

    }
    
    public function create_action() 
    {
        $this->_rules();
        $dt = new DateTime();
        if ($this->form_validation->run() == FALSE) {
            $this->create();
        } else {
            $data = array(
                'nama_pengepul'                 => $this->input->post('nama_pengepul',TRUE),
                'email_pengepul'                => $this->input->post('email_pengepul',TRUE),
                'no_hp_pengepul'                => $this->input->post('no_hp_pengepul',TRUE),
                'password_pengepul'             => $this->input->post('password_pengepul',TRUE),
                'kode_verifikasi_bank_sampah'   => $this->input->post('kode_verifikasi_bank_sampah',TRUE),
                'last_login'                    => $dt->format('Y-m-d H:i:s'),
                'tgl_join'                      => $dt->format('Y-m-d H:i:s'),
            );

            $this->Pengepul_model->insert($data);
            $this->session->set_flashdata('berhasil', 'Berhasil Menambahkan Data');
            redirect(site_url('pengepul/approve'));
        }
    }
    
    public function update($id) 
    {
        $row = $this->Pengepul_model->get_by_id($id);

        if ($row) {
            $data = array(
                'button'                        => 'Update',
                'action'                        => site_url('pengepul/update_action'),
                'id_pengepul'                   => set_value('id_pengepul', $row->id_pengepul),
                'nama_pengepul'                 => set_value('nama_pengepul', $row->nama_pengepul),
                'email_pengepul'                => set_value('email_pengepul', $row->email_pengepul),
                'no_hp_pengepul'                => set_value('no_hp_pengepul', $row->no_hp_pengepul),
                'password_pengepul'             => set_value('password_pengepul', $row->password_pengepul),
                'kode_verifikasi_bank_sampah'   => set_value('kode_verifikasi_bank_sampah', $row->kode_verifikasi_bank_sampah),
                'last_login'                    => set_value('last_login', $row->last_login),
                'tgl_join'                      => set_value('tgl_join', $row->tgl_join),
            );
            $this->load->view('pengepul/pengepul_form', $data);
        } else {
            $this->session->set_flashdata('gagal', 'Data Tidak Ditemukan');
            redirect(site_url('pengepul'));
        }
    }
    
    public function update_action() 
    {
        $this->_rules();
        $dt = new DateTime();
        if ($this->form_validation->run() == FALSE) {
            $this->update($this->input->post('id_pengepul', TRUE));
        } else {
            $data = array(
            'nama_pengepul'                 => $this->input->post('nama_pengepul',TRUE),
            'email_pengepul'                => $this->input->post('email_pengepul',TRUE),
            'no_hp_pengepul'                => $this->input->post('no_hp_pengepul',TRUE),
            'password_pengepul'             => $this->input->post('password_pengepul',TRUE),
            'kode_verifikasi_bank_sampah'   => $this->input->post('kode_verifikasi_bank_sampah',TRUE),
            'last_login'                    => $dt->format('Y-m-d H:i:s'),
            'tgl_join'                      => $dt->format('Y-m-d H:i:s'),
            );

            $this->Pengepul_model->update($this->input->post('id_pengepul', TRUE), $data);
            $this->session->set_flashdata('berhasil', 'Berhasil Merubah Data');
            redirect(site_url('pengepul'));
        }
    }
    
    public function delete($id) 
    {
        $row = $this->Pengepul_model->get_by_id($id);

        if ($row) {
            $this->Pengepul_model->delete($id);
            $this->session->set_flashdata('berhasil', 'Berhasil Menghapus Data');
            redirect(site_url('pengepul'));
        } else {
            $this->session->set_flashdata('gagal', 'Data Tidak Ditemukan');
            redirect(site_url('pengepul'));
        }
    }

    public function _rules() 
    {
	$this->form_validation->set_rules('nama_pengepul', 'nama pengepul', 'trim|required');
	$this->form_validation->set_rules('email_pengepul', 'email pengepul', 'trim|required');
	$this->form_validation->set_rules('no_hp_pengepul', 'no hp pengepul', 'trim|required');
	// $this->form_validation->set_rules('password_pengepul', 'password pengepul', 'trim|required');
	$this->form_validation->set_rules('kode_verifikasi_bank_sampah', 'kode verifikasi bank sampah', 'trim|required');
	// $this->form_validation->set_rules('id_pengepul', 'id_pengepul', 'trim');
	$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }


    
    public function ubahbanned($id) 
    {
        $row = $this->Pengepul_model->get_by_id($id);
         if ($row) {
      
            $data = array(
             
                'id_status_pengepul' => '1',
        );
        
        $this->Pengepul_model->update($id, $data);
        $this->session->set_flashdata('berhasil', 'Berhasil Merubah Data');
        redirect(site_url('pengepul/berkas'));
        } else {
        $this->session->set_flashdata('gagal', 'Data Tidak Ditemukan');
        redirect(site_url('pengepul//berkas'));
        }
    }

    public function ubahstatus($id) 
    {
        $row = $this->Pengepul_model->get_by_id($id);
         $reason          = $this->input->get('reason');
      
        if ($row) {
      
            $data = array(
                'id_status_pengepul' => '4',
                   'reason_reject' => $reason,
        );
        
        $this->Pengepul_model->update($id, $data);
        $this->session->set_flashdata('berhasil', 'Berhasil Merubah Data');
        redirect(site_url('pengepul/banned'));
        } else {
        $this->session->set_flashdata('gagal', 'Data Tidak Ditemukan');
        redirect(site_url('pengepul/banned'));
        }
    }

    public function ubahapprove($id) 
    {
        $row = $this->Pengepul_model->get_by_id($id);
        
        if ($row) {
            $data = array(
                'id_status_pengepul' => '1',
        );
        $this->Pengepul_model->update($id, $data);
        $this->Pengepul_model->insertdetailpengepul($id);
        $this->session->set_flashdata('berhasil', 'Berhasil Merubah Data');
        redirect(site_url('pengepul/approve'));
   } else {
            $this->session->set_flashdata('gagal', 'Data Tidak Ditemukan');
            redirect(site_url('pengepul/approve'));
        }
    }

    public function ubahpremium($id) 
    {
        $row = $this->Pengepul_model->get_by_id($id);
        
        if ($row) {
            $data = array(
                'id_status_pengepul' => '3',
        );
        $this->Pengepul_model->update($id, $data);
        $this->session->set_flashdata('berhasil', 'Berhasil Merubah Data');
        redirect(site_url('pengepul'));
   } else {
            $this->session->set_flashdata('gagal', 'Data Tidak Ditemukan');
            redirect(site_url('pengepul'));
        }
    }


    public function exportxl()
    {
        $q          = $this->input->get('q');
        $subject    = "Pengepul_List";
        // $id         = $this->session->userdata('uid');
           $this->db->query('SET @no=0');
        $sql        =
        "SELECT @no:=@no+1 AS nomor,
            pengepul.nama_pengepul,
            pengepul.email_pengepul,
            pengepul.no_hp_pengepul,
            pengepul.kode_verifikasi_pengepul,
            pengepul.last_login,pengepul.tgl_daftar_pengepul 
        FROM `pengepul` 
        JOIN `status_pengepul` 
        ON `pengepul`.`id_status_pengepul`=`status_pengepul`.`id_status_pengepul` 
        WHERE `pengepul`.`id_status_pengepul` = 3";
        exportSQL('Pengepul_List', $subject, $sql);
    }

    public function exportxlbanned()
    {
        $q          = $this->input->get('q');
        $subject    = "Pengepul_Banned";
        // $id         = $this->session->userdata('uid');
           $this->db->query('SET @no=0');
        $sql        =
        "SELECT 
            @no:=@no+1 AS nomor,
            pengepul.nama_pengepul,
            pengepul.email_pengepul,
            pengepul.no_hp_pengepul,
            pengepul.kode_verifikasi_pengepul,
            pengepul.last_login,pengepul.tgl_daftar_pengepul 
        FROM `pengepul` 
        JOIN `status_pengepul` ON `pengepul`.`id_status_pengepul`=`status_pengepul`.`id_status_pengepul` 
        WHERE `pengepul`.`id_status_pengepul` = 4";
        exportSQL('Pengepul_Banned', $subject, $sql);
    }

    public function exportxlberkas()
    {
        $q          = $this->input->get('q');
        $subject    = "Pengepul_Berkas";
        // $id         = $this->session->userdata('uid');
           $this->db->query('SET @no=0');
        $sql        ="SELECT 
            @no:=@no+1 AS nomor,
            pengepul.nama_pengepul,
            pengepul.email_pengepul,
            pengepul.no_hp_pengepul,
            pengepul.kode_verifikasi_pengepul,
            pengepul.last_login,pengepul.tgl_daftar_pengepul 
        FROM `pengepul` 
        JOIN `status_pengepul` ON `pengepul`.`id_status_pengepul`=`status_pengepul`.`id_status_pengepul` 
        WHERE `pengepul`.`id_status_pengepul` = 1";
        exportSQL('Pengepul_Berkas', $subject, $sql);
    }

    public function exportxlapprove()
    {
        $q          = $this->input->get('q');
        $subject    = "Pengepul_Approve";
        // $id         = $this->session->userdata('uid');
           $this->db->query('SET @no=0');
        $sql        ="SELECT 
            @no:=@no+1 AS nomor,
            pengepul.nama_pengepul,
            pengepul.email_pengepul,
            pengepul.no_hp_pengepul,
            pengepul.kode_verifikasi_pengepul,
            pengepul.last_login,pengepul.tgl_daftar_pengepul 
        FROM `pengepul` 
        JOIN `status_pengepul` ON `pengepul`.`id_status_pengepul`=`status_pengepul`.`id_status_pengepul` 
        WHERE `pengepul`.`id_status_pengepul` = 0";
        exportSQL('Pengepul_Approve', $subject, $sql);
    }

    public function exportxlpremium()
    {
        $q          = $this->input->get('q');
        $subject    = "Pengepul_Premium";
        // $id         = $this->session->userdata('uid');
           $this->db->query('SET @no=0');
        $sql        ="SELECT 
            @no:=@no+1 AS nomor,
            pengepul.nama_pengepul,
            pengepul.email_pengepul,
            pengepul.no_hp_pengepul,
            pengepul.kode_verifikasi_pengepul,
            pengepul.last_login,pengepul.tgl_daftar_pengepul 
        FROM `pengepul` 
        JOIN `status_pengepul` ON `pengepul`.`id_status_pengepul`=`status_pengepul`.`id_status_pengepul` 
        WHERE `pengepul`.`id_status_pengepul` = 2";
        exportSQL('Pengepul_Premium', $subject, $sql);
    }

  public function tolak_berkas()
        {
           $id=$this->input->post('id_pengepul');
           $reason=$this->input->post('reason');

             $data = array(
            'id_status_pengepul'    => 1,
            'reason_reject'     => $reason,
        );
    $this->Pengepul_model->update($id, $data);
            # code...
    redirect('pengepul/detail/'.$id);
        }

    function get_image(){
        $img = $this->input->get('img');
        echo getkontenall($img,'pengepul'); 
    }
 
    function get_by_ktp(){
        $img=$this->input->get('img');
        // $cek='https://kompis-pengepul.s3.ap-southeast-1.amazonaws.com/'.$img;
        // $row = $this->Pengepul_model->get_by_ktp($cek);
        // if ($row) {
            echo getkontenall($img,'pengepul');  
        // } else {
        //     $this->session->set_flashdata('gagal', 'Gambar Tidak Ditemukan');
        //     redirect('pengepul');
        // }
    }

    function get_by_apsi(){
        $img=$this->input->get('img');
        // $cek='https://kompis-pengepul.s3.ap-southeast-1.amazonaws.com/'.$img;
        // $row = $this->Pengepul_model->get_by_apsi($cek);
        // if ($row) {
            echo getkontenall($img,'pengepul');  
        // } else {
        //     $this->session->set_flashdata('gagal', 'Gambar Tidak Ditemukan');
        //     redirect('pengepul');
        // }
    }

     public function cek_ktp_asli()
    {
          $no_ktp=$this->input->get('ktp');
          $nama=$this->input->get('nama');
     
      $data= $this->Pengepul_model->get_ktp_ori($no_ktp,$nama);

         if($data){
        
            echo json_encode($data,200);
        } else{
         
            echo json_encode(['status'=>false,'message'=>'Kombinasi NIK atau Nama tidak ditemnukan'],400);
        }

    }

}

/* End of file Pengepul.php */
/* Location: ./application/controllers/Pengepul.php */
