<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Biller_nasabah extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
		if (cek_token()) {
			$this->load->model('Biller_nasabah_model');
			$this->load->library('form_validation');
			$this->load->library('upload');
		} else {
			logout();
		}
	}

	public function index()
	{
		$model = $this->Biller_nasabah_model;
		$q = urldecode($this->input->get('q', TRUE));
		$start = intval($this->input->get('start'));
    	$awal   = $this->input->get('awal') ? $this->input->get('awal') : date('y').'-01-01';
        $akhir  = $this->input->get('akhir') ? $this->input->get('akhir') : date('y-m-d');
  
		if ($q <> '') {
			$config['base_url'] = base_url() . 'biller_nasabah/?q=' . urlencode($q);
			$config['first_url'] = base_url() . 'biller_nasabah/?q=' . urlencode($q);
		} else {
			$config['base_url'] = base_url() . 'biller_nasabah/';
			$config['first_url'] = base_url() . 'biller_nasabah/';
		}

		$config['per_page'] = 10;
		$config['page_query_string'] = TRUE;
		$config['total_rows'] = $model->total_rows($q,$awal,$akhir);
		$cek = $model->get_limit_data($config['per_page'], $start, $q,$awal,$akhir);

		$this->load->library('pagination');
		$this->pagination->initialize($config);

		$data = array(
			'tittle'        => 'Biller Nasabah',
			'judul'         => 'Biller Nasabah',
			'title_card'    => 'Menu Biller Nasabah',
			'biller_nasabah' => $cek,
			'q'             => $q,
			'pagination'    => $this->pagination->create_links(),
			'total_rows'    => $config['total_rows'],
			'start'         => $start,
			'menu_aktif'	=> 'biller_nasabah',
		);
		$res['datakonten'] = $this->load->view('biller_nasabah/biller_nasabah_list', $data, true);
		$this->load->view('layouts/main_view', $res);
	}
}

/* End of file Biller_nasabah.php */
