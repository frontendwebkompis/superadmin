<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Biller_nasabah_model extends CI_Model
{
	public $table = 'biller_nasabah';
	function total_rows($q = NULL,$awal,$akhir)
	{
		$this->db->join('nasabah_online', "$this->table.id_nasabah_online = nasabah_online.id_nasabah_online");
		 $this->db->where("(UNIX_TIMESTAMP(". $this->table .".datetime) BETWEEN UNIX_TIMESTAMP('" . $awal . "') AND UNIX_TIMESTAMP('" . $akhir  . " 23:59') ) ");
		$this->db->group_start();
		$this->db->or_like("nama_nasabah_online", $q);
		$this->db->or_like("$this->table.id_layanan_tarik_saldo_online", $q);
		$this->db->or_like("$this->table.nama_layanan_tarik_saldo_online", $q);
		$this->db->or_like("$this->table.nomor_pelayanan", $q);
		$this->db->or_like("$this->table.amount", $q);
		$this->db->group_end();

		$this->db->from($this->table);
		return $this->db->count_all_results();
	}

	// get data with limit and search
	function get_limit_data($limit, $start = 0, $q = NULL,$awal,$akhir)
	{
		$this->db->order_by('datetime', 'desc');
		$this->db->join('nasabah_online', "$this->table.id_nasabah_online = nasabah_online.id_nasabah_online");
		$this->db->where("(UNIX_TIMESTAMP(". $this->table .".datetime) BETWEEN UNIX_TIMESTAMP('" . $awal . "') AND UNIX_TIMESTAMP('" . $akhir  . " 23:59') ) ");
		$this->db->group_start();
		$this->db->or_like("nama_nasabah_online", $q);
		$this->db->or_like("$this->table.id_layanan_tarik_saldo_online", $q);
		$this->db->or_like("$this->table.nama_layanan_tarik_saldo_online", $q);
		$this->db->or_like("$this->table.nomor_pelayanan", $q);
		$this->db->or_like("$this->table.amount", $q);
		$this->db->group_end();
		$this->db->limit($limit, $start);
		return $this->db->get($this->table)->result();
	}
}

/* End of file Biller_nasabah_model.php */
