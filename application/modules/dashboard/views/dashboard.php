<div class="row">
	<div class="col-lg-3 col-6">
		<!-- small box -->
		<div class="small-box bg-info">
			<div class="inner">
				<h3><?php echo $tot_pengepul; ?></h3>
				<p>Pengepul</p>
			</div>
			<div class="icon">
				<i class="far fa-user fa-3x"></i>
			</div>
			<a href="<?php echo base_url('pengepul/premium'); ?>" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
		</div>
	</div>

	<!-- ./col -->
	<div class="col-lg-3 col-6">
		<!-- small box -->
		<div class="small-box bg-maroon">
			<div class="inner">
				<h3><?php echo $tot_nasabah; ?></h3>
				<p>Nasabah</p>
			</div>
			<div class="icon">
				<i class="fas fa-user-ninja"></i>
			</div>
			<a href="<?php echo base_url('nasabah_online/premium'); ?>" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
		</div>
	</div>
	<!-- ./col -->
	<div class="col-lg-3 col-6">
		<!-- small box -->
		<div class="small-box bg-success">
			<div class="inner">
				<h3><?php echo $tot_bank_sampah; ?></h3>
				<p>Bank Sampah</p>
			</div>
			<div class="icon">
				<i class="fas fa-dumpster"></i>
			</div>
			<a href="<?php echo base_url('bank_sampah/premium'); ?>" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
		</div>
	</div>
	 <?php if(sekolahMenuStatus()){ ?>
	<div class="col-lg-3 col-6">
		<!-- small box -->
		<div class="small-box bg-purple">
			<div class="inner">
				<h3><?php echo $tot_sekolah; ?></h3>
				<p>Sekolah</p>
			</div>
			<div class="icon">
				<i class="fas fa-school"></i>
			</div>
			<a href="<?php echo base_url('bank_sampah/premium'); ?>" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
		</div>
	</div>
<?php } ?>
	<!-- ./col -->
	<div class="col-lg-3 col-6 ">
		<!-- small box -->
		<div class="small-box bg-navy">
			<div class="inner">
				<h3><?php echo $tot_brand; ?></h3>
				<p>Brand</p>
			</div>
			<div class="icon">
				<i class="fas fa-tag"></i>
			</div>
			<a href="<?php echo base_url('brand'); ?>" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
		</div>
	</div>
	<div class="col-lg-3 col-6">
		<!-- small box -->
		<div class="small-box bg-primary">
			<div class="inner">
				<h3><?php echo $tot_kategori; ?></h3>
				<p>Kategori Sampah</p>
			</div>
			<div class="icon">
				<i class="fas fa-trash"></i>
			</div>
			<a href="<?php echo base_url('kategori_sampah'); ?>" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
		</div>
	</div>
	<div class="col-lg-3 col-6">
		<!-- small box -->
		<div class="small-box bg-secondary">
			<div class="inner">
				<h3><?php echo $tot_jenis; ?></h3>
				<p>Jenis Sampah</p>
			</div>
			<div class="icon">
				<i class="fas fa-trash-restore"></i>
			</div>
			<a href="<?php echo base_url('jenis_sampah'); ?>" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
		</div>
	</div>
	<div class="col-lg-3 col-6">
		<!-- small box -->
		<div class="small-box bg-danger">
			<div class="inner">
				<h3><?php echo $tot_merk; ?></h3>
				<p>Merk Sampah</p>
			</div>
			<div class="icon">
				<i class="fas fa-recycle"></i>
			</div>
			<a href="<?php echo base_url('merk_sampah'); ?>" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
		</div>
	</div>
	<!-- <div class="col-lg-3 col-6"> -->
	<!-- small box -->
	<!-- <div class="small-box bg-light">
			<div class="inner">
				<h3>10</h3>
				<p>Smartbin</p>
			</div>
			<div class="icon">
				<i class="far fa-trash-alt"></i>
			</div>
			<a href="<?php echo base_url('smartbin'); ?>" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
		</div> -->
	<!-- </div> -->
</div>

<center>
  <div class="btn-group mb-2" data-toggle="tooltip" title="Lihat Berdasarkan Tahun" style="width: 100%;">
    <?php
    $thn                       = $this->input->get('tahun') ? $this->input->get('tahun') : '2020';
    $tahun = date("Y");
    $i = $tahun - 4;
    for ($i; $i <= $tahun; $i++) {

    ?>

      <a href="<?= base_url('dashboard?tahun=' . $i) ?>" class="<?= ($thn == $i) ? 'btn btn-primary' : 'btn btn-secondary' ?>">
        <?= ($i == date('Y')) ? $i : $i ?></a>&nbsp;
    <?php  }
    ?>
  </div>
</center>

<div class="content">
	<div class="container-fluid">
<div class="row">
	          <!-- /.col -->
          <div class="col-md-12">
            <div class="card">
              <div class="card-header p-2">
                <ul class="nav nav-pills">
                  <li class="nav-item"><a class="nav-link active" href="#umum" data-toggle="tab">Umum</a></li>
                 <?php if(sekolahMenuStatus()){ ?> <li class="nav-item"><a class="nav-link" href="#sekolah" data-toggle="tab">Sekolah</a></li> <?php } ?>
                </ul>
              </div><!-- /.card-header -->
              <div class="card-body bg-light" >
                <div class="tab-content">
                  <div class="active tab-pane" id="umum">
                    <div class="post">

                    	<div class="container-fluid">
	                    	<div class="row">
	                    		<div class="col-md-8">

									<div class="card card-info" id="pie1">
										<div class="card-header">
										<h3 class="card-title">Kategori Sampah Terlaris(Bank Sampah)</h3>

										<div class="card-tools">
											<button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i>
											</button>
											<button type="button" class="btn btn-tool" data-card-widget="remove"><i class="fas fa-times"></i></button>
										</div>
										</div>
										<div class="card-body">
										<div class="chart">
											<canvas id="sampah" width="600" height="400"></canvas>
										</div>
										</div>
										
									</div>

	                    		</div>
	                    		<div class="col-md-4">

									<div class="card card-info" id="table1">
										<div class="card-header">
											<h3 class="card-title">Nasabah Saldo Terbanyak</h3>

											<div class="card-tools">
												<button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i>
												</button>
												<button type="button" class="btn btn-tool" data-card-widget="remove"><i class="fas fa-times"></i></button>
											</div>
										</div>
										<div class="card-body">
										<table class="table table-bordered table-striped table-condensed">
										<tr>
							                <th class="text-center">Nama Nasabah</th>
							                <th class="text-center">Total Saldo</th>
							            </tr>
							            <?php  foreach ($saldo_terbanyak as $data) { ?>
							            	<tr>
							                <th><?= $data->nama_nasabah_online?></th>
							                <th><?= rupiah($data->saldo)?></th>
							            </tr>
										<?php } ?>
										</table>
										</div>
									</div>

	                    		</div>
	                    	</div>

                    	</div>


                    </div>
                  </div>
                  <div class="tab-pane" id="sekolah">

                  	<div class="container-fluid">
                  		<div class="row">
                  			<div class="col-md-7">
								<div class="card card-info" id="pie2">
									<div class="card-header">
									<h3 class="card-title">Kategori Sampah Terlaris(Sekolah)</h3>

									<div class="card-tools">
										<button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i>
										</button>
										<button type="button" class="btn btn-tool" data-card-widget="remove"><i class="fas fa-times"></i></button>
									</div>
									</div>
									<div class="card-body">
									<div class="chart">
										<canvas id="sampahsekolah" width="600" height="400"></canvas>
									</div>
									</div>
								</div>
                  			</div>
                  			<div class="col-md-5">

								<div class="card card-info" id="table2">
									<div class="card-header">
										<h3 class="card-title">Sekolah Nabung Terbanyak</h3>

										<div class="card-tools">
											<button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i>
											</button>
											<button type="button" class="btn btn-tool" data-card-widget="remove"><i class="fas fa-times"></i></button>
										</div>
									</div>
									<div class="card-body">
									<table class="table table-bordered table-striped table-condensed">
									<tr>
						                <th class="text-center">Nama Sekolah</th>
						                <th class="text-center">Total Sampah</th>
						            </tr>
						            <?php  foreach ($sekolah_terbanyak as $data) { ?>
						            	<tr>
						                <th><?= $data->nama_sekolah?></th>
						                <th><?= $data->total?> KG</th>
						            </tr>
									<?php } ?>
									</table>
									</div>
								</div>

                  			</div>
                  		</div>

                  	</div>

                  </div>
                </div>
                <!-- /.tab-content -->
              </div><!-- /.card-body -->
            </div>
            <!-- /.nav-tabs-custom -->
          </div>
          <!-- /.col -->
</div>
</div>
</div>

				<div class="content">
					<div class="container-fluid">

                  		<div class="row">
                  			<div class="col-md-12">
								<div class="card card-info" id="table3">
									<div class="card-header">
										<h3 class="card-title">Pembelian Pengepul Terbanyak</h3>

										<div class="card-tools">
											<button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i>
											</button>
											<button type="button" class="btn btn-tool" data-card-widget="remove"><i class="fas fa-times"></i></button>
										</div>
									</div>
									<div class="card-body">
									<table class="table table-bordered table-striped table-condensed">
									<tr>
						                <th class="text-center">Nama Pengepul</th>
						               <?php if(sekolahMenuStatus()){ ?>  <th class="text-center">Sampah Sekolah</th> <?php } ?>
						                <th class="text-center">Sampah Bank Sampah</th>
						               
						            </tr>
						            <?php  foreach ($pengepul_terbanyak as $data) { ?>
						            	<tr>
						                <th><?= $data->nama_pengepul?></th>
						               <?php if(sekolahMenuStatus()){ ?>  <th class="text-center"><?= $data->total?> KG</th> <?php } ?>
						                <th class="text-center"><?= $data->totalsam?> KG</th>
						            </tr>
									<?php } 
									 // $this->dash->get_pengepul_terbanyak();
									 // echo $this->db->last_query();
									?>
									</table>
									</div>
								</div>
                  			</div>
                  		</div>
                  	</div>
                  </div>

<div class="content">
	<div class="container-fluid">
<div class="row">
  <div class="col-md-12">
    <div class="card">
      <div class="card-header p-2">
        <ul class="nav nav-pills">
          <li class="nav-item"><a class="nav-link active" href="#mastertab" data-toggle="tab">Master</a></li>
          <li class="nav-item"><a class="nav-link" href="#transaksi" data-toggle="tab">Transaksi</a></li>
          <li class="nav-item"><a class="nav-link" href="#setoran" data-toggle="tab">Setoran</a></li>
        </ul>
      </div>
      <div class="card-body bg-light" >
        <div class="tab-content">
          <div class="active tab-pane" id="mastertab">
            <div class="post">

				<div class="card card-info">
					<div class="card-header">
						<h3 class="card-title">Grafik Master (Pengepul, Nasabah, Bank Sampah,  <?php if(sekolahMenuStatus()){ ?> Sekolah <?php } ?>) <?php
						// $this->dash->get_pembelian_bulanan($thn);
						// echo $this->db->last_query();
						 ?></h3>

						<div class="card-tools">
						<button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i>
						</button>
						<button type="button" class="btn btn-tool" data-card-widget="remove"><i class="fas fa-times"></i></button>
						</div>
					</div>
					<div class="card-body">
						<center>
							<canvas id="master" style="height: 400px; max-width: 80%;"></canvas>
						</center>
					</div>
				</div>

            </div>
          </div>
          <div class="tab-pane" id="transaksi">

			<div class="card card-info">
				<div class="card-header">
					<h3 class="card-title">Grafik Transaksi Sampah
					</h3>

					<div class="card-tools">
					<button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i>
					</button>
					<button type="button" class="btn btn-tool" data-card-widget="remove"><i class="fas fa-times"></i></button>
					</div>
				</div>
				<div class="card-body">
					<center>
						<canvas id="masterpenjualan" style="height: 400px; max-width: 80%;"></canvas>
					</center>
				</div>
			</div>

          </div>
          <div class="tab-pane" id="setoran">

			<div class="card card-info">
				<div class="card-header">
					<h3 class="card-title">Grafik Setoran Sampah</h3>

					<div class="card-tools">
					<button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i>
					</button>
					<button type="button" class="btn btn-tool" data-card-widget="remove"><i class="fas fa-times"></i></button>
					</div>
				</div>
				<div class="card-body">
					<center>
						<canvas id="mastersetoranonline" style="height: 400px; max-width: 80%;"></canvas>
					</center>
				</div>
			</div>

          </div>
        </div>

      </div>
    </div>
  </div>
</div>
</div>
</div>

<script>

(function () {

	// $('#table1').height($('#pie1').height());


}());

//initialisasi grafik line
var canvas = document.getElementById("master");
var ctx = canvas.getContext('2d');
var canvas1 = document.getElementById("masterpenjualan");
var ctxpenjualan = canvas1.getContext('2d');
var canvas2 = document.getElementById("mastersetoranonline");
var ctxsetoranonline = canvas2.getContext('2d');

Chart.defaults.global.defaultFontColor = 'black';
Chart.defaults.global.defaultFontSize = 18;

<?php
//pengepul
				$b[0] = "";
				$cek = 0;
				$c = 0;
				for ($i = 1; $i <= 12; $i++) {
					$cek = 0;
					foreach ($pengepul_bulanan as $peng) {
						if ($i == $peng->bln) {
							$cek = 1;
							$c = $peng->jml;
						}
					}
					if ($cek == 0) {
						$b[$i] = 0;
					} else {
						$b[$i] = $c;
					}
				}
//end pengepul

//penjualan bank
$penjbank[0] = "";
$penjbankkg[0] = "";
$penjbanktotal[0] = "";
				$cekpenjbank = 0;
				$cpenjbankkg = 0;
				$cpenjbanktotal = 0;
				for ($i = 1; $i <= 12; $i++) {
					$cekpenjbank = 0;
					foreach ($penjualan_bulanan_bank as $penj) {
						if ($i == $penj->bln) {
							$cekpenjbank = 1;
							$cpenjbank = $penj->jml;
							$cpenjbankkg = $penj->berat;
							$cpenjbanktotal = $penj->harga_jual;
						}
					}
					if ($cekpenjbank == 0) {
						$penjbank[$i] = 0;
						$penjbankkg[$i] = 0;
						$penjbanktotal[$i] = 0;
					} else {
						$penjbank[$i] = $cpenjbank;
						$penjbankkg[$i] = $cpenjbankkg;
						$penjbanktotal[$i] = $cpenjbanktotal;
					}
				}
				
//endpenjualanbank
//Setoran Online
$setonline[0] = "";
$setonlinekg[0] = "";
$setonlinetotal[0] = "";
				$ceksetonline = 0;
				$csetonline = 0;
				$csetonlinekg = 0;
				$csetonlinetotal = 0;
				for ($i = 1; $i <= 12; $i++) {
					$ceksetonline = 0;
					foreach ($setoran_online_bulanan as $seton) {
						if ($i == $seton->bln) {
							$ceksetonline = 1;
							$csetonline = $seton->jml;
							$csetonlinekg = $seton->berat;
							$csetonlinetotal = $seton->total;
						}
					}
					if ($ceksetonline == 0) {
						$setonline[$i] = 0;
						$setonlinekg[$i] = 0;
						$setonlinetotal[$i] = 0;
					} else {
						$setonline[$i] = $csetonline;
						$setonlinekg[$i] = $csetonlinekg;
						$setonlinetotal[$i] = $csetonlinetotal;
					}
				}
				
//endsetoran

//Setoran Offline
$setoffline[0] = "";
$setofflinetotal[0] = "";
$setofflinekg[0] = "";
				$ceksetoffline = 0;
				$csetoffline = 0;
				$csetofflinetotal = 0;
				$csetofflinekg = 0;
				for ($i = 1; $i <= 12; $i++) {
					$ceksetoffline = 0;
					foreach ($setoran_offline_bulanan as $setoff) {
						if ($i == $setoff->bln) {
							$ceksetoffline = 1;
							$csetoffline = $setoff->jml;
							$csetofflinetotal = $setoff->total;
							$csetofflinekg = $setoff->berat;
						}
					}
					if ($ceksetoffline == 0) {
						$setoffline[$i] = 0;
						$setofflinetotal[$i] = 0;
						$setofflinekg[$i] = 0;
					} else {
						$setoffline[$i] = $csetoffline;
						$setofflinetotal[$i] = $csetofflinetotal;
						$setofflinekg[$i] = $csetofflinekg;
					}
				}
				
//endsetoran
				



//penjualan sekolah
$penjsek[0] = "";
$penjsekkg[0] = "";
$penjsektotal[0] = "";
				$cekpenjsek = 0;
				$cpenjsek = 0;
				$cpenjsekkg = 0;
				$cpenjsektotal = 0;
				for ($i = 1; $i <= 12; $i++) {
					$cekpenjsek = 0;
					foreach ($penjualan_sekolah as $penj) {
						if ($i == $penj->bln) {
							$cekpenjsek = 1;
							$cpenjsek = $penj->jml;
							$cpenjsekkg = $penj->berat;
							$cpenjsektotal = $penj->harga_jual;
						}
					}
					if ($cekpenjsek == 0) {
						$penjsek[$i] = 0;
						$penjsekkg[$i] = 0;
						$penjsektotal[$i] = 0;

					} else {
						$penjsek[$i] = $cpenjsek;
						$penjsekkg[$i] = $cpenjsekkg;
						$penjsektotal[$i] = $cpenjsektotal;
					}
				}
				
//endpenjualansekolah

//Pembelian pengepul
$pempeng[0] = "";
$pempengkg[0] = "";
$pempengtotal[0] = "";
				$cekpempeng = 0;
				$cpempeng = 0;
				$cpempengkg = 0;
				$cpempengtotal = 0;
				for ($i = 1; $i <= 12; $i++) {
					$cekpempeng = 0;
					foreach ($pembelian_pengepul as $pem) {
						if ($i == $pem->bln) {
							$cekpempeng = 1;
							$cpempeng = $pem->jml;
							$cpempengkg = $pem->total_berat;
							$cpempengtotal = $pem->total_harga;
						}
					}
					if ($cekpempeng == 0) {
						$pempeng[$i] = 0;
						$pempengkg[$i] = 0;
						$pempengtotal[$i] = 0;
					} else {
						$pempeng[$i] = $cpempeng;
						$pempengkg[$i] = $cpempengkg;
						$pempengtotal[$i] = $cpempengtotal;
					}
				}
				?>
//endpenjualansekolah


//bank sampah

	<?php
				$banks[] = "";
				$cekbank = 0;
				$c1 = 0;
				for ($i = 1; $i <= 12; $i++) {
					$cekbank = 0;
					foreach ($bank as $data) {
						if ($i == $data->bln) {
							$cekbank = 1;
							$c1 = $data->jml;
						}
					}
					if ($cekbank == 0) {
						$banks[$i] = 0;
					} else {
						$banks[$i] = $c1;
					}
				}
				

				?>
//Sekolah

	<?php
				$sekolahs[] = "";
				$ceksekolah = 0;
				$c5 = 0;
				for ($i = 1; $i <= 12; $i++) {
					$ceksekolah = 0;
					foreach ($sekolah_bul as $data) {
						if ($i == $data->bln) {
							$ceksekolah = 1;
							$c1 = $data->jml;
						}
					}
					if ($ceksekolah == 0) {
						$sekolahs[$i] = 0;
					} else {
						$sekolahs[$i] = $c1;
					}
				}
				
				?>

var datapenjualan = {
  labels: ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"],
  datasets: [{
      label: "Penjualan Bank Sampah",
      fill: false,
      lineTension: 0.1,
      backgroundColor: "rgba(225,0,0,0.4)",
      borderColor: "red",
      borderCapStyle: 'square',
      borderDash: [],
      borderJoinStyle: 'miter',
      pointBorderColor: "black",
      pointBackgroundColor: "black",
      pointBorderWidth: 1,
      pointHoverRadius: 8,
      pointHoverBackgroundColor: "pink",
      pointHoverBorderColor: "brown",
      pointHoverBorderWidth: 2,
      pointRadius: 4,
      pointHitRadius: 10,
      kg:[   <?php
						for ($i = 1; $i <= 12; $i++) {
							echo $penjbankkg[$i] . ',';
						}

						?>],
	 total:[   <?php
						for ($i = 1; $i <= 12; $i++) {
							echo $penjbanktotal[$i] . ',';
						}

						?>],
      data: [
      <?php
						for ($i = 1; $i <= 12; $i++) {
							echo $penjbank[$i] . ',';
						}

						?>
      // 65,59,80,81,56,55,40,59,60,55,30,78
      ],
      spanGaps: true,
    }, <?php if(sekolahMenuStatus()){ ?>
    {
      label: "Penjualan Sekolah",
      fill: false,
      lineTension: 0.1,
      backgroundColor: "rgba(225,0,0,0.4)",
      borderColor: "green",
      borderCapStyle: 'square',
      borderDash: [],
      borderJoinStyle: 'miter',
      pointBorderColor: "black",
      pointBackgroundColor: "black",
      pointBorderWidth: 1,
      pointHoverRadius: 8,
      pointHoverBackgroundColor: "pink",
      pointHoverBorderColor: "brown",
      pointHoverBorderWidth: 2,
      pointRadius: 4,
      pointHitRadius: 10,
       kg:[   <?php
						for ($i = 1; $i <= 12; $i++) {
							echo $penjsekkg[$i] . ',';
						}

						?>],
		total:[   <?php
						for ($i = 1; $i <= 12; $i++) {
							echo $penjsektotal[$i] . ',';
						}

						?>],
      data: [
      <?php
						for ($i = 1; $i <= 12; $i++) {
							echo $penjsek[$i] . ',';
						}

						?>
      // 65,59,80,81,56,55,40,59,60,55,30,78
      ],
      spanGaps: true,
    }, <?php } ?>
    {
      label: "Pembelian Pengepul",
      fill: false,
      lineTension: 0.1,
      backgroundColor: "rgba(225,0,0,0.4)",
      borderColor: "purple",
      borderCapStyle: 'square',
      borderDash: [],
      borderJoinStyle: 'miter',
      pointBorderColor: "black",
      pointBackgroundColor: "black",
      pointBorderWidth: 1,
      pointHoverRadius: 8,
      pointHoverBackgroundColor: "pink",
      pointHoverBorderColor: "brown",
      pointHoverBorderWidth: 2,
      pointRadius: 4,
      pointHitRadius: 10,
         kg:[   <?php
						for ($i = 1; $i <= 12; $i++) {
							echo $pempengkg[$i] . ',';
						}

						?>], 
		  total:[   <?php
						for ($i = 1; $i <= 12; $i++) {
							echo $pempengtotal[$i] . ',';
						}

						?>],
      data: [
      <?php
						for ($i = 1; $i <= 12; $i++) {
							echo $pempeng[$i] . ',';
						}

						?>
      // 65,59,80,81,56,55,40,59,60,55,30,78
      ],
      spanGaps: true,
    },
    ]
};
// Notice the scaleLabel at the same level as Ticks
var options = {
	 tooltips: {
      callbacks: {
        title: function(tooltipItem, data) {
          return data['labels'][tooltipItem[0]['index']];
        },
        label: function(tooltipItem, data) {
         return data.datasets[tooltipItem.datasetIndex].label;
          // return data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
       
        },
        afterLabel: function(tooltipItem, data) {
         return 'Total Transaksi = '+data.datasets[tooltipItem.datasetIndex].kg[tooltipItem.index]+' Kg ('+data.datasets[tooltipItem.datasetIndex].total[tooltipItem.index]+' KOIN)';
       
        }
      },

      backgroundColor: '#FFF',
      titleFontSize: 16,
      titleFontColor: '#333',
      labelFontColor: '#333',
      bodyFontColor: '#000',
      bodyFontSize: 14,
      displayColors: false
    },
    
  scales: {
            yAxes: [{

                scaleLabel: {
                     display: true,
                     labelString: '',
                     fontSize: 10 
                  }
            }]            
        }  
};

// Chart declaration:
var mymasterpenjualan = new Chart(ctxpenjualan, {
  type: 'line',
  data: datapenjualan,
  options: options
});


//end penjualan



var datasetoranonline = {
  labels: ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"],
  datasets: [{
      label: "Setoran Online",
      fill: false,
      lineTension: 0.1,
      backgroundColor: "rgba(225,0,0,0.4)",
      borderColor: "blue",
      borderCapStyle: 'square',
      borderDash: [],
      borderJoinStyle: 'miter',
      pointBorderColor: "black",
      pointBackgroundColor: "black",
      pointBorderWidth: 1,
      pointHoverRadius: 8,
      pointHoverBackgroundColor: "pink",
      pointHoverBorderColor: "brown",
      pointHoverBorderWidth: 2,
      pointRadius: 4,
      pointHitRadius: 10,
      data: [
      <?php
						for ($i = 1; $i <= 12; $i++) {
							echo $setonline[$i] . ',';
						}

						?>
      // 65,59,80,81,56,55,40,59,60,55,30,78
      ], 
       total: [
      <?php
						for ($i = 1; $i <= 12; $i++) {
							echo $setonlinetotal[$i] . ',';
						}

						?>
      // 65,59,80,81,56,55,40,59,60,55,30,78
      ], 
       kg: [
      <?php
						for ($i = 1; $i <= 12; $i++) {
							echo $setonlinekg[$i] . ',';
						}

						?>
      // 65,59,80,81,56,55,40,59,60,55,30,78
      ],
      spanGaps: true,
    },
    {
      label: "Setoran Offline",
      fill: false,
      lineTension: 0.1,
      backgroundColor: "rgba(225,0,0,0.4)",
      borderColor: "yellow",
      borderCapStyle: 'square',
      borderDash: [],
      borderJoinStyle: 'miter',
      pointBorderColor: "black",
      pointBackgroundColor: "black",
      pointBorderWidth: 1,
      pointHoverRadius: 8,
      pointHoverBackgroundColor: "pink",
      pointHoverBorderColor: "brown",
      pointHoverBorderWidth: 2,
      pointRadius: 4,
      pointHitRadius: 10,
      data: [
      <?php
						for ($i = 1; $i <= 12; $i++) {
							echo $setoffline[$i] . ',';
						}

						?>
      // 65,59,80,81,56,55,40,59,60,55,30,78
      ],
       total: [
      <?php
						for ($i = 1; $i <= 12; $i++) {
							echo $setofflinetotal[$i] . ',';
						}

						?>
      // 65,59,80,81,56,55,40,59,60,55,30,78
      ], 
      kg: [
      <?php
						for ($i = 1; $i <= 12; $i++) {
							echo $setofflinekg[$i] . ',';
						}

						?>
      // 65,59,80,81,56,55,40,59,60,55,30,78
      ],
      spanGaps: true,
    },
    ]
};
// Notice the scaleLabel at the same level as Ticks
var options = {
	 tooltips: {
      callbacks: {
        title: function(tooltipItem, data) {
          return data['labels'][tooltipItem[0]['index']];
        },
        label: function(tooltipItem, data) {
         return data.datasets[tooltipItem.datasetIndex].label;
          // return data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
       
        },
        afterLabel: function(tooltipItem, data) {
         return 'Total Transaksi = '+data.datasets[tooltipItem.datasetIndex].kg[tooltipItem.index]+' Kg ('+data.datasets[tooltipItem.datasetIndex].total[tooltipItem.index]+' KOIN)';
       
        }
      },

      backgroundColor: '#FFF',
      titleFontSize: 16,
      titleFontColor: '#333',
      labelFontColor: '#333',
      bodyFontColor: '#000',
      bodyFontSize: 14,
      displayColors: false
    },
  scales: {
            yAxes: [{
                ticks: {
                    beginAtZero:true
                },
                scaleLabel: {
                     display: true,
                     labelString: '',
                     fontSize: 10 
                  }
            }]            
        }  
};

// Chart declaration:
var mymastersetoranonline = new Chart(ctxsetoranonline, {
  type: 'line',
  data: datasetoranonline,
  options: options
});


//end setoran




//nasabah online
<?php
				$nas[] = "";
				$ceknas = 0;
				$c5 = 0;
				for ($i = 1; $i <= 12; $i++) {
					$ceknas = 0;
					foreach ($nasabah_bul as $data) {
						if ($i == $data->bln) {
							$ceknas = 1;
							$c5 = $data->jml;
						}
					}
					if ($ceknas == 0) {
						$nas[$i] = 0;
					} else {
						$nas[$i] = $c5;
					}
				}
				

				?>
	
var data = {
  labels: ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"],
  datasets: [{
      label: "Pengepul",
      fill: false,
      lineTension: 0.1,
      backgroundColor: "rgba(225,0,0,0.4)",
      borderColor: "red",
      borderCapStyle: 'square',
      borderDash: [],
      borderJoinStyle: 'miter',
      pointBorderColor: "black",
      pointBackgroundColor: "black",
      pointBorderWidth: 1,
      pointHoverRadius: 8,
      pointHoverBackgroundColor: "pink",
      pointHoverBorderColor: "brown",
      pointHoverBorderWidth: 2,
      pointRadius: 4,
      pointHitRadius: 10,
      data: [
      <?php
						for ($i = 1; $i <= 12; $i++) {
							echo $b[$i] . ',';
						}

						?>
      // 65,59,80,81,56,55,40,59,60,55,30,78
      ],
      spanGaps: true,
    }, {
      label: "Nasabah",
      fill: false,
      lineTension: 0.1,
      backgroundColor: "rgba(225,0,0,0.4)",
      borderColor: "green",
      borderCapStyle: 'butt',
      borderDash: [],
      borderDashOffset: 0.0,
      borderJoinStyle: 'miter',
      pointBorderColor: "white",
      pointBackgroundColor: "black",
      pointBorderWidth: 1,
      pointHoverRadius: 8,
      pointHoverBackgroundColor: "pink",
      pointHoverBorderColor: "brown",
      pointHoverBorderWidth: 2,
      pointRadius: 4,
      pointHitRadius: 10,
      data: [
       <?php
						for ($i = 1; $i <= 12; $i++) {
							echo $nas[$i] . ',';
						}

						?>
      ],
      spanGaps: false,
    }, {

      label: "Bank Sampah",
      fill: false,
      lineTension: 0.1,
      backgroundColor: "rgba(225,0,0,0.4)",
      borderColor: "blue",
      borderCapStyle: 'butt',
      borderDash: [],
      borderDashOffset: 0.0,
      borderJoinStyle: 'miter',
      pointBorderColor: "white",
      pointBackgroundColor: "black",
      pointBorderWidth: 1,
      pointHoverRadius: 8,
      pointHoverBackgroundColor: "pink",
      pointHoverBorderColor: "brown",
      pointHoverBorderWidth: 2,
      pointRadius: 4,
      pointHitRadius: 10,
      data: [
      <?php
						 for ($i = 1; $i <= 12; $i++) {
							echo $banks[$i] . ',';
						}

						?>
      // 20,40,50,35,74,38,70,80,90,20,30,69
      ],
      spanGaps: true,
    },
     <?php if(sekolahMenuStatus()){ ?>
{

      label: "Sekolah",
      fill: false,
      lineTension: 0.1,
      backgroundColor: "rgba(225,0,0,0.4)",
      borderColor: "grey",
      borderCapStyle: 'butt',
      borderDash: [],
      borderDashOffset: 0.0,
      borderJoinStyle: 'miter',
      pointBorderColor: "white",
      pointBackgroundColor: "black",
      pointBorderWidth: 1,
      pointHoverRadius: 8,
      pointHoverBackgroundColor: "pink",
      pointHoverBorderColor: "brown",
      pointHoverBorderWidth: 2,
      pointRadius: 4,
      pointHitRadius: 10,
      data: [
      <?php
						 for ($i = 1; $i <= 12; $i++) {
							echo $sekolahs[$i] . ',';
						}

						?>
      // 20,40,50,35,74,38,70,80,90,20,30,69
      ],
      spanGaps: true,
    }
<?php } ?>
  ]
};

// Notice the scaleLabel at the same level as Ticks
var options = {
  scales: {
            yAxes: [{
                ticks: {
                    beginAtZero:true
                },
                scaleLabel: {
                     display: true,
                     labelString: '',
                     fontSize: 10 
                  }
            }]            
        }  
};

// Chart declaration:
var mymaster = new Chart(ctx, {
  type: 'line',
  data: data,
  options: options
});


//pie bank
var dataSampah = document.getElementById("sampah");

Chart.defaults.global.defaultFontFamily = "Lato";
Chart.defaults.global.defaultFontSize = 18;

var jumlahSampah = {
    labels: [
         <?php foreach ($nama_pie as $n_pie) {
          echo  "'" . $n_pie->nama_kategori_sampah . "',";
        } ?>
    ],
    datasets: [
        {
			data: [
			  <?php foreach ($berat_pie as $b_pie) {
            echo  $b_pie->total . ",";
          } ?>
				],
            backgroundColor: [
                "red",
                "green",
                "blue",
				"yellow",
				"pink",
				"purple",
				"orange",
            ]
        }]
};

var pieChart = new Chart(dataSampah, {
  type: 'pie',
  data: jumlahSampah,
  options: {
	  tooltips: {
		  callbacks: {
			title: function(tooltipItem, data) {
				return data['labels'][tooltipItem[0]['index']];
			},
			label: function(tooltipItem, data) {
				return formatAngka(`'`+data['datasets'][0]['data'][tooltipItem['index']]+`'`)+' KG';
			},
			afterLabel: function(tooltipItem, data) {
			}
		  }
	  }
  } 
});

//pie sekolah
var dataSampahsek = document.getElementById("sampahsekolah");

Chart.defaults.global.defaultFontFamily = "Lato";
Chart.defaults.global.defaultFontSize = 18;

var jumlahSampahsek = {
    labels: [
         <?php foreach ($sekolah_pie as $n_pie) {
          echo  "'" . $n_pie->nama_kategori_sampah . "',";
        } ?>
    ],
    datasets: [
        {
			data: [
			  <?php foreach ($sekolah_pie as $b_pie) {
            echo  $b_pie->total . ",";
          } ?>
				],
            backgroundColor: [
                "red",
                "green",
                "blue",
				"yellow",
				"pink",
				"purple",
				"orange",
            ]
        }]
};

var pieChart = new Chart(dataSampahsek, {
  type: 'pie',
  data: jumlahSampahsek,
  options: {
	  tooltips: {
		  callbacks: {
			title: function(tooltipItem, data) {
				return data['labels'][tooltipItem[0]['index']];
			},
			label: function(tooltipItem, data) {
				return formatAngka(`'`+data['datasets'][0]['data'][tooltipItem['index']]+`'`)+' KG';
			},
			afterLabel: function(tooltipItem, data) {
			}
		  }
	  }
  } 
});
  </script>