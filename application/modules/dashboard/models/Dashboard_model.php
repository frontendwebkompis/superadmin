<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Dashboard_model extends CI_Model
{


    public $order = 'DESC';

    function __construct()
    {
        parent::__construct();
    }

    // get all
    function get_pengepul_all()
    {
        $this->db->select('count(*) as jml')
            ->where('id_status_pengepul', 2);
        return $this->db->get('pengepul')->row();
    }
    // get all
    function get_nasabah_all()
    {
        $this->db->select('count(*) as jml')
            ->where('id_status_nasabah_online', 2);
        return $this->db->get('nasabah_online')->row();
    }
    function get_bank_sampah_all()
    {
        $this->db->select('count(*) as jml')
            ->where('id_status_bank_sampah', 2);
        return $this->db->get('bank_sampah')->row();
    }
     function get_sekolah_all()
    {
        $this->db->select('count(*) as jml')
            ->where('id_status_bank_sampah', 2);
        return $this->db->get('sekolah')->row();
    }
    function get_kategori_all()
    {
        $this->db->select('count(*) as jml')
            ->where('status_kategori_sampah_remove', 1);
        return $this->db->get('kategori_sampah')->row();
    }
    function get_jenis_all()
    {
        $this->db->select('count(*) as jml')
            ->where('status_jenis_sampah_remove', 1);
        return $this->db->get('jenis_sampah')->row();
    }
    function get_merk_all()
    {
        $this->db->select('count(*) as jml')
            ->where('status_merk_sampah_remove', 1);
        return $this->db->get('merk_sampah')->row();
    }
    function get_brand_all()
    {
        $this->db->select('count(*) as jml')
            ->where('status_brand_remove', 1);
        return $this->db->get('brand')->row();
    }
    function get_pengepul_bulanan($thn)
    {
        $this->db->select('count(*) as jml, MONTH(pengepul.tgl_daftar_pengepul) as bln')
        ->group_by('month(tgl_daftar_pengepul)')
        ->where('year(tgl_daftar_pengepul)',$thn);
    return $this->db->get('pengepul')->result();
    } 
    function get_penjualan_bank($thn)
    {
        $this->db->select('count(penjualan_bank_sampah.id_penjualan_bank_sampah) as jml, MONTH(penjualan_bank_sampah.tgl_penjualan_bank_sampah) as bln,sum(penjualan_bank_sampah_detail.total_berat) berat,sum(penjualan_bank_sampah_detail.harga_jual) harga_jual')
         ->join('penjualan_bank_sampah_detail', 'penjualan_bank_sampah.id_penjualan_bank_sampah=penjualan_bank_sampah_detail.id_penjualan_bank_sampah')
        ->group_by('month(tgl_penjualan_bank_sampah)')
        ->where('id_status_penjualan_bank_sampah',7)
        ->where('year(tgl_penjualan_bank_sampah)',$thn);
    return $this->db->get('penjualan_bank_sampah')->result();
    } 
     function get_pembelian_bulanan($thn)
    {
        $this->db->select('count(pembelian_pengepul.id_pembelian_pengepul) as jml, MONTH(pembelian_pengepul.tgl_pembelian_sampah) as bln, sum(penjualan_sekolah.total_berat) berat_sekolah, sum(penjualan_bank_sampah_detail.total_berat) berat_bank,COALESCE(sum(penjualan_sekolah.total_berat),0)+COALESCE(sum(penjualan_bank_sampah_detail.total_berat),0) total_berat,sum(pembelian_pengepul_detail.harga_beli) as total_harga')
          ->join('pembelian_pengepul_detail', 'pembelian_pengepul_detail.id_pembelian_pengepul=pembelian_pengepul.id_pembelian_pengepul','left')
        ->join('penjualan_sekolah', 'penjualan_sekolah.id_penjualan_sekolah=pembelian_pengepul_detail.id_penjualan_sekolah','left')
         ->join('penjualan_bank_sampah', 'penjualan_bank_sampah.id_penjualan_bank_sampah=pembelian_pengepul_detail.id_penjualan_bank_sampah','left')
          ->join('penjualan_bank_sampah_detail', 'penjualan_bank_sampah.id_penjualan_bank_sampah=penjualan_bank_sampah_detail.id_penjualan_bank_sampah','left')
        ->group_by('month(tgl_pembelian_sampah)')
        ->where('id_status_pembelian_pengepul',7)
        ->where('year(tgl_pembelian_sampah)',$thn);
    return $this->db->get('pembelian_pengepul')->result();
    }
      function get_setoran_online_bulanan($thn)
    {
        $this->db->select('count(setoran_online.id_setoran_online) as jml, MONTH(setoran_online.tgl_setoran) as bln,sum(setoran_online_detail.berat_setoran) berat,sum(setoran_online_detail.harga_setoran) total')
         ->join('setoran_online_detail', 'setoran_online.id_setoran_online=setoran_online_detail.id_setoran_online')
  
        ->group_by('month(setoran_online.tgl_setoran)')
        ->where('id_status_setoran',7)
        ->where('year(setoran_online.tgl_setoran)',$thn);
    return $this->db->get('setoran_online')->result();
    } 
      function get_setoran_offline_bulanan($thn)
    {
        $this->db->select('count(setoran_offline.id_setoran_offline) as jml, MONTH(setoran_offline.created_at) as bln,sum(setoran_offline_detail.harga_setoran) total,sum(setoran_offline_detail.berat_setoran) berat')
          ->join('setoran_offline_detail', 'setoran_offline.id_setoran_offline=setoran_offline_detail.id_setoran_offline')
        ->group_by('month(setoran_offline.created_at)')
        ->where('id_status_setoran',7)
        ->where('year(setoran_offline.created_at)',$thn);
    return $this->db->get('setoran_offline')->result();
    } 
    function get_penjualan_sekolah($thn)
    {
        $this->db->select('count(id_penjualan_sekolah) as jml, MONTH(penjualan_sekolah.tgl_penjualan) as bln,sum(total_berat) as berat,sum(harga_jual) harga_jual')
        ->group_by('month(tgl_penjualan)')
        ->where('id_status_penjualan_bank_sampah',7)
        ->where('year(tgl_penjualan)',$thn);
    return $this->db->get('penjualan_sekolah')->result();
    }
    function get_bank_sampah_bulanan($thn)
    {
        $this->db->select('count(*) as jml, MONTH(bank_sampah.tgl_daftar) as bln')
        ->group_by('month(tgl_daftar)')
        ->where('year(tgl_daftar)',$thn);
    return $this->db->get('bank_sampah')->result();
    }  
      function get_sekolah_bulanan($thn)
    {
        $this->db->select('count(*) as jml, MONTH(sekolah.tgl_daftar) as bln')
        ->group_by('month(tgl_daftar)')
        ->where('year(tgl_daftar)',$thn);
    return $this->db->get('sekolah')->result();
    }  
     function get_nasabah_bulanan($thn)
    {
        $this->db->select('count(*) as jml, MONTH(nasabah_online.tgl_nasabah_daftar) as bln')
        ->group_by('month(tgl_nasabah_daftar)')
        ->where('year(tgl_nasabah_daftar)',$thn);
    return $this->db->get('nasabah_online')->result();
    }
      function get_nasabah_saldo()
    {
      $db=$this->db->select('SUM(setoran_online_detail.harga_setoran) AS saldo,nasabah_online.nama_nasabah_online')
    ->join('setoran_online', 'setoran_online.id_setoran_online=setoran_online_detail.id_setoran_online')
    ->join('nasabah_online', 'nasabah_online.id_nasabah_online=setoran_online.id_nasabah_online')
    ->order_by('saldo','desc')
     ->limit(5)
    ->group_by('setoran_online.id_nasabah_online')
    ->get('setoran_online_detail');
    return $db->result();
    }

    function get_sampah_sekolah(){
         $db=$this->db->select('sekolah.nama_sekolah,COUNT(setoran_siswa.id_setoran_siswa) AS jml,SUM(setoran_siswa_detail.berat_setoran) AS total')
         ->join('setoran_siswa_detail', 'setoran_siswa.id_setoran_siswa=setoran_siswa_detail.id_setoran_siswa')
         ->join('sekolah', 'sekolah.id_sekolah=setoran_siswa.id_sekolah')
             ->group_by('setoran_siswa.id_sekolah')
              ->order_by('total','desc')
               ->limit(5)
              ->get('setoran_siswa');
    return $db->result();
    }
    function get_pengepul_terbanyak(){
         $db=$this->db->select('pengepul.nama_pengepul,COUNT(pembelian_pengepul.id_pembelian_pengepul) AS jml,SUM(penjualan_sekolah.total_berat) AS total,SUM(penjualan_bank_sampah_detail.total_berat) AS totalsam,SUM(penjualan_sekolah.total_berat)+SUM(penjualan_bank_sampah_detail.total_berat) as tot')
         ->join('pembelian_pengepul_detail', 'pembelian_pengepul_detail.id_pembelian_pengepul=pembelian_pengepul.id_pembelian_pengepul','left')
         ->join('penjualan_sekolah', 'penjualan_sekolah.id_penjualan_sekolah=pembelian_pengepul_detail.id_penjualan_sekolah','left')
         ->join('penjualan_bank_sampah', 'penjualan_bank_sampah.id_penjualan_bank_sampah=pembelian_pengepul_detail.id_penjualan_bank_sampah','left')
         ->join('penjualan_bank_sampah_detail', 'penjualan_bank_sampah.id_penjualan_bank_sampah=penjualan_bank_sampah_detail.id_penjualan_bank_sampah','left')
         ->join('pengepul', 'pengepul.id_pengepul=pembelian_pengepul.id_pengepul','left')
           ->where('pembelian_pengepul.id_status_pembelian_pengepul',7)
             ->group_by('pembelian_pengepul.id_pengepul')
              ->order_by('tot','desc')
              ->limit(5)
              ->get('pembelian_pengepul');
    return $db->result();
    }
    function get_pie_nama($thn)
{
    $db=$this->db->select('kategori_sampah.nama_kategori_sampah,sum(setoran_offline_detail.berat_setoran) as total')
    ->join('setoran_offline', 'setoran_offline.id_setoran_offline=setoran_offline_detail.id_setoran_offline','left')
    ->join('kategori_sampah', 'kategori_sampah.id_kategori_sampah=setoran_offline_detail.id_kategori_sampah')
     ->where('setoran_offline.id_status_setoran',7)
    ->where('year(setoran_offline.created_at)',$thn)
    ->group_by('kategori_sampah.nama_kategori_sampah')
    ->get('setoran_offline_detail');
 
    return $db->result();
}
function get_pie_sekolah($thn)
{
    $db=$this->db->select('kategori_sampah.nama_kategori_sampah,sum(setoran_siswa_detail.berat_setoran) as total')
    ->join('setoran_siswa', 'setoran_siswa.id_setoran_siswa=setoran_siswa_detail.id_setoran_siswa','left')
    ->join('kategori_sampah', 'kategori_sampah.id_kategori_sampah=setoran_siswa_detail.id_kategori_sampah')
     ->where('setoran_siswa.id_status_setoran',7)
    ->where('year(setoran_siswa.created_at)',$thn)
    ->group_by('kategori_sampah.nama_kategori_sampah')
    ->get('setoran_siswa_detail');
 
    return $db->result();
}
}
