<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Dashboard extends MX_Controller
{

	function __construct()
    {
		parent::__construct();
		if (cek_token()) {
        $this->load->model('Dashboard_model','dash');
		$this->load->library('form_validation');
		} else {
			logout();
		
		}
    }

	function index()
	{
		$thn 						= $this->input->get('tahun') ? $this->input->get('tahun') : '2020';
		$pengepul 					= $this->dash->get_pengepul_all();
		$nasabah 					= $this->dash->get_nasabah_all();
		$bank_sampah 				= $this->dash->get_bank_sampah_all();
		$sekolah 					= $this->dash->get_sekolah_all();
		$kategori 					= $this->dash->get_kategori_all();
		$jenis_sampah				= $this->dash->get_jenis_all();
		$merk_sampah				= $this->dash->get_merk_all();
		$brand 						= $this->dash->get_brand_all();
		$pengepul_bulanan 			= $this->dash->get_pengepul_bulanan($thn);
		$setoran_online_bulanan 	= $this->dash->get_setoran_online_bulanan($thn);
		$setoran_offline_bulanan 	= $this->dash->get_setoran_offline_bulanan($thn);
		$penjualan_bulanan_bank=  $this->dash->get_penjualan_bank($thn);
		$pembelian_bulanan=  $this->dash->get_pembelian_bulanan($thn);
		$penjualan_sekolah=  $this->dash->get_penjualan_sekolah($thn);
		$bank 						= $this->dash->get_bank_sampah_bulanan($thn);
		$nasabah_bul 				= $this->dash->get_nasabah_bulanan($thn);
		$sekolah_bul 				= $this->dash->get_sekolah_bulanan($thn);
    	$nama_pie             		= $this->dash->get_pie_nama($thn);
		$berat_pie             		= $this->dash->get_pie_nama($thn);
		$sekolah_pie             	= $this->dash->get_pie_sekolah($thn);
		$saldo_terbanyak       		= $this->dash->get_nasabah_saldo();
		$sekolah_terbanyak       	= $this->dash->get_sampah_sekolah();
       $pengepul_terbanyak       = $this->dash->get_pengepul_terbanyak();
     
		$res = array(
			'tittle'					=> 'Dashboard',
			'judul'						=> 'Dashboard',
			'tot_pengepul'				=> $pengepul->jml,
			'tot_nasabah'				=> $nasabah->jml,
			'tot_bank_sampah'			=> $bank_sampah->jml,
			'tot_kategori'				=> $kategori->jml,
			'tot_jenis'					=> $jenis_sampah->jml,
			'tot_merk'					=> $merk_sampah->jml,
			'tot_brand'					=> $brand->jml,
			'tot_sekolah'				=> $sekolah->jml,
			'pengepul_bulanan'			=> $pengepul_bulanan,
			'setoran_online_bulanan'	=> $setoran_online_bulanan,
			'pembelian_pengepul'		=> $pembelian_bulanan,
			'setoran_offline_bulanan'	=> $setoran_offline_bulanan,
			'penjualan_bulanan_bank'	=> $penjualan_bulanan_bank,
			'penjualan_sekolah'			=> $penjualan_sekolah,
			'bank'						=> $bank,
			'nasabah_bul'				=> $nasabah_bul,
			'sekolah_bul'				=> $sekolah_bul,
			'nama_pie'                	=> $nama_pie,
			'sekolah_pie'              	=> $sekolah_pie,
           	'berat_pie'                 => $berat_pie,
           	'saldo_terbanyak'           => $saldo_terbanyak,
           	'sekolah_terbanyak'         => $sekolah_terbanyak,
			'menu_aktif'				=> 'dashboard',
           	'pengepul_terbanyak'        => $pengepul_terbanyak,
		);
		
		$data['datakonten'] = $this->load->view('dashboard/dashboard', $res, true);
		$this->load->view('layouts/main_view', $data);
		
	}
}
