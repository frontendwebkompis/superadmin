<script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>

<div class="row">
	<section class="col-lg connectedSortable ui-sortable">
		<div class="card card-info">
			<div class="card-header">
				<div class="card-title">
					<?php echo $title_card ?>
				</div>
			</div>
			<div class="card-body">
				<div class="tab-content p-0">

					<div class="row">
						<div class="col-md-3 offset-md-9 mb-2">
							<?= search(site_url('laporan_data_smartbin/index'), site_url('laporan_data_smartbin'), $q) ?>
						</div>
					</div>
					<div class="tab-content p-0" style="overflow:auto">
						<table class="table table-bordered table-striped table-condensed table-hover table-sm" style="margin-bottom: 10px">
							<tr>
								<th class="text-center">No</th>
								<th class="text-center">Nama Brand</th>
								<th class="text-center">Kode Smartbin</th>
								<th class="text-center">Lokasi Smartbin</th>
								<th class="text-center">Detail Smartbin</th>
								<th class="text-center">Code Versi Smartbin</th>
								<th class="text-center">Api Check Smartbin</th>
								<th class="text-center">Api Flush Smartbin</th>
							</tr>
							<?php
							if ($total_rows == 0) {
								echo '<tr><td colspan="11" bgcolor="grey" style="color: white; font-weight: bold; text-align: center;">Data Tidak Ditemukan</td></tr>';
							} else {
								foreach ($laporan_data_smartbin as $k) { ?>
									<tr>
										<td class="text-center" width="50px"><?php echo ++$start ?></td>
										<td><?= $k->nama_brand ?></td>
										<td><?= $k->kode_smartbin ?></td>
										<td><?= $k->lokasi_smartbin ?></td>
										<td><?= $k->detail_smartbin ?></td>
										<td><?= $k->code_versi_smartbin ?></td>
										<td><?= $k->api_check_smartbin ?></td>
										<td><?= $k->api_flush_smartbin ?></td>
									</tr>
							<?php }
							} ?>
						</table>
					</div>
					<?= footer($total_rows, $pagination, site_url('laporan_data_smartbin/exportxl?q=' . $q)) ?>
				</div>
			</div>
		</div>
	</section>
</div>

<script>
	function confirm(res) {
		Swal.fire({
			title: 'Anda Yakin Mau Menghapus?',
			text: "Tidak bisa dikembalikan jika sudah dihapus!",
			icon: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'Ya, Hapus!',
			cancelButtonText: 'Batal'
		}).then((result) => {
			if (result.value) {
				Swal.fire(
					'Terhapus!',
					'File sudah terhapus.',
					'success'
				)
				window.location = '<?php echo base_url() . 'laporan_data_smartbin/del_sem/'; ?>' + res;
			}
		});
	}
</script>
