<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Laporan_data_smartbin_model extends CI_Model
{
	public $table = 'smartbin';
	public $id = 'id_smartbin';
	public $order = 'DESC';

	// get total rows
	function total_rows($q = NULL)
	{
		$this->db->join('brand', "$this->table.id_brand = brand.id_brand");

		$this->db->where('status_smartbin_remove', 1);
		$this->db->where('status_smartbin_aktif', 1);
		$this->db->group_start();
		$this->db->like("brand.nama_brand", $q);
		$this->db->or_like("$this->table.kode_smartbin", $q);
		$this->db->or_like("$this->table.lokasi_smartbin", $q);
		$this->db->or_like("$this->table.detail_smartbin", $q);
		$this->db->or_like("$this->table.code_versi_smartbin", $q);
		$this->db->or_like("$this->table.api_check_smartbin", $q);
		$this->db->or_like("$this->table.api_flush_smartbin", $q);
		$this->db->group_end();
		$this->db->from($this->table);
		return $this->db->count_all_results();
	}

	// get data with limit and search
	function get_limit_data($limit, $start = 0, $q = NULL)
	{
		$this->db->order_by($this->id, $this->order);
		$this->db->join('brand', "$this->table.id_brand = brand.id_brand");

		$this->db->where('status_smartbin_remove', 1);
		$this->db->where('status_smartbin_aktif', 1);
		$this->db->group_start();
		$this->db->like("brand.nama_brand", $q);
		$this->db->or_like("$this->table.kode_smartbin", $q);
		$this->db->or_like("$this->table.lokasi_smartbin", $q);
		$this->db->or_like("$this->table.detail_smartbin", $q);
		$this->db->or_like("$this->table.code_versi_smartbin", $q);
		$this->db->or_like("$this->table.api_check_smartbin", $q);
		$this->db->or_like("$this->table.api_flush_smartbin", $q);
		$this->db->group_end();
		$this->db->limit($limit, $start);
		return $this->db->get($this->table)->result();
	}
}

/* End of file Laporan_data_smartbin_model.php */
