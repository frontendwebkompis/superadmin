<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Laporan_data_smartbin extends CI_Controller
{

	public function __construct()
	{
		parent::__construct();
		if (cek_token()) {
			$this->load->model('Laporan_data_smartbin_model');
		} else {
			logout();
		}
	}


	public function index()
	{
		$model = $this->Laporan_data_smartbin_model;
		$q = urldecode($this->input->get('q', TRUE));
		$start = intval($this->input->get('start'));

		if ($q <> '') {
			$config['base_url'] = base_url() . 'laporan_data_smartbin/?q=' . urlencode($q);
			$config['first_url'] = base_url() . 'laporan_data_smartbin/?q=' . urlencode($q);
		} else {
			$config['base_url'] = base_url() . 'laporan_data_smartbin/';
			$config['first_url'] = base_url() . 'laporan_data_smartbin/';
		}

		$config['per_page'] = 10;
		$config['page_query_string'] = TRUE;
		$config['total_rows'] = $model->total_rows($q);
		$laporan_data_smartbin = $model->get_limit_data($config['per_page'], $start, $q);

		$this->load->library('pagination');
		$this->pagination->initialize($config);

		$data = array(
			'tittle'                => 'Laporan Data Smartbin',
			'judul'                 => 'Laporan Data Smartbin',
			'title_card'            => 'List Laporan Data Smartbin',
			'laporan_data_smartbin' => $laporan_data_smartbin,
			'q'                     => $q,
			'pagination'            => $this->pagination->create_links(),
			'total_rows'            => $config['total_rows'],
			'start'                 => $start,
			'menu_aktif'            => 'laporan_data_smartbin'
		);
		$res['datakonten'] = $this->load->view('laporan_data_smartbin/laporan_data_smartbin_list', $data, true);
		$this->load->view('layouts/main_view', $res);
	}
}

/* End of file Laporan_data_smartbin.php */
