<div class="row">
	<section class="col-lg connectedSortable ui-sortable">
		<div class="card card-info">
			<div class="card-header">
                <div class="card-title">
                  <?php echo $titleCard?>
                </div>
            </div>
			<div class="card-body">
					<div class="row">
						<div class="col-md-3 offset-md-9 mb-2">
                            <?= search(site_url('siswa/siswaList/'.$id_sekolah), site_url('siswa/siswaList/'.$id_sekolah), $q) ?>
						</div>
					</div>
					<div class="tab-content p-0" style="overflow:auto">
					<table class="table table-bordered table-striped table-condensed table-hover table-sm" style="margin-bottom: 10px">
						<tr>
							<th class="text-center" width="50px">No</th>
							<th class="text-center">Nama Siswa</th>
							<th class="text-center">Email</th>
							<th class="text-center">No Telp</th>
							<th class="text-center">Alamat</th>
							<th class="text-center">Aksi</th>
                        </tr>
                        <?php 
			            if($total_rows == 0){
			                echo '<tr><td colspan="10" bgcolor="grey" style="color: white; font-weight: bold; text-align: center;">Data Tidak Ditemukan</td></tr>';
			            } else {
                        foreach ($siswa as $data){?>
						<tr>
							<td class="text-center"><?php echo ++$start ?></td>
							<td><?php echo $data->nama_siswa ?></td>
							<td><?php echo $data->email_siswa ?></td>
							<td><?php echo $data->no_hp_siswa ?></td>
							<td><?php echo $data->alamat_siswa ?></td>
							<td class='text-center' width="100px">
								<div class="btn-group btn-group-sm">
                                    <button class="btn btn-<?= bgCard() ?> btn-sm" title="Lihat Detail Siswa"><i class="fas fa-info-circle"></i></button>
                                </div>
							</td>
						</tr>
                        <?php } 
                  		} ?>
					</table>
					</div>
                    <?= footer($total_rows, $pagination, '') ?>
			</div>
		</div>
	</section>
</div>