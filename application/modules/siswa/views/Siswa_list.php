<div class="row">
	<section class="col-lg connectedSortable ui-sortable">
		<div class="card card-info">
			<div class="card-header">
                <div class="card-title">
                  <?php echo $titleCard?>
                </div>
            </div>
			<div class="card-body">
					<div class="row">
						<div class="col-md-3 offset-md-9 mb-2">
                            <?= search(site_url('siswa/index'), site_url('siswa'), $q) ?>
						</div>
					</div>
					<div class="tab-content p-0" style="overflow:auto">
					<table class="table table-bordered table-striped table-condensed table-hover table-sm" style="margin-bottom: 10px">
						<tr>
							<th class="text-center" width="50px">No</th>
							<th class="text-center">Nama Sekolah</th>
							<th class="text-center">Email</th>
							<th class="text-center">No Telp</th>
							<th class="text-center">Alamat</th>
							<th class="text-center">Aksi</th>
                        </tr>
                        <?php 
			            if($total_rows == 0){
			                echo '<tr><td colspan="10" bgcolor="grey" style="color: white; font-weight: bold; text-align: center;">Data Tidak Ditemukan</td></tr>';
			            } else {
                        foreach ($sekolah as $data){?>
						<tr>
							<td class="text-center"><?php echo ++$start ?></td>
							<td><?php echo $data->nama_sekolah ?></td>
							<td><?php echo $data->email_sekolah ?></td>
							<td><?php echo $data->nomor_tlp_sekolah ?></td>
							<td><?php echo $data->alamat_sekolah ?></td>
							<td class='text-center' width="100px">
								<div class="btn-group btn-group-sm">
                                    <a href="<?= base_url('siswa/siswaList/'. $data->id_sekolah) ?>" class="btn btn-<?= bgCard() ?> btn-sm" title="Lihat Siswa"><i class="fas fa-info-circle"></i></a>
                                </div>
							</td>
						</tr>
                        <?php } 
                  		} ?>
					</table>
					</div>
                    <?= footer($total_rows, $pagination, '') ?>
			</div>
		</div>
	</section>
</div>