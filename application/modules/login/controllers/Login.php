<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Login extends MX_Controller
{


	function __construct()
	{
		parent::__construct();

		$this->load->library('form_validation');
		$this->load->model('auth');
	}

	function index()
	{
		if ($this->session->userdata('masuksuper') != TRUE) {
			$this->load->view('login');
		} else {
			redirect('dashboard', 'refresh');
		}
	}

	public function auth()
	{

		$dt = new DateTime();
		if ($this->session->userdata('masuksuper') == TRUE) {
			redirect('dashboard', 'refresh');
		} else {

			$data = [];
			$this->form_validation->set_rules('email', 'Username', 'trim|required');
			$this->form_validation->set_rules('password', 'Password', 'trim|required');
			if ($this->auth->ceklogin($this->input->post('email'), $this->input->post('password'))) {
				$email = $this->input->post('email');
				$dat = $this->db->get_where('superadmin', array('email_superadmin' => $email));

				foreach ($dat->result_array() as $key) {
					$this->session->set_userdata('uidsuper', $key['id_superadmin']);
				}
				$this->session->set_userdata('email_superadmin', $this->input->post('email'));
				$this->session->set_userdata('nama', $key['nama_superadmin']);
				$this->session->set_userdata('masuksuper', TRUE);
				$this->session->set_flashdata('msg', 'Login Berhasil Silahkan Coba lagi');
				$this->session->set_userdata('lastlog', $key['last_login']);


				$token = password_hash(md5($dt->format('YmdHis') . 'SPR'), PASSWORD_DEFAULT);
				$this->session->set_userdata('token', $token);

				$data = array(
					'last_login' => $dt->format('Y-m-d H:i:s'),
					'token' => $token,
				);
				$this->update_lastlog($this->input->post('email'), $data);
				redirect('dashboard', 'refresh');
			} else {
				
				$url = base_url();
				echo $this->session->set_flashdata('msg', '<div class="alert alert-danger" role="alert" style="text-align: center;">Email Atau Password Salah</div>');
				redirect($url);
				$data['pesan'] = "Login Gagal Silahkan Coba lagi";
			}
		}
	}

	function logout()
	{
		$this->session->sess_destroy();
		redirect('login');
	}
	function update_lastlog($email, $data)
	{
		$this->db->where('email_superadmin', $email);
		$this->db->update('superadmin', $data);
	}
}
