<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Provinsi_model extends CI_Model
{
	public $table = 'provinsi';
	function total_rows($q = NULL)
	{
		$this->db->group_start();
		$this->db->or_like("nama_provinsi", $q);
		$this->db->group_end();

		$this->db->from($this->table);
		return $this->db->count_all_results();
	}

	// get data with limit and search
	function get_limit_data($limit, $start = 0, $q = NULL)
	{
		$this->db->order_by('id_provinsi', 'asc');
		$this->db->group_start();
		$this->db->or_like("nama_provinsi", $q);
		$this->db->group_end();
		$this->db->limit($limit, $start);
		return $this->db->get($this->table)->result();
	}
}

/* End of file Biller_bank_sampah_model.php */
