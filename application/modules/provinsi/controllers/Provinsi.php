<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Provinsi extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
		if (cek_token()) {
			$this->load->model('Provinsi_model');
			$this->load->library('form_validation');
			$this->load->library('upload');
		} else {
			logout();
		}
	}

	public function index()
	{
		$model = $this->Provinsi_model;
		$q = urldecode($this->input->get('q', TRUE));
		$start = intval($this->input->get('start'));

		if ($q <> '') {
			$config['base_url'] = base_url() . 'provinsi/?q=' . urlencode($q);
			$config['first_url'] = base_url() . 'provinsi/?q=' . urlencode($q);
		} else {
			$config['base_url'] = base_url() . 'provinsi/';
			$config['first_url'] = base_url() . 'provinsi/';
		}

		$config['per_page'] = 15;
		$config['page_query_string'] = TRUE;
		$config['total_rows'] = $model->total_rows($q);
		$cek = $model->get_limit_data($config['per_page'], $start, $q);

		$this->load->library('pagination');
		$this->pagination->initialize($config);

		$data = array(
			'tittle'        => 'Provinsi',
			'judul'         => 'Provinsi',
			'title_card'    => 'Menu Provinsi',
			'provinsi' => $cek,
			'q'             => $q,
			'pagination'    => $this->pagination->create_links(),
			'total_rows'    => $config['total_rows'],
			'start'         => $start,
			'menu_aktif'	=> 'provinsi',
		);
		$res['datakonten'] = $this->load->view('provinsi/provinsi_list', $data, true);
		$this->load->view('layouts/main_view', $res);
	}
}

/* End of file provinsi.php */
