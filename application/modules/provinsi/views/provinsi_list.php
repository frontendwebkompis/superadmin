<script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>

<div class="row">
	<section class="col-lg connectedSortable ui-sortable">
		<div class="card card-info">
			<div class="card-header">
				<div class="card-title">
					<?php echo $title_card ?>
				</div>
			</div>
			<div class="card-body">
				<div class="tab-content p-0">

					<div class="row">
						<div class="col-md-3 offset-md-9 mb-2">
							<?= search(site_url('provinsi/index'), site_url('provinsi'), $q) ?>
						</div>
					</div>
					<div class="row">
						<div class="col-md-3"></div>
						<div class="col-md-4 text-center">
							<div style="margin-top: 8px" id="message">
								<?php echo $this->session->userdata('message') <> '' ? $this->session->userdata('message') : ''; ?>
							</div>
						</div>
					</div>
					<div class="tab-content p-0" style="overflow:auto">
						<table class="table table-bordered table-striped table-condensed table-hover table-sm" style="margin-bottom: 10px">

							<tr>
								<th class="text-center" width="50px">No</th>
								<th class="text-center">Nama Provinsi</th>
							
							</tr><?php
									if ($total_rows == 0) {
										echo '<tr><td colspan="8" bgcolor="grey" style="color: white; font-weight: bold; text-align: center;">Data Tidak Ditemukan</td></tr>';
									} else {
										foreach ($provinsi as $k) { ?>
									<tr>
										<td class="text-center"><?php echo ++$start ?></td>
										<td><?= $k->nama_provinsi ?></td>
									</tr>
							<?php }
									} ?>
						</table>
					</div>
					<?= footer($total_rows, $pagination, '') ?>

				</div>
			</div>
		</div>
	</section>
</div>

<script>
	function confirm(res) {
		Swal.fire({
			title: 'Anda Yakin Mau Menghapus?',
			text: "Tidak bisa dikembalikan jika sudah dihapus!",
			icon: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'Ya, Hapus!',
			cancelButtonText: 'Batal'
		}).then((result) => {
			if (result.value) {
				Swal.fire(
					'Terhapus!',
					'File sudah terhapus.',
					'success'
				)
				window.location = '<?php echo base_url() . 'provinsi/del_sem/'; ?>' + res + '?id=<?= $aktif ?>';
			}
		});
	}
</script>
