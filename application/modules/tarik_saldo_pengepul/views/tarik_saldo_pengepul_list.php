<div class="row">
    <section class="col-lg connectedSortable ui-sortable">
        <div class="card card-info">
            <div class="card-header">
                <div class="card-title">
                    <?php echo $title_card ?>
                </div>
            </div>
            <div class="card-body">
                <div class="tab-content p-0">
                    <div class="row">
                        <div class="col-md-3 offset-md-9 mb-2">
                            <?= search(site_url('tarik_saldo_pengepul/index'), site_url('tarik_saldo_pengepul'), $q) ?>
                        </div>
                    </div>
                    <div class="tab-content p-0" style="overflow:auto">
                        <table class="table table-bordered table-striped table-condensed table-hover table-sm" style="margin-bottom: 10px">
                            <tr>
                                <th class="text-center" width="80px">No</th>
                                <th class="text-center">Nama Pengepul</th>
                                <th class="text-center">Nama Bank</th>
                                <th class="text-center">Nomor Rekening</th>
                                <th class="text-center">Nama Rekening</th>
                                <th class="text-center">Tanggal</th>
                                <th class="text-center">Status</th>
                                <th class="text-center">Aksi</th>
                            </tr>
                            <?php

                            if ($total_rows == 0) {
                                echo '<tr><td colspan="8" bgcolor="grey" style="color: white; font-weight: bold; text-align: center;">Data Tidak Ditemukan</td></tr>';
                            } else {
                                foreach ($tarik_saldo_pengepul_data as $data) {
                            ?>
                                    <tr>
                                        <td class="text-center"><?php echo ++$start ?></td>
                                        <td class="text-center"><?php echo $data->nama_pengepul ?></td>
                                        <td class="text-center"><?php echo $data->nama_bank ?></td>
                                        <td class="text-center"><?php echo $data->nomer_rekening ?></td>
                                        <td class="text-center"><?php echo $data->nama_rekening ?></td>
                                        <td class="text-center"><?php echo fulldate($data->datetime) ?></td>
                                        <td class="text-center"><?php  if($data->is_read==1) echo '<span class="badge badge-success">Sudh Terkirim</span>'; else echo '<span class="badge badge-danger">Belum Terkirim</span>'; ?></td>
                                        <td class="text-center" width="100px">
                                            <?php if($data->is_read==0){ ?> 
                                            <div class="btn-group btn-group-sm">
                                                <button class="btn btn-success" title="Approve" onclick="cekAksi('<?= $data->id_tarik_saldo_pengepul ?>')"><i class="fas fa-check"></i></button>
                                            </div>
                                        <?php }else{ ?>
                                            <span class="badge badge-danger">Sudah</span>
                                        <?php 
                                        } ?>
                                        </td>
                                    </tr>
                            <?php
                                }
                            }
                            ?>
                        </table>
                    </div>
                    <?= footer($total_rows, $pagination, '') ?>
                </div>
            </div>
        </div>
    </section>
</div>

<script>
    function cekAksi(id) {
        swal({
                title: 'Approve Tarik Saldo?',
                text: "Approve tarik saldo pengepul!",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3366ff',
                cancelButtonColor: '#d33',
                cancelButtonText: 'Batal',
                confirmButtonText: 'Approve',
                closeOnConfirm: false,
            },
            function(isConfirm) {
                if (isConfirm) {
                    window.location.href = "<?php echo site_url('tarik_saldo_pengepul/tarikSaldoPengepulAction/'); ?>" + id;
                } else {

                }
            });
    }
</script>