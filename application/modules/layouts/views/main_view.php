<!DOCTYPE html>
<html>

<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<link rel="apple-touch-icon" sizes="57x57" href="<?= PATH_ASSETS ?>img/favicon.png">
	<link rel="apple-touch-icon" sizes="60x60" href="<?= PATH_ASSETS ?>img/favicon.png">
	<link rel="apple-touch-icon" sizes="72x72" href="<?= PATH_ASSETS ?>img/favicon.png">
	<link rel="apple-touch-icon" sizes="76x76" href="<?= PATH_ASSETS ?>img/favicon.png}">
	<link rel="apple-touch-icon" sizes="114x114" href="<?= PATH_ASSETS ?>img/favicon.png">
	<link rel="apple-touch-icon" sizes="120x120" href="<?= PATH_ASSETS ?>img/favicon.png">
	<link rel="apple-touch-icon" sizes="144x144" href="<?= PATH_ASSETS ?>img/favicon.png">
	<link rel="apple-touch-icon" sizes="152x152" href="<?= PATH_ASSETS ?>img/favicon.png">
	<link rel="apple-touch-icon" sizes="180x180" href="<?= PATH_ASSETS ?>img/favicon.png">
	<link rel="icon" type="image/png" sizes="192x192" href="<?= PATH_ASSETS ?>img/favicon.png">
	<link rel="icon" type="image/png" sizes="32x32" href="<?= PATH_ASSETS ?>img/favicon.png">
	<link rel="icon" type="image/png" sizes="96x96" href="<?= PATH_ASSETS ?>img/favicon.png">
	<link rel="icon" type="image/png" sizes="16x16" href="<?= PATH_ASSETS ?>img/favicon.png">
	<title><?= $tittle  ?></title>
	<!-- Tell the browser to be responsive to screen width -->
	<meta name="viewport" content="width=device-width, initial-scale=1">
	
	<link rel="stylesheet" href="<?= base_url() ?>assets/css/fontfamily.css">
	<link rel="stylesheet" href="<?= base_url() ?>assets/css/icon.css">
	<link rel="stylesheet" href="<?= base_url() ?>assets/plugins/fontawesome-free/css/all.min.css">
	<link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
	<link rel="stylesheet" href="<?= base_url() ?>assets/plugins/ekko-lightbox/ekko-lightbox.css">
	<!-- <link rel="stylesheet" href="<?= base_url() ?>assets/plugins/icheck-bootstrap/icheck-bootstrap.min.css"> -->
	<link rel="stylesheet" href="<?= base_url() ?>assets/dist/css/adminlte.min.css">
	<link rel="stylesheet" href="<?= base_url() ?>assets/plugins/overlayScrollbars/css/OverlayScrollbars.min.css">
	<!-- <link rel="stylesheet" href="<?= base_url() ?>assets/plugins/daterangepicker/daterangepicker.css"> -->
	<link rel="stylesheet" href="<?= base_url(); ?>assets/plugins/sweetalert/sweetalert.css">
	<link rel="stylesheet" href="<?= base_url() ?>assets/plugins/bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.css">
	<link href="<?= base_url(); ?>assets/plugins/select2/select2.min.css" rel="stylesheet" />
	<link rel="stylesheet" href="<?= base_url(); ?>assets/plugins/select2-bootstrap-theme-master/dist/select2-bootstrap.css">
	<!-- <link rel="stylesheet" href="<?= base_url(); ?>assets/plugins/summernote/summernote-bs4.css"> -->
	<link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
	<link rel="stylesheet" href="<?= base_url() ?>assets/css/o-style.css<?= getDateLink() ?>">
	<link rel="stylesheet" href="<?= base_url() ?>assets/plugins/magnific-popup-master/dist/magnific-popup.css">
	<!-- <link rel="stylesheet" href="<?= base_url() ?>assets/plugins/summernote/summernote-bs4.css"> -->
	<link rel="stylesheet" href="<?= base_url() ?>assets/plugins/p-loading-master/dist/css/p-loading.min.css" >

	<script src="<?= base_url() ?>assets/plugins/jquery/jquery.min.js"></script>
	<script src="<?= base_url() ?>assets/plugins/jquery-ui/jquery-ui.min.js"></script>
	<script src="<?= base_url() ?>assets/jquery.chained.js"></script>
	<script src="<?= base_url() ?>assets/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
	<script src="<?= base_url() ?>assets/plugins/chart.js/Chart.min.js"></script>
	<script src="<?= base_url() ?>assets/plugins/moment/moment.min.js"></script>
	<!-- <script src="<?= base_url() ?>assets/plugins/daterangepicker/daterangepicker.js"></script> -->
	<script src="<?= base_url() ?>assets/plugins/ekko-lightbox/ekko-lightbox.min.js"></script>
	<script src="<?= base_url() ?>assets/plugins/select2/select2.min.js"></script>
	<!-- <script src="<?= base_url() ?>assets/plugins/summernote/summernote-bs4.min.js"></script> -->
	<script src="<?= base_url() ?>assets/plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js"></script>
	<script src="<?= base_url() ?>assets/dist/js/adminlte.js"></script>
	<!-- <script src="<?= base_url() ?>assets/dist/js/demo.js"></script> -->
	<!-- <script src="<?= base_url() ?>assets/plugins/datatables/jquery.dataTables.js"></script> -->
	<!-- <script src="<?= base_url() ?>assets/plugins/datatables-bs4/js/dataTables.bootstrap4.js"></script> -->
	<!-- <script src="<?= base_url() ?>assets/plugins/summernote/summernote-bs4.min.js"></script> -->
	<script type="text/javascript" src="<?= base_url() ?>assets/plugins/p-loading-master/dist/js/p-loading.min.js"></script>
	<script src="<?= base_url() ?>assets/plugins/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js"></script>
	<script src="<?= PATH_ASSETS ?>js/o-script.js<?= getDateLink() ?>"></script>
	<script src="<?= PATH_ASSETS ?>plugins/magnific-popup-master/dist/jquery.magnific-popup.min.js"></script>
	<style>
		<?= ($this->session->userdata('darkmode') == true) ? '' : '' ?>
	</style>
</head>

<body class="hold-transition sidebar-mini layout-fixed">
	<div class="wrapper">

		<!-- Navbar -->
		<?php $this->load->view('layouts/navbar'); ?>

		<?php $this->load->view('layouts/sidebar'); ?>

		<!-- Isi -->
		<?php $this->load->view('layouts/konten'); ?>
		<!-- /.isi -->

		<!-- Footer -->
		<?php $this->load->view('layouts/footer'); ?>
		<!-- /.Footer -->
	</div>
</body>

<!-- Bootstrap 4 -->
<script src="<?= PATH_ASSETS ?>plugins/jquery-validation/jquery.validate.min.js"></script>
<script src="<?= base_url(); ?>assets/plugins/sweetalert/sweetalert.min.js"></script>
<!-- swal-ozhora -->

<script>
	<?php include 'assets/js/swal-ozhora.js'; ?>
</script>
<!-- SweetAlert Plugin Js -->



<script type="text/javascript">
	$('#darkmode').click(function() {
		var darkmode = window.localStorage.getItem('darkmode');
		var bol;
		if (darkmode == 'false') {
			bol = true;
		} else {
			bol = false;
		}
		window.localStorage.setItem("darkmode", bol);
		var darkmode = window.localStorage.getItem('darkmode');
		invert();
	});

	function invert() {
		var darkmode = window.localStorage.getItem('darkmode');
		if (darkmode == 'true') {
			$('html').css('filter', 'invert(100%)');
			$('img').css('filter', 'invert(100%)');
			// $('button').css('filter', 'invert(100%)');
			$('.badge').css('filter', 'invert(100%)');
			$('.btn').css('filter', 'invert(100%)');
			$('.small-box').css('filter', 'invert(100%)');
			$('body').css('color', 'black');
			$('aside').removeClass('sidebar-dark-primary');
			$('aside').addClass('sidebar-light-primary');
		} else {
			$('html').css('filter', 'none');
			$('img').css('filter', 'none');
			// $('button').css('filter', 'none');
			$('.badge').css('filter', 'none');
			$('.btn').css('filter', 'none');
			$('.small-box').css('filter', 'none');
			$('aside').removeClass('sidebar-light-primary');
			$('aside').addClass('sidebar-dark-primary');
		}
	}
	invert();

	$(document).on('click', '#btn-logout', function(e) {
		e.preventDefault();
		swal({
				title: 'Yakin akan keluar?',
				// text: "Data yang sudah terhapus tidak dapat dikembalikan!",
				type: 'info',
				showCancelButton: true,
				confirmButtonColor: '#3366ff',
				cancelButtonColor: '#d33',
				cancelButtonText: 'Batal',
				confirmButtonText: 'Ya',
				closeOnConfirm: false,
				// closeOnCancel: false
			},
			function(isConfirm) {
				if (isConfirm) {
					window.location.href = "<?php echo base_url('login/logout'); ?>";
				} else {}
			});
	});

	function startloading(id) {
		$(id).ploading({
			action: 'show'
		})
	}

	function stoploading(id) {
		$(id).ploading({
			action: 'hide'
		})
	}
</script>

</html>