<nav class="main-header navbar navbar-expand navbar-white navbar-light">
  <ul class="navbar-nav">
    <li class="nav-item">
      <a class="nav-link" data-widget="pushmenu" href="#"><i class="fas fa-bars"></i></a>
    </li>
  </ul>

  <ul class="navbar-nav ml-auto">
    <li class="nav-item d-sm-inline-block">
      <button type="button" id="darkmode" class="btn p-1 mr-2 text-lg text-gray" style="border: none; background: transparent;"><i class="fas fa-adjust"></i></button>
      <a href="#">
        <button id="btn-logout" class="btn p-2 mr-1 text-lg text-gray text-danger" style="border: none; background: transparent;" data-toggle="tooltip" title="Log Out"><i class="fas fa-power-off "></i></button>
      </a>
    </li>
  </ul>
</nav>