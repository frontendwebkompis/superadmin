<?php
// Program to display URL of current page. 

if (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on')
  $link = "https";
else
  $link = "http";

// Here append the common URL characters. 
$link .= "://";

// Append the host(domain name, ip) to the URL. 
$link .= $_SERVER['HTTP_HOST'];

// Append the requested resource location to the URL 
$link .= $_SERVER['REQUEST_URI'];

?>

<aside class="main-sidebar sidebar-dark-primary elevation-4 " style="margin-bottom: 400px;">
  <!-- Brand Logo -->
  <div>
    <a href="#" class="brand-link">
      <img src="<?= PATH_ASSETS ?>dist/img/kompis_white.png" alt="logo" class="brand-image">
      <span class="brand-text font-weight-light"><strong>Online</strong></span>
    </a>
  </div>

  <!-- Sidebar -->
  <div class="sidebar">
    <!-- Sidebar user panel (optional) -->
    <div class="user-panel mt-3 pb-3 mb-3 d-flex">
      <div class="image">
        <img src="<?= PATH_ASSETS ?>img/favicon.png" class="img-circle elevation-2" alt="User Image">
      </div>
      <div class="info">
        <a href="<?php echo base_url('profile/detail'); ?>" class="d-block"><?= username() ?></a>
      </div>
    </div>
    <!-- Sidebar Menu -->
    <nav class="mt-2 mb-5">
      <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
        <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
        <li class="nav-item">
          <a href="<?php echo base_url('dashboard'); ?>" class="nav-link 
                <?php if ($menu_aktif == 'dashboard') {
                  echo "active";
                } ?>
                ">
            <i class="nav-icon fas fa-home"></i>
            <p>Dashboard</p>
          </a>
        </li>
        <li class="nav-item has-treeview <?php if (($menu_aktif == 'nasabah_online') or ($menu_aktif == 'nasabah_offline') or ($menu_aktif == 'bank_sampah') or ($menu_aktif == 'sekolah') or ($menu_aktif == 'pengepul') or ($menu_aktif == 'siswa') or ($menu_aktif == 'jabatan_pengurus') or ($menu_aktif == 'jenis_tarik_saldo') or ($menu_aktif == 'bank') or ($menu_aktif == 'provinsi') or ($menu_aktif == 'kabupaten') or ($menu_aktif == 'kecamatan') or ($menu_aktif == 'metode_pembayaran') or ($menu_aktif == 'jenis_kendaraan') or ($menu_aktif == 'nasabah_online') or ($menu_aktif == 'jenjang_pendidikan') or ($menu_aktif == 'lembaga_pendidikan') or ($menu_aktif == 'ekspedisi')) {
                                            echo "menu-open";
                                          } ?>">
          <a href="#" class="nav-link 
                <?php if (($menu_aktif == 'nasabah_online') or ($menu_aktif == 'nasabah_offline') or ($menu_aktif == 'bank_sampah') or ($menu_aktif == 'sekolah') or ($menu_aktif == 'pengepul') or ($menu_aktif == 'siswa') or ($menu_aktif == 'jabatan_pengurus') or ($menu_aktif == 'jenis_tarik_saldo') or ($menu_aktif == 'bank') or ($menu_aktif == 'provinsi') or ($menu_aktif == 'kabupaten') or ($menu_aktif == 'kecamatan') or ($menu_aktif == 'metode_pembayaran') or ($menu_aktif == 'jenis_kendaraan') or ($menu_aktif == 'nasabah_online') or ($menu_aktif == 'jenjang_pendidikan') or ($menu_aktif == 'lembaga_pendidikan') or ($menu_aktif == 'ekspedisi')) {
                  echo "active";
                } ?>
                ">
            <i class="nav-icon fas fa-file-alt"></i>
            <p>
              Master
              <i class="fas fa-angle-left right"></i>
              <span class="badge badge-info right"></span>
            </p>
          </a>
          <ul class="nav nav-treeview">
            <li class="nav-item <?php if (($menu_aktif == 'nasabah_online') or ($menu_aktif == 'nasabah_offline')) {
                                  echo "menu-open";
                                } ?>">
              <a href="#" class="nav-link <?php if (($menu_aktif == 'nasabah_online') or ($menu_aktif == 'nasabah_offline')) {
                                            echo "active";
                                          } ?>">
                <i class="far fa-circle nav-icon"></i>
                <p>Nasabah
                  <i class="fas fa-angle-left right"></i>
                  <span class="badge badge-info right"></span>
                </p>
              </a>
              <ul class="nav nav-treeview">
                <li class="nav-item">
                  <a href="<?php echo base_url('nasabah_offline'); ?>" class="nav-link 
                        <?php if ($menu_aktif == 'nasabah_offline') {
                          echo "active";
                        } ?>">
                    <i class="far fa-circle nav-icon text-blue"></i>
                    <p>Nasabah Offline</p>
                  </a>
                </li>
                <li class="nav-item">
                  <a href="<?php echo base_url('nasabah_online'); ?>" class="nav-link 
                        <?php if ($menu_aktif == 'nasabah_online') {
                          echo "active";
                        } ?>">
                    <i class="far fa-circle nav-icon text-blue"></i>
                    <p>Nasabah Online</p>
                  </a>
                </li>
              </ul>
            </li>
            <li class="nav-item <?php if (($menu_aktif == 'bank_sampah') or ($menu_aktif == 'sekolah')) {
                                  echo "menu-open";
                                } ?>">
              <a href="#" class="nav-link <?php if (($menu_aktif == 'bank_sampah') or ($menu_aktif == 'sekolah')) {
                                            echo "active";
                                          } ?>">
                <i class="far fa-circle nav-icon"></i>
                <p>Bank Sampah
                  <i class="fas fa-angle-left right"></i>
                  <span class="badge badge-info right"></span>
                </p>
              </a>
              <ul class="nav nav-treeview">
                <li class="nav-item">
                  <a href="<?php echo base_url('bank_sampah'); ?>" class="nav-link 
                        <?php if ($menu_aktif == 'bank_sampah') {
                          echo "active";
                        } ?>">
                    <i class="far fa-circle nav-icon text-blue"></i>
                    <p>Bank Sampah Umum</p>
                  </a>
                </li>
                <?php if(sekolahMenuStatus()){ ?>
                  <li class="nav-item">
                    <a href="<?php echo base_url('sekolah'); ?>" class="nav-link 
                          <?php if ($menu_aktif == 'sekolah') {
                            echo "active";
                          } ?>">
                      <i class="far fa-circle nav-icon text-blue"></i>
                      <p>Bank Sampah Sekolah</p>
                    </a>
                  </li>
                <?php } ?>
              </ul>
            </li>
            <li class="nav-item">
              <a href="<?php echo base_url('pengepul'); ?>" class="nav-link 
                    <?php if ($link == base_url('pengepul') or (isset($pengepul_aktif))) {
                      echo "active";
                    } ?>">
                <i class="far fa-circle nav-icon"></i>
                <p>Pengepul</p>
              </a>
            </li>
            <?php if(sekolahMenuStatus()){ ?>
              <li class="nav-item">
                <a href="<?php echo base_url('siswa'); ?>" class="nav-link 
                      <?php if ($menu_aktif == 'siswa') {
                        echo "active";
                      } ?>">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Siswa</p>
                </a>
              </li>
            <?php } ?>
            <li class="nav-item">
              <a href="<?php echo base_url('jabatan_pengurus'); ?>" class="nav-link 
                    <?php if ($menu_aktif == 'jabatan_pengurus') {
                      echo "active";
                    } ?>">
                <i class="far fa-circle nav-icon"></i>
                <p>Jabatan</p>
              </a>
            </li>  
            <li class="nav-item">
              <a href="<?php echo base_url('request_import'); ?>" class="nav-link 
                    <?php if ($menu_aktif == 'request_import') {
                      echo "active";
                    } ?>">
                <i class="far fa-circle nav-icon"></i>
                <p>Request Import</p>
              </a>
            </li>
            <li class="nav-item">
              <a href="<?php echo base_url('jenis_tarik_saldo'); ?>" class="nav-link 
                    <?php if ($menu_aktif == 'jenis_tarik_saldo') {
                      echo "active";
                    } ?>">
                <i class="far fa-circle nav-icon"></i>
                <p>Jenis Tarik Saldo</p>
              </a>
            </li>
            <li class="nav-item">
              <a href="<?php echo base_url('bank'); ?>" class="nav-link 
                    <?php if ($menu_aktif == 'bank') {
                      echo "active";
                    } ?>">
                <i class="far fa-circle nav-icon"></i>
                <p>Bank</p>
              </a>
            </li>
            <li class="nav-item">
              <a href="<?php echo base_url('provinsi'); ?>" class="nav-link 
                    <?php if ($menu_aktif == 'provinsi') {
                      echo "active";
                    } ?>">
                <i class="far fa-circle nav-icon"></i>
                <p>Provinsi</p>
              </a>
            </li>
            <li class="nav-item">
              <a href="<?php echo base_url('kabupaten'); ?>" class="nav-link 
                    <?php if ($menu_aktif == 'kabupaten') {
                      echo "active";
                    } ?>">
                <i class="far fa-circle nav-icon"></i>
                <p>Kabupaten</p>
              </a>
            </li>
            <li class="nav-item">
              <a href="<?php echo base_url('kecamatan'); ?>" class="nav-link 
                    <?php if ($menu_aktif == 'kecamatan') {
                      echo "active";
                    } ?>">
                <i class="far fa-circle nav-icon"></i>
                <p>Kecamatan</p>
              </a>
            </li>
            <li class="nav-item">
              <a href="<?php echo base_url('metode_pembayaran'); ?>" class="nav-link 
                    <?php if ($menu_aktif == 'metode_pembayaran') {
                      echo "active";
                    } ?>">
                <i class="far fa-circle nav-icon"></i>
                <p>Metode Pembayaran</p>
              </a>
            </li>
            <li class="nav-item">
              <a href="<?php echo base_url('jenis_kendaraan'); ?>" class="nav-link 
                    <?php if ($menu_aktif == 'jenis_kendaraan') {
                      echo "active";
                    } ?>">
                <i class="far fa-circle nav-icon"></i>
                <p>Jenis Kendaraan</p>
              </a>
            </li>
            <?php if(sekolahMenuStatus()){ ?>
              <li class="nav-item">
                <a href="<?php echo base_url('jenjang_pendidikan'); ?>" class="nav-link 
                      <?php if ($menu_aktif == 'jenjang_pendidikan') {
                        echo "active";
                      } ?>">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Jenjang Pendidikan</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="<?php echo base_url('lembaga_pendidikan'); ?>" class="nav-link 
                      <?php if ($menu_aktif == 'lembaga_pendidikan') {
                        echo "active";
                      } ?>">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Lembaga Pendidikan</p>
                </a>
              </li>
            <?php } ?>
            <li class="nav-item">
              <a href="<?php echo base_url('ekspedisi'); ?>" class="nav-link 
                    <?php if ($menu_aktif == 'ekspedisi') {
                      echo "active";
                    } ?>">
                <i class="far fa-circle nav-icon"></i>
                <p>Ekspedisi</p>
              </a>
            </li>
          </ul>
        </li>
        <li class="nav-item has-treeview <?php if (($menu_aktif == 'kategori_sampah') or ($menu_aktif == 'jenis_sampah') or ($menu_aktif == 'merk_sampah') or ($menu_aktif == 'request_jenis_sampah')) {
                                            echo "menu-open";
                                          } ?>">
          <a href="#" class="nav-link <?php if (($menu_aktif == 'kategori_sampah') or ($menu_aktif == 'jenis_sampah') or ($menu_aktif == 'merk_sampah') or ($menu_aktif == 'request_jenis_sampah')) {
                                        echo "active";
                                      } ?>">
            <i class="nav-icon fas fa-copy"></i>
            <p>
              Sampah
              <i class="fas fa-angle-left right"></i>
              <span class="badge badge-info right"></span>
            </p>
          </a>
          <ul class="nav nav-treeview">
            <li class="nav-item">
              <a href="<?php echo base_url('kategori_sampah'); ?>" class="nav-link  
                    <?php if ($menu_aktif == 'kategori_sampah') {
                      echo "active";
                    } ?>">
                <i class="far fa-circle nav-icon"></i>
                <p>Kategori Sampah</p>
              </a>
            </li>
            <li class="nav-item">
              <a href="<?php echo base_url('jenis_sampah'); ?>" class="nav-link   
                    <?php if ($menu_aktif == 'jenis_sampah') {
                      echo "active";
                    } ?>">
                <i class="far fa-circle nav-icon"></i>
                <p>Jenis Sampah</p>
              </a>
            </li>
            <li class="nav-item">
              <a href="<?php echo base_url('merk_sampah'); ?>" class="nav-link    
                    <?php if ($menu_aktif == 'merk_sampah') {
                      echo "active";
                    } ?>">
                <i class="far fa-circle nav-icon"></i>
                <p>Merk Sampah</p>
              </a>
            </li>
            <li class="nav-item">
              <a href="<?php echo base_url('request_jenis_sampah'); ?>" class="nav-link    
                    <?php if ($menu_aktif == 'request_jenis_sampah') {
                      echo "active";
                    } ?>">
                <i class="far fa-circle nav-icon"></i>
                <p>Request Jenis Sampah</p>
              </a>
            </li>
          </ul>
        </li>
        <li class="nav-item has-treeview <?php if ($link == base_url('tabel_harga') or (isset($harga_nasional_aktif)) or $menu_aktif == 'tabel_harga_emas') {
                                            echo "menu-open";
                                          } ?>">
          <a href="#" class="nav-link  <?php if ($link == base_url('tabel_harga') or (isset($harga_nasional_aktif)) or $menu_aktif == 'tabel_harga_emas') {
                                          echo "active";
                                        } ?>">
            <i class="nav-icon fas fa-tag"></i>
            <p>
              Tabel Harga
              <i class="fas fa-angle-left right"></i>
              <span class="badge badge-info right"></span>
            </p>
          </a>
          <ul class="nav nav-treeview">
            <li class="nav-item">
              <a href="<?php echo base_url('tabel_harga'); ?>" class="nav-link     
                    <?php if ($link == base_url('tabel_harga') or (isset($harga_nasional_aktif))) {
                      echo "active";
                    } ?>">
                <i class="far fa-circle nav-icon"></i>
                <p>Tabel Harga Nasional</p>
              </a>
            </li>
            <li class="nav-item">
              <a href="<?php echo base_url('tabel_harga_emas'); ?>" class="nav-link     
                    <?php if ($menu_aktif == 'tabel_harga_emas') {
                      echo "active";
                    } ?>">
                <i class="far fa-circle nav-icon"></i>
                <p>Tabel Harga Emas</p>
              </a>
            </li>
          </ul>
        </li>
        <li class="nav-item">
          <a href="<?php echo base_url('list_saldo_nasabah_online'); ?>" class="nav-link     
                <?php if ($menu_aktif == 'list_saldo_nasabah_online') {
                  echo "active";
                } ?>">
            <i class="fas fa-credit-card nav-icon"></i>
            <p>Saldo Nasabah Online</p>
          </a>
        </li>
        <li class="nav-item">
          <a href="<?php echo base_url('list_saldo_nasabah_offline'); ?>" class="nav-link     
                <?php if ($menu_aktif == 'list_saldo_nasabah_offline') {
                  echo "active";
                } ?>">
            <i class="fas fa-credit-card nav-icon"></i>
            <p>Saldo Nasabah Offline</p>
          </a>
        </li>
        <li class="nav-item">
          <a href="<?php echo base_url('gudang_bank_sampah'); ?>" class="nav-link     
                <?php if ($menu_aktif == 'gudang_bank_sampah') {
                  echo "active";
                } ?>">
            <i class="fas fa-box nav-icon"></i>
            <p>Gudang Bank Sampah</p>
          </a>
        </li>
        <li class="nav-item has-treeview <?php if (($menu_aktif == 'setoran_online') or ($menu_aktif == 'setoran_offline') or ($menu_aktif == 'setoran_siswa') or ($menu_aktif == 'pembelian_pengepul')) {
                                            echo "menu-open";
                                          } ?>">
          <a href="#" class="nav-link   <?php if (($menu_aktif == 'setoran_online') or ($menu_aktif == 'setoran_offline') or ($menu_aktif == 'setoran_siswa') or ($menu_aktif == 'pembelian_pengepul')) {
                                          echo "active";
                                        } ?>">
            <i class="nav-icon fas fa-chart-pie"></i>
            <p>
              Transaksi
              <i class="right fas fa-angle-left"></i>
            </p>
          </a>
          <ul class="nav nav-treeview">
            <li class="nav-item <?php if (($menu_aktif == 'setoran_online') or ($menu_aktif == 'setoran_offline')) {
                                  echo "menu-open";
                                } ?>">
              <a href="#" class="nav-link <?php if (($menu_aktif == 'setoran_online') or ($menu_aktif == 'setoran_offline')) {
                                            echo "active";
                                          } ?>">
                <i class="far fa-circle nav-icon"></i>
                <p>Setoran Sampah
                  <i class="fas fa-angle-left right"></i>
                  <span class="badge badge-info right"></span>
                </p>
              </a>
              <ul class="nav nav-treeview">
                <li class="nav-item">
                  <a href="<?php echo base_url('setoran_online'); ?>" class="nav-link 
                        <?php if ($menu_aktif == 'setoran_online') {
                          echo "active";
                        } ?>">
                    <i class="far fa-circle nav-icon text-blue"></i>
                    <p>Setoran Online</p>
                  </a>
                </li>
                <?php if(availableMenuStatus()){ ?>
                  <li class="nav-item">
                    <a href="<?php echo base_url('setoran_offline'); ?>" class="nav-link 
                          <?php if ($menu_aktif == 'setoran_offline') {
                            echo "active";
                          } ?>">
                      <i class="far fa-circle nav-icon text-blue"></i>
                      <p>Setoran Offline</p>
                    </a>
                  </li>
                <?php } ?>
              </ul>
            </li>
            <?php if(sekolahMenuStatus()){ ?>
              <li class="nav-item">
                <a href="<?php echo base_url('setoran_siswa'); ?>" class="nav-link     
                      <?php if ($menu_aktif == 'setoran_siswa') {
                        echo "active";
                      } ?>">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Setoran Siswa</p>
                </a>
              </li>
            <?php } ?>
            <li class="nav-item">
              <a href="<?php echo base_url('pembelian_pengepul'); ?>" class="nav-link     
                    <?php if ($menu_aktif == 'pembelian_pengepul') {
                      echo "active";
                    } ?>">
                <i class="far fa-circle nav-icon"></i>
                <p>Pembelian Pengepul</p>
              </a>
            </li>
          </ul>
        </li>
        <li class="nav-item has-treeview <?php if (($menu_aktif == 'driver_bank_sampah') or ($menu_aktif == 'jadwal_penjemputan')) {
                                            echo 'menu-open';
                                          } ?>">
          <a href="#" class="nav-link     
                <?php if (($menu_aktif == 'driver_bank_sampah') or ($menu_aktif == 'jadwal_penjemputan')) {
                  echo "active";
                } ?>">
            <i class="nav-icon fas fa-truck"></i>
            <p>Jemput Sampah</p>
            <i class="fas fa-angle-left right"></i>
          </a>
          <ul class="nav nav-treeview">
            <li class="nav-item">
              <a href="<?php echo base_url('driver_bank_sampah'); ?>" class="nav-link 
                    <?php if ($menu_aktif == 'driver_bank_sampah') {
                      echo "active";
                    } ?>">
                <i class="far fa-circle nav-icon"></i>
                <p>Driver Bank Sampah</p>
              </a>
            </li>
            <li class="nav-item">
              <a href="<?php echo base_url('jadwal_penjemputan'); ?>" class="nav-link 
                    <?php if ($menu_aktif == 'jadwal_penjemputan') {
                      echo "active";
                    } ?>">
                <i class="far fa-circle nav-icon"></i>
                <p>Jadwal Penjemputan</p>
              </a>
            </li>
          </ul>
        </li>
        <li class="nav-item has-treeview <?php if (($menu_aktif == 'cara_topup') or ($menu_aktif == 'topup_nasabah') or ($menu_aktif == 'topup_pengepul')) {
                                            echo 'menu-open';
                                          } ?>">
          <a href="#" class="nav-link     
                <?php if (($menu_aktif == 'cara_topup') or ($menu_aktif == 'topup_nasabah') or ($menu_aktif == 'topup_pengepul')) {
                  echo "active";
                } ?>">
            <i class="nav-icon fas fa-hand-holding-usd"></i>
            <p>Top Up</p>
            <i class="fas fa-angle-left right"></i>
          </a>
          <ul class="nav nav-treeview">
            <li class="nav-item">
              <a href="<?php echo base_url('cara_topup'); ?>" class="nav-link 
                    <?php if ($menu_aktif == 'cara_topup') {
                      echo "active";
                    } ?>">
                <i class="far fa-circle nav-icon"></i>
                <p>Cara Top Up</p>
              </a>
            </li>
            <li class="nav-item">
              <a href="<?php echo base_url('topup_nasabah'); ?>" class="nav-link 
                    <?php if ($menu_aktif == 'topup_nasabah') {
                      echo "active";
                    } ?>">
                <i class="far fa-circle nav-icon"></i>
                <p>Top Up Nasabah</p>
              </a>
            </li>
            <li class="nav-item">
              <a href="<?php echo base_url('topup_pengepul'); ?>" class="nav-link 
                    <?php if ($menu_aktif == 'topup_pengepul') {
                      echo "active";
                    } ?>">
                <i class="far fa-circle nav-icon"></i>
                <p>Top Up Pengepul</p>
              </a>
            </li>
          </ul>
        </li>
        <li class="nav-item has-treeview <?php if (($menu_aktif == 'tarik_saldo_nasabah_online') or ($menu_aktif == 'tarik_saldo_nasabah_offline') or ($menu_aktif == 'tarik_saldo_bank_sampah') or ($menu_aktif == 'tarik_saldo_pengepul') or ($menu_aktif == 'tarik_saldo_siswa')) {
                                            echo 'menu-open';
                                          } ?>">
          <a href="#" class="nav-link     
                <?php if (($menu_aktif == 'tarik_saldo_nasabah_online') or ($menu_aktif == 'tarik_saldo_nasabah_offline') or ($menu_aktif == 'tarik_saldo_bank_sampah') or ($menu_aktif == 'tarik_saldo_pengepul') or ($menu_aktif == 'tarik_saldo_siswa')) {
                  echo "active";
                } ?>">
            <i class="nav-icon fas fa-wallet"></i>
            <p>Tarik Saldo</p>
            <i class="fas fa-angle-left right"></i>
          </a>
          <ul class="nav nav-treeview">
            <li class="nav-item  <?php if ($menu_aktif == 'tarik_saldo_nasabah_online' or $menu_aktif == 'tarik_saldo_nasabah_offline') {
                                    echo 'menu-open';
                                  } ?>">
              <a href="" class="nav-link 
                    <?php if ($menu_aktif == 'tarik_saldo_nasabah_online' or $menu_aktif == 'tarik_saldo_nasabah_offline') {
                      echo 'active';
                    } ?>">
                <i class="far fa-circle nav-icon"></i>
                <p>Nasabah</p>
                <i class="fas fa-angle-left right"></i>
              </a>
              <ul class="nav nav-treeview">
                <li class="nav-item">
                  <a href="<?php echo base_url('tarik_saldo_nasabah_offline'); ?>" class="nav-link 
                        <?php if ($menu_aktif == 'tarik_saldo_nasabah_offline') {
                          echo "active";
                        } ?>">
                    <i class="far fa-circle nav-icon text-blue"></i>
                    <p>Nasabah Offline</p>
                  </a>
                </li>
                <li class="nav-item">
                  <a href="<?php echo base_url('tarik_saldo_nasabah_online'); ?>" class="nav-link 
                        <?php if ($menu_aktif == 'tarik_saldo_nasabah_online') {
                          echo "active";
                        } ?>">
                    <i class="far fa-circle nav-icon text-blue"></i>
                    <p>Nasabah Online</p>
                  </a>
                </li>
              </ul>
            </li>
            <li class="nav-item">
              <a href="<?php echo base_url('tarik_saldo_bank_sampah'); ?>" class="nav-link 
                    <?php if ($menu_aktif == 'tarik_saldo_bank_sampah') {
                      echo "active";
                    } ?>">
                <i class="far fa-circle nav-icon"></i>
                <p>Bank Sampah</p>
              </a>
            </li>
            <li class="nav-item">
              <a href="<?php echo base_url('tarik_saldo_pengepul'); ?>" class="nav-link 
                    <?php if ($menu_aktif == 'tarik_saldo_pengepul') {
                      echo "active";
                    } ?>">
                <i class="far fa-circle nav-icon"></i>
                <p>Pengepul</p>
              </a>
            </li>
            <?php if(sekolahMenuStatus()){ ?>
              <li class="nav-item">
                <a href="<?php echo base_url('tarik_saldo_siswa'); ?>" class="nav-link 
                      <?php if ($menu_aktif == 'tarik_saldo_siswa') {
                        echo "active";
                      } ?>">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Siswa</p>
                </a>
              </li>
            <?php } ?>
          </ul>
        </li>
        <li class="nav-item has-treeview <?php if (($menu_aktif == 'biller_nasabah') or ($menu_aktif == 'biller_bank_sampah') or ($menu_aktif == 'biller_pengepul')) {
                                            echo 'menu_open';
                                          } ?>">
          <a href="#" class="nav-link     
                <?php if (($menu_aktif == 'biller_nasabah') or ($menu_aktif == 'biller_bank_sampah') or ($menu_aktif == 'biller_pengepul')) {
                  echo "active";
                } ?>">
            <i class="nav-icon fas fa-money-bill"></i>
            <p>Biller</p>
            <i class="fas fa-angle-left right"></i>
          </a>
          <ul class="nav nav-treeview">
            <li class="nav-item">
              <a href="<?php echo base_url('biller_nasabah'); ?>" class="nav-link 
                    <?php if ($menu_aktif == 'biller_nasabah') {
                      echo "active";
                    } ?>">
                <i class="far fa-circle nav-icon"></i>
                <p>Nasabah</p>
              </a>
            </li>
            <li class="nav-item">
              <a href="<?php echo base_url('biller_bank_sampah'); ?>" class="nav-link 
                    <?php if ($menu_aktif == 'biller_bank_sampah') {
                      echo "active";
                    } ?>">
                <i class="far fa-circle nav-icon"></i>
                <p>Bank Sampah</p>
              </a>
            </li>
            <li class="nav-item">
              <a href="<?php echo base_url('biller_pengepul'); ?>" class="nav-link 
                    <?php if ($menu_aktif == 'biller_pengepul') {
                      echo "active";
                    } ?>">
                <i class="far fa-circle nav-icon"></i>
                <p>Pengepul</p>
              </a>
            </li>
          </ul>
        </li>
        <li class="nav-item has-treeview <?php if (($menu_aktif == 'smartbin_counter') or ($menu_aktif == 'data_pengguna') or ($menu_aktif == 'data_sampah')) {
                                            echo 'menu-open';
                                          } ?>">
          <a href="#" class="nav-link     
                <?php if (($menu_aktif == 'smartbin_counter') or ($menu_aktif == 'data_pengguna') or ($menu_aktif == 'data_sampah')) {
                  echo "active";
                } ?>">
            <i class="nav-icon fas fa-laptop-code"></i>
            <p>Smartbin</p>
            <i class="fas fa-angle-left right"></i>
          </a>
          <ul class="nav nav-treeview">
            <li class="nav-item">
              <a href="<?php echo base_url('smartbin_counter'); ?>" class="nav-link 
                    <?php if ($menu_aktif == 'smartbin_counter') {
                      echo "active";
                    } ?>">
                <i class="far fa-circle nav-icon text-white"></i>
                <p>Smartbin Counter</p>
              </a>
            </li>
            <li class="nav-item">
              <a href="<?php echo base_url('data_pengguna'); ?>" class="nav-link 
                    <?php if ($menu_aktif == 'data_pengguna') {
                      echo "active";
                    } ?>">
                <i class="far fa-circle nav-icon text-white"></i>
                <p>Data Pengguna</p>
              </a>
            </li>
            <li class="nav-item">
              <a href="<?php echo base_url('data_sampah'); ?>" class="nav-link 
                    <?php if ($menu_aktif == 'data_sampah') {
                      echo "active";
                    } ?>">
                <i class="far fa-circle nav-icon text-white"></i>
                <p>Data Sampah</p>
              </a>
            </li>
          </ul>
        </li>
        <li class="nav-item has-treeview <?php if (($menu_aktif == 'master_reward') or ($menu_aktif == 'master_brand') or ($menu_aktif == 'redeem_bank_sampah') or ($menu_aktif == 'redeem_nasabah')) {
                                            echo "menu-open";
                                          } ?>">
          <a href="#" class="nav-link   
                <?php if (($menu_aktif == 'master_reward') or ($menu_aktif == 'master_brand') or ($menu_aktif == 'redeem_bank_sampah') or ($menu_aktif == 'redeem_nasabah')) {
                  echo "active";
                } ?>">
            <i class="nav-icon fas fa-certificate"></i>
            <p>
              Tukar Poin
              <i class="fas fa-angle-left right"></i>
            </p>
          </a>
          <ul class="nav nav-treeview">
            <li class="nav-item">
              <a href="<?php echo base_url('reward'); ?>" class="nav-link 
                    <?php if ($menu_aktif == 'master_reward') {
                      echo "active";
                    } ?>">
                <i class="far fa-circle nav-icon"></i>
                <p>Master Rewards</p>
              </a>
            </li>
            <li class="nav-item">
              <a href="<?php echo base_url('brand'); ?>" class="nav-link 
                    <?php if ($menu_aktif == 'master_brand') {
                      echo "active";
                    } ?>">
                <i class="far fa-circle nav-icon"></i>
                <p>Master Brand</p>
              </a>
            </li>
            <li class="nav-item has-treeview <?php if (($menu_aktif == 'redeem_bank_sampah') or ($menu_aktif == 'redeem_nasabah')) {
                                                echo "menu-open";
                                              } ?>">
              <a href="#" class="nav-link 
                    <?php if (($menu_aktif == 'redeem_bank_sampah') or ($menu_aktif == 'redeem_nasabah')) {
                      echo "active";
                    } ?>">
                <i class="far fa-circle nav-icon"></i>
                <p>Redeem Poin</p>
                <i class="fas fa-angle-left right"></i>
              </a>
              <ul class="nav nav-treeview">
                <li class="nav-item">
                  <a href="<?php echo base_url('redeem_nasabah'); ?>" class="nav-link 
                        <?php if ($menu_aktif == 'redeem_nasabah') {
                          echo "active";
                        } ?>">
                    <i class="far fa-circle nav-icon text-blue"></i>
                    <p>Redeem Nasabah</p>
                  </a>
                </li>
                <li class="nav-item">
                  <a href="<?php echo base_url('redeem_bank_sampah'); ?>" class="nav-link 
                        <?php if ($menu_aktif == 'redeem_bank_sampah') {
                          echo "active";
                        } ?>">
                    <i class="far fa-circle nav-icon text-blue"></i>
                    <p>Redeem Bank Sampah</p>
                  </a>
                </li>
              </ul>
            </li>
          </ul>
        </li>
        <li class="nav-item">
          <a href="<?php echo base_url('promo'); ?>" class="nav-link     
                <?php if ($link == base_url('promo') or (isset($promo_aktif))) {
                  echo "active";
                } ?>">
            <i class="nav-icon fas fa-tags"></i>
            <p>Info dan Promo</p>
          </a>
        </li>
        <?php if(availableMenuStatus()){ ?>
          <li class="nav-item">
            <a href="<?php echo base_url('setting_notifikasi'); ?>" class="nav-link     
                  <?php if ($menu_aktif == 'setting_notifikasi') {
                    echo "active";
                  } ?>">
              <i class="nav-icon fas fa-cogs"></i>
              <p>Setting Notifikasi</p>
            </a>
          </li>
        <?php } ?>
        <li class="nav-item has-treeview <?php if (($menu_aktif == 'laporan_setoran') or ($menu_aktif == 'laporan_pembelian_sampah') or ($menu_aktif == 'laporan_topup') or ($menu_aktif == 'laporan_tarik_saldo') or ($menu_aktif == 'laporan_pembelian_biller') or ($menu_aktif == 'laporan_data_smartbin')) {
                                            echo 'menu-open';
                                          } ?>">
          <a href="#" class="nav-link     
                <?php if (($menu_aktif == 'laporan_setoran') or ($menu_aktif == 'laporan_pembelian_sampah') or ($menu_aktif == 'laporan_topup') or ($menu_aktif == 'laporan_tarik_saldo') or ($menu_aktif == 'laporan_pembelian_biller') or ($menu_aktif == 'laporan_data_smartbin')) {
                  echo "active";
                } ?>">
            <i class="nav-icon fas fa-book"></i>
            <p>Laporan</p>
            <i class="fas fa-angle-left right"></i>
          </a>
          <ul class="nav nav-treeview">
            <li class="nav-item">
              <a href="<?php echo base_url('laporan_setoran'); ?>" class="nav-link 
                    <?php if ($menu_aktif == 'laporan_setoran') {
                      echo "active";
                    } ?>">
                <i class="fas fa-file-alt nav-icon"></i>
                <p>Laporan Setoran</p>
              </a>
            </li>
            <li class="nav-item">
              <a href="<?php echo base_url('laporan_pembelian_sampah'); ?>" class="nav-link 
                    <?php if ($menu_aktif == 'laporan_pembelian_sampah') {
                      echo "active";
                    } ?>">
                <i class="fas fa-file-alt nav-icon"></i>
                <p>Laporan Pembelian Sampah</p>
              </a>
            </li>
            <?php if(availableMenuStatus()){ ?>
              <li class="nav-item">
                <a href="<?php echo base_url('laporan_topup'); ?>" class="nav-link 
                      <?php if ($menu_aktif == 'laporan_topup') {
                        echo "active";
                      } ?>">
                  <i class="fas fa-file-alt nav-icon"></i>
                  <p>Laporan Top Up</p>
                </a>
              </li>
            <?php } ?>
            <li class="nav-item">
              <a href="<?php echo base_url('laporan_tarik_saldo'); ?>" class="nav-link 
                    <?php if ($menu_aktif == 'laporan_tarik_saldo') {
                      echo "active";
                    } ?>">
                <i class="fas fa-file-alt nav-icon"></i>
                <p>Laporan Tarik Saldo</p>
              </a>
            </li>
            <li class="nav-item">
              <a href="<?php echo base_url('laporan_pembelian_biller'); ?>" class="nav-link 
                    <?php if ($menu_aktif == 'laporan_pembelian_biller') {
                      echo "active";
                    } ?>">
                <i class="fas fa-file-alt nav-icon"></i>
                <p>Laporan Pembelian Biller</p>
              </a>
            </li>
            <li class="nav-item">
              <a href="<?php echo base_url('laporan_data_smartbin'); ?>" class="nav-link 
                    <?php if ($menu_aktif == 'laporan_data_smartbin') {
                      echo "active";
                    } ?>">
                <i class="fas fa-file-alt nav-icon"></i>
                <p>Laporan Data Smartbin</p>
              </a>
            </li>
          </ul>
        </li>
        <li class="nav-item">
          <a href="<?php echo base_url('bantuan'); ?>" class="nav-link
                <?php if ($link == base_url('bantuan') or (isset($bantuan_aktif))) {
                  echo "active";
                } ?>">
            <i class="nav-icon fas fa-info"></i>
            <p>Bantuan</p>
          </a>
        </li>
        <li class="nav-item">
          <a href="<?php echo base_url('versi_app'); ?>" class="nav-link
                <?php if ($menu_aktif == 'versi_aplikasi') {
                  echo "active";
                } ?>">
            <i class="nav-icon fas fa-code-branch"></i>
            <p>Versi Aplikasi</p>
          </a>
        </li>
        <li class="nav-item has-treeview <?php if (($menu_aktif == 'jam_operasional') or ($menu_aktif == 'pengaduan_layanan') or ($menu_aktif == 'bergabung_mitra')) {
                                            echo 'menu_open';
                                          } ?>">
          <a href="#" class="nav-link     
                <?php if (($menu_aktif == 'jam_operasional') or ($menu_aktif == 'pengaduan_layanan') or ($menu_aktif == 'bergabung_mitra')) {
                  echo "active";
                } ?>">
            <i class="nav-icon fas fa-info-circle"></i>
            <p>Tentang KOMPIS</p>
            <i class="fas fa-angle-left right"></i>
          </a>
          <ul class="nav nav-treeview">
            <li class="nav-item">
              <a href="<?php echo base_url('jam_operasional'); ?>" class="nav-link 
                    <?php if ($menu_aktif == 'jam_operasional') {
                      echo "active";
                    } ?>">
                <i class="far fa-circle nav-icon"></i>
                <p>Jam Operasional</p>
              </a>
            </li>
            <li class="nav-item">
              <a href="<?php echo base_url('pengaduan_layanan'); ?>" class="nav-link 
                    <?php if ($menu_aktif == 'pengaduan_layanan') {
                      echo "active";
                    } ?>">
                <i class="far fa-circle nav-icon"></i>
                <p>Pengaduan Layanan</p>
              </a>
            </li>
            <li class="nav-item">
              <a href="<?php echo base_url('bergabung_mitra'); ?>" class="nav-link 
                    <?php if ($menu_aktif == 'bergabung_mitra') {
                      echo "active";
                    } ?>">
                <i class="far fa-circle nav-icon"></i>
                <p>Bergabung Menjadi Mitra</p>
              </a>
            </li>
          </ul>
        </li>
      </ul>
    </nav>
    <!-- /.sidebar-menu -->
  </div>
  <!-- /.sidebar -->
</aside>