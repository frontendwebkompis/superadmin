<script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>

<div class="row">
	<section class="col-lg connectedSortable ui-sortable">
		<div class="card card-info">
			<div class="card-header">
				<div class="card-title">
					<?php echo $title_card ?>
				</div>
			</div>
			<div class="card-body">
				<div class="tab-content p-0">

					<div class="row">
						<div class="col-md-3 offset-md-9 mb-2">
							<?= search(site_url('smartbin_counter/index'), site_url('smartbin_counter'), $q) ?>
						</div>
					</div>
					<div class="tab-content p-0" style="overflow:auto">
						<table class="table table-bordered table-striped table-condensed table-hover table-sm" style="margin-bottom: 10px">
							<tr>
								<th class="text-center">No</th>
								<th class="text-center">ID Smartbin</th>
								<th class="text-center">Nasabah Online</th>
								<th class="text-center">Kode Smartbin</th>
								<th class="text-center">Jumlah Botol</th>
								<th class="text-center">Point</th>
							</tr>
							<?php
							if ($total_rows == 0) {
								echo '<tr><td colspan="11" bgcolor="grey" style="color: white; font-weight: bold; text-align: center;">Data Tidak Ditemukan</td></tr>';
							} else {
								foreach ($smartbin_counter as $k) { ?>
									<tr>
										<td class="text-center" width="50px"><?php echo ++$start ?></td>
										<td><?= $k->id_smartbin ?></td>
										<td><?= $k->nama_nasabah_online ?></td>
										<td><?= $k->kode_smartbin ?></td>
										<td><?= $k->jumlah_botol ?></td>
										<td><?= $k->point ?></td>
									</tr>
							<?php }
							} ?>
						</table>
					</div>
					<?= footer($total_rows, $pagination, site_url('smartbin_counter/exportxl?q=' . $q)) ?>

				</div>
			</div>
		</div>
	</section>
</div>

<script>
	function confirm(res) {
		Swal.fire({
			title: 'Anda Yakin Mau Menghapus?',
			text: "Tidak bisa dikembalikan jika sudah dihapus!",
			icon: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'Ya, Hapus!',
			cancelButtonText: 'Batal'
		}).then((result) => {
			if (result.value) {
				Swal.fire(
					'Terhapus!',
					'File sudah terhapus.',
					'success'
				)
				window.location = '<?php echo base_url() . 'smartbin_counter/del_sem/'; ?>' + res;
			}
		});
	}
</script>
