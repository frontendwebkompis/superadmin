<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Smartbin_counter extends CI_Controller
{


	public function __construct()
	{
		parent::__construct();
		if (cek_token()) {
			$this->load->model('Smartbin_counter_model');
		} else {
			logout();
		}
	}


	public function index()
	{
		$model = $this->Smartbin_counter_model;
		$q = urldecode($this->input->get('q', TRUE));
		$start = intval($this->input->get('start'));

		if ($q <> '') {
			$config['base_url'] = base_url() . 'smartbin_counter/?q=' . urlencode($q);
			$config['first_url'] = base_url() . 'smartbin_counter/?q=' . urlencode($q);
		} else {
			$config['base_url'] = base_url() . 'smartbin_counter/';
			$config['first_url'] = base_url() . 'smartbin_counter/';
		}

		$config['per_page'] = 10;
		$config['page_query_string'] = TRUE;
		$config['total_rows'] = $model->total_rows($q);
		$smartbin_counter = $model->get_limit_data($config['per_page'], $start, $q);

		$this->load->library('pagination');
		$this->pagination->initialize($config);

		$data = array(
			'tittle'                => 'Smartbin Counter',
			'judul'                 => 'Smartbin Counter',
			'title_card'            => 'List Smartbin Counter',
			'smartbin_counter' 		=> $smartbin_counter,
			'q'                     => $q,
			'pagination'            => $this->pagination->create_links(),
			'total_rows'            => $config['total_rows'],
			'start'                 => $start,
			'menu_aktif'            => 'smartbin_counter'
		);
		$res['datakonten'] = $this->load->view('smartbin_counter/smartbin_counter_list', $data, true);
		$this->load->view('layouts/main_view', $res);
	}
}

/* End of file Smartbin_counter.php */
