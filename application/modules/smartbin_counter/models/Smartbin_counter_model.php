<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Smartbin_counter_model extends CI_Model
{
	public $table = 'smartbin_counter';
	public $id = 'id_smartbin_counter';
	public $order = 'DESC';

	// get total rows
	function total_rows($q = NULL)
	{
		$this->db->join('nasabah_online', "$this->table.id_nasabah_online = nasabah_online.id_nasabah_online");
		$this->db->join('smartbin', "$this->table.id_smartbin = smartbin.id_smartbin");
		$this->db->join('brand', "smartbin.id_brand = brand.id_brand");

		$this->db->where('status_smartbin_counter_remove', 1);
		$this->db->where('status_smartbin_counter_aktif', 1);
		$this->db->where('status_smartbin_remove', 1);
		$this->db->where('status_smartbin_aktif', 1);
		$this->db->group_start();
		$this->db->like("$this->table.id_smartbin", $q);
		$this->db->or_like("nasabah_online.nama_nasabah_online", $q);
		$this->db->or_like("$this->table.kode_smartbin", $q);
		$this->db->or_like("$this->table.jumlah_botol", $q);
		$this->db->or_like("$this->table.point", $q);
		$this->db->group_end();
		$this->db->from($this->table);
		return $this->db->count_all_results();
	}

	// get data with limit and search
	function get_limit_data($limit, $start = 0, $q = NULL)
	{
		$this->db->order_by($this->id, $this->order);
		$this->db->join('nasabah_online', "$this->table.id_nasabah_online = nasabah_online.id_nasabah_online");
		$this->db->join('smartbin', "$this->table.id_smartbin = smartbin.id_smartbin");
		$this->db->join('brand', "smartbin.id_brand = brand.id_brand");

		$this->db->where('status_smartbin_counter_remove', 1);
		$this->db->where('status_smartbin_counter_aktif', 1);
		$this->db->where('status_smartbin_remove', 1);
		$this->db->where('status_smartbin_aktif', 1);
		$this->db->group_start();
		$this->db->like("$this->table.id_smartbin", $q);
		$this->db->or_like("nasabah_online.nama_nasabah_online", $q);
		$this->db->or_like("$this->table.kode_smartbin", $q);
		$this->db->or_like("$this->table.jumlah_botol", $q);
		$this->db->or_like("$this->table.point", $q);
		$this->db->group_end();
		$this->db->limit($limit, $start);
		return $this->db->get($this->table)->result();
	}
}

/* End of file Smartbin_counter_model.php */
