<div class="row">
	<section class="col-lg connectedSortable ui-sortable">
		<div class="card card-info">
			<div class="card-header">
                <div class="card-title">
                  <?php echo $titleCard?>
                </div>
            </div>
			<div class="card-body">
					<div class="row">
						<div class="col-md-3 offset-md-9 mb-2">
                            <?= search(site_url('jenjang/index'), site_url('jenjang'), $q) ?>
						</div>
					</div>
					<div class="tab-content p-0" style="overflow:auto">
					<table class="table table-bordered table-striped table-condensed table-hover table-sm" style="margin-bottom: 10px">
						<tr>
							<th class="text-center" width="50px">No</th>
							<th class="text-center">Nama Jenjang Pendidikan</th>
							<th class="text-center">Status</th>
							<th class="text-center">Aksi</th>
                        </tr>
                        <?php 
			            if($total_rows == 0){
			                echo '<tr><td colspan="10" bgcolor="grey" style="color: white; font-weight: bold; text-align: center;">Data Tidak Ditemukan</td></tr>';
			            } else {
                        foreach ($jenjang as $data){?>
						<tr>
							<td class="text-center"><?php echo ++$start ?></td>
							<td><?php echo $data->nama_jenjang_pendidikan ?></td>
							<td class='text-center'><?php if($data->status_jenjang_pendidikan_aktif == 1){
                                echo '<span class="badge badge-success">Aktif</span>';
                            } else {
                                echo '<span class="badge badge-danger">Non-aktif</span>';
                            } ?></td>
							<td class='text-center' width="100px">
								<div class="btn-group btn-group-sm">
                                    <button class="btn btn-danger btn-sm" title="Hapus Jenjang"><i class="fas fa-trash"></i></button>
                                </div>
							</td>
						</tr>
                        <?php } 
                  		} ?>
					</table>
					</div>
                    <?= footer($total_rows, $pagination, '') ?>
			</div>
		</div>
	</section>
</div>