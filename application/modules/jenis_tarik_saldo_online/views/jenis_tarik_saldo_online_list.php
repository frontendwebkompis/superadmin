<script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>

<div class="row">
	<!-- Left col -->
	<section class="col-lg connectedSortable ui-sortable">
		<!-- Custom tabs (Charts with tabs)-->
		<div class="card card-info">
            <div class="card-header">
                <div class="card-title">
                  <?php echo $title_card?>
                </div>
              </div>
			<div class="card-body">
				<div class="tab-content p-0">

        <div class="row" style="margin-bottom: 10px">
            <div class="col-md-4">
                <?php echo anchor(site_url('jenis_tarik_saldo_online/create'),'Tambah Data', 'class="btn btn-info btn-sm"'); ?>
            </div>
            <div class="col-md-4 text-center">
                <div style="margin-top: 8px" id="message">
                    <?php echo $this->session->userdata('message') <> '' ? $this->session->userdata('message') : ''; ?>
                </div>
            </div>

            <div class="col-md-4 text-right">
                <form action="<?php echo site_url('jenis_tarik_saldo_online/index'); ?>" class="form-inline float-right" method="get">
                    <div class="input-group">
                        <input type="text" class="form-control-sm" placeholder="Cari Saldo Online" name="q" value="<?php echo $q; ?>">
                        <span class="input-group-btn">
                            <?php if ($q <> '') { ?>
                                <a href="<?php echo site_url('jenis_tarik_saldo_online'); ?>" class="btn btn-default btn-sm"><i class="fas fa-redo"></i></a>
                                <?php } ?>
                          <button class="btn btn-info btn-sm" type="submit"><i class="fas fa-search"></i></button>
                        </span>
                    </div>
                </form>
            </div>
        </div>
        <div class="tab-content p-0" style="overflow:auto">
        <table class="table table-bordered table-striped table-condensed table-hover table-sm" style="margin-bottom: 10px">
            <tr>
                <th class="text-center" width="50px">No</th>
                <th class="text-center">Jenis Tarik Saldo Online</th>
                <th class="text-center">Aksi</th>
            </tr>

            <?php
                if($total_rows == 0){
                    echo '<tr><td colspan="8" bgcolor="grey" style="color: white; font-weight: bold; text-align: center;">Data Tidak Ditemukan</td></tr>';
                } else {
            ?>

            <?php foreach ($jenis_tarik_saldo_online_data as $jenis_tarik_saldo_online) { ?>
            <tr>
                <td style="text-align:center"><?php echo ++$start ?></td>
                <td><?php echo $jenis_tarik_saldo_online->nama_jenis_tarik_saldo_online ?></td>
                <td style="text-align:center" width="150px">
                    <div class="btn-group">
                        <a href="<?php echo site_url('jenis_tarik_saldo_online/update/'.$jenis_tarik_saldo_online->id_jenis_tarik_saldo_online); ?>"
                        data-toogle="tooltip" title="Update">
                        <button type="button" class="btn btn-success btn-sm"><i class="far fa-edit"></i></button></a>
                        <a href="#" class="btn btn-danger btn-sm" data-toggle="tooltip" data-placement="top" title="Hapus" onclick="confirm('<?= $jenis_tarik_saldo_online->id_jenis_tarik_saldo_online ?>')" ><i class="fas fa-trash-alt"></i></a>
                    </div>
                </td>
		    </tr>
            <?php } 
            }?>
        </table>
        </div>
        <?= footer($total_rows, $pagination, '') ?>
        </div>
			</div>
		</div>
	</section>
</div>

<script>
function confirm(res) {
    Swal.fire({
    title: 'Anda Yakin Mau Menghapus?',
    text: "Tidak bisa dikembalikan jika sudah dihapus!",
    icon: 'warning',
    showCancelButton: true,
    confirmButtonColor: '#3085d6',
    cancelButtonColor: '#d33',
    confirmButtonText: 'Ya, Hapus!',
    cancelButtonText: 'Batal'
    }).then((result) => {
    if (result.value) {
        Swal.fire(
        'Terhapus!',
        'File sudah terhapus.',
        'success'
        )
        window.location='<?php echo base_url().'jenis_tarik_saldo_online/del_sem/'; ?>'+res;
    }
    });
}
</script>