<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Jenis_tarik_saldo_online extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        if (cek_token()) {
            $this->load->model('Jenis_tarik_saldo_online_model');
            $this->load->library('form_validation');
        } else {
            logout();
        }
    }

    public function index()
    {
        $q = urldecode($this->input->get('q', TRUE));
        $start = intval($this->input->get('start'));

        if ($q <> '') {
            $config['base_url'] = base_url() . 'jenis_tarik_saldo_online?q=' . urlencode($q);
            $config['first_url'] = base_url() . 'jenis_tarik_saldo_online?q=' . urlencode($q);
        } else {
            $config['base_url'] = base_url() . 'jenis_tarik_saldo_online';
            $config['first_url'] = base_url() . 'jenis_tarik_saldo_online';
        }

        $config['per_page'] = 10;
        $config['page_query_string'] = TRUE;
        $config['total_rows'] = $this->Jenis_tarik_saldo_online_model->total_rows($q);
        $jenis_tarik_saldo_online = $this->Jenis_tarik_saldo_online_model->get_limit_data($config['per_page'], $start, $q);

        $this->load->library('pagination');
        $this->pagination->initialize($config);

        $data = array(
            'tittle'                        => 'Tarik Saldo Online',
            'judul'                         => 'Tarik Saldo Online',
            'title_card'                   => 'List Jenis Tarik Saldo',
            'jenis_tarik_saldo_online_data' => $jenis_tarik_saldo_online,
            'q'                             => $q,
            'pagination'                    => $this->pagination->create_links(),
            'total_rows'                    => $config['total_rows'],
            'start'                         => $start,
            'tarik_saldo_online_aktif'      => '1',
        );
        $res['datakonten'] = $this->load->view('jenis_tarik_saldo_online/jenis_tarik_saldo_online_list', $data, true);
        $this->load->view('layouts/main_view', $res);
    }

    public function read($id)
    {
        $row = $this->Jenis_tarik_saldo_online_model->get_by_id($id);
        if ($row) {
            $data = array(
                'tittle'                        => 'Tarik Saldo Online',
                'judul'                         => 'Tarik Saldo Online',
                'title_card'                    => 'Jenis Tarik Saldo',
                'id_jenis_tarik_saldo_online'   => $row->id_jenis_tarik_saldo_online,
                'nama_jenis_tarik_saldo_online' => $row->nama_jenis_tarik_saldo_online,
                'tarik_saldo_online_aktif'      => '1',
            );
            $res['datakonten'] = $this->load->view('jenis_tarik_saldo_online/jenis_tarik_saldo_online_read', $data, true);
            $this->load->view('layouts/main_view', $res);
        } else {
            $this->session->set_flashdata('gagal', 'Data Tidak Di Temukan');
            redirect(site_url('jenis_tarik_saldo_online'));
        }
    }

    public function create()
    {
        $data = array(
            'tittle'                        => 'Tarik Saldo Online',
            'judul'                         => 'Tarik Saldo Online',
            'title_card'                   => 'Tambah Tarik Saldo Online',
            'button'                        => 'Create',
            'action'                        => site_url('jenis_tarik_saldo_online/create_action'),
            'id_jenis_tarik_saldo_online'   => set_value('id_jenis_tarik_saldo_online'),
            'nama_jenis_tarik_saldo_online' => set_value('nama_jenis_tarik_saldo_online'),
            'tarik_saldo_online_aktif'      => '1',
        );
        $res['datakonten'] = $this->load->view('jenis_tarik_saldo_online/jenis_tarik_saldo_online_form', $data, true);
        $this->load->view('layouts/main_view', $res);
    }

    public function create_action()
    {
        $this->_rules();
        $prefix = "JTS1";
        // echo $gabung;
        $row = $this->Jenis_tarik_saldo_online_model->get_by_prefix($prefix);
        $NoUrut = (int) substr($row->max, 4, 4);
        $NoUrut = $NoUrut + 1; //nomor urut +1
        $NoUrut = sprintf('%04d', $NoUrut);
        $fix = $prefix . $NoUrut;
        if ($this->form_validation->run() == FALSE) {
            $this->create();
        } else {
            $data = array(
                'id_jenis_tarik_saldo_online' => $fix,
                'nama_jenis_tarik_saldo_online' => $this->input->post('nama_jenis_tarik_saldo_online', TRUE),
                'id_superadmin' => $this->session->userdata('uid'),
                'status_jenis_tarik_saldo_online_remove' => 1,
            );
            $this->Jenis_tarik_saldo_online_model->insert($data);
            $this->session->set_flashdata('berhasil', 'Berhasil Menambahkan Data');
            redirect(site_url('jenis_tarik_saldo_online'));
        }
    }

    public function update($id)
    {
        $row = $this->Jenis_tarik_saldo_online_model->get_by_id($id);

        if ($row) {
            $data = array(
                'tittle'                        => 'Jenis Tarik Saldo',
                'title_card'                    => 'Jenis Tarik Saldo',
                'judul'                         => 'Jenis Tarik Saldo Online Form',
                'button'                        => 'Update',
                'action'                        => site_url('jenis_tarik_saldo_online/update_action'),
                'id_jenis_tarik_saldo_online'   => set_value('id_jenis_tarik_saldo_online', $row->id_jenis_tarik_saldo_online),
                'nama_jenis_tarik_saldo_online' => set_value('nama_jenis_tarik_saldo_online', $row->nama_jenis_tarik_saldo_online),
                'tarik_saldo_online_aktif'      => '1',
            );
            $res['datakonten'] = $this->load->view('jenis_tarik_saldo_online/jenis_tarik_saldo_online_form', $data, true);
            $this->load->view('layouts/main_view', $res);
        } else {
            $this->session->set_flashdata('gagal', 'Data Tidak Ditemukan');
            redirect(site_url('jenis_tarik_saldo_online'));
        }
    }

    public function update_action()
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->update($this->input->post('id_jenis_tarik_saldo_online', TRUE));
        } else {
            $data = array(
                'nama_jenis_tarik_saldo_online' => $this->input->post('nama_jenis_tarik_saldo_online', TRUE),
                'id_superadmin' => $this->session->userdata('uid'),
            );

            $this->Jenis_tarik_saldo_online_model->update($this->input->post('id_jenis_tarik_saldo_online', TRUE), $data);
            $this->session->set_flashdata('berhasil', 'Berhasil Merubah Data');
            redirect(site_url('jenis_tarik_saldo_online'));
        }
    }

    public function del_sem($id)
    {
        $row = $this->Jenis_tarik_saldo_online_model->get_by_id($id);

        if ($row) {
            $data = array(
                'id_superadmin' => $this->session->userdata('uid'),
                'status_jenis_tarik_saldo_online_remove' => 0,
            );

            $this->Jenis_tarik_saldo_online_model->update($id, $data);
            $this->session->set_flashdata('berhasil', 'Berhasil Menghapus Data');
            redirect(site_url('jenis_tarik_saldo_online'));
        } else {
            $this->session->set_flashdata('gagal', 'Data Tidak Ditemukan');
            redirect(site_url('jenis_tarik_saldo_online'));
        }
    }
    // public function delete($id) 
    // {
    //     $row = $this->Jenis_tarik_saldo_online_model->get_by_id($id);

    //     if ($row) {
    //         $this->Jenis_tarik_saldo_online_model->delete($id);
    //         $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">Berhasil Menghapus Data</div>');
    //         redirect(site_url('jenis_tarik_saldo_online'));
    //     } else {
    //         $this->session->set_flashdata('message', '<div class="alert alert-warning" role="alert">Data Tidak Ditemukan</div>');
    //         redirect(site_url('jenis_tarik_saldo_online'));
    //     }
    // }

    public function _rules()
    {
        $this->form_validation->set_rules('nama_jenis_tarik_saldo_online', 'nama jenis tarik saldo online', 'trim|required');

        $this->form_validation->set_rules('id_jenis_tarik_saldo_online', 'id_jenis_tarik_saldo_online', 'trim');
        $this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

    public function excel()
    {
        $this->load->helper('exportexcel');
        $namaFile = "jenis_tarik_saldo_online.xls";
        $judul = "jenis_tarik_saldo_online";
        $tablehead = 0;
        $tablebody = 1;
        $nourut = 1;
        //penulisan header
        header("Pragma: public");
        header("Expires: 0");
        header("Cache-Control: must-revalidate, post-check=0,pre-check=0");
        header("Content-Type: application/force-download");
        header("Content-Type: application/octet-stream");
        header("Content-Type: application/download");
        header("Content-Disposition: attachment;filename=" . $namaFile . "");
        header("Content-Transfer-Encoding: binary ");

        xlsBOF();

        $kolomhead = 0;
        xlsWriteLabel($tablehead, $kolomhead++, "No");
        xlsWriteLabel($tablehead, $kolomhead++, "Nama Jenis Tarik Saldo Online");

        foreach ($this->Jenis_tarik_saldo_online_model->get_all() as $data) {
            $kolombody = 0;

            //ubah xlsWriteLabel menjadi xlsWriteNumber untuk kolom numeric
            xlsWriteNumber($tablebody, $kolombody++, $nourut);
            xlsWriteLabel($tablebody, $kolombody++, $data->nama_jenis_tarik_saldo_online);

            $tablebody++;
            $nourut++;
        }

        xlsEOF();
        exit();
    }
}

/* End of file Jenis_tarik_saldo_online.php */
/* Location: ./application/controllers/Jenis_tarik_saldo_online.php */
/* Please DO NOT modify this information : */
/* Generated by Harviacode Codeigniter CRUD Generator 2020-02-10 04:37:38 */
/* http://harviacode.com */
