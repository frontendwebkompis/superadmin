<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Tarik_saldo_siswa extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        if (cek_token()) {
        $this->load->model('Tarik_saldo_siswa_model');
        $this->load->library('form_validation');
    } else {
            logout();
        }
    }

    public function index(){
        $q = urldecode($this->input->get('q', TRUE));
        $start = intval($this->input->get('start'));
        
        if ($q <> '') {
            $config['base_url']     = base_url() . 'tarik_saldo_siswa?q=' . urlencode($q);
            $config['first_url']    = base_url() . 'tarik_saldo_siswa?q=' . urlencode($q);
        } else {
            $config['base_url']     = base_url() . 'tarik_saldo_siswa';
            $config['first_url']    = base_url() . 'tarik_saldo_siswa';
        }

        $config['per_page']             = 10;
        $config['page_query_string']    = TRUE;
        $config['total_rows']           = $this->Tarik_saldo_siswa_model->total_rows($q);
        $tarik_saldo_siswa              = $this->Tarik_saldo_siswa_model->get_limit_data($config['per_page'], $start, $q);
        // echo '<pre>'; print_r($tarik_saldo_siswa); echo '</pre>';

        $this->load->library('pagination');
        $this->pagination->initialize($config);

        $data = array(
            'titleCard'                         => 'List Tarik Saldo Siswa',
            'tittle'                            => 'Tarik Saldo Siswa',
            'judul'                             => 'Tarik Saldo Siswa',
            'breadcrumbactive'                  => '',
            'breadcrumb'                        => 'Tarik Saldo Siswa',
            'link'                              => '',
            'tarik_saldo_siswa_data'            => $tarik_saldo_siswa,
            'q'                                 => $q,
            'pagination'                        => $this->pagination->create_links(),
            'total_rows'                        => $config['total_rows'],
            'start'                             => $start,
            'action'                            => site_url('tarik_saldo_siswa/create_action'),
            'kategori_aktif'                    => '1',
            'menu_aktif'                        => 'tarik_saldo_siswa'
        );
        $data['datakonten'] = $this->load->view('Tarik_saldo_siswa_list', $data, true);
        $this->load->view('layouts/main_view', $data);
    }

    // public function jadwal($id){
    //     $this->db->where(array('md5(id_bank_sampah) =' => $id));
    //     $data = $this->db->get('bank_sampah')->row();
    //     if($data){
    //         $this->session->set_flashdata('gagal', 'Fitur belum tersedia');
    //         redirect('jadwal_penjemputan');
    //         // $q      = urldecode($this->input->get('q', TRUE));
    //         // $start  = intval($this->input->get('start'));
            
    //         // if ($q <> '') {
    //         //     $config['base_url'] = base_url() . 'jadwal_penjemputan/driver/'. $id .'?q=' . urlencode($q);
    //         //     $config['first_url'] = base_url() . 'jadwal_penjemputan/driver/'. $id .'?q=' . urlencode($q);
    //         // } else {
    //         //     $config['base_url'] = base_url() . 'jadwal_penjemputan/driver/'. $id .'';
    //         //     $config['first_url'] = base_url() . 'jadwal_penjemputan/driver/'. $id .'';
    //         // }

    //         // $config['per_page']             = 10;
    //         // $config['page_query_string']    = TRUE;
    //         // $config['total_rows']           = $this->Jadwal_pemjemputan_model->total_rows_driver($id, $q);
    //         // $jadwal_penjemputan             = $this->Jadwal_pemjemputan_model->get_limit_data_driver($id, $config['per_page'], $start, $q);

    //         // $this->load->library('pagination');
    //         // $this->pagination->initialize($config);

    //         // $data = array(
    //         //     'titleCard'                 => 'List Driver Bank Sampah',
    //         //     'tittle'                    => 'Driver Bank Sampah',
    //         //     'judul'                     => 'Driver Bank Sampah',
    //         //     'breadcrumbactive'          => '',
    //         //     'breadcrumb'                => 'Driver Bank Sampah',
    //         //     'link'                      => '',
    //         //     'jadwal_penjemputan_data'   => $jadwal_penjemputan,
    //         //     'q'                         => $q,
    //         //     'pagination'                => $this->pagination->create_links(),
    //         //     'total_rows'                => $config['total_rows'],
    //         //     'start'                     => $start,
    //         //     'action'                    => site_url('jadwal_penjemputan/create_action'),
    //         //     'kategori_aktif'            => '1',
    //         //     'menu_aktif'                => 'jadwal_penjemputan',
    //         //     'id_bank_sampah'            => $id,

    //         // );
    //         // $data['datakonten'] = $this->load->view('Jadwal_pemjemputan', $data, true);
    //         // $this->load->view('layouts/main_view', $data);
    //     } else {
    //         $this->session->set_flashdata('gagal', 'Data tidak ditemukan');
    //         redirect('gudang_bank_sampah');
    //     }
    // }
}

/* End of file versi_app.php */
/* Location: ./application/controllers/versi_app.php */
/* Please DO NOT modify this information : */
/* Generated by Harviacode Codeigniter CRUD Generator 2020-07-15 09:43:37 */
/* http://harviacode.com */