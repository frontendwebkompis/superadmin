<div class="col-md-5 mb-2">
<div class="tab-content p-0" style="overflow:auto">
<div class="btn-group">
    <?php 
        if($tombol_aktif == 'bank_sampah'){
            echo anchor(site_url('bank_sampah'),'Bank Sampah', 'class="btn btn-info btn-sm" data-toggle="tooltip" data-placement="top" title="List Bank Sampah"'); 
        } else {
            echo anchor(site_url('bank_sampah'),'Bank Sampah', 'class="btn btn-default btn-sm" data-toggle="tooltip" data-placement="top" title="List Bank Sampah"'); 
        } 
    ?>
    <?php 
        if($tombol_aktif == 'approve'){
            echo anchor(site_url('bank_sampah/approve'),'Approve', 'class="btn btn-success btn-sm" data-toggle="tooltip" data-placement="top" title="List Perlu Approve"'); 
        } else {
            echo anchor(site_url('bank_sampah/approve'),'Approve', 'class="btn btn-default btn-sm" data-toggle="tooltip" data-placement="top" title="List Perlu Approve"'); 
        }
    ?>
    <?php 
        if($tombol_aktif == 'berkas'){
            echo anchor(site_url('bank_sampah/berkas'),'Berkas', 'class="btn btn-secondary btn-sm" data-toggle="tooltip" data-placement="top" title="List Belum Upload Berkas"'); 
        } else {
            echo anchor(site_url('bank_sampah/berkas'),'Berkas', 'class="btn btn-default btn-sm" data-toggle="tooltip" data-placement="top" title="List Belum Upload Berkas"'); 
        }
    ?>
    <?php 
        if($tombol_aktif == 'premium'){
            echo anchor(site_url('bank_sampah/premium'),'Premium', 'class="btn btn-warning btn-sm" data-toggle="tooltip" data-placement="top" title="List Perlu Premium"'); 
        } else {
            echo anchor(site_url('bank_sampah/premium'),'Premium', 'class="btn btn-default btn-sm" data-toggle="tooltip" data-placement="top" title="List Perlu Premium"'); 
        }
        ?>
    <?php 
        if($tombol_aktif == 'banned'){
            echo anchor(site_url('bank_sampah/banned'),'Blokir', 'class="btn btn-danger btn-sm" data-toggle="tooltip" data-placement="top" title="List Blokir"'); 
        } else {
            echo anchor(site_url('bank_sampah/banned'),'Blokir', 'class="btn btn-default btn-sm" data-toggle="tooltip" data-placement="top" title="List Blokir"'); 
        }
    ?>
</div>
</div>
</div>