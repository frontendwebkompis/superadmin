<script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>

<div class="row">
	<!-- Left col -->
	<section class="col-lg connectedSortable ui-sortable">
		<!-- Custom tabs (Charts with tabs)-->
		<div class="card card-info">
            <div class="card-header">
                <div class="card-title">
                  <?php echo $title_card?>
                </div>
            </div>
			<div class="card-body">
				<div class="tab-content p-0">
					
					<table class="table table-bordered table-striped table-condensed table-sm" style="margin-bottom: 20px">
						<tr>	
							<td class="font-weight-bold">Status Bank Sampah</td>
							<td><?php echo $status_bank_sampah;?></td>
						</tr>
						<tr>
							<td class="font-weight-bold">Nama Bank Sampah</td>
							<td><?php echo $nama_bank_sampah; ?></td>
						</tr>
						<tr>
							<td class="font-weight-bold">Alamat Bank Sampah</td>
							<td><?php echo $alamat_bank_sampah; ?></td>
						</tr>
                        <tr>
							<td class="font-weight-bold">Website Bank Sampah</td>
							<td><?php if ($website_bank_sampah == null){
                                echo '<span class="badge btn-danger">Kosong</span>';
                            } else {
                             echo $website_bank_sampah;   
                            } ?></td>
						</tr>
                        <tr>
                            <td class="font-weight-bold">Provinsi</td>
                            <td><?php echo $nama_provinsi; ?></td>
                        </tr>
                        <tr>
                            <td class="font-weight-bold">Kabupaten</td>
                            <td><?php echo $nama_kabupaten; ?></td>
                        </tr>
                        <tr>
                            <td class="font-weight-bold">Kecamatan</td>
                            <td><?php echo $nama_kecamatan; ?></td>
                        </tr>
						<tr>
							<td class="font-weight-bold">Last Login</td>
							<td><?php echo $last_login; ?></td>
						</tr>
					</table>
                    <div class="form-group">
                            <label for="exampleFormControlSelect1">Alasan Blokir</label>
                            <select class="form-control">
                            <option value="">Pilih Alasan Blokir</option>
                            <?php foreach ($reason as $res): ?>
                            	 <option value="<?php echo $res->id_reason; ?>"><?php echo $res->desc_reason; ?></option>
                            <?php endforeach ?>
                            </select>
                        </div>
					<div >
						<a href="<?php echo site_url('bank_sampah') ?>" class="btn btn-primary btn-sm"  style="width: 100px;">Batal</a></td>
                        <a href="#" class="btn btn-danger btn-sm" data-toggle="tooltip" data-placement="top" title="Non Aktif" 
                        onclick="confirm('<?= $id_bank_sampah ?>')"  style="width: 100px;">Blokir <i class="fas fa-power-off"></i></a>
                        <!-- <a href="#" class="btn btn-danger btn-sm" data-toggle="tooltip" data-placement="top" title="Non Aktif" 
                        onclick="confirm('<?=  $id_bank_sampah ?>')" > BLokir</a> -->
					</div>

				</div>
			</div><!-- /.card-body -->
		</div>
		<!-- /.card -->
	</section>
	<!-- /.Left col -->
</div>



<script>
function confirm(res) {
    Swal.fire({
    title: 'Anda Yakin Mau Blokir Bank Sampah?',
    text: "Akun akan segera di Blokir!",
    icon: 'warning',
    showCancelButton: true,
    confirmButtonColor: '#3085d6',
    cancelButtonColor: '#d33',
    confirmButtonText: 'Ya, Blokir!',
    cancelButtonText: 'Batal'
    }).then((result) => {
    if (result.value) {
        Swal.fire(
        'Terblokir!',
        'Akun sudah diblokir.',
        'success'
        )
        window.location='<?php echo base_url().'bank_sampah/ubahbanned/'; ?>'+res;
    }
    });
}
</script>