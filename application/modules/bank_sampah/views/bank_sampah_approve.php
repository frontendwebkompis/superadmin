<script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>

<div class="row">
	<section class="col-lg connectedSortable ui-sortable">
		<div class="card card-info">
			<div class="card-header">
                <div class="card-title">
                  <?php echo $title_card?>
                </div>
            </div>
			<div class="card-body">
				<div class="row">
						
						<?php $this->load->view('bank_sampah/tombol_atas'); ?>

						<div class="col-md-3 offset-md-4 mb-2">
							<?= search(site_url('bank_sampah/approve'), site_url('bank_sampah/approve'), $q) ?>
						</div>
					</div>

					<div class="tab-content p-0" style="overflow:auto">
					<table class="table table-bordered table-striped table-condensed table-hover table-sm" style="margin-bottom: 10px">
						<tr>
							<th class="text-center" width="20px">No</th>
							<th class="text-center">Nama</th>
							<th class="text-center">Alamat</th>
							<th class="text-center">Provinsi</th>
							<th class="text-center">Kabupaten</th>
							<th class="text-center">Kecamatan</th>
							<th class="text-center">Tanggal Join</th>
							<th class="text-center">Status</th>
							<th class="text-center">Aksi</th>
						</tr>
						<?php 
			            if($total_rows == 0){
			                echo '<tr><td colspan="10" bgcolor="grey" style="color: white; font-weight: bold; text-align: center;">Data Tidak Ditemukan</td></tr>';
			            } else {
						foreach ($bank_sampah_data as $bank_sampah) { ?>
							<tr>
								<td class="text-center"><?php echo ++$start ?></td>
								<td><?php echo $bank_sampah->nama_bank_sampah ?></td>
								<td><?php echo $bank_sampah->alamat_bank_sampah ?></td>
								<td><?php echo $bank_sampah->nama_provinsi ?></td>
								<td><?php echo $bank_sampah->nama_kabupaten ?></td>
								<td><?php echo $bank_sampah->nama_kecamatan ?></td>
								<td><?php echo fulldate($bank_sampah->tgl_daftar) ?></td>
								<td class="text-center">
									<?php if($bank_sampah->ket_status_bank_sampah == '0'){
										echo '<span class="badge badge-danger">Non Aktif</span>';
									} else {
										echo '<span class="badge badge-success">Belum Verifikasi Email</span>';
									}
									?>
								</td>
								<td class="text-center" width="50px">
								<div class="btn-group">
									<a href="#" class="btn btn-success btn-sm" data-toggle="tooltip" data-placement="top" title="Approve" 
									onclick="confirm('<?= $bank_sampah->id_bank_sampah ?>')" ><i class="fas fa-check fa-sm"></i></a>
								</div>
							</td>
							</tr>
						<?php }
						} ?>
					</table>
					</div>
					<?= footer($total_rows, $pagination, site_url('bank_sampah/exportxlapprove?q=' . $q)) ?>
			</div>
		</div>
	</section>
</div>

<script>
function confirm(res) {
    Swal.fire({
    title: 'Anda Yakin Mau Approve Bank Sampah?',
    text: "Akan segera di Approve!",
    icon: 'warning',
    showCancelButton: true,
    confirmButtonColor: '#3085d6',
    cancelButtonColor: '#d33',
    confirmButtonText: 'Ya, Approve!',
    cancelButtonText: 'Batal'
    }).then((result) => {
    if (result.value) {
        Swal.fire(
        'Aktif!',
        'Bank Sampah sudah di Approve.',
        'success'
        )
        window.location='<?php echo base_url().'bank_sampah/ubahapprove/'; ?>'+res;
    }
    });
}
</script>