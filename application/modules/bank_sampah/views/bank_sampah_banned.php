<script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>

<div class="row">
	<!-- Left col -->
	<section class="col-lg connectedSortable ui-sortable">
		<!-- Custom tabs (Charts with tabs)-->
		<div class="card card-info">
			<div class="card-header">
                <div class="card-title">
                  <?php echo $title_card?>
                </div>
            </div>
			<div class="card-body">

					<div class="row" style="margin-bottom: 10px">

						<?php $this->load->view('bank_sampah/tombol_atas'); ?>

						<div class="col-md-3 offset-md-4 mb-2">
							<?= search(site_url('bank_sampah/banned'), site_url('bank_sampah/banned'), $q) ?>
						</div>
					</div>
					<div class="tab-content p-0" style="overflow:auto">
					<table class="table table-bordered table-striped table-condensed table-hover table-sm" style="margin-bottom: 10px">
							<tr>
								<th class="text-center" width="20px">No</th>
								<th class="text-center">Nama</th>
								<th class="text-center">Provinsi</th>
								<th class="text-center">Kabupaten</th>
								<th class="text-center">Kecamatan</th>
								<th class="text-center">Email</th>
								<th class="text-center">No Hp</th>
								<th class="text-center">Last Login</th>
								<th class="text-center">Tanggal Join</th>
								<th class="text-center">Alasan Banned</th>
								<th class="text-center" width="100px">Aksi</th>
							</tr><?php 
				            if($total_rows == 0){
				                echo '<tr><td colspan="11" bgcolor="grey" style="color: white; font-weight: bold; text-align: center;">Data Tidak Ditemukan</td></tr>';
				            } else {
							foreach ($bank_sampah_data as $bank_sampah) { ?>
								<tr>
									<td class="text-center"><?php echo ++$start ?></td>
									<td><?php echo $bank_sampah->nama_bank_sampah ?></td>
									<td><?php echo $bank_sampah->nama_provinsi ?></td>
									<td><?php echo $bank_sampah->nama_kabupaten ?></td>
									<td><?php echo $bank_sampah->nama_kecamatan ?></td>
									<td><?php echo $bank_sampah->email_bank_sampah ?></td>
									<td class="text-center"><?php echo $bank_sampah->no_hp_bank_sampah ?></td>
									<td class="text-center"><?php if(empty($bank_sampah->last_login)){
										echo '<span class="badge badge-danger">Kosong</span>';
									} else {
										echo fulldate($bank_sampah->last_login);
									} ?></td>
									<td class="text-center"><?php echo fulldate($bank_sampah->tgl_daftar) ?></td>
									<td class="text-center"></td>
									<td class="text-center">
										<div class="btn-group">
											<a href="<?php echo site_url('bank_sampah/detail/' . $bank_sampah->id_bank_sampah); ?>" data-toogle="tooltip" title="Lihat Detail dan Ubah Premium">
											<button type="button" class="btn btn-info btn-sm"><i class="fas fa-info-circle"></i></button></a>
											<a href="#" class="btn btn-success btn-sm" data-toggle="tooltip" data-placement="top" title="Non Aktif" 
											onclick="confirm('<?=  $bank_sampah->id_bank_sampah ?>')" ><i class="fas fa-power-off"></i></a>
										</div>
									</td>
								</tr>
							<?php } 
							} ?>
						</table>
					</div>
						<?= footer($total_rows, $pagination, site_url('bank_sampah/exportxlbanned?q=' . $q)) ?>
				</div>
			</div>
	</section>
</div>

<script>
function confirm(res) {
    Swal.fire({
    title: 'Anda Yakin Mau Unblokir Bank Sampah?',
    text: "Akun akan segera di Unblokir!",
    icon: 'warning',
    showCancelButton: true,
    confirmButtonColor: '#3085d6',
    cancelButtonColor: '#d33',
    confirmButtonText: 'Ya, Aktifkan!',
    cancelButtonText: 'Batal'
    }).then((result) => {
    if (result.value) {
        Swal.fire(
        'Aktif!',
        'Akun sudah di Unblokir.',
        'success'
        )
        window.location='<?php echo base_url().'bank_sampah/ubahstatus/'; ?>'+res;
    }
    });
}
</script>

