<script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>

<div class="row">
	<!-- Left col -->
	<section class="col-lg connectedSortable ui-sortable">
		<!-- Custom tabs (Charts with tabs)-->
		<div class="card card-info">
            <div class="card-header">
                <div class="card-title">
                  <?php echo $title_card?>
                </div>
            </div>
			<div class="card-body bg-light">
				<div class="tab-content p-0" style="overflow: auto">


		<div class="card card-secondary card-tabs">
            <div class="card-header p-0 pt-1">
              <ul class="nav nav-tabs" id="custom-tabs-one-tab" role="tablist">
                <li class="nav-item">
                  <a class="nav-link active" id="custom-tabs-one-home-tab" data-toggle="pill" href="#custom-tabs-one-home" role="tab" aria-controls="custom-tabs-one-home" aria-selected="true">Detail</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" id="custom-tabs-one-messages-tab" data-toggle="pill" href="#custom-tabs-one-messages" role="tab" aria-controls="custom-tabs-one-messages" aria-selected="false">Penanggung Jawab</a>
                </li>
              </ul>
            </div>

            <div class="card-body">
              <div class="tab-content" id="custom-tabs-one-tabContent">
                <div class="tab-pane fade show active" id="custom-tabs-one-home" role="tabpanel" aria-labelledby="custom-tabs-one-home-tab">
                  	<div class="row">
                      <div class="col-md-8">
                      <table class="table  table-condensed table-striped table-sm">
                        <tr><td class="font-weight-bold">Status Bank Sampah</td>
                          <td><?php echo '<span class="badge badge-success">'.$ket_status_bank_sampah.'</span>'; ?></td>
                        </tr>

                        <tr><td class="font-weight-bold">Nama Bank Sampah</td><td><?php echo $nama_bank_sampah; ?></td></tr>
                        <tr><td class="font-weight-bold">Alamat Bank Sampah</td><td><?php echo $alamat_bank_sampah; ?></td></tr>
                        <tr><td class="font-weight-bold">Provinsi</td><td><?php echo $nama_provinsi; ?></td></tr>
                        <tr><td class="font-weight-bold">Kabupaten</td><td><?php echo $nama_kabupaten; ?></td></tr>
                        <tr><td class="font-weight-bold">Kecamatan</td><td><?php echo $nama_kecamatan; ?></td></tr>
                        <tr><td class="font-weight-bold">Tanggal Join</td><td><?php echo fulldate($tgl_daftar); ?></td></tr>
                        <tr><td class="font-weight-bold">Last Login</td><td><?php echo fulldate($last_login); ?></td></tr>
                        <tr><td class="font-weight-bold">Lat Bank Sampah</td><td><?php echo $lat_bank_sampah; ?></td></tr>
                        <tr><td class="font-weight-bold">Long Bank Sampah</td><td><?php echo $long_bank_sampah; ?></td></tr>
                        <tr><td class="font-weight-bold">Website Bank Sampah</td><td><?php if($website_bank_sampah == null){
                        echo '<span class="badge badge-danger">Kosong</span>';
                      } else { echo $website_bank_sampah; } ?></td></tr>
                      <tr><td class="font-weight-bold">Saldo Bank Sampah</td><td><?php if($saldo_bank_sampah == null){
                          echo '<span class="badge badge-danger">Kosong</span>';
                        } else {
                        echo rupiah($saldo_bank_sampah);
                        }
                         ?></td></tr>
                        <tr><td class="font-weight-bold">Point Bank Sampah</td><td><?php if($point_bank_sampah == null){
                          echo '<span class="badge badge-danger">Kosong</span>';
                        } else {
                        echo $point_bank_sampah; 
                        }
                        ?></td></tr>
                      </table>
                      </div>
                      <div class="col-md-4">

                          <ul class="list-group">
                            <li class="list-group-item">
                              <div class="text-center font-weight-bold">
                                SK Bank Sampah
                              </div>
                              <div class="text-center">
                          <?php
                            if($sk_bank_sampah == null){
                              echo '<span class="badge badge-danger">Kosong</span>';
                            } else { ?>
                          <?php  if(substr($sk_bank_sampah,-4) == '.pdf'){?> 
                                    <a href="<?php echo base_url(); ?>profile/get_by_sk?img=profile/berkas/sk_bank_sampah/<?php echo $id_bank_sampah.'/'.basename($sk_bank_sampah) ?>" >
                                    <button type="button" class="btn btn-primary btn-sm" style="width: 200px;"><i class="fas fa-download"></i> Download SK</button></a>
                                <?php }else{ ?>
                            <a href="<?= base_url()?>bank_sampah/get_by_sk?img=profile/berkas/sk_bank_sampah/<?php echo $id_bank_sampah.'/'.basename($sk_bank_sampah) ?>" data-toggle="lightbox" data-max-width="800" data-title="SK Bank Sampah">
                              <img src="<?= base_url()?>bank_sampah/get_by_sk?img=profile/berkas/sk_bank_sampah/<?php echo $id_bank_sampah.'/'.basename($sk_bank_sampah) ?>" class="img-fluid" style="width: 250px;" alt="Sk Bank Sampah">
                            </a>
                       <?php }?>

                          <?php
                            }
                          ?>
                              </div>
                            </li>
                            <li class="list-group-item">
                              <div class="text-center font-weight-bold">
                                Foto Bangunan Bank Sampah
                              </div>
                              <div class="text-center">
                                <?php
                                if($foto_bangunan_bank_sampah == null){
                                  echo '<span class="badge badge-danger">Kosong</span>';
                                } else { ?>
                                <a href="<?= base_url()?>bank_sampah/get_by_bangunan?img=profile/berkas/foto_bangunan_bank_sampah/<?php echo $id_bank_sampah.'/'.basename($foto_bangunan_bank_sampah) ?>" data-toggle="lightbox" data-max-width="800" data-title="Foto Bangunan Bank Sampah">
                                  <img src="<?= base_url()?>bank_sampah/get_by_bangunan?img=profile/berkas/foto_bangunan_bank_sampah/<?php echo $id_bank_sampah.'/'.basename($foto_bangunan_bank_sampah) ?>" class="img-fluid" style="width: 250px;" alt="Foto Bangunan Bank Sampah">
                                </a>
                              <?php  }
                              ?>
                              </div>
                            </li>
                          </ul>
                        </div>
                      </div>
                    </div>
                <div class="tab-pane fade" id="custom-tabs-one-messages" role="tabpanel" aria-labelledby="custom-tabs-one-messages-tab">
                  	<div class="row">
                     <div class="col-md-8">
                      <table class="table  table-condensed table-striped">

                        <tr>
                          <td class="font-weight-bold">Alamat Penanggung Jawab</td>
                          <td><?php if($alamat_penanggungjawab == null){
                            echo '<span class="badge badge-danger">Kosong</span>';
                          } else {
                            echo $alamat_penanggungjawab;
                          }  ?></td>
                        </tr>
                        <tr>
                          <td class="font-weight-bold">No HP Penanggung Jawab</td>
                          <td><?php if($no_hp_penanggungjawab == null){
                            echo '<span class="badge badge-danger">Kosong</span>';
                          } else {
                            echo $no_hp_penanggungjawab;
                          }  ?></td>
                        </tr>
                        <tr>
                          <td class="font-weight-bold">Email Penanggung jawab</td>
                          <td><?php if($email_penanggungjawab == null){
                            echo '<span class="badge badge-danger">Kosong</span>';
                          } else {
                            echo $email_penanggungjawab; 
                          }
                          ?></td>
                        </tr>

                      <tr><td class="font-weight-bold">Nama Penanggung Jawab</td><td><?php if($nama_penanggungjawab == null){
                          echo '<span class="badge badge-danger">Kosong</span>';
                      } else {
                        echo $nama_penanggungjawab;
                      }  ?></td></tr>
                      <tr><td class="font-weight-bold">No KTP Penanggung Jawab</td><td><?php if($no_ktp_penanggungjawab == null){
                        echo '<span class="badge badge-danger">Kosong</span>';
                      } else {
                       echo $no_ktp_penanggungjawab; 
                      } ?>   <button id="cekktp" onclick="cekktpasli('<?= $no_ktp_penanggungjawab ?>','<?= $nama_penanggungjawab ?>')" class="btn-sm btn-primary" 
                        href=""
                        >Cek Ktp validasi ktp</button></td></tr>
                      

                    </table>
                     </div> 
                     <div class="col-md-4">

                        <ul class="list-group">
                          <li class="list-group-item">
                            <div class="text-center font-weight-bold">Foto KTP Penanggung Jawab</div>
                            <div class="text-center">
                              <?php if($foto_ktp_penanggungjawab == null){
                                echo '<span class="badge badge-danger">Kosong</span>';
                              } else { ?>
                                <a href="<?= base_url()?>bank_sampah/get_image?img=profile/penanggungjawab/foto_ktp_penanggungjawab/<?php echo $id_bank_sampah.'/'.basename($foto_ktp_penanggungjawab) ?>" data-toggle="lightbox" data-max-width="800" data-title="Foto KTP Penanggung Jawab">
                                <img src="<?= base_url()?>bank_sampah/get_image?img=profile/penanggungjawab/foto_ktp_penanggungjawab/<?php echo $id_bank_sampah.'/'.basename($foto_ktp_penanggungjawab) ?>" class="img-fluid" style="width: 250px;" alt="Foto KTP Penanggung Jawab">
                                </a>
                              <?php } ?>
                            </div>
                          </li>
                          <li class="list-group-item">
                            <div class="text-center font-weight-bold">Selfie KTP Penanggung Jawab</div>
                            <div class="text-center">
                              <?php
                            if($selfie_ktp_penanggungjawab == null){
                              echo '<span class="badge badge-danger">Kosong</span>';
                            } else { ?>
                                <a href="<?= base_url()?>bank_sampah/get_image_selfi?img=profile/penanggungjawab/selfie_ktp_penanggungjawab/<?php echo $id_bank_sampah.'/'.basename($selfie_ktp_penanggungjawab) ?>" data-toggle="lightbox" data-max-width="800" data-title="Selfie KTP Penanggung Jawab">
                                <img src="<?= base_url()?>bank_sampah/get_image_selfi?img=profile/penanggungjawab/selfie_ktp_penanggungjawab/<?php echo $id_bank_sampah.'/'.basename($selfie_ktp_penanggungjawab) ?>" class="img-fluid" style="width: 250px;" alt="Selfie KTP Penanggung Jawab">
                                </a>
                           <?php }
                          ?>
                            </div>
                          </li>
                        </ul>
                     </div> 
                    </div>
                    
                </div>
              </div>
            </div>
		  </div>
		  	<div class="btn-group">
			 	<a href="<?php echo site_url('bank_sampah') ?>" data-toogle="tooltip" title="Kembali">
				<button type="button" class="btn btn-primary btn-sm"><i class="far fa-arrow-alt-circle-left"> Batal</i></button></a>
				<!-- <a href="<?php echo site_url('bank_sampah/ubahpremium/') ?>" class="btn btn-warning">Jadikan Premium</a> -->
				<?php if($id_status_bank_sampah!=4){
				 if($id_status_bank_sampah!=3 && $id_status_bank_sampah!=1){ ?>
				<a href="#" class="btn btn-warning btn-sm" data-toggle="tooltip" data-placement="top" title="Jadikan Premium" 
        onclick="premium('<?=  $id_bank_sampah ?>')" >Premium <i class="fas fa-info-circle"></i></a>
			
			<?php } ?>
      <a href="<?php echo site_url('bank_sampah/blokir/'.$id_bank_sampah); ?>"
      data-toogle="tooltip" title="Lihat Detail">
      <button type="button" class="btn btn-danger btn-sm">Blokir <i class="fas fa-power-off"></i></button></a>

			</div>
			<?php }else{ ?>
				<a href="#" class="btn btn-success btn-sm" data-toggle="tooltip" data-placement="top" title="Approve" 
				onclick="unblokir('<?= $id_bank_sampah ?>')" >Aktifkan <i class="fas fa-check"></i></a>
		
				
			<?php } ?>
      <?php if($id_status_bank_sampah==2){ ?>
  <button type="button" class="btn btn-default" data-toggle="modal" data-target="#modal-default">
                  Tolak Berkas
                </button>
 <?php } ?>
				</div>

			</div><!-- /.card-body -->
		</div>
		<!-- /.card -->
	</section>
	<!-- /.Left col -->
</div>

 <div class="modal fade" id="modal-default">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <h4 class="modal-title">Tolak Berkas</h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
              <form method="post" action="<?php echo site_url('bank_sampah/tolak_berkas')?>" >
    <div class="modal-body">
           
       <div class="form-group">
                            <label for="exampleFormControlSelect1">Alasan Tolak</label>
                            <select name="reason" class="form-control">
                            <option value="">Pilih Alasan Tolak</option>
                            <?php foreach ($reason as $res): ?>
                               <option value="<?php echo $res->id_reason; ?>"><?php echo $res->desc_reason; ?></option>
                            <?php endforeach ?>
                            </select>
                        </div>
                        <input type="hidden" name="id_bank_sampah" value="<?= $id_bank_sampah?>">
                       
                   </div>
            <div class="modal-footer justify-content-between">
              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
             <button class="btn btn-danger">Tolak Berkas</button>
            </div>
                </form>
           
          </div>
          <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
      </div>
      <!-- /.modal -->

<!-- Ekko Lightbox -->
<script src="<?= base_url('assets/plugins/ekko-lightbox/ekko-lightbox.min.js') ?>"></script>
<script>
  $(function () {
    function cekktpasli(noktp,nama) {
     // alert(''+noktp+'  '+nama);
      $.ajax({
                url: '<?php echo base_url() ?>bank_sampah/cek_ktp_asli?ktp='+noktp+'&nama='+nama,
                  dataType: "json",
                cache: false,
                success: function(res) {
                  if(res.message=='failed'){
                     alert('Nama dan NIK tidak Sesuai');
                  }else{
                     alert('Nama dan NIK Sesuai');
                  }
                }
              });
  }
    $(document).on('click', '[data-toggle="lightbox"]', function(event) {
      event.preventDefault();
      $(this).ekkoLightbox({
        alwaysShowClose: true
      });
    });

    $('.filter-container').filterizr({gutterPixels: 3});
    $('.btn[data-filter]').on('click', function() {
    $('.btn[data-filter]').removeClass('active');
    $(this).addClass('active');
    });
  })
</script>

<script>
function confirm(res) {
    Swal.fire({
    title: 'Anda Yakin Mau Blokir Bank Sampah?',
    text: "Akun akan segera di Blokir!",
    icon: 'warning',
    showCancelButton: true,
    confirmButtonColor: '#3085d6',
    cancelButtonColor: '#d33',
    confirmButtonText: 'Ya, Blokir!',
    cancelButtonText: 'Batal'
    }).then((result) => {
    if (result.value) {
        Swal.fire(
        'Terblokir!',
        'Akun sudah diblokir.',
        'success'
        )
        window.location='<?php echo base_url().'bank_sampah/ubahbanned/'; ?>'+res;
    }
    });
}

function premium(res) {
    Swal.fire({
    title: 'Anda Yakin mau Premium Akun?',
    text: "Akan segera di Premium kan!",
    icon: 'warning',
    showCancelButton: true,
    confirmButtonColor: '#3085d6',
    cancelButtonColor: '#d33',
    confirmButtonText: 'Ya, Aktifkan!',
    cancelButtonText: 'Batal'
    }).then((result) => {
    if (result.value) {
        Swal.fire(
        'Premium!',
        'Akun sudah Premium.',
        'success'
        )
        window.location='<?php echo base_url().'bank_sampah/ubahpremium/'; ?>'+res;
    }
    });
}

function unblokir(res) {
    Swal.fire({
    title: 'Anda Yakin Mau Unblokir Bank Sampah?',
    text: "Akun akan segera di Unblokir!",
    icon: 'warning',
    showCancelButton: true,
    confirmButtonColor: '#3085d6',
    cancelButtonColor: '#d33',
    confirmButtonText: 'Ya, Aktifkan!',
    cancelButtonText: 'Batal'
    }).then((result) => {
    if (result.value) {
        Swal.fire(
        'Aktif!',
        'Akun sudah di Unblokir.',
        'success'
        )
        window.location='<?php echo base_url().'bank_sampah/ubahstatus/'; ?>'+res;
    }
    });

}
</script>

