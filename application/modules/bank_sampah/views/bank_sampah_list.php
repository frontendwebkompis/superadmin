<script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>

<div class="row">
	<section class="col-lg connectedSortable ui-sortable">
		<div class="card card-info">
			<div class="card-header">
                <div class="card-title">
                  <?php echo $title_card?>
                </div>
            </div>
			<div class="card-body">
					<div class="row">
						
						<?php $this->load->view('bank_sampah/tombol_atas'); ?>
						
						<div class="col-md-3 offset-md-4 mb-2">
							<?= search(site_url('bank_sampah/index'), site_url('bank_sampah'), $q) ?>
						</div>
					</div>
					<div class="tab-content p-0" style="overflow:auto">
					<table class="table table-bordered table-striped table-condensed table-hover table-sm" style="margin-bottom: 10px">
						<tr>
							<th class="text-center" width="50px">No</th>
							<th class="text-center">Nama</th>
							<th class="text-center">Alamat</th>
							<th class="text-center">Email</th>
							<th class="text-center">No Hp</th>
							<th class="text-center">Tanggal Join</th>
							<th class="text-center">Status</th>
							<th class="text-center">Aksi</th>
                        </tr>
                        <?php 
			            if($total_rows == 0){
			                echo '<tr><td colspan="10" bgcolor="grey" style="color: white; font-weight: bold; text-align: center;">Data Tidak Ditemukan</td></tr>';
			            } else {
                        foreach ($bank_sampah_data as $bank_sampah){?>
						<tr>
							<td class="text-center"><?php echo ++$start ?></td>
							<td><?php echo $bank_sampah->nama_bank_sampah ?></td>
							<td><?php echo $bank_sampah->alamat_bank_sampah ?></td>
							<td><?php echo $bank_sampah->email_bank_sampah ?></td>
							<td><?php echo $bank_sampah->no_hp_bank_sampah ?></td>
							<td><?php echo fulldate($bank_sampah->tgl_daftar) ?></td>
							<td class="text-center">
								<?php if($bank_sampah->ket_status_bank_sampah == '0'){
									echo '<span class="badge badge-danger">Non Aktif</span>';
								} else {
									echo '<span class="badge badge-primary">Sudah Verifikasi(Premium)</span>';
								}
								?>
							</td>
							<td class="text-center" width="100px">
								<div class="btn-group">
									<a href="<?php echo site_url('bank_sampah/detail/'.$bank_sampah->id_bank_sampah); ?>"
									data-toogle="tooltip" title="Lihat Detail">
									<button type="button" class="btn btn-info btn-sm"><i class="fas fa-info-circle"></i></button></a>
									<a href="<?php echo site_url('bank_sampah/blokir/'.$bank_sampah->id_bank_sampah); ?>"
									data-toogle="tooltip" title="Lihat Detail">
									<button type="button" class="btn btn-danger btn-sm"><i class="fas fa-power-off"></i></button></a>
								</div>
							</td>
						</tr>
                        <?php } 
                  		} ?>
					</table>
					</div>
					<?= footer($total_rows, $pagination, site_url('bank_sampah/exportxl?q=' . $q)) ?>
			</div>
		</div>
	</section>
</div>


<script>
function confirm(res) {
    Swal.fire({
    title: 'Anda Yakin Mau Blokir Bank Sampah?',
    text: "Akun akan segera di Blokir!",
    icon: 'warning',
    showCancelButton: true,
    confirmButtonColor: '#3085d6',
    cancelButtonColor: '#d33',
    confirmButtonText: 'Ya, Blokir!',
    cancelButtonText: 'Batal'
    }).then((result) => {
    if (result.value) {
        Swal.fire(
        'Terblokir!',
        'Akun sudah diblokir.',
        'success'
        )
        window.location='<?php echo base_url().'bank_sampah/ubahbanned/'; ?>'+res;
    }
    });
}
</script>