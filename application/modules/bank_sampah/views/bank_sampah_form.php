<div class="row">
	<!-- Left col -->
	<section class="col-lg connectedSortable ui-sortable">
		<!-- Custom tabs (Charts with tabs)-->
		<div class="card">
			<div class="card-body">
				<div class="tab-content p-0">

    <form action="<?php echo $action; ?>" method="post">
        <div class="form-group">
            <label for="varchar">Status Bank Sampah <?php echo form_error('id_status_bank_sampah') ?></label>
            <select id="id_status_bank_sampah" class="form-control" name="id_status_bank_sampah">
                <option value="">Pilih Kategori</option>
                <?php
                $status_bank_sampah = $this->db->get('status_bank_sampah')->result();
                foreach ($status_bank_sampah as $stat) { ?>
                <option  <?=  $stat->ket_status_bank_sampah ==  $id_status_bank_sampah ? 'selected' : ''; ?> value="<?php echo $stat->id_status_bank_sampah; ?>">
                <?php echo $stat->ket_status_bank_sampah; ?>
                </option>
                <?php }
                ?>
            </select>
        </div>
	    <!-- <div class="form-group">
            <label for="varchar">Id Status Bank Sampah <?php echo form_error('id_status_bank_sampah') ?></label>
            <input type="text" class="form-control" name="id_status_bank_sampah" id="id_status_bank_sampah" placeholder="Id Status Bank Sampah" value="<?php echo $id_status_bank_sampah; ?>" />
        </div> -->
	    <div class="form-group">
            <label for="varchar">Nama Bank Sampah <?php echo form_error('nama_bank_sampah') ?></label>
            <input type="text" class="form-control" name="nama_bank_sampah" id="nama_bank_sampah" placeholder="Nama Bank Sampah" value="<?php echo $nama_bank_sampah; ?>" />
        </div>
	    <div class="form-group">
            <label for="varchar">Alamat Bank Sampah <?php echo form_error('alamat_bank_sampah') ?></label>
           <textarea name="alamat_bank_sampah" id="alamat_bank_sampah" class="form-control" cols="30" rows="10"><?php echo $alamat_bank_sampah; ?></textarea>
        </div>
	    <div class="form-group">
            <label for="int">Provinsi <?php echo form_error('id_provinsi') ?></label>
            <select id="id_provinsi" class="form-control" name="id_provinsi">
            <option value="">Pilih Provinsi</option>
            <?php foreach ($provinsi as $prov){ ?>
            <option <?=  $prov->id_provinsi ==  $id_provinsi ? 'selected' : ''; ?> value="<?php echo $prov->id_provinsi; ?>">
            <?php echo $prov->nama_provinsi; ?>
            </option><?php }?>
            </select>  
        </div>
	    <div class="form-group">
            <label for="int">Kabupaten <?php echo form_error('id_kabupaten') ?></label>
            <select id="id_kabupaten" class="form-control" name="id_kabupaten">
            <option value="">Pilih Kabupaten</option>
            <?php foreach ($kabupaten as $kab) { ?>
            <option data-chained="<?php echo $kab->id_provinsi; ?>" <?=  $kab->id_kabupaten ==  $id_kabupaten ? 'selected' : ''; ?> value="<?php echo $kab->id_kabupaten; ?>">
            <?php echo $kab->nama_kabupaten; ?>
            </option>
            <?php }?>
            </select>       
        </div>
	    <div class="form-group">
            <label for="int">Kecamatan <?php echo form_error('id_kecamatan') ?></label>
            <select id="id_kecamatan" class="form-control" name="id_kecamatan">
            <option value="">Pilih Kecamatan</option>
            <?php foreach ($kecamatan as $kec) { ?>
            <option data-chained="<?php echo $kec->id_kabupaten; ?>" <?=  $kec->id_kecamatan ==  $id_kecamatan ? 'selected' : ''; ?> value="<?php echo $kec->id_kecamatan; ?>">
            <?php echo $kec->nama_kecamatan; ?>
            </option>
            <?php }?>
            </select>      
        </div>
	    <div class="form-group">
            <label for="decimal">Lat Bank Sampah <?php echo form_error('lat_bank_sampah') ?></label>
            <input type="text" class="form-control" name="lat_bank_sampah" id="lat_bank_sampah" placeholder="Lat Bank Sampah" value="<?php echo $lat_bank_sampah; ?>" />
        </div>
	    <div class="form-group">
            <label for="decimal">Long Bank Sampah <?php echo form_error('long_bank_sampah') ?></label>
            <input type="text" class="form-control" name="long_bank_sampah" id="long_bank_sampah" placeholder="Long Bank Sampah" value="<?php echo $long_bank_sampah; ?>" />
        </div>
	    <div class="form-group">
            <label for="varchar">Email Bank Sampah <?php echo form_error('email_bank_sampah') ?></label>
            <input type="text" class="form-control" name="email_bank_sampah" id="email_bank_sampah" placeholder="Email Bank Sampah" value="<?php echo $email_bank_sampah; ?>" />
        </div>
	    <div class="form-group">
            <label for="varchar">No Hp Bank Sampah <?php echo form_error('no_hp_bank_sampah') ?></label>
            <input type="text" onkeypress="return Angkasaja(event)" maxlength="13" minlength="10" class="form-control" name="no_hp_bank_sampah" id="no_hp_bank_sampah" placeholder="No Hp Bank Sampah" value="<?php echo $no_hp_bank_sampah; ?>" />
        </div>
	    <!-- <div class="form-group">
            <label for="varchar">Password Bank Sampah <?php echo form_error('password_bank_sampah') ?></label>
            <input type="text" class="form-control" name="password_bank_sampah" id="password_bank_sampah" placeholder="Password Bank Sampah" value="<?php echo $password_bank_sampah; ?>" />
        </div> -->
	    <div class="form-group">
            <label for="varchar">Kode Verifikasi Bank Sampah <?php echo form_error('kode_verifikasi_bank_sampah') ?></label>
            <input type="text" onkeypress="return Angkasaja(event)" maxlength="6" minlength="6" class="form-control" name="kode_verifikasi_bank_sampah" id="kode_verifikasi_bank_sampah" placeholder="Kode Verifikasi Bank Sampah" value="<?php echo $kode_verifikasi_bank_sampah; ?>" />
        </div>
	    <!-- <div class="form-group">
            <label for="datetime">Last Login <?php echo form_error('last_login') ?></label>
            <input type="text" class="form-control" name="last_login" id="last_login" placeholder="Last Login" value="<?php echo $last_login; ?>" />
        </div> -->
	    <input type="hidden" name="id_bank_sampah" value="<?php echo $id_bank_sampah; ?>" /> 
	    <button type="submit" class="btn btn-primary">Simpan
        </button> 
	    <a href="<?php echo site_url('bank_sampah') ?>" class="btn btn-default">Batal</a>
	</form>
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js" type="text/javascript" charset="utf-8"></script>
    <script src="../assets/jquery.chained.js?v=1.0.0" type="text/javascript" charset="utf-8"></script>
    <script type="text/javascript" charset="utf-8">
        $(function() {
        /* For jquery.chained.js */
        $("#id_kecamatan").chained("#id_kabupaten");
        $("#id_kabupaten").chained("#id_provinsi");
    })
    </script>
			</div><!-- /.card-body -->
		</div>
		<!-- /.card -->
	</section>
	<!-- /.Left col -->
</div>

<script type="text/javascript">
    function Angkasaja(evt) {
    var charCode = (evt.which) ? evt.which : event.keyCode
    if (charCode > 31 && (charCode < 48 || charCode > 57))
    return false;
    return true;
    }
</script>