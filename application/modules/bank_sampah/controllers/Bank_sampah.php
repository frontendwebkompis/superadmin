<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Bank_sampah extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        if (cek_token()) {
            $this->load->model('Bank_sampah_model');
            $this->load->library('form_validation');
        } else {
            logout();
        }
    }

    public function index()
    {
        $q = urldecode($this->input->get('q', TRUE));
        $start = intval($this->input->get('start'));

        if ($q <> '') {
            $config['base_url'] = base_url() . 'bank_sampah?q=' . urlencode($q);
            $config['first_url'] = base_url() . 'bank_sampah?q=' . urlencode($q);
        } else {
            $config['base_url'] = base_url() . 'bank_sampah';
            $config['first_url'] = base_url() . 'bank_sampah';
        }

        $config['per_page'] = 10;
        $config['page_query_string'] = TRUE;
        $config['total_rows'] = $this->Bank_sampah_model->total_rows($q);
        $bank_sampah = $this->Bank_sampah_model->get_limit_data($config['per_page'], $start, $q);

        $this->load->library('pagination');
        $this->pagination->initialize($config);

        $data = array(
            'tittle'            => 'Bank Sampah',
            'judul'             => 'Bank Sampah',
            'title_card'        => 'List Bank Sampah',
            'tombol_aktif'      => 'bank_sampah',
            'bank_sampah_data'  => $bank_sampah,
            'q'                 => $q,
            'pagination'        => $this->pagination->create_links(),
            'total_rows'        => $config['total_rows'],
            'start'             => $start,
            'bank_sampah_aktif' => '1',
            'menu_aktif'        => 'bank_sampah'
        );
        $res['datakonten'] = $this->load->view('bank_sampah/bank_sampah_list', $data, true);
        $this->load->view('layouts/main_view', $res);
    }

    public function approve()
    {
        $q = urldecode($this->input->get('q', TRUE));
        $start = intval($this->input->get('start'));

        if ($q <> '') {
            $config['base_url'] = base_url() . 'bank_sampah/approve?q=' . urlencode($q);
            $config['first_url'] = base_url() . 'bank_sampah/approve?q=' . urlencode($q);
        } else {
            $config['base_url'] = base_url() . 'bank_sampah/approve';
            $config['first_url'] = base_url() . 'bank_sampah/approve';
        }

        $config['per_page'] = 10;
        $config['page_query_string'] = TRUE;
        $config['total_rows'] = $this->Bank_sampah_model->total_rows_approve($q);
        $bank_sampah = $this->Bank_sampah_model->get_limit_approve($config['per_page'], $start, $q);

        $this->load->library('pagination');
        $this->pagination->initialize($config);

        $data = array(
            'tittle'                => 'Bank Sampah',
            'judul'                 => 'Bank Sampah',
            'title_card'            => 'Approve Bank Sampah',
            'tombol_aktif'          => 'approve',
            'bank_sampah_data'      => $bank_sampah,
            'q'                     => $q,
            'pagination'            => $this->pagination->create_links(),
            'total_rows'            => $config['total_rows'],
            'start'                 => $start,
            'bank_sampah_aktif'     => '1',
            'menu_aktif'            => 'bank_sampah'
        );
        $res['datakonten'] = $this->load->view('bank_sampah/bank_sampah_approve', $data, true);
        $this->load->view('layouts/main_view', $res);
    }


    public function premium()
    {
        $q = urldecode($this->input->get('q', TRUE));
        $start = intval($this->input->get('start'));

        if ($q <> '') {
            $config['base_url'] = base_url() . 'bank_sampah/premium?q=' . urlencode($q);
            $config['first_url'] = base_url() . 'bank_sampah/premium?q=' . urlencode($q);
        } else {
            $config['base_url'] = base_url() . 'bank_sampah/premium';
            $config['first_url'] = base_url() . 'bank_sampah/premium';
        }

        $config['per_page'] = 10;
        $config['page_query_string'] = TRUE;
        $config['total_rows'] = $this->Bank_sampah_model->total_rows_premium($q);
        $bank_sampah = $this->Bank_sampah_model->get_limit_premium($config['per_page'], $start, $q);

        $this->load->library('pagination');
        $this->pagination->initialize($config);

        $data = array(
            'tittle'            => 'Bank Sampah',
            'judul'             => 'Bank Sampah',
            'title_card'        => 'Bank Sampah Premium',
            'tombol_aktif'      => 'premium',
            'bank_sampah_data'  => $bank_sampah,
            'q'                 => $q,
            'pagination'        => $this->pagination->create_links(),
            'total_rows'        => $config['total_rows'],
            'start'             => $start,
            'bank_sampah_aktif' => '1',
            'menu_aktif'        => 'bank_sampah'
        );
        $res['datakonten'] = $this->load->view('bank_sampah/bank_sampah_premium', $data, true);
        $this->load->view('layouts/main_view', $res);
    }
    public function berkas()
    {
        $q = urldecode($this->input->get('q', TRUE));
        $start = intval($this->input->get('start'));

        if ($q <> '') {
            $config['base_url'] = base_url() . 'bank_sampah/berkas?q=' . urlencode($q);
            $config['first_url'] = base_url() . 'bank_sampah/berkas?q=' . urlencode($q);
        } else {
            $config['base_url'] = base_url() . 'bank_sampah/berkas';
            $config['first_url'] = base_url() . 'bank_sampah/berkas';
        }

        $config['per_page'] = 10;
        $config['page_query_string'] = TRUE;
        $config['total_rows'] = $this->Bank_sampah_model->total_rows_berkas($q);
        $bank_sampah = $this->Bank_sampah_model->get_limit_berkas($config['per_page'], $start, $q);

        $this->load->library('pagination');
        $this->pagination->initialize($config);

        $data = array(
            'tittle'                => 'Bank Sampah',
            'judul'                 => 'Bank Sampah',
            'title_card'            => 'Bank Sampah Berkas',
            'tombol_aktif'          => 'berkas',
            'bank_sampah_data'      => $bank_sampah,
            'q'                     => $q,
            'pagination'            => $this->pagination->create_links(),
            'total_rows'            => $config['total_rows'],
            'start'                 => $start,
            'bank_sampah_aktif'     => '1',
            'menu_aktif'            => 'bank_sampah'
        );
        $res['datakonten'] = $this->load->view('bank_sampah/bank_sampah_berkas', $data, true);
        $this->load->view('layouts/main_view', $res);
    }

    public function banned()
    {
        $q = urldecode($this->input->get('q', TRUE));
        $start = intval($this->input->get('start'));

        if ($q <> '') {
            $config['base_url'] = base_url() . 'bank_sampah/banned?q=' . urlencode($q);
            $config['first_url'] = base_url() . 'bank_sampah/banned?q=' . urlencode($q);
        } else {
            $config['base_url'] = base_url() . 'bank_sampah/banned';
            $config['first_url'] = base_url() . 'bank_sampah/banned';
        }

        $config['per_page'] = 10;
        $config['page_query_string'] = TRUE;
        $config['total_rows'] = $this->Bank_sampah_model->total_rows_banned($q);
        $bank_sampah = $this->Bank_sampah_model->get_limit_banned($config['per_page'], $start, $q);

        $this->load->library('pagination');
        $this->pagination->initialize($config);

        $data = array(
            'tittle'                => 'Bank Sampah',
            'judul'                 => 'Bank Sampah',
            'title_card'            => 'Bank Sampah Banned',
            'tombol_aktif'          => 'banned',
            'bank_sampah_data'      => $bank_sampah,
            'q'                     => $q,
            'pagination'            => $this->pagination->create_links(),
            'total_rows'            => $config['total_rows'],
            'start'                 => $start,
            'bank_sampah_aktif'     => '1',
            'menu_aktif'            => 'bank_sampah'
        );
        $res['datakonten'] = $this->load->view('bank_sampah/bank_sampah_banned', $data, true);
        $this->load->view('layouts/main_view', $res);
    }


    public function detail($id)
    {
        $row = $this->Bank_sampah_model->get_bank_sampah_detail_id($id);
        $reason = $this->Bank_sampah_model->get_reason(1);

        if ($row) {
            $data = array(
                'tittle'                        => 'Bank Sampah',
                'judul'                         => 'Bank Sampah',
                'title_card'                    => 'Bank Sampah Detail',
                'id_bank_sampah'                => $row->id_bank_sampah,
                'id_status_bank_sampah'         => $row->id_status_bank_sampah,
                'ket_status_bank_sampah'        => $row->ket_status_bank_sampah,
                'nama_bank_sampah'              => $row->nama_bank_sampah,
                'alamat_bank_sampah'            => $row->alamat_bank_sampah,
                'nama_provinsi'                 => $row->nama_provinsi,
                'nama_kabupaten'                => $row->nama_kabupaten,
                'nama_kecamatan'                => $row->nama_kecamatan,
                'lat_bank_sampah'               => $row->lat_bank_sampah,
                'long_bank_sampah'              => $row->long_bank_sampah,
                'email_bank_sampah'             => $row->email_bank_sampah,
                'no_hp_bank_sampah'             => $row->no_hp_bank_sampah,
                'last_login'                    => $row->last_login,
                'no_telp_bank_sampah'           => $row->no_telp_bank_sampah,
                'foto_bangunan_bank_sampah'     => $row->foto_bangunan_bank_sampah,
                'sk_bank_sampah'                => $row->sk_bank_sampah,
                'tanggal_berdiri_bank_sampah'   => $row->tanggal_berdiri_bank_sampah,
                'slogan_bank_sampah'            => $row->slogan_bank_sampah,
                'website_bank_sampah'           => $row->website_bank_sampah,
                'nama_penanggungjawab'          => $row->nama_penanggungjawab,
                'no_ktp_penanggungjawab'        => $row->no_ktp_penanggungjawab,
                'foto_ktp_penanggungjawab'      => $row->foto_ktp_penanggungjawab,
                'selfie_ktp_penanggungjawab'    => $row->selfie_ktp_penanggungjawab,
                'alamat_penanggungjawab'        => $row->alamat_penanggungjawab,
                'tgl_daftar'                    => $row->tgl_daftar,
                'selfie_ktp_penanggungjawab'    => $row->selfie_ktp_penanggungjawab,
                'no_hp_penanggungjawab'         => $row->no_hp_penanggungjawab,
                'email_penanggungjawab'         => $row->email_penanggungjawab,
                'point_bank_sampah'             => $row->point_bank_sampah,
                'saldo_bank_sampah'             => $row->saldo_bank_sampah,
                'id_bank_sampah'                => $id,
                'reason'                        => $reason,
                'bank_sampah_aktif'             => '1',
                'menu_aktif'                    => 'bank_sampah'
            );

            $res['datakonten'] = $this->load->view('bank_sampah/bank_sampah_detail', $data, true);
            $this->load->view('layouts/main_view', $res);
        } else {
            $this->session->set_flashdata('gagal', 'Data Tidak Di Temukan');
            redirect(site_url('bank_sampah'));
        }
    }

    public function blokir($id)
    {
        $row = $this->Bank_sampah_model->get_bank_sampah_detail_id($id);
        $reason = $this->Bank_sampah_model->get_reason();
        if ($row) {
            $data = array(
                'tittle'                => 'Bank Sampah',
                'reason'                => $reason,
                'judul'                 => 'Bank Sampah',
                'title_card'            => 'Bank Sampah Blokir',
                'id_bank_sampah'        => $row->id_bank_sampah,
                'status_bank_sampah'    => $row->ket_status_bank_sampah,
                'nama_bank_sampah'      => $row->nama_bank_sampah,
                'alamat_bank_sampah'    => $row->alamat_bank_sampah,
                'email_bank_sampah'     => $row->email_bank_sampah,
                'no_hp_bank_sampah'     => $row->no_hp_bank_sampah,
                'last_login'            => $row->last_login,
                'nama_provinsi'         => $row->nama_provinsi,
                'nama_kabupaten'        => $row->nama_kabupaten,
                'nama_kecamatan'        => $row->nama_kecamatan,
                'website_bank_sampah'   => $row->website_bank_sampah,
                'menu_aktif'            => 'bank_sampah'
            );
            $res['datakonten'] = $this->load->view('bank_sampah/bank_sampah_blokir', $data, true);
            $this->load->view('layouts/main_view', $res);
        } else {
            $this->session->set_flashdata('gagal', 'Data Tidak Di Temukan');
            redirect(site_url('bank_sampah'));
        }
    }

    public function create()
    {
        $data = array(
            'tittle'                        => 'Bank Sampah',
            'judul'                         => 'Bank Sampah',
            'button'                        => 'Create',
            'action'                        => site_url('bank_sampah/create_action'),
            'id_bank_sampah'                => set_value('id_bank_sampah'),
            'id_status_bank_sampah'         => set_value('id_status_bank_sampah'),
            'nama_bank_sampah'              => set_value('nama_bank_sampah'),
            'alamat_bank_sampah'            => set_value('alamat_bank_sampah'),
            'id_provinsi'                   => set_value('id_provinsi'),
            'id_kabupaten'                  => set_value('id_kabupaten'),
            'id_kecamatan'                  => set_value('id_kecamatan'),
            'lat_bank_sampah'               => set_value('lat_bank_sampah'),
            'long_bank_sampah'              => set_value('long_bank_sampah'),
            'email_bank_sampah'             => set_value('email_bank_sampah'),
            'no_hp_bank_sampah'             => set_value('no_hp_bank_sampah'),
            'password_bank_sampah'          => set_value('password_bank_sampah'),
            'kode_verifikasi_bank_sampah'   => set_value('kode_verifikasi_bank_sampah'),
            'last_login'                    => set_value('last_login'),
            'menu_aktif'                    => 'bank_sampah'
        );
        $data['provinsi'] = $this->Bank_sampah_model->get_provinsi_all();
        $data['status_bank_sampah'] = $this->Bank_sampah_model->get_status_bank_all();

        $data['kabupaten'] = $this->Bank_sampah_model->get_kabupaten_all();
        $data['kecamatan'] = $this->Bank_sampah_model->get_kecamatan_all();

        $res['datakonten'] = $this->load->view('bank_sampah/bank_sampah_form', $data, true);
        $this->load->view('layouts/main_view', $res);
    }

    public function create_action()
    {
        $this->_rules();
        $dt = new DateTime();
        if ($this->form_validation->run() == FALSE) {
            $this->create();
        } else {
            $prefix = "KMP";
            $prov = $this->input->post('id_provinsi');
            $prov = sprintf('%02d', $prov);
            $kab = $this->input->post('id_kabupaten');
            $kab = sprintf('%03d', $kab);
            $row = $this->Bank_sampah_model->get_kode_akhir($this->input->post('id_kabupaten'));
            $kode = $row->jml;
            $kode = $kode + 1;
            $kode = sprintf('%03d', $kode);
            $idbank = $prefix . $prov . $kab . $kode;
            $data = array(
                'id_bank_sampah'                => $idbank,
                'id_status_bank_sampah'         => $this->input->post('id_status_bank_sampah', TRUE),
                'nama_bank_sampah'              => $this->input->post('nama_bank_sampah', TRUE),
                'alamat_bank_sampah'            => $this->input->post('alamat_bank_sampah', TRUE),
                'id_provinsi'                   => $this->input->post('id_provinsi', TRUE),
                'id_kabupaten'                  => $this->input->post('id_kabupaten', TRUE),
                'id_kecamatan'                  => $this->input->post('id_kecamatan', TRUE),
                'lat_bank_sampah'               => $this->input->post('lat_bank_sampah', TRUE),
                'long_bank_sampah'              => $this->input->post('long_bank_sampah', TRUE),
                'email_bank_sampah'             => $this->input->post('email_bank_sampah', TRUE),
                'no_hp_bank_sampah'             => $this->input->post('no_hp_bank_sampah', TRUE),
                'password_bank_sampah'          => $this->input->post('password_bank_sampah', TRUE),
                'kode_verifikasi_bank_sampah'   => $this->input->post('kode_verifikasi_bank_sampah', TRUE),
                'last_login' => $dt->format('Y-m-d H:i:s'),
                'tgl_join' => $dt->format('Y-m-d H:i:s'),

            );

            $this->Bank_sampah_model->insert($data);
            $this->session->set_flashdata('berhasil', 'Berhasil Menambahkan Data');
            redirect(site_url('bank_sampah'));
        }
    }

    public function update($id)
    {
        $row = $this->Bank_sampah_model->get_by_id($id);

        if ($row) {
            $data = array(
                'button'                        => 'Update',
                'action'                        => site_url('bank_sampah/update_action'),
                'id_bank_sampah'                => set_value('id_bank_sampah', $row->id_bank_sampah),
                'id_status_bank_sampah'         => set_value('id_status_bank_sampah', $row->id_status_bank_sampah),
                'nama_bank_sampah'              => set_value('nama_bank_sampah', $row->nama_bank_sampah),
                'alamat_bank_sampah'            => set_value('alamat_bank_sampah', $row->alamat_bank_sampah),
                'id_provinsi'                   => set_value('id_provinsi', $row->id_provinsi),
                'id_kabupaten'                  => set_value('id_kabupaten', $row->id_kabupaten),
                'id_kecamatan'                  => set_value('id_kecamatan', $row->id_kecamatan),
                'lat_bank_sampah'               => set_value('lat_bank_sampah', $row->lat_bank_sampah),
                'long_bank_sampah'              => set_value('long_bank_sampah', $row->long_bank_sampah),
                'email_bank_sampah'             => set_value('email_bank_sampah', $row->email_bank_sampah),
                'no_hp_bank_sampah'             => set_value('no_hp_bank_sampah', $row->no_hp_bank_sampah),
                'password_bank_sampah'          => set_value('password_bank_sampah', $row->password_bank_sampah),
                'kode_verifikasi_bank_sampah'   => set_value('kode_verifikasi_bank_sampah', $row->kode_verifikasi_bank_sampah),
                'last_login'                    => set_value('last_login', $row->last_login),
                'menu_aktif'                    => 'bank_sampah'
            );
            $this->load->view('bank_sampah/bank_sampah_form', $data);
        } else {
            $this->session->set_flashdata('gagal', 'Data Tidak Ditemukan');
            redirect(site_url('bank_sampah'));
        }
    }

    public function ubahbanned($id)
    {
        $row = $this->Bank_sampah_model->get_by_id($id);
        // 
        if ($row) {

            $data = array(
                'id_status_bank_sampah' => '4',
            );
            $this->Bank_sampah_model->update($id, $data);
            $this->session->set_flashdata('berhasil', 'Berhasil Merubah Data');
            redirect(site_url('bank_sampah/banned'));
        } else {
            $this->session->set_flashdata('gagal', 'Data Tidak Ditemukan');
            redirect(site_url('bank_sampah/banned'));
        }
    }
    public function ubahstatus($id)
    {
        $row = $this->Bank_sampah_model->get_by_id($id);
        // 
        if ($row) {

            $data = array(
                'id_status_bank_sampah' => '1',
            );
            $hari = array("Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat");

            foreach ($hari as $c) {
                $jadwal = array(
                    'id_bank_sampah' => $id,
                    'hari_buka' => $c,
                );
                //tambahkan jadwal bank sampah
                $this->db->insert('jadwal_bank_sampah', $jadwal);
            }
            $this->Bank_sampah_model->update($id, $data);
            $this->session->set_flashdata('berhasil', 'Berhasil Merubah Data');
            redirect(site_url('bank_sampah/berkas'));
        } else {
            $this->session->set_flashdata('gagal', 'Data Tidak Ditemukan');
            redirect(site_url('bank_sampah/berkas'));
        }
    }

    public function ubahapprove($id)
    {
        $row = $this->Bank_sampah_model->get_by_id($id);

        if ($row) {
            $data = array(
                'id_status_bank_sampah' => '1',
            );
            $this->Bank_sampah_model->update($id, $data);
            //tambahkan field di bank sampah detail
            $hari = array("Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat");

            foreach ($hari as $c) {
                $jadwal = array(
                    'id_bank_sampah' => $row->id_bank_sampah,
                    'hari_buka' => $c,
                );
                //tambahkan jadwal bank sampah
                $this->db->insert('jadwal_bank_sampah', $jadwal);
            }
            $this->Bank_sampah_model->insertbanksampahdetail($id);
            $this->session->set_flashdata('berhasil', 'Berhasil Merubah Data');
            redirect(site_url('bank_sampah/approve'));
        } else {
            $this->session->set_flashdata('gagal', 'Data Tidak Ditemukan');
            redirect(site_url('bank_sampah/approve'));
        }
    }

    public function ubahpremium($id)
    {
        $row = $this->Bank_sampah_model->get_by_id($id);

        if ($row) {
            $data = array(
                'id_status_bank_sampah' => '3',
            );
            $this->Bank_sampah_model->update($id, $data);
            $this->session->set_flashdata('berhasil', 'Berhasil Merubah Data');
            redirect(site_url('bank_sampah'));
        } else {
            $this->session->set_flashdata('gagal', 'Data Tidak Ditemukan');
            redirect(site_url('bank_sampah'));
        }
    }
    public function update_action()
    {

        $dt = new DateTime();
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->update($this->input->post('id_bank_sampah', TRUE));
        } else {

            $dt->format('Y-m-d H:i:s');
            $data = array(
                'id_status_bank_sampah' => $this->input->post('id_status_bank_sampah', TRUE),
                'nama_bank_sampah' => $this->input->post('nama_bank_sampah', TRUE),
                'alamat_bank_sampah' => $this->input->post('alamat_bank_sampah', TRUE),
                'id_provinsi' => $this->input->post('id_provinsi', TRUE),
                'id_kabupaten' => $this->input->post('id_kabupaten', TRUE),
                'id_kecamatan' => $this->input->post('id_kecamatan', TRUE),
                'lat_bank_sampah' => $this->input->post('lat_bank_sampah', TRUE),
                'long_bank_sampah' => $this->input->post('long_bank_sampah', TRUE),
                'email_bank_sampah' => $this->input->post('email_bank_sampah', TRUE),
                'no_hp_bank_sampah' => $this->input->post('no_hp_bank_sampah', TRUE),
                'password_bank_sampah' => $this->input->post('password_bank_sampah', TRUE),
                'kode_verifikasi_bank_sampah' => $this->input->post('kode_verifikasi_bank_sampah', TRUE),
            );

            $this->Bank_sampah_model->update($this->input->post('id_bank_sampah', TRUE), $data);
            $this->session->set_flashdata('berhasil', 'Berhasil Merubah Data');
            redirect(site_url('bank_sampah'));
        }
    }

    public function delete($id)
    {
        $row = $this->Bank_sampah_model->get_by_id($id);

        if ($row) {
            $this->Bank_sampah_model->delete($id);
            $this->session->set_flashdata('berhasil', 'Berhasil Menghapus Data');
            redirect(site_url('bank_sampah'));
        } else {
            $this->session->set_flashdata('gagal', 'Data Tidak Ditemukan');
            redirect(site_url('bank_sampah'));
        }
    }

    public function _rules()
    {
        // $this->form_validation->set_rules('id_status_bank_sampah', 'id status bank sampah', 'trim|required');
        $this->form_validation->set_rules('nama_bank_sampah', 'nama bank sampah', 'trim|required');
        $this->form_validation->set_rules('alamat_bank_sampah', 'alamat bank sampah', 'trim|required');
        $this->form_validation->set_rules('id_provinsi', 'id provinsi', 'trim|required');
        $this->form_validation->set_rules('id_kabupaten', 'id kabupaten', 'trim|required');
        $this->form_validation->set_rules('id_kecamatan', 'id kecamatan', 'trim|required');
        $this->form_validation->set_rules('lat_bank_sampah', 'lat bank sampah', 'trim|required|numeric');
        $this->form_validation->set_rules('long_bank_sampah', 'long bank sampah', 'trim|required|numeric');
        $this->form_validation->set_rules('email_bank_sampah', 'email bank sampah', 'trim|required');
        $this->form_validation->set_rules('no_hp_bank_sampah', 'no hp bank sampah', 'trim|required');
        // $this->form_validation->set_rules('password_bank_sampah', 'password bank sampah', 'trim|required');
        $this->form_validation->set_rules('kode_verifikasi_bank_sampah', 'kode verifikasi bank sampah', 'trim|required');

        $this->form_validation->set_rules('id_bank_sampah', 'id_bank_sampah', 'trim');
        $this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }


    public function exportxl()
    {
        $q          = $this->input->get('q');
        $subject    = "Bank_Sampah";
        // $id         = $this->session->userdata('uid');
        $this->db->query('SET @no=0');
        $sql        =
            "SELECT @no:=@no+1 AS nomor,
                bank_sampah.nama_bank_sampah,
                bank_sampah.alamat_bank_sampah,
                bank_sampah.email_bank_sampah,
                bank_sampah.no_hp_bank_sampah,
                bank_sampah.last_login,
                bank_sampah.tgl_daftar,
                status_bank_sampah.ket_status_bank_sampah,
                provinsi.nama_provinsi  
                FROM `bank_sampah` JOIN `status_bank_sampah` 
                ON `bank_sampah`.`id_status_bank_sampah` = `status_bank_sampah`.`id_status_bank_sampah` 
                JOIN `provinsi` ON `bank_sampah`.`id_provinsi` = `provinsi`.`id_provinsi` 
                JOIN `kabupaten` ON `bank_sampah`.`id_kabupaten` = `kabupaten`.`id_kabupaten` 
                JOIN `kecamatan` ON `bank_sampah`.`id_kecamatan` = `kecamatan`.`id_kecamatan` 
                WHERE `bank_sampah`.`id_status_bank_sampah` = 3 ";
        exportSQL('Bank_Sampah', $subject, $sql);
    }

    public function exportxlapprove()
    {
        $q          = $this->input->get('q');
        $subject    = "Bank_Approve";
        // $id         = $this->session->userdata('uid');
        $this->db->query('SET @no=0');
        $sql        =
            "SELECT @no:=@no+1 AS nomor,
                bank_sampah.nama_bank_sampah,
                bank_sampah.alamat_bank_sampah,
                bank_sampah.email_bank_sampah,
                bank_sampah.no_hp_bank_sampah,
                bank_sampah.last_login,
                bank_sampah.tgl_daftar,
                status_bank_sampah.ket_status_bank_sampah,
                provinsi.nama_provinsi  
                FROM `bank_sampah` JOIN `status_bank_sampah` 
                ON `bank_sampah`.`id_status_bank_sampah` = `status_bank_sampah`.`id_status_bank_sampah` 
                JOIN `provinsi` ON `bank_sampah`.`id_provinsi` = `provinsi`.`id_provinsi` 
                JOIN `kabupaten` ON `bank_sampah`.`id_kabupaten` = `kabupaten`.`id_kabupaten` 
                JOIN `kecamatan` ON `bank_sampah`.`id_kecamatan` = `kecamatan`.`id_kecamatan` 
                WHERE `bank_sampah`.`id_status_bank_sampah` = 0 ";
        exportSQL('Bank_Approve', $subject, $sql);
    }

    public function exportxlberkas()
    {
        $q          = $this->input->get('q');
        $subject    = "Bank_Berkas";
        // $id         = $this->session->userdata('uid');
        $this->db->query('SET @no=0');
        $sql        =
            "SELECT @no:=@no+1 AS nomor,
                bank_sampah.nama_bank_sampah,
                bank_sampah.alamat_bank_sampah,
                bank_sampah.email_bank_sampah,
                bank_sampah.no_hp_bank_sampah,
                bank_sampah.last_login,
                bank_sampah.tgl_daftar,
                status_bank_sampah.ket_status_bank_sampah,
                provinsi.nama_provinsi  
                FROM `bank_sampah` JOIN `status_bank_sampah` 
                ON `bank_sampah`.`id_status_bank_sampah` = `status_bank_sampah`.`id_status_bank_sampah` 
                JOIN `provinsi` ON `bank_sampah`.`id_provinsi` = `provinsi`.`id_provinsi` 
                JOIN `kabupaten` ON `bank_sampah`.`id_kabupaten` = `kabupaten`.`id_kabupaten` 
                JOIN `kecamatan` ON `bank_sampah`.`id_kecamatan` = `kecamatan`.`id_kecamatan` 
                WHERE `bank_sampah`.`id_status_bank_sampah` = 1 ";
        exportSQL('Bank_Berkas', $subject, $sql);
    }

    public function exportxlpremium()
    {
        $q          = $this->input->get('q');
        $subject    = "Bank_Premium";
        // $id         = $this->session->userdata('uid');
        $this->db->query('SET @no=0');
        $sql        =
            "SELECT @no:=@no+1 AS nomor,
                bank_sampah.nama_bank_sampah,
                bank_sampah.alamat_bank_sampah,
                bank_sampah.email_bank_sampah,
                bank_sampah.no_hp_bank_sampah,
                bank_sampah.last_login,
                bank_sampah.tgl_daftar,
                status_bank_sampah.ket_status_bank_sampah,
                provinsi.nama_provinsi  
                FROM `bank_sampah` JOIN `status_bank_sampah` 
                ON `bank_sampah`.`id_status_bank_sampah` = `status_bank_sampah`.`id_status_bank_sampah` 
                JOIN `provinsi` ON `bank_sampah`.`id_provinsi` = `provinsi`.`id_provinsi` 
                JOIN `kabupaten` ON `bank_sampah`.`id_kabupaten` = `kabupaten`.`id_kabupaten` 
                JOIN `kecamatan` ON `bank_sampah`.`id_kecamatan` = `kecamatan`.`id_kecamatan` 
                WHERE `bank_sampah`.`id_status_bank_sampah` = 2 ";
        exportSQL('Bank_Premium', $subject, $sql);
    }

    function get_image()
    {

        $img = $this->input->get('img');
        // echo getkonten($img);
        $cek = 'https://kompis-bank-sampah.s3.ap-southeast-1.amazonaws.com/' . $img;
        $row = $this->Bank_sampah_model->get_by_poto($cek);
        if ($row) {
            echo getkonten($img);
        } else {
            $this->session->set_flashdata('cek', 'Gambar Tidak Ditemukan');
            redirect('profile');
        }
    }
    function get_by_sk()
    {

        $img = $this->input->get('img');
        // echo getkonten($img);
        $cek = 'https://kompis-bank-sampah.s3.ap-southeast-1.amazonaws.com/' . $img;
        $row = $this->Bank_sampah_model->get_by_sk($cek);
        if ($row) {
            echo getkonten($img);
        } else {
            $this->session->set_flashdata('cek', 'Gambar Tidak Ditemukan');
            redirect('profile');
        }
    }
    function get_image_selfi()
    {

        $img = $this->input->get('img');
        // echo getkonten($img);
        $cek = 'https://kompis-bank-sampah.s3.ap-southeast-1.amazonaws.com/' . $img;
        $row = $this->Bank_sampah_model->get_by_selfi($cek);
        if ($row) {
            echo getkonten($img);
        } else {
            $this->session->set_flashdata('cek', 'Gambar Tidak Ditemukan');
            redirect('profile');
        }
    }
    function get_by_bangunan()
    {

        $img = $this->input->get('img');
        // echo getkonten($img);
        $cek = 'https://kompis-bank-sampah.s3.ap-southeast-1.amazonaws.com/' . $img;
        $row = $this->Bank_sampah_model->get_by_bangunan($cek);
        if ($row) {
            echo getkonten($img);
        } else {
            $this->session->set_flashdata('cek', 'Gambar Tidak Ditemukan');
            redirect('profile');
        }
    }

    function simpans3()
    {
        echo getkontendown('profile/berkas/foto_bangunan_bank_sampah/KMP260320200002/b8d88eced2cc334cc2b37c1469cda5ec.jpeg');
    }
    public function cek_ktp_asli()
    {
        $no_ktp = $this->input->get('ktp');
        $nama = $this->input->get('nama');
        $data = $this->Bank_sampah_model->get_ktp_ori($no_ktp, $nama);

        if ($data) {

            echo json_encode($data, 200);
        } else {

            echo json_encode(['status' => false, 'message' => 'Kombinasi NIK atau Nama tidak ditemnukan'], 400);
        }
    }
    function exportxlbanned()
    {
        $q          = $this->input->get('q');
        $subject    = "Bank_Banned";
        // $id         = $this->session->userdata('uid');
        $this->db->query('SET @no=0');
        $sql        =
            "SELECT @no:=@no+1 AS nomor,
                bank_sampah.nama_bank_sampah,
                bank_sampah.alamat_bank_sampah,
                bank_sampah.email_bank_sampah,
                bank_sampah.no_hp_bank_sampah,
                bank_sampah.last_login,
                bank_sampah.tgl_daftar,
                status_bank_sampah.ket_status_bank_sampah,
                provinsi.nama_provinsi  
                FROM `bank_sampah` JOIN `status_bank_sampah` 
                ON `bank_sampah`.`id_status_bank_sampah` = `status_bank_sampah`.`id_status_bank_sampah` 
                JOIN `provinsi` ON `bank_sampah`.`id_provinsi` = `provinsi`.`id_provinsi` 
                JOIN `kabupaten` ON `bank_sampah`.`id_kabupaten` = `kabupaten`.`id_kabupaten` 
                JOIN `kecamatan` ON `bank_sampah`.`id_kecamatan` = `kecamatan`.`id_kecamatan` 
                WHERE `bank_sampah`.`id_status_bank_sampah` = 4 ";
        exportSQL('Bank_Banned', $subject, $sql);
    }

    public function tolak_berkas()
    {
        $id = $this->input->post('id_bank_sampah');
        $reason = $this->input->post('reason');

        $data = array(
            'id_status_bank_sampah'    => 1,
            'reason_reject'     => $reason,
        );
        $this->Bank_sampah_model->update($id, $data);
        # code...
        redirect('bank_sampah/detail/' . $id);
    }
}

/* End of file Bank_sampah.php */
/* Location: ./application/controllers/Bank_sampah.php */
