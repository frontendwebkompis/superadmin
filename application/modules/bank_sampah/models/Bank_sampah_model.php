<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Bank_sampah_model extends CI_Model
{

    public $table = 'bank_sampah';
      public $table_detail = 'bank_sampah_detail';
    public $id = 'id_bank_sampah';

    public $order = 'DESC';

    function __construct()
    {
        parent::__construct();
    }

    // get all
    function get_all()
    {
        $this->db->order_by($this->id, $this->order);
        return $this->db->get($this->table)->result();
    }

 // get data by id
    function get_by_poto($id)
    {
        $this->db->where('foto_ktp_penanggungjawab', $id);
        return $this->db->get('bank_sampah_detail')->row();
    } 
    function get_provinsi_all()
    {

        return $this->db->get('provinsi')->result();
    } 
    function get_reason($cek=null)
    {
        if($cek==1){
 $this->db->where('id_proses', 1);
      
        }else{
            $this->db->where('id_proses', 9);
        }
 
        return $this->db->get('reason')->result();
    }
function get_by_sk($id)
    {
        $this->db->where('sk_bank_sampah', $id);
        return $this->db->get($this->table_detail)->row();
    }
function get_by_selfi($id)
    {
        $this->db->where('selfie_ktp_penanggungjawab', $id);
        return $this->db->get($this->table_detail)->row();
    }

 function get_by_bangunan($id)
    {
        $this->db->where('foto_bangunan_bank_sampah', $id);
        return $this->db->get($this->table_detail)->row();
    }
    function get_status_bank_all()
    {

        return $this->db->get('status_bank_sampah')->result();
    }

    function get_kabupaten_all()
    {

        return $this->db->get('kabupaten')->result();
    }
    function get_kecamatan_all()
    {

        return $this->db->get('kecamatan')->result();
    }
    // get data by id
    function get_by_id($id)
    {
        $query = $this->db->query("SELECT bank_sampah.id_bank_sampah,bank_sampah.nama_bank_sampah,bank_sampah.alamat_bank_sampah,status_bank_sampah.ket_status_bank_sampah,provinsi.nama_provinsi,kabupaten.nama_kabupaten,kecamatan.nama_kecamatan,bank_sampah.lat_bank_sampah,bank_sampah.long_bank_sampah,bank_sampah.email_bank_sampah,bank_sampah.no_hp_bank_sampah,bank_sampah.last_login FROM bank_sampah
   	    LEFT JOIN status_bank_sampah ON status_bank_sampah.id_status_bank_sampah=bank_sampah.id_status_bank_sampah
        LEFT JOIN provinsi ON provinsi.id_provinsi=bank_sampah.id_provinsi
        LEFT JOIN kabupaten ON kabupaten.id_kabupaten=bank_sampah.id_kabupaten
        LEFT JOIN kecamatan ON kecamatan.id_kecamatan=bank_sampah.id_kecamatan
        where bank_sampah.id_bank_sampah='" . $id . "'");

        return $query->row();
    }

    // get data untuk bank sampah detail by id
    function get_bank_sampah_detail_id($id)
    {
        $query = $this->db->query("SELECT bank_sampah.id_bank_sampah,bank_sampah.id_status_bank_sampah,bank_sampah.nama_bank_sampah,bank_sampah.alamat_bank_sampah,status_bank_sampah.ket_status_bank_sampah,provinsi.nama_provinsi,kabupaten.nama_kabupaten,kecamatan.nama_kecamatan,bank_sampah.lat_bank_sampah,bank_sampah.long_bank_sampah,bank_sampah.email_bank_sampah,bank_sampah.no_hp_bank_sampah,bank_sampah.last_login,bank_sampah.tgl_daftar,bank_sampah_detail.no_telp_bank_sampah,bank_sampah_detail.foto_bangunan_bank_sampah,bank_sampah_detail.sk_bank_sampah,bank_sampah_detail.tanggal_berdiri_bank_sampah,bank_sampah_detail.slogan_bank_sampah,bank_sampah_detail.website_bank_sampah,bank_sampah_detail.nama_penanggungjawab,bank_sampah_detail.no_ktp_penanggungjawab,bank_sampah_detail.foto_ktp_penanggungjawab,bank_sampah_detail.selfie_ktp_penanggungjawab,bank_sampah_detail.alamat_penanggungjawab,bank_sampah_detail.no_hp_penanggungjawab,bank_sampah_detail.email_penanggungjawab,bank_sampah_detail.saldo_bank_sampah,bank_sampah_detail.point_bank_sampah FROM bank_sampah
        RIGHT JOIN bank_sampah_detail ON bank_sampah_detail.id_bank_sampah=bank_sampah.id_bank_sampah
		LEFT JOIN status_bank_sampah ON status_bank_sampah.id_status_bank_sampah=bank_sampah.id_status_bank_sampah
        LEFT JOIN provinsi ON provinsi.id_provinsi=bank_sampah.id_provinsi
        LEFT JOIN kabupaten ON kabupaten.id_kabupaten=bank_sampah.id_kabupaten
        LEFT JOIN kecamatan ON kecamatan.id_kecamatan=bank_sampah.id_kecamatan
        where bank_sampah.id_bank_sampah='" . $id . "'");

        return $query->row();
    }

    // get total rows
    function total_rows($q = NULL)
    {
        $this->db->where('id_status_bank_sampah', 3);
        $this->db->group_start();
        $this->db->or_like('nama_bank_sampah', $q);
        $this->db->or_like('alamat_bank_sampah', $q);
        $this->db->or_like('id_provinsi', $q);
        $this->db->or_like('id_kabupaten', $q);
        $this->db->or_like('id_kecamatan', $q);
        $this->db->or_like('lat_bank_sampah', $q);
        $this->db->or_like('long_bank_sampah', $q);
        $this->db->or_like('email_bank_sampah', $q);
        $this->db->or_like('no_hp_bank_sampah', $q);
        $this->db->or_like('password_bank_sampah', $q);
        $this->db->or_like('kode_verifikasi_bank_sampah', $q);
        $this->db->or_like('last_login', $q);
        $this->db->group_end();

        $this->db->from($this->table);
        return $this->db->count_all_results();
    }
    // get total rows
    function total_rows_approve($q = NULL)
    {
           $this->db->select('*');
        $this->db->join('status_bank_sampah', 'bank_sampah.id_status_bank_sampah = status_bank_sampah.id_status_bank_sampah');
        $this->db->join('provinsi', 'bank_sampah.id_provinsi = provinsi.id_provinsi');
        $this->db->join('kabupaten', 'bank_sampah.id_kabupaten = kabupaten.id_kabupaten');
        $this->db->join('kecamatan', 'bank_sampah.id_kecamatan = kecamatan.id_kecamatan');
        $this->db->order_by($this->id, $this->order);
        $this->db->where('bank_sampah.id_status_bank_sampah', 0);
        $this->db->group_start();
        $this->db->or_like('nama_bank_sampah', $q);
        $this->db->group_end();
        $this->db->from($this->table);
        return $this->db->count_all_results();
    }

    // get total rows
    function total_rows_premium($q = NULL)
    {
         $this->db->select('*');
        $this->db->join('status_bank_sampah', 'bank_sampah.id_status_bank_sampah = status_bank_sampah.id_status_bank_sampah');
        $this->db->join('provinsi', 'bank_sampah.id_provinsi = provinsi.id_provinsi');
        $this->db->join('kabupaten', 'bank_sampah.id_kabupaten = kabupaten.id_kabupaten');
        $this->db->join('kecamatan', 'bank_sampah.id_kecamatan = kecamatan.id_kecamatan');
        $this->db->order_by($this->id, $this->order);
         $this->db->where('bank_sampah.id_status_bank_sampah', 2);
        $this->db->group_start();
        $this->db->or_like('nama_bank_sampah', $q);
        $this->db->group_end();
        $this->db->from($this->table);
        return $this->db->count_all_results();
    }
    // get total rows
    function total_rows_berkas($q = NULL)
    {
     $this->db->select('*');
        $this->db->join('status_bank_sampah', 'bank_sampah.id_status_bank_sampah = status_bank_sampah.id_status_bank_sampah');
        $this->db->join('provinsi', 'bank_sampah.id_provinsi = provinsi.id_provinsi');
        $this->db->join('kabupaten', 'bank_sampah.id_kabupaten = kabupaten.id_kabupaten');
        $this->db->join('kecamatan', 'bank_sampah.id_kecamatan = kecamatan.id_kecamatan');
        $this->db->order_by($this->id, $this->order);
        // $this->db->like('id_bank_sampah', $q);
        $this->db->where('bank_sampah.id_status_bank_sampah', 1);
        $this->db->group_start();
        $this->db->or_like('nama_bank_sampah', $q);
        $this->db->group_end(); 
        
        $this->db->from($this->table);
        return $this->db->count_all_results();
    }
    // get total rows
    function total_rows_banned($q = NULL)
    {
         $this->db->select('*');
        $this->db->join('status_bank_sampah', 'bank_sampah.id_status_bank_sampah = status_bank_sampah.id_status_bank_sampah');
        $this->db->join('provinsi', 'bank_sampah.id_provinsi = provinsi.id_provinsi');
        $this->db->join('kabupaten', 'bank_sampah.id_kabupaten = kabupaten.id_kabupaten');
        $this->db->join('kecamatan', 'bank_sampah.id_kecamatan = kecamatan.id_kecamatan');
        $this->db->order_by($this->id, $this->order);
        $this->db->where('bank_sampah.id_status_bank_sampah', 4);
        $this->db->group_start();
        $this->db->or_like('nama_bank_sampah', $q);
        $this->db->group_end();

        $this->db->from($this->table);
        return $this->db->count_all_results();
    }

    function get_kode_akhir($kab)
    {
        $query = $this->db->query('SELECT COUNT(*) AS jml FROM bank_sampah WHERE id_kabupaten=' . $kab);

        return $query->row();
    }
    // get data with limit and search
    function get_limit_data($limit, $start = 0, $q = NULL)
    {
        $this->db->select('*');
        $this->db->from('bank_sampah');
        $this->db->join('status_bank_sampah', 'bank_sampah.id_status_bank_sampah = status_bank_sampah.id_status_bank_sampah');
        $this->db->join('provinsi', 'bank_sampah.id_provinsi = provinsi.id_provinsi');
        $this->db->join('kabupaten', 'bank_sampah.id_kabupaten = kabupaten.id_kabupaten');
        $this->db->join('kecamatan', 'bank_sampah.id_kecamatan = kecamatan.id_kecamatan');

        $this->db->where('bank_sampah.id_status_bank_sampah', 3);
        $this->db->group_start();
        $this->db->or_like('nama_bank_sampah', $q);
        $this->db->group_end();
        $this->db->order_by($this->table.'.tgl_daftar', $this->order);
        $this->db->limit($limit, $start);
        return $this->db->get()->result();
    }

    function get_limit_approve($limit, $start = 0, $q = NULL)
    {
        $this->db->select('*');
        $this->db->join('status_bank_sampah', 'bank_sampah.id_status_bank_sampah = status_bank_sampah.id_status_bank_sampah','left join');
        $this->db->join('provinsi', 'bank_sampah.id_provinsi = provinsi.id_provinsi','left join');
        $this->db->join('kabupaten', 'bank_sampah.id_kabupaten = kabupaten.id_kabupaten','left join');
        $this->db->join('kecamatan', 'bank_sampah.id_kecamatan = kecamatan.id_kecamatan','left join');
        $this->db->order_by($this->table.'.tgl_daftar', $this->order);
        $this->db->where('bank_sampah.id_status_bank_sampah', 0);
        $this->db->group_start();
        $this->db->or_like('nama_bank_sampah', $q);
        $this->db->group_end();
        $this->db->limit($limit, $start);
        return $this->db->get('bank_sampah')->result();
    }

    //premium
    function get_limit_premium($limit, $start = 0, $q = NULL)
    {
        $this->db->select('*');
        $this->db->join('status_bank_sampah', 'bank_sampah.id_status_bank_sampah = status_bank_sampah.id_status_bank_sampah');
        $this->db->join('provinsi', 'bank_sampah.id_provinsi = provinsi.id_provinsi');
        $this->db->join('kabupaten', 'bank_sampah.id_kabupaten = kabupaten.id_kabupaten');
        $this->db->join('kecamatan', 'bank_sampah.id_kecamatan = kecamatan.id_kecamatan');
        $this->db->order_by($this->table.'.tgl_daftar', $this->order);
        $this->db->where('bank_sampah.id_status_bank_sampah', 2);
        $this->db->group_start();
        $this->db->or_like('nama_bank_sampah', $q);
        $this->db->group_end();
        $this->db->limit($limit, $start);
        return $this->db->get('bank_sampah')->result();
    }
    //premium
    function get_limit_berkas($limit, $start = 0, $q = NULL)
    {
        $this->db->select('*');
        $this->db->join('status_bank_sampah', 'bank_sampah.id_status_bank_sampah = status_bank_sampah.id_status_bank_sampah');
        $this->db->join('provinsi', 'bank_sampah.id_provinsi = provinsi.id_provinsi');
        $this->db->join('kabupaten', 'bank_sampah.id_kabupaten = kabupaten.id_kabupaten');
        $this->db->join('kecamatan', 'bank_sampah.id_kecamatan = kecamatan.id_kecamatan');
        $this->db->order_by($this->table.'.tgl_daftar', $this->order);
        // $this->db->like('id_bank_sampah', $q);
        $this->db->where('bank_sampah.id_status_bank_sampah', 1);
        $this->db->group_start();
        $this->db->or_like('nama_bank_sampah', $q);
        $this->db->group_end(); 
        $this->db->limit($limit, $start);
        return $this->db->get('bank_sampah')->result();
    }
    //banned
    function get_limit_banned($limit, $start = 0, $q = NULL)
    {
        $this->db->select('*');
        $this->db->join('status_bank_sampah', 'bank_sampah.id_status_bank_sampah = status_bank_sampah.id_status_bank_sampah');
        $this->db->join('provinsi', 'bank_sampah.id_provinsi = provinsi.id_provinsi');
        $this->db->join('kabupaten', 'bank_sampah.id_kabupaten = kabupaten.id_kabupaten');
        $this->db->join('kecamatan', 'bank_sampah.id_kecamatan = kecamatan.id_kecamatan');
        $this->db->order_by($this->table.'.tgl_daftar', $this->order);
        $this->db->where('bank_sampah.id_status_bank_sampah', 4);
        $this->db->group_start();
        $this->db->or_like('nama_bank_sampah', $q);
        $this->db->group_end();
        $this->db->limit($limit, $start);
        return $this->db->get('bank_sampah')->result();
    }


    // insert data
    function insert($data)
    {
        $this->db->insert($this->table, $data);
    }

    // update data
    function update($id, $data)
    {
        $this->db->where($this->id, $id);
        $this->db->update($this->table, $data);
    }

    // update detail bank sampah
    function insertbanksampahdetail($id)
    {
        $data = array(
            'id_bank_sampah' => $id,
        );
        $this->db->insert('bank_sampah_detail', $data);
    }
    // delete data
    function delete($id)
    {
        $this->db->where($this->id, $id);
        $this->db->delete($this->table);
    }

        public function get_ktp_ori($no_ktp, $nama)
    {
        $dt = new DateTime();
        try{
          // $apiHost ='http://api.shipping.esoftplay.com';
        
         $apiHost ='http://api.binderbyte.com/cekktp';
        $key ='e4f44aaa99629e4947818b511afb35aadc3d70959f2c8b02498e2c6e4f364311';
       $client = new GuzzleHttp\Client(['base_uri' => $apiHost,]);

        $data = $client->request('GET', '?api_key=' . $key.'&nik='.$no_ktp.'&nama='.$nama ,[
              
            'http_errors' => false
        ]);
        // if($data->getStatusCode()==200){
        $datas = $data->getBody();
        $data= json_decode($datas->getContents());
       
          return $data;
    
      
       } catch(RequestException $e) {
            $response_s = $e->getResponse()->getBody();
            return $response_s;
        }

        catch(ClientException $ce){
            $response_s = $e->getResponse()->getBody();
            return $response_s;
}
      
    } 
}

/* End of file Bank_sampah_model.php */
/* Location: ./application/models/Bank_sampah_model.php */
/* Please DO NOT modify this information : */
/* Generated by Harviacode Codeigniter CRUD Generator 2020-02-12 04:22:56 */
/* http://harviacode.com */
