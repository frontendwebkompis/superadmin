<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Pembelian_pengepul extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        if (cek_token()) {
            $this->load->model('Pembelian_pengepul_model');
            $this->load->library('form_validation');
        } else {
            logout();
        }
    }

    public function index()
    {
        $q = urldecode($this->input->get('q', TRUE));
        $start = intval($this->input->get('start'));
        $awal   = $this->input->get('awal') ? $this->input->get('awal') : date('y').'-01-01';
        $akhir  = $this->input->get('akhir') ? $this->input->get('akhir') : date('y-m-d');
  
        if ($q <> '') {
            $config['base_url'] = base_url() . 'pembelian_pengepul?q=' . urlencode($q);
            $config['first_url'] = base_url() . 'pembelian_pengepul?q=' . urlencode($q);
        } else {
            $config['base_url'] = base_url() . 'pembelian_pengepul';
            $config['first_url'] = base_url() . 'pembelian_pengepul';
        }

        $config['per_page'] = 10;
        $config['page_query_string'] = TRUE;
        $config['total_rows'] = $this->Pembelian_pengepul_model->total_rows($q,$awal,$akhir);
        $pembelian_pengepul = $this->Pembelian_pengepul_model->get_limit_data($config['per_page'], $start, $q,$awal,$akhir);

        $this->load->library('pagination');
        $this->pagination->initialize($config);

        $data = array(
            'tittle'                    => 'Pembelian Pengepul',
            'judul'                     => 'Pembelian Pengepul',
            'title_card'                => 'List Pembelian Pengepul',
            'pembelian_pengepul_data'   => $pembelian_pengepul,
            'q'                         => $q,
            'pagination'                => $this->pagination->create_links(),
            'total_rows'                => $config['total_rows'],
            'start'                     => $start,
            'pembelian_pengepul_aktif'  => '1',
            'menu_aktif'                => 'pembelian_pengepul'
        );
        $res['datakonten'] = $this->load->view('pembelian_pengepul/pembelian_pengepul_list', $data, true);
        $this->load->view('layouts/main_view', $res);
    } 

    public function tolak()
    {
        $q = urldecode($this->input->get('q', TRUE));
        $start = intval($this->input->get('start'));
        $awal   = $this->input->get('awal') ? $this->input->get('awal') : date('y').'-01-01';
        $akhir  = $this->input->get('akhir') ? $this->input->get('akhir') : date('y-m-d');
  
        if ($q <> '') {
            $config['base_url'] = base_url() . 'pembelian_pengepul/tolak?q=' . urlencode($q);
            $config['first_url'] = base_url() . 'pembelian_pengepul/tolak?q=' . urlencode($q);
        } else {
            $config['base_url'] = base_url() . 'pembelian_pengepul/tolak';
            $config['first_url'] = base_url() . 'pembelian_pengepul/tolak';
        }

        $config['per_page'] = 10;
        $config['page_query_string'] = TRUE;
        $config['total_rows'] = $this->Pembelian_pengepul_model->total_rows_tolak($q,$awal,$akhir);
        $pembelian_pengepul = $this->Pembelian_pengepul_model->get_limit_data_tolak($config['per_page'], $start, $q,$awal,$akhir);

        $this->load->library('pagination');
        $this->pagination->initialize($config);

        $data = array(
            'tittle'                    => 'Pembelian Pengepul',
            'judul'                     => 'Pembelian Pengepul',
            'title_card'                => 'List Tolak Pembelian Pengepul',
            'pembelian_pengepul_data'   => $pembelian_pengepul,
            'q'                         => $q,
            'pagination'                => $this->pagination->create_links(),
            'total_rows'                => $config['total_rows'],
            'start'                     => $start,
            'pembelian_pengepul_aktif'  => '1',
            'menu_aktif'                => 'pembelian_pengepul'
        );
        $res['datakonten'] = $this->load->view('pembelian_pengepul/pembelian_pengepul_tolak', $data, true);
        $this->load->view('layouts/main_view', $res);
    }

    public function detail($id=null)
    {
        $row        = $this->Pembelian_pengepul_model->get_by_id($id);
        $reason     = $this->Pembelian_pengepul_model->get_reason();
     
        if ($row) {
            $data = array(
                'tittle'                            => 'Pembelian Pengepul',
                'judul'                             => 'Pembelian Pengepul',
                'title_card'                        => 'Detail Pembelian Pengepul',
                'id_pembelian_pengepul'             => $row->id_pembelian_pengepul,
                'nama_pengepul'                     => $row->nama_pengepul,
                'id_pengepul'                       => $row->id_pengepul,
                'nama_bank_sampah'                  => $row->nama_bank_sampah,
                'id_status_pembelian_pengepul'      => $row->id_status_pembelian_pengepul,
                'nama_status_pembelian_pengepul'    => $row->nama_status_pembelian_pengepul,
                'nama_sampah'                       => $row->nama_sampah,
                'tgl_pembelian_sampah'              => $row->tgl_pembelian_sampah,
                'update_at'                         => $row->update_at,
                'nilai_pengepul'                    => $row->nilai_pengepul,
                'ulasan_pengepul'                   => $row->ulasan_pengepul,
                'tgl_review'                        => $row->tgl_review,
                'id_reason'                         => $row->id_reason,
                'desc_reason'                       => $row->desc_reason,
                'reason'                            => $reason,
                'pembelian_pengepul_aktif'          => '1',
                'menu_aktif'                        => 'pembelian_pengepul'
            );
            $res['datakonten'] = $this->load->view('pembelian_pengepul/pembelian_pengepul_detail', $data, true);
            $this->load->view('layouts/main_view', $res);
        } else {
            $this->session->set_flashdata('gagal', 'Data Tidak Di Temukan');
            redirect(site_url('pembelian_pengepul'));
        }
    }

    public function create()
    {
        $data = array(
            'button'                        => 'Create',
            'action'                        => site_url('pembelian_pengepul/create_action'),
            'id_pembelian_pengepul'         => set_value('id_pembelian_pengepul'),
            'id_pengepul'                   => set_value('id_pengepul'),
            'id_bank_sampah'                => set_value('id_bank_sampah'),
            'id_status_pembelian_pengepul'  => set_value('id_status_pembelian_pengepul'),
            'id_penjualan_bank_sampah'      => set_value('id_penjualan_bank_sampah'),
            'tgl_pembelian_sampah'          => set_value('tgl_pembelian_sampah'),
            'update_at'                     => set_value('update_at'),
            'nilai_pengepul'                => set_value('nilai_pengepul'),
            'ulasan_pengepul'               => set_value('ulasan_pengepul'),
            'tgl_review'                    => set_value('tgl_review'),
            'id_reason'                     => set_value('id_reason'),
        );
        $this->load->view('pembelian_pengepul/pembelian_pengepul_form', $data);
    }

    public function create_action()
    {
        $this->_rules();
        if ($this->form_validation->run() == FALSE) {
            $this->create();
        } else {
            $data = array(
                'id_pengepul'                   => $this->input->post('id_pengepul', TRUE),
                'id_bank_sampah'                => $this->input->post('id_bank_sampah', TRUE),
                'id_status_pembelian_pengepul'  => $this->input->post('id_status_pembelian_pengepul', TRUE),
                'id_penjualan_bank_sampah'      => $this->input->post('id_penjualan_bank_sampah', TRUE),
                'tgl_pembelian_sampah'          => $this->input->post('tgl_pembelian_sampah', TRUE),
                'update_at'                     => $this->input->post('update_at', TRUE),
                'nilai_pengepul'                => $this->input->post('nilai_pengepul', TRUE),
                'ulasan_pengepul'               => $this->input->post('ulasan_pengepul', TRUE),
                'tgl_review'                    => $this->input->post('tgl_review', TRUE),
                'id_reason'                     => $this->input->post('id_reason', TRUE),
            );

            $this->Pembelian_pengepul_model->insert($data);
            $this->session->set_flashdata('berhasil', 'Berhasil Menambahkan Data');
            redirect(site_url('pembelian_pengepul'));
        }
    }

    public function update($id)
    {
        $row = $this->Pembelian_pengepul_model->get_by_id($id);

        if ($row) {
            $data = array(
                'button'                        => 'Update',
                'action'                        => site_url('pembelian_pengepul/update_action'),
                'id_pembelian_pengepul'         => set_value('id_pembelian_pengepul', $row->id_pembelian_pengepul),
                'id_pengepul'                   => set_value('id_pengepul', $row->id_pengepul),
                'id_bank_sampah'                => set_value('id_bank_sampah', $row->id_bank_sampah),
                'id_status_pembelian_pengepul'  => set_value('id_status_pembelian_pengepul', $row->id_status_pembelian_pengepul),
                'id_penjualan_bank_sampah'      => set_value('id_penjualan_bank_sampah', $row->id_penjualan_bank_sampah),
                'tgl_pembelian_sampah'          => set_value('tgl_pembelian_sampah', $row->tgl_pembelian_sampah),
                'update_at'                     => set_value('update_at', $row->update_at),
                'nilai_pengepul'                => set_value('nilai_pengepul', $row->nilai_pengepul),
                'ulasan_pengepul'               => set_value('ulasan_pengepul', $row->ulasan_pengepul),
                'tgl_review'                    => set_value('tgl_review', $row->tgl_review),
                'id_reason'                     => set_value('id_reason', $row->id_reason),
                'menu_aktif'                    => 'pembelian_pengepul'
            );
            $this->load->view('pembelian_pengepul/pembelian_pengepul_form', $data);
        } else {
            $this->session->set_flashdata('gagal', 'Data Tidak Ditemukan');
            redirect(site_url('pembelian_pengepul'));
        }
    }

    public function update_action()
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->update($this->input->post('id_pembelian_pengepul', TRUE));
        } else {
            $data = array(
                'id_pengepul'                   => $this->input->post('id_pengepul', TRUE),
                'id_bank_sampah'                => $this->input->post('id_bank_sampah', TRUE),
                'id_status_pembelian_pengepul'  => $this->input->post('id_status_pembelian_pengepul', TRUE),
                'id_penjualan_bank_sampah'      => $this->input->post('id_penjualan_bank_sampah', TRUE),
                'tgl_pembelian_sampah'          => $this->input->post('tgl_pembelian_sampah', TRUE),
                'update_at'                     => $this->input->post('update_at', TRUE),
                'nilai_pengepul'                => $this->input->post('nilai_pengepul', TRUE),
                'ulasan_pengepul'               => $this->input->post('ulasan_pengepul', TRUE),
                'tgl_review'                    => $this->input->post('tgl_review', TRUE),
                'id_reason'                     => $this->input->post('id_reason', TRUE),
            );

            $this->Pembelian_pengepul_model->update($this->input->post('id_pembelian_pengepul', TRUE), $data);
            $this->session->set_flashdata('berhasil', 'Berhasil Merubah Data');
            redirect(site_url('pembelian_pengepul'));
        }
    }

    public function ubahstatus($id)
    {
        $row = $this->Pembelian_pengepul_model->get_by_id($id);
        if ($row) {
            $data = array(
                'id_status_pembelian_pengepul' => 2,
            );
            
            $this->Pembelian_pengepul_model->update($id, $data);
            $datas = array(
                'id_status_penjualan_bank_sampah'     => 2,
            );
            $this->db->where('id_penjualan_bank_sampah', $row->id_penjualan_bank_sampah);
            $this->db->update('penjualan_bank_sampah', $datas);
            $this->session->set_flashdata('berhasil', 'Berhasil Konfirmasi Pembayaran');
            redirect(site_url('pembelian_pengepul'));
        } else {
            $this->session->set_flashdata('gagal', 'Data Tidak Ditemukan');
            redirect(site_url('pembelian_pengepul'));
        }
    }

    function get_by_bukti()
    {
        $img=$this->input->get('img');
        $cek='https://kompis-pengepul.s3.ap-southeast-1.amazonaws.com/'.$img;
        $row = $this->Pembelian_pengepul_model->get_by_poto($cek);
        if($row){
            echo getkontenall($img,'pengepul');  
        } else {
            $this->session->set_flashdata('gagal', 'Gambar Tidak Ditemukan');
            redirect('pembelian_pengepul');
        }
    }

    public function _rules()
    {
        $this->form_validation->set_rules('id_pengepul', 'id pengepul', 'trim|required');
        $this->form_validation->set_rules('id_bank_sampah', 'id bank sampah', 'trim|required');
        $this->form_validation->set_rules('id_status_pembelian_pengepul', 'id status pembelian pengepul', 'trim|required');
        $this->form_validation->set_rules('id_penjualan_bank_sampah', 'id penjualan bank sampah', 'trim|required');
        $this->form_validation->set_rules('tgl_pembelian_sampah', 'tgl pembelian sampah', 'trim|required');
        $this->form_validation->set_rules('update_at', 'update at', 'trim|required');
        $this->form_validation->set_rules('nilai_pengepul', 'nilai pengepul', 'trim|required');
        $this->form_validation->set_rules('ulasan_pengepul', 'ulasan pengepul', 'trim|required');
        $this->form_validation->set_rules('tgl_review', 'tgl review', 'trim|required');
        $this->form_validation->set_rules('id_reason', 'id reason', 'trim|required');
        $this->form_validation->set_rules('id_pembelian_pengepul', 'id_pembelian_pengepul', 'trim');
        $this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

    public function tolak_pembayaran()
    {
        $id     = $this->input->post('id_pengepul');
        $idpem  = $this->input->post('id_pembelian_pengepul');
        $reason = $this->input->post('reason');
        $row    = $this->Pembelian_pengepul_model->get_by_id($idpem);
        $data   = array(
            'reason_reject'     => $reason,
        );
        $this->Pembelian_pengepul_model->updatepengepul($id, $data); 

        $data = array(
            'id_reason'                     => $reason,
            'id_status_pembelian_pengepul'  => 8,
        );
        $this->Pembelian_pengepul_model->update($idpem, $data);
        
        $datas = array(
            'id_status_penjualan_bank_sampah'     => 0,
        );
        $this->db->where('id_penjualan_bank_sampah', $row->id_penjualan_bank_sampah);
        $this->db->update('penjualan_bank_sampah', $datas);
        redirect('pembelian_pengepul');
    } 

    public function kembalikan()
    {
        $id     = $this->input->post('id_pengepul');
        $idpem  = $this->input->post('id_pembelian_pengepul');
        $row    = $this->Pembelian_pengepul_model->get_by_id($id_pem);
        $data = array(
            'id_status_pembelian_pengepul'     => 1,
        );
        $this->Pembelian_pengepul_model->update($idpem, $data);
        $data = array(
            'id_status_penjualan_bank_sampah'     => 0,
        );
        $this->db->where('id_penjualan_bank_sampah', $row->id_penjualan_bank_sampah);
        $this->db->update('penjualan_bank_sampah', $data);
        redirect('pembelian_pengepul/detail/'.$this->input->post('id_pembelian_pengepul'));
    }
}

/* End of file Pembelian_pengepul.php */
/* Location: ./application/controllers/Pembelian_pengepul.php */
/* Please DO NOT modify this information : */
/* Generated by Harviacode Codeigniter CRUD Generator 2020-03-18 08:24:05 */
/* http://harviacode.com */
