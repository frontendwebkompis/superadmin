<!doctype html>
<div class="row">
    <!-- Left col -->
    <section class="col-lg connectedSortable ui-sortable">
        <!-- Custom tabs (Charts with tabs)-->
        <h2 style="margin-top:0px">Pengolahan Data Pembelian_pengepul </h2>
        <form action="<?php echo $action; ?>" method="post">
	    <div class="form-group">
            <label for="varchar">Id Pengepul <?php echo form_error('id_pengepul') ?></label>
            <input type="text" class="form-control" name="id_pengepul" id="id_pengepul" placeholder="Id Pengepul" value="<?php echo $id_pengepul; ?>" />
        </div>
	    <div class="form-group">
            <label for="varchar">Id Bank Sampah <?php echo form_error('id_bank_sampah') ?></label>
            <input type="text" class="form-control" name="id_bank_sampah" id="id_bank_sampah" placeholder="Id Bank Sampah" value="<?php echo $id_bank_sampah; ?>" />
        </div>
	    <div class="form-group">
            <label for="int">Id Status Pembelian Pengepul <?php echo form_error('id_status_pembelian_pengepul') ?></label>
            <input type="text" class="form-control" name="id_status_pembelian_pengepul" id="id_status_pembelian_pengepul" placeholder="Id Status Pembelian Pengepul" value="<?php echo $id_status_pembelian_pengepul; ?>" />
        </div>
	    <div class="form-group">
            <label for="varchar">Id Penjualan Bank Sampah <?php echo form_error('id_penjualan_bank_sampah') ?></label>
            <input type="text" class="form-control" name="id_penjualan_bank_sampah" id="id_penjualan_bank_sampah" placeholder="Id Penjualan Bank Sampah" value="<?php echo $id_penjualan_bank_sampah; ?>" />
        </div>
	    <div class="form-group">
            <label for="datetime">Tgl Pembelian Sampah <?php echo form_error('tgl_pembelian_sampah') ?></label>
            <input type="text" class="form-control" name="tgl_pembelian_sampah" id="tgl_pembelian_sampah" placeholder="Tgl Pembelian Sampah" value="<?php echo $tgl_pembelian_sampah; ?>" />
        </div>
	    <div class="form-group">
            <label for="timestamp">Update At <?php echo form_error('update_at') ?></label>
            <input type="text" class="form-control" name="update_at" id="update_at" placeholder="Update At" value="<?php echo $update_at; ?>" />
        </div>
	    <div class="form-group">
            <label for="float">Nilai Pengepul <?php echo form_error('nilai_pengepul') ?></label>
            <input type="text" class="form-control" name="nilai_pengepul" id="nilai_pengepul" placeholder="Nilai Pengepul" value="<?php echo $nilai_pengepul; ?>" />
        </div>
	    <div class="form-group">
            <label for="varchar">Ulasan Pengepul <?php echo form_error('ulasan_pengepul') ?></label>
            <input type="text" class="form-control" name="ulasan_pengepul" id="ulasan_pengepul" placeholder="Ulasan Pengepul" value="<?php echo $ulasan_pengepul; ?>" />
        </div>
	    <div class="form-group">
            <label for="timestamp">Tgl Review <?php echo form_error('tgl_review') ?></label>
            <input type="text" class="form-control" name="tgl_review" id="tgl_review" placeholder="Tgl Review" value="<?php echo $tgl_review; ?>" />
        </div>
	    <div class="form-group">
            <label for="int">Id Reason <?php echo form_error('id_reason') ?></label>
            <input type="text" class="form-control" name="id_reason" id="id_reason" placeholder="Id Reason" value="<?php echo $id_reason; ?>" />
        </div>
	    <input type="hidden" name="id_pembelian_pengepul" value="<?php echo $id_pembelian_pengepul; ?>" /> 
	    <button type="submit" class="btn btn-primary">Simpan
</button> 
	    <a href="<?php echo site_url('pembelian_pengepul') ?>" class="btn btn-default">Batal</a>
	</form>
    </div>
    <!-- /.card-body -->
        </div>
        <!-- /.card -->
    </section>
    <!-- /.Left col -->

</div>