<script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>

<div class="row">
	<section class="col-lg connectedSortable ui-sortable">
		<div class="card card-info">
            <div class="card-header">
                <div class="card-title">
                  <?php echo $title_card?>
                </div>
              </div>
			<div class="card-body">
				<div class="tab-content p-0">
                        <div class="row">
                            <div class="col-md-12 mb-2">
                                <a href="<?php echo base_url('pembelian_pengepul'); ?>" class="btn btn-primary btn-sm">List Pembelian Pengepul</a>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-5 mb-2">
                                <?= filterDate($this->input->get('awal'), $this->input->get('akhir'), base_url('pembelian_pengepul/tolak')); ?>
                            </div>
                            <div class="col-md-3 offset-md-4 mb-2">
                                <?= search(site_url('pembelian_pengepul/tolak'), site_url('pembelian_pengepul/tolak'), $q) ?>
                            </div>
                        </div>
                        <div class="tab-content p-0" style="overflow:auto">
                        <table class="table table-bordered table-striped table-condensed table-hover table-sm" style="margin-bottom: 10px">

                            <tr>
                                <th class="text-center" width="10px">No</th>
                                <th class="text-center">Pengepul</th>
                                <th class="text-center">Bank Sampah</th>
                                <th class="text-center">Sekolah</th>
                                <th class="text-center">Status Pembelian Pengepul</th>
                                <th class="text-center">Nama Sampah</th>
                                <th class="text-center">Tanggal Pembelian Sampah</th>
                                <th class="text-center">Aksi</th>
                            </tr><?php
                                    if($total_rows == 0){
                                        echo '<tr><td colspan="8" bgcolor="grey" style="color: white; font-weight: bold; text-align: center;">Data Tidak Ditemukan</td></tr>';
                                    } else {
                                    foreach ($pembelian_pengepul_data as $pembelian_pengepul) {
                                    ?>
                                <tr>
                                    <td class="text-center"><?php echo ++$start ?></td>
                                    <td><?php echo $pembelian_pengepul->nama_pengepul ?></td>
                                    <td><?php 
                                    if($pembelian_pengepul->nama_bank_sampah == null){
                                        echo '<b><span class="badge badge-danger">-</span></b>';
                                    } else {
                                        echo $pembelian_pengepul->nama_bank_sampah;
                                    } ?></td>
                                    <td><?php
                                    if($pembelian_pengepul->nama_sekolah == null){
                                        echo '<b><span class="badge badge-danger">-</span></b>';
                                    } else {
                                        echo $pembelian_pengepul->nama_sekolah;
                                    } ?></td>
                                    <td class="text-center"><?php echo $pembelian_pengepul->nama_status_pembelian_pengepul ?></td>
                                     <td class="text-center"><?php
                                    if($pembelian_pengepul->nama_sampah == null){
                                        echo $pembelian_pengepul->nama_sampah_sekolah;
                                    } else {
                                        echo $pembelian_pengepul->nama_sampah; 
                                    }
                                    ?></td>
                                    <td class="text-center"><?php echo fulldate($pembelian_pengepul->tgl_pembelian_sampah) ?></td>
                                    <td class="text-center" width="50px">
                                    <div class="btn-group">  
                                        <a href="<?php echo site_url('pembelian_pengepul/detail/' . $pembelian_pengepul->id_pembelian_pengepul); ?>" data-toogle="tooltip" title="Lihat">
                                        <button type="button" class="btn btn-info btn-sm"><i class="fas fa-info-circle"></i></button></a>
                                        <?php
                                        if($pembelian_pengepul->id_status_pembelian_pengepul==1){ ?>
                                        <a href="#" class="btn btn-success btn-sm" data-toggle="tooltip" data-placement="top" title="Ubah Status" onclick="confirm('<?= $pembelian_pengepul->id_pembelian_pengepul ?>')" ><i class="fas fa-check"></i></a>
                                        <?php } 
                                        } ?>
                                    </div>
                                            
                                    </td>
                                </tr>
                            <?php
                                    }
                            ?>
                        </table>
                        </div>
                        <?= footer($total_rows, $pagination, '') ?>
			</div>
		</div>
	</section>
</div>

<script type="text/javascript">
    setDateFilter('akhir', 'awal');

function confirm(res) {
    Swal.fire({
    title: 'Anda Yakin Mau Ubah Status?',
    text: "Tidak bisa dikembalikan jika sudah diubah!",
    icon: 'warning',
    showCancelButton: true,
    confirmButtonColor: '#3085d6',
    cancelButtonColor: '#d33',
    confirmButtonText: 'Ya, Ubah!',
    cancelButtonText: 'Batal'
    }).then((result) => {
    if (result.value) {
        Swal.fire(
        'Status Terubah!',
        'Status sudah berubah.',
        'success'
        )
        window.location='<?php echo base_url().'pembelian_pengepul/ubahstatus/'; ?>'+res;
    }
    });
}
</script>