<script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
<!-----------------------------------Konten-------------------------------------------------- -->
<div class="row">

  <!-- Left col -->
  <section class="col-lg connectedSortable ui-sortable">
    <!-- Custom tabs (Charts with tabs)-->
    <div class="card card-info">
      <div class="card-header">
        <div class="card-title">
          <?php echo $title_card ?>
        </div>
      </div>
      <div class="card-body">

        <div class="row">
          <div class="col-md-8">
            <table class="table table-bordered table-striped table-condensed table-sm" style="margin-bottom: 20px">
              <tr>
                <td class="font-weight-bold pl-3">Pengepul</td>
                <td><?php echo $nama_pengepul; ?></td>
              </tr>
              <tr>
                <td class="font-weight-bold pl-3">Bank Sampah</td>
                <td><?php echo $nama_bank_sampah; ?></td>
              </tr>
              <tr>
                <td class="font-weight-bold pl-3">Status Pembelian Pengepul</td>
                <td><?php echo $nama_status_pembelian_pengepul; ?></td>
              </tr>
              <tr>
                <td class="font-weight-bold pl-3">Nama Sampah</td>
                <td><?php echo $nama_sampah; ?></td>
              </tr>
              <tr>
                <td class="font-weight-bold pl-3">Tanggal Pembelian Sampah</td>
                <td><?php echo fulldate($tgl_pembelian_sampah); ?></td>
              </tr>
              <tr>
                <td class="font-weight-bold pl-3">Update Tanggal</td>
                <td><?php echo fulldate($update_at); ?></td>
              </tr>
              <tr>
                <td class="font-weight-bold pl-3">Nilai Pengepul</td>
                <td><?php echo $nilai_pengepul; ?></td>
              </tr>
              <tr>
                <td class="font-weight-bold pl-3">Ulasan Pengepul</td>
                <td><?php echo $ulasan_pengepul; ?></td>
              </tr>
              <tr>
                <td class="font-weight-bold pl-3">Tanggal Review</td>
                <td><?php echo fulldate($tgl_review); ?></td>
              </tr>
              <tr>
                <td class="font-weight-bold pl-3">Reason</td>
                <td><span class="badge badge-danger"><?php echo $desc_reason; ?></span></td>
              </tr>
            </table>
          </div>
          <div class="col-md-4">
            <!--                 <div class="row justify-content-center font-weight-bold">
                  Foto Apsi
                </div>
                <div class="row justify-content-center">
                  <a href="<?= base_url() ?>pembelian_pengepul/get_by_bukti?img=bukti_pembayaran/<?php echo basename($bukti_pembayaran) ?>" data-toggle="lightbox" data-max-width="800" data-title="Bukti Pembayaran">
                    <img src="<?= base_url() ?>pembelian_pengepul/get_by_bukti?img=bukti_pembayaran/<?php echo basename($bukti_pembayaran) ?>" class="img-fluid" style="max-width: 250px; max-height: 150px;" alt="Bukti Pembayaran">
                  </a>
                </div> -->
          </div>
        </div>

        <div class="btn-group">
          <a href="<?php echo site_url('pembelian_pengepul') ?>" class="btn btn-primary btn-sm" style="width: 100px;">Kembali</a>

          <?php if ($id_status_pembelian_pengepul == 1) { ?>
            <a href="#" class="btn btn-success btn-sm" data-toggle="tooltip" data-placement="top" title="Ubah Status" onclick="confirm('<?= $id_pembelian_pengepul ?>')"><i class="fas fa-check"></i> Terima Pembayaran</a>
          <?php } ?>

          <!-- <?php
                if ($pembelian_pengepul->id_status_pembelian_pengepul == 1)
                  echo anchor(site_url('pembelian_pengepul/ubahstatus/' . $pembelian_pengepul->id_pembelian_pengepul), 'Ubah Status', 'class="btn btn-primary btn-sm"', 'onclick="javasciprt: return confirm(\'Yakin Konfirmasi Pembayaran ?\')"');
                ?> -->
          <?php if ($id_status_pembelian_pengepul != 8) { ?>
            <button type="button" class="btn btn-danger btn-sm" style="display: none" data-toggle="modal" data-target="#modal-default">
              Tolak Pembayaran
            </button>
          <?php } else {
          ?>
            <button type="button" class="btn btn-warning btn-sm" style="display: none" data-toggle="modal" data-target="#modal-kembali">
              Batalkan Pemblokiran
            </button>
          <?php } ?>

        </div>
      </div>
    </div><!-- /.card-body -->
</div>
<!-- /.card -->

</section>
<!-- /.Left col -->
</div>

<div class="modal fade" id="modal-default">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Tolak Pembayaran</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form method="post" action="<?php echo site_url('pembelian_pengepul/tolak_pembayaran') ?>">
        <div class="modal-body">

          <div class="form-group">
            <label for="exampleFormControlSelect1">Alasan Tolak</label>
            <select name="reason" class="form-control">
              <option value="">Pilih Alasan Tolak</option>
              <?php foreach ($reason as $res) : ?>
                <option value="<?php echo $res->id_reason; ?>"><?php echo $res->desc_reason; ?></option>
              <?php endforeach ?>
            </select>
          </div>
          <input type="hidden" name="id_pengepul" value="<?= $id_pengepul ?>">
          <input type="hidden" name="id_pembelian_pengepul" value="<?= $id_pembelian_pengepul ?>">

        </div>
        <div class="modal-footer justify-content-between">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>

          <button class="btn btn-danger">Tolak Pembayaran</button>

        </div>
      </form>

    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
<!-- /.modal -->

<div class="modal fade" id="modal-kembali">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Batalkan Pemblokiran</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form method="post" action="<?php echo site_url('pembelian_pengepul/kembalikan') ?>">
        <div class="modal-body">
          <input type="hidden" name="id_pengepul" value="<?= $id_pengepul ?>">
          <input type="hidden" name="id_pembelian_pengepul" value="<?= $id_pembelian_pengepul ?>">

        </div>
        <div class="modal-footer justify-content-between">
          <button type="button" class="btn btn-default" data-dismiss="modal">Tidak</button>

          <button class="btn btn-success">Batalkan Pemblokiran</button>

        </div>
      </form>

    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
<!-- /.modal -->

<!-- Ekko Lightbox -->
<script src="<?= base_url('assets/plugins/ekko-lightbox/ekko-lightbox.min.js') ?>"></script>
<script>
  $(function() {
    $(document).on('click', '[data-toggle="lightbox"]', function(event) {
      event.preventDefault();
      $(this).ekkoLightbox({
        alwaysShowClose: true
      });
    });

    $('.filter-container').filterizr({
      gutterPixels: 3
    });
    $('.btn[data-filter]').on('click', function() {
      $('.btn[data-filter]').removeClass('active');
      $(this).addClass('active');
    });
  })
</script>

<script>
  function confirm(res) {
    Swal.fire({
      title: 'Anda Yakin Mau Ubah Status?',
      text: "Tidak bisa dikembalikan jika sudah diubah!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Ya, Ubah!',
      cancelButtonText: 'Batal'
    }).then((result) => {
      if (result.value) {
        Swal.fire(
          'Status Terubah!',
          'Status sudah berubah.',
          'success'
        )
        window.location = '<?php echo base_url() . 'pembelian_pengepul/ubahstatus/'; ?>' + res;
      }
    });
  }
</script>