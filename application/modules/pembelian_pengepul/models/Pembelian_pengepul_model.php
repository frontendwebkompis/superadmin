<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Pembelian_pengepul_model extends CI_Model
{

    public $table = 'pembelian_pengepul';
    public $id = 'id_pembelian_pengepul';
    public $order = 'DESC';
    public $id_order = 'pembelian_pengepul.id_pembelian_pengepul';

    function __construct()
    {
        parent::__construct();
    }

    function get_all()
    {
        $this->db->order_by($this->id, $this->order);
        return $this->db->get($this->table)->result();
    }

    function get_by_poto($id)
    {
        $this->db->where('bukti_pembayaran', $id);
        return $this->db->get($this->table)->row();
    }

    function get_reason()
    {
        $this->db->where('id_proses', 11);
        return $this->db->get('reason')->result();
    }

    function get_by_id($id)
    {
        $this->db->join('pembelian_pengepul_detail', 'pembelian_pengepul_detail.id_pembelian_pengepul = pembelian_pengepul.id_pembelian_pengepul','left');
        $this->db->join('pengepul', 'pengepul.id_pengepul = pembelian_pengepul.id_pengepul');
        $this->db->join('bank_sampah', 'bank_sampah.id_bank_sampah = pembelian_pengepul_detail.id_bank_sampah','left');
        $this->db->join('status_pembelian_pengepul', 'status_pembelian_pengepul.id_status_pembelian_pengepul = pembelian_pengepul.id_status_pembelian_pengepul','left');
        $this->db->join('penjualan_bank_sampah', 'penjualan_bank_sampah.id_penjualan_bank_sampah = pembelian_pengepul_detail.id_penjualan_bank_sampah','left');
        $this->db->join('reason', 'reason.id_reason = pembelian_pengepul.id_reason','left');

        $this->db->where($this->id_order, $id);
        return $this->db->get($this->table)->row();
    }

    // get total rows
    function total_rows_tolak($q = NULL,$awal,$akhir)
    {
        $this->db->select('pembelian_pengepul.*, pembelian_pengepul_detail.*, pengepul.*, bank_sampah.*, sekolah.*, status_pembelian_pengepul.*, penjualan_sekolah.*, penjualan_sekolah.nama_sampah as nama_sampah_sekolah, penjualan_bank_sampah.*');
        $this->db->join('pembelian_pengepul_detail', 'pembelian_pengepul_detail.id_pembelian_pengepul = pembelian_pengepul.id_pembelian_pengepul','left join');
        $this->db->join('pengepul', 'pengepul.id_pengepul = pembelian_pengepul.id_pengepul','left join');
        $this->db->join('bank_sampah', 'bank_sampah.id_bank_sampah = pembelian_pengepul_detail.id_bank_sampah','left');
        $this->db->join('sekolah', 'sekolah.id_sekolah = pembelian_pengepul_detail.id_sekolah','left');
        $this->db->join('status_pembelian_pengepul', 'status_pembelian_pengepul.id_status_pembelian_pengepul = pembelian_pengepul.id_status_pembelian_pengepul','left join');
        $this->db->join('penjualan_sekolah', 'penjualan_sekolah.id_penjualan_sekolah = pembelian_pengepul_detail.id_penjualan_sekolah','left');
        $this->db->join('penjualan_bank_sampah', 'penjualan_bank_sampah.id_penjualan_bank_sampah = pembelian_pengepul_detail.id_penjualan_bank_sampah','left');
        $this->db->where("(UNIX_TIMESTAMP(". $this->table .".tgl_pembelian_sampah) BETWEEN UNIX_TIMESTAMP('" . $awal . "') AND UNIX_TIMESTAMP('" . $akhir  . " 23:59') ) ");
        $this->db->order_by('tgl_pembelian_sampah', $this->order);
         $this->db->where('pembelian_pengepul.id_status_pembelian_pengepul',8);
        $this->db->group_start();
        $this->db->or_like('pengepul.nama_pengepul', $q);
        $this->db->or_like('bank_sampah.nama_bank_sampah', $q);
        $this->db->or_like('nilai_pengepul', $q);
        $this->db->or_like('ulasan_pengepul', $q);
        $this->db->group_end();
       
        $this->db->from($this->table);
        return $this->db->count_all_results();
    }
  // get total rows
    function total_rows($q = NULL,$awal,$akhir)
    {
        $this->db->select('pembelian_pengepul.*, pembelian_pengepul_detail.*, pengepul.*, bank_sampah.*, sekolah.*, status_pembelian_pengepul.*, penjualan_sekolah.*, penjualan_sekolah.nama_sampah as nama_sampah_sekolah, penjualan_bank_sampah.*');
        $this->db->join('pembelian_pengepul_detail', 'pembelian_pengepul_detail.id_pembelian_pengepul = pembelian_pengepul.id_pembelian_pengepul','left join');
        $this->db->join('pengepul', 'pengepul.id_pengepul = pembelian_pengepul.id_pengepul','left join');
        $this->db->join('bank_sampah', 'bank_sampah.id_bank_sampah = pembelian_pengepul_detail.id_bank_sampah','left');
        $this->db->join('sekolah', 'sekolah.id_sekolah = pembelian_pengepul_detail.id_sekolah','left');
        $this->db->join('status_pembelian_pengepul', 'status_pembelian_pengepul.id_status_pembelian_pengepul = pembelian_pengepul.id_status_pembelian_pengepul','left join');
        $this->db->join('penjualan_sekolah', 'penjualan_sekolah.id_penjualan_sekolah = pembelian_pengepul_detail.id_penjualan_sekolah','left');
        $this->db->join('penjualan_bank_sampah', 'penjualan_bank_sampah.id_penjualan_bank_sampah = pembelian_pengepul_detail.id_penjualan_bank_sampah','left');
        $this->db->where("(UNIX_TIMESTAMP(". $this->table .".tgl_pembelian_sampah) BETWEEN UNIX_TIMESTAMP('" . $awal . "') AND UNIX_TIMESTAMP('" . $akhir  . " 23:59') ) ");
        $this->db->order_by('tgl_pembelian_sampah', $this->order);
        $this->db->where('pembelian_pengepul.id_status_pembelian_pengepul !=',8);
        $this->db->group_start();
        $this->db->or_like('pengepul.nama_pengepul', $q);
        $this->db->or_like('bank_sampah.nama_bank_sampah', $q);
        $this->db->or_like('nilai_pengepul', $q);
        $this->db->or_like('ulasan_pengepul', $q);
        $this->db->group_end();
       
        $this->db->from($this->table);
        return $this->db->count_all_results();
    }

    // get data with limit and search
    function get_limit_data_tolak($limit, $start = 0, $q = NULL,$awal,$akhir)
    {
        $this->db->select('pembelian_pengepul.*, pembelian_pengepul_detail.*, pengepul.*, bank_sampah.*, sekolah.*, status_pembelian_pengepul.*, penjualan_sekolah.*, penjualan_sekolah.nama_sampah as nama_sampah_sekolah, penjualan_bank_sampah.*, pembelian_pengepul_detail.nama_sampah as nama_sampah');
        $this->db->join('pembelian_pengepul_detail', 'pembelian_pengepul_detail.id_pembelian_pengepul = pembelian_pengepul.id_pembelian_pengepul','left join');
        $this->db->join('pengepul', 'pengepul.id_pengepul = pembelian_pengepul.id_pengepul','left join');
        $this->db->join('bank_sampah', 'bank_sampah.id_bank_sampah = pembelian_pengepul_detail.id_bank_sampah','left');
        $this->db->join('sekolah', 'sekolah.id_sekolah = pembelian_pengepul_detail.id_sekolah','left');
        $this->db->join('status_pembelian_pengepul', 'status_pembelian_pengepul.id_status_pembelian_pengepul = pembelian_pengepul.id_status_pembelian_pengepul','left join');
        $this->db->join('penjualan_sekolah', 'penjualan_sekolah.id_penjualan_sekolah = pembelian_pengepul_detail.id_penjualan_sekolah','left');
        $this->db->join('penjualan_bank_sampah', 'penjualan_bank_sampah.id_penjualan_bank_sampah = pembelian_pengepul_detail.id_penjualan_bank_sampah','left');
        $this->db->where("(UNIX_TIMESTAMP(". $this->table .".tgl_pembelian_sampah) BETWEEN UNIX_TIMESTAMP('" . $awal . "') AND UNIX_TIMESTAMP('" . $akhir  . " 23:59') ) ");
        $this->db->order_by('tgl_pembelian_sampah', $this->order);
        $this->db->where('pembelian_pengepul.id_status_pembelian_pengepul',8);
        $this->db->group_start();
        $this->db->or_like('pengepul.nama_pengepul', $q);
        $this->db->or_like('bank_sampah.nama_bank_sampah', $q);
        $this->db->or_like('nilai_pengepul', $q);
        $this->db->or_like('ulasan_pengepul', $q);
        $this->db->group_end();
        $this->db->limit($limit, $start);
        return $this->db->get($this->table)->result();
    }
  // get data with limit and search
    function get_limit_data($limit, $start = 0, $q = NULL,$awal,$akhir)
    {
        $this->db->select('pembelian_pengepul.*, pembelian_pengepul_detail.*, pengepul.*, bank_sampah.*, sekolah.*, status_pembelian_pengepul.*, penjualan_sekolah.*, penjualan_sekolah.nama_sampah as nama_sampah_sekolah, penjualan_bank_sampah.*, pembelian_pengepul_detail.nama_sampah as nama_sampah');
        $this->db->join('pembelian_pengepul_detail', 'pembelian_pengepul_detail.id_pembelian_pengepul = pembelian_pengepul.id_pembelian_pengepul','left join');
        $this->db->join('pengepul', 'pengepul.id_pengepul = pembelian_pengepul.id_pengepul','left join');
        $this->db->join('bank_sampah', 'bank_sampah.id_bank_sampah = pembelian_pengepul_detail.id_bank_sampah','left');
        $this->db->join('sekolah', 'sekolah.id_sekolah = pembelian_pengepul_detail.id_sekolah','left');
        $this->db->join('status_pembelian_pengepul', 'status_pembelian_pengepul.id_status_pembelian_pengepul = pembelian_pengepul.id_status_pembelian_pengepul','left join');
        $this->db->join('penjualan_sekolah', 'penjualan_sekolah.id_penjualan_sekolah = pembelian_pengepul_detail.id_penjualan_sekolah','left');
        $this->db->join('penjualan_bank_sampah', 'penjualan_bank_sampah.id_penjualan_bank_sampah = pembelian_pengepul_detail.id_penjualan_bank_sampah','left');

        $this->db->where("(UNIX_TIMESTAMP(". $this->table .".tgl_pembelian_sampah) BETWEEN UNIX_TIMESTAMP('" . $awal . "') AND UNIX_TIMESTAMP('" . $akhir  . " 23:59') ) ");
        $this->db->order_by('tgl_pembelian_sampah', $this->order);
        $this->db->where('pembelian_pengepul.id_status_pembelian_pengepul !=',8);
        $this->db->group_start();
        $this->db->or_like('pengepul.nama_pengepul', $q);
        $this->db->or_like('bank_sampah.nama_bank_sampah', $q);
        $this->db->or_like('nilai_pengepul', $q);
        $this->db->or_like('ulasan_pengepul', $q);
        $this->db->group_end();
        $this->db->limit($limit, $start);
        return $this->db->get($this->table)->result();
    }

    // insert data
    function insert($data)
    {
        $this->db->insert($this->table, $data);
    }

    // update data
    function update($id, $data)
    {
        $this->db->where($this->id, $id);
        $this->db->update($this->table, $data);
    }

    function updatepengepul($id, $data)
    {
        $this->db->where('id_pengepul', $id);
        $this->db->update('pengepul', $data);
    }

    // delete data
    function delete($id)
    {
        $this->db->where($this->id, $id);
        $this->db->delete($this->table);
    }
}

/* End of file Pembelian_pengepul_model.php */
/* Location: ./application/models/Pembelian_pengepul_model.php */
/* Please DO NOT modify this information : */
/* Generated by Harviacode Codeigniter CRUD Generator 2020-03-18 08:24:05 */
/* http://harviacode.com */



        // $this->db->select('pembelian_pengepul.*, pembelian_pengepul_detail.*, pengepul.*, bank_sampah.*, sekolah.*, status_pembelian_pengepul.*, penjualan_sekolah.*, penjualan_sekolah.nama_sampah as nama_sampah_sekolah, penjualan_bank_sampah.*, penjualan_bank_sampah.nama_sampah as nama_sampah');
        // $this->db->join('pembelian_pengepul_detail', 'pembelian_pengepul_detail.id_pembelian_pengepul = pembelian_pengepul.id_pembelian_pengepul','left join');
        // $this->db->join('pengepul', 'pengepul.id_pengepul = pembelian_pengepul.id_pengepul','left join');
        // $this->db->join('bank_sampah', 'bank_sampah.id_bank_sampah = pembelian_pengepul_detail.id_bank_sampah','left');
        // $this->db->join('sekolah', 'sekolah.id_sekolah = pembelian_pengepul.id_sekolah','left');
        // $this->db->join('status_pembelian_pengepul', 'status_pembelian_pengepul.id_status_pembelian_pengepul = pembelian_pengepul.id_status_pembelian_pengepul','left join');
        // $this->db->join('penjualan_sekolah', 'penjualan_sekolah.id_penjualan_sekolah = pembelian_pengepul.id_penjualan_sekolah','left');
        // $this->db->join('penjualan_bank_sampah', 'penjualan_bank_sampah.id_penjualan_bank_sampah = pembelian_pengepul.id_penjualan_bank_sampah','left');