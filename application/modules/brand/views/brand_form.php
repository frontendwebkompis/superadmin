<div class="row">
	<section class="col-lg connectedSortable ui-sortable">
		<div class="card card-info">
			<div class="card-header">
                <div class="card-title">
                  <?php echo $title_card?>
                </div>
              </div>
			<div class="card-body">
				<div class="tab-content p-0">

					<form id="dataform" action="<?php echo $action; ?>" method="post" enctype="multipart/form-data">
						<div class="form-group">
							<label for="varchar">Nama Brand <?php echo form_error('nama_brand') ?></label>
							<input required type="text" class="form-control" name="nama_brand" id="nama_brand"
							placeholder="Nama Brand" value="<?php echo $nama_brand; ?>" />
						</div>
						<div class="form-group">
							<label for="varchar">Logo Brand <?php echo form_error('logo_brand') ?></label>
							<?php if($logo_brand !=""){ ?>
								<div class="row justify-content-center mb-2">
									<img style="height:100px" src="<?php echo base_url(); ?>brand/get_image?img=brand/<?php echo basename($logo_brand); ?>" alt="Gambar Brand">
								</div>
								<div class="row justify-content-center mb-2">
									<button class="btn btn-sm btn-secondary" type="button" onclick="cek()"><i class="fas fa-image"></i> Ganti Gambar</button>
								</div>
							<input type="file" style="display:none" accept="image/*" class="form-control " name="logo_brandbaru"
								id="logo_brandbaru">
							<input type="hidden" id="logo_brand" accept="image/*" value="<?php echo $logo_brand ?>" name="logo_brand">
							<?php }else{ ?>
							<input required type="file" accept="image/*" class="form-control" name="logo_brandbaru" id="logo_brandbaru">
							<?php } ?>
						</div>
						<div class="form-group">
							<label for="varchar">Detail Brand <?php echo form_error('detail_brand') ?></label>
							<input required type="text" class="form-control" name="detail_brand" id="detail_brand"
							placeholder="Detail Brand" value="<?php echo $detail_brand; ?>" />
						</div>
						<input type="hidden" name="id_brand" value="<?php echo $id_brand; ?>" />
						<div id="loader" class="alert alert-info" style="display:none">
						    <strong>Info!</strong> Sedang Proses....
						</div>
						<button id="ceksub" type="submit" class="btn btn-info" style="width: 100px;">Simpan
						</button>
						<a href="<?php echo site_url('brand') ?>" class="btn btn-default" style="width: 100px;">Batal</a>
					</form>
					<script>
						function cek() {
							var x = document.getElementById("logo_brandbaru");
							if (x.style.display === "none") {
								x.style.display = "block";
							} else {
								x.style.display = "none";
							}
						}
					</script>

				</div>
			</div>
		</div>
	</section>
</div>

<script type="text/javascript">

    $(document).ready(function () {
      $('#dataform').validate({
        rules: {
            nama_brand: {
                required: true,
            },
            logo_brandbaru: {
            	required: true,
            },
            detail_brand: {
            	required: true,
            }
        },
        messages: {
            nama_brand: {
                required: "Nama Brand Tidak Boleh Kosong",
            },
            logo_brandbaru: {
            	required: "Logo Brand Tidak Boleh Kosong",
            },
            detail_brand: {
            	required: "Detail Brand Tidak Boleh Kosong",
            }
        },
        submitHandler: function(form) {
            $('#loader').show(300); 
        	$('#ceksub').prop('disabled', true);
            form.submit();
        },
        errorElement: 'span',
        errorPlacement: function (error, element) {
          error.addClass('invalid-feedback');
          element.closest('.form-group').append(error);
        },
        highlight: function (element, errorClass, validClass) {
          $(element).addClass('is-invalid');
        },
        unhighlight: function (element, errorClass, validClass) {
          $(element).removeClass('is-invalid');
        },
      });
    });

</script>