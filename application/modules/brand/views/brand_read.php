
<div class="row">
	<section class="col-lg connectedSortable ui-sortable">
		<div class="card card-info">
			<div class="card-header">
				<div class="card-title">
					<?= $title_card; ?>
				</div>
			</div>
			<div class="card-body">
				<div class="tab-content p-0">

					<div class="row">
						<div class="col-md-8">
							<table class="table table-bordered table-striped table-condensed" style="margin-bottom: 20px">
								<tr>
									<td class="font-weight-bold">Nama Brand</td><td><?php echo $nama_brand; ?></td>
								</tr>
								<tr>
									<td class="font-weight-bold">Detail Brand</td><td><?php echo $detail_brand; ?></td>
								</tr>
							</table>
						</div>
						<div class="col-md-4">
							<div class="row justify-content-center">
								<label>Logo Brand</label>
							</div>
							<div class="row justify-content-center">
								<a href="<?php echo base_url(); ?>brand/get_image?img=brand/<?php echo basename($logo_brand); ?>" data-toggle="lightbox" data-max-width="800" data-title="Logo Brand">
                                <img src="<?php echo base_url(); ?>brand/get_image?img=brand/<?php echo basename($logo_brand); ?>" class="img-fluid" style="width: 250px;" alt="Logo Brand">
                                </a>
							</div>
						</div>
					</div>


					<div>
						<a href="<?php echo site_url('brand') ?>" class="btn btn-info" style="width: 100px;">Batal</a>
					</div>

				</div>
			</div>
		</div>
	</section>
</div>

<!-- Ekko Lightbox -->
<script src="<?= base_url('assets/plugins/ekko-lightbox/ekko-lightbox.min.js') ?>"></script>
<script>
  $(function () {
    $(document).on('click', '[data-toggle="lightbox"]', function(event) {
      event.preventDefault();
      $(this).ekkoLightbox({
        alwaysShowClose: true
      });
    });

    $('.filter-container').filterizr({gutterPixels: 3});
    $('.btn[data-filter]').on('click', function() {
    $('.btn[data-filter]').removeClass('active');
    $(this).addClass('active');
    });
  })
</script>