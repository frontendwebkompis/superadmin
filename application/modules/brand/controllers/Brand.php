<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Brand extends CI_Controller
{
    function __construct()
    {

        parent::__construct();
        if (cek_token()) {
            $this->load->model('Brand_model');
            $this->load->library('form_validation');
            $this->load->library('upload');
        } else {
            logout();
        }
    }

    public function index()
    {
        $q = urldecode($this->input->get('q', TRUE));
        $start = intval($this->input->get('start'));

        if ($q <> '') {
            $config['base_url'] = base_url() . 'brand?q=' . urlencode($q);
            $config['first_url'] = base_url() . 'brand?q=' . urlencode($q);
        } else {
            $config['base_url'] = base_url() . 'brand';
            $config['first_url'] = base_url() . 'brand';
        }

        $config['per_page'] = 10;
        $config['page_query_string'] = TRUE;
        $config['total_rows'] = $this->Brand_model->total_rows($q);
        $brand = $this->Brand_model->get_limit_data($config['per_page'], $start, $q);

        $this->load->library('pagination');
        $this->pagination->initialize($config);

        $data = array(
            'tittle'        => 'Brand',
            'judul'         => 'Brand',
            'title_card'    => 'List Brand',
            'brand_data'    => $brand,
            'q'             => $q,
            'pagination'    => $this->pagination->create_links(),
            'total_rows'    => $config['total_rows'],
            'start'         => $start,
            'brand_aktif'   => '1',
            'menu_aktif'    => 'master_brand',
        );
        $res['datakonten'] = $this->load->view('brand/brand_list', $data, true);
        $this->load->view('layouts/main_view', $res);
    }

    public function read($id)
    {
        $row = $this->Brand_model->get_by_id($id);
        if ($row) {
            $data = array(
                'tittle'        => 'Brand',
                'judul'         => 'Brand',
                'title_card'    => 'Detail Brand',
                'id_brand'      => $row->id_brand,
                'nama_brand'    => $row->nama_brand,
                'logo_brand'    => $row->logo_brand,
                'detail_brand'  => $row->detail_brand,
                'brand_aktif'   => '1',
                'menu_aktif'    => 'master_brand',
            );
            $res['datakonten'] = $this->load->view('brand/brand_read', $data, true);
            $this->load->view('layouts/main_view', $res);
        } else {
            $this->session->set_flashdata('gagal', 'Data Tidak Ditemukan');
            redirect(site_url('brand'));
        }
    }

    public function create()
    {
        $data = array(
            'tittle'        => 'Brand',
            'judul'         => 'Tambah Brand',
            'title_card'    => 'Tambah Brand',
            'button'        => 'Create',
            'action'        => site_url('brand/create_action'),
            'id_brand'      => set_value('id_brand'),
            'nama_brand'    => set_value('nama_brand'),
            'logo_brand'    => set_value('logo_brand'),
            'detail_brand'  => set_value('detail_brand'),
            'brand_aktif'   => '1',
            'menu_aktif'    => 'master_brand',
        );
        $res['datakonten'] = $this->load->view('brand/brand_form', $data, true);
        $this->load->view('layouts/main_view', $res);
    }

    public function create_action()
    {
        $prefix = "BRAND";
        // echo $gabung;
        $row = $this->Brand_model->get_by_prefix($prefix);
        $NoUrut = (int) substr($row->max, 5, 4);
        $NoUrut = $NoUrut + 1; //nomor urut +1
        $NoUrut = sprintf('%04d', $NoUrut);
        $fix = $prefix . $NoUrut;
        $this->_rules();


        if ($this->form_validation->run() == FALSE) {
            $this->create();
        } else {

            if (!file_exists('./assets/gambar/brand')) {
                mkdir('./assets/gambar/brand', 0777, true);
            }

            $config['upload_path'] = './assets/gambar/brand/';
            $config['allowed_types'] = 'gif|jpg|png|jpeg|bmp'; 
            $config['encrypt_name'] = TRUE; 

            $this->upload->initialize($config);
            if (!empty($_FILES['logo_brandbaru']['name'])) {

                if ($this->upload->do_upload('logo_brandbaru', TRUE)) {
                    $gbr = $this->upload->data();
                    //Compress Image
                    $config['image_library']    = 'gd2';
                    $config['source_image']     = './assets/gambar/brand/' . $gbr['file_name'];
                    $config['create_thumb']     = FALSE;
                    $config['maintain_ratio']   = FALSE;
                    $config['quality']          = '50%';
                    $config['width']            = 600;
                    $config['height']           = 400;
                    $config['new_image']        = './assets/images/brand/' . $gbr['file_name'];
                    $this->load->library('image_lib', $config);
                    $this->image_lib->resize();
                    $upload = s3_upload('brand/'.$gbr['file_name'], $gbr['full_path']);
                    @unlink('assets/gambar/brand/' . $gbr['file_name']);
                    $gambar=$upload['ObjectURL'];
                }
            } else {
                echo "Image yang diupload kosong";
            }

            $data = array(
                'id_brand' => $fix,
                'nama_brand' => $this->input->post('nama_brand', TRUE),
                'logo_brand' => $gambar,
                'detail_brand' => $this->input->post('detail_brand', TRUE),
                'id_superadmin' => getID(),
                'status_brand_remove' => 1,
            );
            $this->Brand_model->insert($data);
            $this->session->set_flashdata('berhasil', 'Berhasil Menambahkan Data');
            redirect(site_url('brand'));
        }
    }

    public function update($id)
    {
        $row = $this->Brand_model->get_by_id($id);
        if ($row) {
            $data = array(
                'tittle'        => 'Brand',
                'judul'         => 'Update Brand',
                'title_card'    => 'Update Brand',
                'button'        => 'Update',
                'action'        => site_url('brand/update_action'),
                'id_brand'      => set_value('id_brand', $row->id_brand),
                'nama_brand'    => set_value('nama_brand', $row->nama_brand),
                'logo_brand'    => set_value('logo_brand', $row->logo_brand),
                'detail_brand'  => set_value('detail_brand', $row->detail_brand),
                'brand_aktif'   => '1',
                'menu_aktif'    => 'master_brand',
            );
            $res['datakonten'] = $this->load->view('brand/brand_form', $data, true);
            $this->load->view('layouts/main_view', $res);
        } else {
            $this->session->set_flashdata('gagal', 'Data Tidak Ditemukan');
            redirect(site_url('brand'));
        }
    }

    public function update_action()
    {
        $this->_rules();

        $config['upload_path'] = './assets/gambar/brand/';
        $config['allowed_types'] = 'gif|jpg|png|jpeg|bmp';
        $config['encrypt_name'] = TRUE;

        $this->upload->initialize($config);
        if (!empty($_FILES['logo_brandbaru']['name'])) {

            if ($this->upload->do_upload('logo_brandbaru', TRUE)) {
                $gbr = $this->upload->data();
                //Compress Image
                $config['image_library'] = 'gd2';
                $config['source_image'] = './assets/gambar/brand/' . $gbr['file_name'];
                $config['create_thumb'] = FALSE;
                $config['maintain_ratio'] = FALSE;
                $config['quality'] = '50%';
                $config['width'] = 600;
                $config['height'] = 400;
                $config['new_image'] = './assets/images/brand/' . $gbr['file_name'];
                $this->load->library('image_lib', $config);
                $this->image_lib->resize();
                @unlink('assets/gambar/brand/' . $this->input->post('logo_brand'));
                del('brand/'.basename($this->input->post('logo_brand')));
                $upload = s3_upload('brand/'.$gbr['file_name'], $gbr['full_path']);
                $gambar=$upload['ObjectURL'];
                @unlink('assets/gambar/brand/' . $gbr['file_name']);
            }
        } else {
            $gambar = $this->input->post('logo_brand');
        }

        if ($this->form_validation->run() == FALSE) {
            $this->update($this->input->post('id_brand', TRUE));
        } else {
            $data = array(
                'nama_brand' => $this->input->post('nama_brand', TRUE),
                'logo_brand' => $gambar,
                'detail_brand' => $this->input->post('detail_brand', TRUE),
                'id_superadmin' => getID(),
            );

            $this->Brand_model->update($this->input->post('id_brand', TRUE), $data);
            $this->session->set_flashdata('berhasil', 'Berhasil Merubah Data');
            redirect(site_url('brand'));
        }
    }

    public function del_sem($id)
    {
        $row = $this->Brand_model->get_by_id($id);

        if ($row) {
            $data = array(
                'status_brand_remove' => 0,
                'id_superadmin' => getID(),
            );

            $this->Brand_model->update($id, $data);
              del('brand/'.basename($row->logo_brand));
            @unlink('assets/gambar/brand/' . $row->logo_brand);
            $this->session->set_flashdata('berhasil', 'Berhasil Menghapus Data');
            redirect(site_url('brand'));
        } else {
            $this->session->set_flashdata('gagal', 'Data Tidak Ditemukan');
            redirect(site_url('brand'));
        }
    }
      function get_image(){

        $img=$this->input->get('img');
        // echo getkonten($img);
        $cek='https://kompis-bank-sampah.s3.ap-southeast-1.amazonaws.com/'.$img;
         $row = $this->Brand_model->get_by_poto($cek);
          if($row){
          echo getkonten($img);  
        }else{
              $this->session->set_flashdata('cek', 'Gambar Tidak Ditemukan');
                    redirect('profile');
        }
        }

    public function _rules()
    {
        $this->form_validation->set_rules('nama_brand', 'nama brand', 'trim|required');
        $this->form_validation->set_rules('detail_brand', 'detail brand', 'trim|required');

        $this->form_validation->set_rules('id_brand', 'id_brand', 'trim');
        $this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

    public function excel()
    {
        $this->load->helper('exportexcel');
        $namaFile = "brand.xls";
        $judul = "brand";
        $tablehead = 0;
        $tablebody = 1;
        $nourut = 1;
        //penulisan header
        header("Pragma: public");
        header("Expires: 0");
        header("Cache-Control: must-revalidate, post-check=0,pre-check=0");
        header("Content-Type: application/force-download");
        header("Content-Type: application/octet-stream");
        header("Content-Type: application/download");
        header("Content-Disposition: attachment;filename=" . $namaFile . "");
        header("Content-Transfer-Encoding: binary ");

        xlsBOF();

        $kolomhead = 0;
        xlsWriteLabel($tablehead, $kolomhead++, "No");
        xlsWriteLabel($tablehead, $kolomhead++, "Nama Brand");
        xlsWriteLabel($tablehead, $kolomhead++, "Logo Brand");
        xlsWriteLabel($tablehead, $kolomhead++, "Detail Brand");

        foreach ($this->Brand_model->get_all() as $data) {
            $kolombody = 0;

            //ubah xlsWriteLabel menjadi xlsWriteNumber untuk kolom numeric
            xlsWriteNumber($tablebody, $kolombody++, $nourut);
            xlsWriteLabel($tablebody, $kolombody++, $data->nama_brand);
            xlsWriteLabel($tablebody, $kolombody++, $data->logo_brand);
            xlsWriteLabel($tablebody, $kolombody++, $data->detail_brand);

            $tablebody++;
            $nourut++;
        }
        xlsEOF();
        exit();
    }
}

/* End of file Brand.php */
/* Location: ./application/controllers/Brand.php */
