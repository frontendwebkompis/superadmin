<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Laporan_pembelian_biller_model extends CI_Model
{

    public $vtable_peng = 'v_biller_pengepul';
    public $vtable_nasabah = 'v_biller_nasabah';
    public $vtable_bank = 'v_biller_banksampah';
    public $table_peng = 'biller_pengepul';
    public $table_nas = 'biller_nasabah';
    public $table_bs = 'biller_banksampah';
   
    public $order = 'DESC';

    function __construct()
    {
        parent::__construct();
    }

    // get all
    function get_all()
    {
        $this->db->order_by($this->id, $this->order);
        return $this->db->get($this->table)->result();
    }
public function get_by_prefix($prefix = '')
    {
        $query = $this->db->query("SELECT max(".$this->id.") AS max FROM ".$this->table." WHERE ".$this->id." LIKE '$prefix%'");
        return $query->row();
    }
    // get data by id
    function get_by_id($id)
    {
        $this->db->where($this->id, $id);
        return $this->db->get($this->table)->row();
    }
    
    // get total rows
    function total_rows_bank_sampah($q = NULL,$awal,$akhir) {
         $this->db->where("(UNIX_TIMESTAMP(". $this->vtable_bank .".datetime) BETWEEN UNIX_TIMESTAMP('" . $awal . "') AND UNIX_TIMESTAMP('" . $akhir  . " 23:59') ) "); 
           $this->db->group_start();
        $this->db->or_like('nama', $q);
        $this->db->group_end();
        $this->db->from($this->vtable_bank);
        return $this->db->count_all_results();
    }

    // get data with limit and search
    function get_limit_data_bank_sampah($limit, $start = 0, $q = NULL,$awal,$akhir) {
         $this->db->where("(UNIX_TIMESTAMP(". $this->vtable_bank .".datetime) BETWEEN UNIX_TIMESTAMP('" . $awal . "') AND UNIX_TIMESTAMP('" . $akhir  . " 23:59') ) "); 
        $this->db->order_by('datetime', $this->order);
        $this->db->group_start();
        $this->db->or_like('nama', $q);
        $this->db->group_end();
        $this->db->limit($limit, $start);
        return $this->db->get($this->vtable_bank)->result();
    }
        // get total rows
    function total_rows_nasabah($q = NULL,$awal,$akhir) {
         $this->db->where("(UNIX_TIMESTAMP(". $this->vtable_nasabah .".datetime) BETWEEN UNIX_TIMESTAMP('" . $awal . "') AND UNIX_TIMESTAMP('" . $akhir  . " 23:59') ) "); 
           $this->db->group_start();
        $this->db->or_like('nama', $q);
        $this->db->group_end();
        $this->db->from($this->vtable_nasabah);
        return $this->db->count_all_results();
    }

    // get data with limit and search
    function get_limit_data_nasabah($limit, $start = 0, $q = NULL,$awal,$akhir) {
         $this->db->where("(UNIX_TIMESTAMP(". $this->vtable_nasabah .".datetime) BETWEEN UNIX_TIMESTAMP('" . $awal . "') AND UNIX_TIMESTAMP('" . $akhir  . " 23:59') ) "); 
        $this->db->order_by('datetime', $this->order);
        $this->db->group_start();
        $this->db->or_like('nama', $q);
        $this->db->group_end();
        $this->db->limit($limit, $start);
        return $this->db->get($this->vtable_nasabah)->result();
    } 
     // get total rows
    function total_rows($q = NULL,$awal,$akhir) {
         $this->db->where("(UNIX_TIMESTAMP(". $this->vtable_peng .".datetime) BETWEEN UNIX_TIMESTAMP('" . $awal . "') AND UNIX_TIMESTAMP('" . $akhir  . " 23:59') ) "); 
           $this->db->group_start();
        $this->db->or_like('nama', $q);
        $this->db->group_end();
        $this->db->from($this->vtable_peng);
        return $this->db->count_all_results();
    }

    // get data with limit and search
    function get_limit_data($limit, $start = 0, $q = NULL,$awal,$akhir) {
        $this->db->order_by('datetime', $this->order);
         $this->db->where("(UNIX_TIMESTAMP(". $this->vtable_peng .".datetime) BETWEEN UNIX_TIMESTAMP('" . $awal . "') AND UNIX_TIMESTAMP('" . $akhir  . " 23:59') ) "); 
            $this->db->group_start();
        $this->db->or_like('nama', $q);
        $this->db->group_end();
        $this->db->limit($limit, $start);
        return $this->db->get($this->vtable_peng)->result();
    }

    // insert data
    function insert($data)
    {
        $this->db->insert($this->table, $data);
    }

    // update data
    function update($id, $data)
    {
        $this->db->where($this->id, $id);
        $this->db->update($this->table, $data);
    }

    // delete data
    function delete($id)
    {
        $this->db->where($this->id, $id);
        $this->db->delete($this->table);
    }

}

/* End of file Kategori_produk_model.php */
/* Location: ./application/models/Kategori_produk_model.php */
/* Please DO NOT modify this information : */
/* Generated by Harviacode Codeigniter CRUD Generator 2020-07-15 09:43:37 */
/* http://harviacode.com */