<div class="card card-<?= bgCard() ?>">
    <div class="card-header">
        <div class="card-title">
            <?= $titleCard ?>
        </div>
    </div>
    <div class="card-body">
        <div class="row mb-2">
            <div class="col-md-12">
                <?php $this->load->view('laporan_pembelian_biller/tombol_atas'); ?>
            </div>
        </div>
        <div class="row">
            <div class="col-md-8 mb-2">
                <?php
                    if($tombol_aktif=='bank_sampah'){
                        echo filterDate($this->input->get('awal'), $this->input->get('akhir'), base_url('laporan_pembelian_biller/bank_sampah'));
                    } else if($tombol_aktif=='nasabah'){
                        echo filterDate($this->input->get('awal'), $this->input->get('akhir'), base_url('laporan_pembelian_biller/nasabah'));
                    } else {
                        echo filterDate($this->input->get('awal'), $this->input->get('akhir'), base_url('laporan_pembelian_biller'));
                    }
                ?>
            </div>
                <div class="col-md-3 offset-md-1 mb-2">
                        <?php
                            if($tombol_aktif=='bank_sampah'){
                                echo search(site_url('laporan_pembelian_biller/bank_sampah'), site_url('laporan_pembelian_biller/bank_sampah'), $q);
                            } else if($tombol_aktif=='nasabah'){
                                echo search(site_url('laporan_pembelian_biller/nasabah'), site_url('laporan_pembelian_biller/nasabah'), $q);
                            } else {
                                echo search(site_url('laporan_pembelian_biller/index'), site_url('laporan_pembelian_biller'), $q);
                            }
                        ?>
                    </div>
                </div>
           
            <div class="tab-content p-0" style="overflow:auto">
                <table class="table table-bordered table-striped table-condensed table-hover table-sm" style="margin-bottom: 10px">
                    <tr>
                        <th class='text-center' style="width: 60px;">No</th>
                        <th class='text-center'>Tanggal Layanan </th>
                        <th class='text-center'>Nama </th>
                        <th class='text-center'>Nama Layanan </th>
                        <th class='text-center'>Nomer Pelayanan </th>
                        <th class='text-center'>Amount</th>
                        <th class='text-center' style="width: 150px;">Status</th>
                    </tr><?php
                    if ($total_rows == 0) {
                        echo '<tr><td colspan="11" bgcolor="grey" style="color: white; font-weight: bold; text-align: center;">Data Tidak Ditemukan</td></tr>';
                    } else {
                        foreach ($laporan_pembelian_biller_data as $laporan_pembelian_biller)
                        {
                            ?>
                    <tr>
                        <td class='text-center'><?php echo ++$start ?></td>
                        <td class='text-center'><?php echo fulldate($laporan_pembelian_biller->datetime) ?></td>
                        <td><?php echo $laporan_pembelian_biller->nama ?></td>
                        <td><?php echo $laporan_pembelian_biller->nama_layanan_tarik_saldo_online ?></td>
                        <td><?php echo $laporan_pembelian_biller->nomor_pelayanan ?></td>
                        <td class='text-center'><?php echo rupiah($laporan_pembelian_biller->amount) ?></td>
                       
                        <td class='text-center'>
                        
                          <?php if($laporan_pembelian_biller->status==1){
                            echo "<span class='badge badge-success'>Sukses</span>";
                          }else{
                            echo "<span class='badge badge-danger'>Pending</span>";
                       
                          } ?>
                        </td>
                    </tr>
                            <?php
                        }
                    }
                        ?>
                </table>
            </div>
            <?= footer($total_rows, $pagination, '') ?>
        </div>
    </div>
</div>

<div id="modalCreatelaporan_pembelian_biller" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Form Tambah laporan_pembelian_biller</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form action="<?= $action ?>" method="post" id="formTambahlaporan_pembelian_biller" role="form" enctype="multipart/form-data">
                <div class="row ml-3 mr-3">
                    <div class="col-md-12">
                       
                        <div class="form-group">
                            <label for="varchar">Nama Jenis Kendaraan</label>
                            <input type="text" class="form-control" name="nama_laporan_pembelian_biller" id="nama_laporan_pembelian_biller" placeholder="Nama Jenis Kendaraan" value="" />
                        </div>
                        
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <div class="col-md-12 text-center">
                    <button type="submit" class="btn btn-<?= bgCard() ?>" style="width: 100px;">Simpan</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal" style="width: 100px;">Batal</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="modalEditlaporan_pembelian_biller" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Form Edit laporan_pembelian_biller</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form action="<?= $action ?>" method="post" id="formEditlaporan_pembelian_biller" role="form" enctype="multipart/form-data">
                <div class="row ml-3 mr-3">
                    <div class="col-md-12">
                           <div class="form-group">
                            <label for="varchar">laporan_pembelian_biller</label>
                            <input type="text" class="form-control" name="nama_laporan_pembelian_biller" id="nama_laporan_pembelian_biller" placeholder="laporan_pembelian_biller" value="" />
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <div class="col-md-12 text-center">
                    <button type="submit" class="btn btn-<?= bgCard() ?>" style="width: 100px;">Simpan</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal" style="width: 100px;">Batal</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    $(function(){
        setDateFilter('akhir', 'awal');
  
        $('#modalCreatelaporan_pembelian_biller #formTambahlaporan_pembelian_biller').validate({
            rules: {
                nama_laporan_pembelian_biller : {
                    required : true
                }
            },
            messages: {
                nama_laporan_pembelian_biller : {
                    required : "Nama laporan_pembelian_biller Tidak Boleh Kosong"
                }
            },
            errorElement: 'span',
            errorPlacement: function(error, element) {
                error.addClass('invalid-feedback');
                element.closest('.form-group').append(error);
            },
            highlight: function(element, errorClass, validClass) {
                $(element).addClass('is-invalid');
            },
            unhighlight: function(element, errorClass, validClass) {
                $(element).removeClass('is-invalid');
            }
        })
        $('#modalEditlaporan_pembelian_biller #formEditlaporan_pembelian_biller').validate({
            rules: {
                nama_laporan_pembelian_biller : {
                    required : true
                },
            },
            messages: {
                nama_laporan_pembelian_biller : {
                    required : "Nama laporan_pembelian_biller Tidak Boleh Kosong"
                },
            },
            errorElement: 'span',
            errorPlacement: function(error, element) {
                error.addClass('invalid-feedback');
                element.closest('.form-group').append(error);
            },
            highlight: function(element, errorClass, validClass) {
                $(element).addClass('is-invalid');
            },
            unhighlight: function(element, errorClass, validClass) {
                $(element).removeClass('is-invalid');
            }
        })

        $('#modalCreatelaporan_pembelian_biller #keterangan_laporan_pembelian_biller').select2({
            theme: 'bootstrap'
        })

        $('#modalEditlaporan_pembelian_biller #edit_keterangan_laporan_pembelian_biller').select2({
            theme: 'bootstrap'
        })
    })

    $('#modalCreatelaporan_pembelian_biller').on('hidden.bs.modal', function(){
        $('#modalCreatelaporan_pembelian_biller #status_aplikasi').val('')
        $('#modalCreatelaporan_pembelian_biller #status_aplikasi').removeClass('is-invalid')
        $('#modalCreatelaporan_pembelian_biller #keterangan_laporan_pembelian_biller').val('').trigger('change')
        $('#modalCreatelaporan_pembelian_biller #keterangan_laporan_pembelian_biller').removeClass('is-invalid')
        $('#modalCreatelaporan_pembelian_biller #nama_laporan_pembelian_biller').val('')
        $('#modalCreatelaporan_pembelian_biller #nama_laporan_pembelian_biller').removeClass('is-invalid')
    })

    $('#modalEditlaporan_pembelian_biller').on('hidden.bs.modal', function(){
        $('#modalEditlaporan_pembelian_biller #status_aplikasi').val('')
        $('#modalEditlaporan_pembelian_biller #status_aplikasi').removeClass('is-invalid')
        $('#modalEditlaporan_pembelian_biller #edit_keterangan_laporan_pembelian_biller').val('').trigger('change')
        $('#modalEditlaporan_pembelian_biller #edit_keterangan_laporan_pembelian_biller').removeClass('is-invalid')
        $('#modalEditlaporan_pembelian_biller #laporan_pembelian_biller').val('')
        $('#modalEditlaporan_pembelian_biller #laporan_pembelian_biller').removeClass('is-invalid')
    })

    function cekEditlaporan_pembelian_biller(id){
        startloading('#modalEditlaporan_pembelian_biller .modal-content')
        $('#modalEditlaporan_pembelian_biller #formEditlaporan_pembelian_biller').attr('action', '<?= base_url('laporan_pembelian_biller/update_action/') ?>'+id)
        $.ajax({
            url: '<?php echo base_url() ?>laporan_pembelian_biller/ambilDatalaporan_pembelian_biller/' + id,
            dataType: "json",
            cache: false,
            success: function(res) {
                $('#modalEditlaporan_pembelian_biller #nama_laporan_pembelian_biller').val(res.nama_laporan_pembelian_biller)
               
                stoploading('#modalEditlaporan_pembelian_biller .modal-content')
            }
        });
    }

  
    function cekDeletelaporan_pembelian_biller(id){
        swal({
                title: 'Yakin menghapus data?',
                text: "Data yang sudah terhapus tidak dapat dikembalikan!",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#CD5C5C',
                cancelButtonColor: '#d33',
                cancelButtonText: 'Batal',
                confirmButtonText: 'Hapus',
                closeOnConfirm: false,
            },
            function(isConfirm) {
                if (isConfirm) {
                    window.location.href = "<?php echo site_url('laporan_pembelian_biller/delete/'); ?>" + id;
                } else {

                }
            });
    }
</script>