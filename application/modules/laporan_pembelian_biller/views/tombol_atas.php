<div class="tab-content p-0" style="overflow:auto">
    <div class="btn-group">
        <?php 
            if($tombol_aktif == 'bank_sampah'){
                echo anchor(site_url('laporan_pembelian_biller/bank_sampah'),'Bank Sampah', 'class="btn btn-info btn-sm" data-toggle="tooltip" data-placement="top" title="List Biller Bank Sampah" style="width: 120px;"'); 
            } else {
                echo anchor(site_url('laporan_pembelian_biller/bank_sampah'),'Bank Sampah', 'class="btn btn-default btn-sm" data-toggle="tooltip" data-placement="top" title="List Biller Bank Sampah" style="width: 120px;"'); 
            } 
        ?> 
        <?php 
            if($tombol_aktif == 'pengepul'){
                echo anchor(site_url('laporan_pembelian_biller'),'Pengepul', 'class="btn btn-info btn-sm" data-toggle="tooltip" data-placement="top" title="List Pengepul" style="width: 120px;"'); 
            } else {
                echo anchor(site_url('laporan_pembelian_biller'),'Pengepul', 'class="btn btn-default btn-sm" data-toggle="tooltip" data-placement="top" title="List Pengepul" style="width: 120px;"'); 
            } 
        ?> 
        <?php 
            if($tombol_aktif == 'nasabah'){
                echo anchor(site_url('laporan_pembelian_biller/nasabah'),'Nasabah Online', 'class="btn btn-info btn-sm" data-toggle="tooltip" data-placement="top" title="List Biller Nasabah Online" style="width: 120px;"'); 
            } else {
                echo anchor(site_url('laporan_pembelian_biller/nasabah'),'Nasabah Online', 'class="btn btn-default btn-sm" data-toggle="tooltip" data-placement="top" title="List Biller Nasabah Online" style="width: 120px;"'); 
            } 
        ?>
         
    </div>
</div>