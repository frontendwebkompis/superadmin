<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Reward extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        if (cek_token()) {
            $this->load->model('Reward_model');
            $this->load->library('form_validation');
            $this->load->library('upload');
        } else {
            logout();
        }
    }

    public function index()
    {
        $q = urldecode($this->input->get('q', TRUE));
        $start = intval($this->input->get('start'));

        if ($q <> '') {
            $config['base_url'] = base_url() . 'reward?q=' . urlencode($q);
            $config['first_url'] = base_url() . 'reward?q=' . urlencode($q);
        } else {
            $config['base_url'] = base_url() . 'reward';
            $config['first_url'] = base_url() . 'reward';
        }

        $config['per_page'] = 10;
        $config['page_query_string'] = TRUE;
        $config['total_rows'] = $this->Reward_model->total_rows($q);
        $reward = $this->Reward_model->get_limit_data($config['per_page'], $start, $q);

        $this->load->library('pagination');
        $this->pagination->initialize($config);

        $data = array(
            'tittle'        => 'Reward',
            'judul'         => 'Reward',
            'title_card'    => 'List Reward',
            'reward_data'   => $reward,
            'q'             => $q,
            'pagination'    => $this->pagination->create_links(),
            'total_rows'    => $config['total_rows'],
            'start'         => $start,
            'reward_aktif'  => '1',
            'menu_aktif'    => 'master_reward',
        );
        $res['datakonten'] = $this->load->view('reward/reward_list', $data, true);
        $this->load->view('layouts/main_view', $res);
    }

    public function read($id)
    {
        $row = $this->Reward_model->get_by_id($id);
        if ($row) {
            $data = array(
                'tittle'        => 'Reward',
                'judul'         => 'Reward',
                'title_card'    => 'Detail Reward',
                'id_reward'     => $row->id_reward,
                'nama_brand'    => $row->nama_brand,
                'nama_reward'   => $row->nama_reward,
                'detail_reward' => $row->detail_reward,
                'poin_reward'   => $row->poin_reward,
                'status_user'   => $row->status_user,
                'gambar_reward' => $row->gambar_reward,
                'reward_aktif'  => '1',
                'menu_aktif'    => 'master_reward',
            );
            $res['datakonten'] = $this->load->view('reward/reward_read', $data, true);
            $this->load->view('layouts/main_view', $res);
        } else {
            $this->session->set_flashdata(
                'gagal','Data Tidak Ditemukan'
            );
            redirect(site_url('reward'));
        }
    }

    public function create()
    {
        $data = array(
            'tittle'        => 'Reward',
            'judul'         => 'Reward',
            'title_card'    => 'Tambah Reward',
            'button'        => 'Create',
            'action'        => site_url('reward/create_action'),
            'id_reward'     => set_value('id_reward'),
            'id_brand'      => set_value('id_brand'),
            'nama_reward'   => set_value('nama_reward'),
            'detail_reward' => set_value('detail_reward'),
            'poin_reward'   => set_value('poin_reward'),
            'gambar_reward' => set_value('gambar_reward'),
            'reward_aktif'  => '1',
            'menu_aktif'    => 'master_reward',
        );
        $data['brand'] = $this->Reward_model->get_brand_all();
    
        $res['datakonten'] = $this->load->view('reward/reward_form', $data, true);
        $this->load->view('layouts/main_view', $res);
    }

    public function create_action()
    {
        $this->_rules();
        $prefix = "RWD";
        // echo $gabung;
        $row = $this->Reward_model->get_by_prefix($prefix);
        $NoUrut = (int) substr($row->max, 3, 5);
        $NoUrut = $NoUrut + 1; //nomor urut +1
        $NoUrut = sprintf('%05d', $NoUrut);
        $fix = $prefix . $NoUrut;

        if ($this->form_validation->run() == FALSE) {
            $this->create();
        } else {

            if (!file_exists('./assets/gambar/reward')) {
                mkdir('./assets/gambar/reward', 0777, true);
            }

            $config['upload_path']     = './assets/gambar/reward/';
            $config['allowed_types']   = 'gif|jpg|png|jpeg|bmp';
            $config['encrypt_name']    = TRUE;
            $this->upload->initialize($config);
            if (!empty($_FILES['gambar_rewardbaru']['name'])) {
                if ($this->upload->do_upload('gambar_rewardbaru', TRUE)) {
                    $gbr = $this->upload->data();

                    $config['image_library']   = 'gd2';
                    $config['source_image']    = './assets/gambar/reward/' . $gbr['file_name'];
                    $config['create_thumb']    = FALSE;
                    $config['maintain_ratio']  = FALSE;
                    $config['quality']         = '50%';
                    $config['width']           = 600;
                    $config['height']          = 400;
                    $config['new_image']       = './assets/gambar/' . $gbr['file_name'];
                    $this->load->library('image_lib', $config);
                    $this->image_lib->resize();
                    $upload = s3_upload('reward/'.$gbr['file_name'], $gbr['full_path']);
                    @unlink('assets/gambar/reward/'. $gbr['file_name']);
                    $gambar=$upload['ObjectURL'];
                }
            } else {
                $gambar = 'kosong';
            }

            $data = array(
                'id_reward'             => $fix,
                'id_superadmin'         => getID(),
                'nama_reward'           => $this->input->post('nama_reward', TRUE),
                'id_brand'              => $this->input->post('id_brand', TRUE),
                'detail_reward'         => $this->input->post('detail_reward', TRUE),
                'poin_reward'           => $this->input->post('poin_reward', TRUE),
                'status_user'           => $this->input->post('status_user', TRUE),
                'gambar_reward'         => $gambar,
                'status_reward_remove'  => 1,
            );

            $this->Reward_model->insert($data);
            $this->session->set_flashdata('berhasil', 'Berhasil Menambahkan Data');
            redirect(site_url('reward'));
        }
    }

    public function update($id)
    {
        $row = $this->Reward_model->get_by_id($id);

        if ($row) {
            $data = array(
                'tittle'        => 'Reward',
                'judul'         => 'Reward',
                'title_card'    => 'Update Reward',
                'button'        => 'Update',
                'action'        => site_url('reward/update_action'),
                'id_reward'     => set_value('id_reward', $row->id_reward),
                'id_brand'      => set_value('id_brand', $row->id_brand),
                'nama_reward'   => set_value('nama_reward', $row->nama_reward),
                'detail_reward' => set_value('detail_reward', $row->detail_reward),
                'poin_reward'   => set_value('poin_reward', $row->poin_reward),
                'status_user'   => set_value('status_user', $row->status_user),
                'gambar_reward' => set_value('gambar_reward', $row->gambar_reward),
                'reward_aktif'  => '1',
                'menu_aktif'    => 'master_reward',
            );
            $data['brand'] = $this->Reward_model->get_brand_all();
    
            $res['datakonten'] = $this->load->view('reward/reward_form', $data, true);
            $this->load->view('layouts/main_view', $res);
        } else {
            $this->session->set_flashdata('gagal', 'Data Tidak Ditemukan');
            redirect(site_url('reward'));
        }
    }

    public function update_action()
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->update($this->input->post('id_reward', TRUE));
        } else {

            if (!file_exists('./assets/gambar/reward')) {
                mkdir('./assets/gambar/reward', 0777, true);
            }

            $config['upload_path']     = './assets/gambar/reward/';
            $config['allowed_types']   = 'gif|jpg|png|jpeg|bmp';
            $config['encrypt_name']    = TRUE;
            $this->upload->initialize($config);
            if (!empty($_FILES['gambar_rewardbaru']['name'])) {
                if ($this->upload->do_upload('gambar_rewardbaru', TRUE)) {
                    $gbr = $this->upload->data();
                    //Compress Image
                    $config['image_library']   = 'gd2';
                    $config['source_image']    = './assets/gambar/reward/' . $gbr['file_name'];
                    $config['create_thumb']    = FALSE;
                    $config['maintain_ratio']  = FALSE;
                    $config['quality']         = '50%';
                    $config['width']           = 600;
                    $config['height']          = 400;
                    $config['new_image']       = './assets/gambar/reward/' . $gbr['file_name'];
                    $this->load->library('image_lib', $config);
                    $this->image_lib->resize();
                    $upload = s3_upload('reward/'.$gbr['file_name'], $gbr['full_path']);
                    del('reward/'.basename($this->input->post('gambar_reward')));
                    @unlink('assets/gambar/reward/'. $gbr['file_name']);
                    $gambar1=$upload['ObjectURL'];
                }
            } else {
                $gambar1 = $this->input->post('gambar_reward', TRUE);
            }

            $data = array(
                'id_brand'      => $this->input->post('id_brand', TRUE),
                'nama_reward'   => $this->input->post('nama_reward', TRUE),
                'detail_reward' => $this->input->post('detail_reward', TRUE),
                'poin_reward'   => $this->input->post('poin_reward', TRUE),
                'status_user'   => $this->input->post('status_user', TRUE),
                'id_superadmin' => getID(),
                'gambar_reward' => $gambar1,
            );

            $this->Reward_model->update($this->input->post('id_reward', TRUE), $data);
            $this->session->set_flashdata('berhasil', 'Berhasil Merubah Data');
            redirect(site_url('reward'));
        }
    }

    public function del_sem($id)
    {
        $row = $this->Reward_model->get_by_id($id);

        if ($row) {
            $data = array(
                'status_reward_remove' => 0,
                'id_superadmin' => getID(),
            );

            $this->Reward_model->update($id, $data);

            $this->session->set_flashdata('berhasil', 'Berhasil Menghapus Data');
            redirect(site_url('reward'));
        } else {
            $this->session->set_flashdata('gagal', 'Data Tidak Ditemukan');
            redirect(site_url('reward'));
        }
    }

    function get_image(){
        $img=$this->input->get('img');
        $cek='https://kompis-bank-sampah.s3.ap-southeast-1.amazonaws.com/'.$img;
        $row = $this->Reward_model->get_by_poto($cek);
        echo getkonten($img);  
    }


    public function _rules()
    {
        $this->form_validation->set_rules('nama_reward', 'nama reward', 'trim|required');
        $this->form_validation->set_rules('detail_reward', 'detail reward', 'trim|required');
        $this->form_validation->set_rules('poin_reward', 'poin reward', 'trim|required');
        $this->form_validation->set_rules('status_user', 'status user', 'trim|required');
        $this->form_validation->set_rules('id_reward', 'id_reward', 'trim');
        $this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

    public function excel()
    {
        $this->load->helper('exportexcel');
        $namaFile = "reward.xls";
        $judul = "reward";
        $tablehead = 0;
        $tablebody = 1;
        $nourut = 1;
        //penulisan header
        header("Pragma: public");
        header("Expires: 0");
        header("Cache-Control: must-revalidate, post-check=0,pre-check=0");
        header("Content-Type: application/force-download");
        header("Content-Type: application/octet-stream");
        header("Content-Type: application/download");
        header("Content-Disposition: attachment;filename=" . $namaFile . "");
        header("Content-Transfer-Encoding: binary ");

        xlsBOF();

        $kolomhead = 0;
        xlsWriteLabel($tablehead, $kolomhead++, "No");
        xlsWriteLabel($tablehead, $kolomhead++, "Id Tipe Bank Sampah");
        xlsWriteLabel($tablehead, $kolomhead++, "Nama Reward");
        xlsWriteLabel($tablehead, $kolomhead++, "Detail Reward");
        xlsWriteLabel($tablehead, $kolomhead++, "Poin Reward");
        xlsWriteLabel($tablehead, $kolomhead++, "Status Reward");

        foreach ($this->Reward_model->get_all() as $data) {
            $kolombody = 0;

            //ubah xlsWriteLabel menjadi xlsWriteNumber untuk kolom numeric
            xlsWriteNumber($tablebody, $kolombody++, $nourut);
            xlsWriteNumber($tablebody, $kolombody++, $data->id_tipe_bank_sampah);
            xlsWriteLabel($tablebody, $kolombody++, $data->nama_reward);
            xlsWriteLabel($tablebody, $kolombody++, $data->detail_reward);
            xlsWriteNumber($tablebody, $kolombody++, $data->poin_reward);
            xlsWriteLabel($tablebody, $kolombody++, $data->status_reward);

            $tablebody++;
            $nourut++;
        }

        xlsEOF();
        exit();
    }
}

/* End of file Reward.php */
/* Location: ./application/controllers/Reward.php */
