<script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>

<div class="row">
	<section class="col-lg connectedSortable ui-sortable">
		<div class="card card-info">
            <div class="card-header">
                <div class="card-title">
                  <?php echo $title_card?>
                </div>
              </div>
			<div class="card-body">
				<div class="tab-content p-0">

        <div class="row">
            <div class="col-md-4 mb-2">
                <?php echo anchor(site_url('reward/create'),'Tambah Data', 'class="btn btn-info btn-sm"'); ?>
            </div>
            <div class="col-md-3 offset-md-5 mb-2">
                <?= search(site_url('reward/index'), site_url('reward'), $q) ?>
            </div>
        </div>
        <div class="tab-content p-0" style="overflow:auto">
        <table class="table table-bordered table-striped table-condensed table-hover table-sm" style="margin-bottom: 10px">

            <tr>
                <th class="text-center" width="50px">No</th>
                <th class="text-center">Nama Brand</th>
                <th class="text-center">Nama Reward</th>
                <th class="text-center">Detail Reward</th>
                <th class="text-center">Poin Reward</th>
                <th class="text-center">Status Reward</th>
                <th class="text-center">Aksi</th>
            </tr>
            <?php
                if($total_rows == 0){
                    echo '<tr><td colspan="8" bgcolor="grey" style="color: white; font-weight: bold; text-align: center;">Data Tidak Ditemukan</td></tr>';
                } else {
            ?>
            <?php foreach ($reward_data as $reward){ ?>
            <tr>
                <td class="text-center"><?php echo ++$start ?></td>
                <td><?php echo $reward->nama_brand ?></td>
                <td><?php echo $reward->nama_reward ?></td>
                <td><?php echo $reward->detail_reward ?></td>
                <td class="text-center"><?php echo $reward->poin_reward ?></td>
                <td class="text-center"><?php if($reward->status_user==1) echo "Bank Sampah"; else if($reward->status_user==2) echo "Nasabah";else echo "Pengepul"; ?></td>
                <td class="text-center" width="150px">
                    <div class="btn-group">
                        <a href="<?php echo site_url('reward/read/'.$reward->id_reward); ?>"
                        data-toogle="tooltip" title="Lihat">
                        <button type="button" class="btn btn-info btn-sm"><i class="fas fa-info-circle"></i></button></a>
                        <a href="<?php echo site_url('reward/update/'.$reward->id_reward); ?>"
                        data-toogle="tooltip" title="Update">
                        <button type="button" class="btn btn-success btn-sm"><i class="far fa-edit"></i></button></a>
                        <a href="#" class="btn btn-danger btn-sm" data-toggle="tooltip" data-placement="top" title="Hapus" onclick="confirm('<?= $reward->id_reward ?>')" ><i class="fas fa-trash-alt"></i></a>
                    </div>
		    </tr>
            <?php } 
            } ?>
        </table>
        </div>
        <?= footer($total_rows, $pagination, '') ?>
        </div>
			</div>
		</div>
	</section>
</div>

<script>
function confirm(res) {
    Swal.fire({
    title: 'Anda Yakin Mau Menghapus?',
    text: "Tidak bisa dikembalikan jika sudah dihapus!",
    icon: 'warning',
    showCancelButton: true,
    confirmButtonColor: '#3085d6',
    cancelButtonColor: '#d33',
    confirmButtonText: 'Ya, Hapus!',
    cancelButtonText: 'Batal'
    }).then((result) => {
    if (result.value) {
        Swal.fire(
        'Terhapus!',
        'File sudah terhapus.',
        'success'
        )
        window.location='<?php echo base_url().'reward/del_sem/'; ?>'+res;
    }
    });
}
</script>