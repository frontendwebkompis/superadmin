<div class="row">
    <section class="col-lg connectedSortable ui-sortable">
        <div class="card card-info">
            <div class="card-header">
                <div class="card-title">
                  <?php echo $title_card?>
                </div>
              </div>
            <div class="card-body">
                <div class="tab-content p-0">

                    <form action="<?php echo $action; ?>" method="post" id="dataform" enctype="multipart/form-data">
                        <div class="form-group">
                            <label for="varchar">Status User <?php echo form_error('status_user') ?></label>
                            <select id="status_user" class="form-control" name="status_user">
                                <?php
                                if ($status_user == 1) {
                                ?>
                                    <option value="1" selected>Bank Sampah</option>
                                    <option value="2">Nasabah</option>
                                    <option value="3">Pengepul</option>
                                <?php
                                } else if ($status_user == 2) { ?>
                                    <option value="1">Bank Sampah</option>
                                    <option value="2" selected>Nasabah</option>
                                    <option value="3">Pengepul</option>
                                <?php  } else { ?>
                                    <option value="1">Bank Sampah</option>
                                    <option value="2" >Nasabah</option>
                                    <option value="3" selected>Pengepul</option>
                                <?php } ?>
                            </select>
                        </div>
                        <div class="form-group">
							<label for="varchar">Pilih Brand <?php echo form_error('id_brand') ?></label>
							<select id="id_brand" class="form-control" name="id_brand">
								<option value="">Pilih Brand</option>
								<?php foreach ($brand as $brand){ ?>
								<option <?=  $brand->id_brand ==  $id_brand ? 'selected' : ''; ?>
								value="<?php echo $brand->id_brand; ?>">
								<?php echo $brand->nama_brand; ?>
								</option>
								<?php }?>
							</select>
						</div>
                        <div class="form-group">
                            <label for="varchar">Nama Reward <?php echo form_error('nama_reward') ?></label>
                            <input type="text" class="form-control" name="nama_reward" id="nama_reward" placeholder="Nama Reward" value="<?php echo $nama_reward; ?>" />
                        </div>
                        <div class="form-group">
                            <label for="varchar">Gambar Reward <?php echo form_error('gambar_reward') ?></label>
                            <?php if($gambar_reward !=""){ ?>
                                <div class="row justify-content-center mb-2">
                                    <img style="height:100px" src="<?php echo base_url(); ?>reward/get_image?img=reward/<?php echo basename($gambar_reward); ?>" alt="Gambar Reward">
                                </div>
                                <div class="row justify-content-center mb-2">
                                    <button class="btn btn-sm btn-secondary" type="button" onclick="cek()"><i class="fas fa-image"></i> Ganti Gambar</button>
                                </div>
                            <input type="file" style="display:none" accept="image/*" class="form-control " name="gambar_rewardbaru"
                                id="gambar_rewardbaru">
                            <input type="hidden" id="gambar_reward" accept="image/*" value="<?php echo $gambar_reward ?>" name="gambar_reward">
                            <?php }else{ ?>
                            <input required type="file" accept="image/*" class="form-control" name="gambar_rewardbaru" id="gambar_rewardbaru">
                            <?php } ?>
                        </div>
                        <div class="form-group">
                            <label for="varchar">Detail Reward <?php echo form_error('detail_reward') ?></label>
                            <textarea name="detail_reward" class="form-control" id="detail_reward" cols="30" rows="3"><?php echo $detail_reward; ?></textarea> </div>
                        <div class="form-group">
                            <label for="int">Poin Reward <?php echo form_error('poin_reward') ?></label>
                            <input type="text" onkeypress="return Angkasaja(event)" class="form-control" name="poin_reward" id="poin_reward" placeholder="Poin Reward" value="<?php echo $poin_reward; ?>" />
                        </div>
                      
                        <input type="hidden" name="id_reward" value="<?php echo $id_reward; ?>" />
					
                        <button id="btn-simpan" type="submit" class="btn btn-info" style="width: 100px;">Simpan</button>
                        <a href="<?php echo site_url('reward') ?>" class="btn btn-default" style="width: 100px;">Batal</a>
                    </form>
                </div>
            </div>
        </div>
    </section>
</div>

<script type="text/javascript">

    $(document).ready(function () {
      $('#dataform').validate({
        rules: {
            status_user: {
                required: true,
            },
            id_brand: {
                required: true,
            },
            nama_reward: {
                required: true,
            },
            detail_reward: {
                required: true,
            },
            poin_reward: {
                required: true,
            }
        },
        messages: {
            status_user: {
                required: "Status User Tidak Boleh Kosong",
            },
            id_brand: {
                required: "Brand Tidak Boleh Kosong",
            },
            nama_reward: {
                required: "Nama Reward Tidak Boleh Kosong",
            },
            detail_reward: {
                required: "Detail Reward Tidak Boleh Kosong",
            },
            poin_reward: {
                required: "Poin Reward Tidak Boleh Kosong",
            }
        },
        submitHandler: function(form) {
            $('#btn-simpan').prop('disabled', true);
            form.submit();
        },
        errorElement: 'span',
        errorPlacement: function (error, element) {
          error.addClass('invalid-feedback');
          element.closest('.form-group').append(error);
        },
        highlight: function (element, errorClass, validClass) {
          $(element).addClass('is-invalid');
        },
        unhighlight: function (element, errorClass, validClass) {
          $(element).removeClass('is-invalid');
        },
      });
    });

    function Angkasaja(evt) {
        var charCode = (evt.which) ? evt.which : event.keyCode
        if (charCode > 31 && (charCode < 48 || charCode > 57))
            return false;
        return true;
    }

    function cek() {
        var x = document.getElementById("gambar_rewardbaru");
        if (x.style.display === "none") {
            x.style.display = "block";
        } else {
            x.style.display = "none";
        }
    }
</script>