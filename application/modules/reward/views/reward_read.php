<div class="row">
	<section class="col-lg connectedSortable ui-sortable">
		<div class="card card-info">
			<div class="card-header">
                <div class="card-title">
                  <?php echo $title_card?>
                </div>
              </div>
			<div class="card-body">
				<div class="tab-content p-0">
					<div class="row">
						<div class="col-md-8">
							<table class="table table-bordered table-striped table-condensed" style="margin-bottom: 20px">
								<tr><td class="font-weight-bold" width="300px">Nama Brand</td><td><?php echo $nama_brand; ?></td></tr>
								<tr><td class="font-weight-bold" width="300px">Nama Reward</td><td><?php echo $nama_reward; ?></td></tr>
								<tr><td class="font-weight-bold" width="300px">Detail Reward</td><td><?php echo $detail_reward; ?></td></tr>
								<tr><td class="font-weight-bold" width="300px">Poin Reward</td><td><?php echo $poin_reward; ?></td></tr>
								<tr><td class="font-weight-bold" width="300px">Status User</td><td><?php if($status_user==1) echo "Bank Sampah"; else if($status_user==1) echo "Nasabah";else echo "Pengepul";?></td></tr>
							</table>
						</div>
						<div class="col-md-4">
							<div class="row justify-content-center">
								<label>Gambar Reward</label>
							</div>
							<div class="row justify-content-center">
								<img src="<?php echo base_url(); ?>reward/get_image?img=reward/<?php echo basename($gambar_reward); ?>" alt="Gambar Reward" style="max-height:150px; max-width: 250px;">
							</div>
						</div>
					</div>

					<div>
						<a href="<?php echo site_url('reward') ?>" class="btn btn-info" style="width: 100px;">Batal</a>
					</div>

				</div>
			</div>
		</div>
	</section>
</div>
