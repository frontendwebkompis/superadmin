
<div class="tab-content p-0" style="overflow:auto">
<div class="btn-group">
    <?php 
        if($tombol_aktif == 'online'){
            echo anchor(site_url('jenis_tarik_saldo'),'Online', 'class="btn btn-success btn-sm" data-toggle="tooltip" data-placement="top" title="Jenis Tarik Saldo Online" style="width: 150px;"'); 
        } else {
            echo anchor(site_url('jenis_tarik_saldo'),'Online', 'class="btn btn-default btn-sm" data-toggle="tooltip" data-placement="top" title="Jenis Tarik Saldo Online" style="width: 150px;"'); 
        } 
    ?>
    <?php 
        if($tombol_aktif == 'offline'){
            echo anchor(site_url('jenis_tarik_saldo/offline'),'Offline', 'class="btn btn-primary btn-sm" data-toggle="tooltip" data-placement="top" title="Jenis Tarik Saldo Offline" style="width: 150px;"'); 
        } else {
            echo anchor(site_url('jenis_tarik_saldo/offline'),'Offline', 'class="btn btn-default btn-sm" data-toggle="tooltip" data-placement="top" title="Jenis Tarik Saldo Offline" style="width: 150px;"'); 
        }
    ?>
</div>
</div>