<div class="row">
    <section class="col-lg connectedSortable ui-sortable">
        <div class="card card-info">
            <div class="card-header">
                <div class="card-title">
                    <?php echo $titleCard ?>
                </div>
            </div>
            <div class="card-body">
                <div class="row">
                    <div class="col-md-12">
                        <?php $this->load->view('jenis_tarik_saldo/tombol_atas'); ?>
                    </div>
                </div>
                <div class="row mt-2 mb-2">
                    <div class="col-md-8">
                        <button data-toggle="modal" data-target="#modalCreateJTS" class="btn btn-<?= bgCard() ?> btn-sm"><i class="fas fa-pencil-alt"></i> Tambah Jenis Tarik Saldo Offline</button>
                    </div>
                    <div class="col-md-4 text-right">
                        <form action="<?php echo site_url('jenis_tarik_saldo/offline'); ?>" class="form-inline float-right" method="get">
                            <div class="input-group">
                                <input type="text" class="form-control-sm" placeholder="Cari Jenis Tarik Saldo Offline" name="q" value="<?php echo $q; ?>">
                                <span class="btn-group btn-group-sm">
                                    <?php if ($q <> '') { ?>
                                        <a href="<?php echo site_url('jenis_tarik_saldo/offline'); ?>" class="btn btn-default btn-sm"><i class="fas fa-redo fa-sm"></i></a>
                                    <?php } ?>
                                    <button class="btn btn-info btn-sm" type="submit"><i class="fas fa-search fa-sm"></i></button>
                                </span>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="tab-content p-0" style="overflow:auto">
                    <table class="table table-bordered table-striped table-condensed table-hover table-sm" style="margin-bottom: 10px">
                        <tr>
                            <th class="text-center" width="50px">No</th>
                            <th class="text-center">Nama Jenis</th>
                            <th class="text-center">Status</th>
                            <th class="text-center">Aksi</th>
                        </tr>
                        <?php
                        if ($total_rows == 0) {
                            echo '<tr><td colspan="10" bgcolor="grey" style="color: white; font-weight: bold; text-align: center;">Data Tidak Ditemukan</td></tr>';
                        } else {
                            foreach ($jenis as $data) { ?>
                                <tr>
                                    <td class="text-center"><?php echo ++$start ?></td>
                                    <td><?php echo $data->nama_jenis_tarik_saldo_offline ?></td>
                                    <td class='text-center'><?php if ($data->status_jenis_tarik_saldo_offline_aktif == 1) {
                                                                echo '<span class="badge badge-success">Aktif</span>';
                                                            } else {
                                                                echo '<span class="badge badge-danger">Non-aktif</span>';
                                                            }  ?></td>
                                    <td class='text-center' width="100px">
                                        <div class="btn-group btn-group-sm">
                                            <?php
                                            if ($data->status_jenis_tarik_saldo_offline_aktif == 1) { ?>
                                                <button onclick="cekStatus('<?= $data->id_jenis_tarik_saldo_offline ?>')" class="btn btn-secondary" title="Non-aktifkan"><i class="fas fa-times-circle"></i></button>
                                            <?php } else { ?>
                                                <button onclick="cekStatus('<?= $data->id_jenis_tarik_saldo_offline ?>')" class="btn btn-success"><i class="fas fa-check-circle" title="Aktifkan"></i></button>
                                            <?php }
                                            ?>
                                            <button class="btn btn-success btn-sm" title="Edit" data-toggle="modal" data-target="#modalEditJenis" onclick="cekEditJenis('<?= $data->id_jenis_tarik_saldo_offline ?>')"><i class="fas fa-edit"></i></button>

                                            <button onclick="cekDelete('<?= $data->id_jenis_tarik_saldo_offline ?>')" class="btn btn-danger" title="Hapus"><i class="fas fa-trash"></i></button>
                                        </div>
                                    </td>
                                </tr>
                        <?php }
                        } ?>
                    </table>
                </div>
                <?= footer($total_rows, $pagination, '') ?>
            </div>
        </div>
    </section>
</div>


<div id="modalCreateJTS" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-md" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Form Tambah Jenis Tarik Saldo Offline</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form action="<?= $action ?>" method="post" id="formTambahMetode" role="form" enctype="multipart/form-data">
                    <div class="row ml-3 mr-3">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="varchar">Nama jenis tarik saldo Offline</label>
                                <input type="text" class="form-control" name="nama_jenis_tarik_saldo_offline" id="nama_jenis_tarik_saldo_offline" placeholder="Nama Jenis tarik saldo online" value="" />
                            </div>
                            <div class="form-group">
                                <label for="id_menu_tarik_saldo_online">Status Aktif</label>
                                <select name="status_jenis_tarik_saldo_offline_aktif" id="status_jenis_tarik_saldo_offline_aktif" class="form-control" required>
                                    <option value="">Pilih Status Aktif</option>
                                    <option value="1">Aktif</option>
                                    <option value="0">Non Aktif</option>
                                </select>
                            </div>

                        </div>
                    </div>
            </div>
            <div class="modal-footer">
                <div class="col-md-12 text-center">
                    <button type="submit" class="btn btn-<?= bgCard() ?>" style="width: 100px;">Simpan</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal" style="width: 100px;">Batal</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="modalEditJenis" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Form Edit Jenis</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form action="<?= $action ?>" method="post" id="formEditJenis" role="form" enctype="multipart/form-data">
                    <div class="row ml-3 mr-3">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="varchar">Nama jenis tarik saldo Offline</label>
                                <input type="text" class="form-control" name="nama_jenis_tarik_saldo_offline" id="nama_jenis_tarik_saldo_offline" placeholder="Nama Jenis tarik saldo online" value="" />
                            </div>
                            <div class="form-group">
                                <label for="id_menu_tarik_saldo_online">Status Aktif</label>
                                <select name="edit_status_jenis_tarik_saldo_offline_aktif" id="edit_status_jenis_tarik_saldo_offline_aktif" class="form-control" required>
                                    <option value="">Pilih Status Aktif</option>
                                    <option value="1">Aktif</option>
                                    <option value="0">Non Aktif</option>
                                </select>
                            </div>

                        </div>
                    </div>
            </div>
            <div class="modal-footer">
                <div class="col-md-12 text-center">
                    <button type="submit" class="btn btn-<?= bgCard() ?>" style="width: 100px;">Simpan</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal" style="width: 100px;">Batal</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    $(function() {});
    //     $('#modalCreateversi #formTambahversi').validate({
    //         rules: {
    //             status_aplikasi : {
    //                 required : true
    //             },
    //             keterangan_versi : {
    //                 required : true
    //             },
    //              versi : {
    //                 required : true
    //             }
    //         },
    //         messages: {
    //             status_aplikasi : {
    //                 required : "Nama versi Tidak Boleh Kosong"
    //             },
    //             keterangan_versi : {
    //                 required : "Jenis Produk Tidak Boleh Kosong"
    //             },
    //             versi : {
    //                 required : "Versi Tidak Boleh Kosong"
    //             }
    //         },
    //         errorElement: 'span',
    //         errorPlacement: function(error, element) {
    //             error.addClass('invalid-feedback');
    //             element.closest('.form-group').append(error);
    //         },
    //         highlight: function(element, errorClass, validClass) {
    //             $(element).addClass('is-invalid');
    //         },
    //         unhighlight: function(element, errorClass, validClass) {
    //             $(element).removeClass('is-invalid');
    //         }
    //     })
    //     $('#modalEditversi #formEditversi').validate({
    //         rules: {
    //             status_aplikasi : {
    //                 required : true
    //             },
    //             keterangan_versi : {
    //                 required : true
    //             },
    //             versi : {
    //                 required : true
    //             }
    //         },
    //         messages: {
    //             status_aplikasi : {
    //                 required : "Nama versi Tidak Boleh Kosong"
    //             },
    //             keterangan_versi : {
    //                 required : "Jenis Kendaraan Tidak Boleh Kosong"
    //             },
    //             versi : {
    //                 required : "Plat Kendaraan Tidak Boleh Kosong"
    //             }
    //         },
    //         errorElement: 'span',
    //         errorPlacement: function(error, element) {
    //             error.addClass('invalid-feedback');
    //             element.closest('.form-group').append(error);
    //         },
    //         highlight: function(element, errorClass, validClass) {
    //             $(element).addClass('is-invalid');
    //         },
    //         unhighlight: function(element, errorClass, validClass) {
    //             $(element).removeClass('is-invalid');
    //         }
    //     })

    //     $('#modalCreateversi #keterangan_versi').select2({
    //         theme: 'bootstrap'
    //     })

    //     $('#modalEditversi #edit_keterangan_versi').select2({
    //         theme: 'bootstrap'
    //     })
    // })

    // $('#modalCreateversi').on('hidden.bs.modal', function(){
    //     $('#modalCreateversi #status_aplikasi').val('')
    //     $('#modalCreateversi #status_aplikasi').removeClass('is-invalid')
    //     $('#modalCreateversi #keterangan_versi').val('').trigger('change')
    //     $('#modalCreateversi #keterangan_versi').removeClass('is-invalid')
    //     $('#modalCreateversi #versi').val('')
    //     $('#modalCreateversi #versi').removeClass('is-invalid')
    // })

    $('#modalEditJenis').on('hidden.bs.modal', function() {
        $('#modalEditJenis #edit_status_jenis_tarik_saldo_offline_aktif').val('').trigger('change')
        $('#modalEditJenis #edit_status_jenis_tarik_saldo_offline_aktif').removeClass('is-invalid')
        $('#modalEditJenis #nama_jenis_tarik_saldo_offline').val('')
        $('#modalEditJenis #nama_jenis_tarik_saldo_offline').removeClass('is-invalid')
    })

    function cekEditJenis(id) {
        startloading('#modalEditJenis .modal-content')
        $('#modalEditJenis #formEditJenis').attr('action', '<?= base_url('jenis_tarik_saldo/edit_action_offline/') ?>' + id)
        $.ajax({
            url: '<?php echo base_url() ?>jenis_tarik_saldo/ambilDataJenisOffline/' + id,
            dataType: "json",
            cache: false,
            success: function(res) {
                $('#modalEditJenis #nama_jenis_tarik_saldo_offline').val(res.nama_jenis_tarik_saldo_offline)
                $('#modalEditJenis #edit_status_jenis_tarik_saldo_offline_aktif').val(res.status_jenis_tarik_saldo_offline_aktif).trigger('change')
                stoploading('#modalEditJenis .modal-content')
            }
        });
    }

    // function cekNonAktifVersi(id){
    //     swal({
    //           title: 'Yakin mengaktifkan Versi?',
    //             text: "Mengaktifkan Versi Berimbas Pada Aplikasi!",
    //             type: 'warning',
    //             showCancelButton: true,
    //             confirmButtonColor: '#CD5C5C',
    //             cancelButtonColor: '#d33',
    //             cancelButtonText: 'Batal',
    //             confirmButtonText: 'Non Aktifkan',
    //             closeOnConfirm: false,
    //         },
    //         function(isConfirm) {
    //             if (isConfirm) {
    //                 window.location.href = "<?php echo site_url('versi_app/nonaktif/'); ?>" + id;
    //             } else {

    //             }
    //         });
    // } 
    //  function cekAktifVersi(id){
    //     swal({
    //             title: 'Yakin mengaktifkan Versi?',
    //             text: "Mengaktifkan Versi Berimbas Pada Aplikasi!",
    //             type: 'warning',
    //             showCancelButton: true,
    //             confirmButtonColor: '#CD5C5C',
    //             cancelButtonColor: '#d33',
    //             cancelButtonText: 'Batal',
    //             confirmButtonText: 'Aktifkan',
    //             closeOnConfirm: false,
    //         },
    //         function(isConfirm) {
    //             if (isConfirm) {
    //                 window.location.href = "<?php echo site_url('versi_app/aktif/'); ?>" + id;
    //             } else {

    //             }
    //         });
    // }  
    function cekDelete(id) {
        swal({
                title: 'Yakin menghapus data?',
                text: "Data yang sudah terhapus tidak dapat dikembalikan!",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#CD5C5C',
                cancelButtonColor: '#d33',
                cancelButtonText: 'Batal',
                confirmButtonText: 'Hapus',
                closeOnConfirm: false,
            },
            function(isConfirm) {
                if (isConfirm) {
                    window.location.href = "<?php echo site_url('jenis_tarik_saldo/delete_offline/'); ?>" + id;
                } else {

                }
            });
    }

    function cekStatus(id) {
        swal({
                title: 'Yakin merubah status data?',
                text: "Data akan di merubah status!",
                type: 'info',
                showCancelButton: true,
                confirmButtonColor: '#CD5C5C',
                cancelButtonColor: '#d33',
                cancelButtonText: 'Batal',
                confirmButtonText: 'Ubah',
                closeOnConfirm: false,
            },
            function(isConfirm) {
                if (isConfirm) {
                    window.location.href = "<?php echo site_url('jenis_tarik_saldo/status_offline/'); ?>" + id;
                } else {

                }
            });
    }
</script>