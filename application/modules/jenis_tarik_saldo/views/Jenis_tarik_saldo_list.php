<div class="row">
    <section class="col-lg connectedSortable ui-sortable">
        <div class="card card-info">
            <div class="card-header">
                <div class="card-title">
                    <?php echo $titleCard ?>
                </div>
            </div>
            <div class="card-body">
                <div class="row">
                    <div class="col-md-12">
                        <?php $this->load->view('jenis_tarik_saldo/tombol_atas'); ?>
                    </div>
                </div>
                <div class="row mt-2 mb-2">
                    <div class="col-md-8">
                        <button data-toggle="modal" data-target="#modalCreateJTS" class="btn btn-<?= bgCard() ?> btn-sm"><i class="fas fa-pencil-alt"></i> Tambah Jenis Tarik Saldo Online</button>
                    </div>
                    <div class="col-md-4 text-right">
                        <form action="<?php echo site_url('jenis_tarik_saldo/index'); ?>" class="form-inline float-right" method="get">
                            <div class="input-group">
                                <input type="text" class="form-control-sm" placeholder="Cari Jenis Tarik Saldo Online" name="q" value="<?php echo $q; ?>">
                                <span class="btn-group btn-group-sm">
                                    <?php if ($q <> '') { ?>
                                        <a href="<?php echo site_url('jenis_tarik_saldo'); ?>" class="btn btn-default btn-sm"><i class="fas fa-redo fa-sm"></i></a>
                                    <?php } ?>
                                    <button class="btn btn-info btn-sm" type="submit"><i class="fas fa-search fa-sm"></i></button>
                                </span>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="tab-content p-0" style="overflow:auto">
                    <table class="table table-bordered table-striped table-condensed table-hover table-sm" style="margin-bottom: 10px">
                        <tr>
                            <th class="text-center" width="50px">No</th>
                            <th class="text-center">Icon</th>
                            <th class="text-center">Nama Menu</th>
                            <th class="text-center">Nama Jenis</th>
                            <th class="text-center">Aksi</th>
                        </tr>
                        <?php
                        if ($total_rows == 0) {
                            echo '<tr><td colspan="10" bgcolor="grey" style="color: white; font-weight: bold; text-align: center;">Data Tidak Ditemukan</td></tr>';
                        } else {
                            foreach ($jenis as $data) { ?>
                                <tr>
                                    <td class="text-center"><?php echo ++$start ?></td>
                                    <td class='text-center'>
                                        <img src="<?= $data->icon_menu_tarik_saldo_online ?>" alt="" class="" style="max-width: 40px; max-height: 50px;">
                                    </td>
                                    <td><?php echo $data->nama_menu_tarik_saldo_online ?></td>
                                    <td><?php echo $data->nama_jenis_tarik_saldo_online ?></td>
                                    <td class='text-center' width="100px">
                                        <div class="btn-group btn-group-sm">
                                            <!-- <button class="btn btn-<?= bgCard() ?> btn-sm" title="Detail"><i class="fas fa-info-circle"></i></button> -->
                                            <button class="btn btn-success btn-sm" title="Edit" data-toggle="modal" data-target="#modalEditJenis" onclick="cekEditJenis('<?= $data->id_jenis_tarik_saldo_online ?>')"><i class="fas fa-edit"></i></button>
                                            <button onclick="cekDelete('<?= $data->id_jenis_tarik_saldo_online ?>')" class="btn btn-danger"><i class="fas fa-trash"></i></button>
                                        </div>
                                    </td>
                                </tr>
                        <?php }
                        } ?>
                    </table>
                </div>
                <?= footer($total_rows, $pagination, site_url('jenis_tarik_saldo/exportxl?q=' . $q)) ?>

            </div>
        </div>
    </section>
</div>



<div id="modalCreateJTS" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-md" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Form Tambah Jenis Tarik Saldo Online</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form action="<?= $action ?>" method="post" id="formTambahMetode" role="form" enctype="multipart/form-data">
                    <div class="row ml-3 mr-3">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="varchar">ID jenis tarik saldo online</label>
                                <input type="text" class="form-control" name="id_jenis_tarik_saldo_online" id="id_jenis_tarik_saldo_online" placeholder="ID Jenis tarik saldo online" value="" />
                            </div>
                            <div class="form-group">
                                <label for="id_menu_tarik_saldo_online">ID Menu tarik saldo online</label>
                                <select name="id_menu_tarik_saldo_online" id="id_menu_tarik_saldo_online" class="form-control" required>
                                    <option value="">Pilih Menu tarik saldo online</option>
                                    <?php
                                    foreach ($menu_jenis as $data) { ?>
                                        <option value="<?php echo $data->id_menu_tarik_saldo_online; ?>">
                                            <?php echo $data->nama_menu_tarik_saldo_online; ?>
                                        </option>
                                    <?php }
                                    ?>
                                </select>
                                <a href="<?php echo base_url() ?>menu_tarik_saldo">Tambah Menu Tarik Saldo</a>
                            </div>
                            <div class="form-group">
                                <label for="varchar">Nama jenis tarik saldo online</label>
                                <input type="text" class="form-control" name="nama_jenis_tarik_saldo_online" id="nama_jenis_tarik_saldo_online" placeholder="Nama jenis tarik saldo online" value="" />
                            </div>
                        </div>
                    </div>
            </div>
            <div class="modal-footer">
                <div class="col-md-12 text-center">
                    <button type="submit" class="btn btn-<?= bgCard() ?>" style="width: 100px;">Simpan</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal" style="width: 100px;">Batal</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="modalEditJenis" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Form Tambah Jenis Tarik Saldo Online</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form action="<?= $action ?>" method="post" id="formEditJenis" role="form" enctype="multipart/form-data">
                    <div class="row ml-3 mr-3">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="varchar">ID jenis tarik saldo online</label>
                                <input type="text" class="form-control" name="id_jenis_tarik_saldo_online" id="id_jenis_tarik_saldo_online" placeholder="ID Jenis tarik saldo online" value="" />
                            </div>
                            <div class="form-group">
                                <label for="edit_id_menu_tarik_saldo_online">ID Menu tarik saldo online</label>
                                <select name="edit_id_menu_tarik_saldo_online" id="edit_id_menu_tarik_saldo_online" class="form-control" required>
                                    <option value="">Pilih Menu tarik saldo online</option>
                                    <?php
                                    foreach ($menu_jenis as $data) { ?>
                                        <option value="<?php echo $data->id_menu_tarik_saldo_online; ?>">
                                            <?php echo $data->nama_menu_tarik_saldo_online; ?>
                                        </option>
                                    <?php }
                                    ?>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="varchar">Nama jenis tarik saldo online</label>
                                <input type="text" class="form-control" name="nama_jenis_tarik_saldo_online" id="nama_jenis_tarik_saldo_online" placeholder="Nama jenis tarik saldo online" value="" />
                            </div>
                        </div>
                    </div>
            </div>
            <div class="modal-footer">
                <div class="col-md-12 text-center">
                    <button type="submit" class="btn btn-<?= bgCard() ?>" style="width: 100px;">Simpan</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal" style="width: 100px;">Batal</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- <div id="modalEditversi" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Form Edit versi</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form action="<?= $action ?>" method="post" id="formEditversi" role="form" enctype="multipart/form-data">
                <div class="row ml-3 mr-3">
                    <div class="col-md-12">
                       <div class="form-group">
                            <label for="varchar">Jenis Aplikasi</label>
                            <select name="status_aplikasi" id="status_aplikasi" class="form-control" required>
                                <option value="">Pilih Jenis Aplikasi</option>
                                <option value=0>Bank Sampah</option>
                                <option value=1>Nasabah</option>
                                <option value=2>Pengepul</option>
                              
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="varchar">Versi</label>
                            <input type="text" class="form-control" name="versi" id="versi" placeholder="Versi" value="" />
                        </div>
                         <div class="form-group">
                            <label for="varchar">Keterangan Versi</label>
                            <input type="text" class="form-control" name="keterangan_versi" id="keterangan_versi" placeholder="Keterangan Versi" value="" />
                        </div> 
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <div class="col-md-12 text-center">
                    <button type="submit" class="btn btn-<?= bgCard() ?>" style="width: 100px;">Simpan</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal" style="width: 100px;">Batal</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    $(function(){
        $('#modalCreateversi #formTambahversi').validate({
            rules: {
                status_aplikasi : {
                    required : true
                },
                keterangan_versi : {
                    required : true
                },
                 versi : {
                    required : true
                }
            },
            messages: {
                status_aplikasi : {
                    required : "Nama versi Tidak Boleh Kosong"
                },
                keterangan_versi : {
                    required : "Jenis Produk Tidak Boleh Kosong"
                },
                versi : {
                    required : "Versi Tidak Boleh Kosong"
                }
            },
            errorElement: 'span',
            errorPlacement: function(error, element) {
                error.addClass('invalid-feedback');
                element.closest('.form-group').append(error);
            },
            highlight: function(element, errorClass, validClass) {
                $(element).addClass('is-invalid');
            },
            unhighlight: function(element, errorClass, validClass) {
                $(element).removeClass('is-invalid');
            }
        })
        $('#modalEditversi #formEditversi').validate({
            rules: {
                status_aplikasi : {
                    required : true
                },
                keterangan_versi : {
                    required : true
                },
                versi : {
                    required : true
                }
            },
            messages: {
                status_aplikasi : {
                    required : "Nama versi Tidak Boleh Kosong"
                },
                keterangan_versi : {
                    required : "Jenis Kendaraan Tidak Boleh Kosong"
                },
                versi : {
                    required : "Plat Kendaraan Tidak Boleh Kosong"
                }
            },
            errorElement: 'span',
            errorPlacement: function(error, element) {
                error.addClass('invalid-feedback');
                element.closest('.form-group').append(error);
            },
            highlight: function(element, errorClass, validClass) {
                $(element).addClass('is-invalid');
            },
            unhighlight: function(element, errorClass, validClass) {
                $(element).removeClass('is-invalid');
            }
        })

        $('#modalCreateversi #keterangan_versi').select2({
            theme: 'bootstrap'
        })

        $('#modalEditversi #edit_keterangan_versi').select2({
            theme: 'bootstrap'
        })
    })

    $('#modalCreateversi').on('hidden.bs.modal', function(){
        $('#modalCreateversi #status_aplikasi').val('')
        $('#modalCreateversi #status_aplikasi').removeClass('is-invalid')
        $('#modalCreateversi #keterangan_versi').val('').trigger('change')
        $('#modalCreateversi #keterangan_versi').removeClass('is-invalid')
        $('#modalCreateversi #versi').val('')
        $('#modalCreateversi #versi').removeClass('is-invalid')
    })

    $('#modalEditversi').on('hidden.bs.modal', function(){
        $('#modalEditversi #status_aplikasi').val('')
        $('#modalEditversi #status_aplikasi').removeClass('is-invalid')
        $('#modalEditversi #edit_keterangan_versi').val('').trigger('change')
        $('#modalEditversi #edit_keterangan_versi').removeClass('is-invalid')
        $('#modalEditversi #versi').val('')
        $('#modalEditversi #versi').removeClass('is-invalid')
    })

    function cekEditversi(id){
        startloading('#modalEditversi .modal-content')
        $('#modalEditversi #formEditversi').attr('action', '<?= base_url('versi_app/update_action/') ?>'+id)
        $.ajax({
            url: '<?php echo base_url() ?>versi_app/ambilDataversi/' + id,
            dataType: "json",
            cache: false,
            success: function(res) {
                $('#modalEditversi #status_aplikasi').val(res.status_aplikasi)
                $('#modalEditversi #edit_keterangan_versi').val(res.id_keterangan_versi).trigger('change')
                $('#modalEditversi #keterangan_versi').val(res.keterangan_versi)
                $('#modalEditversi #versi').val(res.versi)
                stoploading('#modalEditversi .modal-content')
            }
        });
    }

    function cekNonAktifVersi(id){
        swal({
              title: 'Yakin mengaktifkan Versi?',
                text: "Mengaktifkan Versi Berimbas Pada Aplikasi!",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#CD5C5C',
                cancelButtonColor: '#d33',
                cancelButtonText: 'Batal',
                confirmButtonText: 'Non Aktifkan',
                closeOnConfirm: false,
            },
            function(isConfirm) {
                if (isConfirm) {
                    window.location.href = "<?php echo site_url('versi_app/nonaktif/'); ?>" + id;
                } else {

                }
            });
    } 
     function cekAktifVersi(id){
        swal({
                title: 'Yakin mengaktifkan Versi?',
                text: "Mengaktifkan Versi Berimbas Pada Aplikasi!",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#CD5C5C',
                cancelButtonColor: '#d33',
                cancelButtonText: 'Batal',
                confirmButtonText: 'Aktifkan',
                closeOnConfirm: false,
            },
            function(isConfirm) {
                if (isConfirm) {
                    window.location.href = "<?php echo site_url('versi_app/aktif/'); ?>" + id;
                } else {

                }
            });
    }  
    function cekDeleteVersi(id){
        swal({
                title: 'Yakin menghapus data?',
                text: "Data yang sudah terhapus tidak dapat dikembalikan!",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#CD5C5C',
                cancelButtonColor: '#d33',
                cancelButtonText: 'Batal',
                confirmButtonText: 'Hapus',
                closeOnConfirm: false,
            },
            function(isConfirm) {
                if (isConfirm) {
                    window.location.href = "<?php echo site_url('versi_app/delete/'); ?>" + id;
                } else {

                }
            });
    }
</script> -->

<script type="text/javascript">
    $('#modalEditJenis').on('hidden.bs.modal', function() {
        $('#modalEditJenis #id_jenis_tarik_saldo_online').val('').trigger('change')
        $('#modalEditJenis #id_jenis_tarik_saldo_online').removeClass('is-invalid')
        $('#modalEditJenis #edit_id_menu_tarik_saldo_online').val('').trigger('change')
        $('#modalEditJenis #edit_id_menu_tarik_saldo_online').removeClass('is-invalid')
        $('#modalEditJenis #id_menu_tarik_saldo_online').val('')
        $('#modalEditJenis #id_menu_tarik_saldo_online').removeClass('is-invalid')
    })

    function cekEditJenis(id) {
        startloading('#modalEditJenis .modal-content')
        $('#modalEditJenis #formEditJenis').attr('action', '<?= base_url('jenis_tarik_saldo/edit_action/') ?>' + id)
        $.ajax({
            url: '<?php echo base_url() ?>jenis_tarik_saldo/ambilDataJnsTarikOnline/' + id,
            dataType: "json",
            cache: false,
            success: function(res) {
                $('#modalEditJenis #id_jenis_tarik_saldo_online').val(res.id_jenis_tarik_saldo_online)
                $('#modalEditJenis #nama_jenis_tarik_saldo_online').val(res.nama_jenis_tarik_saldo_online)
                $('#modalEditJenis #edit_id_menu_tarik_saldo_online').val(res.id_menu_tarik_saldo_online).trigger('change')
                stoploading('#modalEditJenis .modal-content')
            }
        });
    }

    function cekDelete(id) {
        swal({
                title: 'Yakin menghapus data?',
                text: "Data yang sudah terhapus tidak dapat dikembalikan!",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#CD5C5C',
                cancelButtonColor: '#d33',
                cancelButtonText: 'Batal',
                confirmButtonText: 'Hapus',
                closeOnConfirm: false,
            },
            function(isConfirm) {
                if (isConfirm) {
                    window.location.href = "<?php echo site_url('jenis_tarik_saldo/delete_online/'); ?>" + id;
                } else {

                }
            });
    }
</script>