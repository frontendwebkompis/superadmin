<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Jenis_tarik_saldo extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        if (cek_token()) {
            $this->load->model('Jenis_tarik_saldo_model');
            $this->load->library('form_validation');
        } else {
            logout();
        }
    }

    public function index()
    {
        $q = urldecode($this->input->get('q', TRUE));
        $start = intval($this->input->get('start'));

        if ($q <> '') {
            $config['base_url'] = base_url() . 'jenis_tarik_saldo?q=' . urlencode($q);
            $config['first_url'] = base_url() . 'jenis_tarik_saldo?q=' . urlencode($q);
        } else {
            $config['base_url'] = base_url() . 'jenis_tarik_saldo';
            $config['first_url'] = base_url() . 'jenis_tarik_saldo';
        }

        $config['per_page']             = 10;
        $config['page_query_string']    = TRUE;
        $config['total_rows']           = $this->Jenis_tarik_saldo_model->total_rows($q);
        $jenis                          = $this->Jenis_tarik_saldo_model->get_limit_data($config['per_page'], $start, $q);
        $menu_jenis                          = $this->Jenis_tarik_saldo_model->get_menu();

        $this->load->library('pagination');
        $this->pagination->initialize($config);

        $data = array(
            'titleCard'                 => 'List Jenis Tarik Saldo Online',
            'tittle'                    => 'Jenis Tarik Saldo',
            'judul'                     => 'Jenis Tarik Saldo',
            'breadcrumbactive'          => '',
            'breadcrumb'                => 'Jenis Tarik Saldo',
            'link'                      => '',
            'jenis'                     => $jenis,
            'menu_jenis'                => $menu_jenis,
            'q'                         => $q,
            'pagination'                => $this->pagination->create_links(),
            'total_rows'                => $config['total_rows'],
            'start'                     => $start,
            'action'                    => site_url('jenis_tarik_saldo/create_action'),
            'kategori_aktif'            => '1',
            'menu_aktif'                => 'jenis_tarik_saldo',
            'tombol_aktif'              => 'online'
        );
        $data['datakonten'] = $this->load->view('Jenis_tarik_saldo_list', $data, true);
        $this->load->view('layouts/main_view', $data);
    }

    public function offline()
    {
        $q = urldecode($this->input->get('q', TRUE));
        $start = intval($this->input->get('start'));

        if ($q <> '') {
            $config['base_url'] = base_url() . 'jenis_tarik_saldo/offline?q=' . urlencode($q);
            $config['first_url'] = base_url() . 'jenis_tarik_saldo/offline?q=' . urlencode($q);
        } else {
            $config['base_url'] = base_url() . 'jenis_tarik_saldo/offline';
            $config['first_url'] = base_url() . 'jenis_tarik_saldo/offline';
        }

        $config['per_page']             = 10;
        $config['page_query_string']    = TRUE;
        $config['total_rows']           = $this->Jenis_tarik_saldo_model->total_rows_offline($q);
        $jenis                          = $this->Jenis_tarik_saldo_model->get_limit_data_offline($config['per_page'], $start, $q);

        $this->load->library('pagination');
        $this->pagination->initialize($config);

        $data = array(
            'titleCard'                 => 'List Jenis Tarik Saldo Offline',
            'tittle'                    => 'Jenis Tarik Saldo',
            'judul'                     => 'Jenis Tarik Saldo',
            'breadcrumbactive'          => 'Jenis Tarik Saldo',
            'breadcrumb'                => 'Jenis Tarik Saldo',
            'link'                      => '',
            'jenis'                     => $jenis,
            'q'                         => $q,
            'pagination'                => $this->pagination->create_links(),
            'total_rows'                => $config['total_rows'],
            'start'                     => $start,
            'action'                    => site_url('jenis_tarik_saldo/create_action_offline'),
            'kategori_aktif'            => '1',
            'menu_aktif'                => 'jenis_tarik_saldo',
            'tombol_aktif'              => 'offline'
        );
        $data['datakonten'] = $this->load->view('Jenis_tarik_saldo_offline_list', $data, true);
        $this->load->view('layouts/main_view', $data);
    }

    public function create_action()
    {
        $data = array(
            'id_jenis_tarik_saldo_online' => $this->input->post('id_jenis_tarik_saldo_online', true),
            'id_menu_tarik_saldo_online' => $this->input->post('id_menu_tarik_saldo_online', true),
            'nama_jenis_tarik_saldo_online' => $this->input->post('nama_jenis_tarik_saldo_online', true),
        );
        $data = $this->db->insert('jenis_tarik_saldo_online', $data);
        if ($data) {
            $this->session->set_flashdata('berhasil', 'Data berhasil ditambahkan');
            redirect('jenis_tarik_saldo');
        } else {
            $this->session->set_flashdata('gagal', 'Data tidak ditambahkan');
            redirect('jenis_tarik_saldo');
        }
    }
    public function create_action_offline()
    {
        $dt = new DateTime();
        $prefix = "JTS";
        $row = $this->Jenis_tarik_saldo_model->get_by_prefix($prefix);
        $NoUrut = (int) substr($row->max, 3, 5);
        $NoUrut = $NoUrut + 1; //nomor urut +1
        $NoUrut = sprintf('%05d', $NoUrut);
        $fix = $prefix . $NoUrut;
        $data = array(
            'id_jenis_tarik_saldo_offline' => $fix,
            'nama_jenis_tarik_saldo_offline' => $this->input->post('nama_jenis_tarik_saldo_offline', true),
            'status_jenis_tarik_saldo_offline_aktif' => $this->input->post('status_jenis_tarik_saldo_offline_aktif', true),
            'id_superadmin' => getID(),
            'status_jenis_tarik_saldo_offline_remove' => 1,
            'created_at'        => $dt->format('Y-m-d H:i:s'),
            'update_at'        => $dt->format('Y-m-d H:i:s'),
        );
        $data = $this->db->insert('jenis_tarik_saldo_offline', $data);
        if ($data) {
            $this->session->set_flashdata('berhasil', 'Data berhasil ditambahkan');
            redirect('jenis_tarik_saldo/offline');
        } else {
            $this->session->set_flashdata('gagal', 'Data tidak ditambahkan');
            redirect('jenis_tarik_saldo/offline');
        }
    }
    public function ambilDataJnsTarikOnline($id)
    {
        $this->db->where('id_jenis_tarik_saldo_online', $id);
        $data = $this->db->get('jenis_tarik_saldo_online')->row();
        echo json_encode($data);
    }
    public function ambilDataJenisOffline($id)
    {
        $this->db->where('id_jenis_tarik_saldo_offline', $id);
        $data = $this->db->get('jenis_tarik_saldo_offline')->row();
        echo json_encode($data);
    }
    public function edit_action_offline($id)
    {
        $dt = new DateTime();
        $data = array(
            'nama_jenis_tarik_saldo_offline' => $this->input->post('nama_jenis_tarik_saldo_offline', true),
            'status_jenis_tarik_saldo_offline_aktif' => $this->input->post('edit_status_jenis_tarik_saldo_offline_aktif', true),
            'id_superadmin' => getID(),
            'update_at'        => $dt->format('Y-m-d H:i:s'),
        );
        $this->db->where('id_jenis_tarik_saldo_offline', $id);
        $data = $this->db->update('jenis_tarik_saldo_offline', $data);
        if ($data) {
            $this->session->set_flashdata('berhasil', 'Data berhasil ditambahkan');
            redirect('jenis_tarik_saldo/offline');
        } else {
            $this->session->set_flashdata('gagal', 'Data tidak ditambahkan');
            redirect('jenis_tarik_saldo/offline');
        }
    }
    public function edit_action($id)
    {
        $data = array(
            'id_jenis_tarik_saldo_online' => $this->input->post('id_jenis_tarik_saldo_online', true),
            'id_menu_tarik_saldo_online' => $this->input->post('edit_id_menu_tarik_saldo_online', true),
            'nama_jenis_tarik_saldo_online' => $this->input->post('nama_jenis_tarik_saldo_online', true),
        );
        $this->db->where('id_jenis_tarik_saldo_online', $id);
        $data = $this->db->update('jenis_tarik_saldo_online', $data);
        if ($data) {
            $this->session->set_flashdata('berhasil', 'Data berhasil ditambahkan');
            redirect('jenis_tarik_saldo');
        } else {
            $this->session->set_flashdata('gagal', 'Data tidak ditambahkan');
            redirect('jenis_tarik_saldo');
        }
    }

    public function delete_online($id)
    {
        $row = $this->Jenis_tarik_saldo_model->get_by_id_online($id);
        $dt = new DateTime();
        if ($row) {
            $this->Jenis_tarik_saldo_model->delete_online($id);
            $this->session->set_flashdata('berhasil', 'Berhasil Menghapus Data');
            redirect(site_url('jenis_tarik_saldo'));
        } else {
            $this->session->set_flashdata('gagal', 'Data Tidak Ditemukan');
            redirect(site_url('jenis_tarik_saldo'));
        }
    }
    public function status_offline($id)
    {
        $row = $this->Jenis_tarik_saldo_model->get_by_id_offline($id);
        $dt = new DateTime();
        if ($row) {
            if ($row->status_jenis_tarik_saldo_offline_aktif == 1) {
                $cek = 0;
            } else {
                $cek = 1;
            }
            $this->Jenis_tarik_saldo_model->status_offline($id, $cek);
            $this->session->set_flashdata('berhasil', 'Berhasil Menghapus Data');
            redirect(site_url('jenis_tarik_saldo/offline'));
        } else {
            $this->session->set_flashdata('gagal', 'Data Tidak Ditemukan');
            redirect(site_url('jenis_tarik_saldo/offline'));
        }
    }
    public function delete_offline($id)
    {
        $row = $this->Jenis_tarik_saldo_model->get_by_id_offline($id);
        $dt = new DateTime();
        if ($row) {
            $this->Jenis_tarik_saldo_model->delete_offline($id);
            $this->session->set_flashdata('berhasil', 'Berhasil Menghapus Data');
            redirect(site_url('jenis_tarik_saldo/offline'));
        } else {
            $this->session->set_flashdata('gagal', 'Data Tidak Ditemukan');
            redirect(site_url('jenis_tarik_saldo/offline'));
        }
    }
}

/* End of file versi_app.php */
/* Location: ./application/controllers/versi_app.php */
/* Please DO NOT modify this information : */
/* Generated by Harviacode Codeigniter CRUD Generator 2020-07-15 09:43:37 */
/* http://harviacode.com */