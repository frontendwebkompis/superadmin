<div class="card card-<?= bgCard() ?>">
    <div class="card-header">
        <div class="card-title">
            <?= $titleCard ?>
        </div>
    </div>
    <div class="card-body">
        <div class="tab-content p-0">
            <div class="row">
                <div class="col-md-3 offset-md-9 mb-2">
                    <?= search(site_url('list_saldo_nasabah_online'), site_url('list_saldo_nasabah_online'), $q) ?>
                </div>
            </div>
            <div class="tab-content p-0" style="overflow:auto">
                <table class="table table-bordered table-striped table-condensed table-hover table-sm" style="margin-bottom: 10px">
                    <tr>
                        <th class='text-center' style="width: 60px;">No</th>
                        <th class='text-center'>Nama Nasabah Online</th>
                        <th class='text-center'>Belum Aktif</th>
                        <th class='text-center'>Aktif</th>
                        <th class='text-center'>Status</th>
                        <th class='text-center' style="width: 100px;">Aksi</th>
                    </tr><?php
                    if ($total_rows == 0) {
                        echo '<tr><td colspan="11" bgcolor="grey" style="color: white; font-weight: bold; text-align: center;">Data Tidak Ditemukan</td></tr>';
                    } else {
                        foreach ($list_saldo_nasabah_online_data as $data)
                        {
                            ?>
                    <tr>
                        <td class='text-center'><?php echo ++$start ?></td>
                        <td><?php echo $data->nama_nasabah_online ?></td>
                        <td><?php echo rupiah($data->tdkaktif) ?></td>
                        <td><?php echo rupiah($data->aktif) ?></td>
                        <td class='text-center'><?php if($data->is_transfer==1) echo '<span class="badge badge-success">Sudah Terkirim</span>'; else echo '<span class="badge badge-danger">Belum Terkirim</span>';  ?></td>
                        <td class='text-center'>
                        <div class="btn-group btn-group-sm">
                            <?php if(($data->aktif == 0) || $data->is_transfer==1){ ?>
                                <button class="btn btn-danger" title="Disabel"><i class="fas fa-ban"></i></button>
                            <?php } else { ?>
                             <button class="btn btn-success" title="Konfirmasi Saldo Nasabah Online" onclick="cekTransfer('<?= $data->id_setoran_online ?>')"><i class="fas fa-check"></i></button>
                            <?php } ?>
                        </div>
                          
                        </td>
                    </tr>
                            <?php
                        }
                    }
                        ?>
                </table>
            </div>
            <?= footer($total_rows, $pagination, '') ?>
        </div>
    </div>
</div>

<div id="modalCreateMetode" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-md" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Form Tambah Ekspedisi</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form action="<?= $action ?>" method="post" id="formTambahMetode" role="form" enctype="multipart/form-data">
                <div class="row ml-3 mr-3">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="varchar">Nama Ekspedisi</label>
                            <input type="text" class="form-control" name="metode_pembayaran" id="metode_pembayaran" placeholder="Metode Pembayaran" value="" />
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <div class="col-md-12 text-center">
                    <button type="submit" class="btn btn-<?= bgCard() ?>" style="width: 100px;">Simpan</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal" style="width: 100px;">Batal</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="modalEditMetode" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Form Tambah Metode Pembayaran</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form action="<?= $action ?>" method="post" id="formTambahMetode" role="form" enctype="multipart/form-data">
                <div class="row ml-3 mr-3">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="varchar">Nama Metode Pembayaran</label>
                            <input type="text" class="form-control" name="metode_pembayaran" id="metode_pembayaran" placeholder="Metode Pembayaran" value="" />
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <div class="col-md-12 text-center">
                    <button type="submit" class="btn btn-<?= bgCard() ?>" style="width: 100px;">Simpan</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal" style="width: 100px;">Batal</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    function cekTransfer(id){
        swal({
              title: 'Yakin Konfirmasi Saldo Nasabah ?', 
              // text: "Data yang sudah terhapus tidak dapat dikembalikan!",
              type: 'info',
              showCancelButton: true,
              confirmButtonColor: '#3366ff',
              cancelButtonColor: '#d33',
              cancelButtonText: 'Batal',
              confirmButtonText: 'Konfirmasi',
              closeOnConfirm: false,
              // closeOnCancel: false
            },
            function(isConfirm){
                if (isConfirm) {
                    window.location.href = "<?php echo base_url('list_saldo_nasabah_online/transfer/'); ?>"+id;
                } else {
                }                
            });
    }
</script>
