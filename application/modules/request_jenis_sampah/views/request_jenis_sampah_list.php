<script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>

<div class="row">
	<section class="col-lg connectedSortable ui-sortable">
		<div class="card card-info">
			<div class="card-header">
                <div class="card-title">
                  <?php echo $title_card?>
                </div>
            </div>
			<div class="card-body">
				<div class="tab-content p-0">
					<div class="row">
						<div class="col-md-3 offset-md-9 mb-2">
							<?= search(site_url('request_jenis_sampah/index'), site_url('request_jenis_sampah'), $q) ?>
						</div>
					</div>
					<div class="tab-content p-0" style="overflow:auto">
					<table class="table table-bordered table-striped table-condensed table-hover table-sm" style="margin-bottom: 10px">
						<tr>
							<th class="text-center">No</th>
							<th class="text-center">Nama Bank Sampah</th>
							<th class="text-center">Nama Kategori Sampah</th>
							<th class="text-center">Nama Jenis Sampah</th>
							<th class="text-center">Foto Jenis Sampah</th>
							<th class="text-center">Dibuat Pada</th>
							<th class="text-center">Di Ubah Pada</th>
							<th class="text-center">Status</th>
							<th class="text-center">Aksi</th>
						</tr>
						<?php 
			            if($total_rows == 0){
			                echo '<tr><td colspan="11" bgcolor="grey" style="color: white; font-weight: bold; text-align: center;">Data Tidak Ditemukan</td></tr>';
			            } else {
						foreach ($request_jenis_sampah_data as $request_jenis_sampah) { ?>
							<tr>
								<td class="text-center" width="50px"><?php echo ++$start ?></td>
								<td><?php echo $request_jenis_sampah->nama_bank_sampah ?></td>
								<td><?php echo $request_jenis_sampah->nama_kategori_sampah ?></td>
								<td><?php echo $request_jenis_sampah->nama_jenis_sampah ?></td>
								<td class='text-center'>
									<img src="<?php echo $request_jenis_sampah->foto_jenis_sampah ?>" alt="Foto Jenis Sampah" style="max-width: 80px; max-height: 80px;">
								</td>
								<td><?php echo fulldate($request_jenis_sampah->created_at) ?></td>
								<td><?php echo fulldate($request_jenis_sampah->update_at) ?></td>
								<td><?php if($request_jenis_sampah->id_status==1){
									 echo "<span class='badge badge-primary'>Diterima</span>";
								}else if($request_jenis_sampah->id_status==2){
 								echo "<span class='badge badge-danger'>Ditolak</span>";
								}else{
									echo "<span class='badge badge-secondary'>Proses Request</span>";
							
								} ?></td>
								<td class="text-center" width="150px">
									<div class="btn-group">
									 <?php if($request_jenis_sampah->id_status==0){
                        	?>
										<a href="#" class="btn btn-success btn-sm" data-toggle="tooltip" data-placement="top" title="Hapus" onclick="confirm('<?= $request_jenis_sampah->id_request ?>')"><i class="fas fa-check"></i></a>
										<a href="#" class="btn btn-danger btn-sm" data-toggle="tooltip" data-placement="top" title="Hapus" onclick="tolak('<?= $request_jenis_sampah->id_request ?>')"><i class="fas fa-times"></i></a>
									<?php } else if($request_jenis_sampah->id_status==1) {
										echo "<span class='badge badge-primary'>Sudah Di Aprove</span>";
									} else{
										 echo "<span class='badge badge-danger'>Sudah Ditolak</span>";
									} ?>
									</div>
								</td>
							</tr>
						<?php } 
						} ?>
					</table>
					</div>
					<?= footer($total_rows, $pagination, site_url('request_jenis_sampah/exportxl?q=' . $q)) ?>
				</div>
			</div>
		</div>
	</section>
</div>

<script>
	function tolak(res) {
		Swal.fire({
			title: 'Anda Yakin Mau Menolak?',
			text: "Tidak bisa dikembalikan ",
			icon: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'Ya, Tolak!',
			cancelButtonText: 'Batal'
		}).then((result) => {
			if (result.value) {
				Swal.fire(
					'Tolak!',
					'Berhasil Menolak.',
					'success'
				)
				window.location = '<?php echo base_url() . 'request_jenis_sampah/tolak/'; ?>' + res;
			}
		});
	}
	function confirm(res) {
		Swal.fire({
			title: 'Anda Yakin Mau Menerima?',
			text: "Tidak bisa dikembalikan!",
			icon: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'Ya, Terima!',
			cancelButtonText: 'Batal'
		}).then((result) => {
			if (result.value) {
				Swal.fire(
					'Terima!',
					'Berhasil Menerima.',
					'success'
				)
				window.location = '<?php echo base_url() . 'request_jenis_sampah/terima/'; ?>' + res;
			}
		});
	}
</script>