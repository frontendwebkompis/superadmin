<div class="row">
	<section class="col-lg connectedSortable ui-sortable">
		<div class="card card-info">
            <div class="card-header">
                <div class="card-title">
                  <?php echo $title_card?>
                </div>
            </div>
			<div class="card-body">
				<div class="tab-content p-0">

					<table class="table table-bordered table-striped table-condensed" style="margin-bottom: 20px">
						<tr>
							<td>Nama Kategori Sampah</td>
							<td><?php echo $nama_kategori_sampah; ?></td>
						</tr>
						<tr>
							<td>Nama Treatment Sampah</td>
							<td><?php echo $nama_treatment_sampah; ?></td>
						</tr>
					</table>
					<div><a href="<?php echo site_url('kategori_sampah') ?>" class="btn btn-info">Batal</a></div>

				</div>
			</div>
		</div>
	</section>
</div>
