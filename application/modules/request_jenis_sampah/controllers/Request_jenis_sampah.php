<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Request_jenis_sampah extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        if (cek_token()) {
            $this->load->model('Request_jenis_sampah_model');
            $this->load->library('form_validation');
        } else {
            logout();
        }
    }

    public function index()
    {
        $q = urldecode($this->input->get('q', TRUE));
        $start = intval($this->input->get('start'));

        if ($q <> '') {
            $config['base_url'] = base_url() . 'request_jenis_sampah/?q=' . urlencode($q);
            $config['first_url'] = base_url() . 'request_jenis_sampah/?q=' . urlencode($q);
        } else {
            $config['base_url'] = base_url() . 'request_jenis_sampah/';
            $config['first_url'] = base_url() . 'request_jenis_sampah/';
        }

        $config['per_page'] = 10;
        $config['page_query_string'] = TRUE;
        $config['total_rows'] = $this->Request_jenis_sampah_model->total_rows($q);
        $request_jenis_sampah = $this->Request_jenis_sampah_model->get_limit_data($config['per_page'], $start, $q);

        $this->load->library('pagination');
        $this->pagination->initialize($config);

        $data = array(
            'tittle'                => 'Request Jenis Sampah',
            'judul'                 => 'Request Jenis Sampah',
            'title_card'            => 'List Request Jenis Sampah',
            'request_jenis_sampah_data'  => $request_jenis_sampah,
            'q'                     => $q,
            'pagination'            => $this->pagination->create_links(),
            'total_rows'            => $config['total_rows'],
            'start'                 => $start,
            'request_jenis_sampah_aktif' => '1',
            'menu_aktif'            => 'request_jenis_sampah'
        );
        $res['datakonten'] = $this->load->view('request_jenis_sampah/request_jenis_sampah_list', $data, true);
        $this->load->view('layouts/main_view', $res);
    }

    public function read($id)
    {
        $row = $this->Request_jenis_sampah_model->get_by_id($id);
        if ($row) {
            $data = array(
                'tittle'                => 'Kategori Sampah',
                'judul'                 => 'Kategori Sampah',
                'title_card'            => 'Detail Kategori Sampah',
                'id_request_jenis_sampah'    => $row->id_request_jenis_sampah,
                'nama_request_jenis_sampah'  => $row->nama_request_jenis_sampah,
                'nama_treatment_sampah' => $row->nama_treatment_sampah,
                'request_jenis_sampah_aktif' => '1',
                'menu_aktif'            => 'request_jenis_sampah'
            );
            $res['datakonten'] = $this->load->view('request_jenis_sampah/request_jenis_sampah_read', $data, true);
            $this->load->view('layouts/main_view', $res);
        } else {
            $this->session->set_flashdata('gagal', 'Data Tidak Di Temukan');
            redirect(site_url('request_jenis_sampah'));
        }
    }

    public function create()
    {
        $treatment = $this->Request_jenis_sampah_model->get_treat_all();
        $data = array(
            'tittle'                        => 'Kategori Sampah',
            'judul'                         => 'Kategori Sampah',
            'title_card'                    => 'Tambah Kategori Sampah',
            'button'                        => 'Create',
            'treatment'                     => $treatment,
            'id_treatment_sampah'           => set_value('id_treatment_sampah'),
            'action'                        => site_url('request_jenis_sampah/create_action'),
            'id_request_jenis_sampah'            => set_value('id_request_jenis_sampah'),
            'nama_request_jenis_sampah'          => set_value('nama_request_jenis_sampah'),
            'request_jenis_sampah_aktif'         => '1',
            'menu_aktif'                    => 'request_jenis_sampah'
        );

        $res['datakonten'] = $this->load->view('request_jenis_sampah/request_jenis_sampah_form', $data, true);
        $this->load->view('layouts/main_view', $res);
    }

    public function create_action()
    {
        $this->_rules();
        $dt = new DateTime();
        $prefix = "KAS";
        // echo $gabung;
        $row = $this->Request_jenis_sampah_model->get_by_prefix($prefix);
        $NoUrut = (int) substr($row->max, 3, 4);
        $NoUrut = $NoUrut + 1; //nomor urut +1
        $NoUrut = sprintf('%04d', $NoUrut);
        $fix = $prefix . $NoUrut;
        if ($this->form_validation->run() == FALSE) {
            $this->create();
        } else {
            $data = array(
                'id_request_jenis_sampah'            => $fix,
                'nama_request_jenis_sampah'          => $this->input->post('nama_request_jenis_sampah', TRUE),
                'id_treatment_sampah'           => $this->input->post('id_treatment_sampah', TRUE),
                'id_superadmin'                 => $this->session->userdata('uid'),
                'created_at'                    => @date('Y-m-d H:i:s'),
                'status_request_jenis_sampah_remove' => '1',
            );

            $this->Request_jenis_sampah_model->insert($data);
            $this->session->set_flashdata('berhasil', 'Berhasil Menambahkan Data');
            redirect(site_url('request_jenis_sampah'));
        }
    }

    public function update($id)
    {
        $row = $this->Request_jenis_sampah_model->get_by_id($id);
        $treatment = $this->Request_jenis_sampah_model->get_treat_all();


        if ($row) {
            $data = array(
                'tittle'                => 'Kategori Sampah',
                'judul'                 => 'Kategori Sampah',
                'title_card'            => 'Update Kategori Sampah',
                'button'                => 'Update',
                'treatment'             => $treatment,
                'action'                => site_url('request_jenis_sampah/update_action'),
                'id_request_jenis_sampah'    => set_value('id_request_jenis_sampah', $row->id_request_jenis_sampah),
                'id_treatment_sampah'   => set_value('id_treatment_sampah', $row->id_treatment_sampah),
                'nama_request_jenis_sampah'  => set_value('nama_request_jenis_sampah', $row->nama_request_jenis_sampah),
                'request_jenis_sampah_aktif' => '1',
                'menu_aktif'            => 'request_jenis_sampah'
            );
            $res['datakonten'] = $this->load->view('request_jenis_sampah/request_jenis_sampah_form', $data, true);
            $this->load->view('layouts/main_view', $res);
        } else {
            $this->session->set_flashdata('gagal', 'Data Tidak Ditemukan');
            redirect(site_url('request_jenis_sampah'));
        }
    }

    public function update_action()
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->update($this->input->post('id_request_jenis_sampah', TRUE));
        } else {
            $data = array(
                'nama_request_jenis_sampah'  => $this->input->post('nama_request_jenis_sampah', TRUE),
                'id_treatment_sampah'   => $this->input->post('id_treatment_sampah', TRUE),
                'id_superadmin'         => $this->session->userdata('uid'),
            );

            $this->Request_jenis_sampah_model->update($this->input->post('id_request_jenis_sampah', TRUE), $data);
            $this->session->set_flashdata('berhasil', 'Berhasil Merubah Data');
            redirect(site_url('request_jenis_sampah'));
        }
    }

    public function del_sem($id)
    {
        $row = $this->Request_jenis_sampah_model->get_by_id($id);

        if ($row) {
            $data = array(
                'status_request_jenis_sampah_remove' => 0,
                'id_superadmin' => $this->session->userdata('uid'),
            );

            $this->Request_jenis_sampah_model->update($id, $data);

            $this->session->set_flashdata('berhasil', 'Berhasil Menghapus Data');
            redirect(site_url('request_jenis_sampah'));
        } else {
            $this->session->set_flashdata('gagal', 'Data Tidak Ditemukan');
            redirect(site_url('request_jenis_sampah'));
        }
    }
    public function terima($id)
    {
        $row = $this->Request_jenis_sampah_model->get_by_id($id);

        if ($row) {
            $data = array(
                'id_status' => 1,
            );

            $this->Request_jenis_sampah_model->update($id, $data);

            $this->session->set_flashdata('berhasil', 'Berhasil Merubah Data');
            redirect(site_url('request_jenis_sampah'));
        } else {
            $this->session->set_flashdata('gagal', 'Data Tidak Ditemukan');
            redirect(site_url('request_jenis_sampah'));
        }
    }
    public function tolak($id)
    {
        $row = $this->Request_jenis_sampah_model->get_by_id($id);

        if ($row) {
            $data = array(
                'id_status' => 2,
            );

            $this->Request_jenis_sampah_model->update($id, $data);

            $this->session->set_flashdata('berhasil', 'Berhasil Menghapus Data');
            redirect(site_url('request_jenis_sampah'));
        } else {
            $this->session->set_flashdata('gagal', 'Data Tidak Ditemukan');
            redirect(site_url('request_jenis_sampah'));
        }
    }


    public function _rules()
    {
        $this->form_validation->set_rules('nama_request_jenis_sampah', 'nama kategori sampah', 'trim|required');

        $this->form_validation->set_rules('id_request_jenis_sampah', 'id_request_jenis_sampah', 'trim');
        $this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

    public function excel()
    {
        $this->load->helper('exportexcel');
        $namaFile = "request_jenis_sampah.xls";
        $judul = "request_jenis_sampah";
        $tablehead = 0;
        $tablebody = 1;
        $nourut = 1;
        //penulisan header
        header("Pragma: public");
        header("Expires: 0");
        header("Cache-Control: must-revalidate, post-check=0,pre-check=0");
        header("Content-Type: application/force-download");
        header("Content-Type: application/octet-stream");
        header("Content-Type: application/download");
        header("Content-Disposition: attachment;filename=" . $namaFile . "");
        header("Content-Transfer-Encoding: binary ");

        xlsBOF();

        $kolomhead = 0;
        xlsWriteLabel($tablehead, $kolomhead++, "No");
        xlsWriteLabel($tablehead, $kolomhead++, "Nama Kategori Sampah");

        foreach ($this->Request_jenis_sampah_model->get_all() as $data) {
            $kolombody = 0;

            //ubah xlsWriteLabel menjadi xlsWriteNumber untuk kolom numeric
            xlsWriteNumber($tablebody, $kolombody++, $nourut);
            xlsWriteLabel($tablebody, $kolombody++, $data->nama_request_jenis_sampah);

            $tablebody++;
            $nourut++;
        }

        xlsEOF();
        exit();
    }
    public function exportxl()
    {
        $q          = $this->input->get('q');
        $subject    = "Kategori";
        // $id         = $this->session->userdata('uid');
        $sql        = "SELECT * FROM `request_jenis_sampah` JOIN `treatment_sampah` ON `treatment_sampah`.`id_treatment_sampah` = `request_jenis_sampah`.`id_treatment_sampah` WHERE `status_request_jenis_sampah_remove` = 1";
        exportSQL('Kategori', $subject, $sql);
    }
}

/* End of file request_jenis_sampah.php */
/* Location: ./application/controllers/request_jenis_sampah.php */
/* Please DO NOT modify this information : */
/* Generated by Harviacode Codeigniter CRUD Generator 2020-02-07 19:00:53 */
/* http://harviacode.com */
