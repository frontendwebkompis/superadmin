<!doctype html>
<html>
    <head>
        <title>Tambah Nasabah_online_detail</title>
        <link rel="stylesheet" href="<?php echo base_url('assets/bootstrap/css/bootstrap.min.css') ?>"/>
        <style>
            body{
                padding: 15px;
            }
        </style>
    </head>
    <body>
        <h2 style="margin-top:0px">Pengolahan Data Nasabah_online_detail </h2>
        <form action="<?php echo $action; ?>" method="post">
	    <div class="form-group">
            <label for="varchar">Id Nasabah Online <?php echo form_error('id_nasabah_online') ?></label>
            <input type="text" class="form-control" name="id_nasabah_online" id="id_nasabah_online" placeholder="Id Nasabah Online" value="<?php echo $id_nasabah_online; ?>" />
        </div>
	    <div class="form-group">
            <label for="varchar">Alamat Nasabah Online <?php echo form_error('alamat_nasabah_online') ?></label>
            <input type="text" class="form-control" name="alamat_nasabah_online" id="alamat_nasabah_online" placeholder="Alamat Nasabah Online" value="<?php echo $alamat_nasabah_online; ?>" />
        </div>
	    <div class="form-group">
            <label for="decimal">Lat Nasabah Online <?php echo form_error('lat_nasabah_online') ?></label>
            <input type="text" class="form-control" name="lat_nasabah_online" id="lat_nasabah_online" placeholder="Lat Nasabah Online" value="<?php echo $lat_nasabah_online; ?>" />
        </div>
	    <div class="form-group">
            <label for="decimal">Long Nasabah Online <?php echo form_error('long_nasabah_online') ?></label>
            <input type="text" class="form-control" name="long_nasabah_online" id="long_nasabah_online" placeholder="Long Nasabah Online" value="<?php echo $long_nasabah_online; ?>" />
        </div>
	    <div class="form-group">
            <label for="int">Id Provinsi <?php echo form_error('id_provinsi') ?></label>
            <input type="text" class="form-control" name="id_provinsi" id="id_provinsi" placeholder="Id Provinsi" value="<?php echo $id_provinsi; ?>" />
        </div>
	    <div class="form-group">
            <label for="int">Id Kabupaten <?php echo form_error('id_kabupaten') ?></label>
            <input type="text" class="form-control" name="id_kabupaten" id="id_kabupaten" placeholder="Id Kabupaten" value="<?php echo $id_kabupaten; ?>" />
        </div>
	    <div class="form-group">
            <label for="int">Id Kecamatan <?php echo form_error('id_kecamatan') ?></label>
            <input type="text" class="form-control" name="id_kecamatan" id="id_kecamatan" placeholder="Id Kecamatan" value="<?php echo $id_kecamatan; ?>" />
        </div>
	    <div class="form-group">
            <label for="int">No Ktp Nasabah Online <?php echo form_error('no_ktp_nasabah_online') ?></label>
            <input type="text" class="form-control" name="no_ktp_nasabah_online" id="no_ktp_nasabah_online" placeholder="No Ktp Nasabah Online" value="<?php echo $no_ktp_nasabah_online; ?>" />
        </div>
	    <div class="form-group">
            <label for="varchar">Foto Ktp Nasabah Online <?php echo form_error('foto_ktp_nasabah_online') ?></label>
            <input type="text" class="form-control" name="foto_ktp_nasabah_online" id="foto_ktp_nasabah_online" placeholder="Foto Ktp Nasabah Online" value="<?php echo $foto_ktp_nasabah_online; ?>" />
        </div>
	    <div class="form-group">
            <label for="int">Saldo Nasabah Online <?php echo form_error('saldo_nasabah_online') ?></label>
            <input type="text" class="form-control" name="saldo_nasabah_online" id="saldo_nasabah_online" placeholder="Saldo Nasabah Online" value="<?php echo $saldo_nasabah_online; ?>" />
        </div>
	    <div class="form-group">
            <label for="int">Point Nasabah Online <?php echo form_error('point_nasabah_online') ?></label>
            <input type="text" class="form-control" name="point_nasabah_online" id="point_nasabah_online" placeholder="Point Nasabah Online" value="<?php echo $point_nasabah_online; ?>" />
        </div>
	    <!-- <input type="hidden" name="" value="<?php //echo $; ?>" />  -->
	    <button type="submit" class="btn btn-primary">Simpan</button> 
	    <a href="<?php echo site_url('nasabah_online_detail') ?>" class="btn btn-default">Batal</a>
	</form>
    </body>
</html>