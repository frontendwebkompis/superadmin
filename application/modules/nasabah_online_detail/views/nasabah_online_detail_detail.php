<!doctype html>
<html>
    <head>
        <title>Detail Nasabah_online_detail</title>
        <link rel="stylesheet" href="<?php echo base_url('assets/bootstrap/css/bootstrap.min.css') ?>"/>
        <style>
            body{
                padding: 15px;
            }
        </style>
    </head>
    <body>
        <h2 style="margin-top:0px">Nasabah_online_detail Detail</h2>
        <table class="table">
	    <tr><td>Id Nasabah Online</td><td><?php echo $id_nasabah_online; ?></td></tr>
	    <tr><td>Alamat Nasabah Online</td><td><?php echo $alamat_nasabah_online; ?></td></tr>
	    <tr><td>Lat Nasabah Online</td><td><?php echo $lat_nasabah_online; ?></td></tr>
	    <tr><td>Long Nasabah Online</td><td><?php echo $long_nasabah_online; ?></td></tr>
	    <tr><td>Id Provinsi</td><td><?php echo $id_provinsi; ?></td></tr>
	    <tr><td>Id Kabupaten</td><td><?php echo $id_kabupaten; ?></td></tr>
	    <tr><td>Id Kecamatan</td><td><?php echo $id_kecamatan; ?></td></tr>
	    <tr><td>No Ktp Nasabah Online</td><td><?php echo $no_ktp_nasabah_online; ?></td></tr>
	    <tr><td>Foto Ktp Nasabah Online</td><td><?php echo $foto_ktp_nasabah_online; ?></td></tr>
	    <tr><td>Saldo Nasabah Online</td><td><?php echo $saldo_nasabah_online; ?></td></tr>
	    <tr><td>Point Nasabah Online</td><td><?php echo $point_nasabah_online; ?></td></tr>
	    <tr><td></td><td><a href="<?php echo site_url('nasabah_online_detail') ?>" class="btn btn-default">Batal</a></td></tr>
	</table>
        </body>
</html>