<!doctype html>
<html>
    <head>
        <title>Crud Data Nasabah_online_detail</title>
        <link rel="stylesheet" href="<?php echo base_url('assets/bootstrap/css/bootstrap.min.css') ?>"/>
        <style>
            body{
                padding: 15px;
            }
        </style>
    </head>
    <body>
        <h2 style="margin-top:0px"> List Nasabah_online_detail</h2>
        <div class="row" style="margin-bottom: 10px">
            <div class="col-md-4">
                <?php echo anchor(site_url('nasabah_online_detail/create'),'Tambah Data', 'class="btn btn-primary"'); ?>
            </div>
            <div class="col-md-4 text-center">
                <div style="margin-top: 8px" id="message">
                    <?php echo $this->session->userdata('message') <> '' ? $this->session->userdata('message') : ''; ?>
                </div>
            </div>
            <div class="col-md-1 text-right">
            </div>
            <div class="col-md-3 text-right">
                <form action="<?php echo site_url('nasabah_online_detail/index'); ?>" class="form-inline" method="get">
                    <div class="input-group">
                        <input type="text" class="form-control" name="q" value="<?php echo $q; ?>">
                        <span class="input-group-btn">
                            <?php if ($q <> '') { ?>
                                <a href="<?php echo site_url('nasabah_online_detail'); ?>" class="btn btn-default">Reset</a>
                                <?php } ?>
                          <button class="btn btn-primary" type="submit">Pencarian</button>
                        </span>
                    </div>
                </form>
            </div>
        </div>
        <table class="table table-bordered" style="margin-bottom: 10px">
            <tr>
                <th>No</th>
                <th>Id Nasabah Online</th>
                <th>Alamat Nasabah Online</th>
                <th>Lat Nasabah Online</th>
                <th>Long Nasabah Online</th>
                <th>Id Provinsi</th>
                <th>Id Kabupaten</th>
                <th>Id Kecamatan</th>
                <th>No Ktp Nasabah Online</th>
                <th>Foto Ktp Nasabah Online</th>
                <th>Saldo Nasabah Online</th>
                <th>Point Nasabah Online</th>
                <th style="text-align:center">Aksi</th>
            </tr>
            <?php foreach ($nasabah_online_detail_data as $nasabah_online_detail) { ?>
            <tr>
                <td width="80px"><?php echo ++$start ?></td>
                <td><?php echo $nasabah_online_detail->id_nasabah_online ?></td>
                <td><?php echo $nasabah_online_detail->alamat_nasabah_online ?></td>
                <td><?php echo $nasabah_online_detail->lat_nasabah_online ?></td>
                <td><?php echo $nasabah_online_detail->long_nasabah_online ?></td>
                <td><?php echo $nasabah_online_detail->id_provinsi ?></td>
                <td><?php echo $nasabah_online_detail->id_kabupaten ?></td>
                <td><?php echo $nasabah_online_detail->id_kecamatan ?></td>
                <td><?php echo $nasabah_online_detail->no_ktp_nasabah_online ?></td>
                <td><?php echo $nasabah_online_detail->foto_ktp_nasabah_online ?></td>
                <td><?php echo $nasabah_online_detail->saldo_nasabah_online ?></td>
                <td><?php echo $nasabah_online_detail->point_nasabah_online ?></td>
                <td style="text-align:center" width="200px">
                    <?php 
                    echo anchor(site_url('nasabah_online_detail/detail/'.$nasabah_online_detail->id_nasabah_online_detail),'Lihat'); 
                    echo ' | '; 
                    echo anchor(site_url('nasabah_online_detail/update/'.$nasabah_online_detail->id_nasabah_online_detail),'Update'); 
                    echo ' | '; 
                    echo anchor(site_url('nasabah_online_detail/delete/'.$nasabah_online_detail->id_nasabah_online_detail),'Hapus','onclick="javasciprt: return confirm(\'Yakin Anda ingin Menghapus ini ?\')"'); 
                    ?>
                </td>
		    </tr>
            <?php } ?>
        </table>
        <?= footer($total_rows, $pagination, '') ?>
    </body>
</html>