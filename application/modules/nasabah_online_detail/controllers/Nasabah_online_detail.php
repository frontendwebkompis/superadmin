<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Nasabah_online_detail extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Nasabah_online_detail_model');
        $this->load->library('form_validation');
    }

    public function index()
    {
        $q = urldecode($this->input->get('q', TRUE));
        $start = intval($this->input->get('start'));
        
        if ($q <> '') {
            $config['base_url'] = base_url() . 'nasabah_online_detail/index.html?q=' . urlencode($q);
            $config['first_url'] = base_url() . 'nasabah_online_detail/index.html?q=' . urlencode($q);
        } else {
            $config['base_url'] = base_url() . 'nasabah_online_detail/index.html';
            $config['first_url'] = base_url() . 'nasabah_online_detail/index.html';
        }

        $config['per_page'] = 10;
        $config['page_query_string'] = TRUE;
        $config['total_rows'] = $this->Nasabah_online_detail_model->total_rows($q);
        $nasabah_online_detail = $this->Nasabah_online_detail_model->get_limit_data($config['per_page'], $start, $q);

        $this->load->library('pagination');
        $this->pagination->initialize($config);

        $data = array(
            'nasabah_online_detail_data' => $nasabah_online_detail,
            'q' => $q,
            'pagination' => $this->pagination->create_links(),
            'total_rows' => $config['total_rows'],
            'start' => $start,
        );
        $this->load->view('nasabah_online_detail/nasabah_online_detail_list', $data);
    }

    public function detail($id) 
    {
        $row = $this->Nasabah_online_detail_model->get_by_id($id);
        if ($row) {
            $data = array(
		'id_nasabah_online' => $row->id_nasabah_online,
		'alamat_nasabah_online' => $row->alamat_nasabah_online,
		'lat_nasabah_online' => $row->lat_nasabah_online,
		'long_nasabah_online' => $row->long_nasabah_online,
		'id_provinsi' => $row->id_provinsi,
		'id_kabupaten' => $row->id_kabupaten,
		'id_kecamatan' => $row->id_kecamatan,
		'no_ktp_nasabah_online' => $row->no_ktp_nasabah_online,
		'foto_ktp_nasabah_online' => $row->foto_ktp_nasabah_online,
		'saldo_nasabah_online' => $row->saldo_nasabah_online,
		'point_nasabah_online' => $row->point_nasabah_online,
	    );
            $this->load->view('nasabah_online_detail/nasabah_online_detail_detail', $data);
        } else {
            $this->session->set_flashdata('message', '<div class="alert alert-warning" role="alert">Data Tidak Di Temukan</div>');
            redirect(site_url('nasabah_online_detail'));
        }
    }

    public function create() 
    {
        $data = array(
        'button' => 'Create',
        'action' => site_url('nasabah_online_detail/create_action'),
	    'id_nasabah_online' => set_value('id_nasabah_online'),
	    'alamat_nasabah_online' => set_value('alamat_nasabah_online'),
	    'lat_nasabah_online' => set_value('lat_nasabah_online'),
	    'long_nasabah_online' => set_value('long_nasabah_online'),
	    'id_provinsi' => set_value('id_provinsi'),
	    'id_kabupaten' => set_value('id_kabupaten'),
	    'id_kecamatan' => set_value('id_kecamatan'),
	    'no_ktp_nasabah_online' => set_value('no_ktp_nasabah_online'),
	    'foto_ktp_nasabah_online' => set_value('foto_ktp_nasabah_online'),
	    'saldo_nasabah_online' => set_value('saldo_nasabah_online'),
	    'point_nasabah_online' => set_value('point_nasabah_online'),
	);
        $this->load->view('nasabah_online_detail/nasabah_online_detail_form', $data);
    }
    
    public function create_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->create();
        } else {
            $data = array(
		'id_nasabah_online' => $this->input->post('id_nasabah_online',TRUE),
		'alamat_nasabah_online' => $this->input->post('alamat_nasabah_online',TRUE),
		'lat_nasabah_online' => $this->input->post('lat_nasabah_online',TRUE),
		'long_nasabah_online' => $this->input->post('long_nasabah_online',TRUE),
		'id_provinsi' => $this->input->post('id_provinsi',TRUE),
		'id_kabupaten' => $this->input->post('id_kabupaten',TRUE),
		'id_kecamatan' => $this->input->post('id_kecamatan',TRUE),
		'no_ktp_nasabah_online' => $this->input->post('no_ktp_nasabah_online',TRUE),
		'foto_ktp_nasabah_online' => $this->input->post('foto_ktp_nasabah_online',TRUE),
		'saldo_nasabah_online' => $this->input->post('saldo_nasabah_online',TRUE),
		'point_nasabah_online' => $this->input->post('point_nasabah_online',TRUE),
	    );

            $this->Nasabah_online_detail_model->insert($data);
            $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">Berhasil Menambahkan Data </div>');
            redirect(site_url('nasabah_online_detail'));
        }
    }
    
    public function update($id) 
    {
        $row = $this->Nasabah_online_detail_model->get_by_id($id);

        if ($row) {
            $data = array(
            'button' => 'Update',
            'action' => site_url('nasabah_online_detail/update_action'),
            'id_nasabah_online' => set_value('id_nasabah_online', $row->id_nasabah_online),
            'alamat_nasabah_online' => set_value('alamat_nasabah_online', $row->alamat_nasabah_online),
            'lat_nasabah_online' => set_value('lat_nasabah_online', $row->lat_nasabah_online),
            'long_nasabah_online' => set_value('long_nasabah_online', $row->long_nasabah_online),
            'id_provinsi' => set_value('id_provinsi', $row->id_provinsi),
            'id_kabupaten' => set_value('id_kabupaten', $row->id_kabupaten),
            'id_kecamatan' => set_value('id_kecamatan', $row->id_kecamatan),
            'no_ktp_nasabah_online' => set_value('no_ktp_nasabah_online', $row->no_ktp_nasabah_online),
            'foto_ktp_nasabah_online' => set_value('foto_ktp_nasabah_online', $row->foto_ktp_nasabah_online),
            'saldo_nasabah_online' => set_value('saldo_nasabah_online', $row->saldo_nasabah_online),
            'point_nasabah_online' => set_value('point_nasabah_online', $row->point_nasabah_online),
            );
            $this->load->view('nasabah_online_detail/nasabah_online_detail_form', $data);
        } else {
            $this->session->set_flashdata('message', '<div class="alert alert-warning" role="alert">Data Tidak Ditemukan</div>');
            redirect(site_url('nasabah_online_detail'));
        }
    }
    
    public function update_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->update($this->input->post('', TRUE));
        } else {
            $data = array(
		'id_nasabah_online' => $this->input->post('id_nasabah_online',TRUE),
		'alamat_nasabah_online' => $this->input->post('alamat_nasabah_online',TRUE),
		'lat_nasabah_online' => $this->input->post('lat_nasabah_online',TRUE),
		'long_nasabah_online' => $this->input->post('long_nasabah_online',TRUE),
		'id_provinsi' => $this->input->post('id_provinsi',TRUE),
		'id_kabupaten' => $this->input->post('id_kabupaten',TRUE),
		'id_kecamatan' => $this->input->post('id_kecamatan',TRUE),
		'no_ktp_nasabah_online' => $this->input->post('no_ktp_nasabah_online',TRUE),
		'foto_ktp_nasabah_online' => $this->input->post('foto_ktp_nasabah_online',TRUE),
		'saldo_nasabah_online' => $this->input->post('saldo_nasabah_online',TRUE),
		'point_nasabah_online' => $this->input->post('point_nasabah_online',TRUE),
	    );

            $this->Nasabah_online_detail_model->update($this->input->post('', TRUE), $data);
            $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">Berhasil Merubah Data</div>');
            redirect(site_url('nasabah_online_detail'));
        }
    }
    
    public function delete($id) 
    {
        $row = $this->Nasabah_online_detail_model->get_by_id($id);

        if ($row) {
            $this->Nasabah_online_detail_model->delete($id);
            $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">Berhasil Menghapus Data</div>');
            redirect(site_url('nasabah_online_detail'));
        } else {
            $this->session->set_flashdata('message', '<div class="alert alert-warning" role="alert">Data Tidak Ditemukan</div>');
            redirect(site_url('nasabah_online_detail'));
        }
    }

    public function _rules() 
    {
	$this->form_validation->set_rules('id_nasabah_online', 'id nasabah online', 'trim|required');
	$this->form_validation->set_rules('alamat_nasabah_online', 'alamat nasabah online', 'trim|required');
	$this->form_validation->set_rules('lat_nasabah_online', 'lat nasabah online', 'trim|required|numeric');
	$this->form_validation->set_rules('long_nasabah_online', 'long nasabah online', 'trim|required|numeric');
	$this->form_validation->set_rules('id_provinsi', 'id provinsi', 'trim|required');
	$this->form_validation->set_rules('id_kabupaten', 'id kabupaten', 'trim|required');
	$this->form_validation->set_rules('id_kecamatan', 'id kecamatan', 'trim|required');
	$this->form_validation->set_rules('no_ktp_nasabah_online', 'no ktp nasabah online', 'trim|required');
	$this->form_validation->set_rules('foto_ktp_nasabah_online', 'foto ktp nasabah online', 'trim|required');
	$this->form_validation->set_rules('saldo_nasabah_online', 'saldo nasabah online', 'trim|required');
	$this->form_validation->set_rules('point_nasabah_online', 'point nasabah online', 'trim|required');

	$this->form_validation->set_rules('', '', 'trim');
	$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

}

/* End of file Nasabah_online_detail.php */
/* Location: ./application/controllers/Nasabah_online_detail.php */
/* Please DO NOT modify this information : */
/* Generated by Harviacode Codeigniter CRUD Generator 2020-02-13 05:54:45 */
/* http://harviacode.com */