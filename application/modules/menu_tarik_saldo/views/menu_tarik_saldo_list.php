<div class="card card-primary">
    <div class="card-header">
        <div class="card-title">
            <?= $titleCard ?>
        </div>
    </div>
    <div class="card-body">
        <div class="tab-content p-0">
            <div class="row" style="margin-bottom: 10px">
                <div class="col-md-4">
                    <button class="btn btn-sm btn-primary" title="Tambah menu_tarik_saldo" data-toggle="modal" data-target="#modalCreatemenu_tarik_saldo"><i class="fas fa-pencil-alt"></i> Tambah Data</button>
                </div>
                <div class="col-md-4 text-center">
                    <div style="margin-top: 8px" id="message">
                        <?php echo $this->session->userdata('message') <> '' ? $this->session->userdata('message') : ''; ?>
                    </div>
                </div>
                <div class="col-md-4 text-right">
                    <form action="<?php echo site_url('menu_tarik_saldo/index'); ?>" method="get" class="float-right">
                        <div class="input-group">
                            <input type="text" name="q" placeholder="Search" class="form-control-sm" value="<?php echo $q; ?>" style='width:140px;'>
                            <span class="input-group-append">
                                <?php
                                if ($q <> '') {
                                ?>
                                    <a href="<?php echo site_url('menu_tarik_saldo'); ?>" class="btn btn-secondary btn-sm">Reset</a>
                                <?php
                                }
                                ?>
                                <button type="submit" class="btn btn-warning btn-sm">Cari</button>
                            </span>
                        </div>
                    </form>
                </div>
            </div>

            <div class="tab-content p-0" style="overflow:auto">
                <table class="table table-bordered table-striped table-condensed table-hover table-sm" style="margin-bottom: 10px">
                    <tr>
                        <th class="text-center">No</th>
                        <th class="text-center">Id Menu Tarik Saldo Online</th>
                        <th class="text-center">Nama Menu Tarik Saldo Online</th>
                        <th class="text-center">Ikon Menu Sampah</th>
                        <th class="text-center">Aksi</th>
                    </tr>
                    <?php
                    if ($total_rows == 0) {
                        echo '<tr><td colspan="11" bgcolor="grey" style="color: white; font-weight: bold; text-align: center;">Data Tidak Ditemukan</td></tr>';
                    } else {
                        foreach ($menu_tarik_saldo_data as $menu_tarik_saldo) { ?>
                            <tr>
                                <td class="text-center" width="50px"><?php echo ++$start ?></td>

                                <td><?php echo $menu_tarik_saldo->id_menu_tarik_saldo_online ?></td>
                                <td><?php echo $menu_tarik_saldo->nama_menu_tarik_saldo_online ?></td>
                                <td class='text-center'><img style="max-width:80px; max-height:80px;" src="<?php echo $menu_tarik_saldo->icon_menu_tarik_saldo_online ?>"></td>
                                <td>
                                    <button class="btn btn-success btn-sm" title="Edit" data-toggle="modal" data-target="#modalEditmenu_tarik_saldo" onclick="cekEditmenu_tarik_saldo('<?= $menu_tarik_saldo->id_menu_tarik_saldo_online ?>')"><i class="fas fa-edit"></i></button>
                                </td>
                            </tr>
                    <?php }
                    } ?>
                </table>
            </div>
            <div class="row">
                <div class="col-md-6 py-2">
                    <?= totalrow($total_rows) ?>
                </div>
                <div class="col-md-6">
                    <?php echo $pagination ?>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="modalCreatemenu_tarik_saldo" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Form Tambah Menu Tarik Saldo</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form action="<?= $action ?>" method="post" id="formTambahmenu_tarik_saldo" role="form" enctype="multipart/form-data">
                    <div class="row ml-3 mr-3">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="varchar">Id Menu Tarik Saldo</label>
                                <input type="text" class="form-control" name="id_menu_tarik_saldo_online" id="id_menu_tarik_saldo_online" placeholder="Id Menu Tarik Saldo" value="" />
                            </div>
                            <div class="form-group">
                                <label for="varchar">Nama Menu Tarik Saldo</label>
                                <input type="text" class="form-control" name="nama_menu_tarik_saldo_online" id="nama_menu_tarik_saldo_online" placeholder="Nama Menu Tarik Saldo" value="" />
                            </div>
                            <div class="form-group">
                                <label for="varchar">Ikon Menu Tarik Saldo</label>
                                <input type="file" class="form-control" name="icon_menu_tarik_saldo_online" id="icon_menu_tarik_saldo_online" placeholder="Ikon Menu Tarik Saldo" value="" />
                            </div>
                        </div>
                    </div>
            </div>
            <div class="modal-footer">
                <div class="col-md-12 text-center">
                    <button type="submit" class="btn btn-<?= bgCard() ?>" style="width: 100px;">Simpan</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal" style="width: 100px;">Batal</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="modalEditmenu_tarik_saldo" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Form Edit menu_tarik_saldo</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form action="<?= $action ?>" method="post" id="formEditmenu_tarik_saldo" role="form" enctype="multipart/form-data">
                    <div class="row ml-3 mr-3">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="varchar">Id Menu Tarik Saldo</label>
                                <input type="text" class="form-control" name="id_menu_tarik_saldo_online" id="id_menu_tarik_saldo_online" placeholder="Id Menu Tarik Saldo" value="" />
                                <span style="color:red">*Merubah ID menu dapat menyebabkan perubahan pada tarik saldo online</span>
                            </div>
                            <div class="form-group">
                                <label for="varchar">Nama Menu Tarik Saldo</label>
                                <input type="text" class="form-control" name="nama_menu_tarik_saldo_online" id="nama_menu_tarik_saldo_online" placeholder="Nama Menu Tarik Saldo" value="" />
                            </div>
                            <img style="max-width:80px; max-height:80px;" id="ikon_menu" src="">
                                <button class="btn btn-sm btn-secondary" type="button" onclick="cek()"><i class="fas fa-image"></i> Ganti Gambar</button>
                            <div class="form-group">
                                <label for="varchar">Ikon Menu Tarik Saldo</label>
                                <input  style="display:none" type="file" class="form-control" name="edit_icon_menu_tarik_saldo_online" id="edit_icon_menu_tarik_saldo_online" placeholder="Ikon Menu Tarik Saldo" value="" />
                            </div> 
                            
                        </div>
                    </div>
            </div>
            <div class="modal-footer">
                <div class="col-md-12 text-center">
                    <button type="submit" class="btn btn-<?= bgCard() ?>" style="width: 100px;">Simpan</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal" style="width: 100px;">Batal</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    $(function() {
        $('#modalCreatemenu_tarik_saldo #formTambahmenu_tarik_saldo').validate({
            rules: {
                id_menu_tarik_saldo_online: {
                    required: true
                },
                icon_menu_tarik_saldo_online: {
                    required: true
                },
                nama_menu_tarik_saldo_online: {
                    required: true
                }
            },
            messages: {
                id_menu_tarik_saldo_online: {
                    required: "Nama menu_tarik_saldo Tidak Boleh Kosong"
                },
                icon_menu_tarik_saldo_online: {
                    required: "Jenis Produk Tidak Boleh Kosong"
                },
                nama_menu_tarik_saldo_online: {
                    required: "Plat Kendaraan Tidak Boleh Kosong"
                }
            },
            errorElement: 'span',
            errorPlacement: function(error, element) {
                error.addClass('invalid-feedback');
                element.closest('.form-group').append(error);
            },
            highlight: function(element, errorClass, validClass) {
                $(element).addClass('is-invalid');
            },
            unhighlight: function(element, errorClass, validClass) {
                $(element).removeClass('is-invalid');
            }
        })
        $('#modalEditmenu_tarik_saldo #formEditmenu_tarik_saldo').validate({
            rules: {
                id_menu_tarik_saldo_online: {
                    required: true
                },
                icon_menu_tarik_saldo_online: {
                    required: true
                },
                nama_menu_tarik_saldo_online: {
                    required: true
                }
            },
            messages: {
                id_menu_tarik_saldo_online: {
                    required: "Nama menu_tarik_saldo Tidak Boleh Kosong"
                },
               
                nama_menu_tarik_saldo_online: {
                    required: "Plat Kendaraan Tidak Boleh Kosong"
                }
            },
            errorElement: 'span',
            errorPlacement: function(error, element) {
                error.addClass('invalid-feedback');
                element.closest('.form-group').append(error);
            },
            highlight: function(element, errorClass, validClass) {
                $(element).addClass('is-invalid');
            },
            unhighlight: function(element, errorClass, validClass) {
                $(element).removeClass('is-invalid');
            }
        })


    })

    $('#modalCreatemenu_tarik_saldo').on('hidden.bs.modal', function() {
        $('#modalCreatemenu_tarik_saldo #id_menu_tarik_saldo_online').val('')
        $('#modalCreatemenu_tarik_saldo #id_menu_tarik_saldo_online').removeClass('is-invalid')
        $('#modalCreatemenu_tarik_saldo #icon_menu_tarik_saldo_online').val('')
        $('#modalCreatemenu_tarik_saldo #icon_menu_tarik_saldo_online').removeClass('is-invalid')
        $('#modalCreatemenu_tarik_saldo #nama_menu_tarik_saldo_online').val('')
        $('#modalCreatemenu_tarik_saldo #nama_menu_tarik_saldo_online').removeClass('is-invalid')
    })

    $('#modalEditmenu_tarik_saldo').on('hidden.bs.modal', function() {
        $('#modalEditmenu_tarik_saldo #id_menu_tarik_saldo_online').val('')
        $('#modalEditmenu_tarik_saldo #id_menu_tarik_saldo_online').removeClass('is-invalid')
        $('#modalEditmenu_tarik_saldo #edit_icon_menu_tarik_saldo_online').val('')
        $('#modalEditmenu_tarik_saldo #edit_icon_menu_tarik_saldo_online').removeClass('is-invalid')
        $('#modalEditmenu_tarik_saldo #nama_menu_tarik_saldo_online').val('')
        $('#modalEditmenu_tarik_saldo #nama_menu_tarik_saldo_online').removeClass('is-invalid')
    })

    function cekEditmenu_tarik_saldo(id) {
        startloading('#modalEditmenu_tarik_saldo .modal-content')
        $('#modalEditmenu_tarik_saldo #formEditmenu_tarik_saldo').attr('action', '<?= base_url('menu_tarik_saldo/update_action/') ?>' + id)
        $.ajax({
            url: '<?php echo base_url() ?>menu_tarik_saldo/ambilDataRequest/' + id,
            dataType: "json",
            cache: false,
            success: function(res) {
                $('#modalEditmenu_tarik_saldo #id_menu_tarik_saldo_online').val(res.id_menu_tarik_saldo_online)
                $('#modalEditmenu_tarik_saldo #edit_icon_menu_tarik_saldo_online').val(res.id_icon_menu_tarik_saldo_online).trigger('change')
                $('#modalEditmenu_tarik_saldo #ikon_menu').attr("src", res.icon_menu_tarik_saldo_online)
                $('#modalEditmenu_tarik_saldo #nama_menu_tarik_saldo_online').val(res.nama_menu_tarik_saldo_online)
                stoploading('#modalEditmenu_tarik_saldo .modal-content')
            }
        });
    }

    function cekDeletemenu_tarik_saldo(id) {
        swal({
                title: 'Yakin menghapus data?',
                text: "Data yang sudah terhapus tidak dapat dikembalikan!",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#CD5C5C',
                cancelButtonColor: '#d33',
                cancelButtonText: 'Batal',
                confirmButtonText: 'Hapus',
                closeOnConfirm: false,
            },
            function(isConfirm) {
                if (isConfirm) {
                    window.location.href = "<?php echo site_url('menu_tarik_saldo/delete/'); ?>" + id;
                } else {

                }
            });
    }
        function cek() {
                            var x = document.getElementById("edit_icon_menu_tarik_saldo_online");
                            if (x.style.display === "none") {
                                x.style.display = "block";
                            } else {
                                x.style.display = "none";
                                x.value="";
                            }
                        }
</script>