<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Menu_tarik_saldo extends MX_Controller
{
    function __construct()
    {
        parent::__construct();
        if (cek_token()) {
            $this->load->model('Menu_tarik_saldo_model');
            $this->load->library('pagination');
            $this->load->library('form_validation');
            $this->load->library('upload');
            $this->load->library('image_lib');
        } else {
            logout();
        }
    }

    public function index()
    {
        $q = urldecode($this->input->get('q', TRUE));
        $start = intval($this->input->get('start'));

        if ($q <> '') {
            $config['base_url']     = base_url() . 'menu_tarik_saldo?q=' . urlencode($q);
            $config['first_url']    = base_url() . 'menu_tarik_saldo?q=' . urlencode($q);
        } else {
            $config['base_url']     = base_url() . 'menu_tarik_saldo';
            $config['first_url']    = base_url() . 'menu_tarik_saldo';
        }
        $config['per_page']             = 10;
        $config['page_query_string']    = TRUE;
        $config['total_rows']           = $this->Menu_tarik_saldo_model->total_rows($q);
        $menu_tarik_saldo                = $this->Menu_tarik_saldo_model->get_limit_data($config['per_page'], $start, $q);


        $this->pagination->initialize($config);

        $data = array(
            'titleCard'                 => 'List Menu Tarik Saldo',
            'tittle'                     => 'Menu Tarik Saldo',
            'judul'                     => 'Menu Tarik Saldo',
            'breadcrumbactive'          => '',
            'breadcrumb'                => 'Menu Tarik Saldo',
            'cekaktif'                  => 'menu_tarik_sado',
            'menu_tarik_saldo_data'   => $menu_tarik_saldo,
            'q'                                 => $q,
            'pagination'                        => $this->pagination->create_links(),
            'total_rows'                        => $config['total_rows'],
            'start'                             => $start,
            'breadcrumb'                        => 'Jenis Sampah',
            'breadcrumbactive'                  => 'Dashboard',
            'link'                              => base_url('jenis_sampah_perbanksampah'),
            'menu_aktif'                          => 'menu_tarik_saldo',
            'action'                            => base_url('menu_tarik_saldo/createAction'),
        );
        $data['kategori']   = $this->Menu_tarik_saldo_model->get_kategori();
        $data['jenis']      = $this->Menu_tarik_saldo_model->get_jenis();
        $data['datakonten'] = $this->load->view('menu_tarik_saldo/menu_tarik_saldo_list', $data, TRUE);
        $this->load->view('layouts/main_view', $data);
    }

    public function createAction()
    {

        if (!file_exists('./assets/gambar/jenis_tariksaldo')) {
            mkdir('./assets/gambar/jenis_tariksaldo', 0777, true);
        }

        $config['upload_path']     = './assets/gambar/jenis_tariksaldo';   //path folder
        $config['allowed_types']   = 'gif|jpg|png|jpeg|bmp';                                      //type yang dapat diakses bisa anda sesuaikan
        $config['encrypt_name']    = TRUE;                                                        //Enkripsi nama yang terupload
        $this->upload->initialize($config);
        if (!empty($_FILES['icon_menu_tarik_saldo_online']['name'])) {
            if ($this->upload->do_upload('icon_menu_tarik_saldo_online', TRUE)) {
                $gbr = $this->upload->data();
                //Compress Image
                $config['image_library']   = 'gd2';
                $config['source_image']    = './assets/gambar/jenis_tariksaldo/' . $gbr['file_name'];
                $config['create_thumb']    = FALSE;
                $config['maintain_ratio']  = FALSE;
                $config['quality']         = '50%';
                $config['width']           = 600;
                $config['height']          = 400;
                $config['new_image']       = './assets/gambar/' . $gbr['file_name'];
                $this->load->library('image_lib', $config);
                $this->image_lib->resize();
                $upload = s3_upload_pengepul('jenis_tariksaldo/' . $gbr['file_name'], $gbr['full_path']);
                @unlink('assets/gambar/jenis_tariksaldo/' . $gbr['file_name']);
                $gambar1 = $upload['ObjectURL'];
            }
        } else {
            $gambar1 = 'kosong';
            // echo "Image Selfi yang diupload kosong ";
        }
        // $prefix = "REQ".getID();
        // $row    = $this->Menu_tarik_saldo_model->get_by_prefix($prefix);
        // $NoUrut = (int) substr($row->max, 18, 4);
        // $NoUrut = $NoUrut + 1; //nomor urut +1
        // $NoUrut = sprintf('%04d', $NoUrut);
        // $fix    = $prefix . $NoUrut;
        // $dt = new DateTime();
        $data = array(
            'nama_menu_tarik_saldo_online'            => $this->input->post('nama_menu_tarik_saldo_online', TRUE),
            'icon_menu_tarik_saldo_online'                => $gambar1,
            'id_menu_tarik_saldo_online'                => $this->input->post('id_menu_tarik_saldo_online', TRUE)
        );
        $data = $this->db->insert('menu_tarik_saldo_online', $data);
        $this->session->set_flashdata('berhasil', 'Data Menambahkan Data');
        redirect('menu_tarik_saldo');
    }

    public function ambilDataRequest($id)
    {
        $this->db->select('*');
        $this->db->where('id_menu_tarik_saldo_online', $id);
        $data = $this->db->get('menu_tarik_saldo_online')->row();
        echo json_encode($data);
    }

    public function update_action($id)
    {
         if (!file_exists('./assets/gambar/jenis_tariksaldo')) {
            mkdir('./assets/gambar/jenis_tariksaldo', 0777, true);
        }
         $this->db->where('id_menu_tarik_saldo_online', $id);
        $cekdata = $this->db->get('menu_tarik_saldo_online')->row();
     
        $config['upload_path']     = './assets/gambar/jenis_tariksaldo';   //path folder
        $config['allowed_types']   = 'gif|jpg|png|jpeg|bmp';                                      //type yang dapat diakses bisa anda sesuaikan
        $config['encrypt_name']    = TRUE;                                                        //Enkripsi nama yang terupload
        $this->upload->initialize($config);
        if (!empty($_FILES['edit_icon_menu_tarik_saldo_online']['name'])) {
            if ($this->upload->do_upload('edit_icon_menu_tarik_saldo_online', TRUE)) {
                $gbr = $this->upload->data();
                //Compress Image
                $config['image_library']   = 'gd2';
                $config['source_image']    = './assets/gambar/jenis_tariksaldo/' . $gbr['file_name'];
                $config['create_thumb']    = FALSE;
                $config['maintain_ratio']  = FALSE;
                $config['quality']         = '50%';
                $config['width']           = 600;
                $config['height']          = 400;
                $config['new_image']       = './assets/gambar/' . $gbr['file_name'];
                $this->load->library('image_lib', $config);
                $this->image_lib->resize();
                $upload = s3_upload_pengepul('jenis_tariksaldo/' . $gbr['file_name'], $gbr['full_path']);
                @unlink('assets/gambar/jenis_tariksaldo/' . $gbr['file_name']);
                $gambar1 = $upload['ObjectURL'];
            }
        } else {
            $gambar1 = $cekdata->icon_menu_tarik_saldo_online;
            // echo "Image Selfi yang diupload kosong ";
        }

        $dt = new DateTime();
        $data = array(
            'id_menu_tarik_saldo_online'            => $this->input->post('id_menu_tarik_saldo_online', TRUE),
            'nama_menu_tarik_saldo_online'            => $this->input->post('nama_menu_tarik_saldo_online', TRUE),
            'icon_menu_tarik_saldo_online'            => $gambar1,
        );

        $data = $this->db->where('id_menu_tarik_saldo_online', $id)->update('menu_tarik_saldo_online', $data);
        if($data){
        $this->session->set_flashdata('berhasil', 'Berhasil Merubah Data');
        redirect('menu_tarik_saldo');
        }else{
        $this->session->set_flashdata('gagal', 'Gagal Merubah Data');
        redirect('menu_tarik_saldo');
        }
    }

    public function exportxl()
    {
        $q          = $this->input->get('q');
        $subject    = "menu_tarik_saldo";
        $id         = getID();
        $this->db->query('SET @no=0');
        $sql        = "SELECT @no:=@no+1 AS nomor, nama_menu_tarik_saldo_online, nama_jenis_sampah, harga_jual, harga_beli
        FROM `menu_tarik_saldo`
        JOIN `kategori_sampah` ON `menu_tarik_saldo`.`id_kategori_sampah` = `kategori_sampah`.`id_kategori_sampah`
        JOIN `jenis_sampah` ON `jenis_sampah`.`id_jenis_sampah` = `menu_tarik_saldo`.`id_jenis_sampah`
        WHERE `menu_tarik_saldo`.`status_jenis_sampah_remove` = 1
        AND `id_bank_sampah` = '$id'
        AND   (
        `kategori_sampah`.`nama_menu_tarik_saldo_online` LIKE '%$q%' ESCAPE '!'
        OR  `jenis_sampah`.`nama_jenis_sampah` LIKE '%$q%' ESCAPE '!'
        OR  `menu_tarik_saldo`.`harga_beli` LIKE '%$q%' ESCAPE '!'
        OR  `menu_tarik_saldo`.`harga_jual` LIKE '%$q%' ESCAPE '!'
         )
        ORDER BY `id_menu_tarik_saldo` ASC";
        exportSQL('menu_tarik_saldo', $subject, $sql);
    }
}

/* End of file Jenis_sampah_perbanksampah.php */
/* Location: ./application/controllers/Jenis_sampah_perbanksampah.php */
/* Please DO NOT modify this information : */
/* Generated by Harviacode Codeigniter CRUD Generator 2020-03-02 14:16:13 */
/* http://harviacode.com */
