<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Driver_bank_sampah extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        if (cek_token()) {
        $this->load->model('Driver_bank_sampah_model');
        $this->load->library('form_validation');
    } else {
            logout();
        }
    }

    public function index(){
        $q = urldecode($this->input->get('q', TRUE));
        $start = intval($this->input->get('start'));
        
        if ($q <> '') {
            $config['base_url']     = base_url() . 'driver_bank_sampah?q=' . urlencode($q);
            $config['first_url']    = base_url() . 'driver_bank_sampah?q=' . urlencode($q);
        } else {
            $config['base_url']     = base_url() . 'driver_bank_sampah';
            $config['first_url']    = base_url() . 'driver_bank_sampah';
        }

        $config['per_page']             = 10;
        $config['page_query_string']    = TRUE;
        $config['total_rows']           = $this->Driver_bank_sampah_model->total_rows($q);
        $driver_bank_sampah             = $this->Driver_bank_sampah_model->get_limit_data($config['per_page'], $start, $q);

        $this->load->library('pagination');
        $this->pagination->initialize($config);

        $data = array(
            'titleCard'                 => 'List Driver Bank Sampah',
            'tittle'                    => 'Driver Bank Sampah',
            'judul'                     => 'Driver Bank Sampah',
            'breadcrumbactive'          => '',
            'breadcrumb'                => 'Driver Bank Sampah',
            'link'                      => '',
            'driver_bank_sampah_data'   => $driver_bank_sampah,
            'q'                         => $q,
            'pagination'                => $this->pagination->create_links(),
            'total_rows'                => $config['total_rows'],
            'start'                     => $start,
            'action'                    => site_url('driver_bank_sampah/create_action'),
            'kategori_aktif'            => '1',
            'menu_aktif'                => 'driver_bank_sampah'
        );
        $data['datakonten'] = $this->load->view('Driver_bank_sampah_list', $data, true);
        $this->load->view('layouts/main_view', $data);
    }

    public function driver($id){
        $this->db->where(array('md5(id_bank_sampah) =' => $id));
        $data = $this->db->get('bank_sampah')->row();
        if($data){
            $q      = urldecode($this->input->get('q', TRUE));
            $start  = intval($this->input->get('start'));
            
            if ($q <> '') {
                $config['base_url'] = base_url() . 'driver_bank_sampah/driver/'. $id .'?q=' . urlencode($q);
                $config['first_url'] = base_url() . 'driver_bank_sampah/driver/'. $id .'?q=' . urlencode($q);
            } else {
                $config['base_url'] = base_url() . 'driver_bank_sampah/driver/'. $id .'';
                $config['first_url'] = base_url() . 'driver_bank_sampah/driver/'. $id .'';
            }

            $config['per_page']             = 10;
            $config['page_query_string']    = TRUE;
            $config['total_rows']           = $this->Driver_bank_sampah_model->total_rows_driver($id, $q);
            $driver_bank_sampah             = $this->Driver_bank_sampah_model->get_limit_data_driver($id, $config['per_page'], $start, $q);

            $this->load->library('pagination');
            $this->pagination->initialize($config);

            $data = array(
                'titleCard'                 => 'List Driver Bank Sampah',
                'tittle'                    => 'Driver Bank Sampah',
                'judul'                     => 'Driver Bank Sampah',
                'breadcrumbactive'          => '',
                'breadcrumb'                => 'Driver Bank Sampah',
                'link'                      => '',
                'driver_bank_sampah_data'   => $driver_bank_sampah,
                'q'                         => $q,
                'pagination'                => $this->pagination->create_links(),
                'total_rows'                => $config['total_rows'],
                'start'                     => $start,
                'action'                    => site_url('driver_bank_sampah/create_action'),
                'kategori_aktif'            => '1',
                'menu_aktif'                => 'driver_bank_sampah',
                'id_bank_sampah'            => $id,

            );
            $data['datakonten'] = $this->load->view('Driver_bank_sampah', $data, true);
            $this->load->view('layouts/main_view', $data);
        } else {
            $this->session->set_flashdata('gagal', 'Data tidak ditemukan');
            redirect('gudang_bank_sampah');
        }
    }

}

/* End of file versi_app.php */
/* Location: ./application/controllers/versi_app.php */
/* Please DO NOT modify this information : */
/* Generated by Harviacode Codeigniter CRUD Generator 2020-07-15 09:43:37 */
/* http://harviacode.com */