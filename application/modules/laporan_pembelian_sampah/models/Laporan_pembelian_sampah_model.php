<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Laporan_pembelian_sampah_model extends CI_Model
{

    public $vtable = 'v_pembelian_pengepul';
    public $table = 'pembelian_pengepul';
    public $id = 'id_pembelian_pengepul';
    public $vtable_nasabah = 'v_tarik_saldo_nasabah';
    public $vtable_bank = 'v_tarik_saldo_banksampah';
    public $table_peng = 'biller_pengepul';
    public $table_nas = 'biller_nasabah';
    public $table_bs = 'biller_banksampah';
     public $id_order = 'pembelian_pengepul.id_pembelian_pengepul';

    public $order = 'DESC';

    function __construct()
    {
        parent::__construct();
    }

    // get all
    function get_all()
    {
        $this->db->order_by($this->id, $this->order);
        return $this->db->get($this->table)->result();
    }
public function get_by_prefix($prefix = '')
    {
        $query = $this->db->query("SELECT max(".$this->id.") AS max FROM ".$this->table." WHERE ".$this->id." LIKE '$prefix%'");
        return $query->row();
    }
    // get data by id
    function get_by_id($id)
    {
       $this->db->join('pembelian_pengepul_detail', 'pembelian_pengepul_detail.id_pembelian_pengepul = pembelian_pengepul.id_pembelian_pengepul','left');
        $this->db->join('pengepul', 'pengepul.id_pengepul = pembelian_pengepul.id_pengepul');
        $this->db->join('bank_sampah', 'bank_sampah.id_bank_sampah = pembelian_pengepul_detail.id_bank_sampah','left');
        $this->db->join('status_pembelian_pengepul', 'status_pembelian_pengepul.id_status_pembelian_pengepul = pembelian_pengepul.id_status_pembelian_pengepul','left');
        $this->db->join('penjualan_bank_sampah', 'penjualan_bank_sampah.id_penjualan_bank_sampah = pembelian_pengepul_detail.id_penjualan_bank_sampah','left');
        $this->db->join('reason', 'reason.id_reason = pembelian_pengepul.id_reason','left');

        $this->db->where($this->id_order, $id);
        return $this->db->get($this->table)->row();
    }
    
    // get total rows
    function total_rows_bank_sampah($q = NULL,$awal,$akhir) {
           $this->db->where("(UNIX_TIMESTAMP(". $this->vtable_peng .".datetime) BETWEEN UNIX_TIMESTAMP('" . $awal . "') AND UNIX_TIMESTAMP('" . $akhir  . " 23:59') ) ");
           $this->db->group_start();
        $this->db->or_like('nama', $q);
        $this->db->group_end();
        $this->db->from($this->vtable_bank);
        return $this->db->count_all_results();
    }

    // get data with limit and search
    function get_limit_data_bank_sampah($limit, $start = 0, $q = NULL,$awal,$akhir) {
        $this->db->order_by('datetime', $this->order);
          $this->db->where("(UNIX_TIMESTAMP(". $this->vtable_peng .".datetime) BETWEEN UNIX_TIMESTAMP('" . $awal . "') AND UNIX_TIMESTAMP('" . $akhir  . " 23:59') ) ");
        $this->db->group_start();
        $this->db->or_like('nama', $q);
        $this->db->group_end();
        $this->db->limit($limit, $start);
        return $this->db->get($this->vtable_bank)->result();
    }
        // get total rows
    function total_rows_nasabah($q = NULL,$awal,$akhir) {
          $this->db->where("(UNIX_TIMESTAMP(". $this->vtable_nasabah .".datetime) BETWEEN UNIX_TIMESTAMP('" . $awal . "') AND UNIX_TIMESTAMP('" . $akhir  . " 23:59') ) ");
           $this->db->group_start();
        $this->db->or_like('nama', $q);
        $this->db->group_end();
        $this->db->from($this->vtable_nasabah);
        return $this->db->count_all_results();
    }

    // get data with limit and search
    function get_limit_data_nasabah($limit, $start = 0, $q = NULL,$awal,$akhir) {
        $this->db->order_by('datetime', $this->order);
          $this->db->where("(UNIX_TIMESTAMP(". $this->vtable_nasabah .".datetime) BETWEEN UNIX_TIMESTAMP('" . $awal . "') AND UNIX_TIMESTAMP('" . $akhir  . " 23:59') ) ");
        $this->db->group_start();
        $this->db->or_like('nama', $q);
        $this->db->group_end();
        $this->db->limit($limit, $start);
        return $this->db->get($this->vtable_nasabah)->result();
    } 
     // get total rows
    function total_rows($q = NULL,$awal,$akhir) {
        $this->db->join('pengepul','pengepul.id_pengepul='. $this->table .'.id_pengepul');
        $this->db->where("(UNIX_TIMESTAMP(". $this->table .".tgl_pembelian_sampah) BETWEEN UNIX_TIMESTAMP('" . $awal . "') AND UNIX_TIMESTAMP('" . $akhir  . " 23:59') ) ");
        $this->db->group_start();
        $this->db->or_like('nama_pengepul', $q);
        $this->db->group_end();
        // $this->db->group_by('id_pembelian_pengepul');
        $this->db->from($this->table);
        return $this->db->count_all_results();
    } 
    function total_rows_detail($id,$q = NULL) {
        $this->db->join('pengepul','pengepul.id_pengepul='. $this->vtable .'.id_pengepul');
       
        $this->db->where('id_pembelian_pengepul',$id);
        $this->db->group_start();
        $this->db->or_like('pengepul.nama_pengepul', $q);
        $this->db->group_end();
           $this->db->from($this->vtable);
        return $this->db->count_all_results();
    }

    // get data with limit and search
    function get_limit_data_detail($id,$limit, $start = 0, $q = NULL) {
        $this->db->join('pengepul','pengepul.id_pengepul='. $this->vtable .'.id_pengepul');
        $this->db->order_by('tgl_pembelian_sampah', $this->order);
          $this->db->where('id_pembelian_pengepul',$id);
        $this->db->group_start();
        $this->db->or_like('pengepul.nama_pengepul', $q);
        $this->db->group_end();
        // $this->db->group_by('id_pembelian_pengepul');
        $this->db->limit($limit, $start);
        return $this->db->get($this->vtable)->result();
    } // get data with limit and search
    function get_limit_data($limit, $start = 0, $q = NULL,$awal,$akhir) {
        $this->db->join('pengepul','pengepul.id_pengepul='. $this->table .'.id_pengepul');
        $this->db->where("(UNIX_TIMESTAMP(". $this->table .".tgl_pembelian_sampah) BETWEEN UNIX_TIMESTAMP('" . $awal . "') AND UNIX_TIMESTAMP('" . $akhir  . " 23:59') ) ");
        $this->db->order_by('tgl_pembelian_sampah', $this->order);
        $this->db->group_start();
        $this->db->or_like('pengepul.nama_pengepul', $q);
        $this->db->group_end();
        // $this->db->group_by('id_pembelian_pengepul');
        $this->db->limit($limit, $start);
        return $this->db->get($this->table)->result();
    }

    // insert data
    function insert($data)
    {
        $this->db->insert($this->table, $data);
    }

    // update data
    function update($id, $data)
    {
        $this->db->where($this->id, $id);
        $this->db->update($this->table, $data);
    }

    // delete data
    function delete($id)
    {
        $this->db->where($this->id, $id);
        $this->db->delete($this->table);
    }

}

/* End of file Kategori_produk_model.php */
/* Location: ./application/models/Kategori_produk_model.php */
/* Please DO NOT modify this information : */
/* Generated by Harviacode Codeigniter CRUD Generator 2020-07-15 09:43:37 */
/* http://harviacode.com */