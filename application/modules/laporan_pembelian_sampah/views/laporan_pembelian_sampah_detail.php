
<div class="row">

    <section class="col-lg connectedSortable ui-sortable">
        <div class="card card-info">
            <div class="card-header">
                <div class="card-title">
                  <?php echo $titleCard?>
                </div>
              </div>

            <div class="card-body">
                <table class="table table-bordered table-striped table-condensed table-sm" style="margin-bottom: 20px">
                    <tr><td class="font-weight-bold pl-3">Pengepul</td><td><?php echo $nama_pengepul; ?></td></tr>
                    <tr><td class="font-weight-bold pl-3">Bank Sampah</td><td><?php echo $nama_bank_sampah; ?></td></tr>
                    <tr><td class="font-weight-bold pl-3">Status Pembelian Pengepul</td><td><?php echo $nama_status_pembelian_pengepul; ?></td></tr>
                    <tr><td class="font-weight-bold pl-3">Nama Sampah</td><td><?php echo $nama_sampah; ?></td></tr>
                    <tr><td class="font-weight-bold pl-3">Tanggal Pembelian Sampah</td><td><?php echo fulldate($tgl_pembelian_sampah); ?></td></tr>
                    <tr><td class="font-weight-bold pl-3">Update Tanggal</td><td><?php echo fulldate($update_at); ?></td></tr>
                    <tr><td class="font-weight-bold pl-3">Nilai Pengepul</td><td><?php echo $nilai_pengepul; ?></td></tr>
                    <tr><td class="font-weight-bold pl-3">Ulasan Pengepul</td><td><?php echo $ulasan_pengepul; ?></td></tr>
                    <tr><td class="font-weight-bold pl-3">Tanggal Review</td><td><?php echo fulldate($tgl_review); ?></td></tr>
                    <tr><td class="font-weight-bold pl-3">Reason</td><td><span class="badge badge-danger"><?php echo $desc_reason; ?></span></td></tr>
                </table>
                <div>
                    <a href="<?php echo site_url('laporan_pembelian_sampah') ?>" class="btn btn-primary btn-sm mb-2">Kembali</a>
                </div>
                
                <table class="table table-bordered table-striped table-condensed">
                    <tr>
                        <th class="text-center" width="20px">No</th>
                        <th class="text-center" width="100px">Nama Sampah</th>
                        <th class="text-center">Harga Beli</th>
                        <th class="text-center">Jumlah Dibeli</th>
                        <th class="text-center">Jumlah Nego</th>
                        <th class="text-center">Catatan Pembeli</th>
                        <th class="text-center">Total Harga</th>
                    </tr>
                    <?php foreach ($laporan_pembelian_sampah_data as $data) { ?>
                        <tr>
                            <td class="text-center"><?php echo ++$start ?></td>
                            <td><?php echo $data->nama_sampah ?></td>
                            <td class="text-center" width="150px"><?php echo rupiah($data->harga_beli) ?></td>
                            <td class="text-center" width="150px"><?php echo $data->stok_dibeli ?></td>
                            <td class="text-center" width="150px"><?php  if($data->count_nego==0) echo 0; ?></td>
                            <td class="text-center" width="150px"><?php if($data->catatan_pembeli=="") echo "Tidak Ada Catatan"; ?></td> 
                            <td class="text-center" width="150px"><?php echo rupiah($data->total_harga) ?></td>
                        </tr>
                    <?php } ?>
                </table>
                <?= footer($total_rows, $pagination, '') ?>
            </div>
        </div>
</div>

</section>
</div>