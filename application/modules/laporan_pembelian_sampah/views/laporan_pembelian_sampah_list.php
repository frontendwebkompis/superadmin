<div class="card card-primary">
    <div class="card-header">
        <div class="card-title">
            <?= $titleCard ?>
        </div>
    </div>
    <div class="card-body">
            <div class="row">
                <div class="col-md-5 mb-2">
                    <?= filterDate($this->input->get('awal'), $this->input->get('akhir'), base_url('laporan_pembelian_sampah')) ?>
                </div>
                <div class="col-md-3 offset-md-4 mb-2">
                    <?php if($tombol_aktif=='bank_sampah'){ 
                        echo search(site_url('laporan_pembelian_sampah/bank_sampah'),site_url('laporan_pembelian_sampah/bank_sampah'), $q);
                    } else if($tombol_aktif=='nasabah'){ 
                        echo search(site_url('laporan_pembelian_sampah/nasabah'),site_url('laporan_pembelian_sampah/nasabah'), $q);
                    } else { 
                        echo search(site_url('laporan_pembelian_sampah/index'),site_url('laporan_pembelian_sampah'), $q);
                    } ?>
                </div>
            </div>
           
            <div class="tab-content p-0" style="overflow:auto">
                <table class="table table-bordered table-striped table-condensed table-hover table-sm" style="margin-bottom: 10px">
                    <tr>
                        <th class='text-center' style="width: 60px;">No</th>
                        <th class='text-center'>Tanggal Pembelian Sampah </th>
                        <th class='text-center'>Nama Pengepul</th>
                        <th class='text-center'>Jenis Pengiriman </th>
                        <th class='text-center'>Biaya Ongkir </th>
                        <th class='text-center'>Jumlah Item </th>
                        <th class='text-center'>Tanggal Pengiriman </th>
                        <th class='text-center'>Tanggal Terima </th>
                        <th class='text-center' style="width: 150px;">Aksi</th>
                    </tr><?php
                    if ($total_rows == 0) {
                        echo '<tr><td colspan="11" bgcolor="grey" style="color: white; font-weight: bold; text-align: center;">Data Tidak Ditemukan</td></tr>';
                    } else {
                        foreach ($laporan_pembelian_sampah_data as $laporan_pembelian_sampah)
                        {
                            ?>
                    <tr>
                        <td class='text-center'><?php echo ++$start ?></td>
                        <td><?php echo fulldate($laporan_pembelian_sampah->tgl_pembelian_sampah) ?></td>
                        <td><?php echo $laporan_pembelian_sampah->nama_pengepul ?></td>
                        <td><?php if($laporan_pembelian_sampah->jenis_pengiriman==1){
                            echo "COD";
                        }else{
                             echo "Jasa Pengiriman";
                        } ?></td>
                         <td><?php echo rupiah($laporan_pembelian_sampah->biaya_ongkir) ?></td>
                          <td><?php echo $laporan_pembelian_sampah->nama_pengepul ?></td>
                            <td><?php echo fulldate($laporan_pembelian_sampah->tgl_pengiriman) ?></td>  
                            <td><?php echo fulldate($laporan_pembelian_sampah->tgl_terima) ?></td>
                        <td class='text-center'><div class="btn-group"><a href="<?php echo site_url('laporan_pembelian_sampah/detail/'.$laporan_pembelian_sampah->id_pembelian_pengepul); ?>"
                                    data-toogle="tooltip" title="Lihat"><button type="button" class="btn btn-info btn-sm"><i class="fas fa-info-circle"></i></button></a>
                            </div>
                                </td>
                    </tr>
                            <?php
                        }
                    }
                        ?>
                </table>
            </div>
            <?= footer($total_rows, $pagination, '') ?>
        </div>
    </div>
</div>

<div id="modalCreatelaporan_pembelian_sampah" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Form Tambah laporan_pembelian_sampah</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form action="<?= $action ?>" method="post" id="formTambahlaporan_pembelian_sampah" role="form" enctype="multipart/form-data">
                <div class="row ml-3 mr-3">
                    <div class="col-md-12">
                       
                        <div class="form-group">
                            <label for="varchar">Nama Jenis Kendaraan</label>
                            <input type="text" class="form-control" name="nama_laporan_pembelian_sampah" id="nama_laporan_pembelian_sampah" placeholder="Nama Jenis Kendaraan" value="" />
                        </div>
                        
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <div class="col-md-12 text-center">
                    <button type="submit" class="btn btn-<?= bgCard() ?>" style="width: 100px;">Simpan</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal" style="width: 100px;">Batal</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="modalEditlaporan_pembelian_sampah" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Form Edit laporan_pembelian_sampah</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form action="<?= $action ?>" method="post" id="formEditlaporan_pembelian_sampah" role="form" enctype="multipart/form-data">
                <div class="row ml-3 mr-3">
                    <div class="col-md-12">
                           <div class="form-group">
                            <label for="varchar">laporan_pembelian_sampah</label>
                            <input type="text" class="form-control" name="nama_laporan_pembelian_sampah" id="nama_laporan_pembelian_sampah" placeholder="laporan_pembelian_sampah" value="" />
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <div class="col-md-12 text-center">
                    <button type="submit" class="btn btn-<?= bgCard() ?>" style="width: 100px;">Simpan</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal" style="width: 100px;">Batal</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    $(function(){
        setDateFilter('akhir','awal')

        $('#modalCreatelaporan_pembelian_sampah #formTambahlaporan_pembelian_sampah').validate({
            rules: {
                nama_laporan_pembelian_sampah : {
                    required : true
                },
             
            },
            messages: {
                nama_laporan_pembelian_sampah : {
                    required : "Nama laporan_pembelian_sampah Tidak Boleh Kosong"
                },
                
            },
            errorElement: 'span',
            errorPlacement: function(error, element) {
                error.addClass('invalid-feedback');
                element.closest('.form-group').append(error);
            },
            highlight: function(element, errorClass, validClass) {
                $(element).addClass('is-invalid');
            },
            unhighlight: function(element, errorClass, validClass) {
                $(element).removeClass('is-invalid');
            }
        })
        $('#modalEditlaporan_pembelian_sampah #formEditlaporan_pembelian_sampah').validate({
            rules: {
                nama_laporan_pembelian_sampah : {
                    required : true
                },
             
            },
            messages: {
                nama_laporan_pembelian_sampah : {
                    required : "Nama laporan_pembelian_sampah Tidak Boleh Kosong"
                },
              
            },
            errorElement: 'span',
            errorPlacement: function(error, element) {
                error.addClass('invalid-feedback');
                element.closest('.form-group').append(error);
            },
            highlight: function(element, errorClass, validClass) {
                $(element).addClass('is-invalid');
            },
            unhighlight: function(element, errorClass, validClass) {
                $(element).removeClass('is-invalid');
            }
        })

        $('#modalCreatelaporan_pembelian_sampah #keterangan_laporan_pembelian_sampah').select2({
            theme: 'bootstrap'
        })

        $('#modalEditlaporan_pembelian_sampah #edit_keterangan_laporan_pembelian_sampah').select2({
            theme: 'bootstrap'
        })
    })

    $('#modalCreatelaporan_pembelian_sampah').on('hidden.bs.modal', function(){
        $('#modalCreatelaporan_pembelian_sampah #is_read_aplikasi').val('')
        $('#modalCreatelaporan_pembelian_sampah #is_read_aplikasi').removeClass('is-invalid')
        $('#modalCreatelaporan_pembelian_sampah #keterangan_laporan_pembelian_sampah').val('').trigger('change')
        $('#modalCreatelaporan_pembelian_sampah #keterangan_laporan_pembelian_sampah').removeClass('is-invalid')
        $('#modalCreatelaporan_pembelian_sampah #nama_laporan_pembelian_sampah').val('')
        $('#modalCreatelaporan_pembelian_sampah #nama_laporan_pembelian_sampah').removeClass('is-invalid')
    })

    $('#modalEditlaporan_pembelian_sampah').on('hidden.bs.modal', function(){
        $('#modalEditlaporan_pembelian_sampah #is_read_aplikasi').val('')
        $('#modalEditlaporan_pembelian_sampah #is_read_aplikasi').removeClass('is-invalid')
        $('#modalEditlaporan_pembelian_sampah #edit_keterangan_laporan_pembelian_sampah').val('').trigger('change')
        $('#modalEditlaporan_pembelian_sampah #edit_keterangan_laporan_pembelian_sampah').removeClass('is-invalid')
        $('#modalEditlaporan_pembelian_sampah #laporan_pembelian_sampah').val('')
        $('#modalEditlaporan_pembelian_sampah #laporan_pembelian_sampah').removeClass('is-invalid')
    })

    function cekEditlaporan_pembelian_sampah(id){
        startloading('#modalEditlaporan_pembelian_sampah .modal-content')
        $('#modalEditlaporan_pembelian_sampah #formEditlaporan_pembelian_sampah').attr('action', '<?= base_url('laporan_pembelian_sampah/update_action/') ?>'+id)
        $.ajax({
            url: '<?php echo base_url() ?>laporan_pembelian_sampah/ambilDatalaporan_pembelian_sampah/' + id,
            dataType: "json",
            cache: false,
            success: function(res) {
                $('#modalEditlaporan_pembelian_sampah #nama_laporan_pembelian_sampah').val(res.nama_laporan_pembelian_sampah)
               
                stoploading('#modalEditlaporan_pembelian_sampah .modal-content')
            }
        });
    }

  
    function cekDeletelaporan_pembelian_sampah(id){
        swal({
                title: 'Yakin menghapus data?',
                text: "Data yang sudah terhapus tidak dapat dikembalikan!",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#CD5C5C',
                cancelButtonColor: '#d33',
                cancelButtonText: 'Batal',
                confirmButtonText: 'Hapus',
                closeOnConfirm: false,
            },
            function(isConfirm) {
                if (isConfirm) {
                    window.location.href = "<?php echo site_url('laporan_pembelian_sampah/delete/'); ?>" + id;
                } else {

                }
            });
    }
</script>