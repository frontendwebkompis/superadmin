<div class="row">
	<!-- Left col -->
	<section class="col-lg connectedSortable ui-sortable">
		<!-- Custom tabs (Charts with tabs)-->
		<div class="card">
			<div class="card-body">
				<div class="tab-content p-0" style="overflow:auto">

        <div class="row" style="margin-bottom: 10px">
            <div class="col-md-4">
                <?php echo anchor(site_url('profile/create'),'Tambah Data', 'class="btn btn-primary"'); ?>
            </div>
            <div class="col-md-4 text-center">
                <div style="margin-top: 8px" id="message">
                    <?php echo $this->session->userdata('message') <> '' ? $this->session->userdata('message') : ''; ?>
                </div>
            </div>
            <div class="col-md-1 text-right">
            </div>
            <div class="col-md-3 text-right">
                <form action="<?php echo site_url('profile/index'); ?>" class="form-inline" method="get">
                    <div class="input-group">
                        <input type="text" class="form-control" name="q" value="<?php echo $q; ?>">
                        <span class="input-group-btn">
                            <?php if ($q <> '') { ?>
                                <a href="<?php echo site_url('profile'); ?>" class="btn btn-default">Reset</a>
                                <?php } ?>
                          <button class="btn btn-primary" type="submit">Pencarian</button>
                        </span>
                    </div>
                </form>
            </div>
        </div>
        <table class="table table-bordered table-striped table-condensed"s style="margin-bottom: 10px">
            <tr>
                <th>No</th>
                <th>Username</th>
                <!-- <th>Password</th> -->
                <th>Email</th>
                <th>Nama Admin</th>
                <th>Last Login</th>
                <th>Dibuat Tgl</th>
                <th style="text-align:center">Aksi</th>
            </tr>
            <?php foreach ($profile_data as $profile) { ?>
            <tr>
                <td width="80px"><?php echo ++$start ?></td>
                <td><?php echo $profile->username ?></td>
                <!-- <td><?php echo $profile->password ?></td> -->
                <td><?php echo $profile->email_superadmin ?></td>
                <td><?php echo $profile->nama_admin ?></td>
                <td><?php echo $profile->last_login ?></td>
                <td><?php echo $profile->dibuat_tgl ?></td>
                <td style="text-align:center" width="200px">
                    <?php 
                    echo anchor(site_url('profile/detail/'.$profile->id_superadmin),'Lihat'); 
                    echo ' | '; 
                    echo anchor(site_url('profile/update/'.$profile->id_superadmin),'Update'); 
                    echo ' | '; 
                    echo anchor(site_url('profile/delete/'.$profile->id_superadmin),'Hapus','onclick="javasciprt: return confirm(\'Yakin Anda ingin Menghapus ini ?\')"'); 
                    ?>
                </td>
		    </tr>
            <?php } ?>
        </table>
        <?= footer($total_rows, $pagination, '') ?>
        </div>
			</div>
		</div>
	</section>
</div>