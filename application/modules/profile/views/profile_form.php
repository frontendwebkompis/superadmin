<div class="row">
	<!-- Left col -->
	<section class="col-lg connectedSortable ui-sortable">
		<!-- Custom tabs (Charts with tabs)-->
		<div class="card">

			<div class="card-body">
				<div class="tab-content p-0">

        <form action="<?php echo $action; ?>" method="post">
            <!-- <div class="form-group">
                <label for="varchar">Username <?php echo form_error('username') ?></label>
                <input type="text" readonly class="form-control" name="username" id="username" placeholder="Username" value="<?php echo $username; ?>" />
            </div>
            <div class="form-group">
                <label for="varchar">email_superadmin <?php echo form_error('email_superadmin') ?></label>
                <input type="text" readonly class="form-control" name="email_superadmin" id="email_superadmin" placeholder="email_superadmin" value="<?php echo $email_superadmin; ?>" />
            </div> -->
            <div class="form-group">
                <label for="varchar">Password Lama <?php echo form_error('password') ?></label>
                <input type="password" class="form-control" name="passwordlama" id="passwordlama" placeholder="Password"  />
            </div>
            <div class="form-group">
                <label for="varchar">Password Baru <?php echo form_error('password') ?></label>
                <input type="password" class="form-control" name="passwordbaru" id="passwordbaru" placeholder="Password"  />
            </div>
            <div class="form-group">
                <label for="varchar">Confirm Password <?php echo form_error('password') ?></label>
                <input type="password" onkeyup="CekKonfirmPass()" class="form-control" name="password" id="password" placeholder="Password"  />
                <small class="info help-block"> 
                    <span id="oke" style="color:green;display:none">Oke Pasword Sama</span>
                    <span id="tdkoke" style="color:red;display:none">Pasword Tidak Sama</span>
                </small>
            </div>
            <!-- <div class="form-group">
                <label for="varchar">Nama Admin <?php echo form_error('nama_admin') ?></label>
                <input type="text" class="form-control" name="nama_admin" id="nama_admin" placeholder="Nama Admin" value="<?php echo $nama_admin; ?>" />
            </div> -->
            <!-- <div class="form-group">
                <label for="datetime">Last Login <?php echo form_error('last_login') ?></label>
                <input type="text" class="form-control" name="last_login" id="last_login" placeholder="Last Login" value="<?php echo $last_login; ?>" />
            </div>
            <div class="form-group">
                <label for="datetime">Dibuat Tgl <?php echo form_error('dibuat_tgl') ?></label>
                <input type="text" class="form-control" name="dibuat_tgl" id="dibuat_tgl" placeholder="Dibuat Tgl" value="<?php echo $dibuat_tgl; ?>" />
            </div> -->
            <div style="margin-top: 8px" id="message">
                <?php echo $this->session->userdata('message') <> '' ? $this->session->userdata('message') : ''; ?>
            </div>
            <button type="submit" style="display:none" id="bt-simpan" class="btn btn-primary">Simpan</button> 
            <a href="<?php echo site_url('profile/detail') ?>" class="btn btn-default">Batal</a>
	    </form>
    <script>
        function CekKonfirmPass() {
            var passbaru = document.getElementById("passwordbaru").value;  
            var pass = document.getElementById("password").value;  
            if(pass==passbaru){
            document.getElementById("bt-simpan").style.display = "inline-block";
            document.getElementById("oke").style.display = "block";
            document.getElementById("tdkoke").style.display = "none";
            }else{
                document.getElementById("bt-simpan").style.display = "none";
            document.getElementById("oke").style.display = "none";
            document.getElementById("tdkoke").style.display = "block";
            }
        }
    </script>
    
			</div><!-- /.card-body -->
		</div>
		<!-- /.card -->
	</section>
	<!-- /.Left col -->
</div>