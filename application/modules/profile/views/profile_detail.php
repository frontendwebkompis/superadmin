<div class="row">
	<section class="col-lg connectedSortable ui-sortable">
		<div class="card card-info">
			<div class="card-header">
				<div class="card-title">
					<?= $title_card ?>
				</div>
			</div>
			<div class="card-body">
				<div class="row mx-3">
					<div class="col-sm-12">
						<div class="tab-content p-0">
							<table class="table table-sm" border="0">
								<tr>
									<td>Username</td>
									<td><?php echo $username; ?><button class="float-right btn btn-sm btn-success" data-toggle="modal" data-target="#modalEditUsername" title="Edit Username"><i class="fas fa-edit"></i></button></td>
								</tr>
								<tr>
									<td>Email</td>
									<td><?php echo $email_superadmin; ?></td>
								</tr>
								<tr>
									<td>Nama Admin</td>
									<td><?php echo $nama_superadmin; ?></td>
								</tr>

								<tr>
									<td>Last Login</td>
									<td><?php $this->load->helper('date');
										echo timespan(strtotime($this->session->userdata('lastlog')), strtotime(date('Y-m-d H:i:s')), 4) . ' yang lalu'; ?></td>
								</tr>
								<tr>
									<td>Dibuat Tanggal</td>
									<td><?php echo fulldate($created_at); ?></td>
								</tr>
							</table>
							<a href="<?php echo site_url('profile/update') ?>" class="btn btn-primary btn-sm"><i class="fas fa-key"></i> Edit Password</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
</div>

<div id="modalEditUsername" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabel">Form Edit Username</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<form action="<?= base_url('profile/EditUsername') ?>" method="post" id="formEditUsername" role="form" enctype="multipart/form-data">
					<div class="row ml-3 mr-3">
						<div class="col-md-12">
							<div class="form-group">
								<label for="username">Username</label>
								<input type="text" id="username" name="username" class="form-control" placeholder="Masukkan username" required>
							</div>
						</div>
					</div>
			</div>
			<div class="modal-footer">
				<div class="col-md-12 text-center">
					<button type="submit" class="btn btn-<?= bgCard() ?>" style="width: 100px;">Simpan</button>
					<button type="button" class="btn btn-secondary" data-dismiss="modal" style="width: 100px;">Batal</button>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>

<script>
	$(function(){
		$('#modalEditUsername #formEditUsername').validate({
			rules: {
				username: {
					required: true
				}
			},
			messages: {
				username: {
					required: "Username tidak boleh kosong"
				}
			},
			errorElement: 'span',
			errorPlacement: function(error, element) {
				error.addClass('invalid-feedback');
				element.closest('.form-group').append(error);
			},
			highlight: function(element, errorClass, validClass) {
				$(element).addClass('is-invalid');
			},
			unhighlight: function(element, errorClass, validClass) {
				$(element).removeClass('is-invalid');
			}
		})
	})
</script>