<?php

if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class Profile extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
		if (cek_token()) {
			$this->load->model('Profile_model');
			$this->load->library('form_validation');
		} else {
			logout();
		}
	}

	public function index()
	{
		$this->detail();
	}

	public function detail()
	{
		$id = getID();
		$row = $this->Profile_model->get_by_id($id);
		if ($row) {
			$data = array(
				'tittle'            => 'Profile',
				'judul'             => 'Profile',
				'title_card'        => 'Profile Superadmin',
				'id_superadmin'     => $row->id_superadmin,
				'username'          => $row->username,
				'password'          => $row->password,
				'email_superadmin'  => $row->email_superadmin,
				'nama_superadmin'   => $row->nama_superadmin,
				'last_login'        => $row->last_login,
				'created_at'        => $row->created_at,
				'menu_aktif' => ''
			);
			$res['datakonten'] = $this->load->view('profile/profile_detail', $data, true);
			$this->load->view('layouts/main_view', $res);
		} else {
			$this->session->set_flashdata('message', '<div class="alert alert-warning" role="alert">Data Tidak Di Temukan</div>');
			// redirect(site_url('profile'));
		}
	}

	public function create()
	{
		$data = array(
			'button' => 'Create',
			'action' => site_url('profile/create_action'),
			'id_superadmin' => set_value('id_superadmin'),
			'username' => set_value('username'),
			'password' => set_value('password'),
			'email_superadmin' => set_value('email_superadmin'),
			'nama_superadmin_admin' => set_value('nama_superadmin_admin'),
			'last_login' => set_value('last_login'),
			'created_at' => set_value('created_at'),
		);
		$this->load->view('profile/profile_form', $data);
	}

	public function create_action()
	{
		$this->_rules();

		if ($this->form_validation->run() == FALSE) {
			$this->create();
		} else {
			$data = array(
				'username' => $this->input->post('username', TRUE),
				'password' => $this->input->post('password', TRUE),
				'email_superadmin' => $this->input->post('email_superadmin', TRUE),
				'nama_superadmin_admin' => $this->input->post('nama_superadmin_admin', TRUE),
				'last_login' => $this->input->post('last_login', TRUE),
				'created_at' => $this->input->post('created_at', TRUE),
			);

			$this->Profile_model->insert($data);
			$this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">Berhasil Menambahkan Data </div>');
			redirect(site_url('profile'));
		}
	}

	public function update()
	{
		$email_superadmin = $this->session->userdata('email_superadmin');
		$row = $this->Profile_model->get_by_email($email_superadmin);

		// if ($row) {
		$data = array(
			'tittle'    => 'Profile Update',
			'judul'     => 'Profile Update',
			'button' => 'Update',
			'menu_aktif' => '',
			'action' => site_url('profile/update_action'),
			'id_superadmin' => set_value('id_superadmin', $row->id_superadmin),
			'username' => set_value('username', $row->username),
			'password' => set_value('password', $row->password),
			'email_superadmin' => set_value('email_superadmin', $row->email_superadmin),
			'nama_superadmin' => set_value('nama_superadmin', $row->nama_superadmin),
			'last_login' => set_value('last_login', $row->last_login),
			'created_at' => set_value('created_at', $row->created_at),
		);
		$res['datakonten'] = $this->load->view('profile/profile_form', $data, true);
		$this->load->view('layouts/main_view', $res);
		// } else {
		//     $this->session->set_flashdata('message', '<div class="alert alert-warning" role="alert">Data Tidak Ditemukan</div>');
		//     redirect(site_url('profile'));
		// }
	}

	public function update_action()
	{
		$this->_rules();
		$email_superadmin = $this->session->userdata('email_superadmin');
		if ($this->form_validation->run() == FALSE) {
			$this->update($this->input->post('id_superadmin', TRUE));
		} else {
			$row = $this->Profile_model->get_by_email($email_superadmin);
			$cekemail = md5($email_superadmin);

			if (hash('sha256', $cekemail . $this->input->post('passwordlama')) == $row->password) {
				$data = array(
					// 'username' => $this->input->post('username',TRUE),
					'password' => hash('sha256', $cekemail . $this->input->post('password', TRUE)),
					// 'email_superadmin' => $this->input->post('email_superadmin',TRUE),
					// 'nama_superadmin_admin' => $this->input->post('nama_superadmin_admin',TRUE),
					// 'last_login' => $this->input->post('last_login',TRUE),
					// 'created_at' => $this->input->post('created_at',TRUE),
				);
			} else {
				$this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">Password lama yang anda masukan tidak sesuai</div>');
				redirect(site_url('profile/update'));
			}
			$email_superadmin = $this->session->userdata('email_superadmin');
			$this->Profile_model->update($email_superadmin, $data);
			$this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">Berhasil Merubah Data</div>');
			redirect(site_url('profile/update'));
		}
	}

	public function delete($id)
	{
		$row = $this->Profile_model->get_by_id($id);

		if ($row) {
			$this->Profile_model->delete($id);
			$this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">Berhasil Menghapus Data</div>');
			redirect(site_url('profile'));
		} else {
			$this->session->set_flashdata('message', '<div class="alert alert-warning" role="alert">Data Tidak Ditemukan</div>');
			redirect(site_url('profile'));
		}
	}

	public function EditUsername(){
		$username = $this->input->post('username', TRUE);
		$data = array(
			'username' => $username
		);
		$data = $this->db->where('id_superadmin', getID())->update('superadmin', $data);
		if($data){
			$this->session->set_flashdata('berhasil', 'Data berhasil diubah');
			redirect('profile');
		} else {
			$this->session->set_flashdata('gagal', 'Data tidak diubah');
			redirect('profile');
		}
	}

	public function _rules()
	{
		// $this->form_validation->set_rules('username', 'username', 'trim|required');
		$this->form_validation->set_rules('password', 'password', 'trim|required');
		// $this->form_validation->set_rules('email_superadmin', 'email_superadmin', 'trim|required');
		// $this->form_validation->set_rules('nama_superadmin_admin', 'nama_superadmin admin', 'trim|required');
		// $this->form_validation->set_rules('last_login', 'last login', 'trim|required');
		// $this->form_validation->set_rules('created_at', 'dibuat tgl', 'trim|required');

		// $this->form_validation->set_rules('id_superadmin', 'id_superadmin', 'trim');
		$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
	}
}

/* End of file profile.php */
/* Location: ./application/controllers/profile.php */
