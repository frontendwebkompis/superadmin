<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Merk_sampah extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        if (cek_token()) {
            $this->load->model('Merk_sampah_model');
            $this->load->library('form_validation');
        } else {
            logout();
        }
    }

    public function index()
    {
        $q = urldecode($this->input->get('q', TRUE));
        $start = intval($this->input->get('start'));

        if ($q <> '') {
            $config['base_url'] = base_url() . 'merk_sampah?q=' . urlencode($q);
            $config['first_url'] = base_url() . 'merk_sampah?q=' . urlencode($q);
        } else {
            $config['base_url'] = base_url() . 'merk_sampah';
            $config['first_url'] = base_url() . 'merk_sampah';
        }

        $config['per_page'] = 10;
        $config['page_query_string'] = TRUE;
        $config['total_rows'] = $this->Merk_sampah_model->total_rows($q);
        $merk_sampah = $this->Merk_sampah_model->get_limit_data($config['per_page'], $start, $q);

        $this->load->library('pagination');
        $this->pagination->initialize($config);

        $data = array(
            'tittle'            => 'Merk Sampah',
            'judul'             => 'Merk Sampah',
            'title_card'        => 'List Merk Sampah',
            'merk_sampah_data'  => $merk_sampah,
            'q'                 => $q,
            'pagination'        => $this->pagination->create_links(),
            'total_rows'        => $config['total_rows'],
            'start'             => $start,
            'merk_sampah_aktif' => '1',
            'menu_aktif'        => 'merk_sampah'
        );
        $res['datakonten'] = $this->load->view('merk_sampah/merk_sampah_list', $data, true);
        $this->load->view('layouts/main_view', $res);
    }

    public function read($id)
    {
        $row = $this->Merk_sampah_model->get_by_id($id);
        if ($row) {
            $data = array(
                'tittle'            => 'Merk Sampah',
                'judul'             => 'Merk Sampah',
                'title_card'        => 'List Merk Sampah',
                'id_merk'           => $row->id_merk,
                'nama_jenis_sampah' => $row->nama_jenis_sampah,
                'nama_merk'         => $row->nama_merk,
                'merk_sampah_aktif' => '1',
                'menu_aktif'        => 'merk_sampah'
            );
            $res['datakonten'] = $this->load->view('merk_sampah/merk_sampah_read', $data, true);
            $this->load->view('layouts/main_view', $res);
        } else {
            $this->session->set_flashdata('gagal', 'Data Tidak Di Temukan');
            redirect(site_url('merk_sampah'));
        }
    }

    public function create()
    {
        $data = array(
            'tittle'            => 'Merk Sampah',
            'judul'             => 'Merk Sampah',
            'title_card'        => 'Tambah Merk Sampah',
            'button'            => 'Create',
            'action'            => site_url('merk_sampah/create_action'),
            'id_merk'           => set_value('id_merk'),
            'id_jenis_sampah'   => set_value('id_jenis_sampah'),
            'nama_merk'         => set_value('nama_merk'),
            'merk_sampah_aktif' => '1',
            'menu_aktif'        => 'merk_sampah'
        );
        $data['jenis_sampah'] = $this->Merk_sampah_model->get_jenis_all();

        $res['datakonten'] = $this->load->view('merk_sampah/merk_sampah_form', $data, true);
        $this->load->view('layouts/main_view', $res);
    }

    public function create_action()
    {
        $this->_rules();

        $prefix = "MRK";
        // echo $gabung;
        $row = $this->Merk_sampah_model->get_by_prefix($prefix);
        $cek = $this->Merk_sampah_model->get_by_jenis($this->input->post('id_jenis_sampah'));
        $valid=0;
        foreach ($cek as $cek) {
            # code...
            if($cek->nama_merk==$this->input->post('nama_merk'))
                $valid=1;
        }
        $NoUrut = (int) substr($row->max, 3, 5);
        $NoUrut = $NoUrut + 1; //nomor urut +1
        $NoUrut = sprintf('%05d', $NoUrut);
        $fix = $prefix . $NoUrut;
        if ($this->form_validation->run() == FALSE) {
            $this->create();
        } else {
            if($valid==0){
            $data = array(
                'id_merk' => $fix,
                'id_superadmin' => getID(),
                'id_jenis_sampah' => $this->input->post('id_jenis_sampah', TRUE),
                'nama_merk' => $this->input->post('nama_merk', TRUE),
                'status_merk_sampah_remove' => 1,
            );

            $this->Merk_sampah_model->insert($data);
            $this->session->set_flashdata('berhasil', 'Berhasil Menambahkan Data');
            redirect(site_url('merk_sampah'));
         }else{
         $this->session->set_flashdata('gagal', 'Nama Merk dengan Jenis Sampah yang sama sudah Ada');
            redirect(site_url('merk_sampah'));
    }
        }

    }

    public function update($id)
    {
        $row = $this->Merk_sampah_model->get_by_id($id);

        if ($row) {
            $data = array(
                'tittle'            => 'Merk Sampah',
                'judul'             => 'Merk Sampah',
                'title_card'        => 'Update Merk Sampah',
                'button'            => 'Update',
                'action'            => site_url('merk_sampah/update_action'),
                'id_merk'           => set_value('id_merk', $row->id_merk),
                'id_jenis_sampah'   => set_value('id_jenis_sampah', $row->id_jenis_sampah),
                'nama_merk'         => set_value('nama_merk', $row->nama_merk),
                'merk_sampah_aktif' => '1',
                'menu_aktif'        => 'merk_sampah'
            );
            $data['jenis_sampah'] = $this->Merk_sampah_model->get_jenis_all();

            $res['datakonten'] = $this->load->view('merk_sampah/merk_sampah_form', $data, true);
            $this->load->view('layouts/main_view', $res);
        } else {
            $this->session->set_flashdata('gagal', 'Data Tidak Ditemukan');
            redirect(site_url('merk_sampah'));
        }
    }

    public function update_action()
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->update($this->input->post('id_merk', TRUE));
        } else {
            $data = array(
                'id_jenis_sampah' => $this->input->post('id_jenis_sampah', TRUE),
                'nama_merk' => $this->input->post('nama_merk', TRUE),
                'id_superadmin' => getID(),
            );

            $this->Merk_sampah_model->update($this->input->post('id_merk', TRUE), $data);
            $this->session->set_flashdata('berhasil', 'Berhasil Merubah Data');
            redirect(site_url('merk_sampah'));
        }
    }

    public function del_sem($id)
    {
        $row = $this->Merk_sampah_model->get_by_id($id);

        if ($row) {
            $data = array(
                'status_merk_sampah_remove' => 0,
                'id_superadmin' => getID(),
            );
            $this->db->where('id_merk', $id);
            $this->db->update('merk_sampah', $data);
            $this->session->set_flashdata('berhasil', 'Berhasil Menghapus Data');
            redirect(site_url('merk_sampah'));
        } else {
            $this->session->set_flashdata('gagal', 'Data Tidak Ditemukan');
            redirect(site_url('merk_sampah'));
        }
    }
    public function delete($id)
    {
        $row = $this->Merk_sampah_model->get_by_id($id);

        if ($row) {
            $this->Merk_sampah_model->delete($id);
            $this->session->set_flashdata('berhasil', 'Berhasil Menghapus Data');
            redirect(site_url('merk_sampah'));
        } else {
            $this->session->set_flashdata('gagal', 'Data Tidak Ditemukan');
            redirect(site_url('merk_sampah'));
        }
    }

    public function _rules()
    {
        $this->form_validation->set_rules('id_jenis_sampah', 'id kategori sampah', 'trim|required');
        $this->form_validation->set_rules('nama_merk', 'nama merk', 'trim|required');

        $this->form_validation->set_rules('id_merk', 'id_merk', 'trim');
        $this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

    public function excel()
    {
        $this->load->helper('exportexcel');
        $namaFile = "merk_sampah.xls";
        $judul = "merk_sampah";
        $tablehead = 0;
        $tablebody = 1;
        $nourut = 1;
        //penulisan header
        header("Pragma: public");
        header("Expires: 0");
        header("Cache-Control: must-revalidate, post-check=0,pre-check=0");
        header("Content-Type: application/force-download");
        header("Content-Type: application/octet-stream");
        header("Content-Type: application/download");
        header("Content-Disposition: attachment;filename=" . $namaFile . "");
        header("Content-Transfer-Encoding: binary ");

        xlsBOF();

        $kolomhead = 0;
        xlsWriteLabel($tablehead, $kolomhead++, "No");
        xlsWriteLabel($tablehead, $kolomhead++, "Id Kategori Sampah");
        xlsWriteLabel($tablehead, $kolomhead++, "Nama Merk");

        foreach ($this->Merk_sampah_model->get_all() as $data) {
            $kolombody = 0;

            //ubah xlsWriteLabel menjadi xlsWriteNumber untuk kolom numeric
            xlsWriteNumber($tablebody, $kolombody++, $nourut);
            xlsWriteNumber($tablebody, $kolombody++, $data->nama_kategori_sampah);
            xlsWriteLabel($tablebody, $kolombody++, $data->nama_merk);

            $tablebody++;
            $nourut++;
        }

        xlsEOF();
        exit();
    }
}

/* End of file Merk_sampah.php */
/* Location: ./application/controllers/Merk_sampah.php */
