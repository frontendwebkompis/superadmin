<script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>

<div class="row">
	<section class="col-lg connectedSortable ui-sortable">
		<div class="card card-info">
			<div class="card-header">
                <div class="card-title">
                  <?php echo $title_card?>
                </div>
            </div>
			<div class="card-body">
				<div class="tab-content p-0">
					<div class="row">
						<div class="col-md-4 mb-2">
							<?php echo anchor(site_url('merk_sampah/create'),'Tambah Data', 'class="btn btn-info btn-sm"'); ?>
						</div>
						<div class="col-md-3 offset-md-5 mb-2">
							<?= search(site_url('merk_sampah/index'), site_url('merk_sampah'), $q) ?>
						</div>
					</div>
					<div class="tab-content p-0" style="overflow:auto">
					<table class="table table-bordered table-striped table-condensed table-hover table-sm" style="margin-bottom: 10px">
						<tr>
							<th class="text-center" width="50px">No</th>
							<th class="text-center">Jenis Sampah</th>
							<th class="text-center">Nama Merk</th>
							<th class="text-center">Aksi</th>
						</tr>

						<?php
							if($total_rows == 0){
								echo '<tr><td colspan="8" bgcolor="grey" style="color: white; font-weight: bold; text-align: center;">Data Tidak Ditemukan</td></tr>';
							} else {
						?>
						<?php foreach ($merk_sampah_data as $merk_sampah) { ?>
						<tr> 
							<td class="text-center"><?php echo ++$start ?></td>
							<td><?php echo $merk_sampah->nama_jenis_sampah ?></td>
							<td><?php echo $merk_sampah->nama_merk ?></td>
							<td class="text-center" width="150px">
								<div class="btn-group">
									<a href="<?php echo site_url('merk_sampah/update/'.$merk_sampah->id_merk); ?>"
									data-toogle="tooltip" title="Update">
									<button type="button" class="btn btn-success btn-sm"><i class="far fa-edit"></i></button></a>
									<a href="#" class="btn btn-danger btn-sm" data-toggle="tooltip" data-placement="top" title="Hapus" onclick="confirm('<?=  $merk_sampah->id_merk ?>')" ><i class="fas fa-trash-alt"></i></a>
								</div>
							</td>
						</tr>
						<?php } 
						}?>
					</table>
					</div>
					<?= footer($total_rows, $pagination, site_url('merk_sampah/exportxl?q=' . $q)) ?>
					</body>
				</div>
			</div>
		</div>
	</section>
</div>

<script>
function confirm(res) {
    Swal.fire({
    title: 'Anda Yakin Mau Menghapus?',
    text: "Tidak bisa dikembalikan jika sudah dihapus!",
    icon: 'warning',
    showCancelButton: true,
    confirmButtonColor: '#3085d6',
    cancelButtonColor: '#d33',
    confirmButtonText: 'Ya, Hapus!',
    cancelButtonText: 'Batal'
    }).then((result) => {
    if (result.value) {
        Swal.fire(
        'Terhapus!',
        'File sudah terhapus.',
        'success'
        )
        window.location='<?php echo base_url().'merk_sampah/del_sem/'; ?>'+res;
    }
    });
}
</script>
