<div class="row">
	<!-- Left col -->
	<section class="col-lg connectedSortable ui-sortable">
		<!-- Custom tabs (Charts with tabs)-->
		<div class="card card-info">
			<div class="card-header">
                <div class="card-title">
                  <?php echo $title_card?>
                </div>
            </div>
			<div class="card-body">
				<div class="tab-content p-0">

					<form action="<?php echo $action; ?>" method="post" id="dataform">
						<div class="form-group">
							<label for="varchar">Jenis Sampah <?php echo form_error('id_jenis_sampah') ?></label>
							<select id="id_jenis_sampah" class="form-control" name="id_jenis_sampah">
								<option value="">Pilih Jenis Sampah</option>
								<?php foreach ($jenis_sampah as $kat){ ?>
								<option <?=  $kat->id_jenis_sampah ==  $id_jenis_sampah ? 'selected' : ''; ?>
								value="<?php echo $kat->id_jenis_sampah; ?>">
								<?php echo $kat->nama_jenis_sampah; ?>
								</option>
								<?php }?>
							</select>
						</div>
						<div class="form-group">
							<label for="varchar">Nama Merk <?php echo form_error('nama_merk') ?></label>
							<input type="text" class="form-control" name="nama_merk" id="nama_merk"
								placeholder="Nama Merk" value="<?php echo $nama_merk; ?>" />
						</div>
						<input type="hidden" name="id_merk" value="<?php echo $id_merk; ?>" />
						<button type="submit" class="btn btn-info" id="btn-simpan">Simpan
						</button>
						<a href="<?php echo site_url('merk_sampah') ?>" class="btn btn-default">Batal</a>
					</form>

				</div>
			</div><!-- /.card-body -->
		</div>
		<!-- /.card -->
	</section>
	<!-- /.Left col -->
</div>

<script type="text/javascript">
    $(document).ready(function () {
      $('#dataform').validate({
        rules: {
          id_jenis_sampah: {
          	required: true,
          },
          nama_merk: {
          	required: true,
          }
        },
        messages: {
          id_jenis_sampah: {
          	required: "Jenis Sampah Tidak Boleh Kosong",
          },
          nama_merk: {
          	required: "Nama Merk Tidak Boleh Kosong",
          }
        },
        submitHandler: function(form) {
          $('#btn-simpan').prop('disabled', true);
          form.submit();
        },
        errorElement: 'span',
        errorPlacement: function (error, element) {
          error.addClass('invalid-feedback');
          element.closest('.form-group').append(error);
        },
        highlight: function (element, errorClass, validClass) {
          $(element).addClass('is-invalid');
        },
        unhighlight: function (element, errorClass, validClass) {
          $(element).removeClass('is-invalid');
        },
      });
    });
</script>