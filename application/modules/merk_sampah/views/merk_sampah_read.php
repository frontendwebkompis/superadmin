<div class="row">
	<!-- Left col -->
	<section class="col-lg connectedSortable ui-sortable">
		<!-- Custom tabs (Charts with tabs)-->
		<div class="card card-info">
			<div class="card-header">
                <div class="card-title">
                  <?php echo $title_card?>
                </div>
            </div>
			<div class="card-body">
				<div class="tab-content p-0" style="overflow:auto">
					<table class="table table-bordered table-striped table-condensed table-hover" style="margin-bottom: 10px">
						<tr>
							<td>Jenis Sampah</td><td><?php echo $nama_jenis_sampah; ?></td>
						</tr>
						<tr>
							<td>Nama Merk</td><td><?php echo $nama_merk; ?></td>
						</tr>
					</table>
					<div>
					<a href="<?php echo site_url('merk_sampah') ?>" class="btn btn-info">Batal</a>
					</div>

				</div>
			</div><!-- /.card-body -->
		</div>
		<!-- /.card -->
	</section>
	<!-- /.Left col -->
</div>
