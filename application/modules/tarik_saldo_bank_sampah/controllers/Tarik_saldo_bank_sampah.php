<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Tarik_saldo_bank_sampah extends CI_Controller
{

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Tarik_saldo_bank_sampah_model');
		$this->load->library('form_validation');
	}

	public function index()
	{
		$model = $this->Tarik_saldo_bank_sampah_model;
		$q = urldecode($this->input->get('q', TRUE));
		$start = intval($this->input->get('start'));

		if ($q <> '') {
			$config['base_url'] = base_url() . 'tarik_saldo_bank_sampah/index.html?q=' . urlencode($q);
			$config['first_url'] = base_url() . 'tarik_saldo_bank_sampah/index.html?q=' . urlencode($q);
		} else {
			$config['base_url'] = base_url() . 'tarik_saldo_bank_sampah/index.html';
			$config['first_url'] = base_url() . 'tarik_saldo_bank_sampah/index.html';
		}

		$config['per_page'] = 10;
		$config['page_query_string'] = TRUE;
		$config['total_rows'] = 0; //$model->total_rows($q);
		$tarik_saldo_bank_sampah = []; //$model->get_limit_data($config['per_page'], $start, $q);

		$this->load->library('pagination');
		$this->pagination->initialize($config);

		$data = array(
			'tittle'    					=> 'Tarik Saldo Bank Sampah',
			'titleCard'    					=> 'Tarik Saldo Bank Sampah List',
			'judul'     					=> 'Tarik Saldo Bank Sampah',
			'tarik_saldo_bank_sampah_data' 	=> $tarik_saldo_bank_sampah,
			'q' 							=> $q,
			'pagination' 					=> $this->pagination->create_links(),
			'total_rows' 					=> $config['total_rows'],
			'start' 						=> $start,
			'menu_aktif'					=> 'tarik_saldo_bank_sampah'
		);
		$res['datakonten'] = $this->load->view('tarik_saldo_bank_sampah/tarik_saldo_bank_sampah_list', $data, true);
		$this->load->view('layouts/main_view', $res);
	}
}

/* End of file Tarik_saldo_bank_sampah.php */
