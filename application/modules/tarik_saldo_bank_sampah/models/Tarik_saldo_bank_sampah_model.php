<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Tarik_saldo_bank_sampah_model extends CI_Model
{



	public $table = 'tarik_saldo_bs';
	public $id = 'id_tarik_saldo_bs';
	public $order = 'DESC';
	// get total rows
	function total_rows($q = NULL)
	{
		$this->db->group_start();
		$this->db->like('id_tbl_harga_detail', $q);
		$this->db->or_like('id_tabel_harga', $q);
		$this->db->group_end();
		$this->db->from($this->table);
		return $this->db->count_all_results();
	}

	// get data with limit and search
	function get_limit_data($limit, $start = 0, $q = NULL)
	{
		$this->db->group_start();
		$this->db->like('id_tbl_harga_detail', $q);
		$this->db->or_like('id_tabel_harga', $q);
		$this->db->group_end();
		$this->db->limit($limit, $start);
		return $this->db->get($this->table)->result();
	}
}

/* End of file Tarik_saldo_bank_sampah_model.php */
