<div class="card card-<?= bgCard() ?>">
	<div class="card-header">
		<div class="card-title">
			<?= $titleCard ?>
		</div>
	</div>
	<div class="card-body">
		<div class="tab-content p-0">
			<div class="row">
				<div class="col-md-3 offset-md-9 mb-2">
					<?= search(site_url('tarik_saldo_bank_sampah'), site_url('tarik_saldo_bank_sampah'), $q) ?>
				</div>
			</div>
			<div class="tab-content p-0" style="overflow:auto">
				<table class="table table-bordered table-striped table-condensed table-hover table-sm" style="margin-bottom: 10px">
					<tr>
						<th style="text-align:center" width="20px">No</th>
						<th style="text-align:center">Aksi</th>
					</tr>
					<?php
					if ($total_rows == 0) {
						echo '<tr><td colspan="11" bgcolor="grey" style="color: white; font-weight: bold; text-align: center;">Data Tidak Ditemukan</td></tr>';
					} else {
						foreach ($tarik_saldo_bank_sampah_data as $k) { ?>
							<tr>
								<td style="text-align:center"><?php echo ++$start ?></td>
								<td style="text-align:center" width="100px">
									<div class="btn-group">
										<a href="<?php echo site_url('tarik_saldo_bank_sampah/read/' . $k->id_tarik_saldo_bs); ?>" data-toogle="tooltip" title="Detail">
											<button type="button" class="btn btn-info"><i class="fas fa-info-circle"></i></button></a>
										<a href="<?php echo site_url('tarik_saldo_bank_sampah/update/' . $k->id_tarik_saldo_bs); ?>" data-toogle="tooltip" title="Update">
											<button type="button" class="btn btn-success"><i class="far fa-edit"></i></button></a>
										<a href="#" class="btn btn-danger" data-toggle="tooltip" data-placement="top" title="Hapus" onclick="confirm(<?= $k->id_tarik_saldo_bs ?>)"><i class="fas fa-trash-alt"></i></a>
									</div>
								</td>
							</tr>
					<?php }
					} ?>
				</table>
			</div>
			<?= footer($total_rows, $pagination, '') ?>
		</div>
	</div>
</div>