<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');
class Promo extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        if (cek_token()) {
            $this->load->model('Promo_model');
            $this->load->library('form_validation');
            date_default_timezone_set('Asia/Jakarta');

            $this->load->library('upload');
        } else {
            logout();
        }
    }

    public function index()
    {
        $q = urldecode($this->input->get('q', TRUE));
        $start = intval($this->input->get('start'));

        if ($q <> '') {
            $config['base_url'] = base_url() . 'promo?q=' . urlencode($q);
            $config['first_url'] = base_url() . 'promo?q=' . urlencode($q);
        } else {
            $config['base_url'] = base_url() . 'promo';
            $config['first_url'] = base_url() . 'promo';
        }

        $config['per_page'] = 10;
        $config['page_query_string'] = TRUE;
        $config['total_rows'] = $this->Promo_model->total_rows($q);
        $promo = $this->Promo_model->get_limit_data($config['per_page'], $start, $q);

        $this->load->library('pagination');
        $this->pagination->initialize($config);

        $data = array(
            'tittle'        => 'Promo',
            'judul'         => 'Promo',
            'title_card'    => 'Promo List',
            'promo_data'    => $promo,
            'q'             => $q,
            'pagination'    => $this->pagination->create_links(),
            'total_rows'    => $config['total_rows'],
            'start'         => $start,
            'promo_aktif'   => '1',
            'menu_aktif'    => 'promo'
        );
        $res['datakonten'] = $this->load->view('promo/promo_list', $data, true);
        $this->load->view('layouts/main_view', $res);
    }

    public function read($id)
    {
        $row = $this->Promo_model->get_by_id($id);
        if ($row) {
            $data = array(
                'tittle'        => 'Promo',
                'judul'         => 'Promo',
                'title_card'    => 'Promo Detail',
                'id_promo'      => $row->id_promo,
                'judul_promo'   => $row->judul_promo,
                'gambar_promo'  => $row->gambar_promo,
                'detail_promo'  => $row->detail_promo,
                'id_tipe_promo' => $row->id_tipe_promo,
                'created_at'    => $row->created_at,
                'ket_status'    => $row->ket_status,
                'promo_aktif'   => '1',
                'menu_aktif'    => 'promo'
            );
            $res['datakonten'] = $this->load->view('promo/promo_read', $data, true);
            $this->load->view('layouts/main_view', $res);
        } else {
            $this->session->set_flashdata(
                'gagal', 'Data Tidak Di Temukan'
            );
            redirect(site_url('promo'));
        }
    }

    public function create()
    {
        $data = array(
            'tittle'            => 'Promo',
            'judul'             => 'Promo',
            'title_card'        => 'Tambah Promo',
            'button'            => 'Create',
            'action'            => site_url('promo/create_action'),
            'id_promo'          => set_value('id_promo'),
            'judul_promo'       => set_value('judul_promo'),
            'gambar_promo'      => set_value('gambar_promo'),
            'detail_promo'      => set_value('detail_promo'),
            'id_tipe_promo'     => set_value('id_tipe_promo'),
            'created_at_promo'  => set_value('created_at_promo'),
            'id_status_promo'   => set_value('id_status_promo'),
            'promo_aktif'       => '1',
            'menu_aktif'        => 'promo'
        );
        $data['status_infopromo'] = $this->Promo_model->get_status_all();

        $res['datakonten'] = $this->load->view('promo/promo_form', $data, true);
        $this->load->view('layouts/main_view', $res);
    }

    public function create_action()
    {
        $this->_rules();
        $dt = new DateTime();
        $dt->format('Y-m-d H:i:s');
        $prefix = "PRO" . $dt->format('dmY');
        // echo $gabung;
        $row = $this->Promo_model->get_by_prefix($prefix);
        $NoUrut = (int) substr($row->max, 11, 4);
        $NoUrut = $NoUrut + 1; //nomor urut +1
        $NoUrut = sprintf('%04d', $NoUrut);
        $fix = $prefix . $NoUrut;
        
        $config['upload_path']      = './assets/gambar/gambar_promo';
        $config['allowed_types']    = 'gif|jpg|png|jpeg|bmp';
        $config['encrypt_name']     = TRUE;

        $this->upload->initialize($config);
        if (!empty($_FILES['gambar_promobaru']['name'])) {

            if ($this->upload->do_upload('gambar_promobaru', TRUE)) {
                $gbr = $this->upload->data();
                $config['image_library']    = 'gd2';
                $config['source_image']     = './assets/gambar/gambar_promo/' . $gbr['file_name'];
                $config['create_thumb']     = FALSE;
                $config['maintain_ratio']   = FALSE;
                $config['quality']          = '50%';
                $config['width']            = 600;
                $config['height']           = 400;
                $config['new_image']        = './assets/images/gambar_promo/' . $gbr['file_name'];
                $this->load->library('image_lib', $config);
                $this->image_lib->resize();
                $upload = s3_upload('promo/'.$gbr['file_name'], $gbr['full_path']);
                @unlink('assets/gambar/gambar_promo/' . $gbr['file_name']);
                $gambar=$upload['ObjectURL'];
            }

        } else {
            echo "Image yang diupload kosong";
        }

        if ($this->form_validation->run() == FALSE) {
            $this->create();
        } else {
            $data = array(
                'id_promo'              => $fix,
                'judul_promo'           => $this->input->post('judul_promo', TRUE),
                'gambar_promo'          => $gambar,
                'detail_promo'          => $this->input->post('detail_promo', TRUE),
                'id_tipe_promo'         => $this->input->post('id_tipe_promo', TRUE),
                'id_status_promo'       => $this->input->post('id_status_promo', TRUE),
                'created_at'            => $dt->format('Y-m-d H:i:s'),
                'status_promo_remove'   => 1,
               
            );
            $this->Promo_model->insert($data);
            $this->session->set_flashdata('berhasil', 'Berhasil Menambahkan Data');
            redirect(site_url('promo'));
        }
    }

    public function update($id)
    {
        $row = $this->Promo_model->get_by_id($id);

        if ($row) {
            $data = array(
                'tittle'            => 'Promo',
                'judul'             => 'Promo',
                'title_card'        => 'Promo Update',
                'button'            => 'Update',
                'action'            => site_url('promo/update_action'),
                'id_promo'          => set_value('id_promo', $row->id_promo),
                'judul_promo'       => set_value('judul_promo', $row->judul_promo),
                'gambar_promo'      => set_value('gambar_promo', $row->gambar_promo),
                'detail_promo'      => set_value('detail_promo', $row->detail_promo),
                'id_tipe_promo'     => set_value('id_tipe_promo', $row->id_tipe_promo),
                'id_status_promo'   => set_value('id_status_promo', $row->id_status_promo),
                'promo_aktif'       => '1',
                'menu_aktif'        => 'promo'
            );
         $data['status_infopromo'] = $this->Promo_model->get_status_all();

            $res['datakonten'] = $this->load->view('promo/promo_form', $data, true);
            $this->load->view('layouts/main_view', $res);
        } else {
            $this->session->set_flashdata('gagal', 'Data Tidak Ditemukan');
            redirect(site_url('promo'));
        }
    }

    public function update_action()
    {
        $this->_rules();
        $config['upload_path'] = './assets/gambar/gambar_promo/';
        $config['allowed_types'] = 'gif|jpg|png|jpeg|bmp';
        $config['encrypt_name'] = TRUE;


        $this->upload->initialize($config);
        if (!empty($_FILES['gambar_promobaru']['name'])) {

            if ($this->upload->do_upload('gambar_promobaru', TRUE)) {
                $gbr = $this->upload->data();
                $config['image_library']    = 'gd2';
                $config['source_image']     = './assets/gambar/gambar_promo/' . $gbr['file_name'];
                $config['create_thumb']     = FALSE;
                $config['maintain_ratio']   = FALSE;
                $config['quality']          = '50%';
                $config['width']            = 600;
                $config['height']           = 400;
                $config['new_image']        = './assets/images/gambar_promo/' . $gbr['file_name'];
                $this->load->library('image_lib', $config);
                $this->image_lib->resize();
                @unlink('assets/gambar/gambar_promo/' . $this->input->post('gambar_promo'));
                 del(basename('promo/'.$this->input->post('gambar_promo')));
                $upload = s3_upload('promo/'.$gbr['file_name'], $gbr['full_path']);
                $gambar=$upload['ObjectURL'];
            }
        } else {
            $gambar = $this->input->post('gambar_promo');
        }


        if ($this->form_validation->run() == FALSE) {
            $this->update($this->input->post('id_promo', TRUE));
        } else {
            $data = array(
                'judul_promo'       => $this->input->post('judul_promo', TRUE),
                'gambar_promo'      => $gambar,
                'detail_promo'      => $this->input->post('detail_promo', TRUE),
                'id_tipe_promo'     => $this->input->post('id_tipe_promo', TRUE),
                'id_status_promo'   => $this->input->post('id_status_promo', TRUE),
            );

            $this->Promo_model->update($this->input->post('id_promo', TRUE), $data);
            $this->session->set_flashdata('berhasil', 'Berhasil Merubah Data');
            redirect(site_url('promo'));
        }
    }


    public function del_sem($id)
    {
        $row = $this->Promo_model->get_by_id($id);

        if ($row) {
            $data = array(
                'status_promo_remove' => 0,
            );
            $this->Promo_model->update($id, $data);
            del(basename('promo/'.$row->gambar_promo));
            // @unlink('assets/gambar/gambar_promo/' . $row->gambar_promo);
            $this->session->set_flashdata('berhasil', 'Berhasil Menghapus Data');
            redirect(site_url('promo'));
        } else {
            $this->session->set_flashdata('gagal', 'Data Tidak Ditemukan');
            redirect(site_url('promo'));
        }
    }

    function get_image(){
        $img=$this->input->get('img');
        $cek='https://kompis-bank-sampah.s3.ap-southeast-1.amazonaws.com/'.$img;
        $row = $this->Promo_model->get_by_poto($cek);
        if($row){
            echo getkonten($img);  
        }else{
            $this->session->set_flashdata('cek', 'Gambar Tidak Ditemukan');
            redirect('profile');
        }
    }

    public function _rules()
    {
        $this->form_validation->set_rules('judul_promo', 'judul promo', 'trim|required');
        $this->form_validation->set_rules('detail_promo', 'detail promo', 'trim|required');
        $this->form_validation->set_rules('id_tipe_promo', 'tipe promo', 'trim|required');
        $this->form_validation->set_rules('id_promo', 'id_promo', 'trim');
        $this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

    public function excel()
    {
        $this->load->helper('exportexcel');
        $namaFile   = "promo.xls";
        $judul      = "promo";
        $tablehead  = 0;
        $tablebody  = 1;
        $nourut     = 1;
        //penulisan header
        header("Pragma: public");
        header("Expires: 0");
        header("Cache-Control: must-revalidate, post-check=0,pre-check=0");
        header("Content-Type: application/force-download");
        header("Content-Type: application/octet-stream");
        header("Content-Type: application/download");
        header("Content-Disposition: attachment;filename=" . $namaFile . "");
        header("Content-Transfer-Encoding: binary ");

        xlsBOF();

        $kolomhead = 0;
        xlsWriteLabel($tablehead, $kolomhead++, "No");
        xlsWriteLabel($tablehead, $kolomhead++, "Judul Promo");
        xlsWriteLabel($tablehead, $kolomhead++, "Gambar Promo");
        xlsWriteLabel($tablehead, $kolomhead++, "Detail Promo");
        xlsWriteLabel($tablehead, $kolomhead++, "Tipe Promo");
        xlsWriteLabel($tablehead, $kolomhead++, "Created At Promo");
        xlsWriteLabel($tablehead, $kolomhead++, "Status Promo");

        foreach ($this->Promo_model->get_all() as $data) {
            $kolombody = 0;

            //ubah xlsWriteLabel menjadi xlsWriteNumber untuk kolom numeric
            xlsWriteNumber($tablebody, $kolombody++, $nourut);
            xlsWriteLabel($tablebody, $kolombody++, $data->judul_promo);
            xlsWriteLabel($tablebody, $kolombody++, $data->gambar_promo);
            xlsWriteLabel($tablebody, $kolombody++, $data->detail_promo);
            xlsWriteNumber($tablebody, $kolombody++, $data->tipe_promo);
            xlsWriteLabel($tablebody, $kolombody++, $data->created_at_promo);
            xlsWriteLabel($tablebody, $kolombody++, $data->status_promo);

            $tablebody++;
            $nourut++;
        }

        xlsEOF();
        exit();
    }
}

/* End of file Promo.php */
/* Location: ./application/controllers/Promo.php */
