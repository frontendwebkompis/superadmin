<script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>

<div class="row">
    <section class="col-lg connectedSortable ui-sortable">
        <div class="card card-info">
            <div class="card-header">
                <div class="card-title">
                    <?php echo $title_card ?>
                </div>
            </div>
            <div class="card-body">
                <div class="tab-content p-0">
                    <div class="row">
                        <div class="col-md-4 mb-2">
                            <?php echo anchor(site_url('promo/create'), 'Tambah Data', 'class="btn btn-info btn-sm"'); ?>
                        </div>
                        <div class="col-md-3 offset-md-5 mb-2">
                            <?= search(site_url('promo/index'), site_url('promo'), $q) ?>
                        </div>
                    </div>
                    <div class="tab-content p-0" style="overflow:auto">
                        <table class="table table-bordered table-striped table-condensed table-hover table-sm mb-2">
                            <tr>
                                <th class="text-center" width="50px">No</th>
                                <th class="text-center">Judul Promo</th>
                                <th class="text-center">Gambar Promo</th>
                                <th class="text-center">Detail Promo</th>
                                <th class="text-center">Tipe Promo</th>
                                <th class="text-center">Keterangan</th>
                                <th class="text-center">Dibuat Tanggal</th>
                                <th class="text-center">Aksi</th>
                            </tr>
                            <?php
                            if ($total_rows == 0) {
                                echo '<tr><td colspan="8" bgcolor="grey" style="color: white; font-weight: bold; text-align: center;">Data Tidak Ditemukan</td></tr>';
                            } else {
                            ?>
                                <?php foreach ($promo_data as $promo) { ?>
                                    <tr>
                                        <td class="text-center"><?php echo ++$start ?></td>
                                        <td><?php echo $promo->judul_promo ?></td>
                                        <td class="text-center">
                                            <?php
                                            if ($promo->gambar_promo == null) {
                                                echo '<span class="badge badge-danger">Kosong</span>';
                                            } else { ?>
                                                <img src="<?php echo base_url(); ?>promo/get_image?img=promo/<?php echo basename($promo->gambar_promo); ?>" alt="Gambar Logo Bank" style="max-height:100px;">
                                            <?php   }
                                            ?>
                                        </td>
                                        <td><?php echo $promo->detail_promo ?></td>
                                        <td class="text-center"><?php if ($promo->id_tipe_promo == 1) echo "Info";
                                                                else echo "Promo" ?></td>
                                        <td class="text-center"><?php echo $promo->ket_status ?></td>
                                        <td class="text-center"><?php echo fulldate($promo->created_at) ?></td>
                                        <td class="text-center" width="150px">
                                            <div class="btn-group">
                                                <a href="<?php echo site_url('promo/read/' . $promo->id_promo); ?>" data-toogle="tooltip" title="Lihat">
                                                    <button type="button" class="btn btn-info btn-sm"><i class="fas fa-info-circle"></i></button></a>
                                                <a href="<?php echo site_url('promo/update/' . $promo->id_promo); ?>" data-toogle="tooltip" title="Update">
                                                    <button type="button" class="btn btn-success btn-sm"><i class="far fa-edit"></i></button></a>
                                                <a href="#" data-toggle="tooltip" data-placement="top" title="Hapus" onclick="confirm('<?= $promo->id_promo ?>')"><button class="btn btn-danger btn-sm"><i class="fas fa-trash-alt"></i></button></a>

                                            </div>
                                        </td>
                                    </tr>
                            <?php }
                            } ?>
                        </table>
                    </div>
                    <?= footer($total_rows, $pagination, '') ?>
                </div>
            </div>
        </div>
    </section>
</div>

<script>
    function confirm(res) {
        Swal.fire({
            title: 'Anda Yakin Mau Menghapus?',
            text: "Tidak bisa dikembalikan jika sudah dihapus!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Ya, Hapus!',
            cancelButtonText: 'Batal'
        }).then((result) => {
            if (result.value) {
                Swal.fire(
                    'Terhapus!',
                    'File sudah terhapus.',
                    'success'
                )
                window.location = '<?php echo base_url() . 'promo/del_sem/'; ?>' + res;
            }
        });
    }
</script>