<div class="row">
	<section class="col-lg connectedSortable ui-sortable">
		<div class="card card-info">
      <div class="card-header">
        <div class="card-title"> 
          <?php echo $title_card?>
        </div>
      </div>
			<div class="card-body">
				<div class="tab-content p-0">

        <form id="dataform" action="<?php echo $action; ?>" method="post" enctype="multipart/form-data">
	        <div class="form-group">
            <label for="varchar">Judul Promo <?php echo form_error('judul_promo') ?></label>
            <input type="text" class="form-control" name="judul_promo" id="judul_promo" placeholder="Judul Promo" value="<?php echo $judul_promo; ?>" />
          </div>
	        <div class="form-group">
            <label for="varchar">Gambar Promo <?php echo form_error('gambar_promo') ?></label>
            <?php if($gambar_promo !=""){ ?>
              <div class="row justify-content-center mb-2">
                  <img class="img-fluid rounded" style="height:160px" src="<?php echo $gambar_promo ?>" alt="Gambar Promo">
              </div>
              <div class="row justify-content-center mb-2"> 
                  <button class="btn btn-secondary btn-sm" type="button" onclick="cek()"><i class="fas fa-image"></i> Ganti Gambar</button>
              </div>
            
            <input type="file" style="display:none" accept="image/*" class="form-control" name="gambar_promobaru" id="gambar_promobaru">
            <input  type="hidden" id="gambar_promo" accept="image/*" value="<?php echo $gambar_promo ?>" name="gambar_promo">
            <?php }else{ ?>
            <input type="file" class="form-control" accept="image/*" name="gambar_promobaru" id="gambar_promobaru">
            <?php } ?>    
          </div>
	        <div class="form-group">
            <label for="varchar">Detail Promo <?php echo form_error('detail_promo') ?></label>
            <textarea class="textarea" required name="detail_promo" class="form-control" id="detail_promo" cols="30" rows="3"><?php echo $detail_promo; ?></textarea>
          </div>
	        <div class="form-group">
            <label for="int">Tipe Promo <?php echo form_error('id_tipe_promo') ?></label>
            <select required type="text" class="form-control" name="id_tipe_promo" id="id_tipe_promo" placeholder="Tipe Promo" value="<?php echo $tipe_promo; ?>" >
              <option value="">Pilih Kategori</option>
              <option value="1" <?=  1 ==  $id_tipe_promo ? 'selected' : ''; ?> >Info</option>
              <option value="2" <?=  2 ==  $id_tipe_promo ? 'selected' : ''; ?> >Promosi</option>
            </select>
</div>

                <div class="form-group">
                  <label for="int">Status Promo <?php echo form_error('id_tipe_promo') ?></label>
                <select disabled required type="text" class="form-control" name="id_status_promo" id="id_status_promo"  >
                  <option nama="-1" value="">Pilih Status Promo</option>
                  <?php foreach ($status_infopromo as $promo) { 
                    if($promo->id_status_infopromo==1 or $promo->id_status_infopromo==2 or $promo->id_status_infopromo==3){
                      $cek=1;
                    }else{
                      $cek=2;
                    }
                    ?>
                  <option nama="<?= $cek ?>" value="<?php echo $promo->id_status_infopromo; ?>"  <?=  $promo->id_status_infopromo ==  $id_status_promo ? 'selected' : ''; ?> ><?php echo $promo->ket_status; ?> </option>
                   <?php }
                    ?>
                    </select>
                  </div>


            <div id="loader" class="alert alert-info" style="display:none">
                <strong>Info!</strong> Sedang Proses....
            </div>
            <input type="hidden" name="id_promo" value="<?php echo $id_promo; ?>" /> 
            <button id="ceksub" type="submit" class="btn btn-info" style="width: 100px;">Simpan</button> 
	          <a href="<?php echo site_url('promo') ?>" class="btn btn-default" style="width: 100px;">Batal</a>


    <script>
        function cek() {
          var x = document.getElementById("gambar_promobaru");
          if (x.style.display === "none") {
            x.style.display = "block";
          } else {
            x.style.display = "none";
          }
        } 

        $(document).ready(function () {
          $('#dataform').validate({
            rules: {
              judul_promo: {
                required: true,
              },
              gambar_promobaru: {
                required: true,
              },
              detail_promo: {
                required: true,
              },
              id_status_promo:{
                required: true,
              },
              id_tipe_promo: {
                required: true,
              }
            },
            messages: {
              judul_promo: {
                required: "Judul Promo Tidak Boleh Kosong",
              },
              gambar_promobaru: {
                required: "Gambar Tidak Boleh Kosong",
              },
              detail_promo:{
                required: "Detail Tidak Boleh Kosong",
              },
              id_status_promo: {
                required: "Status Promo Tidak Boleh Kosong",
              },
              id_tipe_promo : {
                required: "Tipe Promo Tidak Boleh Kosong",
              }
            },
            submitHandler: function(form) {
              $('#id_status_promo').prop('disabled', false);
              $('#loader').show(300); 
              $('#ceksub').prop('disabled', true);
              form.submit();
            },
            errorElement: 'span',
            errorPlacement: function (error, element) {
              error.addClass('invalid-feedback');
              element.closest('.form-group').append(error);
            },
            highlight: function (element, errorClass, validClass) {
              $(element).addClass('is-invalid');
            },
            unhighlight: function (element, errorClass, validClass) {
              $(element).removeClass('is-invalid');
            },
            

          });
        });


    </script>
 </div>
		</div>
	</section>
</div>
<script type="text/javascript">
  var interval = $('#id_status_promo option').clone();
$('#id_tipe_promo').on('change', function() {
    var val = this.value;
     $('#id_status_promo').prop("disabled", false);
 $('#id_status_promo').html( 
            interval.filter(function() { 
            if($(this).attr('nama')==val){
               return ($(this).attr('nama')==val);
               }else{
               return ($(this).attr('nama')==-1);
               }
            })
        );

});
 $(function () {
    // Summernote
    $('.textarea').summernote()
  })

</script>