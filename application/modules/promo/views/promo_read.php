<div class="row">
	<section class="col-lg connectedSortable ui-sortable">
		<div class="card card-info">
			<div class="card-header">
				<div class="card-title">
					<?php echo $title_card ?>
				</div>
			</div>
			<div class="card-body">
				<div class="tab-content p-0">

					<div class="row">
						<div class="col-md-8">
							<table class="table table-bordered table-striped table-condensed table-sm" style="margin-bottom: 20px">
								<tr>
									<td class="font-weight-bold pl-3">Judul</td>
									<td><?php echo $judul_promo; ?></td>
								</tr>
								<tr>
									<td class="font-weight-bold pl-3">Detail</td>
									<td><?php echo $detail_promo; ?></td>
								</tr>
								<tr>
									<td class="font-weight-bold pl-3">Tipe Promo</td>
									<td><?php if ($id_tipe_promo == 1) echo "Info pl-3";
										else echo "Promo" ?></td>
								</tr>
								<tr>
									<td class="font-weight-bold pl-3">Keterangan</td>
									<td><?php echo $ket_status ?></td>
								</tr>
								<tr>
									<td class="font-weight-bold pl-3">Dibuat Tanggal</td>
									<td><?php
										if ($created_at == null) {
											echo '<span class="badge badge-danger">Kosong</span>';
										} else {
											echo fulldate($created_at);
										}  ?></td>
								</tr>

								<!-- <tr><td><a href="<?php echo site_url('promo') ?>" class="btn btn-primary">Batal</a></td></tr> -->
							</table>
						</div>
						<div class="col-md-4">
							<div class="row justify-content-center">
								<label class="text-center">Gambar Promo</label>
							</div>
							<div class="row justify-content-center">
								<?php
								if ($gambar_promo == null) {
									echo '<span class="badge badge-danger">Kosong</span>';
								} else { ?>
									<a href="<?php echo $gambar_promo ?>" data-toggle="lightbox" data-max-width="800" data-title="Gambar Promo">
										<img src="<?php echo $gambar_promo ?>" class="img-fluid rounded img-fluid" style="width: 250px;" alt="Gambar Promo">
									</a>
								<?php	}
								?>
							</div>
						</div>
					</div>

					<div>
						<a href="<?php echo site_url('promo') ?>" class="btn btn-info" style="width: 100px;">Kembali</a>
					</div>


				</div>
			</div>
	</section>
</div>

<!-- Ekko Lightbox -->
<script src="<?= base_url('assets/plugins/ekko-lightbox/ekko-lightbox.min.js') ?>"></script>
<script>
	$(function() {
		$(document).on('click', '[data-toggle="lightbox"]', function(event) {
			event.preventDefault();
			$(this).ekkoLightbox({
				alwaysShowClose: true
			});
		});

		$('.filter-container').filterizr({
			gutterPixels: 3
		});
		$('.btn[data-filter]').on('click', function() {
			$('.btn[data-filter]').removeClass('active');
			$(this).addClass('active');
		});
	})
</script>