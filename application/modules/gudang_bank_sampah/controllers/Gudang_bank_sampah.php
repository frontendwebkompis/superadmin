<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Gudang_bank_sampah extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        if (cek_token()) {
            $this->load->model('Gudang_bank_sampah_model');
            $this->load->library('form_validation');
        } else {
            logout();
        }
    }

    public function index()
    {
        $q = urldecode($this->input->get('q', TRUE));
        $start = intval($this->input->get('start'));

        if ($q <> '') {
            $config['base_url'] = base_url() . 'gudang_bank_sampah?q=' . urlencode($q);
            $config['first_url'] = base_url() . 'gudang_bank_sampah?q=' . urlencode($q);
        } else {
            $config['base_url'] = base_url() . 'gudang_bank_sampah';
            $config['first_url'] = base_url() . 'gudang_bank_sampah';
        }

        $config['per_page']             = 10;
        $config['page_query_string']    = TRUE;
        $config['total_rows']           = $this->Gudang_bank_sampah_model->total_rows($q);
        $gudang_bank_sampah             = $this->Gudang_bank_sampah_model->get_limit_data($config['per_page'], $start, $q);

        $this->load->library('pagination');
        $this->pagination->initialize($config);

        $data = array(
            'titleCard'                 => 'List Gudang Bank Sampah',
            'tittle'                    => 'Gudang Bank Sampah',
            'judul'                     => 'Gudang Bank Sampah',
            'breadcrumbactive'          => '',
            'breadcrumb'                => 'Gudang Bank Sampah',
            'link'                      => '',
            'gudang_bank_sampah_data'   => $gudang_bank_sampah,
            'q'                         => $q,
            'pagination'                => $this->pagination->create_links(),
            'total_rows'                => $config['total_rows'],
            'start'                     => $start,
            'action'                    => site_url('gudang_bank_sampah/create_action'),
            'kategori_aktif'            => '1',
            'menu_aktif'                => 'gudang_bank_sampah'
        );
        $data['datakonten'] = $this->load->view('Gudang_bank_sampah_list', $data, true);
        $this->load->view('layouts/main_view', $data);
    }

    public function gudang($id = null)
    {
        $this->db->where(array('md5(id_bank_sampah) =' => $id));
        $data = $this->db->get('bank_sampah')->row();
        if ($data) {
            $q      = urldecode($this->input->get('q', TRUE));
            $start  = intval($this->input->get('start'));

            if ($q <> '') {
                $config['base_url'] = base_url() . 'gudang_bank_sampah/gudang/' . $id . '?q=' . urlencode($q);
                $config['first_url'] = base_url() . 'gudang_bank_sampah/gudang/' . $id . '?q=' . urlencode($q);
            } else {
                $config['base_url'] = base_url() . 'gudang_bank_sampah/gudang/' . $id . '';
                $config['first_url'] = base_url() . 'gudang_bank_sampah/gudang/' . $id . '';
            }

            $config['per_page']             = 10;
            $config['page_query_string']    = TRUE;
            $config['total_rows']           = $this->Gudang_bank_sampah_model->total_rows_gudang($id, $q);
            $gudang_bank_sampah             = $this->Gudang_bank_sampah_model->get_limit_data_gudang($id, $config['per_page'], $start, $q);

            $this->load->library('pagination');
            $this->pagination->initialize($config);

            $data = array(
                'titleCard'                 => 'List Gudang Bank Sampah',
                'tittle'                    => 'Gudang Bank Sampah',
                'judul'                     => 'Gudang Bank Sampah',
                'breadcrumbactive'          => '',
                'breadcrumb'                => 'Gudang Bank Sampah',
                'link'                      => '',
                'gudang_bank_sampah_data'   => $gudang_bank_sampah,
                'q'                         => $q,
                'pagination'                => $this->pagination->create_links(),
                'total_rows'                => $config['total_rows'],
                'start'                     => $start,
                'action'                    => site_url('gudang_bank_sampah/create_action'),
                'kategori_aktif'            => '1',
                'menu_aktif'                => 'gudang_bank_sampah',
                'id_bank_sampah'            => $id,

            );
            $data['datakonten'] = $this->load->view('Gudang_bank_sampah', $data, true);
            $this->load->view('layouts/main_view', $data);
        } else {
            $this->session->set_flashdata('gagal', 'Data tidak ditemukan');
            redirect('gudang_bank_sampah');
        }
    }

    public function update_action($id_bank_sampah = null, $id = null)
    {
        $model =  $this->Gudang_bank_sampah_model;
        $this->form_validation->set_rules('stok', 'stok', 'trim|required|is_natural');

        if ($this->form_validation->run() == FALSE) {
            $this->session->set_flashdata('gagal', 'Isi stok dengan benar');
            $this->gudang($id);
        } else {
            $stok = $this->input->post('stok');
            $data = [
                'stok' => $stok,
            ];
            $model->update($id, $data);
            $this->session->set_flashdata('berhasil', 'Data berhasil diubah');
            redirect('gudang_bank_sampah/gudang/' . $id_bank_sampah);
        }
    }

    public function get_gudang_bank_sampah($id = null)
    {
        $this->db->where('id_gudang_banksampah', $id);
        $data = $this->db->get('gudang_banksampah')->row();
        echo json_encode($data);
    }
}

/* End of file versi_app.php */
/* Location: ./application/controllers/versi_app.php */
/* Please DO NOT modify this information : */
/* Generated by Harviacode Codeigniter CRUD Generator 2020-07-15 09:43:37 */
/* http://harviacode.com */