<div class="card card-<?= bgCard() ?>">
    <div class="card-header">
        <div class="card-title">
            <?= $titleCard ?>
        </div>
    </div>
    <div class="card-body">
        <div class="tab-content p-0">
            <div class="row">
                <div class="col-md-3 offset-md-9 mb-2">
                    <?= search(site_url('gudang_bank_sampah'), site_url('gudang_bank_sampah'), $q) ?>
                </div>
            </div>
            <div class="tab-content p-0" style="overflow:auto">
                <table class="table table-bordered table-striped table-condensed table-hover table-sm" style="margin-bottom: 10px">
                    <tr>
                        <th class='text-center' style="width: 60px;">No</th>
                        <th class='text-center'>Nama Bank Sampah</th>
                        <th class='text-center'>Email</th>
                        <th class='text-center'>No HP</th>
                        <th class='text-center'>Alamat</th>
                        <th class='text-center' style="width: 100px;">Aksi</th>
                    </tr><?php
                    if ($total_rows == 0) {
                        echo '<tr><td colspan="11" bgcolor="grey" style="color: white; font-weight: bold; text-align: center;">Data Tidak Ditemukan</td></tr>';
                    } else {
                        foreach ($gudang_bank_sampah_data as $data)
                        {
                            ?>
                    <tr>
                        <td class='text-center'><?php echo ++$start ?></td>
                        <td><?php echo $data->nama_bank_sampah ?></td>
                        <td><?php echo $data->email_bank_sampah ?></td>
                        <td><?php echo $data->no_hp_bank_sampah ?></td>
                        <td><?php echo $data->alamat_bank_sampah ?></td>
                        <td class='text-center'>
                        <div class="btn-group btn-group-sm">
                            <a href="<?= base_url('gudang_bank_sampah/gudang/'.$data->id_bank_sampah) ?>" class="btn btn-<?= bgCard() ?>" title="Gudang Bank Sampah"><i class="fas fa-info-circle"></i></a>
                        </div>
                          
                        </td>
                    </tr>
                            <?php
                        }
                    }
                        ?>
                </table>
            </div>
            <?= footer($total_rows, $pagination, '') ?>
        </div>
    </div>
</div>

<div id="modalCreateMetode" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-md" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Form Tambah Ekspedisi</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form action="<?= $action ?>" method="post" id="formTambahMetode" role="form" enctype="multipart/form-data">
                <div class="row ml-3 mr-3">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="varchar">Nama Ekspedisi</label>
                            <input type="text" class="form-control" name="metode_pembayaran" id="metode_pembayaran" placeholder="Metode Pembayaran" value="" />
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <div class="col-md-12 text-center">
                    <button type="submit" class="btn btn-<?= bgCard() ?>" style="width: 100px;">Simpan</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal" style="width: 100px;">Batal</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="modalEditMetode" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Form Tambah Metode Pembayaran</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form action="<?= $action ?>" method="post" id="formTambahMetode" role="form" enctype="multipart/form-data">
                <div class="row ml-3 mr-3">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="varchar">Nama Metode Pembayaran</label>
                            <input type="text" class="form-control" name="metode_pembayaran" id="metode_pembayaran" placeholder="Metode Pembayaran" value="" />
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <div class="col-md-12 text-center">
                    <button type="submit" class="btn btn-<?= bgCard() ?>" style="width: 100px;">Simpan</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal" style="width: 100px;">Batal</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- <div id="modalEditversi" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Form Edit versi</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form action="<?= $action ?>" method="post" id="formEditversi" role="form" enctype="multipart/form-data">
                <div class="row ml-3 mr-3">
                    <div class="col-md-12">
                       <div class="form-group">
                            <label for="varchar">Jenis Aplikasi</label>
                            <select name="status_aplikasi" id="status_aplikasi" class="form-control" required>
                                <option value="">Pilih Jenis Aplikasi</option>
                                <option value=0>Bank Sampah</option>
                                <option value=1>Nasabah</option>
                                <option value=2>Pengepul</option>
                              
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="varchar">Versi</label>
                            <input type="text" class="form-control" name="versi" id="versi" placeholder="Versi" value="" />
                        </div>
                         <div class="form-group">
                            <label for="varchar">Keterangan Versi</label>
                            <input type="text" class="form-control" name="keterangan_versi" id="keterangan_versi" placeholder="Keterangan Versi" value="" />
                        </div> 
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <div class="col-md-12 text-center">
                    <button type="submit" class="btn btn-<?= bgCard() ?>" style="width: 100px;">Simpan</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal" style="width: 100px;">Batal</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    $(function(){
        $('#modalCreateversi #formTambahversi').validate({
            rules: {
                status_aplikasi : {
                    required : true
                },
                keterangan_versi : {
                    required : true
                },
                 versi : {
                    required : true
                }
            },
            messages: {
                status_aplikasi : {
                    required : "Nama versi Tidak Boleh Kosong"
                },
                keterangan_versi : {
                    required : "Jenis Produk Tidak Boleh Kosong"
                },
                versi : {
                    required : "Versi Tidak Boleh Kosong"
                }
            },
            errorElement: 'span',
            errorPlacement: function(error, element) {
                error.addClass('invalid-feedback');
                element.closest('.form-group').append(error);
            },
            highlight: function(element, errorClass, validClass) {
                $(element).addClass('is-invalid');
            },
            unhighlight: function(element, errorClass, validClass) {
                $(element).removeClass('is-invalid');
            }
        })
        $('#modalEditversi #formEditversi').validate({
            rules: {
                status_aplikasi : {
                    required : true
                },
                keterangan_versi : {
                    required : true
                },
                versi : {
                    required : true
                }
            },
            messages: {
                status_aplikasi : {
                    required : "Nama versi Tidak Boleh Kosong"
                },
                keterangan_versi : {
                    required : "Jenis Kendaraan Tidak Boleh Kosong"
                },
                versi : {
                    required : "Plat Kendaraan Tidak Boleh Kosong"
                }
            },
            errorElement: 'span',
            errorPlacement: function(error, element) {
                error.addClass('invalid-feedback');
                element.closest('.form-group').append(error);
            },
            highlight: function(element, errorClass, validClass) {
                $(element).addClass('is-invalid');
            },
            unhighlight: function(element, errorClass, validClass) {
                $(element).removeClass('is-invalid');
            }
        })

        $('#modalCreateversi #keterangan_versi').select2({
            theme: 'bootstrap'
        })

        $('#modalEditversi #edit_keterangan_versi').select2({
            theme: 'bootstrap'
        })
    })

    $('#modalCreateversi').on('hidden.bs.modal', function(){
        $('#modalCreateversi #status_aplikasi').val('')
        $('#modalCreateversi #status_aplikasi').removeClass('is-invalid')
        $('#modalCreateversi #keterangan_versi').val('').trigger('change')
        $('#modalCreateversi #keterangan_versi').removeClass('is-invalid')
        $('#modalCreateversi #versi').val('')
        $('#modalCreateversi #versi').removeClass('is-invalid')
    })

    $('#modalEditversi').on('hidden.bs.modal', function(){
        $('#modalEditversi #status_aplikasi').val('')
        $('#modalEditversi #status_aplikasi').removeClass('is-invalid')
        $('#modalEditversi #edit_keterangan_versi').val('').trigger('change')
        $('#modalEditversi #edit_keterangan_versi').removeClass('is-invalid')
        $('#modalEditversi #versi').val('')
        $('#modalEditversi #versi').removeClass('is-invalid')
    })

    function cekEditversi(id){
        startloading('#modalEditversi .modal-content')
        $('#modalEditversi #formEditversi').attr('action', '<?= base_url('versi_app/update_action/') ?>'+id)
        $.ajax({
            url: '<?php echo base_url() ?>versi_app/ambilDataversi/' + id,
            dataType: "json",
            cache: false,
            success: function(res) {
                $('#modalEditversi #status_aplikasi').val(res.status_aplikasi)
                $('#modalEditversi #edit_keterangan_versi').val(res.id_keterangan_versi).trigger('change')
                $('#modalEditversi #keterangan_versi').val(res.keterangan_versi)
                $('#modalEditversi #versi').val(res.versi)
                stoploading('#modalEditversi .modal-content')
            }
        });
    }

    function cekNonAktifVersi(id){
        swal({
              title: 'Yakin mengaktifkan Versi?',
                text: "Mengaktifkan Versi Berimbas Pada Aplikasi!",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#CD5C5C',
                cancelButtonColor: '#d33',
                cancelButtonText: 'Batal',
                confirmButtonText: 'Non Aktifkan',
                closeOnConfirm: false,
            },
            function(isConfirm) {
                if (isConfirm) {
                    window.location.href = "<?php echo site_url('versi_app/nonaktif/'); ?>" + id;
                } else {

                }
            });
    } 
     function cekAktifVersi(id){
        swal({
                title: 'Yakin mengaktifkan Versi?',
                text: "Mengaktifkan Versi Berimbas Pada Aplikasi!",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#CD5C5C',
                cancelButtonColor: '#d33',
                cancelButtonText: 'Batal',
                confirmButtonText: 'Aktifkan',
                closeOnConfirm: false,
            },
            function(isConfirm) {
                if (isConfirm) {
                    window.location.href = "<?php echo site_url('versi_app/aktif/'); ?>" + id;
                } else {

                }
            });
    }  
    function cekDeleteVersi(id){
        swal({
                title: 'Yakin menghapus data?',
                text: "Data yang sudah terhapus tidak dapat dikembalikan!",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#CD5C5C',
                cancelButtonColor: '#d33',
                cancelButtonText: 'Batal',
                confirmButtonText: 'Hapus',
                closeOnConfirm: false,
            },
            function(isConfirm) {
                if (isConfirm) {
                    window.location.href = "<?php echo site_url('versi_app/delete/'); ?>" + id;
                } else {

                }
            });
    }
</script> -->