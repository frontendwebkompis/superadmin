<!doctype html>
<html>
    <head>
        <title>Crud Data Tb_superadmin</title>
        <link rel="stylesheet" href="<?php echo base_url('assets/bootstrap/css/bootstrap.min.css') ?>"/>
        <style>
            body{
                padding: 15px;
            }
        </style>
    </head>
    <body>
        <h2 style="margin-top:0px"> List Tb_superadmin</h2>
        <div class="row" style="margin-bottom: 10px">
            <div class="col-md-4">
                <?php echo anchor(site_url('tb_superadmin/create'),'Tambah Data', 'class="btn btn-primary"'); ?>
            </div>
            <div class="col-md-4 text-center">
                <div style="margin-top: 8px" id="message">
                    <?php echo $this->session->userdata('message') <> '' ? $this->session->userdata('message') : ''; ?>
                </div>
            </div>
            <div class="col-md-1 text-right">
            </div>
            <div class="col-md-3 text-right">
                <form action="<?php echo site_url('tb_superadmin/index'); ?>" class="form-inline" method="get">
                    <div class="input-group">
                        <input type="text" class="form-control" name="q" value="<?php echo $q; ?>">
                        <span class="input-group-btn">
                            <?php 
                                if ($q <> '')
                                {
                                    ?>
                                    <a href="<?php echo site_url('tb_superadmin'); ?>" class="btn btn-default">Reset</a>
                                    <?php
                                }
                            ?>
                          <button class="btn btn-primary" type="submit">Pencarian</button>
                        </span>
                    </div>
                </form>
            </div>
        </div>
        <table class="table table-bordered" style="margin-bottom: 10px">
            <tr>
                <th>No</th>
		<th>Username</th>
		<th>Password</th>
		<th>Email</th>
		<th>Nama Admin</th>
		<th>Last Login</th>
		<th>Dibuat Tgl</th>
		<th style="text-align:center">Aksi</th>
            </tr><?php
            foreach ($tb_superadmin_data as $tb_superadmin)
            {
                ?>
                <tr>
			<td width="80px"><?php echo ++$start ?></td>
			<td><?php echo $tb_superadmin->username ?></td>
			<td><?php echo $tb_superadmin->password ?></td>
			<td><?php echo $tb_superadmin->email ?></td>
			<td><?php echo $tb_superadmin->nama_admin ?></td>
			<td><?php echo $tb_superadmin->last_login ?></td>
			<td><?php echo $tb_superadmin->dibuat_tgl ?></td>
			<td style="text-align:center" width="200px">
				<?php 
				echo anchor(site_url('tb_superadmin/detail/'.$tb_superadmin->id_super),'Lihat'); 
				echo ' | '; 
				echo anchor(site_url('tb_superadmin/update/'.$tb_superadmin->id_super),'Update'); 
				echo ' | '; 
				echo anchor(site_url('tb_superadmin/delete/'.$tb_superadmin->id_super),'Hapus','onclick="javasciprt: return confirm(\'Yakin Anda ingin Menghapus ini ?\')"'); 
				?>
			</td>
		</tr>
                <?php
            }
            ?>
        </table>
        <?= footer($total_rows, $pagination, '') ?>
    </body>
</html>