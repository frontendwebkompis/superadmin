<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Superadmin extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Tb_superadmin_model');
        $this->load->library('form_validation');
    }

    public function index()
    {
        $q = urldecode($this->input->get('q', TRUE));
        $start = intval($this->input->get('start'));
        
        if ($q <> '') {
            $config['base_url'] = base_url() . 'tb_superadmin/index.html?q=' . urlencode($q);
            $config['first_url'] = base_url() . 'tb_superadmin/index.html?q=' . urlencode($q);
        } else {
            $config['base_url'] = base_url() . 'tb_superadmin/index.html';
            $config['first_url'] = base_url() . 'tb_superadmin/index.html';
        }

        $config['per_page'] = 10;
        $config['page_query_string'] = TRUE;
        $config['total_rows'] = $this->Tb_superadmin_model->total_rows($q);
        $tb_superadmin = $this->Tb_superadmin_model->get_limit_data($config['per_page'], $start, $q);

        $this->load->library('pagination');
        $this->pagination->initialize($config);

        $data = array(
            'tb_superadmin_data' => $tb_superadmin,
            'q' => $q,
            'pagination' => $this->pagination->create_links(),
            'total_rows' => $config['total_rows'],
            'start' => $start,
        );
        $this->load->view('tb_superadmin/tb_superadmin_list', $data);
    }

    public function detail($id) 
    {
        $row = $this->Tb_superadmin_model->get_by_id($id);
        if ($row) {
            $data = array(
		'id_super' => $row->id_super,
		'username' => $row->username,
		'password' => $row->password,
		'email' => $row->email,
		'nama_admin' => $row->nama_admin,
		'last_login' => $row->last_login,
		'dibuat_tgl' => $row->dibuat_tgl,
	    );
            $this->load->view('tb_superadmin/tb_superadmin_detail', $data);
        } else {
            $this->session->set_flashdata('message', '<div class="alert alert-warning" role="alert">
  Data Tidak Di Temukan
</div>');
            redirect(site_url('tb_superadmin'));
        }
    }

    public function create() 
    {
        $data = array(
            'button' => 'Create',
            'action' => site_url('tb_superadmin/create_action'),
	    'id_super' => set_value('id_super'),
	    'username' => set_value('username'),
	    'password' => set_value('password'),
	    'email' => set_value('email'),
	    'nama_admin' => set_value('nama_admin'),
	    'last_login' => set_value('last_login'),
	    'dibuat_tgl' => set_value('dibuat_tgl'),
	);
        $this->load->view('tb_superadmin/tb_superadmin_form', $data);
    }
    
    public function create_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->create();
        } else {
            $data = array(
		'username' => $this->input->post('username',TRUE),
		'password' => $this->input->post('password',TRUE),
		'email' => $this->input->post('email',TRUE),
		'nama_admin' => $this->input->post('nama_admin',TRUE),
		'last_login' => $this->input->post('last_login',TRUE),
		'dibuat_tgl' => $this->input->post('dibuat_tgl',TRUE),
	    );

            $this->Tb_superadmin_model->insert($data);
            $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">Berhasil Menambahkan Data </div>');
            redirect(site_url('tb_superadmin'));
        }
    }
    
    public function update($id) 
    {
        $row = $this->Tb_superadmin_model->get_by_id($id);

        if ($row) {
            $data = array(
                'button' => 'Update',
                'action' => site_url('tb_superadmin/update_action'),
		'id_super' => set_value('id_super', $row->id_super),
		'username' => set_value('username', $row->username),
		'password' => set_value('password', $row->password),
		'email' => set_value('email', $row->email),
		'nama_admin' => set_value('nama_admin', $row->nama_admin),
		'last_login' => set_value('last_login', $row->last_login),
		'dibuat_tgl' => set_value('dibuat_tgl', $row->dibuat_tgl),
	    );
            $this->load->view('tb_superadmin/tb_superadmin_form', $data);
        } else {
            $this->session->set_flashdata('message', '<div class="alert alert-warning" role="alert">Data Tidak Ditemukan</div>');
            redirect(site_url('tb_superadmin'));
        }
    }
    
    public function update_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->update($this->input->post('id_super', TRUE));
        } else {
            $data = array(
		'username' => $this->input->post('username',TRUE),
		'password' => $this->input->post('password',TRUE),
		'email' => $this->input->post('email',TRUE),
		'nama_admin' => $this->input->post('nama_admin',TRUE),
		'last_login' => $this->input->post('last_login',TRUE),
		'dibuat_tgl' => $this->input->post('dibuat_tgl',TRUE),
	    );

            $this->Tb_superadmin_model->update($this->input->post('id_super', TRUE), $data);
            $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">Berhasil Merubah Data</div>');
            redirect(site_url('tb_superadmin'));
        }
    }
    
    public function delete($id) 
    {
        $row = $this->Tb_superadmin_model->get_by_id($id);

        if ($row) {
            $this->Tb_superadmin_model->delete($id);
            $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">Berhasil Menghapus Data</div>');
            redirect(site_url('tb_superadmin'));
        } else {
            $this->session->set_flashdata('message', '<div class="alert alert-warning" role="alert">Data Tidak Ditemukan</div>');
            redirect(site_url('tb_superadmin'));
        }
    }

    public function _rules() 
    {
	$this->form_validation->set_rules('username', 'username', 'trim|required');
	$this->form_validation->set_rules('password', 'password', 'trim|required');
	$this->form_validation->set_rules('email', 'email', 'trim|required');
	$this->form_validation->set_rules('nama_admin', 'nama admin', 'trim|required');
	$this->form_validation->set_rules('last_login', 'last login', 'trim|required');
	$this->form_validation->set_rules('dibuat_tgl', 'dibuat tgl', 'trim|required');

	$this->form_validation->set_rules('id_super', 'id_super', 'trim');
	$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

}

/* End of file Tb_superadmin.php */
/* Location: ./application/controllers/Tb_superadmin.php */
/* Please DO NOT modify this information : */
/* Generated by Harviacode Codeigniter CRUD Generator 2020-02-12 08:42:32 */
/* http://harviacode.com */