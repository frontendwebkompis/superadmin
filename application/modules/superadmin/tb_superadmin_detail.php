<!doctype html>
<html>
    <head>
        <title>Detail Tb_superadmin</title>
        <link rel="stylesheet" href="<?php echo base_url('assets/bootstrap/css/bootstrap.min.css') ?>"/>
        <style>
            body{
                padding: 15px;
            }
        </style>
    </head>
    <body>
        <h2 style="margin-top:0px">Tb_superadmin Detail</h2>
        <table class="table">
	    <tr><td>Username</td><td><?php echo $username; ?></td></tr>
	    <tr><td>Password</td><td><?php echo $password; ?></td></tr>
	    <tr><td>Email</td><td><?php echo $email; ?></td></tr>
	    <tr><td>Nama Admin</td><td><?php echo $nama_admin; ?></td></tr>
	    <tr><td>Last Login</td><td><?php echo $last_login; ?></td></tr>
	    <tr><td>Dibuat Tgl</td><td><?php echo $dibuat_tgl; ?></td></tr>
	    <tr><td></td><td><a href="<?php echo site_url('tb_superadmin') ?>" class="btn btn-default">Batal</a></td></tr>
	</table>
        </body>
</html>