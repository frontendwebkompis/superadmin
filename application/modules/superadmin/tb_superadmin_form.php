<!doctype html>
<html>
    <head>
        <title>Tambah Tb_superadmin</title>
        <link rel="stylesheet" href="<?php echo base_url('assets/bootstrap/css/bootstrap.min.css') ?>"/>
        <style>
            body{
                padding: 15px;
            }
        </style>
    </head>
    <body>
        <h2 style="margin-top:0px">Pengolahan Data Tb_superadmin </h2>
        <form action="<?php echo $action; ?>" method="post">
	    <div class="form-group">
            <label for="varchar">Username <?php echo form_error('username') ?></label>
            <input type="text" class="form-control" name="username" id="username" placeholder="Username" value="<?php echo $username; ?>" />
        </div>
	    <div class="form-group">
            <label for="varchar">Password <?php echo form_error('password') ?></label>
            <input type="text" class="form-control" name="password" id="password" placeholder="Password" value="<?php echo $password; ?>" />
        </div>
	    <div class="form-group">
            <label for="varchar">Email <?php echo form_error('email') ?></label>
            <input type="text" class="form-control" name="email" id="email" placeholder="Email" value="<?php echo $email; ?>" />
        </div>
	    <div class="form-group">
            <label for="varchar">Nama Admin <?php echo form_error('nama_admin') ?></label>
            <input type="text" class="form-control" name="nama_admin" id="nama_admin" placeholder="Nama Admin" value="<?php echo $nama_admin; ?>" />
        </div>
	    <div class="form-group">
            <label for="datetime">Last Login <?php echo form_error('last_login') ?></label>
            <input type="text" class="form-control" name="last_login" id="last_login" placeholder="Last Login" value="<?php echo $last_login; ?>" />
        </div>
	    <div class="form-group">
            <label for="datetime">Dibuat Tgl <?php echo form_error('dibuat_tgl') ?></label>
            <input type="text" class="form-control" name="dibuat_tgl" id="dibuat_tgl" placeholder="Dibuat Tgl" value="<?php echo $dibuat_tgl; ?>" />
        </div>
	    <input type="hidden" name="id_super" value="<?php echo $id_super; ?>" /> 
	    <button type="submit" class="btn btn-primary">Simpan
</button> 
	    <a href="<?php echo site_url('tb_superadmin') ?>" class="btn btn-default">Batal</a>
	</form>
    </body>
</html>