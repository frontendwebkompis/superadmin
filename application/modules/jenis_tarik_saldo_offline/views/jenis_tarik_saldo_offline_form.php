<div class="row">
	<!-- Left col -->
	<section class="col-lg connectedSortable ui-sortable">
		<!-- Custom tabs (Charts with tabs)-->
		<div class="card card-info">
			<div class="card-header">
                <div class="card-title">
                  <?php echo $title_card?>
                </div>
              </div>
			<div class="card-body">
				<div class="tab-content p-0">
                
        <form action="<?php echo $action; ?>" method="post" id="dataform">
			<div class="form-group">
				<label for="varchar">Nama Jenis Tarik Saldo Offline <?php echo form_error('nama_jenis_tarik_saldo_offline') ?></label>
				<input type="text" class="form-control" name="nama_jenis_tarik_saldo_offline" id="nama_jenis_tarik_saldo_offline" placeholder="Nama Jenis Tarik Saldo Offline" value="<?php echo $nama_jenis_tarik_saldo_offline; ?>" />
			</div>
			<input type="hidden" name="id_jenis_tarik_saldo_offline" value="<?php echo $id_jenis_tarik_saldo_offline; ?>" /> 
			<button type="submit" class="btn btn-info" style="width: 100px;">Simpan</button> 
			<a href="<?php echo site_url('jenis_tarik_saldo_offline') ?>" class="btn btn-default" style="width: 100px;">Batal</a>
		</form>

    	</div><!-- /.card-body -->
		</div>
		<!-- /.card -->
	</section>
	<!-- /.Left col -->
</div>

<script type="text/javascript">
	$(document).ready(function () {
      $('#dataform').validate({
        rules: {
          nama_jenis_tarik_saldo_offline: {
          	required: true,
          }
        },
        messages: {
          nama_jenis_tarik_saldo_offline: {
          	required: "Nama Jenis Tarik Saldo Tidak Boleh Kosong",
          }
        },
        submitHandler: function(form) {
          $('#btn-simpan').prop('disabled', true);
          form.submit();
        },
        errorElement: 'span',
        errorPlacement: function (error, element) {
          error.addClass('invalid-feedback');
          element.closest('.form-group').append(error);
        },
        highlight: function (element, errorClass, validClass) {
          $(element).addClass('is-invalid');
        },
        unhighlight: function (element, errorClass, validClass) {
          $(element).removeClass('is-invalid');
        },
      });
    });
</script>