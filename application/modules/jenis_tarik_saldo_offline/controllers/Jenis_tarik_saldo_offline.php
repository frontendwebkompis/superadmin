<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Jenis_tarik_saldo_offline extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        if (cek_token()) {
            $this->load->model('Jenis_tarik_saldo_offline_model');
            $this->load->library('form_validation');
        } else {
            logout();
        }
    }

    public function index()
    {
        $q = urldecode($this->input->get('q', TRUE));
        $start = intval($this->input->get('start'));

        if ($q <> '') {
            $config['base_url'] = base_url() . 'jenis_tarik_saldo_offline?q=' . urlencode($q);
            $config['first_url'] = base_url() . 'jenis_tarik_saldo_offline?q=' . urlencode($q);
        } else {
            $config['base_url'] = base_url() . 'jenis_tarik_saldo_offline';
            $config['first_url'] = base_url() . 'jenis_tarik_saldo_offline';
        }

        $config['per_page']             = 10;
        $config['page_query_string']    = TRUE;
        $config['total_rows']           = $this->Jenis_tarik_saldo_offline_model->total_rows($q);
        $jenis_tarik_saldo_offline      = $this->Jenis_tarik_saldo_offline_model->get_limit_data($config['per_page'], $start, $q);

        $this->load->library('pagination');
        $this->pagination->initialize($config);

        $data = array(
            'tittle'                            => 'Tarik Saldo Offline',
            'judul'                             => 'Tarik Saldo Offline',
            'title_card'                        => 'Jenis Tarik Saldo Offline',
            'jenis_tarik_saldo_offline_data'    => $jenis_tarik_saldo_offline,
            'q'                                 => $q,
            'pagination'                        => $this->pagination->create_links(),
            'total_rows'                        => $config['total_rows'],
            'start'                             => $start,
            'tarik_saldo_offline_aktif'         => '1',
        );
        $res['datakonten'] = $this->load->view('jenis_tarik_saldo_offline/jenis_tarik_saldo_offline_list', $data, true);
        $this->load->view('layouts/main_view', $res);
    }

    public function read($id)
    {
        $row = $this->Jenis_tarik_saldo_offline_model->get_by_id($id);
        if ($row) {
            $data = array(
                'tittle'                            => 'Jenis Tarik Saldo',
                'judul'                             => 'Jenis Tarik Saldo Offline',
                'title_card'                        => 'Jenis Tarik Saldo Offline',
                'id_jenis_tarik_saldo_offline'      => $row->id_jenis_tarik_saldo_offline,
                'nama_jenis_tarik_saldo_offline'    => $row->nama_jenis_tarik_saldo_offline,
                'tarik_saldo_offline_aktif'         => '1',
            );
            $res['datakonten'] = $this->load->view('jenis_tarik_saldo_offline/jenis_tarik_saldo_offline_read', $data, true);
            $this->load->view('layouts/main_view', $res);
        } else {
            $this->session->set_flashdata('gagal', 'Data Tidak Di Temukan');
            redirect(site_url('jenis_tarik_saldo_offline'));
        }
    }

    public function create()
    {
        $data = array(
            'tittle'                            => 'Tarik Saldo Offline',
            'judul'                             => 'Tarik Saldo Offline',
            'title_card'                        => 'Tambah Tarik Saldo Offline',
            'button'                            => 'Create',
            'action'                            => site_url('jenis_tarik_saldo_offline/create_action'),
            'id_jenis_tarik_saldo_offline'      => set_value('id_jenis_tarik_saldo_offline'),
            'nama_jenis_tarik_saldo_offline'    => set_value('nama_jenis_tarik_saldo_offline'),
            'tarik_saldo_offline_aktif'         => '1',
        );
        $res['datakonten'] = $this->load->view('jenis_tarik_saldo_offline/jenis_tarik_saldo_offline_form', $data, true);
        $this->load->view('layouts/main_view', $res);
    }

    public function create_action()
    {
        $this->_rules();
        $prefix = "JTS0";
        // echo $gabung;
        $row = $this->Jenis_tarik_saldo_offline_model->get_by_prefix($prefix);
        $NoUrut = (int) substr($row->max, 4, 4);
        $NoUrut = $NoUrut + 1; //nomor urut +1
        $NoUrut = sprintf('%04d', $NoUrut);
        $fix = $prefix . $NoUrut;
        if ($this->form_validation->run() == FALSE) {
            $this->create();
        } else {
            $data = array(
                'id_jenis_tarik_saldo_offline'              => $fix,
                'nama_jenis_tarik_saldo_offline'            => $this->input->post('nama_jenis_tarik_saldo_offline', TRUE),
                'id_superadmin'                             => $this->session->userdata('uid'),
                'status_jenis_tarik_saldo_offline_remove'   => 1,
            );

            $this->Jenis_tarik_saldo_offline_model->insert($data);
            $this->session->set_flashdata('berhasil', 'Berhasil Menambahkan Data');
            redirect(site_url('jenis_tarik_saldo_offline'));
        }
    }

    public function update($id)
    {
        $row = $this->Jenis_tarik_saldo_offline_model->get_by_id($id);

        if ($row) {
            $data = array(
                'tittle'                            => 'Tarik Saldo Offline',
                'judul'                             => 'Tarik Saldo Offline',
                'title_card'                        => 'Ubah Tarik Saldo Offline',
                'button'                            => 'Update',
                'action'                            => site_url('jenis_tarik_saldo_offline/update_action'),
                'id_jenis_tarik_saldo_offline'      => set_value('id_jenis_tarik_saldo_offline', $row->id_jenis_tarik_saldo_offline),
                'nama_jenis_tarik_saldo_offline'    => set_value('nama_jenis_tarik_saldo_offline', $row->nama_jenis_tarik_saldo_offline),
                'tarik_saldo_offline_aktif'         => '1',
            );
            $res['datakonten'] = $this->load->view('jenis_tarik_saldo_offline/jenis_tarik_saldo_offline_form', $data, true);
            $this->load->view('layouts/main_view', $res);
        } else {
            $this->session->set_flashdata('gagal', 'Data Tidak Ditemukan');
            redirect(site_url('jenis_tarik_saldo_offline'));
        }
    }

    public function update_action()
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->update($this->input->post('id_jenis_tarik_saldo_offline', TRUE));
        } else {
            $data = array(
                'nama_jenis_tarik_saldo_offline' => $this->input->post('nama_jenis_tarik_saldo_offline', TRUE),
                'id_superadmin' => $this->session->userdata('uid'),
            );

            $this->Jenis_tarik_saldo_offline_model->update($this->input->post('id_jenis_tarik_saldo_offline', TRUE), $data);
            $this->session->set_flashdata('berhasil', 'Berhasil Merubah Data');
            redirect(site_url('jenis_tarik_saldo_offline'));
        }
    }

    public function del_sem($id)
    {
        $row = $this->Jenis_tarik_saldo_offline_model->get_by_id($id);

        if ($row) {
            $data = array(
                'id_superadmin' => $this->session->userdata('uid'),
                'status_jenis_tarik_saldo_offline_remove' => 0,
            );

            $this->Jenis_tarik_saldo_offline_model->update($id, $data);
            $this->session->set_flashdata('berhasil', 'Berhasil Menghapus Data');
            redirect(site_url('jenis_tarik_saldo_offline'));
        } else {
            $this->session->set_flashdata('gagal', 'Data Tidak Ditemukan');
            redirect(site_url('jenis_tarik_saldo_offline'));
        }
    }

    public function _rules()
    {
        $this->form_validation->set_rules('nama_jenis_tarik_saldo_offline', 'nama jenis tarik saldo offline', 'trim|required');
        $this->form_validation->set_rules('id_jenis_tarik_saldo_offline', 'id_jenis_tarik_saldo_offline', 'trim');
        $this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

    public function excel()
    {
        $this->load->helper('exportexcel');
        $namaFile = "jenis_tarik_saldo_offline.xls";
        $judul = "jenis_tarik_saldo_offline";
        $tablehead = 0;
        $tablebody = 1;
        $nourut = 1;
        //penulisan header
        header("Pragma: public");
        header("Expires: 0");
        header("Cache-Control: must-revalidate, post-check=0,pre-check=0");
        header("Content-Type: application/force-download");
        header("Content-Type: application/octet-stream");
        header("Content-Type: application/download");
        header("Content-Disposition: attachment;filename=" . $namaFile . "");
        header("Content-Transfer-Encoding: binary ");

        xlsBOF();

        $kolomhead = 0;
        xlsWriteLabel($tablehead, $kolomhead++, "No");
        xlsWriteLabel($tablehead, $kolomhead++, "Nama Jenis Tarik Saldo Offline");

        foreach ($this->Jenis_tarik_saldo_offline_model->get_all() as $data) {
            $kolombody = 0;

            //ubah xlsWriteLabel menjadi xlsWriteNumber untuk kolom numeric
            xlsWriteNumber($tablebody, $kolombody++, $nourut);
            xlsWriteLabel($tablebody, $kolombody++, $data->nama_jenis_tarik_saldo_offline);

            $tablebody++;
            $nourut++;
        }

        xlsEOF();
        exit();
    }
}

/* End of file Jenis_tarik_saldo_offline.php */
/* Location: ./application/controllers/Jenis_tarik_saldo_offline.php */
/* Please DO NOT modify this information : */
/* Generated by Harviacode Codeigniter CRUD Generator 2020-02-10 04:35:02 */
/* http://harviacode.com */
