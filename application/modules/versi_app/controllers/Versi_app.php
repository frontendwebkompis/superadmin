<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Versi_app extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        if (cek_token()) {
            $this->load->model('Versi_app_model');
            $this->load->library('form_validation');
        } else {
            logout();
        }
    }

    public function index()
    {
        $q = urldecode($this->input->get('q', TRUE));
        $start = intval($this->input->get('start'));

        if ($q <> '') {
            $config['base_url'] = base_url() . 'versi_app?q=' . urlencode($q);
            $config['first_url'] = base_url() . 'versi_app?q=' . urlencode($q);
        } else {
            $config['base_url'] = base_url() . 'versi_app';
            $config['first_url'] = base_url() . 'versi_app';
        }

        $config['per_page'] = 10;
        $config['page_query_string'] = TRUE;
        $config['total_rows'] = $this->Versi_app_model->total_rows($q);
        $versi_app     = $this->Versi_app_model->get_limit_data($config['per_page'], $start, $q);
        $kendaraan  = $this->db->get('jenis_kendaraan')->result();

        $this->load->library('pagination');
        $this->pagination->initialize($config);

        $data = array(
            'titleCard'                 => 'List Versi Aplikasi',
            'tittle'                     => 'Versi Aplikasi',
            'judul'                     => 'Versi Aplikasi',
            'breadcrumbactive'          => '',
            'breadcrumb'                => 'Versi Aplikasi',
            'cekaktif'                  => 'versi_apliksi',
            'link'                      => '',
            'versi_app_data'            => $versi_app,
            'q'                         => $q,
            'pagination'                => $this->pagination->create_links(),
            'total_rows'                => $config['total_rows'],
            'start'                     => $start,
            'action'                    => site_url('versi_app/create_action'),
            'kategori_aktif'            => '1',
            'menu_aktif'                => 'versi_aplikasi'
        );
        $data['kendaraan']  = $kendaraan;
        $data['datakonten'] = $this->load->view('versi_app_list', $data, true);
        $this->load->view('layouts/main_view', $data);
    }

    public function detail($id)
    {
        $row = $this->Versi_app_model->get_by_id($id);
        if ($row) {
            $data = array(
                'titleCard'                 => 'Data Dashboard',
                'title'                     => 'Dashboard | Mitra',
                'judul'                     => 'Dashboard',
                'breadcrumbactive'          => '',
                'breadcrumb'                => 'Dashboard',
                'cekaktif'                  => 'dashboard',
                'link'                      => '',
                'id_versi_app' => $row->id_versi_app,
                'nama_versi_app' => $row->nama_versi_app,
                'prefix_kat' => $row->prefix_kat,
                'created_at' => $row->created_at,
                'edited_at' => $row->edited_at,
            );
            $data['datakonten'] = $this->load->view('tb_versi_app_detail', $data, true);
            $this->load->view('layouts/main_view', $data);
        } else {
            $this->session->set_flashdata('message', '<div class="alert alert-warning" role="alert">
  Data Tidak Di Temukan
</div>');
            redirect(site_url('versi_app'));
        }
    }

    public function create()
    {
        $data = array(
            'titleCard'                 => 'Data Dashboard',
            'title'                     => 'Dashboard |',
            'judul'                     => 'Dashboard',
            'breadcrumbactive'          => '',
            'breadcrumb'                => 'Dashboard',
            'cekaktif'                  => 'dashboard',
            'link'                      => '',
            'button' => 'Create',
            'action' => site_url('versi_app/create_action'),
            'id_versi_app' => set_value('id_versi_app'),
            'nama_versi_app' => set_value('nama_versi_app'),
            'prefix_kat' => set_value('prefix_kat'),
            'created_at' => set_value('created_at'),
            'edited_at' => set_value('edited_at'),
            'sts_rmv_versi_app' => set_value('sts_rmv_versi_app'),
        );
        $data['datakonten'] = $this->load->view('tb_versi_app_form', $data, true);
        $this->load->view('layouts/main_view', $data);
    }

    public function create_action()
    {
        $this->_rules();
        $dt = new DateTime();
        // $prefix = "DRV".getID();
        // // echo $gabung;
        // $row    = $this->Versi_app_model->get_by_prefix($prefix);
        // $NoUrut = (int) substr($row->max, 18, 4);
        // $NoUrut = $NoUrut + 1; //nomor urut +1
        // $NoUrut = sprintf('%04d', $NoUrut);
        // $fix    = $prefix . $NoUrut;
        if ($this->form_validation->run() == FALSE) {
            $this->session->set_flashdata('gagal', 'Gagal Menambahkan Data');
            redirect(site_url('versi_app'));
        } else {
            $data = array(
                'keterangan_versi'           => $this->input->post('keterangan_versi', TRUE),
                'versi'    => $this->input->post('versi', TRUE),
                'status_aplikasi'        => $this->input->post('status_aplikasi', TRUE),
                'is_versi_active'        => 0,
                'sts_versi_rmv'        => 1,
                'tgl_buat'        => $dt->format('Y-m-d H:i:s'),
                'tgl_update'        => $dt->format('Y-m-d H:i:s'),
            );

            $this->Versi_app_model->insert($data);
            $this->session->set_flashdata('berhasil', 'Berhasil Menambahkan Data');
            redirect(site_url('versi_app'));
        }
    }

    public function update($id)
    {
        $row = $this->Versi_app_model->get_by_id($id);

        if ($row) {
            $data = array(
                'titleCard'                 => 'Data Dashboard',
                'title'                     => 'Dashboard | Mitra',
                'judul'                     => 'Dashboard',
                'breadcrumbactive'          => '',
                'breadcrumb'                => 'Dashboard',
                'cekaktif'                  => 'dashboard',
                'link'                      => '',
                'button' => 'Update',
                'action' => site_url('versi_app/update_action'),
                'id_versi_app' => set_value('id_versi_app', $row->id_versi_app),
                'nama_versi_app' => set_value('nama_versi_app', $row->nama_versi_app),
                'prefix_kat' => set_value('prefix_kat', $row->prefix_kat),
                'created_at' => set_value('created_at', $row->created_at),
                'edited_at' => set_value('edited_at', $row->edited_at),
                'sts_rmv_versi_app' => set_value('sts_rmv_versi_app', $row->sts_rmv_versi_app),
            );
            $data['datakonten'] = $this->load->view('tb_versi_app_form', $data, true);
            $this->load->view('layouts/main_view', $data);
        } else {
            $this->session->set_flashdata('message', '<div class="alert alert-warning" role="alert">Data Tidak Ditemukan</div>');
            redirect(site_url('versi_app'));
        }
    }

    public function update_action($id)
    {
        $this->_rules();
        $dt = new DateTime();
        // if ($this->form_validation->run() == FALSE) {
        //     $this->update($id);
        // } else {
        $data = array(
            'keterangan_versi'           => $this->input->post('keterangan_versi', TRUE),
            'versi'    => $this->input->post('versi', TRUE),
            'status_aplikasi'        => $this->input->post('status_aplikasi', TRUE),
            'tgl_update'        => $dt->format('Y-m-d H:i:s'),
        );

        $this->Versi_app_model->update($id, $data);
        $this->session->set_flashdata('berhasil', 'Berhasil Merubah Data');
        redirect(site_url('versi_app'));
        // }
    }

    public function delete($id)
    {
        $row = $this->Versi_app_model->get_by_id($id);
        $dt = new DateTime();
        if ($row) {
            $data = array(
                'is_versi_active' => 0,
                'sts_versi_rmv' => 0,
                'tgl_update'        => $dt->format('Y-m-d H:i:s'),
            );
            $this->Versi_app_model->update($id, $data);
            $this->session->set_flashdata('berhasil', 'Berhasil Menghapus Data');
            redirect(site_url('versi_app'));
        } else {
            $this->session->set_flashdata('gagal', 'Data Tidak Ditemukan');
            redirect(site_url('versi_app'));
        }
    }
    public function nonaktif($id)
    {

        $row = $this->Versi_app_model->get_by_id($id);

        $dt = new DateTime();
        if ($row) {
            $this->db->where('status_aplikasi', $row->status_aplikasi);
            $this->db->where('sts_versi_rmv', 1);
            $this->db->where('is_versi_active', 1);
            $this->db->from('versi_app');
            $cekjmlaktif = $this->db->count_all_results();
            $cekjmlaktif = $cekjmlaktif - 1;
            if ($cekjmlaktif != 0) {
                $data = array(
                    'is_versi_active' => 0,
                    'tgl_update'        => $dt->format('Y-m-d H:i:s'),
                );
                $this->Versi_app_model->update($id, $data);
                $this->session->set_flashdata('berhasil', 'Berhasil Menonaktifkan Data');
                redirect(site_url('versi_app'));
            } else {
                $this->session->set_flashdata('gagal', 'Minimal 1 status aktif');
                redirect(site_url('versi_app'));
            }
        } else {
            $this->session->set_flashdata('gagal', 'Data Tidak Ditemukan');
            redirect(site_url('versi_app'));
        }
    }
    public function aktif($id)
    {
        $row = $this->Versi_app_model->get_by_id($id);
        $dt = new DateTime();
        if ($row) {
            $data = array(
                'is_versi_active' => 0,
                'tgl_update'        => $dt->format('Y-m-d H:i:s'),
            );
            $this->db->where('status_aplikasi', $row->status_aplikasi);
            $this->db->update('versi_app', $data);
            $data = array(
                'is_versi_active' => 1,
                'tgl_update'        => $dt->format('Y-m-d H:i:s'),
            );
            $this->Versi_app_model->update($id, $data);
            $this->session->set_flashdata('berhasil', 'Berhasil Mengaktifkan Versi');
            redirect(site_url('versi_app'));
        } else {
            $this->session->set_flashdata('gagal', 'Data Tidak Ditemukan');
            redirect(site_url('versi_app'));
        }
    }

    public function _rules()
    {
        $this->form_validation->set_rules('versi', 'nama kategori produk', 'trim|required');
        $this->form_validation->set_rules('status_aplikasi', 'Jenis Kendaraan', 'trim|required');
        $this->form_validation->set_rules('keterangan_versi', 'Plat Kendaraan', 'trim|required');

        $this->form_validation->set_rules('id_versi', 'id_versi', 'trim');
        $this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

    public function ambilDataversi($id)
    {
        $this->db->where('id_versi', $id);
        $data = $this->db->get('versi_app')->row();
        echo json_encode($data);
    }
}

/* End of file versi_app.php */
/* Location: ./application/controllers/versi_app.php */
/* Please DO NOT modify this information : */
/* Generated by Harviacode Codeigniter CRUD Generator 2020-07-15 09:43:37 */
/* http://harviacode.com */