<div class="card card-<?= bgCard() ?>">
    <div class="card-header">
        <div class="card-title">
            <?= $titleCard ?>
        </div>
    </div>
    <div class="card-body">
        <div class="tab-content p-0">
            <div class="row">
                <div class="col-md-4 mb-2">
                    <button class="btn btn-sm btn-<?= bgCard() ?>" title="Tambah versi" data-toggle="modal" data-target="#modalCreateversi"><i class="fas fa-pencil-alt"></i> Tambah versi</button>
                </div>
                <div class="col-md-3 offset-md-5 mb-2">
                    <?= search(site_url('versi_app/index'), site_url('versi_app/index'), $q) ?>
                </div>
            </div>
            <div class="tab-content p-0" style="overflow:auto">
                <table class="table table-bordered table-striped table-condensed table-hover table-sm" style="margin-bottom: 10px">
                    <tr>
                        <th class='text-center' style="width: 60px;">No</th>
                        <th class='text-center'>Versi </th>
                        <th class='text-center'>Jenis Aplikasi</th>
                        <th class='text-center'>Nama Keterangan Versi</th>
                        <th class='text-center'>Tanggal Buat</th>
                        <th class='text-center'>Tanggal Update</th>
                        <th class='text-center' style="width: 150px;">Aksi</th>
                    </tr><?php
                            if ($total_rows == 0) {
                                echo '<tr><td colspan="11" bgcolor="grey" style="color: white; font-weight: bold; text-align: center;">Data Tidak Ditemukan</td></tr>';
                            } else {
                                foreach ($versi_app_data as $versi) {
                            ?>
                            <tr>
                                <td class='text-center'><?php echo ++$start ?></td>
                                <td><?php echo $versi->versi ?></td>
                                <td class='text-center'><?php
                                                        if ($versi->status_aplikasi == 0) {
                                                            echo "Bank Sampah";
                                                        } else if ($versi->status_aplikasi == 1) {
                                                            echo "Nasabah";
                                                        } else {
                                                            echo "Pengepul";
                                                        } ?></td>

                                <td class='text-center'><?php echo $versi->keterangan_versi ?></td>
                                <td class='text-center'><?php echo fulldate($versi->tgl_buat) ?></td>
                                <td class='text-center'><?php echo fulldate($versi->tgl_update) ?></td>
                                <td class='text-center'>  <?php if ($versi->is_versi_active == 1) { ?>
                                          <span class="badge badge-success">Aktif</span>
                                      <?php }else{ ?> 
                                          <span class="badge badge-danger">Tidak Aktif</span>
                                    <?php } ?>
                                    </td>
                                <td class='text-center'>
                                    <div class="btn-group">
                                        <button class="btn btn-success btn-sm" title="Edit" data-toggle="modal" data-target="#modalEditversi" onclick="cekEditversi('<?= $versi->id_versi ?>')"><i class="fas fa-edit"></i></button>
                                        <button class="btn btn-danger btn-sm" title="Delete" onclick="cekDeleteVersi('<?= $versi->id_versi ?>')"><i class="fas fa-trash"></i></button>
                                        <?php if ($versi->is_versi_active != 1) { ?>
                                            <button class="btn btn-primary btn-sm" title="Aktif" onclick="cekAktifVersi('<?= $versi->id_versi ?>')"><i class="fas fa-table"></i></button>
                                        <?php } ?>
                                        <?php if ($versi->is_versi_active == 1) { ?>
                                            <button class="btn btn-secondary btn-sm" title="Non Aktif" onclick="cekNonAktifVersi('<?= $versi->id_versi ?>')"><i class="fas fa-book"></i></button>
                                        <?php } ?>
                                    </div>

                                </td>
                            </tr>
                    <?php
                                }
                            }
                    ?>
                </table>
            </div>
            <?= footer($total_rows, $pagination, '') ?>
        </div>
    </div>
</div>

<div id="modalCreateversi" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Form Tambah versi</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form action="<?= $action ?>" method="post" id="formTambahversi" role="form" enctype="multipart/form-data">
                    <div class="row ml-3 mr-3">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="varchar">Jenis Aplikasi</label>
                                <select name="status_aplikasi" id="status_aplikasi" class="form-control" required>
                                    <option value="">Pilih Jenis Aplikasi</option>
                                    <option value=0>Bank Sampah</option>
                                    <option value=1>Nasabah</option>
                                    <option value=2>Pengepul</option>

                                </select>
                            </div>
                            <div class="form-group">
                                <label for="varchar">Versi</label>
                                <input type="text" class="form-control" name="versi" id="versi" placeholder="Versi" value="" />
                            </div>
                            <div class="form-group">
                                <label for="varchar">Keterangan Versi</label>
                                <input type="text" class="form-control" name="keterangan_versi" id="keterangan_versi" placeholder="Keterangan Versi" value="" />
                            </div>

                        </div>
                    </div>
            </div>
            <div class="modal-footer">
                <div class="col-md-12 text-center">
                    <button type="submit" class="btn btn-<?= bgCard() ?>" style="width: 100px;">Simpan</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal" style="width: 100px;">Batal</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="modalEditversi" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Form Edit versi</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form action="<?= $action ?>" method="post" id="formEditversi" role="form" enctype="multipart/form-data">
                    <div class="row ml-3 mr-3">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="varchar">Jenis Aplikasi</label>
                                <select name="status_aplikasi" id="status_aplikasi" class="form-control" required>
                                    <option value="">Pilih Jenis Aplikasi</option>
                                    <option value=0>Bank Sampah</option>
                                    <option value=1>Nasabah</option>
                                    <option value=2>Pengepul</option>

                                </select>
                            </div>
                            <div class="form-group">
                                <label for="varchar">Versi</label>
                                <input type="text" class="form-control" name="versi" id="versi" placeholder="Versi" value="" />
                            </div>
                            <div class="form-group">
                                <label for="varchar">Keterangan Versi</label>
                                <input type="text" class="form-control" name="keterangan_versi" id="keterangan_versi" placeholder="Keterangan Versi" value="" />
                            </div>
                        </div>
                    </div>
            </div>
            <div class="modal-footer">
                <div class="col-md-12 text-center">
                    <button type="submit" class="btn btn-<?= bgCard() ?>" style="width: 100px;">Simpan</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal" style="width: 100px;">Batal</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    $(function() {
        $('#modalCreateversi #formTambahversi').validate({
            rules: {
                status_aplikasi: {
                    required: true
                },
                keterangan_versi: {
                    required: true
                },
                versi: {
                    required: true
                }
            },
            messages: {
                status_aplikasi: {
                    required: "Nama versi Tidak Boleh Kosong"
                },
                keterangan_versi: {
                    required: "Jenis Produk Tidak Boleh Kosong"
                },
                versi: {
                    required: "Versi Tidak Boleh Kosong"
                }
            },
            errorElement: 'span',
            errorPlacement: function(error, element) {
                error.addClass('invalid-feedback');
                element.closest('.form-group').append(error);
            },
            highlight: function(element, errorClass, validClass) {
                $(element).addClass('is-invalid');
            },
            unhighlight: function(element, errorClass, validClass) {
                $(element).removeClass('is-invalid');
            }
        })
        $('#modalEditversi #formEditversi').validate({
            rules: {
                status_aplikasi: {
                    required: true
                },
                keterangan_versi: {
                    required: true
                },
                versi: {
                    required: true
                }
            },
            messages: {
                status_aplikasi: {
                    required: "Nama versi Tidak Boleh Kosong"
                },
                keterangan_versi: {
                    required: "Jenis Kendaraan Tidak Boleh Kosong"
                },
                versi: {
                    required: "Plat Kendaraan Tidak Boleh Kosong"
                }
            },
            errorElement: 'span',
            errorPlacement: function(error, element) {
                error.addClass('invalid-feedback');
                element.closest('.form-group').append(error);
            },
            highlight: function(element, errorClass, validClass) {
                $(element).addClass('is-invalid');
            },
            unhighlight: function(element, errorClass, validClass) {
                $(element).removeClass('is-invalid');
            }
        })

        // $('#modalCreateversi #keterangan_versi').select2({
        //     theme: 'bootstrap'
        // })

        // $('#modalEditversi #edit_keterangan_versi').select2({
        //     theme: 'bootstrap'
        // })
    })

    $('#modalCreateversi').on('hidden.bs.modal', function() {
        $('#modalCreateversi #status_aplikasi').val('')
        $('#modalCreateversi #status_aplikasi').removeClass('is-invalid')
        $('#modalCreateversi #keterangan_versi').val('').trigger('change')
        $('#modalCreateversi #keterangan_versi').removeClass('is-invalid')
        $('#modalCreateversi #versi').val('')
        $('#modalCreateversi #versi').removeClass('is-invalid')
    })

    $('#modalEditversi').on('hidden.bs.modal', function() {
        $('#modalEditversi #status_aplikasi').val('')
        $('#modalEditversi #status_aplikasi').removeClass('is-invalid')
        $('#modalEditversi #edit_keterangan_versi').val('').trigger('change')
        $('#modalEditversi #edit_keterangan_versi').removeClass('is-invalid')
        $('#modalEditversi #versi').val('')
        $('#modalEditversi #versi').removeClass('is-invalid')
    })

    function cekEditversi(id) {
        startloading('#modalEditversi .modal-content')
        $('#modalEditversi #formEditversi').attr('action', '<?= base_url('versi_app/update_action/') ?>' + id)
        $.ajax({
            url: '<?php echo base_url() ?>versi_app/ambilDataversi/' + id,
            dataType: "json",
            cache: false,
            success: function(res) {
                $('#modalEditversi #status_aplikasi').val(res.status_aplikasi)
                $('#modalEditversi #edit_keterangan_versi').val(res.id_keterangan_versi).trigger('change')
                $('#modalEditversi #keterangan_versi').val(res.keterangan_versi)
                $('#modalEditversi #versi').val(res.versi)
                stoploading('#modalEditversi .modal-content')
            }
        });
    }

    function cekNonAktifVersi(id) {
        swal({
                title: 'Yakin mengaktifkan Versi?',
                text: "Mengaktifkan Versi Berimbas Pada Aplikasi!",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#CD5C5C',
                cancelButtonColor: '#d33',
                cancelButtonText: 'Batal',
                confirmButtonText: 'Non Aktifkan',
                closeOnConfirm: false,
            },
            function(isConfirm) {
                if (isConfirm) {
                    window.location.href = "<?php echo site_url('versi_app/nonaktif/'); ?>" + id;
                } else {

                }
            });
    }

    function cekAktifVersi(id) {
        swal({
                title: 'Yakin mengaktifkan Versi?',
                text: "Mengaktifkan Versi Berimbas Pada Aplikasi!",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#CD5C5C',
                cancelButtonColor: '#d33',
                cancelButtonText: 'Batal',
                confirmButtonText: 'Aktifkan',
                closeOnConfirm: false,
            },
            function(isConfirm) {
                if (isConfirm) {
                    window.location.href = "<?php echo site_url('versi_app/aktif/'); ?>" + id;
                } else {

                }
            });
    }

    function cekDeleteVersi(id) {
        swal({
                title: 'Yakin menghapus data?',
                text: "Data yang sudah terhapus tidak dapat dikembalikan!",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#CD5C5C',
                cancelButtonColor: '#d33',
                cancelButtonText: 'Batal',
                confirmButtonText: 'Hapus',
                closeOnConfirm: false,
            },
            function(isConfirm) {
                if (isConfirm) {
                    window.location.href = "<?php echo site_url('versi_app/delete/'); ?>" + id;
                } else {

                }
            });
    }
</script>