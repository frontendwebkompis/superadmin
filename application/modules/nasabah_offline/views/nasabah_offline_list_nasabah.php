<script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>

<div class="row">
	<section class="col-lg connectedSortable ui-sortable">
		<div class="card card-info">
            <div class="card-header">
                <div class="card-title">
                  <?php echo $title_card?>
                </div>
              </div>
            <div class="card-body">
				<div class="tab-content">
                    <div class="row mb-2">
                        <div class="col-md-3 offset-md-9">
                            <?= search(site_url('nasabah_offline/nasabah/'.$id_bank_sampah.'index'), site_url('nasabah_offline/nasabah/'.$id_bank_sampah), $q ) ?>
                        </div>
                    </div>
                    <div class="tab-content p-0" style="overflow:auto">
                        <table id="example2"  class="table table-bordered table-striped table-condensed table-hover table-sm" style="margin-bottom: 10px">
                            <thead>
                            <tr>
                                <th class="text-center" width="60px">No</th>
                                <th class="text-center">Nama Nasabah</th>
                                <th class="text-center">Alamat </th>
                                <th class="text-center">No HP</th>
                                <th class="text-center" width="100px">Aksi</th>
                            </tr>
                                </thead>
                                <tbody>
                            <?php 
                            if($total_rows == 0){
                                echo '<tr><td colspan="10" bgcolor="grey" style="color: white; font-weight: bold; text-align: center;">Data Tidak Ditemukan</td></tr>';
                            } else {
                            foreach ($nasabah as $data) { ?>
                            <tr>
                                <td class="text-center"><?php echo ++$start ?></td>
                                <td><?php echo $data->nama_nasabah_offline ?></td>
                                <td><?php echo $data->alamat_nasabah_offline ?></td>
                                <td><?php echo $data->no_hp_nasabah_offline ?></td>
                                <td class="text-center">
                                    <div class="btn-group">
                                        <button class="btn btn-info btn-sm" data-toggle="modal" data-target="#modal_detail_nasabah" title="Lihat Nasabah Bank Sampah" onclick="cekDetail('<?= $data->id_nasabah_offline ?>')"><i class="fas fa-info-circle fa-sm"></i>
                                        </button>
                                    </div>
                                </>
                            </tr>
                                <?php } 
                                }   ?>
                            </tbody>
                        </table>
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            <?= totalrow($total_rows) ?>
                        </div>
                        <div class="col-md-8 text-right">
                            <?php echo $pagination ?>
                        </div>
                    </div>
                </div>
			</div>
		</div>
	</section>
</div>


<div id="modal_detail_nasabah" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-xl" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Detail Nasabah</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row px-3">
                    <div class="col-md-7">
                        <table class="table table-sm" border="0">
                            <tr>
                                <td width="200px" style="font-weight: bold">Nama Nasabah</td>
                                <td id="nama_nasabah_offline"><span class="badge badge-danger">Kosong</span></td>
                            </tr>
                            <tr>
                                <td style="font-weight: bold">No Hp Nasabah</td>
                                <td id="no_hp_nasabah_offline"><span class="badge badge-danger">Kosong</span></td>
                            </tr>
                            <tr>
                                <td style="font-weight: bold">Email Nasabah</td>
                                <td id="email_nasabah_offline"><span class="badge badge-danger">Kosong</span></td>
                            </tr>
                            <tr>
                                <td style="font-weight: bold">Alamat Nasabah</td>
                                <td id="alamat_nasabah_offline"><span class="badge badge-danger">Kosong</span></td>
                            </tr>
                            <tr>
                                <td style="font-weight: bold">Provinsi</td>
                                <td id="id_provinsi"><span class="badge badge-danger">Kosong</span></td>
                            </tr>
                            <tr>
                                <td style="font-weight: bold">Kabupaten</td>
                                <td id="id_kabupaten"><span class="badge badge-danger">Kosong</span></td>
                            </tr>
                            <tr>
                                <td style="font-weight: bold">Kecamatan</td>
                                <td id="id_kecamatan"><span class="badge badge-danger">Kosong</span></td>
                            </tr>
                            <tr>
                                <td style="font-weight: bold">No Ktp Nasabah</td>
                                <td id="no_ktp_nasabah_offline"><span class="badge badge-danger">Kosong</span></td>
                            </tr>
                            <tr>
                                <td style="font-weight: bold">Tgl Join</td>
                                <td id="tgl_nasabah_daftar">
                                <span class="badge badge-danger">Kosong</span>
                                </td>
                            </tr>
                            <tr>
                                <td style="font-weight: bold">Saldo Nasabah</td>
                                <td id="saldo_nasabah_offline"><span class="badge badge-danger">Kosong</span></td>
                            </tr>
                        </table>
                    </div>
                    <div class="col-md-5 text-center">
                        <label for="">Foto KTP Nasabah</label><br>
                        <img src="" alt="Foto KTP Nasabah" id="foto_ktp_nasabah_offline" style="max-width: 200px; max-height: 150px;">
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <div class="col-md-12 text-center">
                    <button type="button" class="btn btn-<?= bgCard() ?>" data-dismiss="modal" style="width: 100px;">Kembali</button>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
$('#modal_detail_nasabah').on('hidden.bs.modal', function(){
    $('#modal_detail_nasabah #nama_nasabah_offline').html('<span class="badge badge-danger">Kosong</span>')
    $('#modal_detail_nasabah #no_hp_nasabah_offline').html('<span class="badge badge-danger">Kosong</span>')
    $('#modal_detail_nasabah #email_nasabah_offline').html('<span class="badge badge-danger">Kosong</span>')
    $('#modal_detail_nasabah #alamat_nasabah_offline').html('<span class="badge badge-danger">Kosong</span>')
    $('#modal_detail_nasabah #id_provinsi').html('<span class="badge badge-danger">Kosong</span>')
    $('#modal_detail_nasabah #id_kabupaten').html('<span class="badge badge-danger">Kosong</span>')
    $('#modal_detail_nasabah #id_kecamatan').html('<span class="badge badge-danger">Kosong</span>')
    $('#modal_detail_nasabah #no_ktp_nasabah_offline').html('<span class="badge badge-danger">Kosong</span>')
    $('#modal_detail_nasabah #tgl_nasabah_daftar').html('<span class="badge badge-danger">Kosong</span>')
    $('#modal_detail_nasabah #saldo_nasabah_offline').html('<span class="badge badge-danger">Kosong</span>')
    $('#modal_detail_nasabah #foto_ktp_nasabah_offline').attr('src', '')
})

function cekDetail(id){
    startloading('#modal_detail_nasabah .modal-content')
    $.ajax({
        url: '<?php echo base_url() ?>nasabah_offline/ambilDataNasabahOffline/' + id,
        dataType: "json",
        cache: false,
        success: function(res) {
            console.log(res)
            $('#modal_detail_nasabah #nama_nasabah_offline').html(res.nama_nasabah_offline)
            $('#modal_detail_nasabah #no_hp_nasabah_offline').html(res.no_hp_nasabah_offline)
            $('#modal_detail_nasabah #email_nasabah_offline').html(res.email_nasabah_offline)
            $('#modal_detail_nasabah #alamat_nasabah_offline').html(res.alamat_nasabah_offline)
            $('#modal_detail_nasabah #id_provinsi').html(res.nama_provinsi)
            $('#modal_detail_nasabah #id_kabupaten').html(res.nama_kabupaten)
            $('#modal_detail_nasabah #id_kecamatan').html(res.nama_kecamatan)
            $('#modal_detail_nasabah #no_ktp_nasabah_offline').html(res.no_ktp_nasabah_offline)
            $('#modal_detail_nasabah #tgl_nasabah_daftar').html(res.tgl_nasabah_daftar)
            $('#modal_detail_nasabah #saldo_nasabah_offline').html(res.saldo_nasabah_offline)
            $('#modal_detail_nasabah #foto_ktp_nasabah_offline').attr('src',res.foto_ktp_nasabah_offline)

            stoploading('#modal_detail_nasabah .modal-content')
        }
    });
}

function confirm(res) {
    Swal.fire({
    title: 'Anda Yakin Mau Blokir Nasabah?',
    text: "Akun akan segera di Blokir!",
    icon: 'warning',
    showCancelButton: true,
    confirmButtonColor: '#3085d6',
    cancelButtonColor: '#d33',
    confirmButtonText: 'Ya, Blokir!',
    cancelButtonText: 'Batal'
    }).then((result) => {
    if (result.value) {
        Swal.fire(
        'Terblokir!',
        'Akun sudah diblokir.',
        'success'
        )
        window.location='<?php echo base_url().'nasabah_online/ubahbanned/'; ?>'+res;
    }
    });
}
</script>