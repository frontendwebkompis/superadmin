<script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>

<div class="row">
	<section class="col-lg connectedSortable ui-sortable">
		<div class="card card-info">
            <div class="card-header">
                <div class="card-title">
                  <?php echo $title_card?>
                </div>
              </div>
            <div class="card-body">
				<div class="tab-content p-0">
                    <div class="row text-left" style="margin-bottom: 10px">
                        <div class="col-md-3 offset-md-9">
                            <?php echo search(site_url('nasabah_offline/index'), site_url('nasabah_offline'), $q) ?>
                        </div>
                    </div>
                    <div class="tab-content p-0" style="overflow:auto">
                        <table id="example2"  class="table table-bordered table-striped table-condensed table-hover table-sm" style="margin-bottom: 10px">
                            <thead>
                            <tr>
                                <th class="text-center" width="60px">No</th>
                                <th class="text-center">Logo</th>
                                <th class="text-center">Nama Bank Sampah </th>
                                <th class="text-center">Alamat</th>
                                <th class="text-center" width="100px">Aksi</th>
                            </tr>
                                </thead>
                                <tbody>
                            <?php 
                            if($total_rows == 0){
                                echo '<tr><td colspan="10" bgcolor="grey" style="color: white; font-weight: bold; text-align: center;">Data Tidak Ditemukan</td></tr>';
                            } else {
                            foreach ($bank as $data) { ?>
                            <tr>
                                <td class="text-center"><?php echo ++$start ?></td>
                                <td class='text-center'><?php if(empty($data->avatar)){ ?>
                                    <img src="<?= PATH_ASSETS ?>gambar/logo/kompis_blue.png" alt="AdminLTE Logo" class="brand-image img-circle elevation-3" style="max-width: 60px; max-height: 60px;">
                                <?php } else { ?>
                                    <img src="<?= $data->avatar; ?>" alt="Logo Bank Sampah" class="brand-image img-circle elevation-3" style="max-width: 60px; max-height: 60px;">
                                <?php } ?></td>
                                <td><?php echo $data->nama_bank_sampah ?></td>
                                <td><?php echo $data->alamat_bank_sampah ?></td>
                                <td class="text-center">
                                    <div class="btn-group">
                                        <a href="<?php echo site_url('nasabah_offline/nasabah/'.$data->id_bank_sampah); ?>"
                                        data-toogle="tooltip" title="Lihat Nasabah Bank Sampah">
                                        <button type="button" class="btn btn-info btn-sm"><i class="fas fa-info-circle fa-sm"></i></button></a>
                                    </div>
                                </>
                            </tr>
                                <?php } 
                                }   ?>
                            </tbody>
                        </table>
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            <?= totalrow($total_rows) ?>
                        </div>
                        <div class="col-md-8 text-right">
                            <?php echo $pagination ?>
                        </div>
                    </div>
                </div>
			</div>
		</div>
	</section>
</div>


<script>
function confirm(res) {
    Swal.fire({
    title: 'Anda Yakin Mau Blokir Nasabah?',
    text: "Akun akan segera di Blokir!",
    icon: 'warning',
    showCancelButton: true,
    confirmButtonColor: '#3085d6',
    cancelButtonColor: '#d33',
    confirmButtonText: 'Ya, Blokir!',
    cancelButtonText: 'Batal'
    }).then((result) => {
    if (result.value) {
        Swal.fire(
        'Terblokir!',
        'Akun sudah diblokir.',
        'success'
        )
        window.location='<?php echo base_url().'nasabah_online/ubahbanned/'; ?>'+res;
    }
    });
}
</script>