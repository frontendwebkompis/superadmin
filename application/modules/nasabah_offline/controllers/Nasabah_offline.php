<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Nasabah_offline extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        if (cek_token()) {
            $this->load->model('Nasabah_offline_model');
            $this->load->library('form_validation');
        } else {
            logout();
        }
    }

    public function index()
    {
        $q = urldecode($this->input->get('q', TRUE));
        $start = intval($this->input->get('start'));

        if ($q <> '') {
            $config['base_url'] = base_url() . 'nasabah_offline?q=' . urlencode($q);
            $config['first_url'] = base_url() . 'nasabah_offline?q=' . urlencode($q);
        } else {
            $config['base_url'] = base_url() . 'nasabah_offline';
            $config['first_url'] = base_url() . 'nasabah_offline';
        }

        $config['per_page']             = 10;
        $config['page_query_string']    = TRUE;
        $config['total_rows']           = $this->Nasabah_offline_model->total_rows($q);
        $bank                           = $this->Nasabah_offline_model->get_limit_data($config['per_page'], $start, $q);

        $this->load->library('pagination');
        $this->pagination->initialize($config);

        $data = array(
            'tittle'                => 'Nasabah Offline',
            'judul'                 => 'Nasabah Offline',
            'title_card'            => 'List Bank Sampah',
            'bank'                  => $bank,
            'q'                     => $q,
            'pagination'            => $this->pagination->create_links(),
            'total_rows'            => $config['total_rows'],
            'start'                 => $start,
            'menu_aktif'            => 'nasabah_offline',
        );
        $res['datakonten'] = $this->load->view('nasabah_offline/nasabah_offline_list', $data, true);
        $this->load->view('layouts/main_view', $res);
    }

    public function nasabah($id)
    {
        // $this->db->where(array('md5(id_bank_sampah) = ' => $id));
        $this->db->where('id_bank_sampah', $id);
        $this->db->where('id_status_bank_sampah', '3');
        $data = $this->db->get('bank_sampah')->row();
        if ($data) {
            $q = urldecode($this->input->get('q', TRUE));
            $start = intval($this->input->get('start'));

            if ($q <> '') {
                $config['base_url'] = base_url() . 'nasabah_offline/' . $id . '?q=' . urlencode($q);
                $config['first_url'] = base_url() . 'nasabah_offline/' . $id . '?q=' . urlencode($q);
            } else {
                $config['base_url'] = base_url() . 'nasabah_offline/' . $id;
                $config['first_url'] = base_url() . 'nasabah_offline/' . $id;
            }

            $config['per_page']             = 10;
            $config['page_query_string']    = TRUE;
            $config['total_rows']           = $this->Nasabah_offline_model->total_rows_nasabah($id, $q);
            $nasabah                        = $this->Nasabah_offline_model->get_limit_data_nasabah($id, $config['per_page'], $start, $q);

            $this->load->library('pagination');
            $this->pagination->initialize($config);

            $data = array(
                'tittle'                => 'Nasabah Offline',
                'judul'                 => 'Nasabah Offline',
                'title_card'            => 'List Nasabah Offline',
                'nasabah'               => $nasabah,
                'q'                     => $q,
                'pagination'            => $this->pagination->create_links(),
                'total_rows'            => $config['total_rows'],
                'start'                 => $start,
                'menu_aktif'            => 'nasabah_offline',
                'id_bank_sampah'        => $id
            );
            $res['datakonten'] = $this->load->view('nasabah_offline/nasabah_offline_list_nasabah', $data, true);
            $this->load->view('layouts/main_view', $res);
        } else {
            $this->session->set_flashdata('gagal', 'Data tidak ditemukan');
            redirect('nasabah_offline');
        }
    }

    public function ambilDataNasabahOffline($id)
    {
        $this->db->where('id_nasabah_offline', $id);
        $this->db->join('provinsi', 'provinsi.id_provinsi = nasabah_offline.id_provinsi');
        $this->db->join('kabupaten', 'kabupaten.id_kabupaten = nasabah_offline.id_kabupaten');
        $this->db->join('kecamatan', 'kecamatan.id_kecamatan = nasabah_offline.id_kecamatan');
        $data = $this->db->get('nasabah_offline')->row();
        $data->tgl_nasabah_daftar = fulldate($data->tgl_nasabah_daftar);
        echo json_encode($data);
    }
}

/* End of file Nasabah_online.php */
/* Location: ./application/controllers/Nasabah_online.php */
