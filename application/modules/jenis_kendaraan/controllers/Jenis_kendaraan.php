<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Jenis_kendaraan extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        if (cek_token()) {
        $this->load->model('Jenis_kendaraan_model');
        $this->load->library('form_validation');
    } else {
            logout();
        }
    }

    public function index()
    {
        $q = urldecode($this->input->get('q', TRUE));
        $start = intval($this->input->get('start'));
        
        if ($q <> '') {
            $config['base_url'] = base_url() . 'jenis_kendaraan?q=' . urlencode($q);
            $config['first_url'] = base_url() . 'jenis_kendaraan?q=' . urlencode($q);
        } else {
            $config['base_url'] = base_url() . 'jenis_kendaraan';
            $config['first_url'] = base_url() . 'jenis_kendaraan';
        }

        $config['per_page'] = 10;
        $config['page_query_string'] = TRUE;
        $config['total_rows'] = $this->Jenis_kendaraan_model->total_rows($q);
        $jenis_kendaraan     = $this->Jenis_kendaraan_model->get_limit_data($config['per_page'], $start, $q);
        $kendaraan  = $this->db->get('jenis_kendaraan')->result();

        $this->load->library('pagination');
        $this->pagination->initialize($config);

        $data = array(
            'titleCard'                 => 'List Jenis Kendaraan',
            'tittle'                    => 'Jenis Kendaraan',
            'judul'                     => 'Jenis Kendaraan',
            'breadcrumbactive'          => '',
            'breadcrumb'                => 'Jenis Kendaraan',
            'cekaktif'                  => 'Versi App',
            'link'                      => '',
            'jenis_kendaraan_data'      => $jenis_kendaraan,
            'q'                         => $q,
            'pagination'                => $this->pagination->create_links(),
            'total_rows'                => $config['total_rows'],
            'start'                     => $start,
            'action'                    => site_url('jenis_kendaraan/create_action'),
            'kategori_aktif'            => '1',
            'menu_aktif'               => 'jenis_kendaraan'
        );
        $data['kendaraan']  = $kendaraan;
        $data['datakonten'] = $this->load->view('jenis_kendaraan_list', $data, true);
        $this->load->view('layouts/main_view', $data);
    }

    public function create_action()
    {
        $dt = new DateTime();
        // $prefix = "DRV".getID();
        // // echo $gabung;
        // $row    = $this->Jenis_kendaraan_model->get_by_prefix($prefix);
        // $NoUrut = (int) substr($row->max, 18, 4);
        // $NoUrut = $NoUrut + 1; //nomor urut +1
        // $NoUrut = sprintf('%04d', $NoUrut);
        // $fix    = $prefix . $NoUrut;
        // if ($this->form_validation->run() == FALSE) {
        //     $this->session->set_flashdata('gagal', 'Gagal Menambahkan Data');
        //     redirect(site_url('jenis_kendaraan'));
        // } else {
            $data = array(
                'nama_jenis_kendaraan'           => $this->input->post('nama_jenis_kendaraan',TRUE),
                   
	        );

            $this->Jenis_kendaraan_model->insert($data);
            $this->session->set_flashdata('berhasil', 'Berhasil Menambahkan Data');
            redirect(site_url('jenis_kendaraan'));
        // }
    }
    
    public function delete($id) 
    {
        $row = $this->Jenis_kendaraan_model->get_by_id($id);
        $dt = new DateTime();
        if ($row) {
            //   $data = array(
            //      'is_versi_active' => 0,
            //     'sts_versi_rmv' => 0,
            //      'tgl_update'        => $dt->format('Y-m-d H:i:s'),
            // );
            $this->Jenis_kendaraan_model->delete($id);
            $this->session->set_flashdata('berhasil', 'Berhasil Menghapus Data');
            redirect(site_url('jenis_kendaraan'));
        } else {
            $this->session->set_flashdata('gagal', 'Data Tidak Ditemukan');
            redirect(site_url('jenis_kendaraan'));
        }
    } 
   public function exportxl()
    {
        $q          = $this->input->get('q');
        $subject    = "Jenis_kendaraan";
        // $id         = $this->session->userdata('uid');
           $this->db->query('SET @no=0');
        $sql        ="SELECT 
            @no:=@no+1 AS nomor,
           nama_jenis_kendaraan, 
        FROM `jenis_kendaraan`";
           exportSQL('Jenis_kendaraan', $subject, $sql);
    }
}
