<div class="card card-<?= bgCard() ?>">
    <div class="card-header">
        <div class="card-title">
            <?= $titleCard ?>
        </div>
    </div>
    <div class="card-body">
        <div class="tab-content p-0">
            <div class="row" style="margin-bottom: 10px">
                <div class="col-md-4">
                    <button class="btn btn-sm btn-<?= bgCard() ?>" title="Tambah jenis_kendaraan" data-toggle="modal" data-target="#modalCreatejenis_kendaraan"><i class="fas fa-pencil-alt"></i> Tambah Jenis Kendaraan</button>
                </div>
                <div class="col-md-4 text-center">
                    <div style="margin-top: 8px" id="message">
                        <?php echo $this->session->userdata('message') <> '' ? $this->session->userdata('message') : ''; ?>
                    </div>
                </div>
                <div class="col-md-4 text-right">
                    <form action="<?php echo site_url('jenis_kendaraan'); ?>" class="form-inline float-right" method="get">
                        <div class="input-group">
                            <input type="text" class="form-control-sm" placeholder="Cari Jenis Kendaraan" name="q" value="<?php echo $q; ?>">
                            <span class="btn-group">
                                <?php if ($q <> '') { ?>
                                    <a href="<?php echo site_url('jenis_kendaraan'); ?>" class="btn btn-default btn-sm"><i class="fas fa-redo fa-sm"></i></a>
                                    <?php } ?>
                            <button class="btn btn-<?= bgCard() ?> btn-sm" type="submit"><i class="fas fa-search fa-sm"></i></button>
                            </span>
                        </div>
                    </form>
                </div>
            </div>
            <div class="tab-content p-0" style="overflow:auto">
                <table class="table table-bordered table-striped table-condensed table-hover table-sm" style="margin-bottom: 10px">
                    <tr>
                        <th class='text-center' style="width: 60px;">No</th>
                        <th class='text-center'>Jenis Kendaraan </th>
                       
                        <th class='text-center' style="width: 150px;">Aksi</th>
                    </tr><?php
                    if ($total_rows == 0) {
                        echo '<tr><td colspan="11" bgcolor="grey" style="color: white; font-weight: bold; text-align: center;">Data Tidak Ditemukan</td></tr>';
                    } else {
                        foreach ($jenis_kendaraan_data as $jenis_kendaraan)
                        {
                            ?>
                    <tr>
                        <td class='text-center'><?php echo ++$start ?></td>
                        <td><?php echo $jenis_kendaraan->nama_jenis_kendaraan ?></td>
                       
                        <td class='text-center'>
                        <div class="btn-group">
                            <!-- <button class="btn btn-<?= bgCard() ?> btn-sm" title="Detail"><i class="fas fa-info-circle"></i></button> -->
                           
                            <button class="btn btn-danger btn-sm" title="Delete" onclick="cekDeletejenis_kendaraan('<?= $jenis_kendaraan->id_jenis_kendaraan ?>')"><i class="fas fa-trash"></i></button>
                          
                        </div>
                          
                        </td>
                    </tr>
                            <?php
                        }

                    }
                   
                        ?>
                </table>
            </div>
             <?= footer($total_rows, $pagination, site_url('jenis_kendaraan/exportxl?q=' . $q)) ?>
        </div>
    </div>
</div>

<div id="modalCreatejenis_kendaraan" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Form Tambah Jenis Kendaraan</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form action="<?= $action ?>" method="post" id="formTambahjenis_kendaraan" role="form" enctype="multipart/form-data">
                <div class="row ml-3 mr-3">
                    <div class="col-md-12">
                       
                        <div class="form-group">
                            <label for="varchar">Nama Jenis Kendaraan</label>
                            <input type="text" class="form-control" name="nama_jenis_kendaraan" id="nama_jenis_kendaraan" placeholder="Nama Jenis Kendaraan" value="" />
                        </div>
                        
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <div class="col-md-12 text-center">
                    <button type="submit" class="btn btn-<?= bgCard() ?>" style="width: 100px;">Simpan</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal" style="width: 100px;">Batal</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="modalEditjenis_kendaraan" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Form Edit jenis_kendaraan</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form action="<?= $action ?>" method="post" id="formEditjenis_kendaraan" role="form" enctype="multipart/form-data">
                <div class="row ml-3 mr-3">
                    <div class="col-md-12">
                           <div class="form-group">
                            <label for="varchar">jenis_kendaraan</label>
                            <input type="text" class="form-control" name="nama_jenis_kendaraan" id="nama_jenis_kendaraan" placeholder="jenis_kendaraan" value="" />
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <div class="col-md-12 text-center">
                    <button type="submit" class="btn btn-<?= bgCard() ?>" style="width: 100px;">Simpan</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal" style="width: 100px;">Batal</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    $(function(){
        $('#modalCreatejenis_kendaraan #formTambahjenis_kendaraan').validate({
            rules: {
                nama_jenis_kendaraan : {
                    required : true
                },
             
            },
            messages: {
                nama_jenis_kendaraan : {
                    required : "Nama jenis_kendaraan Tidak Boleh Kosong"
                },
                
            },
            errorElement: 'span',
            errorPlacement: function(error, element) {
                error.addClass('invalid-feedback');
                element.closest('.form-group').append(error);
            },
            highlight: function(element, errorClass, validClass) {
                $(element).addClass('is-invalid');
            },
            unhighlight: function(element, errorClass, validClass) {
                $(element).removeClass('is-invalid');
            }
        })
        $('#modalEditjenis_kendaraan #formEditjenis_kendaraan').validate({
            rules: {
                nama_jenis_kendaraan : {
                    required : true
                },
             
            },
            messages: {
                nama_jenis_kendaraan : {
                    required : "Nama jenis_kendaraan Tidak Boleh Kosong"
                },
              
            },
            errorElement: 'span',
            errorPlacement: function(error, element) {
                error.addClass('invalid-feedback');
                element.closest('.form-group').append(error);
            },
            highlight: function(element, errorClass, validClass) {
                $(element).addClass('is-invalid');
            },
            unhighlight: function(element, errorClass, validClass) {
                $(element).removeClass('is-invalid');
            }
        })

        $('#modalCreatejenis_kendaraan #keterangan_jenis_kendaraan').select2({
            theme: 'bootstrap'
        })

        $('#modalEditjenis_kendaraan #edit_keterangan_jenis_kendaraan').select2({
            theme: 'bootstrap'
        })
    })

    $('#modalCreatejenis_kendaraan').on('hidden.bs.modal', function(){
       
        $('#modalCreatejenis_kendaraan #nama_jenis_kendaraan').val('')
        $('#modalCreatejenis_kendaraan #nama_jenis_kendaraan').removeClass('is-invalid')
    })

    $('#modalEditjenis_kendaraan').on('hidden.bs.modal', function(){
        $('#modalEditjenis_kendaraan #status_aplikasi').val('')
        $('#modalEditjenis_kendaraan #status_aplikasi').removeClass('is-invalid')
        $('#modalEditjenis_kendaraan #edit_keterangan_jenis_kendaraan').val('').trigger('change')
        $('#modalEditjenis_kendaraan #edit_keterangan_jenis_kendaraan').removeClass('is-invalid')
        $('#modalEditjenis_kendaraan #jenis_kendaraan').val('')
        $('#modalEditjenis_kendaraan #jenis_kendaraan').removeClass('is-invalid')
    })

    function cekEditjenis_kendaraan(id){
        startloading('#modalEditjenis_kendaraan .modal-content')
        $('#modalEditjenis_kendaraan #formEditjenis_kendaraan').attr('action', '<?= base_url('jenis_kendaraan/update_action/') ?>'+id)
        $.ajax({
            url: '<?php echo base_url() ?>jenis_kendaraan/ambilDatajenis_kendaraan/' + id,
            dataType: "json",
            cache: false,
            success: function(res) {
                $('#modalEditjenis_kendaraan #nama_jenis_kendaraan').val(res.nama_jenis_kendaraan)
               
                stoploading('#modalEditjenis_kendaraan .modal-content')
            }
        });
    }

  
    function cekDeletejenis_kendaraan(id){
        swal({
                title: 'Yakin menghapus data?',
                text: "Data yang sudah terhapus tidak dapat dikembalikan!",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#CD5C5C',
                cancelButtonColor: '#d33',
                cancelButtonText: 'Batal',
                confirmButtonText: 'Hapus',
                closeOnConfirm: false,
            },
            function(isConfirm) {
                if (isConfirm) {
                    window.location.href = "<?php echo site_url('jenis_kendaraan/delete/'); ?>" + id;
                } else {

                }
            });
    }
</script>