<script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>

<div class="row">
	<section class="col-lg connectedSortable ui-sortable">
		<div class="card">
			<div class="card-body">
				<div class="tab-content p-0">
        <div class="row">
            <div class="col-md-4 mb-2">
                <?php echo anchor(site_url('tabel_harga_detail/create'),'Tambah Data', 'class="btn btn-primary"'); ?>
            </div>
            <div class="col-md-3 offset-md-5 mb-2">
                <?= search(site_url('tabel_harga_detail/index'), site_url('tabel_harga_detail'), $q) ?>
            </div>
        </div>
        <table class="table table-bordered" style="margin-bottom: 10px">
            <tr>
                <th style="text-align:center" width="20px">No</th>
                <th style="text-align:center">Kategori Sampah</th>
                <th style="text-align:center">Jenis Sampah</th>
                <th style="text-align:center">Provinsi</th>
                <th style="text-align:center">Harga</th>
                <th style="text-align:center">Aksi</th>
            </tr>
            <?php foreach ($tabel_harga_detail_data as $tabel_harga_detail) { ?>
            <tr>
                <td style="text-align:center"><?php echo ++$start ?></td>
                <td><?php echo $tabel_harga_detail->id_tabel_harga ?></td>
                <td></td>
                <td><?php echo $tabel_harga_detail->id_provinsi ?></td>
                <td>Rp <?php echo $tabel_harga_detail->harga_provinsi ?></td>
                <td style="text-align:center" width="100px">
                    <div class="btn-group">
                        <a href="<?php echo site_url('tabel_harga_detail/read/'.$tabel_harga_detail->id_tbl_harga_detail); ?>"
                        data-toogle="tooltip" title="Detail">
                        <button type="button" class="btn btn-info"><i class="fas fa-info-circle"></i></button></a>
                        <a href="<?php echo site_url('tabel_harga_detail/update/'.$tabel_harga_detail->id_tbl_harga_detail); ?>"
                        data-toogle="tooltip" title="Update">
                        <button type="button" class="btn btn-success"><i class="far fa-edit"></i></button></a>
                        <a href="#" class="btn btn-danger" data-toggle="tooltip" data-placement="top" title="Hapus" onclick="confirm(<?= $tabel_harga_detail->id_tbl_harga_detail ?>)" ><i class="fas fa-trash-alt"></i></a>
                    </div>
                </td>
		    </tr>
            <?php } ?>
        </table>
        <?= footer($total_rows, $pagination, site_url('tabel_harga_detail/excel')) ?>
        </div>

			</div>
		</div>
	</section>
</div>

<script>
function confirm(res) {
    Swal.fire({
    title: 'Anda Yakin Mau Menghapus?',
    text: "Tidak bisa dikembalikan jika sudah dihapus!",
    icon: 'warning',
    showCancelButton: true,
    confirmButtonColor: '#3085d6',
    cancelButtonColor: '#d33',
    confirmButtonText: 'Ya, Hapus!',
    cancelButtonText: 'Batal'
    }).then((result) => {
    if (result.value) {
        Swal.fire(
        'Terhapus!',
        'File sudah terhapus.',
        'success'
        )
        window.location='<?php echo base_url().'tabel_harga_detail/delete/'; ?>'+res;
    }
    });
}
</script>