<div class="row">
	<!-- Left col -->
	<section class="col-lg connectedSortable ui-sortable">
		<!-- Custom tabs (Charts with tabs)-->
		<div class="card">
			<div class="card-body">
				<div class="tab-content p-0">

        <form action="<?php echo $action; ?>" method="post">
            <div class="form-group">
                <label for="varchar">Kategori Sampah <?php echo form_error('id_kategori_sampah') ?></label>
                    <select id="id_kategori_sampah" class="form-control" name="id_kategori_sampah">
                        <option value="">Pilih Kategori</option>
                        <?php foreach ($kategori_sampah as $kat) { ?>
                        <option <?=  $kat->id_kategori_sampah ==  $id_kategori_sampah ? 'selected' : ''; ?> value="<?php echo $kat->id_kategori_sampah; ?>">
                        <?php echo $kat->nama_kategori_sampah; ?>
                        </option>
                        <?php } ?>
                    </select>
            </div>
            <div class="form-group">
                <label for="varchar">Jenis Sampah <?php echo form_error('id_jenis_sampah') ?></label>
                <select id="id_jenis_sampah" class="form-control" name="id_jenis_sampah">
                    <option value="">Pilih Jenis</option>
                    <?php foreach ($jenis_sampah as $jenis) { ?>
                    <option <?=  $jenis->id_jenis_sampah ==  $id_jenis_sampah ? 'selected' : ''; ?> value="<?php echo $jenis->id_jenis_sampah; ?>">
                    <?php echo $jenis->nama_jenis_sampah; ?>
                    </option>
                    <?php } ?>
                </select>
            </div>
            <div class="form-group">
                <label for="int">Provinsi <?php echo form_error('harga_provinsi') ?></label>
                <select id="id_provinsi" class="form-control" name="id_provinsi">
                <option value="">Pilih Provinsi</option>
                <?php foreach ($provinsi as $prov){ ?>
                <option <?=  $prov->id_provinsi ==  $id_provinsi ? 'selected' : ''; ?> value="<?php echo $prov->id_provinsi; ?>">
                <?php echo $prov->nama_provinsi; ?>
                </option><?php }?>
                </select>
                <!-- <input type="text" class="form-control" name="harga_provinsi" id="harga_provinsi" placeholder="Provinsi" value="<?php echo $harga_provinsi; ?>" /> -->
            </div>
            <div class="form-group">
                <label for="int">Harga</label>
                <input type="text" onkeypress="return Angkasaja(event)" class="form-control" name="harga_provinsi" id="harga_provinsi" placeholder="Harga Provinsi" />
            </div>
            <input type="hidden" name="id_tbl_harga_detail" value="<?php echo $id_tbl_harga_detail; ?>" /> 
            <button type="submit" class="btn btn-primary">Simpan</button> 
            <a href="<?php echo site_url('tabel_harga_detail') ?>" class="btn btn-default">Batal</a>
	    </form>
    </div>

    </div><!-- /.card-body -->
		</div>
		<!-- /.card -->
	</section>
	<!-- /.Left col -->
</div>


<script type="text/javascript">
    function Angkasaja(evt) {
    var charCode = (evt.which) ? evt.which : event.keyCode
    if (charCode > 31 && (charCode < 48 || charCode > 57))
    return false;
    return true;
    }
</script>