<div class="row">
	<!-- Left col -->
	<section class="col-lg connectedSortable ui-sortable">
		<!-- Custom tabs (Charts with tabs)-->
		<div class="card">
			<div class="card-body">
				<div class="tab-content p-0">

        <table class="table table-bordered table-striped table-condensed" style="margin-bottom: 20px">
            <tr><td>Kategori Sampah</td><td><?php echo $id_tabel_harga; ?></td></tr>
            <tr><td>Jenis Sampah</td><td></td></tr>
            <tr><td>Provinsi</td><td><?php echo $id_provinsi; ?></td></tr>
            <tr><td>Kabupaten</td><td></td></tr>
            <tr><td>Harga</td><td>Rp <?php echo $harga_provinsi; ?></td></tr>
            <!-- <tr><td></td><td><a href="<?php echo site_url('tabel_harga_detail') ?>" class="btn btn-default">Batal</a></td></tr> -->
	    </table>
        <div>
            <a href="<?php echo site_url('tabel_harga_detail') ?>" class="btn btn-primary">Batal</a>
        </div>

        </div><!-- /.card-body -->
		</div>
		<!-- /.card -->
	</section>
	<!-- /.Left col -->
</div>