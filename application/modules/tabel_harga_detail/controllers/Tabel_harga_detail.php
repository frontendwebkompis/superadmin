<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Tabel_harga_detail extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Tabel_harga_detail_model');
        $this->load->library('form_validation');
    }

    public function index()
    {
        $q = urldecode($this->input->get('q', TRUE));
        $start = intval($this->input->get('start'));
        
        if ($q <> '') {
            $config['base_url'] = base_url() . 'tabel_harga_detail/index.html?q=' . urlencode($q);
            $config['first_url'] = base_url() . 'tabel_harga_detail/index.html?q=' . urlencode($q);
        } else {
            $config['base_url'] = base_url() . 'tabel_harga_detail/index.html';
            $config['first_url'] = base_url() . 'tabel_harga_detail/index.html';
        }

        $config['per_page'] = 10;
        $config['page_query_string'] = TRUE;
        $config['total_rows'] = $this->Tabel_harga_detail_model->total_rows($q);
        $tabel_harga_detail = $this->Tabel_harga_detail_model->get_limit_data($config['per_page'], $start, $q);

        $this->load->library('pagination');
        $this->pagination->initialize($config);

        $data = array(
            'tittle'    => 'Tabel Harga Nasional',
            'judul'     => 'Tabel Harga Provinsi',
            'tabel_harga_detail_data' => $tabel_harga_detail,
            'q' => $q,
            'pagination' => $this->pagination->create_links(),
            'total_rows' => $config['total_rows'],
            'start' => $start,
        );
        $res['datakonten'] = $this->load->view('tabel_harga_detail/tabel_harga_detail_list', $data,true);
        $this->load->view('layouts/main_view', $res);
    }

    public function read($id) 
    {
        $row = $this->Tabel_harga_detail_model->get_by_id($id);
        if ($row) {
            $data = array(
            'tittle'    => 'Tabel Harga Nasional',
            'judul'     => 'Tabel Harga Provinsi',
            'id_tbl_harga_detail' => $row->id_tbl_harga_detail,
            'id_tabel_harga' => $row->id_tabel_harga,
            'id_provinsi' => $row->id_provinsi,
            'harga_provinsi' => $row->harga_provinsi,
            );
            $res['datakonten'] = $this->load->view('tabel_harga_detail/tabel_harga_detail_read', $data,true);
            $this->load->view('layouts/main_view', $res);
        } else {
            $this->session->set_flashdata('gagal', 'Data Tidak Di Temukan');
            redirect(site_url('tabel_harga_detail'));
        }
    }

    public function create() 
    {
        $data = array(
            'tittle'    => 'Tabel Harga Nasional',
            'judul'     => 'Tabel Harga Provinsi',
        'button' => 'Create',
        'action' => site_url('tabel_harga_detail/create_action'),
        'id_tbl_harga_detail' => set_value('id_tbl_harga_detail'),
	    'id_tabel_harga' => set_value('id_tabel_harga'),
	    'id_provinsi' => set_value('id_provinsi'),
	    'harga_provinsi' => set_value('harga_provinsi'),
	);
        $res['datakonten'] = $this->load->view('tabel_harga_detail/tabel_harga_detail_form', $data,true);
        $this->load->view('layouts/main_view', $res);
    }
    
    public function create_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->create();
        } else {
            $data = array(
            'id_tabel_harga' => $this->input->post('id_tabel_harga',TRUE),
            'id_provinsi' => $this->input->post('id_provinsi',TRUE),
            'harga_provinsi' => $this->input->post('harga_provinsi',TRUE),
            );

            $this->Tabel_harga_detail_model->insert($data);
            $this->session->set_flashdata('berhasil', 'Berhasil Menambahkan Data');
            redirect(site_url('tabel_harga_detail'));
        }
    }
    
    public function update($id) 
    {
        $row = $this->Tabel_harga_detail_model->get_by_id($id);

        if ($row) {
            $data = array(
            'button' => 'Update',
            'action' => site_url('tabel_harga_detail/update_action'),
            'id_tbl_harga_detail' => set_value('id_tbl_harga_detail', $row->id_tbl_harga_detail),
            'id_tabel_harga' => set_value('id_tabel_harga', $row->id_tabel_harga),
            'id_provinsi' => set_value('id_provinsi', $row->id_provinsi),
            'harga_provinsi' => set_value('harga_provinsi', $row->harga_provinsi),
            );
            $this->load->view('tabel_harga_detail/tabel_harga_detail_form', $data);
        } else {
            $this->session->set_flashdata('gagal', 'Data Tidak Ditemukan');
            redirect(site_url('tabel_harga_detail'));
        }
    }
    
    public function update_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->update($this->input->post('id_tbl_harga_detail', TRUE));
        } else {
            $data = array(
            'id_tabel_harga' => $this->input->post('id_tabel_harga',TRUE),
            'id_provinsi' => $this->input->post('id_provinsi',TRUE),
            'harga_provinsi' => $this->input->post('harga_provinsi',TRUE),
            );

            $this->Tabel_harga_detail_model->update($this->input->post('id_tbl_harga_detail', TRUE), $data);
            $this->session->set_flashdata('berhasil', 'Berhasil Merubah Data');
            redirect(site_url('tabel_harga_detail'));
        }
    }
    
    public function delete($id) 
    {
        $row = $this->Tabel_harga_detail_model->get_by_id($id);

        if ($row) {
            $this->Tabel_harga_detail_model->delete($id);
            $this->session->set_flashdata('berhasil', 'Berhasil Menghapus Data');
            redirect(site_url('tabel_harga_detail'));
        } else {
            $this->session->set_flashdata('gagal', 'Data Tidak Ditemukan');
            redirect(site_url('tabel_harga_detail'));
        }
    }

    public function _rules() 
    {
	$this->form_validation->set_rules('id_tabel_harga', 'id tabel harga', 'trim|required');
	$this->form_validation->set_rules('id_provinsi', 'id provinsi', 'trim|required');
	$this->form_validation->set_rules('harga_provinsi', 'harga provinsi', 'trim|required');

	$this->form_validation->set_rules('id_tbl_harga_detail', 'id_tbl_harga_detail', 'trim');
	$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

    public function excel()
    {
        $this->load->helper('exportexcel');
        $namaFile = "tabel_harga_detail.xls";
        $judul = "tabel_harga_detail";
        $tablehead = 0;
        $tablebody = 1;
        $nourut = 1;
        //penulisan header
        header("Pragma: public");
        header("Expires: 0");
        header("Cache-Control: must-revalidate, post-check=0,pre-check=0");
        header("Content-Type: application/force-download");
        header("Content-Type: application/octet-stream");
        header("Content-Type: application/download");
        header("Content-Disposition: attachment;filename=" . $namaFile . "");
        header("Content-Transfer-Encoding: binary ");

        xlsBOF();

        $kolomhead = 0;
        xlsWriteLabel($tablehead, $kolomhead++, "No");
        xlsWriteLabel($tablehead, $kolomhead++, "Id Tabel Harga");
        xlsWriteLabel($tablehead, $kolomhead++, "Id Provinsi");
        xlsWriteLabel($tablehead, $kolomhead++, "Harga Provinsi");

	foreach ($this->Tabel_harga_detail_model->get_all() as $data) {
            $kolombody = 0;

            //ubah xlsWriteLabel menjadi xlsWriteNumber untuk kolom numeric
            xlsWriteNumber($tablebody, $kolombody++, $nourut);
            xlsWriteNumber($tablebody, $kolombody++, $data->id_tabel_harga);
            xlsWriteNumber($tablebody, $kolombody++, $data->id_provinsi);
            xlsWriteNumber($tablebody, $kolombody++, $data->harga_provinsi);

	    $tablebody++;
            $nourut++;
        }

        xlsEOF();
        exit();
    }

}

/* End of file Tabel_harga_detail.php */
/* Location: ./application/controllers/Tabel_harga_detail.php */
