<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Laporan_tarik_saldo extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        if (cek_token()) {
        $this->load->model('Laporan_tarik_saldo_model');
        $this->load->library('form_validation');
    } else {
            logout();
        }
    }

    public function index(){
        $q = urldecode($this->input->get('q', TRUE));
        $start = intval($this->input->get('start'));
         $awal   = $this->input->get('awal') ? $this->input->get('awal') : date('y').'-01-01';
        $akhir  = $this->input->get('akhir') ? $this->input->get('akhir') : date('y-m-d');
      
        if ($q <> '') {
            $config['base_url'] = base_url() . 'laporan_tarik_saldo?q=' . urlencode($q);
            $config['first_url'] = base_url() . 'laporan_tarik_saldo?q=' . urlencode($q);
        } else {
            $config['base_url'] = base_url() . 'laporan_tarik_saldo';
            $config['first_url'] = base_url() . 'laporan_tarik_saldo';
        }

        $config['per_page'] = 15;
        $config['page_query_string'] = TRUE;
        $config['total_rows'] = $this->Laporan_tarik_saldo_model->total_rows($q,$awal,$akhir);
        $laporan_tarik_saldo     = $this->Laporan_tarik_saldo_model->get_limit_data($config['per_page'], $start, $q,$awal,$akhir);
     
        $this->load->library('pagination');
        $this->pagination->initialize($config);

        $data = array(
            'titleCard'                 => 'List Laporan Tarik Saldo Pengepul',
            'tittle'                     => 'Laporan Tarik Saldo Pengepul',
            'judul'                     => 'Laporan Tarik Saldo Pengepul',
            'breadcrumbactive'          => '',
            'breadcrumb'                => 'Laporan Tarik Saldo Pengepul',
            'cekaktif'                  => 'Laporan Tarik Saldo Pengepul',
            'link'                      => '',
            'laporan_tarik_saldo_data'               => $laporan_tarik_saldo,
            'q'                         => $q,
            'pagination'                => $this->pagination->create_links(),
            'total_rows'                => $config['total_rows'],
            'start'                     => $start,
            'action'                    => site_url('laporan_tarik_saldo/create_action'),
            'kategori_aktif'            => '1',
             'menu_aktif'            => 'laporan_tarik_saldo',
             'tombol_aktif'            => 'pengepul'
        );
         $data['datakonten'] = $this->load->view('laporan_tarik_saldo_list', $data, true);
        $this->load->view('layouts/main_view', $data);
    }
   public function bank_sampah(){
        $q = urldecode($this->input->get('q', TRUE));
        $start = intval($this->input->get('start'));
             $awal   = $this->input->get('awal') ? $this->input->get('awal') : date('y').'-01-01';
        $akhir  = $this->input->get('akhir') ? $this->input->get('akhir') : date('y-m-d');
    
        if ($q <> '') {
            $config['base_url'] = base_url() . 'laporan_tarik_saldo/bank_sampah?q=' . urlencode($q);
            $config['first_url'] = base_url() . 'laporan_tarik_saldo/bank_sampah?q=' . urlencode($q);
        } else {
            $config['base_url'] = base_url() . 'laporan_tarik_saldo/bank_sampah';
            $config['first_url'] = base_url() . 'laporan_tarik_saldo/bank_sampah';
        }

        $config['per_page'] = 15;
        $config['page_query_string'] = TRUE;
        $config['total_rows'] = $this->Laporan_tarik_saldo_model->total_rows_bank_sampah($q,$awal,$akhir);
        $laporan_tarik_saldo     = $this->Laporan_tarik_saldo_model->get_limit_data_bank_sampah($config['per_page'], $start, $q,$awal,$akhir);
     
        $this->load->library('pagination');
        $this->pagination->initialize($config);

        $data = array(
              'titleCard'                 => 'List Laporan Tarik Saldo Bank Sampah',
            'tittle'                     => 'Laporan Tarik Saldo Bank Sampah',
            'judul'                     => 'Laporan Tarik Saldo Bank Sampah',
            'breadcrumbactive'          => '',
            'breadcrumb'                => 'Laporan Tarik Saldo Bank Sampah',
            'cekaktif'                  => 'Laporan Tarik Saldo Bank Sampah',
            'link'                      => '',
            'laporan_tarik_saldo_data'               => $laporan_tarik_saldo,
            'q'                         => $q,
            'pagination'                => $this->pagination->create_links(),
            'total_rows'                => $config['total_rows'],
            'start'                     => $start,
            'action'                    => site_url('laporan_tarik_saldo/create_action'),
            'kategori_aktif'            => '1',
             'menu_aktif'            => 'laporan_tarik_saldo',
             'tombol_aktif'            => 'bank_sampah'
        );
         $data['datakonten'] = $this->load->view('laporan_tarik_saldo_list', $data, true);
        $this->load->view('layouts/main_view', $data);
    }
    public function nasabah(){
        $q = urldecode($this->input->get('q', TRUE));
        $start = intval($this->input->get('start'));
             $awal   = $this->input->get('awal') ? $this->input->get('awal') : date('y').'-01-01';
        $akhir  = $this->input->get('akhir') ? $this->input->get('akhir') : date('y-m-d');
    
        if ($q <> '') {
            $config['base_url'] = base_url() . 'laporan_tarik_saldo/nasabah?q=' . urlencode($q);
            $config['first_url'] = base_url() . 'laporan_tarik_saldo/nasabah?q=' . urlencode($q);
        } else {
            $config['base_url'] = base_url() . 'laporan_tarik_saldo/nasabah';
            $config['first_url'] = base_url() . 'laporan_tarik_saldo/nasabah';
        }

        $config['per_page'] = 15;
        $config['page_query_string'] = TRUE;
        $config['total_rows'] = $this->Laporan_tarik_saldo_model->total_rows_nasabah($q,$awal,$akhir);
        $laporan_tarik_saldo     = $this->Laporan_tarik_saldo_model->get_limit_data_nasabah($config['per_page'], $start, $q,$awal,$akhir);
     
        $this->load->library('pagination');
        $this->pagination->initialize($config);

        $data = array(
            'titleCard'                 => 'List Laporan Tarik Saldo Nasabah Online',
            'tittle'                     => 'Laporan Tarik Saldo Nasabah Online',
            'judul'                     => 'Laporan Tarik Saldo Nasabah Online',
            'breadcrumbactive'          => '',
            'breadcrumb'                => 'Laporan Tarik Saldo Nasabah Online',
            'cekaktif'                  => 'Laporan Tarik Saldo Nasabah Online',
            'link'                      => '',
            'laporan_tarik_saldo_data'               => $laporan_tarik_saldo,
            'q'                         => $q,
            'pagination'                => $this->pagination->create_links(),
            'total_rows'                => $config['total_rows'],
            'start'                     => $start,
            'action'                    => site_url('laporan_tarik_saldo/create_action'),
            'kategori_aktif'            => '1',
             'menu_aktif'            => 'laporan_tarik_saldo',
             'tombol_aktif'            => 'nasabah'
        );
         $data['datakonten'] = $this->load->view('laporan_tarik_saldo_list', $data, true);
        $this->load->view('layouts/main_view', $data);
    }

    public function detail($id) 
    {
        $row = $this->Laporan_tarik_saldo->get_by_id($id);
        if ($row) {
        $data = array(
        'titleCard'                 => 'Data Dashboard',
        'tittle'                     => 'Dashboard | Mitra',
        'judul'                     => 'Dashboard',
        'breadcrumbactive'          => '',
        'breadcrumb'                => 'Dashboard',
         'cekaktif'                  => 'dashboard',
         'link'                      => '',
		'id_laporan_tarik_saldo' => $row->id_laporan_tarik_saldo,
		'nama_laporan_tarik_saldo' => $row->nama_laporan_tarik_saldo,
		'prefix_kat' => $row->prefix_kat,
		'created_at' => $row->created_at,
		'edited_at' => $row->edited_at,
	    );
        $data['datakonten'] = $this->load->view('tb_laporan_tarik_saldo_detail', $data, true);
        $this->load->view('layouts/main_view', $data);
        } else {
            $this->session->set_flashdata('message', '<div class="alert alert-warning" role="alert">
  Data Tidak Di Temukan
</div>');
            redirect(site_url('laporan_tarik_saldo'));
        }
    }

    public function create() 
    {
        $data = array(
                'titleCard'                 => 'Data Dashboard',
            'tittle'                     => 'Dashboard |',
            'judul'                     => 'Dashboard',
            'breadcrumbactive'          => '',
            'breadcrumb'                => 'Dashboard',
            'cekaktif'                  => 'dashboard',
            'link'                      => '',
                'button' => 'Create',
                'action' => site_url('laporan_tarik_saldo/create_action'),
            'id_laporan_tarik_saldo' => set_value('id_laporan_tarik_saldo'),
            'nama_laporan_tarik_saldo' => set_value('nama_laporan_tarik_saldo'),
            'prefix_kat' => set_value('prefix_kat'),
            'created_at' => set_value('created_at'),
            'edited_at' => set_value('edited_at'),
            'sts_rmv_laporan_tarik_saldo' => set_value('sts_rmv_laporan_tarik_saldo'),
	    );
         $data['datakonten'] = $this->load->view('tb_laporan_tarik_saldo_form', $data, true);
        $this->load->view('layouts/main_view', $data);

    }
    
    public function create_action(){
        $this->_rules();
        $dt = new DateTime();
        // $prefix = "DRV".getID();
        // // echo $gabung;
        // $row    = $this->Laporan_tarik_saldo->get_by_prefix($prefix);
        // $NoUrut = (int) substr($row->max, 18, 4);
        // $NoUrut = $NoUrut + 1; //nomor urut +1
        // $NoUrut = sprintf('%04d', $NoUrut);
        // $fix    = $prefix . $NoUrut;
        if ($this->form_validation->run() == FALSE) {
            $this->session->set_flashdata('gagal', 'Gagal Menambahkan Data');
            redirect(site_url('laporan_tarik_saldo'));
        } else {
            $data = array(
                'laporan_tarik_saldo'           => $this->input->post('laporan_tarik_saldo',TRUE),
              
	        );

            $this->Laporan_tarik_saldo->insert($data);
            $this->session->set_flashdata('berhasil', 'Berhasil Menambahkan Data');
            redirect(site_url('laporan_tarik_saldo'));
        }
    }
    
    public function update($id) 
    {
        $row = $this->Laporan_tarik_saldo->get_by_id($id);

        if ($row) {
            $data = array(
             'titleCard'                 => 'Data Dashboard',
         'title'                     => 'Dashboard | Mitra',
        'judul'                     => 'Dashboard',
        'breadcrumbactive'          => '',
        'breadcrumb'                => 'Dashboard',
         'cekaktif'                  => 'dashboard',
         'link'                      => '',
                'button' => 'Update',
                'action' => site_url('laporan_tarik_saldo/update_action'),
		'id_laporan_tarik_saldo' => set_value('id_laporan_tarik_saldo', $row->id_laporan_tarik_saldo),
		'nama_laporan_tarik_saldo' => set_value('nama_laporan_tarik_saldo', $row->nama_laporan_tarik_saldo),
		'prefix_kat' => set_value('prefix_kat', $row->prefix_kat),
		'created_at' => set_value('created_at', $row->created_at),
		'edited_at' => set_value('edited_at', $row->edited_at),
		'sts_rmv_laporan_tarik_saldo' => set_value('sts_rmv_laporan_tarik_saldo', $row->sts_rmv_laporan_tarik_saldo),
	    );
               $data['datakonten'] = $this->load->view('tb_laporan_tarik_saldo_form', $data, true);
        $this->load->view('layouts/main_view', $data);

        } else {
            $this->session->set_flashdata('message', '<div class="alert alert-warning" role="alert">Data Tidak Ditemukan</div>');
            redirect(site_url('laporan_tarik_saldo'));
        }
    }
    
    public function update_action($id) 
    {
        $this->_rules();
        $dt = new DateTime();
        // if ($this->form_validation->run() == FALSE) {
        //     $this->update($id);
        // } else {
            $data = array(
		     'laporan_tarik_saldo'           => $this->input->post('laporan_tarik_saldo',TRUE),
         
	    );

            $this->Laporan_tarik_saldo->update($id, $data);
         $this->session->set_flashdata('berhasil', 'Berhasil Merubah Data');
            redirect(site_url('laporan_tarik_saldo'));
        // }
    }
    
    public function delete($id) 
    {
        $row = $this->Laporan_tarik_saldo->get_by_id($id);
        $dt = new DateTime();
        if ($row) {
              $data = array(
                 'is_versi_active' => 0,
                'sts_versi_rmv' => 0,
                 'tgl_update'        => $dt->format('Y-m-d H:i:s'),
            );
            $this->Laporan_tarik_saldo->update($id, $data);
            $this->session->set_flashdata('berhasil', 'Berhasil Menghapus Data');
            redirect(site_url('laporan_tarik_saldo'));
        } else {
            $this->session->set_flashdata('gagal', 'Data Tidak Ditemukan');
            redirect(site_url('laporan_tarik_saldo'));
        }
    } 
    public function nonaktif($id) 
    {
        $row = $this->Laporan_tarik_saldo->get_by_id($id);
        $dt = new DateTime();
        if ($row) {
              $data = array(
                'is_versi_active' => 0,
                 'tgl_update'        => $dt->format('Y-m-d H:i:s'),
            );
            $this->Laporan_tarik_saldo->update($id, $data);
            $this->session->set_flashdata('berhasil', 'Berhasil Menghapus Data');
            redirect(site_url('laporan_tarik_saldo'));
        } else {
            $this->session->set_flashdata('gagal', 'Data Tidak Ditemukan');
            redirect(site_url('laporan_tarik_saldo'));
        }
    } 
     public function aktif($id) 
    {
        $row = $this->Laporan_tarik_saldo->get_by_id($id);
        $dt = new DateTime();
        if ($row) {
              $data = array(
                'is_versi_active' => 1,
                 'tgl_update'        => $dt->format('Y-m-d H:i:s'),
            );
            $this->Laporan_tarik_saldo->update($id, $data);
            $this->session->set_flashdata('berhasil', 'Berhasil Menghapus Data');
            redirect(site_url('laporan_tarik_saldo'));
        } else {
            $this->session->set_flashdata('gagal', 'Data Tidak Ditemukan');
            redirect(site_url('laporan_tarik_saldo'));
        }
    }

    public function _rules() 
    {
        $this->form_validation->set_rules('laporan_tarik_saldo', 'nama Jenis kendaraan', 'trim|required');
 
        $this->form_validation->set_rules('id_laporan_tarik_saldo', 'id_versi', 'trim');
        $this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

    public function ambilDataversi($id){
        $this->db->where('id_laporan_tarik_saldo', $id);
        $data = $this->db->get('laporan_tarik_saldo')->row();
        echo json_encode($data);
    }

}

/* End of file laporan_tarik_saldo.php */
/* Location: ./application/controllers/laporan_tarik_saldo.php */
/* Please DO NOT modify this information : */
/* Generated by Harviacode Codeigniter CRUD Generator 2020-07-15 09:43:37 */
/* http://harviacode.com */