<div class="card card-<?= bgCard() ?>">
    <div class="card-header">
        <div class="card-title">
            <?= $titleCard ?>
        </div>
    </div>
    <div class="card-body">
        <div class="row mb-2">
            <div class="col-md-12">
                <?php $this->load->view('laporan_tarik_saldo/tombol_atas'); ?>
            </div>
        </div>
            <div class="row">
                <div class="col-md-8 mb-2">
                    <?php
                        if($tombol_aktif == 'bank_sampah'){
                            echo filterDate($this->input->get('awal'), $this->input->get('akhir'), base_url('laporan_tarik_saldo/bank_sampah'));
                        } else if($tombol_aktif=='nasabah'){
                            echo filterDate($this->input->get('awal'), $this->input->get('akhir'), base_url('laporan_tarik_saldo/nasabah'));
                        } else {
                            echo filterDate($this->input->get('awal'), $this->input->get('akhir'), base_url('laporan_tarik_saldo'));
                        }
                    ?>
                </div>
                <div class="col-md-3 offset-md-1 mb-2">
                    <?php
                        if($tombol_aktif == 'bank_sampah'){
                            echo search(site_url('laporan_tarik_saldo/bank_sampah'), site_url('laporan_tarik_saldo/bank_sampah'), $q);
                        } else if($tombol_aktif=='nasabah'){
                            echo search(site_url('laporan_tarik_saldo/nasabah'), site_url('laporan_tarik_saldo/nasabah'), $q);
                        } else {
                            echo search(site_url('laporan_tarik_saldo/index'), site_url('laporan_tarik_saldo'), $q);
                        }
                    ?>
                </div>
            </div>
           
            <div class="tab-content p-0" style="overflow:auto">
                <table class="table table-bordered table-striped table-condensed table-hover table-sm" style="margin-bottom: 10px">
                    <tr>
                        <th class='text-center' style="width: 60px;">No</th>
                        <th class='text-center'>Tanggal Tarik Saldo </th>
                        <th class='text-center'>Nama </th>
                        <th class='text-center'>Nama Rekening </th>
                        <th class='text-center'>Nomer Rekening </th>
                        <th class='text-center'>Amount</th>
                       
                        <th class='text-center' style="width: 150px;">Status</th>
                    </tr><?php
                    if ($total_rows == 0) {
                        echo '<tr><td colspan="11" bgcolor="grey" style="color: white; font-weight: bold; text-align: center;">Data Tidak Ditemukan</td></tr>';
                    } else {
                        foreach ($laporan_tarik_saldo_data as $laporan_tarik_saldo)
                        {
                            ?>
                    <tr>
                        <td class='text-center'><?php echo ++$start ?></td>
                        <td><?php echo fulldate($laporan_tarik_saldo->datetime) ?></td>
                        <td><?php echo $laporan_tarik_saldo->nama ?></td>
                        <td><?php echo $laporan_tarik_saldo->nama_rekening ?></td>
                        <td><?php echo $laporan_tarik_saldo->nomer_rekening ?></td>
                        <td class='text-center'><?php echo rupiah($laporan_tarik_saldo->amount) ?></td>
                       
                        <td class='text-center'>
                        
                          <?php if($laporan_tarik_saldo->is_read==1){
                            echo "<span class='badge badge-success'>Sukses</span>";
                          }else{
                            echo "<span class='badge badge-danger'>Pending</span>";
                       
                          } ?>
                        </td>
                    </tr>
                            <?php
                        }
                    }
                        ?>
                </table>
            </div>
            <?= footer($total_rows, $pagination, '') ?>
        </div>
    </div>
</div>

<div id="modalCreatelaporan_tarik_saldo" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Form Tambah laporan_tarik_saldo</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form action="<?= $action ?>" method="post" id="formTambahlaporan_tarik_saldo" role="form" enctype="multipart/form-data">
                <div class="row ml-3 mr-3">
                    <div class="col-md-12">
                       
                        <div class="form-group">
                            <label for="varchar">Nama Jenis Kendaraan</label>
                            <input type="text" class="form-control" name="nama_laporan_tarik_saldo" id="nama_laporan_tarik_saldo" placeholder="Nama Jenis Kendaraan" value="" />
                        </div>
                        
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <div class="col-md-12 text-center">
                    <button type="submit" class="btn btn-<?= bgCard() ?>" style="width: 100px;">Simpan</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal" style="width: 100px;">Batal</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="modalEditlaporan_tarik_saldo" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Form Edit laporan_tarik_saldo</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form action="<?= $action ?>" method="post" id="formEditlaporan_tarik_saldo" role="form" enctype="multipart/form-data">
                <div class="row ml-3 mr-3">
                    <div class="col-md-12">
                           <div class="form-group">
                            <label for="varchar">laporan_tarik_saldo</label>
                            <input type="text" class="form-control" name="nama_laporan_tarik_saldo" id="nama_laporan_tarik_saldo" placeholder="laporan_tarik_saldo" value="" />
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <div class="col-md-12 text-center">
                    <button type="submit" class="btn btn-<?= bgCard() ?>" style="width: 100px;">Simpan</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal" style="width: 100px;">Batal</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    $(function(){
        setDateFilter('akhir', 'awal');

        $('#modalCreatelaporan_tarik_saldo #formTambahlaporan_tarik_saldo').validate({
            rules: {
                nama_laporan_tarik_saldo : {
                    required : true
                },
             
            },
            messages: {
                nama_laporan_tarik_saldo : {
                    required : "Nama laporan_tarik_saldo Tidak Boleh Kosong"
                },
                
            },
            errorElement: 'span',
            errorPlacement: function(error, element) {
                error.addClass('invalid-feedback');
                element.closest('.form-group').append(error);
            },
            highlight: function(element, errorClass, validClass) {
                $(element).addClass('is-invalid');
            },
            unhighlight: function(element, errorClass, validClass) {
                $(element).removeClass('is-invalid');
            }
        })
        $('#modalEditlaporan_tarik_saldo #formEditlaporan_tarik_saldo').validate({
            rules: {
                nama_laporan_tarik_saldo : {
                    required : true
                },
             
            },
            messages: {
                nama_laporan_tarik_saldo : {
                    required : "Nama laporan_tarik_saldo Tidak Boleh Kosong"
                },
              
            },
            errorElement: 'span',
            errorPlacement: function(error, element) {
                error.addClass('invalid-feedback');
                element.closest('.form-group').append(error);
            },
            highlight: function(element, errorClass, validClass) {
                $(element).addClass('is-invalid');
            },
            unhighlight: function(element, errorClass, validClass) {
                $(element).removeClass('is-invalid');
            }
        })

        $('#modalCreatelaporan_tarik_saldo #keterangan_laporan_tarik_saldo').select2({
            theme: 'bootstrap'
        })

        $('#modalEditlaporan_tarik_saldo #edit_keterangan_laporan_tarik_saldo').select2({
            theme: 'bootstrap'
        })
    })

    $('#modalCreatelaporan_tarik_saldo').on('hidden.bs.modal', function(){
        $('#modalCreatelaporan_tarik_saldo #is_read_aplikasi').val('')
        $('#modalCreatelaporan_tarik_saldo #is_read_aplikasi').removeClass('is-invalid')
        $('#modalCreatelaporan_tarik_saldo #keterangan_laporan_tarik_saldo').val('').trigger('change')
        $('#modalCreatelaporan_tarik_saldo #keterangan_laporan_tarik_saldo').removeClass('is-invalid')
        $('#modalCreatelaporan_tarik_saldo #nama_laporan_tarik_saldo').val('')
        $('#modalCreatelaporan_tarik_saldo #nama_laporan_tarik_saldo').removeClass('is-invalid')
    })

    $('#modalEditlaporan_tarik_saldo').on('hidden.bs.modal', function(){
        $('#modalEditlaporan_tarik_saldo #is_read_aplikasi').val('')
        $('#modalEditlaporan_tarik_saldo #is_read_aplikasi').removeClass('is-invalid')
        $('#modalEditlaporan_tarik_saldo #edit_keterangan_laporan_tarik_saldo').val('').trigger('change')
        $('#modalEditlaporan_tarik_saldo #edit_keterangan_laporan_tarik_saldo').removeClass('is-invalid')
        $('#modalEditlaporan_tarik_saldo #laporan_tarik_saldo').val('')
        $('#modalEditlaporan_tarik_saldo #laporan_tarik_saldo').removeClass('is-invalid')
    })

    function cekEditlaporan_tarik_saldo(id){
        startloading('#modalEditlaporan_tarik_saldo .modal-content')
        $('#modalEditlaporan_tarik_saldo #formEditlaporan_tarik_saldo').attr('action', '<?= base_url('laporan_tarik_saldo/update_action/') ?>'+id)
        $.ajax({
            url: '<?php echo base_url() ?>laporan_tarik_saldo/ambilDatalaporan_tarik_saldo/' + id,
            dataType: "json",
            cache: false,
            success: function(res) {
                $('#modalEditlaporan_tarik_saldo #nama_laporan_tarik_saldo').val(res.nama_laporan_tarik_saldo)
               
                stoploading('#modalEditlaporan_tarik_saldo .modal-content')
            }
        });
    }

  
    function cekDeletelaporan_tarik_saldo(id){
        swal({
                title: 'Yakin menghapus data?',
                text: "Data yang sudah terhapus tidak dapat dikembalikan!",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#CD5C5C',
                cancelButtonColor: '#d33',
                cancelButtonText: 'Batal',
                confirmButtonText: 'Hapus',
                closeOnConfirm: false,
            },
            function(isConfirm) {
                if (isConfirm) {
                    window.location.href = "<?php echo site_url('laporan_tarik_saldo/delete/'); ?>" + id;
                } else {

                }
            });
    }
</script>