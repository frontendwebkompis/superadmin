<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Nasabah_online extends MX_Controller
{
    function __construct()
    {
        parent::__construct();
        if (cek_token()) {
            $this->load->model('Nasabah_online_model');
            $this->load->library('form_validation');
        } else {
            logout();
        }
    }

    public function index()
    {
        $q = urldecode($this->input->get('q', TRUE));
        $start = intval($this->input->get('start'));

        if ($q <> '') {
            $config['base_url'] = base_url() . 'nasabah_online?q=' . urlencode($q);
            $config['first_url'] = base_url() . 'nasabah_online?q=' . urlencode($q);
        } else {
            $config['base_url'] = base_url() . 'nasabah_online';
            $config['first_url'] = base_url() . 'nasabah_online';
        }

        $config['per_page'] = 10;
        $config['page_query_string'] = TRUE;
        $config['total_rows'] = $this->Nasabah_online_model->total_rows_premium($q);
        $nasabah_online = $this->Nasabah_online_model->get_limit_data_premium($config['per_page'], $start, $q);

        $this->load->library('pagination');
        $this->pagination->initialize($config);

        $data = array(
            'tittle'                => 'Nasabah',
            'judul'                 => 'Nasabah',
            'title_card'            => 'List Nasabah',
            'nasabah_online_data'   => $nasabah_online,
            'q'                     => $q,
            'pagination'            => $this->pagination->create_links(),
            'total_rows'            => $config['total_rows'],
            'start'                 => $start,
            'menu_aktif'            => 'nasabah_online',
        );
        $res['datakonten'] = $this->load->view('nasabah_online/nasabah_online_list', $data, true);
        $this->load->view('layouts/main_view', $res);
    }
    //halaman approve
    public function approve()
    {
        $q = urldecode($this->input->get('q', TRUE));
        $start = intval($this->input->get('start'));

        if ($q <> '') {
            $config['base_url'] = base_url() . 'nasabah_online/approve?q=' . urlencode($q);
            $config['first_url'] = base_url() . 'nasabah_online/approve?q=' . urlencode($q);
        } else {
            $config['base_url'] = base_url() . 'nasabah_online/approve';
            $config['first_url'] = base_url() . 'nasabah_online/approve';
        }

        $config['per_page'] = 10;
        $config['page_query_string'] = TRUE;
        $config['total_rows'] = $this->Nasabah_online_model->total_rows_approve($q);
        $nasabah_online = $this->Nasabah_online_model->get_limit_data_approve($config['per_page'], $start, $q);

        $this->load->library('pagination');
        $this->pagination->initialize($config);

        $data = array(
            'tittle'                => 'Nasabah',
            'judul'                 => 'Nasabah',
            'title_card'            => 'List Nasabah Approve',
            'nasabah_online_data'   => $nasabah_online,
            'q'                     => $q,
            'pagination'            => $this->pagination->create_links(),
            'total_rows'            => $config['total_rows'],
            'start'                 => $start,
            'menu_aktif'            => 'nasabah_online',
        );
        $res['datakonten'] = $this->load->view('nasabah_online/nasabah_online_list_approve', $data, true);
        $this->load->view('layouts/main_view', $res);
    }

    //halaman premium
    public function premium()
    {
        $q = urldecode($this->input->get('q', TRUE));
        $start = intval($this->input->get('start'));

        if ($q <> '') {
            $config['base_url'] = base_url() . 'nasabah_online/premium?q=' . urlencode($q);
            $config['first_url'] = base_url() . 'nasabah_online/premium?q=' . urlencode($q);
        } else {
            $config['base_url'] = base_url() . 'nasabah_online/premium';
            $config['first_url'] = base_url() . 'nasabah_online/premium';
        }

        $config['per_page'] = 10;
        $config['page_query_string'] = TRUE;
        $config['total_rows'] = $this->Nasabah_online_model->total_rows_jadikan_premium($q);
        $nasabah_online = $this->Nasabah_online_model->get_limit_data_jadikan_premium($config['per_page'], $start, $q);

        $this->load->library('pagination');
        $this->pagination->initialize($config);

        $data = array(
            'tittle'                => 'Nasabah',
            'judul'                 => 'Nasabah',
            'title_card'            => 'List Nasabah Premium',
            'nasabah_online_data'   => $nasabah_online,
            'q'                     => $q,
            'pagination'            => $this->pagination->create_links(),
            'total_rows'            => $config['total_rows'],
            'start'                 => $start,
            'menu_aktif'            => 'nasabah_online',
        );
        $res['datakonten'] = $this->load->view('nasabah_online/nasabah_online_list_premium', $data, true);
        $this->load->view('layouts/main_view', $res);
    }
    //halaman premium
    public function berkas()
    {
        $q = urldecode($this->input->get('q', TRUE));
        $start = intval($this->input->get('start'));

        if ($q <> '') {
            $config['base_url'] = base_url() . 'nasabah_online/berkas?q=' . urlencode($q);
            $config['first_url'] = base_url() . 'nasabah_online/berkas?q=' . urlencode($q);
        } else {
            $config['base_url'] = base_url() . 'nasabah_online/berkas';
            $config['first_url'] = base_url() . 'nasabah_online/berkas';
        }

        $config['per_page'] = 10;
        $config['page_query_string'] = TRUE;
        $config['total_rows'] = $this->Nasabah_online_model->total_rows_berkas($q);
        $nasabah_online = $this->Nasabah_online_model->get_limit_data_berkas($config['per_page'], $start, $q);

        $this->load->library('pagination');
        $this->pagination->initialize($config);

        $data = array(
            'tittle'                => 'Nasabah',
            'judul'                 => 'Nasabah',
            'title_card'            => 'List Nasabah Belum Upload Berkas',
            'nasabah_online_data'   => $nasabah_online,
            'q'                     => $q,
            'pagination'            => $this->pagination->create_links(),
            'total_rows'            => $config['total_rows'],
            'start'                 => $start,
            'menu_aktif'            => 'nasabah_online',
        );
        $res['datakonten'] = $this->load->view('nasabah_online/nasabah_online_list_berkas', $data, true);
        $this->load->view('layouts/main_view', $res);
    }

    // halaman banned
    public function banned()
    {
        $q = urldecode($this->input->get('q', TRUE));
        $start = intval($this->input->get('start'));

        if ($q <> '') {
            $config['base_url'] = base_url() . 'nasabah_online/banned?q=' . urlencode($q);
            $config['first_url'] = base_url() . 'nasabah_online/banned?q=' . urlencode($q);
        } else {
            $config['base_url'] = base_url() . 'nasabah_online/banned';
            $config['first_url'] = base_url() . 'nasabah_online/banned';
        }

        $config['per_page'] = 10;
        $config['page_query_string'] = TRUE;
        $config['total_rows'] = $this->Nasabah_online_model->total_rows_banned($q);
        $nasabah_online = $this->Nasabah_online_model->get_limit_data_banned($config['per_page'], $start, $q);

        $this->load->library('pagination');
        $this->pagination->initialize($config);

        $data = array(
            'tittle'                => 'Nasabah',
            'judul'                 => 'Nasabah',
            'title_card'            => 'List Nasabah Banned',
            'nasabah_online_data'   => $nasabah_online,
            'q'                     => $q,
            'pagination'            => $this->pagination->create_links(),
            'total_rows'            => $config['total_rows'],
            'start'                 => $start,
            'menu_aktif'            => 'nasabah_online',
        );
        $res['datakonten'] = $this->load->view('nasabah_online/nasabah_online_list_banned', $data, true);
        $this->load->view('layouts/main_view', $res);
    }


    public function detail($id)
    {
        $row    = $this->Nasabah_online_model->get_by_id_nasabah_detail($id);
        $reason = $this->Nasabah_online_model->get_reason(1);
        if ($row) {
            $parsed = parse_url($row->foto_ktp_nasabah_online);
            $foto_ktp_nasabah_online=$row->foto_ktp_nasabah_online;
            $data = array(
                'tittle'                        => 'Nasabah',
                'title_card'                    => 'Nasabah Detail',
                'judul'                         => 'Nasabah',
                'id_nasabah_online'             => $row->id_nasabah_online,
                'reason'                        => $reason,
                'id_status_nasabah_online'      => $row->id_status_nasabah_online,
                'ket_status_nasabah_online'     => $row->ket_status_nasabah_online,
                'nama_nasabah_online'           => $row->nama_nasabah_online,
                'email_nasabah_online'          => $row->email_nasabah_online,
                'no_hp_nasabah_online'          => $row->no_hp_nasabah_online,
                'last_login'                    => $row->last_login,
                'tgl_nasabah_daftar'            => $row->tgl_nasabah_daftar,
                'alamat_nasabah_online'         => $row->alamat_nasabah_online,
                'lat_nasabah_online'            => $row->lat_nasabah_online,
                'long_nasabah_online'           => $row->long_nasabah_online,
                'nama_provinsi'                 => $row->nama_provinsi,
                'nama_kabupaten'                => $row->nama_kabupaten,
                'nama_kecamatan'                => $row->nama_kecamatan,
                'no_ktp_nasabah_online'         => $row->no_ktp_nasabah_online,
                'foto_ktp_nasabah_online'       => $foto_ktp_nasabah_online,
                'foto_ktp_selfie_nasabah'       => $row->foto_ktp_selfie_nasabah_online,
                'saldo_nasabah_online'          => $row->saldo_nasabah_online,
                'point_nasabah_online'          => $row->point_nasabah_online,
                'menu_aktif'                    => 'nasabah_online',
            );
            $res['datakonten'] = $this->load->view('nasabah_online/nasabah_online_detail', $data, true);
            $this->load->view('layouts/main_view', $res);
        } else {
            $this->session->set_flashdata('gagal', 'Data Tidak Di Temukan');
            redirect(site_url('nasabah_online'));
        }
    }

    public function blokir($id)
    {
        $row = $this->Nasabah_online_model->get_by_id_nasabah_detail($id);
        $reason=$this->Nasabah_online_model->get_reason();
        if ($row) {
            $data = array(
            'tittle'                => 'Nasabah',
            'judul'                 => 'Nasabah Blokir Detail',
            'reason'                => $reason,
            'id_nasabah_online'     => $row->id_nasabah_online,
            'status_nasabah_online' => $row->ket_status_nasabah_online,
            'nama_nasabah_online'   => $row->nama_nasabah_online,
            'email_nasabah_online'  => $row->email_nasabah_online,
            'no_hp_nasabah_online'  => $row->no_hp_nasabah_online,
            'last_login'            => $row->last_login,
            'alamat_nasabah_online' => $row->alamat_nasabah_online,
            'menu_aktif'            => 'nasabah_online',
        );
        $res['datakonten'] = $this->load->view('nasabah_online/nasabah_online_blokir', $data, true);
        $this->load->view('layouts/main_view', $res);
      } else {
            $this->session->set_flashdata('gagal', 'Data Tidak Di Temukan');
            redirect(site_url('nasabah_online'));
        }
    
    }
    public function cek()
    {
       echo getkontenall('foto_ktp/200812011506_1597169704334.jpg','nasabah');
    }
    public function create()
    {

        $data = array(
            'tittle'                    => 'Nasabah',
            'judul'                     => 'Nasabah Create',
            'button'                    => 'Create',
            'action'                    => site_url('nasabah_online/create_action'),
            'id_nasabah_online'         => set_value('id_nasabah_online'),
            'id_status_nasabah_online'  => set_value('id_status_nasabah_online'),
            'nama_nasabah_online'       => set_value('nama_nasabah_online'),
            'email_nasabah_online'      => set_value('email_nasabah_online'),
            'no_hp_nasabah_online'      => set_value('no_hp_nasabah_online'),
            'password_nasabah_online'   => set_value('password_nasabah_online'),
        );
        $data['status_nasabah_online'] = $this->Nasabah_online_model->get_stat_nasabah_all();
        $res['datakonten'] = $this->load->view('nasabah_online/nasabah_online_form', $data, true);
        $this->load->view('layouts/main_view', $res);
    }

    public function create_action()
    {
        $this->_rules();
        $dt = new DateTime();
        if ($this->form_validation->run() == FALSE) {
            $this->create();
        } else {
            $data = array(
                'id_status_nasabah_online'  => $this->input->post('id_status_nasabah_online', TRUE),
                'nama_nasabah_online'       => $this->input->post('nama_nasabah_online', TRUE),
                'email_nasabah_online'      => $this->input->post('email_nasabah_online', TRUE),
                'no_hp_nasabah_online'      => $this->input->post('no_hp_nasabah_online', TRUE),
                'password_nasabah_online'   => $this->input->post('password_nasabah_online', TRUE),
                'last_login'                => $dt->format('Y-m-d H:i:s'),
                'tgl_join'                  => $dt->format('Y-m-d H:i:s'),
            );
            $this->Nasabah_online_model->insert($data);
            $this->session->set_flashdata('berhasil', 'Berhasil Menambahkan Data');
            redirect(site_url('nasabah_online'));
        }
    }

    public function update($id)
    {
        $row = $this->Nasabah_online_model->get_by_id($id);

        if ($row) {
            $data = array(
                'button'                    => 'Update',
                'action'                    => site_url('nasabah_online/update_action'),
                'id_nasabah_online'         => set_value('id_nasabah_online', $row->id_nasabah_online),
                'id_status_nasabah_online'  => set_value('id_status_nasabah_online', $row->id_status_nasabah_online),
                'nama_nasabah_online'       => set_value('nama_nasabah_online', $row->nama_nasabah_online),
                'email_nasabah_online'      => set_value('email_nasabah_online', $row->email_nasabah_online),
                'no_hp_nasabah_online'      => set_value('no_hp_nasabah_online', $row->no_hp_nasabah_online),
                'password_nasabah_online'   => set_value('password_nasabah_online', $row->password_nasabah_online),
            );
            $this->load->view('nasabah_online/nasabah_online_form', $data);
        } else {
            $this->session->set_flashdata('gagal', 'Data Tidak Ditemukan');
            redirect(site_url('nasabah_online'));
        }
    }

    public function update_action()
    {
        $this->_rules();
        if ($this->form_validation->run() == FALSE) {
            $this->update($this->input->post('id_nasabah_online', TRUE));
        } else {
            $data = array(
                'id_status_nasabah_online'  => $this->input->post('id_status_nasabah_online', TRUE),
                'nama_nasabah_online'       => $this->input->post('nama_nasabah_online', TRUE),
                'email_nasabah_online'      => $this->input->post('email_nasabah_online', TRUE),
                'no_hp_nasabah_online'      => $this->input->post('no_hp_nasabah_online', TRUE),
                'password_nasabah_online'   => $this->input->post('password_nasabah_online', TRUE),
            );
            $this->Nasabah_online_model->update($this->input->post('id_nasabah_online', TRUE), $data);
            $this->session->set_flashdata('berhasil', 'Berhasil Merubah Data');
            redirect(site_url('nasabah_online'));
        }
    }

    public function delete($id)
    {
        $row = $this->Nasabah_online_model->get_by_id($id);

        if ($row) {
            $this->Nasabah_online_model->delete($id);
            $this->session->set_flashdata('berhasil', 'Berhasil Menghapus Data');
            redirect(site_url('nasabah_online'));
        } else {
            $this->session->set_flashdata('gagal', 'Data Tidak Ditemukan');
            redirect(site_url('nasabah_online'));
        }
    }

    public function _rules()
    {
        $this->form_validation->set_rules('id_status_nasabah_online', 'id status nasabah online', 'trim|required');
        $this->form_validation->set_rules('nama_nasabah_online', 'nama nasabah online', 'trim|required');
        $this->form_validation->set_rules('email_nasabah_online', 'email nasabah online', 'trim|required');
        $this->form_validation->set_rules('no_hp_nasabah_online', 'no hp nasabah online', 'trim|required');
        $this->form_validation->set_rules('id_nasabah_online', 'id_nasabah_online', 'trim');
        $this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

    public function ubahstatus($id)
    {
        $row = $this->Nasabah_online_model->get_by_id($id);
        // 
        if ($row) {
            $data = array(
                'id_status_nasabah_online' => '1',
            );
            $this->Nasabah_online_model->update($id, $data);
            $this->session->set_flashdata('berhasil', 'Berhasil Merubah Data');
            redirect(site_url('nasabah_online/banned'));
        } else {
            $this->session->set_flashdata('gagal', 'Data Tidak Ditemukan');
            redirect(site_url('nasabah_online/banned'));
        }
    }

    public function ubahapprove($id)
    {
        $row = $this->Nasabah_online_model->get_by_id($id);

        if ($row->id_status_nasabah_online == 0) {
            $data = array(
                'id_status_nasabah_online' => '1',
            );
            $this->Nasabah_online_model->update($id, $data);
             $this->Nasabah_online_model->insertdetailnasabah($id);
            $this->session->set_flashdata('berhasil', 'Berhasil Merubah Data');
            redirect(site_url('nasabah_online/approve'));
        } else {
            $this->session->set_flashdata('gagal', 'Data Tidak Ditemukan');
            redirect(site_url('nasabah_online/approve'));
        }
    }

    public function ubahpremium($id)
    {
        $row = $this->Nasabah_online_model->get_by_id($id);

        if ($row->id_status_nasabah_online == 2) {
            $data = array(
                'id_status_nasabah_online' => '3',
            );
            $this->Nasabah_online_model->update($id, $data);
            $this->session->set_flashdata('berhasil', 'Berhasil Merubah Data');
            redirect(site_url('nasabah_online/premium'));
        } else {
            $this->session->set_flashdata('gagal', 'Data Tidak Ditemukan');
            redirect(site_url('nasabah_online/premium'));
        }
    }

    public function ubahbanned($id)
    {
        $row = $this->Nasabah_online_model->get_by_id($id);
 $reason          = $this->input->get('reason');
        if ($row) {
            $data = array(
                'id_status_nasabah_online' => '4',
                'reason_reject' => $reason,
            );
            $this->Nasabah_online_model->update($id, $data);
            $this->session->set_flashdata('berhasil', 'Berhasil Merubah Data');
            redirect(site_url('nasabah_online/banned'));
        } else {
            $this->session->set_flashdata('gagal', 'Data Tidak Ditemukan');
            redirect(site_url('nasabah_online/banned'));
        }
    }

    public function exportxl()
    {
        $q          = $this->input->get('q');
        $subject    = "Nasabah_Online";
        // $id         = $this->session->userdata('uid');
           $this->db->query('SET @no=0');
        $sql        =
            "SELECT @no:=@no+1 AS nomor, 
            `nasabah_online`.nama_nasabah_online,
            `nasabah_online`.no_hp_nasabah_online,
            `nasabah_online`.email_nasabah_online,
            `nasabah_online`.last_login,
            `nasabah_online`.tgl_nasabah_daftar,
            `status_nasabah_online`.`ket_status_nasabah_online` 
            as `namastatus` 
            FROM `nasabah_online` 
            JOIN `status_nasabah_online` 
            ON `nasabah_online`.`id_status_nasabah_online`=`status_nasabah_online`.`id_status_nasabah_online` 
            WHERE `nasabah_online`.`nama_nasabah_online` 
            LIKE '%%' ESCAPE '!' 
            AND `nasabah_online`.`id_status_nasabah_online` = 3 ";
        exportSQL('Nasabah_Online', $subject, $sql);
    }

    public function exportxlapprove()
    {
        $q          = $this->input->get('q');
        $subject    = "Nasabah_Approve";
        // $id         = $this->session->userdata('uid');
           $this->db->query('SET @no=0');
        $sql        =
            "SELECT @no:=@no+1 AS nomor, 
            `nasabah_online`.nama_nasabah_online,
            `nasabah_online`.no_hp_nasabah_online,
            `nasabah_online`.email_nasabah_online,
            `nasabah_online`.last_login,
            `nasabah_online`.tgl_nasabah_daftar,
            `status_nasabah_online`.`ket_status_nasabah_online` 
            as `namastatus` 
            FROM `nasabah_online` 
            JOIN `status_nasabah_online` 
            ON `nasabah_online`.`id_status_nasabah_online`=`status_nasabah_online`.`id_status_nasabah_online` 
            WHERE `nasabah_online`.`nama_nasabah_online` 
            LIKE '%%' ESCAPE '!' 
            AND `nasabah_online`.`id_status_nasabah_online` = 0 ";
        exportSQL('Nasabah_Approve', $subject, $sql);
    }

    public function exportxlberkas()
    {
        $q          = $this->input->get('q');
        $subject    = "Nasabah_Berkas";
        // $id         = $this->session->userdata('uid');
           $this->db->query('SET @no=0');
        $sql        =
            "SELECT @no:=@no+1 AS nomor, 
            `nasabah_online`.nama_nasabah_online,
            `nasabah_online`.no_hp_nasabah_online,
            `nasabah_online`.email_nasabah_online,
            `nasabah_online`.last_login,
            `nasabah_online`.tgl_nasabah_daftar,
            `status_nasabah_online`.`ket_status_nasabah_online` 
            as `namastatus` 
            FROM `nasabah_online` 
            JOIN `status_nasabah_online` 
            ON `nasabah_online`.`id_status_nasabah_online`=`status_nasabah_online`.`id_status_nasabah_online` 
            WHERE `nasabah_online`.`nama_nasabah_online` 
            LIKE '%%' ESCAPE '!' 
            AND `nasabah_online`.`id_status_nasabah_online` = 1 ";
        exportSQL('Nasabah_Berkas', $subject, $sql);
    }

    public function exportxlpremium()
    {
        $q          = $this->input->get('q');
        $subject    = "Nasabah_Premium";
        // $id         = $this->session->userdata('uid');
           $this->db->query('SET @no=0');
        $sql        =
            "SELECT @no:=@no+1 AS nomor, 
            `nasabah_online`.nama_nasabah_online,
            `nasabah_online`.no_hp_nasabah_online,
            `nasabah_online`.email_nasabah_online,
            `nasabah_online`.last_login,
            `nasabah_online`.tgl_nasabah_daftar,
            `status_nasabah_online`.`ket_status_nasabah_online` 
            as `namastatus` 
            FROM `nasabah_online` 
            JOIN `status_nasabah_online` 
            ON `nasabah_online`.`id_status_nasabah_online`=`status_nasabah_online`.`id_status_nasabah_online` 
            WHERE `nasabah_online`.`nama_nasabah_online` 
            LIKE '%%' ESCAPE '!' 
            AND `nasabah_online`.`id_status_nasabah_online` = 2 ";
        exportSQL('Nasabah_Premium', $subject, $sql);
    }

    public function exportxlbanned()
    {
        $q          = $this->input->get('q');
        $subject    = "Nasabah_Banned";
        // $id         = $this->session->userdata('uid');
           $this->db->query('SET @no=0');
        $sql        =
            "SELECT @no:=@no+1 AS nomor, 
            `nasabah_online`.nama_nasabah_online,
            `nasabah_online`.no_hp_nasabah_online,
            `nasabah_online`.email_nasabah_online,
            `nasabah_online`.last_login,
            `nasabah_online`.tgl_nasabah_daftar,
            `status_nasabah_online`.`ket_status_nasabah_online` 
            as `namastatus` 
            FROM `nasabah_online` 
            JOIN `status_nasabah_online` 
            ON `nasabah_online`.`id_status_nasabah_online`=`status_nasabah_online`.`id_status_nasabah_online` 
            WHERE `nasabah_online`.`nama_nasabah_online` 
            LIKE '%%' ESCAPE '!' 
            AND `nasabah_online`.`id_status_nasabah_online` = 4 ";
        exportSQL('Nasabah_Banned', $subject, $sql);
    }

    public function tolak_berkas() {
        $id=$this->input->post('id_nasabah_online');
        $reason=$this->input->post('reason');

        $data = array(
            'id_status_nasabah_online'  => 1,
            'reason_reject'             => $reason,
        );
        $this->Nasabah_online_model->update($id, $data);
        redirect('nasabah_online/detail/'.$id);
    }

  function get_image(){
        $img=$this->input->get('img');
        // echo getkonten($img);
        // $cek='https://kompis-nasabah.s3.ap-southeast-1.amazonaws.com/'.$img;
         // $row = $this->Nasabah_online_model->get_by_poto($cek);
         //  if($row){
          echo getkontenall($img,'nasabah'); 
        // }else{
        //       $this->session->set_flashdata('gagal', 'Gambar Tidak Ditemukan');
        //             redirect('nasabah_online');
        // }
        }
       
     public function cek_ktp_asli()
    {
          $no_ktp=$this->input->get('ktp');
          $nama=$this->input->get('nama');
      $data= $this->Nasabah_online_model->get_ktp_ori($no_ktp,$nama);

         if($data){
        
            echo json_encode($data,200);
        } else{
         
            echo json_encode(['status'=>false,'message'=>'Kombinasi NIK atau Nama tidak ditemnukan'],400);
        }

    }
}

/* End of file Nasabah_online.php */
/* Location: ./application/controllers/Nasabah_online.php */
