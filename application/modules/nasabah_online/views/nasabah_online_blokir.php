<script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>

<div class="row">
	<!-- Left col -->
	<section class="col-lg connectedSortable ui-sortable">
		<!-- Custom tabs (Charts with tabs)-->
		<div class="card">
			<div class="card-body">
				<div class="tab-content p-0">
					
					<table class="table table-bordered table-striped table-condensed" style="margin-bottom: 20px">
						<tr>	
							<td>Status Nasabah</td>
							<td><?php echo $status_nasabah_online; ?></td>
						</tr>
						<tr>
							<td>Nama Nasabah</td>
							<td><?php echo $nama_nasabah_online; ?></td>
						</tr>
						<tr>
							<td>Alamat Nasabah</td>
							<td><?php echo $alamat_nasabah_online; ?></td>
						</tr>
                        <tr>
							<td>Email Nasabah</td>
							<td><?php echo $email_nasabah_online; ?></td>
						</tr>
                        <tr>
							<td>No HP</td>
							<td><?php echo $no_hp_nasabah_online; ?></td>
						</tr>
						<tr>
							<td>Last Login</td>
							<td><?php echo fulldate($last_login); ?></td>
						</tr>
					</table>
                    <div class="form-group">
                            <label for="exampleFormControlSelect1">Alasan Blokir</label>
                            <select name="reason" id="reason" class="form-control">
                            <option value="-1">Pilih Alasan Blokir</option>
                             <?php foreach ($reason as $res): ?>
                                 <option value="<?php echo $res->id_reason; ?>"><?php echo $res->desc_reason; ?></option>
                            <?php endforeach ?>
                            </select>
                        </div>
					<div class="btn-group">
						<a href="<?php echo site_url('nasabah_online') ?>" class="btn btn-primary btn-sm">Batal</a></td>
                        <a style="display: none" id="blokir" href="#" class="btn btn-danger btn-sm" data-toggle="tooltip" data-placement="top" title="Non Aktif" 
                        onclick="confirm('<?=  $id_nasabah_online ?>')" >Blokir</a>
					</div>

				</div>
			</div>
		</div>
	</section>
</div>



<script>
$("#reason"). change(function(){
var cek = $(this). children("option:selected"). val();
if(cek>0){
	$('#blokir').css('display','block');
}else{
$('#blokir').css('display','none');
}
});
function confirm(res) {
    Swal.fire({
    title: 'Anda Yakin Mau Blokir Nasabah?',
    text: "Akun akan segera di Blokir!",
    icon: 'warning',
    showCancelButton: true,
    confirmButtonColor: '#3085d6',
    cancelButtonColor: '#d33',
    confirmButtonText: 'Ya, Blokir!',
    cancelButtonText: 'Batal'
    }).then((result) => {
    if (result.value) {
        Swal.fire(
        'Terblokir!',
        'Akun sudah diblokir.',
        'success'
        )
        window.location='<?php echo base_url().'nasabah_online/ubahbanned/'; ?>'+res+'?reason='+$('#reason').val();
    }
    });
}
</script>