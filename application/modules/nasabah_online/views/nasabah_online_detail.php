<script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>

<div class="row">
	<section class="col-lg connectedSortable ui-sortable">
		<div class="card card-info">
      <div class="card-header">
        <div class="card-title">
          <?= $title_card ?>
        </div>
      </div>
			<div class="card-body">
				<div class="tab-content p-0">

        <div class="row">
          <div class="col-md-8">
            <table class="table table-bordered table-striped table-condensed table-sm">
              <tr><td class="font-weight-bold">Nama Nasabah</td><td><?php
              if($nama_nasabah_online == null){
                echo '<span class="badge badge-danger">Kosong</span>';
              } else {
                echo $nama_nasabah_online;
              }  ?></td></tr>
              <tr><td class="font-weight-bold">Email Nasabah</td><td><?php
              if ($email_nasabah_online == null){
                echo '<span class="badge badge-danger">Kosong</span>';
              } else {
                echo $email_nasabah_online;
              }  ?></td></tr>
              <tr><td class="font-weight-bold">No Hp Nasabah</td><td><?php
              if($no_hp_nasabah_online == null){
                echo '<span class="badge badge-danger">Kosong</span>';
              } else {
                echo $no_hp_nasabah_online;
              }  ?></td></tr>
              <tr><td class="font-weight-bold">Last Login</td><td><?php
              if($last_login == null){
                echo '<span class="badge badge-danger">Kosong</span>';
              } else {
                echo fulldate($last_login);
              }  ?></td></tr>
              <tr><td class="font-weight-bold">Tanggal Join</td><td><?php
              if($tgl_nasabah_daftar == null){
                echo '<span class="badge badge-danger">Kosong</span>';
              } else {
                echo fulldate($tgl_nasabah_daftar);
              }  ?></td></tr>
              <tr><td class="font-weight-bold">Alamat Nasabah</td><td><?php
              if($alamat_nasabah_online == null){
                echo '<span class="badge badge-danger">Kosong</span>';
              } else {
                echo $alamat_nasabah_online;
              }  ?></td></tr>
              <tr><td class="font-weight-bold">Lat</td><td><?php
              if($lat_nasabah_online == null){
                echo '<span class="badge badge-danger">Kosong</span>';
              } else {
                echo $lat_nasabah_online;
              }  ?></td></tr>
              <tr><td class="font-weight-bold">Long</td><td><?php
              if($long_nasabah_online == null){
                echo '<span class="badge badge-danger">Kosong</span>';
              } else {
                echo $long_nasabah_online;
              }  ?></td></tr>
              <tr><td class="font-weight-bold">Nama Provinsi</td><td><?php
              if($nama_provinsi == null){
                echo '<span class="badge badge-danger">Kosong</span>';
              } else {
                echo $nama_provinsi;
              }  ?></td></tr>
              <tr><td class="font-weight-bold">Nama Kabupaten</td><td><?php
              if($nama_kabupaten == null) {
                echo '<span class="badge badge-danger">Kosong</span>';
              } else {
                echo $nama_kabupaten;
              }  ?></td></tr>
              <tr><td class="font-weight-bold">Nama Kecamatan</td><td><?php
              if($nama_kecamatan == null){
                echo '<span class="badge badge-danger">Kosong</span>';
              } else {
                echo $nama_kecamatan;
              }  ?></td></tr>
              <tr><td class="font-weight-bold">No KTP Nasabah</td><td><?php
              if($no_ktp_nasabah_online == null){
                echo '<span class="badge badge-danger">Kosong</span>';
              } else {
                echo $no_ktp_nasabah_online;
              }  ?>
               <button id="cekktp" onclick="cekktpasli('<?= $no_ktp_nasabah_online ?>','<?= $nama_nasabah_online ?>')" class="btn-sm btn-primary float-right" 
                        href=""
                        >Cek Ktp validasi ktp</button>
                
              </td></tr>
              <tr><td class="font-weight-bold">Saldo Nasabah</td><td><?php
              if($saldo_nasabah_online == null){
                echo '<span class="badge badge-danger">Kosong</span>';
              } else {
                echo $saldo_nasabah_online;
              }  ?></td></tr>
              <tr><td class="font-weight-bold">Point Nasabah</td><td><?php
              if($point_nasabah_online == null){
                echo '<span class="badge badge-danger">Kosong</span>';
              } else {
                echo $point_nasabah_online;
              }  ?></td></tr>
            </table>
          </div>
          <div class="col-md-4">
            <ul class="list-group">
                  <li class="list-group-item">
                    <div class="text-center font-weight-bold">
                      Foto KTP Nasabah
                    </div>
                    <div class="text-center">
                    <?php
                      if($foto_ktp_nasabah_online == null){
                        echo '<span class="badge badge-danger">Kosong</span>';
                      } else { ?>
                      <a href="<?= base_url()?>nasabah_online/get_image?img=<?=$foto_ktp_nasabah_online?>" class="image-link">
                        <img src="<?= base_url()?>nasabah_online/get_image?img=<?=$foto_ktp_nasabah_online?>" class="img-fluid" style="max-width: 250px; max-height: 150px;" alt="Foto KTP Nasabah Online">
                      </a>
                      <!-- <a href="<?= base_url()?>nasabah_online/get_image?img=<?=$foto_ktp_nasabah_online?>" data-toggle="lightbox" data-max-width="800" data-title="Foto KTP Nasabah Online">
                        <img src="<?= base_url()?>nasabah_online/get_image?img=<?=$foto_ktp_nasabah_online?>" class="img-fluid" style="max-width: 250px; max-height: 150px;" alt="Foto KTP Nasabah Online">
                      </a> -->
                    <?php  }
                    ?>
                    </div>
                  </li>
                  <li class="list-group-item">
                    <div class="text-center font-weight-bold">
                      Foto Selfie KTP Nasabah
                    </div>
                    <div class="text-center">
                    <?php
                      if($foto_ktp_selfie_nasabah == null){
                        echo '<span class="badge badge-danger">Kosong</span>';
                      } else { ?>
                      <a href="<?= base_url()?>nasabah_online/get_image?img=<?=$foto_ktp_selfie_nasabah?>" class="image-link">
                        <img src="<?= base_url()?>nasabah_online/get_image?img=<?=$foto_ktp_selfie_nasabah?>" class="img-fluid" style="max-width: 250px; max-height: 150px;" alt="Foto KTP Nasabah Online">
                      </a>
                      <!-- <a href="<?= base_url()?>nasabah_online/get_image?img=<?=$foto_ktp_selfie_nasabah?>" data-toggle="lightbox" data-max-width="800" data-title="Foto Selfie KTP Nasabah">
                        <img src="<?= base_url()?>nasabah_online/get_image?img=<?=$foto_ktp_selfie_nasabah?>" class="img-fluid" style="max-width: 250px; max-height: 150px;" alt="Foto KTP Nasabah Online">
                      </a> -->
                    <?php  }
                    ?>
                    </div>
                  </li>
                </ul>
          </div> 
        </div>

		  	<div class="btn-group btn-group-sm">
          <a href="javascript:history.go(-1)" data-toogle="tooltip" title="Kembali" class="btn btn-primary btn-sm" style="min-width: 100px;">
            <i class="far fa-arrow-alt-circle-left"> Batal</i>
          </a>
          <?php if($id_status_nasabah_online!=4){?>
            <?php if($id_status_nasabah_online!=3 && $id_status_nasabah_online!=1){?>
              <a href="#" class="btn btn-warning btn-sm" data-toggle="tooltip" data-placement="top" title="Jadikan Premium" onclick="premium('<?=  $id_nasabah_online ?>')"  style="min-width: 100px;">Premium <i class="fas fa-info-circle"></i></a>
              <button type="button" class="btn btn-default" data-toggle="modal" data-target="#modal-default" style="min-width: 100px;"> Tolak Berkas</button>
            <?php } ?>
            <a href="<?php echo site_url('nasabah_online/blokir/'.$id_nasabah_online); ?>" title="Lihat Detail" class="btn btn-danger btn-sm" style="min-width: 100px;">
            <i class="fas fa-power-off"></i> Blokir</a>
          <?php }else{ ?>
            <a href="#" class="btn btn-success btn-sm" data-toggle="tooltip" data-placement="top" title="Aktifkan Nasabah" onclick="unblokir('<?=  $id_nasabah_online ?>')"  style="min-width: 100px;"><i class="fas fa-check"></i> Aktifkan</a>
          <?php } ?>
			  </div>
   
			</div>
		</div>
	</section>
</div>

<div class="modal fade" id="modal-default">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Tolak Berkas</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
        <form method="post" action="<?php echo site_url('nasabah_online/tolak_berkas')?>" >
          <div class="modal-body">
            <div class="form-group">
                <label for="exampleFormControlSelect1">Alasan Tolak</label>
                <select name="reason" class="form-control">
                <option value="">Pilih Alasan Tolak</option>
                <?php foreach ($reason as $res): ?>
                    <option value="<?php echo $res->id_reason; ?>"><?php echo $res->desc_reason; ?></option>
                <?php endforeach ?>
                </select>
            </div>
            <input type="hidden" name="id_nasabah_online" value="<?= $id_nasabah_online?>">
          </div>
          <div class="modal-footer justify-content-between">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            <button class="btn btn-danger">Tolak Berkas</button>
          </div>
        </form>
    </div>
  </div>
</div>


<script>
  $(function(){
    $('.image-link').magnificPopup({type:'image'});

    $(document).on('click', '[data-toggle="lightbox"]', function(event) {
      event.preventDefault();
      $(this).ekkoLightbox({
        alwaysShowClose: true
      });
    });
  })

function cekktpasli(noktp,nama) {
     // alert(''+noktp+'  '+nama);
      $.ajax({
        url: '<?php echo base_url() ?>nasabah_online/cek_ktp_asli?ktp='+noktp+'&nama='+nama,
          dataType: "json",
        cache: false,
        success: function(res) {
          if(res.message=='failed'){
              alert('Nama dan NIK tidak Sesuai');
          }else{
              alert('Nama dan NIK Sesuai');
          }
        }
      });
  }
function confirm(res) {
    Swal.fire({
    title: 'Anda Yakin Mau Blokir Nasabah?',
    text: "Akun akan segera di Blokir!",
    icon: 'warning',
    showCancelButton: true,
    confirmButtonColor: '#3085d6',
    cancelButtonColor: '#d33',
    confirmButtonText: 'Ya, Blokir!',
    cancelButtonText: 'Batal'
    }).then((result) => {
    if (result.value) {
        Swal.fire(
        'Terblokir!',
        'Akun sudah diblokir.',
        'success'
        )
        window.location='<?php echo base_url().'nasabah_online/ubahbanned/'; ?>'+res;
    }
    });
}

function premium(res) {
    Swal.fire({
    title: 'Anda Yakin mau Premium Akun?',
    text: "Akan segera di Premium kan!",
    icon: 'warning',
    showCancelButton: true,
    confirmButtonColor: '#3085d6',
    cancelButtonColor: '#d33',
    confirmButtonText: 'Ya, Aktifkan!',
    cancelButtonText: 'Batal'
    }).then((result) => {
    if (result.value) {
        Swal.fire(
        'Premium!',
        'Akun sudah Premium.',
        'success'
        )
        window.location='<?php echo base_url().'nasabah_online/ubahpremium/'; ?>'+res;
    }
    });
}

function unblokir(res) {
    Swal.fire({
    title: 'Anda Yakin Mau Unblokir Nasabah?',
    text: "Akun akan segera di Unblokir!",
    icon: 'warning',
    showCancelButton: true,
    confirmButtonColor: '#3085d6',
    cancelButtonColor: '#d33',
    confirmButtonText: 'Ya, Aktifkan!',
    cancelButtonText: 'Batal'
    }).then((result) => {
    if (result.value) {
        Swal.fire(
        'Aktif!',
        'Akun sudah di Unblokir.',
        'success'
        )
        window.location='<?php echo base_url().'nasabah_online/ubahstatus/'; ?>'+res;
    }
    });
}
</script>