<script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>

<div class="row">
	<section class="col-lg connectedSortable ui-sortable">
		<div class="card card-info">
            <div class="card-header">
                <div class="card-title">
                  <?php echo $title_card?>
                </div>
            </div>
			<div class="card-body">
				<div class="tab-content p-0">
        <div class="row">
            <div class="col-md-5 mb-2">
            <div class="tab-content p-0" style="overflow:auto">
                <div class="btn-group">
                    <?php echo anchor(site_url('nasabah_online'),'Nasabah', 'class="btn btn-default btn-sm" data-toggle="tooltip" data-placement="top" title="List Semua Nasabah"'); ?>
                    <?php echo anchor(site_url('nasabah_online/approve'),'Approve', 'class="btn btn-success btn-sm" data-toggle="tooltip" data-placement="top" title="List Perlu di Approve"'); ?>
                    <?php echo anchor(site_url('nasabah_online/berkas'),'Berkas', 'class="btn btn-default btn-sm" data-toggle="tooltip" data-placement="top" title="List Belum Upload Berkas"'); ?>
                    <?php echo anchor(site_url('nasabah_online/premium'),'Premium', 'class="btn btn-default btn-sm" data-toggle="tooltip" data-placement="top" title="List Perlu di Premium"'); ?>
                    <?php echo anchor(site_url('nasabah_online/banned'),'Blokir', 'class="btn btn-default btn-sm" data-toggle="tooltip" data-placement="top" title="List Blokir"'); ?>
                </div>
            </div>
            </div>
            <div class="col-md-3 offset-md-4 mb-2">
                <?= search(site_url('nasabah_online/approve'), site_url('nasabah_online/approve'), $q) ?>
            </div>
        </div>
        <div class="tab-content p-0" style="overflow:auto">
        <table class="table table-bordered table-striped table-hover table-sm" style="margin-bottom: 10px">
            <tr>
                <th class="text-center" widht="20px">No </th>
                <th class="text-center">Nama </th>
                <th class="text-center">Email </th>
                <th class="text-center">No Hp </th>
                <th class="text-center">Tanggal Join </th>
                <th class="text-center">Status </th>
                <th class="text-center">Aksi</th>
            </tr>
            <?php 
            if($total_rows == 0){
                echo '<tr><td colspan="10" bgcolor="grey" style="color: white; font-weight: bold; text-align: center;">Data Tidak Ditemukan</td></tr>';
            } else {
            foreach ($nasabah_online_data as $nasabah_online){?>
            <tr>
                <td class="text-center"><?php echo ++$start ?></td>
                <td><?php echo $nasabah_online->nama_nasabah_online ?></td>
                <td><?php echo $nasabah_online->email_nasabah_online ?></td>
                <td><?php echo $nasabah_online->no_hp_nasabah_online ?></td>
                <td class="text-center"><?php echo fulldate($nasabah_online->tgl_nasabah_daftar) ?></td>
                <td class="text-center">
                    <?php if($nasabah_online->namastatus == '0'){
                        echo '<span class="badge badge-danger">Non Aktif</span>';
                    } else {
                        echo '<span class="badge badge-success">Belum Verifikasi Email</span>';
                    }
                    ?>
                </td>
                <td class="text-center" width="50px">
                    <div class="btn-group">
                        <a href="#" class="btn btn-success btn-sm" data-toggle="tooltip" data-placement="top" title="Approve" onclick="confirm('<?=  $nasabah_online->id_nasabah_online ?>')" ><i class="fas fa-check fa-sm"></i></a>
                    </div>
                </td>
		    </tr>
            <?php } 
            } ?>
        </table>
        </div>
        <?= footer($total_rows, $pagination, site_url('nasabah_online/exportxlapprove?q=' . $q)) ?>
			</div>
		</div>
	</section>
</div>

<script>
function confirm(res) {
    Swal.fire({
    title: 'Anda Yakin Mau Approve Nasabah?',
    text: "Akan segera di Approve!",
    icon: 'warning',
    showCancelButton: true,
    confirmButtonColor: '#3085d6',
    cancelButtonColor: '#d33',
    confirmButtonText: 'Approve!',
    cancelButtonText: 'Batal'
    }).then((result) => {
    if (result.value) {
        Swal.fire(
        'Aktif!',
        'Akun sudah di Approve.',
        'success'
        )
        window.location='<?php echo base_url().'nasabah_online/ubahapprove/'; ?>'+res;
    }
    });
}
</script>