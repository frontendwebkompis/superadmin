<script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>

<div class="row">
	<!-- Left col -->
	<section class="col-lg connectedSortable ui-sortable">
		<!-- Custom tabs (Charts with tabs)-->
		<div class="card card-info">
            <div class="card-header">
                <div class="card-title">
                  <?php echo $title_card?>
                </div>
              </div>
            <div class="card-body">
            
				<div class="tab-content p-0">
        
        <div class="row">

            <div class="col-md-5 mb-2">
            <div class="tab-content p-0" style="overflow:auto">
                <div class="btn-group">
                    <?php echo anchor(site_url('nasabah_online'),'Nasabah', 'class="btn btn-info btn-sm" data-toggle="tooltip" data-placement="top" title="List Semua Nasabah"'); ?>
                    <?php echo anchor(site_url('nasabah_online/approve'),'Approve', 'class="btn btn-default btn-sm" data-toggle="tooltip" data-placement="top" title="List Perlu di Approve"'); ?>
                    <?php echo anchor(site_url('nasabah_online/berkas'),'Berkas', 'class="btn btn-default btn-sm" data-toggle="tooltip" data-placement="top" title="List Belum Upload Berkas"'); ?>
                    <?php echo anchor(site_url('nasabah_online/premium'),'Premium', 'class="btn btn-default btn-sm" data-toggle="tooltip" data-placement="top" title="List Perlu di Premium"'); ?>
                    <?php echo anchor(site_url('nasabah_online/banned'),'Blokir', 'class="btn btn-default btn-sm" data-toggle="tooltip" data-placement="top" title="List Blokir"'); ?>
                </div>
            </div>
            </div>
            <div class="col-md-3 offset-md-4 mb-2">
                <?= search(site_url('nasabah_online/index'), site_url('nasabah_online'), $q) ?>
            </div>
        </div>
        <div class="tab-content p-0" style="overflow:auto">
        <table id="example2"  class="table table-bordered table-striped table-condensed table-hover table-sm" style="margin-bottom: 10px">
               <thead>
            <tr>
                <th class="text-center" width="10px">No</th>
                <th class="text-center">Nama </th>
                <th class="text-center">Email </th>
                <th class="text-center">No Hp </th>
                <th class="text-center">Last Login </th>
                <th class="text-center">Tanggal Join </th>
                <th class="text-center">Status </th>
                <th class="text-center" width="100px">Aksi</th>
            </tr>
                </thead>
                <tbody>
            <?php 
            if($total_rows == 0){
                echo '<tr><td colspan="10" bgcolor="grey" style="color: white; font-weight: bold; text-align: center;">Data Tidak Ditemukan</td></tr>';
            } else {
            foreach ($nasabah_online_data as $nasabah_online) { ?>
            <tr>
                <td class="text-center"><?php echo ++$start ?></td>
                <td><?php echo $nasabah_online->nama_nasabah_online ?></td>
                <td><?php echo $nasabah_online->email_nasabah_online ?></td>
                <td><?php echo $nasabah_online->no_hp_nasabah_online ?></td>
                <td class="text-center"><?php echo fulldate($nasabah_online->last_login) ?></td>
                <td class="text-center"><?php echo fulldate($nasabah_online->tgl_nasabah_daftar) ?></td>
                <td class="text-center">
                    <?php if($nasabah_online->namastatus == '0'){
                        echo '<span class="badge badge-danger">Non Aktif</span>';
                    } else {
                        echo '<span class="badge badge-info">Sudah Verifikasi(premium)</span>';
                    }
                    ?>
                </td>
                <td class="text-center">
                    <div class="btn-group">
                        <a href="<?php echo site_url('nasabah_online/detail/'.$nasabah_online->id_nasabah_online); ?>"
                        data-toogle="tooltip" title="Lihat Detail">
                        <button type="button" class="btn btn-info btn-sm"><i class="fas fa-info-circle fa-sm"></i></button></a>
                        <a href="<?php echo site_url('nasabah_online/blokir/'.$nasabah_online->id_nasabah_online); ?>"
                        data-toogle="tooltip" title="Lihat Detail">
                        <button type="button" class="btn btn-danger btn-sm"><i class="fas fa-power-off fa-sm"></i></button></a>
                    </div>
                </td>
	        </tr>
                <?php } 
                }   ?>
            </tbody>
        </table>
        </div>
        <?= footer($total_rows, $pagination, site_url('nasabah_online/exportxl?q=' . $q)) ?>
        </div>
			</div>
		</div>
	</section>
</div>


<script>
function confirm(res) {
    Swal.fire({
    title: 'Anda Yakin Mau Blokir Nasabah?',
    text: "Akun akan segera di Blokir!",
    icon: 'warning',
    showCancelButton: true,
    confirmButtonColor: '#3085d6',
    cancelButtonColor: '#d33',
    confirmButtonText: 'Ya, Blokir!',
    cancelButtonText: 'Batal'
    }).then((result) => {
    if (result.value) {
        Swal.fire(
        'Terblokir!',
        'Akun sudah diblokir.',
        'success'
        )
        window.location='<?php echo base_url().'nasabah_online/ubahbanned/'; ?>'+res;
    }
    });
}
</script>