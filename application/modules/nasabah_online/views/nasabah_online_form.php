<div class="row">
	<section class="col-lg connectedSortable ui-sortable">
		<div class="card">
			<div class="card-body">
				<div class="tab-content p-0">
        <form action="<?php echo $action; ?>" method="post">
	    <div class="form-group">
            <label for="varchar">Status Nasabah <?php echo form_error('id_status_nasabah_online') ?></label>
            <select id="id_status_nasabah_online" class="form-control" name="id_status_nasabah_online">
            <option value="">Pilih Kategori</option>
            <?php foreach ($status_nasabah_online as $stat) { ?>
            <option  <?=  $stat->id_status_nasabah_online ==  $id_status_nasabah_online ? 'selected' : ''; ?> value="<?php echo $stat->id_status_nasabah_online; ?>">
            <?php echo $stat->ket_status_nasabah_online; ?>
            </option>
            <?php }
            ?>
            </select>
        </div>
	    <div class="form-group">
            <label for="varchar">Nama Nasabah <?php echo form_error('nama_nasabah_online') ?></label>
            <input type="text" class="form-control" name="nama_nasabah_online" id="nama_nasabah_online" placeholder="Nama Nasabah Online" value="<?php echo $nama_nasabah_online; ?>" />
        </div>
	    <div class="form-group">
            <label for="varchar">Email Nasabah <?php echo form_error('email_nasabah_online') ?></label>
            <input type="text" class="form-control" name="email_nasabah_online" id="email_nasabah_online" placeholder="Email Nasabah Online" value="<?php echo $email_nasabah_online; ?>" />
        </div>
	    <div class="form-group">
            <label for="varchar">No Hp Nasabah <?php echo form_error('no_hp_nasabah_online') ?></label>
            <input type="text" onkeypress="return Angkasaja(event)" wclass="form-control" name="no_hp_nasabah_online" id="no_hp_nasabah_online" placeholder="No Hp Nasabah Online" value="<?php echo $no_hp_nasabah_online; ?>" />
        </div>
	    <input type="hidden" name="id_nasabah_online" value="<?php echo $id_nasabah_online; ?>" /> 
	    <button type="submit" class="btn btn-primary">Simpan</button> 
	    <a href="<?php echo site_url('nasabah_online') ?>" class="btn btn-default">Batal</a>
	</form>
    </div>
		</div>
	</section>
</div>

<script type="text/javascript">
    function Angkasaja(evt) {
    var charCode = (evt.which) ? evt.which : event.keyCode
    if (charCode > 31 && (charCode < 48 || charCode > 57))
    return false;
    return true;
    }
</script>