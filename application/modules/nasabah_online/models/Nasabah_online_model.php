<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Nasabah_online_model extends CI_Model
{

    public $table = 'nasabah_online';
    public $id = 'id_nasabah_online';
    public $order = 'DESC';

    function __construct()
    {
        parent::__construct();
    }

    // get all
    function get_all()
    {
        $this->db->order_by($this->id, $this->order);
        return $this->db->get($this->table)->result();
    }
    function get_reason($cek=null)
    {
        if($cek==1){
 $this->db->where('id_proses', 1);
      
        }else{
            $this->db->where('id_proses', 8);
        }
 
        return $this->db->get('reason')->result();
    }
    function get_stat_nasabah_all()
    {
        return $this->db->get('status_nasabah_online')->result();
    }
    // get data by id
    function get_by_id($id)
    {
        $this->db->where($this->id, $id);
        return $this->db->get($this->table)->row();
    }
      // get data by id nasabah detail
      function get_by_id_nasabah_detail($id)
      {
        $query = $this->db->query("SELECT nasabah_online.id_nasabah_online,nasabah_online.id_status_nasabah_online,status_nasabah_online.ket_status_nasabah_online,nama_nasabah_online,nasabah_online.email_nasabah_online,nasabah_online.no_hp_nasabah_online,nasabah_online.last_login,nasabah_online.tgl_nasabah_daftar,nasabah_online_detail.alamat_nasabah_online,nasabah_online_detail.lat_nasabah_online,nasabah_online_detail.long_nasabah_online,provinsi.nama_provinsi,kabupaten.nama_kabupaten,kecamatan.nama_kecamatan,nasabah_online_detail.no_ktp_nasabah_online,nasabah_online_detail.foto_ktp_nasabah_online,nasabah_online_detail.foto_ktp_selfie_nasabah_online,nasabah_online_detail.saldo_nasabah_online,nasabah_online_detail.point_nasabah_online FROM nasabah_online
        LEFT JOIN status_nasabah_online ON status_nasabah_online.id_status_nasabah_online=nasabah_online.id_status_nasabah_online
        RIGHT JOIN nasabah_online_detail ON nasabah_online_detail.id_nasabah_online=nasabah_online.id_nasabah_online 
        LEFT JOIN provinsi ON provinsi.id_provinsi=nasabah_online_detail.id_provinsi
        LEFT JOIN kabupaten ON kabupaten.id_kabupaten=nasabah_online_detail.id_kabupaten
        LEFT JOIN kecamatan ON kecamatan.id_kecamatan=nasabah_online_detail.id_kecamatan        
        where nasabah_online.id_nasabah_online='".$id."'");

       return $query->row();
      }
    // get total rows
    function total_rows($q = NULL) {
        $this->db->like('id_nasabah_online', $q);
        $this->db->or_like('id_status_nasabah_online', $q);
        $this->db->or_like('nama_nasabah_online', $q);
        $this->db->or_like('email_nasabah_online', $q);
        $this->db->or_like('no_hp_nasabah_online', $q);
        $this->db->or_like('password_nasabah_online', $q);
        $this->db->from($this->table);
        return $this->db->count_all_results();
    }

     
    // get data with limit and search
    function get_limit_data($limit, $start = 0, $q = NULL) {
      
        $this->db->like('nasabah_online.id_nasabah_online', $q);
        $this->db->or_like('nasabah_online.id_status_nasabah_online', $q);
        $this->db->or_like('nasabah_online.nama_nasabah_online', $q);
        $this->db->or_like('nasabah_online.email_nasabah_online', $q);
        $this->db->or_like('nasabah_online.no_hp_nasabah_online', $q);
        $this->db->order_by($this->id, $this->order);
        $this->db->limit($limit, $start);
        $this->db->join('status_nasabah_online', 'nasabah_online.id_status_nasabah_online=status_nasabah_online.id_status_nasabah_online');
        $this->db->select('
            nasabah_online.*,
            status_nasabah_online.ket_status_nasabah_online as namastatus,

            
        ');
        
        return $this->db->get($this->table)->result();
    }

//untuk halaman approve nasabah
    // get total rows
    function total_rows_berkas($q = NULL) {
        
        $this->db->or_like('nama_nasabah_online', $q);
        $this->db->where('id_status_nasabah_online', 1);
        $this->db->from($this->table);
            return $this->db->count_all_results();
        }
    
      // get data with limit and search
      function get_limit_data_berkas($limit, $start = 0, $q = NULL) {
        
	    $this->db->like($this->table.'.nama_nasabah_online', $q);
        $this->db->where($this->table.'.id_status_nasabah_online', 1);
        $this->db->order_by($this->table.'.tgl_nasabah_daftar', $this->order);
        $this->db->limit($limit, $start);
        $this->db->join('status_nasabah_online', 'nasabah_online.id_status_nasabah_online=status_nasabah_online.id_status_nasabah_online');
        $this->db->select('
            nasabah_online.*,
            status_nasabah_online.ket_status_nasabah_online as namastatus,

            
        ');
        return $this->db->get($this->table)->result();
    }
//untuk halaman approve nasabah
    // get total rows
    function total_rows_jadikan_premium($q = NULL) {
        
        $this->db->or_like('nama_nasabah_online', $q);
        $this->db->where('id_status_nasabah_online', 2);
        $this->db->from($this->table);
            return $this->db->count_all_results();
        }
    
      // get data with limit and search
      function get_limit_data_jadikan_premium($limit, $start = 0, $q = NULL) {
        
	    $this->db->like($this->table.'.nama_nasabah_online', $q);
        $this->db->where($this->table.'.id_status_nasabah_online', 2);
        $this->db->order_by($this->table.'.tgl_nasabah_daftar', $this->order);
        $this->db->limit($limit, $start);
        $this->db->join('status_nasabah_online', 'nasabah_online.id_status_nasabah_online=status_nasabah_online.id_status_nasabah_online');
        $this->db->select('
            nasabah_online.*,
            status_nasabah_online.ket_status_nasabah_online as namastatus,

            
        ');
        return $this->db->get($this->table)->result();
    }
    //batasan approve
//untuk halaman approve nasabah
    // get total rows
    function total_rows_approve($q = NULL) {
        
        $this->db->or_like('nama_nasabah_online', $q);
        $this->db->where('id_status_nasabah_online', 0);
        $this->db->from($this->table);
            return $this->db->count_all_results();
        }
    
      // get data with limit and search
      function get_limit_data_approve($limit, $start = 0, $q = NULL) {
        
	    $this->db->like($this->table.'.nama_nasabah_online', $q);
        $this->db->where($this->table.'.id_status_nasabah_online', 0);
        $this->db->order_by($this->table.'.tgl_nasabah_daftar', $this->order);
        $this->db->limit($limit, $start);
        $this->db->join('status_nasabah_online', 'nasabah_online.id_status_nasabah_online=status_nasabah_online.id_status_nasabah_online');
        $this->db->select('
            nasabah_online.*,
            status_nasabah_online.ket_status_nasabah_online as namastatus,

            
        ');
        return $this->db->get($this->table)->result();
    }
    //batasan approve


    //untuk halaman premium nasabah
    // get total rows
    function total_rows_premium($q = NULL) {
        
        $this->db->or_like('nama_nasabah_online', $q);
        $this->db->where('id_status_nasabah_online', 3);
        $this->db->from($this->table);
        return $this->db->count_all_results();
    }
        
          // get data with limit and search
    function get_limit_data_premium($limit, $start = 0, $q = NULL) {
            
        $this->db->like($this->table.'.nama_nasabah_online', $q);
        $this->db->where($this->table.'.id_status_nasabah_online', 3);
        $this->db->order_by('nasabah_online.tgl_nasabah_daftar', $this->order);
        $this->db->limit($limit, $start);
        $this->db->join('status_nasabah_online', 'nasabah_online.id_status_nasabah_online=status_nasabah_online.id_status_nasabah_online');
        $this->db->select('
            nasabah_online.*,
            status_nasabah_online.ket_status_nasabah_online as namastatus,

            
        ');
        return $this->db->get($this->table)->result();
    }
        //batasan premium
    //untuk halaman premium nasabah
    // get total rows
    function total_rows_banned($q = NULL) {
        
     $this->db->like($this->table.'.nama_nasabah_online', $q);
        $this->db->where($this->table.'.id_status_nasabah_online', 4);
        $this->db->order_by($this->id, $this->order);
         $this->db->join('status_nasabah_online', 'nasabah_online.id_status_nasabah_online=status_nasabah_online.id_status_nasabah_online');
        $this->db->join('reason', 'nasabah_online.reason_reject=reason.id_reason','left');
              $this->db->from($this->table);
        return $this->db->count_all_results();
    }
        
          // get data with limit and search
    function get_limit_data_banned($limit, $start = 0, $q = NULL) {
            
        $this->db->like($this->table.'.nama_nasabah_online', $q);
        $this->db->where($this->table.'.id_status_nasabah_online', 4);
        $this->db->order_by($this->table.'.tgl_nasabah_daftar', $this->order);
        $this->db->limit($limit, $start);
        $this->db->join('status_nasabah_online', 'nasabah_online.id_status_nasabah_online=status_nasabah_online.id_status_nasabah_online');
        $this->db->join('reason', 'nasabah_online.reason_reject=reason.id_reason','left');
        $this->db->select('
            nasabah_online.*,
            status_nasabah_online.ket_status_nasabah_online as namastatus,desc_reason
    
        ');
        return $this->db->get($this->table)->result();
    }
        //batasan premium

    // insert data
    function insert($data)
    {
        $this->db->insert($this->table, $data);
    }
  function insertdetailnasabah($id)
    {
        $data = array(
            'id_nasabah_online' => $id,
        );
        $this->db->insert('nasabah_online_detail', $data);
    }
    // update data
    function update($id, $data)
    {
        $this->db->where($this->id, $id);
        $this->db->update($this->table, $data);
    }

    // delete data
    function delete($id)
    {
        $this->db->where($this->id, $id);
        $this->db->delete($this->table);
    }

      public function get_ktp_ori($no_ktp, $nama)
    {
        $dt = new DateTime();
        try{
          // $apiHost ='http://api.shipping.esoftplay.com';
        
         $apiHost ='http://api.binderbyte.com/cekktp';
        $key ='e4f44aaa99629e4947818b511afb35aadc3d70959f2c8b02498e2c6e4f364311';
       $client = new GuzzleHttp\Client(['base_uri' => $apiHost,]);

        $data = $client->request('GET', '?api_key=' . $key.'&nik='.$no_ktp.'&nama='.$nama ,[
              
            'http_errors' => false
        ]);
        // if($data->getStatusCode()==200){
        $datas = $data->getBody();
        $data= json_decode($datas->getContents());
       
          return $data;
    
      
       } catch(RequestException $e) {
            $response_s = $e->getResponse()->getBody();
            return $response_s;
        }

        catch(ClientException $ce){
            $response_s = $e->getResponse()->getBody();
            return $response_s;
}
      
    } 

}

/* End of file Nasabah_online_model.php */
/* Location: ./application/models/Nasabah_online_model.php */
/* Please DO NOT modify this information : */
/* Generated by Harviacode Codeigniter CRUD Generator 2020-02-13 05:05:46 */
/* http://harviacode.com */