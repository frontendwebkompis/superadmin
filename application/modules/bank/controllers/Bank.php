<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Bank extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        if (cek_token()) {
            $this->load->model('Bank_model');
            $this->load->library('form_validation');
            $this->load->library('upload');
            $this->load->library('image_lib');
        } else {
            logout();
        }
    }

    public function index()
    {
        $q = urldecode($this->input->get('q', TRUE));
        $start = intval($this->input->get('start'));

        if ($q <> '') {
            $config['base_url'] = base_url() . 'bank/?q=' . urlencode($q);
            $config['first_url'] = base_url() . 'bank/?q=' . urlencode($q);
        } else {
            $config['base_url'] = base_url() . 'bank/';
            $config['first_url'] = base_url() . 'bank/';
        }

        $config['per_page'] = 10;
        $config['page_query_string'] = TRUE;
        $config['total_rows'] = $this->Bank_model->total_rows($q);
        $bank = $this->Bank_model->get_limit_data($config['per_page'], $start, $q);

        $this->load->library('pagination');
        $this->pagination->initialize($config);

        $data = array(
            'tittle'        => 'Bank',
            'judul'         => 'Bank',
            'title_card'    => 'List Bank',
            'bank_data'     => $bank,
            'q'             => $q,
            'pagination'    => $this->pagination->create_links(),
            'total_rows'    => $config['total_rows'],
            'start'         => $start,
            'bank_aktif'    => '1',
            'menu_aktif'    => 'bank'
        );
        $res['datakonten'] = $this->load->view('bank/bank_list', $data, true);
        $this->load->view('layouts/main_view', $res);
    }

    public function detail($id)
    {
        $row = $this->Bank_model->get_by_id($id);
        if ($row) {
            $data = array(
                'tittle'        => 'Bank',
                'judul'         => 'Bank',
                'title_card'    => 'Bank Detail',
                'id_bank'       => $row->id_bank,
                'nama_bank'     => $row->nama_bank,
                'kode_bank'     => $row->kode_bank,
                'logo_bank'     => $row->logo_bank,
                'bank_aktif'    => '1',
                'menu_aktif'    => 'bank'
            );
            $res['datakonten'] = $this->load->view('bank/bank_detail', $data, true);
            $this->load->view('layouts/main_view', $res);
        } else {
            $this->session->set_flashdata('gagal', 'Data Tidak Di Temukan');
            redirect(site_url('bank'));
        }
    }

    public function create()
    {
        $data = array(
            'tittle'        => 'Bank',
            'judul'         => 'Bank',
            'title_card'    => 'Tambah Bank',
            'button'        => 'Create',
            'action'        => site_url('bank/create_action'),
            'id_bank'       => set_value('id_bank'),
            'nama_bank'     => set_value('nama_bank'),
            'kode_bank'     => set_value('kode_bank'),
            'logo_bank'     => set_value('logo_bank'),
            'bank_aktif'    => '1',
            'menu_aktif'    => 'bank'
        );
        $res['datakonten'] = $this->load->view('bank/bank_form', $data, true);
        $this->load->view('layouts/main_view', $res);
    }

    public function create_action()
    {
        $this->_rules();

        $prefix = "BANK";
        // echo $gabung;
        $row = $this->Bank_model->get_by_prefix($prefix);
        $NoUrut = (int) substr($row->max, 4, 4);
        $NoUrut = $NoUrut + 1; //nomor urut +1
        $NoUrut = sprintf('%04d', $NoUrut);
        $fix = $prefix . $NoUrut;

        if ($this->form_validation->run() == FALSE) {
            $this->create();
        } else {

            if (!file_exists('./assets/gambar/bank')) {
                mkdir('./assets/gambar/bank', 0777, true);
            }

            $config['upload_path']     = './assets/gambar/bank/';   //path folder
            $config['allowed_types']   = 'gif|jpg|png|jpeg|bmp';                                      //type yang dapat diakses bisa anda sesuaikan
            $config['encrypt_name']    = TRUE;                                                        //Enkripsi nama yang terupload
            $this->upload->initialize($config);
            if (!empty($_FILES['logo_bankbaru']['name'])) {
                if ($this->upload->do_upload('logo_bankbaru', TRUE)) {
                    $gbr = $this->upload->data();
                    //Compress Image
                    $config['image_library']   = 'gd2';
                    $config['source_image']    = './assets/gambar/bank/' . $gbr['file_name'];
                    $config['create_thumb']    = FALSE;
                    $config['maintain_ratio']  = FALSE;
                    $config['quality']         = '50%';
                    $config['width']           = 600;
                    $config['height']          = 400;
                    $config['new_image']       = './assets/gambar/' . $gbr['file_name'];
                    $this->load->library('image_lib', $config);
                    $this->image_lib->resize();
                    $upload = s3_upload('bank/' . $gbr['file_name'], $gbr['full_path']);
                    @unlink('assets/gambar/bank/' . $gbr['file_name']);
                    $gambar1 = $upload['ObjectURL'];
                }
            } else {
                $gambar1 = 'kosong';
                // echo "Image Selfi yang diupload kosong ";
            }

            $data = array(
                'id_bank'               => $fix,
                'nama_bank'             => $this->input->post('nama_bank', TRUE),
                'kode_bank'             => $this->input->post('kode_bank', TRUE),
                'logo_bank'             => $gambar1,
                'id_superadmin'         => getID(),
                'status_bank_remove'    => 1,
                'created_at'            => @date('Y-m-d H:i:s'),
            );

            $this->Bank_model->insert($data);
            $this->session->set_flashdata('berhasil', 'Berhasil Menambahkan Data');
            redirect(site_url('bank'));
        }
    }

    public function update($id)
    {
        $row = $this->Bank_model->get_by_id($id);

        if ($row) {
            $data = array(
                'tittle'        => 'Bank',
                'judul'         => 'Bank',
                'title_card'    => 'Update Bank',
                'button'        => 'Update',
                'action'        => site_url('bank/update_action'),
                'id_bank'       => set_value('id_bank', $row->id_bank),
                'nama_bank'     => set_value('nama_bank', $row->nama_bank),
                'kode_bank'     => set_value('kode_bank', $row->kode_bank),
                'logo_bank'     => set_value('logo_bank', $row->logo_bank),
                'bank_aktif'    => '1',
                'menu_aktif'    => 'bank'
            );
            $res['datakonten'] = $this->load->view('bank/bank_form', $data, true);
            $this->load->view('layouts/main_view', $res);
        } else {
            $this->session->set_flashdata('gagal', 'Data Tidak Ditemukan');
            redirect(site_url('bank'));
        }
    }

    public function update_action()
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->update($this->input->post('id_bank', TRUE));
        } else {
            if (!file_exists('./assets/gambar/bank')) {
                mkdir('./assets/gambar/bank', 0777, true);
            }

            $config['upload_path']     = './assets/gambar/bank/';   //path folder
            $config['allowed_types']   = 'gif|jpg|png|jpeg|bmp';                                      //type yang dapat diakses bisa anda sesuaikan
            $config['encrypt_name']    = TRUE;                                                        //Enkripsi nama yang terupload
            $this->upload->initialize($config);
            if (!empty($_FILES['logo_bankbaru']['name'])) {
                if ($this->upload->do_upload('logo_bankbaru', TRUE)) {
                    $gbr = $this->upload->data();
                    //Compress Image
                    $config['image_library']   = 'gd2';
                    $config['source_image']    = './assets/gambar/bank/' . $gbr['file_name'];
                    $config['create_thumb']    = FALSE;
                    $config['maintain_ratio']  = FALSE;
                    $config['quality']         = '50%';
                    $config['width']           = 600;
                    $config['height']          = 400;
                    $config['new_image']       = './assets/gambar/' . $gbr['file_name'];
                    $this->load->library('image_lib', $config);
                    $this->image_lib->resize();
                    $upload = s3_upload('bank/' . $gbr['file_name'], $gbr['full_path']);
                    del('bank/' . basename($this->input->post('logo_bank')));
                    @unlink('assets/gambar/bank/' . $gbr['file_name']);
                    $gambar1 = $upload['ObjectURL'];
                }
            } else {
                $gambar1 = $this->input->post('logo_bank', TRUE);
                // echo "Image Selfi yang diupload kosong ";
            }
            $data = array(
                'nama_bank' => $this->input->post('nama_bank', TRUE),
                'kode_bank' => $this->input->post('kode_bank', TRUE),
                'logo_bank' => $gambar1,
                'id_superadmin' => getID(),

                'update_at'                         => @date('Y-m-d H:i:s'),
            );

            $this->Bank_model->update($this->input->post('id_bank', TRUE), $data);
            $this->session->set_flashdata('berhasil', 'Berhasil Merubah Data');
            redirect(site_url('bank'));
        }
    }

    public function del_sem($id)
    {
        $row = $this->Bank_model->get_by_id($id);

        if ($row) {
            del('bank/' . basename($row->logo_bank));

            $data = array(
                'status_bank_remove' => 0,
                'id_superadmin' => getID(),
                'update_at'                         => @date('Y-m-d H:i:s'),

            );

            $this->Bank_model->update($id, $data);
            $this->session->set_flashdata('berhasil', 'Berhasil Menghapus Data');
            redirect(site_url('bank'));
        } else {
            $this->session->set_flashdata('gagal', 'Data Tidak Ditemukan');
            redirect(site_url('bank'));
        }
    }
    public function delete($id)
    {
        $row = $this->Bank_model->get_by_id($id);

        if ($row) {
            $this->Bank_model->delete($id);
            $this->session->set_flashdata('berhasil', 'Berhasil Menghapus Data');
            redirect(site_url('bank'));
        } else {
            $this->session->set_flashdata('gagal', 'Data Tidak Ditemukan');
            redirect(site_url('bank'));
        }
    }

    public function _rules()
    {
        $this->form_validation->set_rules('nama_bank', 'nama bank', 'trim|required');
        $this->form_validation->set_rules('kode_bank', 'kode bank', 'trim|required');

        $this->form_validation->set_rules('id_bank', 'id_bank', 'trim');
        $this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }
    function get_image()
    {

        $img = $this->input->get('img');
        // echo getkonten($img);
        $cek = 'https://kompis-bank-sampah.s3.ap-southeast-1.amazonaws.com/' . $img;
        $row = $this->Bank_model->get_by_poto($cek);
        // if($row){
        echo getkonten($img);
        // }else{
        //       $this->session->set_flashdata('gagal', 'Gambar Tidak Ditemukan');
        //             redirect('bank');
        // }
    }
}

/* End of file Bank.php */
/* Location: ./application/controllers/Bank.php */
/* Please DO NOT modify this information : */
/* Generated by Harviacode Codeigniter CRUD Generator 2020-02-12 03:10:31 */
/* http://harviacode.com */
