<script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>

<div class="row">
	<!-- Left col -->
	<section class="col-lg connectedSortable ui-sortable">
		<!-- Custom tabs (Charts with tabs)-->
		<div class="card card-info">
			<div class="card-header">
                <div class="card-title">
                  <?php echo $title_card?>
                </div>
              </div>
			<div class="card-body">
				<div class="tab-content p-0">

					<div class="row" style="margin-bottom: 10px">
						<div class="col-md-4">
							<?php echo anchor(site_url('bank/create'),'Tambah Data', 'class="btn btn-info btn-sm"'); ?>
						</div>
						<div class="col-md-4 text-center">
							<div style="margin-top: 8px" id="message">
								<?php echo $this->session->userdata('message') <> '' ? $this->session->userdata('message') : ''; ?>
							</div>
						</div>
						<div class="col-md-4 text-right">
							<form action="<?php echo site_url('bank/index'); ?>" class="form-inline float-right" method="get">
								<div class="input-group">
									<input type="text" class="form-control-sm" placeholder="Cari Bank" name="q" value="<?php echo $q; ?>">
									<span class="input-group-btn">
										<?php if ($q <> '') { ?>
										<a href="<?php echo site_url('bank'); ?>" class="btn btn-default btn-sm"><i class="fas fa-redo"></i></a>
										<?php } ?>
										<button class="btn btn-info btn-sm" type="submit"><i class="fas fa-search"></i></button>
									</span>
								</div>
							</form>
						</div>
					</div>
					<div class="tab-content p-0" style="overflow:auto">
					<table class="table table-bordered table-striped table-condensed table-hover table-sm" style="margin-bottom: 10px">

						<tr>
							<th class="text-center" width="50px">No</th>
							<th>Nama Bank</th>
							<th class="text-center">Kode Bank</th>
							<th class="text-center">Logo Bank</th>
							<th class="text-center">Aksi</th>
                        </tr>
                        <?php
                        	if($total_rows == 0){
                        		echo '<tr><td colspan="8" bgcolor="grey" style="color: white; font-weight: bold; text-align: center;">Data Tidak Ditemukan</td></tr>';
                        	} else {
                        ?>
                            <?php foreach ($bank_data as $bank){ ?>
						<tr>
							<td class="text-center"><?php echo ++$start ?></td>
							<td><?php echo $bank->nama_bank ?></td>
							<td class="text-center" width="150px"><?php echo $bank->kode_bank ?></td>
							<td class="text-center">
								<img src="<?php echo base_url(); ?>bank/get_image?img=bank/<?php echo basename($bank->logo_bank); ?>" alt="Gambar Logo Bank" style="max-height:100px; max-width: 200px;" class="img-thumbnail">
							</td>
							<td class="text-center" width="120px">
								<div class="btn-group">
									<a href="<?php echo site_url('bank/update/'.$bank->id_bank); ?>"
									data-toogle="tooltip" title="Update">
									<button type="button" class="btn btn-success btn-sm"><i class="far fa-edit"></i></button></a>
									<a href="#" class="btn btn-danger btn-sm" data-toggle="tooltip" data-placement="top" title="Hapus" onclick="confirm('<?= $bank->id_bank ?>')" ><i class="fas fa-trash-alt"></i></a>
								</div>
							</td>
						</tr>
						    <?php } 
						} ?>
					</table>
					</div>
					   <?= footer($total_rows, $pagination, site_url('bank/exportxl?q=' . $q)) ?>
				</div>
			</div><!-- /.card-body -->
		</div>
		<!-- /.card -->
	</section>
	<!-- /.Left col -->
</div>

<script>
function confirm(res) {
    Swal.fire({
    title: 'Anda Yakin Mau Menghapus?',
    text: "Tidak bisa dikembalikan jika sudah dihapus!",
    icon: 'warning',
    showCancelButton: true,
    confirmButtonColor: '#3085d6',
    cancelButtonColor: '#d33',
    confirmButtonText: 'Ya, Hapus!',
    cancelButtonText: 'Batal'
    }).then((result) => {
    if (result.value) {
        Swal.fire(
        'Terhapus!',
        'File sudah terhapus.',
        'success'
        )
        window.location='<?php echo base_url().'bank/del_sem/'; ?>'+res;
    }
    });
}
</script>