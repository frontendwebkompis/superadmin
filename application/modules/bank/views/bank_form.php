<div class="row">
	<!-- Left col -->
	<section class="col-lg connectedSortable ui-sortable">
		<!-- Custom tabs (Charts with tabs)-->
		<div class="card card-info">
			<div class="card-header">
                <div class="card-title">
                  <?php echo $title_card?>
                </div>
              </div>
			<div class="card-body">
				<div class="tab-content p-0">

					<form id="dataform" action="<?php echo $action; ?>" method="post" enctype="multipart/form-data">
						<div class="form-group">
							<label for="varchar">Nama Bank <?php echo form_error('nama_bank') ?></label>
							<input type="text" class="form-control" name="nama_bank" id="nama_bank"
								placeholder="Nama Bank" value="<?php echo $nama_bank; ?>" />
						</div>
						<div class="form-group">
							<label for="varchar">Kode Bank <?php echo form_error('kode_bank') ?></label>
							<input type="text" maxlength="3" minlength="3" onkeypress="return Angkasaja(event)" class="form-control" name="kode_bank" id="kode_bank"
								placeholder="Kode Bank" value="<?php echo $kode_bank; ?>" />
						</div>
						<div class="form-group">
							<label for="varchar">Logo Bank <?php echo form_error('logo_brand') ?></label>
							<?php if($logo_bank !=""){ ?>
								<div class="row justify-content-center mb-2">
									<img style="height:100px" src="<?php echo base_url(); ?>bank/get_image?img=bank/<?php echo basename($logo_bank);?>" alt="">
								</div>
								<div class="row justify-content-center mb-2">
									<button class="btn btn-sm btn-secondary" type="button" onclick="cek()"><i class="fas fa-image"></i> Ganti Gambar</button>
								</div>
							<input type="file" style="display:none" accept="image/*" class="form-control" name="logo_bankbaru" id="logo_bankbaru">
							<input type="hidden" id="logo_bank" accept="image/*" value="<?php echo $logo_bank ?>" name="logo_bank">
							<?php }else{ ?>
							<input required type="file" accept="image/*" class="form-control" name="logo_bankbaru" id="logo_bankbaru">
							<?php } ?>
						</div>
						<input type="hidden" name="id_bank" value="<?php echo $id_bank; ?>" />
						<button id="ceksub" type="submit" class="btn btn-info" style="width: 100px;">Simpan</button>
						<a href="<?php echo site_url('bank') ?>" class="btn btn-default" style="width: 100px;">Batal</a>
					</form>
					 <div id="loader" class="alert alert-info mt-2" style="display:none">
					    <strong>Info!</strong> Sedang Proses....
					</div>
				</div>
			</div><!-- /.card-body -->
		</div>
		<!-- /.card -->
	</section>
	<!-- /.Left col -->
</div>

<script type="text/javascript">

    $(document).ready(function () {
      $('#dataform').validate({
        rules: {
        	nama_bank: {
        		required: true,
        	},
        	kode_bank: {
        		required: true,
        	},
        	logo_bankbaru: {
        		required: true,
        	}
        },
        messages: {
        	nama_bank: {
        		required: "Nama Bank Tidak Boleh Kosong",
        	},
        	kode_bank: {
        		required: "Kode Bank Tidak Boleh Kosong",
        	},
        	logo_bankbaru: {
        		required: "Logo Bank Tidak Boleh Kosong",
        	}
        },
        submitHandler: function(form) {
	        $('#loader').show(300); 
	        $('#ceksub').prop('disabled', true);
          	form.submit();
        },
        errorElement: 'span',
        errorPlacement: function (error, element) {
          error.addClass('invalid-feedback');
          element.closest('.form-group').append(error);
        },
        highlight: function (element, errorClass, validClass) {
          $(element).addClass('is-invalid');
        },
        unhighlight: function (element, errorClass, validClass) {
          $(element).removeClass('is-invalid');
        },
      });
    });

	function cek() {
		var x = document.getElementById("logo_bankbaru");
		if (x.style.display === "none") {
			x.style.display = "block";
		} else {
			x.style.display = "none";
		}
	}

	function Angkasaja(evt) {
	var charCode = (evt.which) ? evt.which : event.keyCode
	if (charCode > 31 && (charCode < 48 || charCode > 57))
	return false;
	return true;
	}
</script>