<div class="row">
	<!-- Left col -->
	<section class="col-lg connectedSortable ui-sortable">
		<!-- Custom tabs (Charts with tabs)-->
		<div class="card card-info">
			<div class="card-header">
                <div class="card-title">
                  <?php echo $title_card?>
                </div>
              </div>
			<div class="card-body">
				<div class="tab-content p-0">
					
					<table class="table table-bordered table-striped table-condensed" style="margin-bottom: 20px">
						<tr>	
							<td>Nama Bank</td>
							<td><?= $nama_bank ?></td>
						</tr>
						<tr>
							<td>Kode</td>
							<td><?= $kode_bank ?></td>
						</tr>
						<tr>
							<td>Logo Bank</td>
							<td><img src="<?php echo base_url(); ?>bank/get_image?img=bank/<?php echo basename($logo_bank); ?>" alt="Gambar Logo Bank" style="height:100px"></td>
						</tr>
						
					</table>
					<div class="btn-group">
						<a href="<?php echo site_url('bank') ?>" class="btn btn-info">Batal</a></td>
						
					</div>

				</div>
			</div><!-- /.card-body -->
		</div>
		<!-- /.card -->
	</section>
	<!-- /.Left col -->
</div>
