<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Bank_model extends CI_Model
{

    public $table = 'bank';
    public $id = 'id_bank';
    public $order = 'DESC';

    function __construct()
    {
        parent::__construct();
    }

    // get data by id
    function get_by_poto($id)
    {
        $this->db->where('logo_bank', $id);
        return $this->db->get($this->table)->row();
    }
    public function get_by_prefix($prefix = '')
    {
        # code..
        $query = $this->db->query("SELECT max(" . $this->id . ") AS max FROM " . $this->table . " WHERE " . $this->id . " LIKE '$prefix%'");
        return $query->row();
    }
    // get all
    function get_all()
    {
        $this->db->order_by($this->id, $this->order);
        return $this->db->get($this->table)->result();
    }

    // get data by id
    function get_by_id($id)
    {
        $this->db->where($this->id, $id);
        return $this->db->get($this->table)->row();
    }

    // get total rows
    function total_rows($q = NULL)
    {
        $this->db->where('status_bank_remove', 1);
        $this->db->group_start();
        $this->db->or_like('nama_bank', $q);
        $this->db->or_like('kode_bank', $q);
        $this->db->group_end();
        $this->db->from($this->table);
        return $this->db->count_all_results();
    }

    // get data with limit and search
    function get_limit_data($limit, $start = 0, $q = NULL)
    {
        $this->db->order_by($this->id, $this->order);
        $this->db->where('status_bank_remove', 1);
        $this->db->group_start();
        $this->db->or_like('nama_bank', $q);
        $this->db->or_like('kode_bank', $q);
        $this->db->group_end();
        $this->db->limit($limit, $start);
        return $this->db->get($this->table)->result();
    }

    // insert data
    function insert($data)
    {
        $this->db->insert($this->table, $data);
    }

    // update data
    function update($id, $data)
    {
        $this->db->where($this->id, $id);
        $this->db->update($this->table, $data);
    }

    // delete data
    function delete($id)
    {
        $this->db->where($this->id, $id);
        $this->db->delete($this->table);
    }
}

/* End of file Bank_model.php */
/* Location: ./application/models/Bank_model.php */
