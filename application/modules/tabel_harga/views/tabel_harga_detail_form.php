<div class="row">
	<section class="col-lg connectedSortable ui-sortable">
		<div class="card card-info">
            <div class="card-header">
                <div class="card-title">
                    <?= $title_card ?>
                </div>
            </div>
			<div class="card-body">
				<div class="tab-content p-0">
            
        <form action="<?php echo $action; ?>" method="post">
            <?php
             if($id_provinsi==""){ ?>
            <div class="form-group">
                <label for="varchar">Provinsi  <?php echo form_error('id_provinsi') ?></label>
                    <select required id="id_provinsi" class="form-control" name="id_provinsi">
                            <option value="">Pilih Provinsi </option>
                            <?php
                            foreach ($provinsi as $prov) { 
                            ?>
                            <option <?= $prov->id_provinsi ==  $id_provinsi ? 'selected' : ''; ?> value="<?php echo $prov->id_provinsi; ?>">
                                <?php echo $prov->nama_provinsi;?>
                            </option>
                            <?php      
                            }
                            ?>
                    </select>
            </div>
        <?php }else{
        foreach ($provinsi_all as $prov) { 
            if($prov->id_provinsi ==  $id_provinsi){
              echo "<h3>Nama Provinsi = <b>".$prov->nama_provinsi."</b></h3>";
              echo '<input type="hidden" name="id_provinsi" id="id_provinsi" value="'.$id_provinsi.'">';
             }
         }
        }?>

            <div class="form-group">
                <label for="int">Harga Provinsi <?php echo form_error('harga_nasional') ?></label>
                <input required type="text" onkeypress="return Angkasaja(event)" class="form-control" name="harga_provinsi" id="harga_provinsi" placeholder="Harga Nasional" value="<?php echo $harga_provinsi; ?>" />
            </div>
                <input type="hidden" name="id_tbl_harga_detail" value="<?php echo $id_tbl_harga_detail; ?>" /> 
                <button type="submit" class="btn btn-info" style="width: 100px;">Simpan</button> 
                <a href="<?php echo site_url('tabel_harga/read/'.$id) ?>" class="btn btn-default" style="width: 100px;">Batal</a>
        </form>

    </div>
    </div>
		</div>
	</section>
</div>

<script type="text/javascript">
    function Angkasaja(evt) {
    var charCode = (evt.which) ? evt.which : event.keyCode
    if (charCode > 31 && (charCode < 48 || charCode > 57))
    return false;
    return true;
    }
</script>