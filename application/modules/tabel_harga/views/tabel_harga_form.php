<div class="row">
	<section class="col-lg connectedSortable ui-sortable">
		<div class="card card-info">
            <div class="card-header">
                <div class="card-title">
                  <?php echo $title_card?>
                </div>
              </div>
			<div class="card-body">
				<div class="tab-content p-0">
            
        <form action="<?php echo $action; ?>" method="post" id="dataform">
          <?php  if($id_tabel_harga==""){ ?>
            <div class="form-group">
             <label for="varchar">Kategori Sampah <?php echo form_error('id_kategori_sampah') ?></label>
                    <select  id="id_kategori_sampah" class="form-control" name="id_kategori_sampah">
                        <option value="">Pilih Kategori</option>
                        <?php
                         foreach ($kategori_sampah as $kat) { ?>
                        <option 
                        <?php if($kat->id_kategori_sampah ==  $id_kategori_sampah){echo 'selected'; $cek=$kat->nama_kategori_sampah;
                    }else { echo '';  }
                        ?> value="<?php echo $kat->id_kategori_sampah; ?>">
                        <?php echo $kat->nama_kategori_sampah; ?>
                        </option>
                        <?php } ?>
                    </select>
            </div>
        <?php }else{
            // echo $id_kategori_sampah;
             foreach ($kategori_sampah_cek as $kat) { 
                 if($kat->id_kategori_sampah ==  $id_kategori_sampah){ ?>
                <h2>Nama Kategori= <span class="color:red"> <?php echo $kat->nama_kategori_sampah; ?></span></h2>  
                <input type="hidden" name="id_kategori_sampah" value="<?= $id_kategori_sampah ?>">  
                    <?php
                 }
             }
              
        } ?>
          
            <div class="form-group">
                <label for="int">Harga Nasional <?php echo form_error('harga_nasional') ?></label>
                <input type="text" onkeypress="return Angkasaja(event)" class="form-control" name="harga_nasional" id="harga_nasional" placeholder="Harga Nasional" value="<?php echo $harga_nasional; ?>" />
            </div>
                <input type="hidden" name="id_tabel_harga" value="<?php echo $id_tabel_harga; ?>" /> 
                <button id="btn-simpan" type="submit" class="btn btn-info" style="width: 100px;">Simpan</button> 
                <a href="<?php echo site_url('tabel_harga') ?>" class="btn btn-default" style="width: 100px;">Batal</a>
        </form>
    </div>
    </div>
		</div>
	</section>
</div>

<script type="text/javascript">

    $(document).ready(function () {
      $('#dataform').validate({
        rules: {
          id_kategori_sampah: {
            required: true,
          },
          harga_nasional: {
            required: true,
          }
        },
        messages: {
          id_kategori_sampah: {
            required: "Kategori Sampah Tidak Boleh Kosong",
          },
          harga_nasional: {
            required: "Harga Nasional Tidak Boleh Kosong",
          }
        },
        submitHandler: function(form) {
          $('#btn-simpan').prop('disabled', true);
          form.submit();
        },
        errorElement: 'span',
        errorPlacement: function (error, element) {
          error.addClass('invalid-feedback');
          element.closest('.form-group').append(error);
        },
        highlight: function (element, errorClass, validClass) {
          $(element).addClass('is-invalid');
        },
        unhighlight: function (element, errorClass, validClass) {
          $(element).removeClass('is-invalid');
        },
      });
    });

    function Angkasaja(evt) {
        var charCode = (evt.which) ? evt.which : event.keyCode
        if (charCode > 31 && (charCode < 48 || charCode > 57))
        return false;
        return true;
    }
</script>