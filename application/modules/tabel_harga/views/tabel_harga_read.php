<script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>

<div class="row">
    <section class="col-lg connectedSortable ui-sortable">
        <div class="card card-info">
            <div class="card-header">
                <div class="card-title">
                  <?php echo $title_card?>
                </div>
            </div>
            <div class="card-body">

                <div class="tab-content p-0">
                    <div class="row px-3">
                        <div class="col-md-6">
                            <table class="table table-sm" border="0">
                                <tr>
                                    <td class='font-weight-bold'>Kategori Sampah</td>
                                    <td><?php echo $nama_kategori_sampah; ?></td>
                                </tr>
                                <tr>
                                    <td class='font-weight-bold'>Jenis Sampah</td>
                                    <td><?php echo $nama_jenis_sampah; ?></td>
                                </tr>
                                <tr>
                                    <td class='font-weight-bold'>Harga Nasional</td>
                                    <td><?php echo rupiah($harga_nasional); ?><button class="float-right btn btn-default btn-xs" title="Edit Harga Nasional" data-toggle="modal" data-target="#modalEditHarga" onclick="cekEditHarga('<?= $id_tabel_harga ?>')"><i class="fas fa-edit fa-sm"></i></button></td>
                                </tr>
                                <tr><td colspan="2"></td></tr>
                            </table>
                        </div>
                    </div>
                    <div class="row px-3">
                        <div class="row" style="margin-bottom: 10px">
                            <div class="col-md-auto">
                                <!-- <?php echo anchor(site_url('tabel_harga/create_detail/' . $id_tabel_harga), 'Tambah Data', 'class="btn btn-info btn-sm" style="width:120px;"'); ?> -->
                                <a href="<?php echo site_url('tabel_harga') ?>" class="btn btn-secondary btn-sm" style="width:120px;">Kembali</a>
                                <button class="btn btn-info btn-sm" title="Tambah Data" data-toggle="modal" data-target="#modalCreateHargaProv" style="width: 120px;">Tambah Data</button>
                            </div>
                        </div>
                    </div>
                    <div class="row"> 
               <div class="col-md-4 text-center"></div>
                    <div class="col-md-4 text-center">
                        <div style="margin-top: 8px" id="message">
                            <?php echo $this->session->userdata('message') <> '' ? $this->session->userdata('message') : ''; ?>
                        </div>
                    </div>
                </div>
                        
                    <table class="table table-bordered table-striped table-condensed table-hover table-sm"style="margin-bottom: 10px">
                        <tr>
                            <th style="text-align:center" width="60px">No</th>
                            <th class='text-center'>Provinsi</th>
                            <!-- <th>Jenis Sampah</th> -->
                            <th class='text-center'>Harga Nasional</th>
                            <th style="text-align:center">Aksi</th>
                        </tr>
                        <?php if(count($tabel_harga_detail)==0){
                            echo '<tr><td colspan="11" bgcolor="grey" style="color: white; font-weight: bold; text-align: center;">Data Tidak Ditemukan</td></tr>';
                        }else{ ?>
                        <?php foreach ($tabel_harga_detail as $tabel_harga) { ?>
                        <tr>
                            <td style="text-align:center"><?php echo ++$start ?></td>
                            <td><?php echo $tabel_harga->nama_provinsi ?></td>
                            <td class='text-center'><?php echo rupiah($tabel_harga->harga_provinsi); ?></td>
                            <td style="text-align:center" width="100px">
                                <div class="btn-group">
                                    <!-- <a href="<?php echo site_url('tabel_harga/update_detail/' . $tabel_harga->id_tbl_harga_detail . '?id_harga=' . $id_tabel_harga); ?>" data-toogle="tooltip" title="Update">
                                        <button type="button" class="btn btn-success btn-sm"><i class="far fa-edit"></i></button></a> -->
                                    <button class="btn btn-success btn-sm" title="Edit Harga" data-toggle="modal" data-target="#modalEditHargaProv" onclick="cekEdit('<?= $tabel_harga->id_tbl_harga_detail ?>','<?= $id_tabel_harga ?>')"><i class="fas fa-edit"></i></button>
                                    <a href="#" class="btn btn-danger btn-sm" data-toggle="tooltip" data-placement="top" title="Hapus" onclick="confirm('<?= $id_tabel_harga ?>','<?= $tabel_harga->id_tbl_harga_detail ?>')"><i class="fas fa-trash-alt"></i></a>
                                </div>
                            </td>
                        </tr>
                        <?php } 
                    } ?>
                    </table>

                    <a href="<?php echo site_url('tabel_harga/exportxlprovinsi/'.$id_tabel_harga.'?jns='.clean($nama_kategori_sampah))?>" class="btn btn-success btn-sm"><i class="fas fa-file-excel"></i> Export Excel</a>
                    
                </div>
            </div>
    </section>
</div>

<div id="modalEditHarga" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabel">Form Edit Harga</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<form action="" method="post" id="formEditHarga" role="form" enctype="multipart/form-data">
                    <div class="row px-3">
                        <div class="col-md-12">
                            <table class="table table-sm" border="0">
                                <tr>
                                    <td class='font-weight-bold'>Kategori Sampah</td>
                                    <td id="kategori_sampah"><span class="badge badge-danger">Kosong</span></td>
                                </tr>
                                <tr>
                                    <td class='font-weight-bold'>Jenis Sampah</td>
                                    <td id="jenis_sampah"><span class="badge badge-danger">Kosong</span></td>
                                </tr>
                                <tr>
                                    <td colspan="2"></td>
                                </tr>
                            </table>
                        </div>
                    </div>
					<div class="row ml-3 mr-3">
						<div class="col-md-12">
                            <div class="form-group">
                                <label for="harga_nasional">Harga Nasional</label>
                                <input type="number" id="harga_nasional" name="harga_nasional" class="form-control" onkeypress="return hanyaAngka(event)" placeholder="0">
                            </div>
						</div>
					</div>
			</div>
			<div class="modal-footer">
				<div class="col-md-12 text-center">
					<button type="submit" class="btn btn-primary" style="width: 100px;">Simpan</button>
					<button type="button" class="btn btn-secondary" data-dismiss="modal" style="width: 100px;">Batal</button>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>

<div id="modalCreateHargaProv" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabel">Form Tambah Harga Provinsi</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<form action="<?= $action ?>" method="post" id="formTambahHarga" role="form" enctype="multipart/form-data">
					<div class="row ml-3 mr-3">
						<div class="col-md-12">
                            <div class="form-group">
                                <label for="id_provinsi">Provinsi</label>
                                <select name="id_provinsi" id="id_provinsi" class="form-control" required>
                                    <option value="">Pilih Provinsi</option>
                                    <?php
                                        foreach($provinsi as $data){ ?>
                                            <option value="<?= $data->id_provinsi ?>"><?= $data->nama_provinsi ?></option>
                                    <?php  }
                                    ?>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="harga_provinsi">Harga Provinsi</label>
                                <input type="number" name="harga_provinsi" id="harga_provinsi" class="form-control" required onkeypress="return hanyaAngka(event)" placeholder="0">
                            </div>
						</div>
					</div>
			</div>
			<div class="modal-footer">
				<div class="col-md-12 text-center">
					<button type="submit" class="btn btn-primary" style="width: 100px;">Simpan</button>
					<button type="button" class="btn btn-secondary" data-dismiss="modal" style="width: 100px;">Batal</button>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>

<div id="modalEditHargaProv" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabel">Form Tambah Harga Provinsi</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<form action="" method="post" id="formEditHarga" role="form" enctype="multipart/form-data">
					<div class="row ml-3 mr-3">
						<div class="col-md-12">
                            <table class="table table-sm" border="0">
                                <tr>
                                    <td class='font-weight-bold'>Provinsi</td>
                                    <td id="nama_provinsi"><span class="badge badge-danger">Kosong</span></td>
                                </tr>
                                <tr>
                                    <td colspan="2"></td>
                                </tr>
                            </table>
                            <div class="form-group">
                                <label for="harga_provinsi">Harga Provinsi</label>
                                <input type="number" name="harga_provinsi" id="harga_provinsi" class="form-control" required onkeypress="return hanyaAngka(event)" placeholder="0">
                            </div>
						</div>
					</div>
			</div>
			<div class="modal-footer">
				<div class="col-md-12 text-center">
					<button type="submit" class="btn btn-primary" style="width: 100px;">Simpan</button>
					<button type="button" class="btn btn-secondary" data-dismiss="modal" style="width: 100px;">Batal</button>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>

<script>
    $(function(){
        $('#modalCreateHargaProv #formTambahHarga').validate({
            rules: {
                id_provinsi: {
                    required: true
                },
                harga_provinsi: {
                    required: true
                }
            },
            messages: {
                id_provinsi: {
                    required: "Provinsi Tidak Boleh Kosong"
                },
                harga_provinsi: {
                    required: "Harga Tidak Boleh Kosong"
                }
            },
            errorElement: 'span',
			errorPlacement: function(error, element) {
				error.addClass('invalid-feedback');
				element.closest('.form-group').append(error);
			},
			highlight: function(element, errorClass, validClass) {
				$(element).addClass('is-invalid');
			},
			unhighlight: function(element, errorClass, validClass) {
				$(element).removeClass('is-invalid');
			}
        })

        $('#modalEditHargaProv #formEditHarga').validate({
            rules: {
                harga_provinsi: {
                    required: true
                }
            },
            messages: {
                harga_provinsi: {
                    required: "Harga Tidak Boleh Kosong"
                }
            },
            errorElement: 'span',
			errorPlacement: function(error, element) {
				error.addClass('invalid-feedback');
				element.closest('.form-group').append(error);
			},
			highlight: function(element, errorClass, validClass) {
				$(element).addClass('is-invalid');
			},
			unhighlight: function(element, errorClass, validClass) {
				$(element).removeClass('is-invalid');
			}
        })
    })

    $('#modalCreateHargaProv').on('hidden.bs.modal', function(){
        $('#modalCreateHargaProv #id_provinsi').val('')
        $('#modalCreateHargaProv #id_provinsi').removeClass('is-invalid')
        $('#modalCreateHargaProv #harga_provinsi').val('')
        $('#modalCreateHargaProv #harga_provinsi').removeClass('is-invalid')
    })

    $('#modalEditHargaProv').on('hidden.bs.modal', function(){
        $('#modalEditHargaProv #nama_provinsi').html('<span class="badge badge-danger">Kosong</span>')
        $('#modalEditHargaProv #harga_provinsi').val('')
        $('#modalEditHargaProv #harga_provinsi').removeClass('is-invalid')
    })

    function confirm(id, id_d) {
        Swal.fire({
            title: 'Anda Yakin Mau Menghapus?',
            text: "Tidak bisa dikembalikan jika sudah dihapus!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Ya, Hapus!',
            cancelButtonText: 'Batal'
        }).then((result) => {
            if (result.value) {
                window.location = "<?php echo site_url('tabel_harga/delete_detail/'); ?>" + id + '/' + id_d;
            }
        });
        // ,
        //     function(isConfirm) {
        //         if (isConfirm) {
        //             window.location.href = "<?php echo site_url('tabel_harga/delete_detail/'); ?>" + id + '/' + id_d;
        //         } else {

        //         }
		// });
    }

    function hanyaAngka(evt) {
        var charCode = (evt.which) ? evt.which : event.keyCode
        if (charCode > 31 && (charCode < 48 || charCode > 57))
            return false;
        return true;
    }

    function cekEdit(id_d, id){
        startloading('#modalEditHargaProv .modal-content')
        $('#modalEditHargaProv #formEditHarga').attr('action', '<?= site_url('tabel_harga/update_detail_action/') ?>'+id+'/'+id_d);
        $.ajax({
            url: '<?php echo site_url() ?>tabel_harga/ambilDataDetailHarga/'+id_d,
            dataType: "json",
            cache: false,
            success: function(res) {
                $('#modalEditHargaProv #nama_provinsi').html(res.nama_provinsi)
                $('#modalEditHargaProv #harga_provinsi').val(res.harga_provinsi)
                stoploading('#modalEditHargaProv .modal-content')
            }
        });
    }

    function cekEditHarga(id){
        startloading('#modalEditHarga .modal-content')
        $('#modalEditHarga #formEditHarga').attr('action', '<?= site_url('tabel_harga/updateHargaActions/') ?>'+id)
        $.ajax({
            url: '<?php echo site_url() ?>tabel_harga/ambilDataHarga/'+id,
            dataType: "json",
            cache: false,
            success: function(res) {
                $('#modalEditHarga #kategori_sampah').html(res.nama_kategori_sampah)
                $('#modalEditHarga #jenis_sampah').html(res.nama_jenis_sampah)
                $('#modalEditHarga #harga_nasional').val(res.harga_nasional)
                stoploading('#modalEditHarga .modal-content')
            }
        });
    }
</script>