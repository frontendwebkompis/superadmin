<script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>

<div class="row">
	<section class="col-lg connectedSortable ui-sortable">
		<div class="card card-info">
            <div class="card-header">
                <div class="card-title">
                  <?php echo $title_card?>
                </div>
              </div>
			<div class="card-body">
				<div class="tab-content p-0">

        <div class="row">
            <div class="col-md-4 mb-2">
                <button class="btn btn-info btn-sm" data-toggle="modal" data-target="#modalCreateHarga">Tambah Data</button>
            </div>
            <div class="col-md-3 offset-md-5 mb-2">
                <?= search(site_url('tabel_harga/index'), site_url('tabel_harga'), $q) ?>
            </div>
        </div>
        <div class="tab-content p-0" style="overflow:auto">
        <table class="table table-bordered table-striped table-condensed table-hover table-sm" style="margin-bottom: 10px">
            <tr>
                <th class="text-center" width="50px">No</th>
                <th class="text-center">Kategori Sampah</th>
                <th class="text-center">Jenis Sampah</th>
                <th class="text-center">Harga Nasional</th>
                <th class="text-center">Aksi</th>
            </tr>

            <?php
                if($total_rows == 0){
                    echo '<tr><td colspan="8" bgcolor="grey" style="color: white; font-weight: bold; text-align: center;">Data Tidak Ditemukan</td></tr>';
                } else {
            ?>

            <?php
            foreach ($tabel_harga_data as $tabel_harga) { ?>
            <tr>
                <td style="text-align:center"><?php echo ++$start ?></td>
                <td><?php echo $tabel_harga->nama_kategori_sampah ?></td>
                <td><?php if(empty($tabel_harga->nama_jenis_sampah)){
                    echo '<span class="badge badge-danger">Kosong</span>';
                } else {
                    echo $tabel_harga->nama_jenis_sampah;
                } ?></td>
                <td><?php echo rupiah($tabel_harga->harga_nasional) ?></td>
                <td style="text-align:center" width="150px">
                    <div class="btn-group">
                        <a href="<?php echo site_url('tabel_harga/read/'.$tabel_harga->id_tabel_harga); ?>"
                        data-toogle="tooltip" title="Lihat" class="btn btn-info btn-sm">
                        <i class="fas fa-info-circle"></i></a>
                        <button class="btn btn-success btn-sm" data-toggle="modal" data-target="#modalEditHarga" onclick="cekEdit('<?= $tabel_harga->id_tabel_harga ?>')"><i class="fas fa-edit"></i></button>
                    </div>
                </td>
		    </tr>
            <?php } 
            }?>
        </table>
        </div>
        <?= footer($total_rows, $pagination, site_url('tabel_harga/exportxl?q='.$q)) ?>
        </div>
			</div>
		</div>
	</section>
</div>

<div id="modalCreateHarga" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabel">Form Tambah Harga</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<form action="<?= $action ?>" method="post" id="formTambahHarga" role="form" enctype="multipart/form-data">
					<div class="row ml-3 mr-3">
						<div class="col-md-12">
                            <div class="form-group">
                                <label for="id_kategori_sampah">Kategori Sampah</label>
                                <select name="id_kategori_sampah" id="id_kategori_sampah" class="form-control">
                                    <option value="">Pilih Kategori Sampah</option>
                                    <?php
                                    foreach ($kategori as $data) { ?>
                                        <option value="<?php echo $data->id_kategori_sampah; ?>">
                                            <?php echo $data->nama_kategori_sampah; ?>
                                        </option>
                                    <?php }
                                    ?>
                                </select>
                            </div>
						</div>
						<div class="col-md-12">
                            <div class="form-group">
                                <label for="id_jenis_sampah">Jenis Sampah</label>
                                <select name="id_jenis_sampah" id="id_jenis_sampah" class="form-control">
                                    <option value="">Pilih Jenis Sampah</option>
                                    <?php
                                    foreach ($jenis_sampah as $data) { ?>
                                        <option data-chained="<?php echo $data->id_kategori_sampah; ?>" value="<?php echo $data->id_jenis_sampah; ?>">
                                            <?php echo $data->nama_jenis_sampah; ?>
                                        </option>
                                    <?php }
                                    ?>
                                </select>
                            </div>
						</div>
						<div class="col-md-12">
                            <div class="form-group">
                                <label for="harga_nasional">Harga Nasional</label>
                                <input type="number" id="harga_nasional" name="harga_nasional" class="form-control" onkeypress="return hanyaAngka(event)" placeholder="0">
                            </div>
						</div>
					</div>
			</div>
			<div class="modal-footer">
				<div class="col-md-12 text-center">
					<button type="submit" class="btn btn-primary" style="width: 100px;">Simpan</button>
					<button type="button" class="btn btn-secondary" data-dismiss="modal" style="width: 100px;">Batal</button>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>

<div id="modalEditHarga" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabel">Form Edit Harga</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<form action="" method="post" id="formEditHarga" role="form" enctype="multipart/form-data">
                    <div class="row px-3">
                        <div class="col-md-12">
                            <table class="table table-sm" border="0">
                                <tr>
                                    <td class='font-weight-bold'>Kategori Sampah</td>
                                    <td id="kategori_sampah"><span class="badge badge-danger">Kosong</span></td>
                                </tr>
                                <tr>
                                    <td class='font-weight-bold'>Jenis Sampah</td>
                                    <td id="jenis_sampah"><span class="badge badge-danger">Kosong</span></td>
                                </tr>
                                <tr>
                                    <td colspan="2"></td>
                                </tr>
                            </table>
                        </div>
                    </div>
					<div class="row ml-3 mr-3">
						<div class="col-md-12">
                            <div class="form-group">
                                <label for="harga_nasional">Harga Nasional</label>
                                <input type="number" id="harga_nasional" name="harga_nasional" class="form-control" onkeypress="return hanyaAngka(event)" placeholder="0">
                            </div>
						</div>
					</div>
			</div>
			<div class="modal-footer">
				<div class="col-md-12 text-center">
					<button type="submit" class="btn btn-primary" style="width: 100px;">Simpan</button>
					<button type="button" class="btn btn-secondary" data-dismiss="modal" style="width: 100px;">Batal</button>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>

<script>
    $(function(){
        $("#id_jenis_sampah").chained("#id_kategori_sampah");

        $('#formTambahHarga').validate({
            rules: {
                id_kategori_sampah: {
                    required: true
                },
                id_jenis_sampah: {
                    required: true
                },
                harga_nasional: {
                    required: true
                }
            },
            messages: {
                id_kategori_sampah: {
                    required: "Kategori Sampah Tidak Boleh Kosong"
                },
                id_jenis_sampah: {
                    required: "Jenis Sampah Tidak Boleh Kosong"
                },
                harga_nasional: {
                    required: "harga Tidak Boleh Kosong"
                }
            },
            errorElement: 'span',
			errorPlacement: function(error, element) {
				error.addClass('invalid-feedback');
				element.closest('.form-group').append(error);
			},
			highlight: function(element, errorClass, validClass) {
				$(element).addClass('is-invalid');
			},
			unhighlight: function(element, errorClass, validClass) {
				$(element).removeClass('is-invalid');
			}
        })

        $('#formEditHarga').validate({
            rules: {
                harga_nasional: {
                    required: true
                }
            },
            messages: {
                harga_nasional: "Harga Tidak Boleh Kosong"
            },
            errorElement: 'span',
			errorPlacement: function(error, element) {
				error.addClass('invalid-feedback');
				element.closest('.form-group').append(error);
			},
			highlight: function(element, errorClass, validClass) {
				$(element).addClass('is-invalid');
			},
			unhighlight: function(element, errorClass, validClass) {
				$(element).removeClass('is-invalid');
			}
        })
    })

    $('#modalEditHarga').on('hidden.bs.modal', function(){
        $('#modalEditHarga #kategori_sampah').html('<span class="badge badge-danger">Kosong</span>')
        $('#modalEditHarga #jenis_sampah').html('<span class="badge badge-danger">Kosong</span>')
        $('#modalEditHarga #harga_nasional').val('')
        $('#modalEditHarga #harga_nasional').removeClass('is-invalid')
    })

    $('#modalCreateHarga').on('hidden.bs.modal', function(){
        $('#modalCreateHarga #id_kategori_sampah').val('')
        $('#modalCreateHarga #id_kategori_sampah').removeClass('is-invalid')
        $('#modalCreateHarga #id_jenis_sampah').val('')
        $('#modalCreateHarga #id_jenis_sampah').removeClass('is-invalid')
        $('#modalCreateHarga #harga_nasional').val('')
        $('#modalCreateHarga #harga_nasional').removeClass('is-invalid')
    })

    function hanyaAngka(evt) {
        var charCode = (evt.which) ? evt.which : event.keyCode
        if (charCode > 31 && (charCode < 48 || charCode > 57))
            return false;
        return true;
    }

    function confirm(res) {
        Swal.fire({
        title: 'Anda Yakin Mau Menghapus?',
        text: "Tidak bisa dikembalikan jika sudah dihapus!",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Ya, Hapus!',
        cancelButtonText: 'Batal'
        }).then((result) => {
        if (result.value) {
            Swal.fire(
            'Terhapus!',
            'File sudah terhapus.',
            'success'
            )
            window.location='<?php echo site_url().'tabel_harga/del_sem/'; ?>'+res;
        }
        });
    }

    function cekEdit(id){
        startloading('#modalEditHarga .modal-content')
        $('#modalEditHarga #formEditHarga').attr('action', '<?= site_url('tabel_harga/updateHargaAction/') ?>'+id)
        $.ajax({
            url: '<?php echo site_url() ?>tabel_harga/ambilDataHarga/'+id,
            dataType: "json",
            cache: false,
            success: function(res) {
                $('#modalEditHarga #kategori_sampah').html(res.nama_kategori_sampah)
                $('#modalEditHarga #jenis_sampah').html(res.nama_jenis_sampah)
                $('#modalEditHarga #harga_nasional').val(res.harga_nasional)
                stoploading('#modalEditHarga .modal-content')
            }
        });
    }

</script>