<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Tabel_harga extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        if (cek_token()) {
            $this->load->model('Tabel_harga_model');
            $this->load->library('form_validation');
        } else {
            logout();
        }
    }

    public function index()
    {
        $q = urldecode($this->input->get('q', TRUE));
        $start = intval($this->input->get('start'));

        if ($q <> '') {
            $config['base_url'] = base_url() . 'tabel_harga?q=' . urlencode($q);
            $config['first_url'] = base_url() . 'tabel_harga?q=' . urlencode($q);
        } else {
            $config['base_url'] = base_url() . 'tabel_harga';
            $config['first_url'] = base_url() . 'tabel_harga';
        }

        $config['per_page'] = 10;
        $config['page_query_string'] = TRUE;
        $config['total_rows'] = $this->Tabel_harga_model->total_rows($q);
        $tabel_harga = $this->Tabel_harga_model->get_limit_data($config['per_page'], $start, $q);

        $this->load->library('pagination');
        $this->pagination->initialize($config);

        $data = array(
            'tittle'                => 'Tabel Harga Nasional',
            'judul'                 => 'Tabel Harga Nasional',
            'title_card'            => 'List Harga Nasional',
            'tabel_harga_data'      => $tabel_harga,
            'q'                     => $q,
            'pagination'            => $this->pagination->create_links(),
            'total_rows'            => $config['total_rows'],
            'start'                 => $start,
            'harga_nasional_aktif'  => '1',
            'action'                => base_url('tabel_harga/create_action'),
            'menu_aktif'            => 'tabel_harga'
        );

        $data['kategori']       = $this->Tabel_harga_model->get_kategori();
        $data['jenis_sampah']   = $this->Tabel_harga_model->get_jenis();
        $res['datakonten']  = $this->load->view('tabel_harga/tabel_harga_list', $data, true);
        $this->load->view('layouts/main_view', $res);
    }

    public function read($id)
    {
        $row = $this->Tabel_harga_model->get_by_id($id);
        if ($row) {

            // $data['kategori_sampah'] = $this->Tabel_harga_model->get_kategori_all();
            $q = urldecode($this->input->get('q', TRUE));
            $start = intval($this->input->get('start'));

            if ($q <> '') {
                $config['base_url'] = base_url() . 'tabel_harga/index.html?q=' . urlencode($q);
                $config['first_url'] = base_url() . 'tabel_harga/index.html?q=' . urlencode($q);
            } else {
                $config['base_url'] = base_url() . 'tabel_harga/index.html';
                $config['first_url'] = base_url() . 'tabel_harga/index.html';
            }

            $config['per_page'] = 10;
            $config['page_query_string'] = TRUE;
            $config['total_rows'] = $this->Tabel_harga_model->total_rows_detail($q, $id);
            $tabel_harga_detail = $this->Tabel_harga_model->get_limit_data_detail($config['per_page'], $start, $q, $id);

            $this->load->library('pagination');
            $this->pagination->initialize($config);

            $data = array(
                'id_tabel_harga'        => $row->id_tabel_harga,
                'nama_kategori_sampah'  => $row->nama_kategori_sampah,
                'nama_jenis_sampah'     => $row->nama_jenis_sampah,
                'harga_nasional'        => $row->harga_nasional,
                'tittle'                => 'Tabel Harga Nasional',
                'judul'                 => 'Tabel Harga Nasional',
                'title_card'            => 'Detail Harga Nasional',
                'tabel_harga_detail'    => $tabel_harga_detail,
                'q'                     => $q,
                'pagination'            => $this->pagination->create_links(),
                'total_rows'            => $config['total_rows'],
                'start'                 => $start,
                'harga_nasional_aktif'  => '1',
                'action'                => base_url('tabel_harga/create_detail_action/'.$id),
                'menu_aktif'            => 'tabel_harga'
            );
            $data['provinsi']   = $data['provinsi'] = $this->Tabel_harga_model->get_provinsi_all($id);
            $res['datakonten']  = $this->load->view('tabel_harga/tabel_harga_read', $data, true);
            $this->load->view('layouts/main_view', $res);
        } else {
            $this->session->set_flashdata(
                'gagal','Data Tidak Di Temukan'
            );
            redirect(site_url('tabel_harga'));
        }
    }

    public function create()
    {
        $data = array(
            'tittle'                => 'Harga Nasional',
            'judul'                 => 'Harga Nasional',
            'title_card'            => 'Tambah Harga Nasional',
            'button'                => 'Create',
            'action'                => site_url('tabel_harga/create_action'),
            'id_tabel_harga'        => set_value('id_tabel_harga'),
            'id_kategori_sampah'    => set_value('id_kategori_sampah'),
            'harga_nasional'        => set_value('harga_nasional'),
            'harga_nasional_aktif'  => '1',
            'menu_aktif'            => 'tabel_harga'
        );
        $data['kategori_sampah'] = $this->Tabel_harga_model->get_kategori_all();
        $data['kategori_sampah_cek'] = $this->Tabel_harga_model->get_kategori();

        $res['datakonten'] = $this->load->view('tabel_harga/tabel_harga_form', $data, true);
        $this->load->view('layouts/main_view', $res);
    }
    public function create_detail($id)
    {
        $data = array(
            'tittle'                => 'Tabel Harga',
            'judul'                 => 'Tabel Harga',
            'title_card'           => 'Tabel Harga Form',
            'button'                => 'Create',
            'action'                => site_url('tabel_harga/create_detail_action/' . $id),
            'id_tbl_harga_detail'   => set_value('id_tbl_harga_detail'),
            'id_provinsi'           => set_value('id_provinsi'),
            'id'                    => $id,
            'harga_provinsi'        => set_value('harga_provinsi'),
            'harga_nasional_aktif'  => '1',
            'menu_aktif'            => 'tabel_harga'
        );
        $data['provinsi'] = $this->Tabel_harga_model->get_provinsi_all($id);

        $res['datakonten'] = $this->load->view('tabel_harga/tabel_harga_detail_form', $data, true);
        $this->load->view('layouts/main_view', $res);
    }

    public function create_action()
    {
        $dt = new DateTime();
        $prefix = "THR" . $dt->format('dmY');
        $row    = $this->Tabel_harga_model->get_by_prefix($prefix);
        $NoUrut = (int) substr($row->max, 11, 3);
        $NoUrut = $NoUrut + 1; //nomor urut +1
        $NoUrut = sprintf('%03d', $NoUrut);
        $fix = $prefix . $NoUrut;

        $this->db->where('id_kategori_sampah', $this->input->post('id_kategori_sampah', TRUE));
        $this->db->where('id_jenis_sampah', $this->input->post('id_jenis_sampah', TRUE));
        $data = $this->db->get('tabel_harga')->row();
        if($data){
            $this->session->set_flashdata('gagal', 'Data jenis sudah ada');
            redirect('tabel_harga/read/'.$data->id_tabel_harga);
        } else {
            $data = array(
                'id_tabel_harga'            => $fix,
                'id_kategori_sampah'        => $this->input->post('id_kategori_sampah', TRUE),
                'id_jenis_sampah'           => $this->input->post('id_jenis_sampah', TRUE),
                'harga_nasional'            => $this->input->post('harga_nasional', TRUE),
                'id_superadmin'             => getID(),
                'status_tabel_harga_remove' => 1,
                'created_at'                => $dt->format('Y-m-d H:i:s')
            );
            $data = $this->Tabel_harga_model->insert($data);
            if($data){
                $this->session->set_flashdata('berhasil', 'Data berhasil ditambahkan');
                redirect('tabel_harga');
            } else {
                $this->session->set_flashdata('gagal', 'Data tidak ditambahkan');
                redirect('tabel_harga');
            }
        }
    }

    public function create_detail_action($id)
    {
        $this->db->where('id_tabel_harga', $id);
        $this->db->where('id_provinsi', $this->input->post('id_provinsi', TRUE));
        $data = $this->db->get('tabel_harga_detail')->row();
        if($data){
            $this->session->set_flashdata('gagal', 'Data sudah ada');
            redirect('tabel_harga/read/'.$id);
        } else {
            $data = array(
                'id_tabel_harga'    => $id,
                'id_provinsi'       => $this->input->post('id_provinsi', TRUE),
                'harga_provinsi'    => $this->input->post('harga_provinsi', TRUE),
            );
    
            $data = $this->Tabel_harga_model->insertdetail($data);
            if($data){
                $this->session->set_flashdata('berhasil', 'Berhasil Menambahkan Data');
                redirect(site_url('tabel_harga/read/' . $id));
            } else {
                $this->session->set_flashdata('gagal', 'Data Tidak Ditambahkan');
                redirect(site_url('tabel_harga/read/' . $id));
            }
        }
    }

    // public function update($id)
    // {
    //     $row = $this->Tabel_harga_model->get_by_id($id);

    //     if ($row) {
    //         $data = array(
    //             'tittle'                => 'Harga Nasional',
    //             'judul'                 => 'Harga Nasional',
    //             'title_card'           => 'Tabel Harga Nasional',
    //             'button'                => 'Update',
    //             'action'                => site_url('tabel_harga/update_action'),
    //             'id_tabel_harga'        => set_value('id_tabel_harga', $row->id_tabel_harga),
    //             'id_kategori_sampah'    => set_value('id_kategori_sampah', $row->id_kategori_sampah),
    //             'harga_nasional'        => set_value('harga_nasional', $row->harga_nasional),
    //             'harga_nasional_aktif'  => '1',
    //         );
            
    //         $data['kategori_sampah'] = $this->Tabel_harga_model->get_kategori_all();
    //         $data['kategori_sampah_cek'] = $this->Tabel_harga_model->get_kategori();

    //         $res['datakonten'] = $this->load->view('tabel_harga/tabel_harga_form', $data, true);
    //         $this->load->view('layouts/main_view', $res);
    //     } else {
    //         $this->session->set_flashdata('gagal', 'Data Tidak Ditemukan');
    //         redirect(site_url('tabel_harga'));
    //     }
    // }
    public function update_detail($id)
    {
        $row = $this->Tabel_harga_model->get_by_id_detail($id);
        $id_harga = $this->input->get('id_harga');
        if ($row) {
            $data = array(
                'tittle'                => 'Harga Nasional',
                'judul'                 => 'Harga Nasional',
                'tittle_card'           => 'Tabel Harga Nasional',
                'button'                => 'Update',
                'action'                => site_url('tabel_harga/update_detail_action?id_harga=' . $id_harga),
                'id_tbl_harga_detail'   => set_value('id_tbl_harga_detail', $row->id_tbl_harga_detail),
                'id_provinsi'           => set_value('id_provinsi', $row->id_provinsi),
                'harga_provinsi'        => set_value('harga_provinsi', $row->harga_provinsi),
                'id'                    => $id,
                'harga_nasional_aktif'  => '1',
                'menu_aktif'            => 'tabel_harga'
            );
            $data['provinsi'] = $this->Tabel_harga_model->get_provinsi_all($id_harga);
            $data['provinsi_all'] = $this->Tabel_harga_model->get_provinsi();

            $res['datakonten'] = $this->load->view('tabel_harga/tabel_harga_detail_form', $data, true);
            $this->load->view('layouts/main_view', $res);
        } else {
            $this->session->set_flashdata('gagal', 'Data Tidak Ditemukan');
            redirect(site_url('tabel_harga/read/' . $id_harga));
        }
    }

    // public function update_action()
    // {
    //     $this->_rules();

    //     if ($this->form_validation->run() == FALSE) {
    //         $this->update($this->input->post('id_tabel_harga', TRUE));
    //     } else {
    //         $data = array(
    //             'id_kategori_sampah' => $this->input->post('id_kategori_sampah', TRUE),
    //             'harga_nasional' => $this->input->post('harga_nasional', TRUE),
    //             'id_superadmin' => $this->session->userdata('uid'),
    //         );

    //         $this->Tabel_harga_model->update($this->input->post('id_tabel_harga', TRUE), $data);
    //         $this->session->set_flashdata('berhasil', 'Berhasil Merubah Data');
    //         redirect(site_url('tabel_harga'));
    //     }
    // }
    public function update_detail_action($id, $id_d)
    {
        $this->db->where('id_tabel_harga', $id);
        $data = $this->db->get('tabel_harga')->row();

        if($data){
            $this->db->where('id_tbl_harga_detail', $id_d);
            $data = $this->db->get('tabel_harga_detail')->row();
            if($data){
                $data = array(
                    'harga_provinsi'    => $this->input->post('harga_provinsi', TRUE),
                );
        
                $data = $this->Tabel_harga_model->updatedetail($id_d, $data);
                if($data){
                    $this->session->set_flashdata('berhasil', 'Berhasil Merubah Data');
                    redirect(site_url('tabel_harga/read/' . $id));
                } else {
                    $this->session->set_flashdata('gagal', 'Data tidak diubah');
                    redirect(site_url('tabel_harga/read/' . $id));
                }
                
            } else {
                $this->session->set_flashdata('gagal', 'Data tidak ditemukan');
                redirect('tabel_harga/read/'.$id);
            }
        } else {
            $this->session->set_flashdata('gagal', 'Data tidak ditemukan');
            redirect('tabel_harga/read/'.$id);
        }

    }

    public function del_sem($id)
    {
        $row = $this->Tabel_harga_model->get_by_id($id);

        if ($row) {
            $data = array(
                'status_tabel_harga_remove' => 0,
                'id_superadmin' => getID(),
            );
            $this->Tabel_harga_model->update($id, $data);
            $this->session->set_flashdata('berhasil', 'Berhasil Menghapus Data');
            redirect(site_url('tabel_harga'));
        } else {
            $this->session->set_flashdata('gagal', 'Data Tidak Ditemukan');
            redirect(site_url('tabel_harga'));
        }
    }
    public function delete_detail($id, $id_d)
    {
        $row = $this->Tabel_harga_model->get_by_id_detail($id_d);
        if ($row) {
            $data = $this->Tabel_harga_model->delete($id_d);
            if($data){
                $this->session->set_flashdata('berhasil', 'Data berhasil dihapus');
                redirect('tabel_harga/read/'.$id);
            } else {
                $this->session->set_flashdata('gagal', 'Data tidak dihapus');
                redirect('tabel_harga/read/'.$id);
            }
        } else {
            $this->session->set_flashdata('gagal', 'Data Tidak Ditemukan');
            redirect(site_url('tabel_harga/read/' . $id));
        }
    }

    public function _rules()
    {
        $this->form_validation->set_rules('id_kategori_sampah', 'id kategori sampah', 'trim|required');
        $this->form_validation->set_rules('harga_nasional', 'harga nasional', 'trim|required');
        $this->form_validation->set_rules('id_tabel_harga', 'id_tabel_harga', 'trim');
        $this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

    public function excel()
    {
        $this->load->helper('exportexcel');
        $namaFile = "tabel_harga.xls";
        $judul = "tabel_harga";
        $tablehead = 0;
        $tablebody = 1;
        $nourut = 1;
        //penulisan header
        header("Pragma: public");
        header("Expires: 0");
        header("Cache-Control: must-revalidate, post-check=0,pre-check=0");
        header("Content-Type: application/force-download");
        header("Content-Type: application/octet-stream");
        header("Content-Type: application/download");
        header("Content-Disposition: attachment;filename=" . $namaFile . "");
        header("Content-Transfer-Encoding: binary ");

        xlsBOF();

        $kolomhead = 0;
        xlsWriteLabel($tablehead, $kolomhead++, "No");
        xlsWriteLabel($tablehead, $kolomhead++, "Id Kategori Sampah");
        xlsWriteLabel($tablehead, $kolomhead++, "Id Jenis Sampah");
        xlsWriteLabel($tablehead, $kolomhead++, "Harga Nasional");

        foreach ($this->Tabel_harga_model->get_all() as $data) {
            $kolombody = 0;

            //ubah xlsWriteLabel menjadi xlsWriteNumber untuk kolom numeric
            xlsWriteNumber($tablebody, $kolombody++, $nourut);
            xlsWriteNumber($tablebody, $kolombody++, $data->id_kategori_sampah);
            xlsWriteNumber($tablebody, $kolombody++, $data->id_jenis_sampah);
            xlsWriteNumber($tablebody, $kolombody++, $data->harga_nasional);

            $tablebody++;
            $nourut++;
        }

        xlsEOF();
        exit();
    }

    public function exportxl()
    {
        $q          = $this->input->get('q');
        $subject    = "Tabel_Harga_Nasional";
        $this->db->query('SET @no=0');
        // $id         = $this->session->userdata('uid');
        // $sql        ="SELECT kategori_sampah.nama_kategori_sampah,tabel_harga.harga_nasional FROM `tabel_harga` JOIN `kategori_sampah` ON `tabel_harga`.`id_kategori_sampah` = `kategori_sampah`.`id_kategori_sampah` WHERE `status_tabel_harga_remove` = 1 ";
        $sql = "SELECT  @no:=@no+1 AS nomor, kategori_sampah.nama_kategori_sampah, jenis_sampah.nama_jenis_sampah ,CONCAT(tabel_harga.harga_nasional,' KOIN') AS harga_nasional
        FROM `tabel_harga`
        JOIN `kategori_sampah` ON `tabel_harga`.`id_kategori_sampah` = `kategori_sampah`.`id_kategori_sampah`
        JOIN `jenis_sampah` ON `tabel_harga`.`id_jenis_sampah` = `jenis_sampah`.`id_jenis_sampah`
        WHERE `status_tabel_harga_remove` = 1
        AND   (
        `kategori_sampah`.`nama_kategori_sampah` LIKE '%$q%' ESCAPE '!'
        OR  `jenis_sampah`.`nama_jenis_sampah` LIKE '%$q%' ESCAPE '!'
        OR  `harga_nasional` LIKE '%$q%' ESCAPE '!'
         )
        ORDER BY `tabel_harga`.`id_tabel_harga` DESC";
        exportSQL('Tabel_Harga_Nasional', $subject, $sql);
    }

    public function exportxlprovinsi($id)
    {

        $q          = $this->input->get('q');
        $jns          = $this->input->get('jns');     
        $subject    = $jns."_THP";
        // $id         = $this->session->userdata('uid');
        $sql        ="SELECT kategori_sampah.nama_kategori_sampah,provinsi.nama_provinsi,tabel_harga_detail.harga_provinsi FROM `tabel_harga_detail` JOIN `provinsi` ON `tabel_harga_detail`.`id_provinsi` = `provinsi`.`id_provinsi` JOIN tabel_harga ON tabel_harga.id_tabel_harga= tabel_harga_detail.id_tabel_harga JOIN kategori_sampah ON tabel_harga.id_kategori_sampah=kategori_sampah.id_kategori_sampah
         WHERE tabel_harga_detail.id_tabel_harga = '".$id."' ";
       
        exportSQL($jns.'_THP', $subject, $sql);
    }

    public function ambilDataHarga($id){
        $this->db->join('kategori_sampah', 'kategori_sampah.id_kategori_sampah = tabel_harga.id_kategori_sampah');
        $this->db->join('jenis_sampah', 'jenis_sampah.id_jenis_sampah = tabel_harga.id_jenis_sampah');
        $this->db->where('id_tabel_harga', $id);
        $this->db->where('status_tabel_harga_remove', '1');
        $data = $this->db->get('tabel_harga')->row();
        echo json_encode($data);
    }

    public function updateHargaAction($id){
        $this->db->where('id_tabel_harga', $id);
        $data = $this->db->get('tabel_harga')->row();
        if($data){
            $dt = new DateTime();
            $data = array(
                'harga_nasional'    => $this->input->post('harga_nasional', TRUE),
                'update_at'         => $dt->format('Y-m-d H:i:s')
            );
            $data = $this->db->where('id_tabel_harga', $id)->update('tabel_harga', $data);
            if($data){
                $this->session->set_flashdata('berhasil', 'Data berhasil diubah');
                redirect('tabel_harga');
            } else {
                $this->session->set_flashdata('gagal', 'Data tidak diubah');
                redirect('tabel_harga');
            }
        } else {
            $this->session->set_flashdata('gagal', 'Data tidak ditemukan');
            redirect('tabel_harga');
        }
    }

    public function updateHargaActions($id){
        $this->db->where('id_tabel_harga', $id);
        $data = $this->db->get('tabel_harga')->row();
        if($data){
            $dt = new DateTime();
            $data = array(
                'harga_nasional'    => $this->input->post('harga_nasional', TRUE),
                'update_at'         => $dt->format('Y-m-d H:i:s')
            );
            $data = $this->db->where('id_tabel_harga', $id)->update('tabel_harga', $data);
            if($data){
                $this->session->set_flashdata('berhasil', 'Data berhasil diubah');
                redirect('tabel_harga/read/'.$id);
            } else {
                $this->session->set_flashdata('gagal', 'Data tidak diubah');
                redirect('tabel_harga/read/'.$id);
            }
        } else {
            $this->session->set_flashdata('gagal', 'Data tidak ditemukan');
            redirect('tabel_harga');
        }
    }

    public function ambilDataDetailHarga($id){
        $this->db->join('provinsi', 'provinsi.id_provinsi = tabel_harga_detail.id_provinsi');
        $this->db->where('id_tbl_harga_detail', $id);
        $data = $this->db->get('tabel_harga_detail')->row();
        echo json_encode($data);
    }
}
/* End of file Tabel_harga.php */
/* Location: ./application/controllers/Tabel_harga.php */
