<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Ekspedisi_model extends CI_Model
{

    public $table   = 'ekspedisi';
    public $id      = 'kode_ekspedisi';
    public $order   = 'DESC';

    function __construct()
    {
        parent::__construct();
    }

    function total_rows($q = NULL) {
        $this->db->group_start();
        $this->db->or_like('nama_ekspedisi', $q);
        $this->db->group_end();
        $this->db->from($this->table);
        return $this->db->count_all_results();
    }

    function get_limit_data($limit, $start = 0, $q = NULL) {
        $this->db->order_by($this->id, $this->order);
        $this->db->group_start();
        $this->db->or_like('nama_ekspedisi', $q);
        $this->db->group_end();
        $this->db->limit($limit, $start);
        return $this->db->get($this->table)->result();
    }

    function insert($data)
    {
        $this->db->insert($this->table, $data);
    }

    function update($id, $data)
    {
        $this->db->where($this->id, $id);
        $this->db->update($this->table, $data);
    }

    function delete($id)
    {
        $this->db->where($this->id, $id);
        $this->db->delete($this->table);
    }

}

/* End of file Kategori_produk_model.php */
/* Location: ./application/models/Kategori_produk_model.php */
/* Please DO NOT modify this information : */
/* Generated by Harviacode Codeigniter CRUD Generator 2020-07-15 09:43:37 */
/* http://harviacode.com */