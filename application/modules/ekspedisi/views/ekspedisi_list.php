<div class="card card-<?= bgCard() ?>">
    <div class="card-header">
        <div class="card-title">
            <?= $titleCard ?>
        </div>
    </div>
    <div class="card-body">
        <div class="tab-content p-0">
            <div class="row">
                <div class="col-md-4 mb-2">
                    <button class="btn btn-sm btn-<?= bgCard() ?>" title="Tambah Ekspedisi" data-toggle="modal" data-target="#modalCreateMetode"><i class="fas fa-pencil-alt"></i> Tambah Ekspedisi</button>
                </div>
                <div class="col-md-3 offset-md-5 mb-2">
                    <?= search(site_url('ekspedisi'), site_url('ekspedisi'), $q) ?>
                </div>
            </div>
            <div class="tab-content p-0" style="overflow:auto">
                <table class="table table-bordered table-striped table-condensed table-hover table-sm" style="margin-bottom: 10px">
                    <tr>
                        <th class='text-center' style="width: 60px;">No</th>
                        <th class='text-center'>Nama Ekspedisi</th>
                        <th class='text-center' style="width: 150px;">Aksi</th>
                    </tr><?php
                    if ($total_rows == 0) {
                        echo '<tr><td colspan="11" bgcolor="grey" style="color: white; font-weight: bold; text-align: center;">Data Tidak Ditemukan</td></tr>';
                    } else {
                        foreach ($ekspedisi_data as $data)
                        {
                            ?>
                    <tr>
                        <td class='text-center'><?php echo ++$start ?></td>
                        <td><?php echo $data->nama_ekspedisi ?></td>
                        <td class='text-center'>
                        <div class="btn-group">
                            <button class="btn btn-success btn-sm" title="Edit" data-toggle="modal" data-target="#modalEditversi" onclick="cekEditversi('<?= $data->kode_ekspedisi ?>')"><i class="fas fa-edit"></i></button>
                            <button class="btn btn-danger btn-sm" title="Edit" data-toggle="modal" data-target="#modalEditversi" onclick="cekEditversi('<?= $data->kode_ekspedisi ?>')"><i class="fas fa-trash"></i></button>
                        </div>
                          
                        </td>
                    </tr>
                            <?php
                        }
                    }
                        ?>
                </table>
            </div>
            <?= footer($total_rows, $pagination, '') ?>
        </div>
    </div>
</div>

<div id="modalCreateMetode" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-md" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Form Tambah Ekspedisi</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form action="<?= $action ?>" method="post" id="formTambahMetode" role="form" enctype="multipart/form-data">
                <div class="row ml-3 mr-3">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="varchar">Nama Ekspedisi</label>
                            <input type="text" class="form-control" name="metode_pembayaran" id="metode_pembayaran" placeholder="Metode Pembayaran" value="" />
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <div class="col-md-12 text-center">
                    <button type="submit" class="btn btn-<?= bgCard() ?>" style="width: 100px;">Simpan</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal" style="width: 100px;">Batal</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="modalEditMetode" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Form Tambah Metode Pembayaran</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form action="<?= $action ?>" method="post" id="formTambahMetode" role="form" enctype="multipart/form-data">
                <div class="row ml-3 mr-3">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="varchar">Nama Metode Pembayaran</label>
                            <input type="text" class="form-control" name="metode_pembayaran" id="metode_pembayaran" placeholder="Metode Pembayaran" value="" />
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <div class="col-md-12 text-center">
                    <button type="submit" class="btn btn-<?= bgCard() ?>" style="width: 100px;">Simpan</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal" style="width: 100px;">Batal</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>