<div class="row">
	<!-- Left col -->
	<section class="col-lg connectedSortable ui-sortable">
		<!-- Custom tabs (Charts with tabs)-->
		<div class="card card-info">
			<div class="card-header">
                <div class="card-title">
                  <?php echo $title_card?>
                </div>
              </div>
			<div class="card-body">
				<div class="tab-content p-0">

					<form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="dataform">
						<div class="form-group">
							<label for="varchar">    Nama Bantuan <?php echo form_error('nama_menu_bantuan') ?></label>
							<input type="text" class="form-control" name="nama_menu_bantuan" id="nama_menu_bantuan"
							placeholder="Nama Bantuan" value="<?php echo $nama_menu_bantuan; ?>" />
						</div>
						     <div class="form-group">
						     	<?php 
						     	if($aktif==1){
						     		echo "<h3>Status User : Bank Sampah</h3>";
						     	}else if($aktif==2){
						     		echo "<h3>Status User : Nasabah</h3>";
						     	}else{
						     			echo "<h3>Status User : Pengepul</h3>";
						     	}
						     	?>
                             	<input type="hidden" name="status_user" id="status_user" value="<?=$aktif?>">
                                </select>
                            </div>
						<input type="hidden" name="id_menu_bantuan" value="<?php echo $id_menu_bantuan; ?>" />

						<button id="btn-simpan" type="submit" class="btn btn-info" style="width: 100px;">Simpan
						</button>
						<a href="<?php echo site_url('bantuan') ?>" class="btn btn-default" style="width: 100px;">Batal</a>
					</form>
				</div>
			</div><!-- /.card-body -->
		</div>
		<!-- /.card -->
	</section>
	<!-- /.Left col -->
</div>

<script>

    $(document).ready(function () {
      $('#dataform').validate({
        rules: {
        	nama_menu_bantuan: {
        		required: true,
        	}
        },
        messages: {
        	nama_menu_bantuan: {
        		required: "Nama Bantuan Tidak Boleh Kosong",
        	}
        },
        submitHandler: function(form) {
	        $('#btn-simpan').prop('disabled', true);
          	form.submit();
        },
        errorElement: 'span',
        errorPlacement: function (error, element) {
          error.addClass('invalid-feedback');
          element.closest('.form-group').append(error);
        },
        highlight: function (element, errorClass, validClass) {
          $(element).addClass('is-invalid');
        },
        unhighlight: function (element, errorClass, validClass) {
          $(element).removeClass('is-invalid');
        },
      });
    });

	function cek() {
		var x = document.getElementById("logo_brandbaru");
		if (x.style.display === "none") {
			x.style.display = "block";
		} else {
			x.style.display = "none";
		}
	}
</script>