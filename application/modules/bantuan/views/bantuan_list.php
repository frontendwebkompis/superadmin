<script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>

<div class="row">
	<section class="col-lg connectedSortable ui-sortable">
		<div class="card card-info">
			<div class="card-header">
                <div class="card-title">
                  <?php echo $title_card?>
                </div>
              </div>
			<div class="card-body">
				<div class="tab-content p-0">

		<div class="row">
			<div class="col-md-4 mb-2">
				<?php echo anchor(site_url('bantuan/create?aktif='.$aktif),'Tambah Data', 'class="btn btn-info btn-sm"'); ?>
			</div>
			<div class="col-md-4 mb-2">
            	<a href="<?php echo base_url('bantuan') ?>" class="btn <?= ($aktif==1) ? ' btn-success btn-sm' : ' btn-default btn-sm'; ?>"> Bank Sampah</a>
				<a href="<?php echo base_url('bantuan/nasabah') ?>" class="btn <?= ($aktif==2) ? ' btn-success btn-sm' : ' btn-default btn-sm'; ?>" >Nasabah</a>
				<a href="<?php echo base_url('bantuan/pengepul') ?>" class="btn <?= ($aktif==3) ? ' btn-success btn-sm' : ' btn-default btn-sm'; ?>" > Pengepul</a>  
            </div>
			<div class="col-md-3 offset-md-1 mb-2">
				<?= search(site_url('bantuan/index'), site_url('bantuan'), $q) ?>
			</div>
		</div>
				<div class="tab-content p-0" style="overflow:auto">
				<table class="table table-bordered table-striped table-condensed table-hover table-sm" style="margin-bottom: 10px">

						<tr>
							<th class="text-center" width="50px">No</th>
							<th>Nama Bantuan</th>
							<th>Status User</th>
							<th class="text-center" width="70px">Order</th>
							<th class="text-center">Aksi</th>
						</tr><?php 
						if($total_rows == 0){
							echo '<tr><td colspan="8" bgcolor="grey" style="color: white; font-weight: bold; text-align: center;">Data Tidak Ditemukan</td></tr>';
						} else {
						foreach ($bantuans as $bantuan){?>
						<tr>
							<td class="text-center"><?php echo ++$start ?></td>
							<td><?php echo $bantuan->nama_menu_bantuan ?></td>
							<td><?php 
							if($bantuan->status_user==1)
							echo "Bank Sampah";
							else if($bantuan->status_user==2)echo "Nasabah Online"; else
							echo "Pengepul"; ?></td>
							<td>
								<?php
								if(count($bantuans)==1)
									echo "Urutan Pertama";
								if(cekposisi($bantuan->status_user)->min!=$bantuan->order_id){
									?>
								<a href="bantuan/cekup/<?php echo $bantuan->id_menu_bantuan ?>?id=<?= $aktif ?>"><i class="fas fa-arrow-circle-up"></i></a>
									<?php }?>
									<?php
								if(cekposisi($bantuan->status_user)->max!=$bantuan->order_id){
									?>
								<a href="bantuan/cekdown/<?php echo $bantuan->id_menu_bantuan ?>?id=<?= $aktif ?>"><i class="fas fa-arrow-circle-down"></i></a>
									<?php }?>
							</td>
							<td class="text-center" width="150px">
								<div class="btn-group">
									<a href="<?php echo site_url('bantuan_detail/tampil/'.$bantuan->id_menu_bantuan.'?aktif='.$aktif); ?>"
									data-toogle="tooltip" title="Lihat">
									<button type="button" class="btn btn-info btn-sm"><i class="fas fa-info-circle"></i></button></a>
									<a href="<?php echo site_url('bantuan/update/'.$bantuan->id_menu_bantuan.'?aktif='.$aktif); ?>"
									data-toogle="tooltip" title="Update">
									<button type="button" class="btn btn-success btn-sm"><i class="far fa-edit"></i></button></a>
									<a href="#" class="btn btn-danger btn-sm" data-toggle="tooltip" data-placement="top" title="Hapus" onclick="confirm('<?= $bantuan->id_menu_bantuan ?>')" ><i class="fas fa-trash-alt"></i></a>
								</div>
							</td>
						</tr>
						<?php } 
						} ?>
					</table>
					</div>
					<?= footer($total_rows, $pagination, '') ?>
				</div>
			</div>
		</div>
	</section>
</div>

<script>
function confirm(res) {
    Swal.fire({
    title: 'Anda Yakin Mau Menghapus?',
    text: "Tidak bisa dikembalikan jika sudah dihapus!",
    icon: 'warning',
    showCancelButton: true,
    confirmButtonColor: '#3085d6',
    cancelButtonColor: '#d33',
    confirmButtonText: 'Ya, Hapus!',
    cancelButtonText: 'Batal'
    }).then((result) => {
    if (result.value) {
        Swal.fire(
        'Terhapus!',
        'File sudah terhapus.',
        'success'
        )
        window.location='<?php echo base_url().'bantuan/del_sem/'; ?>'+res+'?id=<?= $aktif ?>';
    }
    });
}
</script>