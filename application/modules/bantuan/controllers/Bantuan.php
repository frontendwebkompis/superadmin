<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Bantuan extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        if (cek_token()) {
            $this->load->model('Bantuan_model');
            // $this->load->model('Bantuan_detail_model');
            $this->load->library('form_validation');
            $this->load->library('upload');
        } else {
            logout();
        }
    }

    public function index()
    {
        $q = urldecode($this->input->get('q', TRUE));
        $start = intval($this->input->get('start'));

        if ($q <> '') {
            $config['base_url'] = base_url() . 'bantuan/?q=' . urlencode($q);
            $config['first_url'] = base_url() . 'bantuan/?q=' . urlencode($q);
        } else {
            $config['base_url'] = base_url() . 'bantuan/';
            $config['first_url'] = base_url() . 'bantuan/';
        }

        $config['per_page'] = 10;
        $config['page_query_string'] = TRUE;
        $config['total_rows'] = $this->Bantuan_model->total_rows($q);
        $cek = $this->Bantuan_model->get_limit_data($config['per_page'], $start, $q);

        $this->load->library('pagination');
        $this->pagination->initialize($config);

        $data = array(
            'tittle'        => 'Bantuan',
            'judul'         => 'Bantuan',
            'title_card'    => 'Menu Bantuan',
            'bantuans'      => $cek,
            'q'             => $q,
            'pagination'    => $this->pagination->create_links(),
            'total_rows'    => $config['total_rows'],
            'start'         => $start,
            'aktif'         =>'1',
            'bantuan_aktif' => '1',
            'menu_aktif'    => 'bantuan',
        );
        $res['datakonten'] = $this->load->view('bantuan/bantuan_list', $data, true);
        $this->load->view('layouts/main_view', $res);
    }
public function nasabah()
    {
        $q = urldecode($this->input->get('q', TRUE));
        $start = intval($this->input->get('start'));

        if ($q <> '') {
            $config['base_url'] = base_url() . 'bantuan/nasabah?q=' . urlencode($q);
            $config['first_url'] = base_url() . 'bantuan/?q=' . urlencode($q);
        } else {
            $config['base_url'] = base_url() . 'bantuan/nasabah';
            $config['first_url'] = base_url() . 'bantuan/nasabah';
        }

        $config['per_page'] = 10;
        $config['page_query_string'] = TRUE;
        $config['total_rows'] = $this->Bantuan_model->total_rows_nasabah($q);
        $cek = $this->Bantuan_model->get_limit_data_nasabah($config['per_page'], $start, $q);

        $this->load->library('pagination');
        $this->pagination->initialize($config);

        $data = array(
            'tittle'        => 'Bantuan',
            'judul'         => 'Bantuan',
            'title_card'    => 'Menu Bantuan',
            'bantuans'      => $cek,
            'q'             => $q,
            'pagination'    => $this->pagination->create_links(),
            'total_rows'    => $config['total_rows'],
            'start'         => $start,
            'aktif'         => '2',
            'bantuan_aktif' => '1',
            'menu_aktif'    => 'bantuan',
        );
        $res['datakonten'] = $this->load->view('bantuan/bantuan_list', $data, true);
        $this->load->view('layouts/main_view', $res);
    }
    public function pengepul()
    {
        $q = urldecode($this->input->get('q', TRUE));
        $start = intval($this->input->get('start'));

        if ($q <> '') {
            $config['base_url'] = base_url() . 'bantuan/pengepul?q=' . urlencode($q);
            $config['first_url'] = base_url() . 'bantuan/pengepul?q=' . urlencode($q);
        } else {
            $config['base_url'] = base_url() . 'bantuan/pengepul';
            $config['first_url'] = base_url() . 'bantuan/pengepul';
        }

        $config['per_page'] = 10;
        $config['page_query_string'] = TRUE;
        $config['total_rows'] = $this->Bantuan_model->total_rows_pengepul($q);
        $cek = $this->Bantuan_model->get_limit_data_pengepul($config['per_page'], $start, $q);

        $this->load->library('pagination');
        $this->pagination->initialize($config);

        $data = array(
            'tittle'        => 'Bantuan',
            'judul'         => 'Bantuan',
            'title_card'    => 'Menu Bantuan',
            'bantuans'      => $cek,
            'q'             => $q,
            'pagination'    => $this->pagination->create_links(),
            'total_rows'    => $config['total_rows'],
            'start'         => $start,
            'aktif'         =>'3',
            'bantuan_aktif' => '1',
            'menu_aktif'    => 'bantuan',
        );
        $res['datakonten'] = $this->load->view('bantuan/bantuan_list', $data, true);
        $this->load->view('layouts/main_view', $res);
    }

    public function read($id)
    {
        $row = $this->Bantuan_model->get_by_id($id);
        if ($row) {
            $data = array(
                'tittle'            => 'Brand',
                'judul'             => 'Brand',
                'title_card'        => 'Detail Brand',
                'id_menu_bantuan'   => $row->id_menu_bantuan,
                'nama_menu_bantuan' => $row->nama_menu_bantuan,
                'bantuan_aktif'     => '1',
                'menu_aktif'        => 'bantuan',
            );
            $res['datakonten'] = $this->load->view('bantuan/bantuan_read', $data, true);
            $this->load->view('layouts/main_view', $res);
        } else {
            $this->session->set_flashdata('message', '<div class="alert alert-warning" role="alert">
            Data Tidak Di Temukan</div>');
            redirect(site_url('bantuan'));
        }
    }

    public function create()
    {
         $order = $this->Bantuan_model->get_by_order();   
         $aktif=$this->input->get('aktif');
     
        $data = array(
            'tittle'            => 'Bantuan',
            'judul'             => 'Bantuan',
            'title_card'        => 'Tambah Bantuan',
            'button'            => 'Create',
            'status_user'       => set_value('status_user'),
            'action'            => site_url('bantuan/create_action'),
            'id_menu_bantuan'   => set_value('id_menu_bantuan'),
            'nama_menu_bantuan' => set_value('nama_menu_bantuan'),
            'order'             => $order,
            'aktif'             =>$aktif,
            'bantuan_aktif'     => '1',
            'menu_aktif'        => 'bantuan',
        );
        $res['datakonten'] = $this->load->view('bantuan/bantuan_form', $data, true);
        $this->load->view('layouts/main_view', $res);
    }


    public function create_action()
    {

  $order = $this->Bantuan_model->get_by_order();
  $cek=0;   
      $cek=$order->order_id+1; 
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->create();
        } else {
            $data = array(

                'nama_menu_bantuan' => $this->input->post('nama_menu_bantuan', TRUE),
                'order_id' => $cek,
 'status_user' => $this->input->post('status_user', TRUE),
                'status_remove' => 1,
                 'created_at' => @date('Y-m-d H:i:s'),
           
            );
            $this->Bantuan_model->insert($data);
            $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">Berhasil Menambahkan Data </div>');
            redirect(site_url('bantuan_detail/tampil/' . $this->db->insert_id()));
        }
    }

function cekup($id=null){
$iddata=$this->input->get('id');
$row = $this->Bantuan_model->get_by_id($id);
if($row){
$simpan=$row->order_id;
$tambah=$simpan-1;
 $data = array(
      'order_id' => $simpan,
            );
            $this->Bantuan_model->updateorder($tambah, $data);

            $data = array(
      'order_id' => $tambah,
            );
            $this->Bantuan_model->update($row->id_menu_bantuan, $data);
            if($iddata==1)
            redirect('bantuan'); 
            else if($iddata==2)
            redirect('bantuan/nasabah');
            else 
            redirect('bantuan/pengepul');
} else {
            $this->session->set_flashdata('message', '<div class="alert alert-warning" role="alert">
            Data Tidak Di Temukan</div>');

            redirect(site_url('bantuan'));
        }

}
function cekdown($id=null){

$iddata=$this->input->get('id');
$row = $this->Bantuan_model->get_by_id($id);
if($row){
$simpan=$row->order_id;
$tambah=$simpan+1;
 $data = array(
      'order_id' => $simpan,
            );
            $this->Bantuan_model->updateorder($tambah, $data);

            $data = array(
      'order_id' => $tambah,
            );
            $this->Bantuan_model->update($row->id_menu_bantuan, $data);
   if($iddata==1)
            redirect('bantuan'); 
            else if($iddata==2)
            redirect('bantuan/nasabah');
            else 
            redirect('bantuan/pengepul');
} else {
            $this->session->set_flashdata('message', '<div class="alert alert-warning" role="alert">
            Data Tidak Di Temukan</div>');
            redirect(site_url('bantuan'));
        }
}
    public function update($id)
    {
        $aktif=$this->input->get('aktif');
        $row = $this->Bantuan_model->get_by_id($id);
        if ($row) {
            $data = array(
                'tittle'            => 'Bantuan',
                'judul'             => 'Bantuan',
                'title_card'        => 'Update Bantuan',
                'button'            => 'Update',
                'action'            => site_url('bantuan/update_action'),
                'id_menu_bantuan'   => set_value('id_menu_bantuan', $row->id_menu_bantuan),
                'nama_menu_bantuan' => set_value('nama_menu_bantuan', $row->nama_menu_bantuan),
                'aktif'             => $aktif,
                'bantuan_aktif'     => '1',
                'menu_aktif'        => 'bantuan',
            );
            $res['datakonten'] = $this->load->view('bantuan/bantuan_form', $data, true);
            $this->load->view('layouts/main_view', $res);
        } else {
            $this->session->set_flashdata('gagal', 'Data Tidak Ditemukan');
            if($aktif==1)
            redirect(site_url('bantuan'));
        else if($aktif==2)
            redirect(site_url('bantuan/nasabah'));
        else redirect(site_url('bantuan/pengepul'));
        }
    }

    public function update_action()
    {
        $this->_rules();
           $aktif=$this->input->get('aktif');
     
       if ($this->form_validation->run() == FALSE) {
            $this->update($this->input->post('id_menu_bantuan', TRUE));
        } else {
            $data = array(
                'nama_menu_bantuan' => $this->input->post('nama_menu_bantuan', TRUE),
            );

            $this->Bantuan_model->update($this->input->post('id_menu_bantuan', TRUE), $data);
            $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">Berhasil Merubah Data</div>');
           if($aktif==1)
            redirect(site_url('bantuan'));
        else if($aktif==2)
            redirect(site_url('bantuan/nasabah'));
        else redirect(site_url('bantuan/pengepul'));
        }
    }

    public function del_sem($id)
    {
        $row = $this->Bantuan_model->get_by_id($id);
$iddata=$this->input->get('id');
$cekdata=$this->Bantuan_model->get_order_all($iddata);

foreach ($cekdata as $key ) {
    # code...
  
    if($key->order_id>$row->order_id)
    {
          $ss=$key->order_id;
        $kurang=$ss-1;
        $datas = array(
      'order_id' => $kurang,
            ); 
            $this->Bantuan_model->update($key->id_menu_bantuan, $datas);
      
    }
}
        
        if ($row) {
            $data = array(
                'status_remove' => 0,
                'order_id'=>'',
            );
            $this->Bantuan_model->update($id, $data);
            $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">Berhasil Menghapus Data</div>');
            redirect(site_url('bantuan'));
        } else {
            $this->session->set_flashdata('message', '<div class="alert alert-warning" role="alert">Data Tidak Ditemukan</div>');
            redirect(site_url('bantuan'));
        }
    }


    public function _rules()
    {
        $this->form_validation->set_rules('nama_menu_bantuan', 'nama menu bantuan', 'trim|required');
        $this->form_validation->set_rules('id_menu_bantuan', 'id_menu_bantuan', 'trim');
        $this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

}

/* End of file Brand.php */
/* Location: ./application/controllers/Brand.php */
