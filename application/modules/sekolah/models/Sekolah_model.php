<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Sekolah_model extends CI_Model
{

    public $table = 'sekolah';
    public $table_detail = 'sekolah_detail';
    public $id = 'id_sekolah';
    public $order = 'DESC';

    function __construct()
    {
        parent::__construct();
    }

    function get_by_poto($id)
    {
        $this->db->where('foto_ktp_penanggungjawab', $id);
        return $this->db->get($this->table_detail)->row();
    }
    
    function get_by_logo($id)
    {
        $this->db->where('logo_sekolah', $id);
        return $this->db->get($this->table)->row();
    }
    function get_by_sk($id)
    {
        $this->db->where('sk_sekolah', $id);
        return $this->db->get($this->table_detail)->row();
    }
    function get_by_bangunan($id)
    {
        $this->db->where('foto_bangunan_sekolah', $id);
        return $this->db->get($this->table_detail)->row();
    }
    function get_by_poto_selfi($id)
    {
        $this->db->where('selfie_ktp_penanggungjawab', $id);
        return $this->db->get($this->table_detail)->row();
    }
    // get all
    function get_all()
    {
        $this->db->order_by($this->id, $this->order);
        return $this->db->get($this->table)->result();
    }
    function get_reason($cek=0)
    {
        if($cek==1){
 $this->db->where('id_proses', 1);
      
        }else{
            $this->db->where('id_proses', 12);
        }
 
        return $this->db->get('reason')->result();
    }
    // get data by id
    function get_by_id($id)
    {
        $this->db->where($this->id, $id);
        return $this->db->get($this->table)->row();
    }

    // get data by id
    function get_by_id_detail($id)
    {
        $query = $this->db->query("SELECT sekolah.id_sekolah, sekolah.id_status_bank_sampah,sekolah.email_sekolah,status_bank_sampah.ket_status_bank_sampah,sekolah.nama_sekolah,sekolah.nomor_tlp_sekolah,sekolah.kode_verifikasi_sekolah,sekolah.last_login,sekolah.tgl_daftar,sekolah.alamat_sekolah,provinsi.nama_provinsi,kabupaten.nama_kabupaten,kecamatan.nama_kecamatan,sekolah.lat_sekolah,sekolah.long_sekolah,sekolah_detail.* FROM sekolah
        LEFT JOIN status_bank_sampah ON status_bank_sampah.id_status_bank_sampah=sekolah.id_status_bank_sampah
        RIGHT JOIN sekolah_detail ON sekolah_detail.id_sekolah=sekolah.id_sekolah
        LEFT JOIN provinsi ON provinsi.id_provinsi=sekolah.id_provinsi
        LEFT JOIN kabupaten ON kabupaten.id_kabupaten=sekolah.id_kabupaten
        LEFT JOIN kecamatan ON kecamatan.id_kecamatan=sekolah.id_kecamatan
         where sekolah.id_sekolah='" . $id . "'");

        return $query->row();
    }

    // get total rows
    function total_rows($q = NULL)
    {
        
        $this->db->where($this->table . '.id_status_bank_sampah', 3);
        $this->db->group_start();
        $this->db->or_like($this->table . '.nama_sekolah', $q);
        $this->db->group_end();
        $this->db->join('status_bank_sampah', 'sekolah.id_status_bank_sampah=status_bank_sampah.id_status_bank_sampah');
            $this->db->from($this->table);
        return $this->db->count_all_results();
    }

    // get data with limit and search
    function get_limit_data($limit, $start = 0, $q = NULL)
    {
        $this->db->order_by($this->id, $this->order);
        $this->db->where($this->table . '.id_status_bank_sampah', 3);
        $this->db->group_start();
        $this->db->or_like($this->table . '.nama_sekolah', $q);
        $this->db->group_end();
        $this->db->limit($limit, $start);
   $this->db->join('status_bank_sampah', 'sekolah.id_status_bank_sampah=status_bank_sampah.id_status_bank_sampah');
        return $this->db->get($this->table)->result();
    }

    // get total rows
    function total_rows_approve($q = NULL)
    {
        $this->db->where($this->table . '.id_status_bank_sampah', 0);
        $this->db->group_start();
        $this->db->or_like('nama_sekolah', $q);
        $this->db->group_end();
        $this->db->join('status_bank_sampah', 'sekolah.id_status_bank_sampah=status_bank_sampah.id_status_bank_sampah');
        $this->db->from($this->table);
        return $this->db->count_all_results();
    }

    // get data with limit and search
    function get_limit_data_approve($limit, $start = 0, $q = NULL)
    {
      $this->db->order_by($this->id, $this->order);
        $this->db->where($this->table . '.id_status_bank_sampah', 0);
        $this->db->group_start();
        $this->db->or_like($this->table . '.nama_sekolah', $q);
        $this->db->group_end();
        $this->db->limit($limit, $start);
   $this->db->join('status_bank_sampah', 'sekolah.id_status_bank_sampah=status_bank_sampah.id_status_bank_sampah');
        return $this->db->get($this->table)->result();
    }
    // get total rows
    function total_rows_premium($q = NULL)
    {
        $this->db->where($this->table . '.id_status_bank_sampah', 2);
        $this->db->group_start();
        $this->db->or_like('nama_sekolah', $q);
        $this->db->group_end();
        $this->db->from($this->table);
       $this->db->join('status_bank_sampah', 'sekolah.id_status_bank_sampah=status_bank_sampah.id_status_bank_sampah');
             return $this->db->count_all_results();
    }

    // get data with limit and search
    function get_limit_data_premium($limit, $start = 0, $q = NULL)
    {
       $this->db->order_by($this->id, $this->order);
        $this->db->where($this->table . '.id_status_bank_sampah', 2);
        $this->db->group_start();
        $this->db->or_like($this->table . '.nama_sekolah', $q);
        $this->db->group_end();
        $this->db->limit($limit, $start);
   $this->db->join('status_bank_sampah', 'sekolah.id_status_bank_sampah=status_bank_sampah.id_status_bank_sampah');
        return $this->db->get($this->table)->result();
    }
    // get total rows
    // get total rows
    function total_rows_banned($q = NULL)
    {
        $this->db->where($this->table . '.id_status_bank_sampah', 4);
        $this->db->group_start();
        $this->db->or_like('nama_sekolah', $q);
        $this->db->group_end();
        $this->db->from($this->table);
       $this->db->join('status_bank_sampah', 'sekolah.id_status_bank_sampah=status_bank_sampah.id_status_bank_sampah');
             return $this->db->count_all_results();
    }

    // get data with limit and search
    function get_limit_data_banned($limit, $start = 0, $q = NULL)
    {
      $this->db->order_by($this->id, $this->order);
        $this->db->where($this->table . '.id_status_bank_sampah', 4);
        $this->db->group_start();
        $this->db->or_like($this->table . '.nama_sekolah', $q);
        $this->db->group_end();
        $this->db->limit($limit, $start);
   $this->db->join('status_bank_sampah', 'sekolah.id_status_bank_sampah=status_bank_sampah.id_status_bank_sampah');
        return $this->db->get($this->table)->result();
    }
    // get total rows
    function total_rows_berkas($q = NULL)
    {
        $this->db->where($this->table . '.id_status_bank_sampah', 1);
        $this->db->group_start();
        $this->db->or_like('nama_sekolah', $q);
        $this->db->group_end();
        $this->db->from($this->table);
        $this->db->join('status_bank_sampah', 'sekolah.id_status_bank_sampah=status_bank_sampah.id_status_bank_sampah');
        return $this->db->count_all_results();
    }

    // get data with limit and search
    function get_limit_data_berkas($limit, $start = 0, $q = NULL)
    {
    $this->db->order_by($this->id, $this->order);
        $this->db->where($this->table . '.id_status_bank_sampah', 1);
        $this->db->group_start();
        $this->db->or_like($this->table . '.nama_sekolah', $q);
        $this->db->group_end();
        $this->db->limit($limit, $start);
   $this->db->join('status_bank_sampah', 'sekolah.id_status_bank_sampah=status_bank_sampah.id_status_bank_sampah');
        return $this->db->get($this->table)->result();
    }

    // insert data
    function insertdetailsekolah($id)
    {
        $data = array(
            'id_sekolah' => $id,
        );
        $this->db->insert('sekolah_detail', $data);
    }

    // insert data
    function insert($data)
    {
        $this->db->insert($this->table, $data);
    }

    // update data
    function update($id, $data)
    {
        $this->db->where($this->id, $id);
        $this->db->update($this->table, $data);
    }

    // delete data
    function delete($id)
    {
        $this->db->where($this->id, $id);
        $this->db->delete($this->table);
    }
}

/* End of file sekolah_model.php */
/* Location: ./application/models/sekolah_model.php */
/* Please DO NOT modify this information : */
/* Generated by Harviacode Codeigniter CRUD Generator 2020-02-14 04:12:17 */
/* http://harviacode.com */
