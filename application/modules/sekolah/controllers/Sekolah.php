<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Sekolah extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        if (cek_token()) {
            $this->load->model('Sekolah_model');
            $this->load->library('form_validation');
        } else {
            logout();
        }
    }

    public function index()
    {
        $q = urldecode($this->input->get('q', TRUE));
        $start = intval($this->input->get('start'));
        
        if ($q <> '') {
            $config['base_url'] = base_url() . 'sekolah?q=' . urlencode($q);
            $config['first_url'] = base_url() . 'sekolah?q=' . urlencode($q);
        } else {
            $config['base_url'] = base_url() . 'sekolah';
            $config['first_url'] = base_url() . 'sekolah';
        }

        $config['per_page']             = 10;
        $config['page_query_string']    = TRUE;
        $config['total_rows']           = $this->Sekolah_model->total_rows($q);
        $sekolah = $this->Sekolah_model->get_limit_data($config['per_page'], $start, $q);

        $this->load->library('pagination');
        $this->pagination->initialize($config);

        $data = array(
            'tittle'            => 'Sekolah',
            'judul'             => 'Sekolah',
            'title_card'        => 'List Sekolah',
            'tombol_aktif'      => 'sekolah',
            'sekolah_data'      => $sekolah,
            'q'                 => $q,
            'pagination'        => $this->pagination->create_links(),
            'total_rows'        => $config['total_rows'],
            'start'             => $start,
            'sekolah_aktif'     => '1',
            'menu_aktif'        => 'sekolah'
        );
        $res['datakonten'] = $this->load->view('sekolah/sekolah_list', $data,true);
        $this->load->view('layouts/main_view', $res);
    }


    public function approve()
    {
        $q = urldecode($this->input->get('q', TRUE));
        $start = intval($this->input->get('start'));
        
        if ($q <> '') {
            $config['base_url'] = base_url() . 'sekolah/approve?q=' . urlencode($q);
            $config['first_url'] = base_url() . 'sekolah/approve?q=' . urlencode($q);
        } else {
            $config['base_url'] = base_url() . 'sekolah/approve';
            $config['first_url'] = base_url() . 'sekolah/approve';
        }

        $config['per_page'] = 10;
        $config['page_query_string'] = TRUE;
        $config['total_rows'] = $this->Sekolah_model->total_rows_approve($q);
        $sekolah = $this->Sekolah_model->get_limit_data_approve($config['per_page'], $start, $q);

        $this->load->library('pagination');
        $this->pagination->initialize($config);

        $data = array(
            'tittle'            => 'Sekolah',
            'judul'             => 'Sekolah',
            'title_card'        => 'List Sekolah Approve',
            'tombol_aktif'      => 'approve',
            'sekolah_data'      => $sekolah,
            'q'                 => $q,
            'pagination'        => $this->pagination->create_links(),
            'total_rows'        => $config['total_rows'],
            'start'             => $start,
            'sekolah_aktif'     => 'sekolah',
            'menu_aktif'        => 'sekolah'
        );
        $res['datakonten'] = $this->load->view('sekolah/sekolah_list_approve', $data,true);
        $this->load->view('layouts/main_view', $res);
    }
    
    public function premium()
    {
        $q = urldecode($this->input->get('q', TRUE));
        $start = intval($this->input->get('start'));
        
        if ($q <> '') {
            $config['base_url'] = base_url() . 'sekolah/premium?q=' . urlencode($q);
            $config['first_url'] = base_url() . 'sekolah/premium?q=' . urlencode($q);
        } else {
            $config['base_url'] = base_url() . 'sekolah/premium';
            $config['first_url'] = base_url() . 'sekolah/premium';
        }

        $config['per_page'] = 10;
        $config['page_query_string'] = TRUE;
        $config['total_rows'] = $this->Sekolah_model->total_rows_premium($q);
        $sekolah = $this->Sekolah_model->get_limit_data_premium($config['per_page'], $start, $q);

        $this->load->library('pagination');
        $this->pagination->initialize($config);

        $data = array(
            'tittle'            => 'Sekolah',
            'judul'             => 'Sekolah',
            'title_card'        => 'List Sekolah Premium',
            'tombol_aktif'      => 'premium',
            'sekolah_data'      => $sekolah,
            'q'                 => $q,
            'pagination'        => $this->pagination->create_links(),
            'total_rows'        => $config['total_rows'],
            'start'             => $start,
            'sekolah_aktif'     => 'sekolah',
            'menu_aktif'        => 'sekolah'
        );
        $res['datakonten'] = $this->load->view('sekolah/sekolah_list_premium', $data,true);
        $this->load->view('layouts/main_view', $res);
    }

    public function berkas()
    {
        $q = urldecode($this->input->get('q', TRUE));
        $start = intval($this->input->get('start'));
        
        if ($q <> '') {
            $config['base_url'] = base_url() . 'sekolah/berkas?q=' . urlencode($q);
            $config['first_url'] = base_url() . 'sekolah/berkas?q=' . urlencode($q);
        } else {
            $config['base_url'] = base_url() . 'sekolah/berkas';
            $config['first_url'] = base_url() . 'sekolah/berkas';
        }

        $config['per_page'] = 10;
        $config['page_query_string'] = TRUE;
        $config['total_rows'] = $this->Sekolah_model->total_rows_berkas($q);
        $sekolah = $this->Sekolah_model->get_limit_data_berkas($config['per_page'], $start, $q);

        $this->load->library('pagination');
        $this->pagination->initialize($config);

        $data = array(
            'tittle'            => 'sekolah',
            'judul'             => 'sekolah',
            'title_card'        => 'List sekolah Berkas',
            'tombol_aktif'      => 'berkas',
            'sekolah_data'     => $sekolah,
            'q'                 => $q,
            'pagination'        => $this->pagination->create_links(),
            'total_rows'        => $config['total_rows'],
            'start'             => $start,
            'sekolah_aktif'    => 'sekolah',
            'menu_aktif'        => 'sekolah'
        );
        $res['datakonten'] = $this->load->view('sekolah/sekolah_list_berkas', $data,true);
        $this->load->view('layouts/main_view', $res);
    }

    public function banned()
    {
        $q = urldecode($this->input->get('q', TRUE));
        $start = intval($this->input->get('start'));
        
        if ($q <> '') {
            $config['base_url'] = base_url() . 'sekolah/banned?q=' . urlencode($q);
            $config['first_url'] = base_url() . 'sekolah/banned?q=' . urlencode($q);
        } else {
            $config['base_url'] = base_url() . 'sekolah/banned';
            $config['first_url'] = base_url() . 'sekolah/banned';
        }

        $config['per_page'] = 10;
        $config['page_query_string'] = TRUE;
        $config['total_rows'] = $this->Sekolah_model->total_rows_banned($q);
        $sekolah = $this->Sekolah_model->get_limit_data_banned($config['per_page'], $start, $q);

        $this->load->library('pagination');
        $this->pagination->initialize($config);

        $data = array(
            'tittle'            => 'sekolah',
            'judul'             => 'sekolah',
            'title_card'        => 'List sekolah Banned',
            'tombol_aktif'      => 'banned',
            'sekolah_data'      => $sekolah,
            'q'                 => $q,
            'pagination'        => $this->pagination->create_links(),
            'total_rows'        => $config['total_rows'],
            'start'             => $start,
            'sekolah_aktif'      => 'sekolah',
            'menu_aktif'        => 'sekolah'
        );
        $res['datakonten'] = $this->load->view('sekolah/sekolah_list_banned', $data,true);
        $this->load->view('layouts/main_view', $res);
    }

    public function detail($id) 
    {
        $row = $this->Sekolah_model->get_by_id_detail($id);
           $reason = $this->Sekolah_model->get_reason(1);
        if ($row) {
            $data = array(
            'tittle'                    => 'sekolah',
            'judul'                     => 'sekolah',
            'title_card'                => 'Detail sekolah',
            'id_sekolah'                => $row->id_sekolah,
            'reason'                    => $reason,
            'id_status_bank_sampah'     => $row->id_status_bank_sampah,
            'nama_sekolah'              => $row->nama_sekolah,
            'email_sekolah'             => $row->email_sekolah,
            'nomor_tlp_sekolah'         => $row->nomor_tlp_sekolah,
            'kode_verifikasi_sekolah'   => $row->kode_verifikasi_sekolah,
            'last_login'                => $row->last_login,
            'tgl_daftar'                => $row->tgl_daftar,
            'alamat_sekolah'            => $row->alamat_sekolah,
            'nama_provinsi'             => $row->nama_provinsi,
            'nama_kabupaten'            => $row->nama_kabupaten,
            'nama_kecamatan'            => $row->nama_kecamatan,
            'lat_sekolah'               => $row->lat_sekolah,
            'long_sekolah'              => $row->long_sekolah,
            'saldo_sekolah'             => $row->saldo_sekolah,
            'point_sekolah'             => $row->point_sekolah,
            'sk_sekolah'                => $row->sk_sekolah,
            'foto_bangunan_sekolah'     => $row->foto_bangunan_sekolah,
            'no_ktp_penanggungjawab'    => $row->no_ktp_penanggungjawab,
            'nama_penanggungjawab'      => $row->nama_penanggungjawab,
            'alamat_penanggungjawab'    => $row->alamat_penanggungjawab,
            'no_hp_penanggungjawab'     => $row->no_hp_penanggungjawab,
            'email_penanggungjawab'     => $row->email_penanggungjawab,
            'foto_ktp_penanggungjawab'  => $row->foto_ktp_penanggungjawab,
            'selfie_ktp_penanggungjawab'=> $row->selfie_ktp_penanggungjawab,
            'sekolah_aktif'             => '1',
            'menu_aktif'                => 'sekolah'
            );
            $res['datakonten'] = $this->load->view('sekolah/sekolah_detail', $data,true);
            $this->load->view('layouts/main_view', $res);
        } else {
            $this->session->set_flashdata('gagal', 'Data Tidak Di Temukan');
            redirect(site_url('sekolah'));
        }
    }

    public function blokir($id)
    {
         $row = $this->Sekolah_model->get_by_id_detail($id);
         $reason = $this->Sekolah_model->get_reason(0);
        if ($row) {
        $data = array(
            'tittle'                => 'sekolah',
            'judul'                 => 'sekolah Blokir Detail',
            'id_sekolah'            => $row->id_sekolah,
            'nama_sekolah'          => $row->nama_sekolah,
            'reason'                => $reason,
            'status_bank_sampah'        => $row->ket_status_bank_sampah,
            'email_sekolah'         => $row->email_sekolah,
            'nomor_tlp_sekolah'     => $row->nomor_tlp_sekolah,
            'last_login'            => $row->last_login,
            'tgl_daftar'            => $row->tgl_daftar,
            'alamat_sekolah'        => $row->alamat_sekolah,
            'sekolah_aktif'         => '1',
            'menu_aktif'            => 'sekolah'
        );
        $res['datakonten'] = $this->load->view('sekolah/sekolah_blokir', $data,true);
        $this->load->view('layouts/main_view', $res);
         } else {
            $this->session->set_flashdata('gagal', 'Data Tidak Di Temukan');
            redirect(site_url('sekolah'));
        }
    }

    public function create() 
    {
        $data = array(
            'tittle'                        => 'sekolah',
            'judul'                         => 'Tambah sekolah',
            'button'                        => 'Create',
            'action'                        => site_url('sekolah/create_action'),
            'id_sekolah'                    => set_value('id_sekolah'),
            'nama_sekolah'                  => set_value('nama_sekolah'),
            'email_sekolah'                 => set_value('email_sekolah'),
            'nomor_tlp_sekolah'             => set_value('nomor_tlp_sekolah'),
            'password_sekolah'              => set_value('password_sekolah'),
            'kode_verifikasi_bank_sampah'   => set_value('kode_verifikasi_bank_sampah'),
            'last_login'                    => set_value('last_login'),
            'tgl_join'                      => set_value('tgl_join'),
            'sekolah_aktif'                 => '1',
            'menu_aktif'                    => 'sekolah'
	    );
        $res['datakonten'] = $this->load->view('sekolah/sekolah_form', $data,true);
        $this->load->view('layouts/main_view', $res);

    }
    
    public function create_action() 
    {
        $this->_rules();
        $dt = new DateTime();
        if ($this->form_validation->run() == FALSE) {
            $this->create();
        } else {
            $data = array(
            'nama_sekolah'                  => $this->input->post('nama_sekolah',TRUE),
            'email_sekolah'                 => $this->input->post('email_sekolah',TRUE),
            'nomor_tlp_sekolah'             => $this->input->post('nomor_tlp_sekolah',TRUE),
            'password_sekolah'              => $this->input->post('password_sekolah',TRUE),
            'kode_verifikasi_bank_sampah'   => $this->input->post('kode_verifikasi_bank_sampah',TRUE),
            'last_login'                    => $dt->format('Y-m-d H:i:s'),
            'tgl_join'                      => $dt->format('Y-m-d H:i:s'),
            );

            $this->Sekolah_model->insert($data);
            $this->session->set_flashdata('berhasil', 'Berhasil Menambahkan Data');
            redirect(site_url('sekolah/approve'));
        }
    }
    
    public function update($id) 
    {
        $row = $this->Sekolah_model->get_by_id($id);

        if ($row) {
            $data = array(
                'button'                        => 'Update',
                'action'                        => site_url('sekolah/update_action'),
                'id_sekolah'                   => set_value('id_sekolah', $row->id_sekolah),
                'nama_sekolah'                 => set_value('nama_sekolah', $row->nama_sekolah),
                'email_sekolah'                => set_value('email_sekolah', $row->email_sekolah),
                'nomor_tlp_sekolah'                => set_value('nomor_tlp_sekolah', $row->nomor_tlp_sekolah),
                'password_sekolah'             => set_value('password_sekolah', $row->password_sekolah),
                'kode_verifikasi_bank_sampah'   => set_value('kode_verifikasi_bank_sampah', $row->kode_verifikasi_bank_sampah),
                'last_login'                    => set_value('last_login', $row->last_login),
                'tgl_join'                      => set_value('tgl_join', $row->tgl_join),
                'menu_aktif'                    => 'sekolah'
                );
            $this->load->view('sekolah/sekolah_form', $data);
        } else {
            $this->session->set_flashdata('gagal', 'Data Tidak Ditemukan');
            redirect(site_url('sekolah'));
        }
    }
    
    public function update_action() 
    {
        $this->_rules();
        $dt = new DateTime();
        if ($this->form_validation->run() == FALSE) {
            $this->update($this->input->post('id_sekolah', TRUE));
        } else {
            $data = array(
            'nama_sekolah'                 => $this->input->post('nama_sekolah',TRUE),
            'email_sekolah'                => $this->input->post('email_sekolah',TRUE),
            'nomor_tlp_sekolah'                => $this->input->post('nomor_tlp_sekolah',TRUE),
            'password_sekolah'             => $this->input->post('password_sekolah',TRUE),
            'kode_verifikasi_bank_sampah'   => $this->input->post('kode_verifikasi_bank_sampah',TRUE),
            'last_login'                    => $dt->format('Y-m-d H:i:s'),
            'tgl_join'                      => $dt->format('Y-m-d H:i:s'),
            );

            $this->Sekolah_model->update($this->input->post('id_sekolah', TRUE), $data);
            $this->session->set_flashdata('berhasil', 'Berhasil Merubah Data');
            redirect(site_url('sekolah'));
        }
    }
    
    public function delete($id) 
    {
        $row = $this->Sekolah_model->get_by_id($id);

        if ($row) {
            $this->Sekolah_model->delete($id);
            $this->session->set_flashdata('berhasil', 'Berhasil Menghapus Data');
            redirect(site_url('sekolah'));
        } else {
            $this->session->set_flashdata('gagal', 'Data Tidak Ditemukan');
            redirect(site_url('sekolah'));
        }
    }

    public function _rules() 
    {
	$this->form_validation->set_rules('nama_sekolah', 'nama sekolah', 'trim|required');
	$this->form_validation->set_rules('email_sekolah', 'email sekolah', 'trim|required');
	$this->form_validation->set_rules('nomor_tlp_sekolah', 'no hp sekolah', 'trim|required');
	// $this->form_validation->set_rules('password_sekolah', 'password sekolah', 'trim|required');
	$this->form_validation->set_rules('kode_verifikasi_bank_sampah', 'kode verifikasi bank sampah', 'trim|required');
	// $this->form_validation->set_rules('id_sekolah', 'id_sekolah', 'trim');
	$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }


    
    public function ubahbanned($id) 
    {
        $row = $this->Sekolah_model->get_by_id($id);
         if ($row) {
      
            $data = array(
             
                'id_status_bank_sampah' => '1',
        );
        
        $this->Sekolah_model->update($id, $data);
        $this->session->set_flashdata('berhasil', 'Berhasil Merubah Data');
        redirect(site_url('sekolah/berkas'));
        } else {
        $this->session->set_flashdata('gagal', 'Data Tidak Ditemukan');
        redirect(site_url('sekolah//berkas'));
        }
    }

    public function ubahstatus($id) 
    {
        $row = $this->Sekolah_model->get_by_id($id);
         $reason          = $this->input->get('reason');
      
        if ($row) {
      
            $data = array(
                'id_status_bank_sampah' => '4',
                   'reason_reject' => $reason,
        );
        
        $this->Sekolah_model->update($id, $data);
        $this->session->set_flashdata('berhasil', 'Berhasil Merubah Data');
        redirect(site_url('sekolah/banned'));
        } else {
        $this->session->set_flashdata('gagal', 'Data Tidak Ditemukan');
        redirect(site_url('sekolah/banned'));
        }
    }

    public function ubahapprove($id) 
    {
        $row = $this->Sekolah_model->get_by_id($id);
        
        if ($row) {
            $data = array(
                'id_status_bank_sampah' => '1',
        );
        $this->Sekolah_model->update($id, $data);
        $this->Sekolah_model->insertdetailsekolah($id);
        $this->session->set_flashdata('berhasil', 'Berhasil Merubah Data');
        redirect(site_url('sekolah/approve'));
   } else {
            $this->session->set_flashdata('gagal', 'Data Tidak Ditemukan');
            redirect(site_url('sekolah/approve'));
        }
    }

   public function ubahblokir($id) 
    {
        $row = $this->Sekolah_model->get_by_id($id);
        
        if ($row) {
            $data = array(
                'id_status_bank_sampah' => '1',
        );
        $this->Sekolah_model->update($id, $data);
        $this->session->set_flashdata('berhasil', 'Berhasil Merubah Data');
        redirect(site_url('sekolah/approve'));
   } else {
            $this->session->set_flashdata('gagal', 'Data Tidak Ditemukan');
            redirect(site_url('sekolah/approve'));
        }
    }
    public function ubahpremium($id) 
    {
        $row = $this->Sekolah_model->get_by_id($id);
        
        if ($row) {
            $data = array(
                'id_status_bank_sampah' => '3',
        );
        $this->Sekolah_model->update($id, $data);
        $this->session->set_flashdata('berhasil', 'Berhasil Merubah Data');
        redirect(site_url('sekolah'));
   } else {
            $this->session->set_flashdata('gagal', 'Data Tidak Ditemukan');
            redirect(site_url('sekolah'));
        }
    }


    public function exportxl()
    {
        $q          = $this->input->get('q');
        $subject    = "sekolah_List";
        // $id         = $this->session->userdata('uid');
           $this->db->query('SET @no=0');
        $sql        =
        "SELECT @no:=@no+1 AS nomor,
            sekolah.nama_sekolah,
            sekolah.email_sekolah,
            sekolah.nomor_tlp_sekolah,
            sekolah.kode_verifikasi_sekolah,
            sekolah.last_login,sekolah.tgl_daftar 
        FROM `sekolah` 
        JOIN `status_bank_sampah` 
        ON `sekolah`.`id_status_bank_sampah`=`status_bank_sampah`.`id_status_bank_sampah` 
        WHERE `sekolah`.`id_status_bank_sampah` = 3";
        exportSQL('sekolah_List', $subject, $sql);
    }

    public function exportxlbanned()
    {
        $q          = $this->input->get('q');
        $subject    = "sekolah_Banned";
        // $id         = $this->session->userdata('uid');
           $this->db->query('SET @no=0');
        $sql        =
        "SELECT 
            @no:=@no+1 AS nomor,
            sekolah.nama_sekolah,
            sekolah.email_sekolah,
            sekolah.nomor_tlp_sekolah,
            sekolah.kode_verifikasi_sekolah,
            sekolah.last_login,sekolah.tgl_daftar
        FROM `sekolah` 
        JOIN `status_bank_sampah` ON `sekolah`.`id_status_bank_sampah`=`status_bank_sampah`.`id_status_bank_sampah` 
        WHERE `sekolah`.`id_status_bank_sampah` = 4";
        exportSQL('sekolah_Banned', $subject, $sql);
    }

    public function exportxlberkas()
    {
        $q          = $this->input->get('q');
        $subject    = "sekolah_Berkas";
        // $id         = $this->session->userdata('uid');
           $this->db->query('SET @no=0');
        $sql        ="SELECT 
            @no:=@no+1 AS nomor,
            sekolah.nama_sekolah,
            sekolah.email_sekolah,
            sekolah.nomor_tlp_sekolah,
            sekolah.kode_verifikasi_sekolah,
            sekolah.last_login,sekolah.tgl_daftar
        FROM `sekolah` 
        JOIN `status_bank_sampah` ON `sekolah`.`id_status_bank_sampah`=`status_bank_sampah`.`id_status_bank_sampah` 
        WHERE `sekolah`.`id_status_bank_sampah` = 1";
        exportSQL('sekolah_Berkas', $subject, $sql);
    }

    public function exportxlapprove()
    {
        $q          = $this->input->get('q');
        $subject    = "sekolah_Approve";
        // $id         = $this->session->userdata('uid');
           $this->db->query('SET @no=0');
        $sql        ="SELECT 
            @no:=@no+1 AS nomor,
            sekolah.nama_sekolah,
            sekolah.email_sekolah,
            sekolah.nomor_tlp_sekolah,
            sekolah.kode_verifikasi_sekolah,
            sekolah.last_login,sekolah.tgl_daftar 
        FROM `sekolah` 
        JOIN `status_bank_sampah` ON `sekolah`.`id_status_bank_sampah`=`status_bank_sampah`.`id_status_bank_sampah` 
        WHERE `sekolah`.`id_status_bank_sampah` = 0";
        exportSQL('sekolah_Approve', $subject, $sql);
    }

    public function exportxlpremium()
    {
        $q          = $this->input->get('q');
        $subject    = "sekolah_Premium";
        // $id         = $this->session->userdata('uid');
           $this->db->query('SET @no=0');
        $sql        ="SELECT 
            @no:=@no+1 AS nomor,
            sekolah.nama_sekolah,
            sekolah.email_sekolah,
            sekolah.nomor_tlp_sekolah,
            sekolah.kode_verifikasi_sekolah,
            sekolah.last_login,sekolah.tgl_daftar
        FROM `sekolah` 
        JOIN `status_bank_sampah` ON `sekolah`.`id_status_bank_sampah`=`status_bank_sampah`.`id_status_bank_sampah` 
        WHERE `sekolah`.`id_status_bank_sampah` = 2";
        exportSQL('sekolah_Premium', $subject, $sql);
    }

  public function tolak_berkas()
        {
           $id=$this->input->post('id_sekolah');
           $reason=$this->input->post('reason');

             $data = array(
            'id_status_bank_sampah'     => 1,
            'reason_reject'             => $reason,
        );
    $this->Sekolah_model->update($id, $data);
            # code...
    redirect('sekolah/detail/'.$id);
        }

    function get_image(){
        $img=$this->input->get('img');
        $cek='https://kompis-sekolah.s3.ap-southeast-1.amazonaws.com/'.$img;
        $row = $this->Sekolah_model->get_by_poto($cek);
        if($row){
            echo getkontenall($img,'sekolah');  
        } else {
            $this->session->set_flashdata('gagal', 'Gambar Tidak Ditemukan');
            redirect('sekolah');
        }
    }

         function get_sk()
    {

        $img = $this->input->get('img');
        // echo getkonten($img);
        $cek = 'https://kompis-sekolah.s3.ap-southeast-1.amazonaws.com/' . $img;
        $row = $this->Sekolah_model->get_by_sk($cek);
        if ($row) {
            echo getkontenall($img,'sekolah');
        } else {
            $this->session->set_flashdata('gagal', 'Gambar Tidak Ditemukan');
            redirect('sekolah');
        }
    }
      function get_bangunan()
    {

        $img = $this->input->get('img');
        // echo getkonten($img);
        $cek = 'https://kompis-sekolah.s3.ap-southeast-1.amazonaws.com/' . $img;
         $row = $this->Sekolah_model->get_by_bangunan($cek);
        if ($row) {
            echo getkontenall($img,'sekolah');
        } else {
            $this->session->set_flashdata('gagal', 'Gambar Tidak Ditemukan');
            redirect('sekolah');
        }
    }
    function get_image_selfi()
    {

        $img = $this->input->get('img');
        // echo getkonten($img);
        $cek = 'https://kompis-sekolah.s3.ap-southeast-1.amazonaws.com/' . $img;
        $row = $this->Sekolah_model->get_by_poto_selfi($cek);
        if ($row) {
            echo getkontenall($img,'sekolah');
        } else {
            $this->session->set_flashdata('gagal', 'Gambar Tidak Ditemukan');
            redirect('sekolah');
        }
    }
}

/* End of file sekolah.php */
/* Location: ./application/controllers/sekolah.php */
