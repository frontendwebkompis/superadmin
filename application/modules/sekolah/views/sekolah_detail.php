<script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>

<div class="row">
  <!-- Left col -->
  <section class="col-lg connectedSortable ui-sortable">
    <!-- Custom tabs (Charts with tabs)-->
    <div class="card card-info">
      <div class="card-header">
        <div class="card-title">
          <?= $title_card; ?>
        </div>
      </div>
      <div class="card-body">
        <div class="tab-content p-0">
          <div class="card card-secondary card-tabs">
            <div class="card-header p-0 pt-1">
              <ul class="nav nav-tabs" id="custom-tabs-one-tab" role="tablist">
                <li class="nav-item">
                  <a class="nav-link active" id="custom-tabs-one-home-tab" data-toggle="pill" href="#custom-tabs-one-home" role="tab" aria-controls="custom-tabs-one-home" aria-selected="true">Detail</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" id="custom-tabs-one-profile-tab" data-toggle="pill" href="#custom-tabs-one-profile" role="tab" aria-controls="custom-tabs-one-profile" aria-selected="false">Penanggung Jawab</a>
                </li>
              </ul>
            </div>

            <div class="card-body">
              <div class="tab-content" id="custom-tabs-one-tabContent">
                <div class="tab-pane fade show active" id="custom-tabs-one-home" role="tabpanel" aria-labelledby="custom-tabs-one-home-tab">

                  <div class="row">
                    <div class="col-md-8">
                      <table class="table table-bordered table-striped table-condensed table-sm" style="margin-bottom: 10px">
                        <tr>
                          <td class="font-weight-bold">Nama sekolah</td>
                          <td><?php
                              if ($nama_sekolah == null) {
                                echo '<span class="badge badge-danger">Kosong</span>';
                              } else {
                                echo $nama_sekolah;
                              }  ?></td>
                        </tr>
                        <tr>
                          <td class="font-weight-bold">Email sekolah</td>
                          <td><?php
                              if ($email_sekolah == null) {
                                echo '<span class="badge badge-danger">Kosong</span>';
                              } else {
                                echo $email_sekolah;
                              } ?></td>
                        </tr>
                        <tr>
                          <td class="font-weight-bold">No Hp sekolah</td>
                          <td><?php
                              if ($nomor_tlp_sekolah == null) {
                                echo '<span class="badge badge-danger">Kosong</span>';
                              } else {
                                echo $nomor_tlp_sekolah;
                              }  ?></td>
                        </tr>
                        <tr>
                          <td class="font-weight-bold">Kode Verifikasi Sekolah</td>
                          <td><?php
                              if ($kode_verifikasi_sekolah == null) {
                                echo '<span class="badge badge-danger">Kosong</span>';
                              } else {
                                echo $kode_verifikasi_sekolah;
                              }  ?></td>
                        </tr>
                        <tr>
                          <td class="font-weight-bold">Last Login</td>
                          <td><?php
                              if ($last_login == null) {
                                echo '<span class="badge badge-danger">Kosong</span>';
                              } else {
                                echo fulldate($last_login);
                              }  ?></td>
                        </tr>
                        <tr>
                          <td class="font-weight-bold">Tanggal Join</td>
                          <td><?php
                              if ($tgl_daftar == null) {
                                echo '<span class="badge badge-danger">Kosong</span>';
                              } else {
                                echo fulldate($tgl_daftar);
                              }  ?></td>
                        </tr>
                        <tr>
                          <td class="font-weight-bold">Alamat sekolah</td>
                          <td><?php
                              if ($alamat_sekolah == null) {
                                echo '<span class="badge badge-danger">Kosong</span>';
                              } else {
                                echo $alamat_sekolah;
                              }  ?></td>
                        </tr>
                        <tr>
                          <td class="font-weight-bold">Provinsi</td>
                          <td><?php
                              if ($nama_provinsi == null) {
                                echo '<span class="badge badge-danger">Kosong</span>';
                              } else {
                                echo $nama_provinsi;
                              }  ?></td>
                        </tr>
                        <tr>
                          <td class="font-weight-bold">Kabupaten</td>
                          <td><?php
                              if ($nama_kabupaten == null) {
                                echo '<span class="badge badge-danger">Kosong</span>';
                              } else {
                                echo $nama_kabupaten;
                              }  ?></td>
                        </tr>
                        <tr>
                          <td class="font-weight-bold">Kecamatan</td>
                          <td><?php
                              if ($nama_kecamatan == null) {
                                echo '<span class="badge badge-danger">Kosong</span>';
                              } else {
                                echo $nama_kecamatan;
                              }  ?></td>
                        </tr>
                        <tr>
                          <td class="font-weight-bold">lat</td>
                          <td><?php
                              if ($lat_sekolah == null) {
                                echo '<span class="badge badge-danger">Kosong</span>';
                              } else {
                                echo $lat_sekolah;
                              }  ?></td>
                        </tr>
                        <tr>
                          <td class="font-weight-bold">long</td>
                          <td><?php
                              if ($long_sekolah == null) {
                                echo '<span class="badge badge-danger">Kosong</span>';
                              } else {
                                echo $long_sekolah;
                              }  ?></td>
                        </tr>

                        <tr>
                          <td class="font-weight-bold">Saldo sekolah</td>
                          <td class="font-weight-bold"><?php
                                                        if ($saldo_sekolah == null) {
                                                          echo '<span class="badge badge-danger">Kosong</span>';
                                                        } else {
                                                          echo $saldo_sekolah;
                                                        }  ?></td>
                        </tr>
                        <tr>
                          <td class="font-weight-bold">Point sekolah</td>
                          <td class="font-weight-bold"><?php
                                                        if ($point_sekolah == null) {
                                                          echo '<span class="badge badge-danger">Kosong</span>';
                                                        } else {
                                                          echo $point_sekolah;
                                                        }  ?></td>
                        </tr>

                      </table>
                    </div>
                    <div class="col-md-4">
                      <ul class="list-group">
                        <li class="list-group-item">
                          <div class="text-center font-weight-bold">
                            SK Sekolah
                          </div>
                          <div class="text-center">
                            <?php
                            if ($sk_sekolah == null) {
                              echo '<span class="badge badge-danger">Kosong</span>';
                            } else { ?>
                              <?php if (substr($sk_sekolah, -4) == '.pdf') { ?>
                                <a href="<?php echo base_url(); ?>sekolah/get_sk?img=profile/berkas/sk_sekolah/<?php echo $id_sekolah . '/' . basename($sk_sekolah); ?>">
                                  <button type="button" class="btn btn-primary btn-sm" style="width: 200px;"><i class="fas fa-download"></i> Download SK</button></a>
                              <?php } else { ?>
                                <a href="<?php echo base_url(); ?>sekolah/get_sk?img=profile/berkas/sk_sekolah/<?php echo $id_sekolah . '/' . basename($sk_sekolah); ?>" data-toggle="lightbox" data-max-width="800" data-title="SK Sekolah">
                                  <img src="<?php echo base_url(); ?>sekolah/get_sk?img=profile/berkas/sk_sekolah/<?php echo $id_sekolah . '/' . basename($sk_sekolah); ?>" class="img-fluid" style="width: 250px;" alt="Sk Sekolah">
                                </a>
                            <?php }
                            }
                            ?>
                          </div>
                        </li>
                        <li class="list-group-item">
                          <div class="text-center font-weight-bold">
                            Foto Bangunan Sekolah
                          </div>
                          <div class="text-center">
                            <?php
                            if ($foto_bangunan_sekolah == null) {
                              echo '<span class="badge badge-danger">Kosong</span>';
                            } else { ?>
                              <a href="<?php echo base_url(); ?>sekolah/get_bangunan?img=profile/berkas/foto_bangunan_sekolah/<?php echo $id_sekolah . '/' . basename($foto_bangunan_sekolah); ?>" data-toggle="lightbox" data-max-width="800" data-title="Foto Bangunan Sekolah">
                                <img style="width:250px; margin-bottom: 5px;" src="<?php echo base_url(); ?>sekolah/get_bangunan?img=profile/berkas/foto_bangunan_sekolah/<?php echo $id_sekolah . '/' . basename($foto_bangunan_sekolah); ?>" class="img-fluid" style="width: 250px;" alt="Foto Bangunan Sekolah">
                              </a>
                            <?php  }
                            ?>
                          </div>
                        </li>
                      </ul>
                    </div>
                  </div>


                </div>
                <div class="tab-pane fade" id="custom-tabs-one-profile" role="tabpanel" aria-labelledby="custom-tabs-one-profile-tab">
                  <div class="row">
                    <div class="col-md-8">
                      <table class="table table-bordered table-striped table-condensed">
                        <tr>
                          <td class="font-weight-bold">Nama Penanggung Jawab</td>
                          <td><?php
                              if ($nama_penanggungjawab == null) {
                                echo '<span class="badge badge-danger">Kosong</span>';
                              } else {
                                echo $nama_penanggungjawab;
                              }  ?></td>
                        </tr>
                        <tr>
                          <td class="font-weight-bold">Email Penanggung Jawab</td>
                          <td><?php
                              if ($email_penanggungjawab == null) {
                                echo '<span class="badge badge-danger">Kosong</span>';
                              } else {
                                echo $email_penanggungjawab;
                              }  ?></td>
                        </tr>
                        <tr>
                          <td class="font-weight-bold">Nomor HP Penanggung Jawab</td>
                          <td><?php
                              if ($no_hp_penanggungjawab == null) {
                                echo '<span class="badge badge-danger">Kosong</span>';
                              } else {
                                echo $no_hp_penanggungjawab;
                              }  ?></td>
                        </tr>
                        <tr>
                          <td class="font-weight-bold">Nomor KTP Penanggung Jawab</td>
                          <td><?php
                              if ($no_ktp_penanggungjawab == null) {
                                echo '<span class="badge badge-danger">Kosong</span>';
                              } else {
                                echo $no_ktp_penanggungjawab;
                              }  ?></td>
                        </tr>
                        <tr>
                          <td class="font-weight-bold">Alamat Penanggung Jawab</td>
                          <td><?php
                              if ($alamat_penanggungjawab == null) {
                                echo '<span class="badge badge-danger">Kosong</span>';
                              } else {
                                echo $alamat_penanggungjawab;
                              }  ?></td>
                        </tr>
                      </table>
                    </div>
                    <div class="col-md-4">
                      <ul class="list-group">
                        <li class="list-group-item">
                          <div class="text-center font-weight-bold">
                            Foto KTP Penanggung Jawab
                          </div>
                          <div class="text-center">
                            <?php
                            if ($foto_ktp_penanggungjawab == null) {
                              echo '<span class="badge badge-danger">Kosong</span>';
                            } else { ?>
                              <a href="<?php echo base_url(); ?>sekolah/get_image?img=profile/penanggungjawab/foto_ktp_penanggungjawab/<?php echo $id_sekolah . '/' . basename($foto_ktp_penanggungjawab); ?>" data-toggle="lightbox" data-max-width="800" data-title="Foto KTP Penanggung Jawab">
                                <img src="<?php echo base_url(); ?>sekolah/get_image?img=profile/penanggungjawab/foto_ktp_penanggungjawab/<?php echo $id_sekolah . '/' . basename($foto_ktp_penanggungjawab); ?>" style="width:250px;" class="img-fluid" style="width: 250px;" alt="Foto KTP Penanggung Jawab">
                              </a>
                            <?php  }
                            ?>
                          </div>
                        </li>
                        <li class="list-group-item">
                          <div class="text-center font-weight-bold">
                            Selfie KTP Penanggung Jawab
                          </div>
                          <div class="text-center">
                            <?php
                            if ($selfie_ktp_penanggungjawab == null) {
                              echo '<span class="badge badge-danger">Kosong</span>';
                            } else { ?>
                              <a href="<?php echo base_url(); ?>sekolah/get_image_selfi?img=profile/penanggungjawab/selfie_ktp_penanggungjawab/<?php echo $id_sekolah . '/' . basename($selfie_ktp_penanggungjawab); ?>" data-toggle="lightbox" data-max-width="800" data-title="Selfie KTP Penanggung Jawab">
                                <img src="<?php echo base_url(); ?>sekolah/get_image_selfi?img=profile/penanggungjawab/selfie_ktp_penanggungjawab/<?php echo $id_sekolah . '/' . basename($selfie_ktp_penanggungjawab); ?>" style="width: 250px;" alt="Selfie KTP Penanggung Jawab">

                              </a>
                            <?php  }
                            ?>
                          </div>
                        </li>
                      </ul>
                    </div>
                  </div>

                </div>
              </div>
            </div>
          </div>
          <div class="btn-group">
            <a href="<?php echo site_url('sekolah/premium') ?>" data-toogle="tooltip" title="Kembali">
              <button type="button" class="btn btn-primary btn-sm"><i class="far fa-arrow-alt-circle-left"> Kembali</i></button></a>
            <?php if ($id_status_bank_sampah != 4) { ?>
              <?php if ($id_status_bank_sampah != 3 && $id_status_bank_sampah != 1) { ?>
                <a href="#" class="btn btn-warning btn-sm" data-toggle="tooltip" data-placement="top" title="Jadikan Premium" onclick="premium('<?= $id_sekolah ?>')">Premium <i class="fas fa-info-circle"></i></a>
                <button type="button" class="btn btn-default btn-sm" data-toggle="modal" data-target="#modal-default">
                  Tolak Berkas
                </button>
              <?php } ?>
              <a href="<?php echo site_url('sekolah/blokir/' . $id_sekolah); ?>" data-toogle="tooltip" title="Putus Mitra">
                <button type="button" class="btn btn-danger btn-sm">Putus Mitra <i class="fas fa-power-off fa-sm"></i></button></a>

            <?php } else { ?>
              <a href="#" class="btn btn-success btn-sm" data-toggle="tooltip" data-placement="top" title="Aktifkan sekolah" onclick="unblokir('<?= $id_sekolah ?>')">Aktifkan <i class="fas fa-check"></i></a>

            <?php } ?>
          </div>


        </div>
      </div><!-- /.card-body -->
    </div>
    <!-- /.card -->
  </section>
  <!-- /.Left col -->
</div>



<div class="modal fade" id="modal-default">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Tolak Berkas</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form method="post" action="<?php echo site_url('sekolah/tolak_berkas') ?>">
        <div class="modal-body">

          <div class="form-group">
            <label for="exampleFormControlSelect1">Alasan Tolak</label>
            <select name="reason" class="form-control">
              <option value="">Pilih Alasan Tolak</option>
              <?php foreach ($reason as $res) : ?>
                <option value="<?php echo $res->id_reason; ?>"><?php echo $res->desc_reason; ?></option>
              <?php endforeach ?>
            </select>
          </div>
          <input type="hidden" name="id_sekolah" value="<?= $id_sekolah ?>">

        </div>
        <div class="modal-footer justify-content-between">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          <button class="btn btn-danger">Tolak Berkas</button>
        </div>
      </form>

    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
<!-- /.modal -->

<!-- Ekko Lightbox -->
<script src="<?= base_url('assets/plugins/ekko-lightbox/ekko-lightbox.min.js') ?>"></script>
<script>
  $(function() {
    $(document).on('click', '[data-toggle="lightbox"]', function(event) {
      event.preventDefault();
      $(this).ekkoLightbox({
        alwaysShowClose: true
      });
    });

    $('.filter-container').filterizr({
      gutterPixels: 3
    });
    $('.btn[data-filter]').on('click', function() {
      $('.btn[data-filter]').removeClass('active');
      $(this).addClass('active');
    });
  })
</script>

<script>
  function confirm(res) {
    Swal.fire({
      title: 'Anda Yakin Mau Blokir sekolah?',
      text: "Akun akan segera di Blokir!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Ya, Blokir!',
      cancelButtonText: 'Batal'
    }).then((result) => {
      if (result.value) {
        Swal.fire(
          'Terblokir!',
          'Akun sudah diblokir.',
          'success'
        )
        window.location = '<?php echo base_url() . 'sekolah/ubahstatus/'; ?>' + res;
      }
    });
  }

  function premium(res) {
    Swal.fire({
      title: 'Anda Yakin mau Premium Akun?',
      text: "Akan segera di Premium kan!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Ya, Aktifkan!',
      cancelButtonText: 'Batal'
    }).then((result) => {
      if (result.value) {
        Swal.fire(
          'Premium!',
          'Akun sudah Premium.',
          'success'
        )
        window.location = '<?php echo base_url() . 'sekolah/ubahpremium/'; ?>' + res;
      }
    });
  }

  function unblokir(res) {
    Swal.fire({
      title: 'Anda Yakin Mau Unblokir sekolah?',
      text: "Akun akan segera di Unblokir!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Ya, Aktifkan!',
      cancelButtonText: 'Batal'
    }).then((result) => {
      if (result.value) {
        Swal.fire(
          'Aktif!',
          'Akun sudah di Unblokir.',
          'success'
        )
        window.location = '<?php echo base_url() . 'sekolah/ubahapprove/'; ?>' + res;
      }
    });
  }
</script>