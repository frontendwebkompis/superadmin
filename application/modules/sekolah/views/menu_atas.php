<div class="col-md-5 mb-2">
    <div class="tab-content p-0" style="overflow:auto">
        <div class="btn-group">
            <?php 
                if($tombol_aktif == 'sekolah'){
                    echo anchor(site_url('sekolah'),'Sekolah', 'class="btn btn-info btn-sm" data-toggle="tooltip" data-placement="top" title="List sekolah Premium"');
                } else {
                    echo anchor(site_url('sekolah'),'Sekolah', 'class="btn btn-default btn-sm" data-toggle="tooltip" data-placement="top" title="List sekolah Premium"');
                }
            ?>
            <?php
                if($tombol_aktif == 'approve'){
                    echo anchor(site_url('sekolah/approve'),'Approve', 'class="btn btn-success btn-sm " data-toggle="tooltip" data-placement="top" title="List Perlu di Approve"'); 
                } else {
                    echo anchor(site_url('sekolah/approve'),'Approve', 'class="btn btn-default btn-sm " data-toggle="tooltip" data-placement="top" title="List Perlu di Approve"'); 
                }
            ?>
            <?php
                if($tombol_aktif == 'berkas'){
                    echo anchor(site_url('sekolah/berkas'),'Berkas', 'class="btn btn-secondary btn-sm " data-toggle="tooltip" data-placement="top" title="List Belum Upload Berkas"'); 
                } else {
                    echo anchor(site_url('sekolah/berkas'),'Berkas', 'class="btn btn-default btn-sm " data-toggle="tooltip" data-placement="top" title="List Belum Upload Berkas"'); 
                }
            ?>
            <?php 
                if($tombol_aktif == 'premium'){
                    echo anchor(site_url('sekolah/premium'),'Premium', 'class="btn btn-warning btn-sm " data-toggle="tooltip" data-placement="top" title="List Perlu di Premium"'); 
                } else {
                    echo anchor(site_url('sekolah/premium'),'Premium', 'class="btn btn-default btn-sm " data-toggle="tooltip" data-placement="top" title="List Perlu di Premium"'); 
                }
            ?>
            <?php 
                if($tombol_aktif == 'banned'){
                    echo anchor(site_url('sekolah/banned'),'Putus Mitra', 'class="btn btn-danger btn-sm " data-toggle="tooltip" data-placement="top" title="List Putus Mitra"'); 
                } else {
                    echo anchor(site_url('sekolah/banned'),'Putus Mitra', 'class="btn btn-default btn-sm " data-toggle="tooltip" data-placement="top" title="List Putus Mitra"'); 
                }
            ?>
        </div>
    </div>
</div>
