<script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>

<div class="row">
	<!-- Left col -->
	<section class="col-lg connectedSortable ui-sortable">
		<!-- Custom tabs (Charts with tabs)-->
		<div class="card">
			<div class="card-body">
				<div class="tab-content p-0">
					
					<table class="table table-bordered table-striped table-condensed" style="margin-bottom: 20px">
						<tr>	
							<td>Status sekolah</td>
							<td><?php echo $status_bank_sampah; ?></td>
						</tr>
						<tr>
                            <td>Nama sekolah</td>
                            <td><?php echo $nama_sekolah; ?></td>
						</tr>
						<tr>
                            <td>Alamat sekolah</td>
                            <td><?php echo $alamat_sekolah; ?></td>
						</tr>
                        <tr>
                            <td>Email sekolah</td>
                            <td><?php echo $email_sekolah; ?></td>
						</tr>
                        <tr>
                            <td>No Hp sekolah</td>
                            <td><?php echo $nomor_tlp_sekolah; ?></td>
						</tr>
						<tr>
                            <td>Last Login</td>
                            <td><?php echo $last_login; ?></td>
						</tr>
					</table>
                    <div class="form-group">
                            <label for="exampleFormControlSelect1">Alasan Putus Mitra</label>
                            <select name="reason" id="reason" class="form-control">
                            <option value="">Pilih Alasan Putus Mitra</option>
                             <?php foreach ($reason as $res): ?>
                                 <option value="<?php echo $res->id_reason; ?>"><?php echo $res->desc_reason; ?></option>
                            <?php endforeach ?>
                            </select>
                        </div>
					<div class="btn-group">
						<a href="<?php echo site_url('sekolah') ?>" class="btn btn-primary btn-sm">Batal</a></td>
                        <a href="#" id="blokir" class="btn btn-danger btn-sm" data-toggle="tooltip" data-placement="top" title="Non Aktif" 
                        onclick="confirm('<?=  $id_sekolah ?>')" >Putus Mitra</a>
					</div>

				</div>
			</div><!-- /.card-body -->
		</div>
		<!-- /.card -->
	</section>
	<!-- /.Left col -->
</div>



<script>
    $("#reason"). change(function(){
var cek = $(this). children("option:selected"). val();
// alert(cek);
if(cek>0){
    $('#blokir').css('display','block');
}else{
$('#blokir').css('display','none');
}
});
function confirm(res) {
    Swal.fire({
    title: 'Anda Yakin Mau Putus Mitra Sekolah?',
    text: "Akun akan segera di Putus Mitra!",
    icon: 'warning',
    showCancelButton: true,
    confirmButtonColor: '#3085d6',
    cancelButtonColor: '#d33',
    confirmButtonText: 'Ya, Putuskan!',
    cancelButtonText: 'Batal'
    }).then((result) => {
    if (result.value) {
        Swal.fire(
        'Terputus!',
        'Akun sudah diputuskan mitra.',
        'success'
        )
        window.location='<?php echo base_url().'sekolah/ubahstatus/'; ?>'+res+'?reason='+$('#reason').val();
    }
    });
}

function premium(res) {
    Swal.fire({
    title: 'Anda Yakin mau Premium Akun?',
    text: "Akan segera di Premium kan!",
    icon: 'warning',
    showCancelButton: true,
    confirmButtonColor: '#3085d6',
    cancelButtonColor: '#d33',
    confirmButtonText: 'Ya, Aktifkan!',
    cancelButtonText: 'Batal'
    }).then((result) => {
    if (result.value) {
        Swal.fire(
        'Premium!',
        'Akun sudah Premium.',
        'success'
        )
        window.location='<?php echo base_url().'sekolah/ubahpremium/'; ?>'+res;
    }
    });
}

function unblokir(res) {
    Swal.fire({
    title: 'Anda Yakin Mau Unblokir sekolah?',
    text: "Akun akan segera di Unblokir!",
    icon: 'warning',
    showCancelButton: true,
    confirmButtonColor: '#3085d6',
    cancelButtonColor: '#d33',
    confirmButtonText: 'Ya, Aktifkan!',
    cancelButtonText: 'Batal'
    }).then((result) => {
    if (result.value) {
        Swal.fire(
        'Aktif!',
        'Akun sudah di Unblokir.',
        'success'
        )
        window.location='<?php echo base_url().'sekolah/ubahblokir/'; ?>'+res;
    }
    });
}
</script>