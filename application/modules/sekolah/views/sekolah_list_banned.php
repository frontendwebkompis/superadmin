<script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>

<div class="row">
    <section class="col-lg connectedSortable ui-sortable">
        <div class="card card-info">
            <div class="card-header">
                <div class="card-title">
                    <?php echo $title_card ?>
                </div>
            </div>
            <div class="card-body">
                <div class="tab-content p-0">

                    <div class="row">
                        <?php $this->load->view('sekolah/menu_atas'); ?>
                        <div class="col-md-3 offset-md-4 mb-2">
                            <?= search(site_url('sekolah/banned'), site_url('sekolah/banned'), $q) ?>
                        </div>
                    </div>
                    <div class="tab-content p-0" style="overflow:auto">
                        <table class="table table-bordered table-striped table-condensed table-hover table-sm">
                            <tr>
                                <th class="text-center" width="40px">No</th>
                                <th class="text-center">Nama</th>
                                <th class="text-center">Email</th>
                                <th class="text-center">No Hp</th>
                                <th class="text-center">Alasan Banned</th>
                                <th class="text-center">Last Login</th>
                                <th class="text-center">Tanggal Join</th>
                                <th class="text-center" width="100px">Aksi</th>
                            </tr>
                            <?php
                            if ($total_rows == 0) {
                                echo '<tr><td colspan="11" bgcolor="grey" style="color: white; font-weight: bold; text-align: center;">Data Tidak Ditemukan</td></tr>';
                            } else {
                                foreach ($sekolah_data as $sekolah) { ?>
                                    <tr>
                                        <td class="text-center"><?php echo ++$start ?></td>
                                        <td class="text-center"><?php echo $sekolah->nama_sekolah ?></td>
                                        <td class="text-center"><?php echo $sekolah->email_sekolah ?></td>
                                        <td class="text-center"><?php echo $sekolah->nomor_tlp_sekolah ?></td>
                                        <td class="text-center"><?php echo $sekolah->kode_verifikasi_sekolah ?></td>
                                        <td class="text-center"><?php echo fulldate($sekolah->last_login) ?></td>
                                        <td class="text-center"><?php echo fulldate($sekolah->tgl_daftar) ?></td>
                                        <td class="text-center">
                                            <?php if ($sekolah->ket_status_bank_sampah == '0') {
                                                echo '<span class="badge badge-danger">Non Aktif</span>';
                                            } else {
                                                echo '<span class="badge badge-secondary">Belum Upload Berkas</span>';
                                            }
                                            ?>
                                        </td>
                                        <td class="text-center">
                                            <div class="btn-group">
                                                <a href="<?php echo site_url('sekolah/detail/' . $sekolah->id_sekolah); ?>" data-toogle="tooltip" title="Lihat Detail">
                                                    <button type="button" class="btn btn-info btn-sm"><i class="fas fa-info-circle"></i></button></a>
                                                <a href="#" class="btn btn-success btn-sm" data-toggle="tooltip" data-placement="top" title="Aktifkan sekolah" onclick="confirm('<?= $sekolah->id_sekolah ?>')"><i class="fas fa-power-off"></i></a>
                                            </div>
                                        </td>
                                    </tr>
                            <?php }
                            } ?>
                        </table>
                    </div>
                    <?= footer($total_rows, $pagination, site_url('sekolah/exportxlbanned?q=' . $q)) ?>
                </div>
            </div>
    </section>
</div>

<script>
    function confirm(res) {
        Swal.fire({
            title: 'Anda Yakin Mau Unblokir sekolah?',
            text: "Akun akan segera di Unblokir!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Ya, Aktifkan!',
            cancelButtonText: 'Batal'
        }).then((result) => {
            if (result.value) {
                Swal.fire(
                    'Aktif!',
                    'Akun sudah di Unblokir.',
                    'success'
                )
                window.location = '<?php echo base_url() . 'sekolah/ubahbanned/'; ?>' + res;
            }
        });
    }
</script>