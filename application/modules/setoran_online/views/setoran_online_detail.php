<!-----------------------------------Konten-------------------------------------------------- -->
<div class="row">

    <!-- Left col -->
    <section class="col-lg connectedSortable ui-sortable">
        <!-- Custom tabs (Charts with tabs)-->
        <div class="card card-info">
            <div class="card-header">
                <div class="card-title">
                    <?php echo $title_card ?>
                </div>
            </div>

            <div class="card-body">
                <table class="table table-bordered table-striped table-condensed" style="margin-bottom: 20px">
                    <tr>
                        <td width="300px"> Nasabah Online</td>
                        <td><?php echo $nama_nasabah_online; ?></td>
                    </tr>
                    <tr>
                        <td width="300px"> Bank Sampah</td>
                        <td><?php echo $nama_bank_sampah; ?></td>
                    </tr>
                    <tr>
                        <td width="300px"> Status Setoran</td>
                        <td><?php echo $nama_status_setoran; ?></td>
                    </tr>
                    <tr>
                        <td width="300px"> Jenis Transaksi</td>
                        <td><?php echo $nama_jenis_transaksi; ?></td>
                    </tr>
                    <tr>
                        <td width="300px">Tanggal Setoran</td>
                        <td><?php echo fulldate($tgl_setoran); ?></td>
                    </tr>
                    <tr>
                        <td width="300px">Reason</td>
                        <td><?php echo $desc_reason; ?></td>
                    </tr>
                    <tr>
                        <td width="300px">Nilai Nasabah Online</td>
                        <td><?php echo $nilai_nasabah_online; ?></td>
                    </tr>
                    <tr>
                        <td width="300px">Nilai Bank Sampah</td>
                        <td><?php echo $nilai_bank_sampah; ?></td>
                    </tr>
                    <tr>
                        <td width="300px">Ulasan Bank Sampah</td>
                        <td><?php echo $ulasan_bank_sampah; ?></td>
                    </tr>
                    <tr>
                        <td width="300px">Ulasan Nasabah</td>
                        <td><?php echo $ulasan_nasabah; ?></td>
                    </tr>
                    <tr>
                        <td width="300px">Tanggal Review</td>
                        <td><?php echo fulldate($tgl_review); ?></td>
                    </tr>


                </table>
                <div>
                    <a href="<?php echo site_url('setoran_online') ?>" class="btn btn-primary btn-sm">Batal</a>
                </div>

                <div class="card-body">
                    <table class="table table-bordered table-striped table-condensed" style="margin-bottom: 20px">
                        <tr>
                            <th style="text-align:center" width="20px">No</th>
                            <th style="text-align:center" width="100px">Kategori Sampah</th>
                            <th style="text-align:center">Jenis Sampah</th>
                            <th style="text-align:center">Merek Sampah</th>
                            <th style="text-align:center">Berat Setoran</th>
                            <th style="text-align:center">Harga Setoran</th>

                        </tr>
                        <?php foreach ($setoran_online_data as $data) { ?>
                            <tr>
                                <td style="text-align:center"><?php echo ++$start ?></td>
                                <td><?php echo $data->nama_kategori_sampah ?></td>
                                <td style="text-align:center" width="150px"><?php echo $data->nama_jenis_sampah ?></td>
                                <td style="text-align:center" width="150px"><?php echo $data->nama_merk ?></td>
                                <td style="text-align:center" width="150px"><?php echo $data->berat_setoran ?></td>
                                <td style="text-align:center" width="150px"><?php echo rupiah($data->harga_setoran) ?></td>

                            </tr>
                        <?php } ?>
                    </table>
                </div>
              
                      <?= footer($total_rows, $pagination, site_url('setoran_online/detailexportxl?q=' . $q)) ?>


            </div>
        </div><!-- /.card-body -->
</div>
<!-- /.card -->

</section>
<!-- /.Left col -->
</div>