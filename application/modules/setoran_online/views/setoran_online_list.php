<div class="row">
    <section class="col-lg connectedSortable ui-sortable">
        <div class="card card-info">
            <div class="card-header">
                <div class="card-title">
                    <?php echo $title_card ?>
                </div>
            </div>
            <div class="card-body">
                <div class="tab-content p-0">

                    <div class="row">
                        <div class="col-md-5 mb-2">
                            <?= filterDate($this->input->get('awal'), $this->input->get('akhir'), base_url('setoran_online')); ?>
                        </div>
                        <div class="col-md-3 offset-md-4 mb-2">
                            <?= search(site_url('setoran_online/index'), site_url('setoran_online'), $q) ?>
                        </div>
                    </div>
                    <div class="tab-content p-0" style="overflow:auto">
                        <table class="table table-bordered table-striped table-condensed table-hover table-sm" style="margin-bottom: 10px">

                            <tr>
                                <th class="text-center" width="60px">No</th>
                                <th class="text-center">Nasabah Online</th>
                                <th class="text-center">Bank Sampah</th>
                                <th class="text-center"> Status Setoran</th>
                                <th class="text-center"> Jenis Transaksi</th>
                                <th class="text-center">Tanggal Setoran</th>
                                <th class="text-center">Aksi</th>
                            </tr>
                            <?php

                            if ($total_rows == 0) {
                                echo '<tr><td colspan="8" bgcolor="grey" style="color: white; font-weight: bold; text-align: center;">Data Tidak Ditemukan</td></tr>';
                            } else {
                                foreach ($setoran_online_data as $setoran_online) {
                            ?>
                                    <tr>
                                        <td class="text-center"><?php echo ++$start ?></td>
                                        <td><?php echo $setoran_online->nama_nasabah_online ?></td>
                                        <td><?php echo $setoran_online->nama_bank_sampah ?></td>
                                        <td class="text-center"><?php echo $setoran_online->nama_status_setoran ?></td>
                                        <td><?php echo $setoran_online->nama_jenis_transaksi ?></td>
                                        <td class="text-center"><?php echo fulldate($setoran_online->tgl_setoran) ?></td>
                                        <td class="text-center" width="100px">
                                            <div class="btn-group">
                                                <a href="<?php echo site_url('setoran_online/detail/' . $setoran_online->id_setoran_online); ?>" data-toogle="tooltip" title="Lihat">
                                                    <button type="button" class="btn btn-info btn-sm"><i class="fas fa-info-circle fa-sm"></i></button></a>
                                            </div>
                                        </td>
                                    </tr>
                            <?php
                                }
                            }
                            ?>
                        </table>
                    </div>
                    <?= footer($total_rows, $pagination, site_url('setoran_online/exportxl?q=' . $q)) ?>
                </div>
            </div>
        </div>
    </section>
</div>
</div>
<script type="text/javascript">
    setDateFilter('akhir', 'awal');
</script>