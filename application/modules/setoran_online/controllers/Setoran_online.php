<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Setoran_online extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Setoran_online_model');
        $this->load->library('form_validation');
    }

    public function index()
    {
        $q = urldecode($this->input->get('q', TRUE));
        $start = intval($this->input->get('start'));
        $awal   = $this->input->get('awal') ? $this->input->get('awal') : date('y').'-01-01';
        $akhir  = $this->input->get('akhir') ? $this->input->get('akhir') : date('y-m-d');
   
        if ($q <> '') {
            $config['base_url'] = base_url() . 'setoran_online?q=' . urlencode($q);
            $config['first_url'] = base_url() . 'setoran_online?q=' . urlencode($q);
        } else {
            $config['base_url'] = base_url() . 'setoran_online';
            $config['first_url'] = base_url() . 'setoran_online';
        }

        $config['per_page'] = 10;
        $config['page_query_string'] = TRUE;
        $config['total_rows'] = $this->Setoran_online_model->total_rows($q,$awal,$akhir);
        $setoran_online = $this->Setoran_online_model->get_limit_data($config['per_page'], $start, $q,$awal,$akhir);

        $this->load->library('pagination');
        $this->pagination->initialize($config);

        $data = array(
            'tittle'                => 'Setoran Online',
            'judul'                 => 'Setoran Online',
            'title_card'            => 'List Setoran Online',
            'setoran_online_data'   => $setoran_online,
            'q'                     => $q,
            'pagination'            => $this->pagination->create_links(),
            'total_rows'            => $config['total_rows'],
            'start'                 => $start,
            'setoran_online-aktif'  => '1',
            'menu_aktif'            => 'setoran_online'
        );
        $res['datakonten'] = $this->load->view('setoran_online/setoran_online_list', $data, true);
        $this->load->view('layouts/main_view', $res);
    }

    public function detail($id)
    {
        $row = $this->Setoran_online_model->get_by_id($id);
        if ($row) {
            $q = urldecode($this->input->get('q', TRUE));
            $start = intval($this->input->get('start'));

            if ($q <> '') {
                $config['base_url'] = base_url() . 'setoran_online/detail/' . $id . '?q=' . urlencode($q);
                $config['first_url'] = base_url() . 'setoran_online/detail/' . $id . '?q=' . urlencode($q);
            } else {
                $config['base_url'] = base_url() . 'setoran_online/detail/' . $id;
                $config['first_url'] = base_url() . 'setoran_online/detail/' . $id;
            }

            $config['per_page'] = 10;
            $config['page_query_string'] = TRUE;
            $config['total_rows'] = $this->Setoran_online_model->total_rows_detail($q, $id);
            $setoran_online = $this->Setoran_online_model->get_limit_data_detail($config['per_page'], $start, $q, $id);

            $this->load->library('pagination');
            $this->pagination->initialize($config);

            $data = array(
                'tittle'                => 'Setoran Online',
                'judul'                 => 'Setoran Online',
                'title_card'            => 'Detail Setoran Online',
                'id_setoran_online'     => $row->id_setoran_online,
                'nama_nasabah_online'   => $row->nama_nasabah_online,
                'nama_bank_sampah'      => $row->nama_bank_sampah,
                'nama_status_setoran'   => $row->nama_status_setoran,
                'nama_jenis_transaksi'  => $row->nama_jenis_transaksi,
                'tgl_setoran'           => $row->tgl_setoran,
                'id_reason'             => $row->id_reason,
                'desc_reason'           => $row->desc_reason,
                'nilai_nasabah_online'  => $row->nilai_nasabah_online,
                'nilai_bank_sampah'     => $row->nilai_bank_sampah,
                'ulasan_bank_sampah'    => $row->ulasan_bank_sampah,
                'ulasan_nasabah'        => $row->ulasan_nasabah,
                'tgl_review'            => $row->tgl_review,
                'setoran_online_data'   => $setoran_online,
                'q'                     => $q,
                'pagination'            => $this->pagination->create_links(),
                'total_rows'            => $config['total_rows'],
                'start'                 => $start,
                'setoran_online-aktif'  => '1',
                'menu_aktif'            => 'setoran_online'
            );

            $res['datakonten'] = $this->load->view('setoran_online/setoran_online_detail', $data, true);
            $this->load->view('layouts/main_view', $res);
        } else {
            $this->session->set_flashdata('gagal', 'Data Tidak Di Temukan');
            redirect(site_url('setoran_online'));
        }
    }

    public function create()
    {
        $data = array(
            'tittle'                => 'Setoran Online',
            'judul'                 => 'List Setoran Online',
            'button'                => 'Create',
            'action'                => site_url('setoran_online/create_action'),
            'id_setoran_online'     => set_value('id_setoran_online'),
            'id_nasabah_online'     => set_value('id_nasabah_online'),
            'id_bank_sampah'        => set_value('id_bank_sampah'),
            'id_status_setoran'     => set_value('id_status_setoran'),
            'id_jenis_transaksi'    => set_value('id_jenis_transaksi'),
            'tgl_setoran'           => set_value('tgl_setoran'),
            'id_reason'             => set_value('id_reason'),
            'nilai_nasabah_online'  => set_value('nilai_nasabah_online'),
            'nilai_bank_sampah'     => set_value('nilai_bank_sampah'),
            'ulasan_bank_sampah'    => set_value('ulasan_bank_sampah'),
            'ulasan_nasabah'        => set_value('ulasan_nasabah'),
            'tgl_review'            => set_value('tgl_review'),
            'menu_aktif'            => 'setoran_online'
        );
        $res['datakonten'] = $this->load->view('setoran_online/setoran_online_form', $data, true);
        $this->load->view('layouts/main_view', $res);
    }

    public function create_action()
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->create();
        } else {
            $data = array(
                'id_nasabah_online' => $this->input->post('id_nasabah_online', TRUE),
                'id_bank_sampah' => $this->input->post('id_bank_sampah', TRUE),
                'id_status_setoran' => $this->input->post('id_status_setoran', TRUE),
                'id_jenis_transaksi' => $this->input->post('id_jenis_transaksi', TRUE),
                'tgl_setoran' => $this->input->post('tgl_setoran', TRUE),
                'id_reason' => $this->input->post('id_reason', TRUE),
                'nilai_nasabah_online' => $this->input->post('nilai_nasabah_online', TRUE),
                'nilai_bank_sampah' => $this->input->post('nilai_bank_sampah', TRUE),
                'ulasan_bank_sampah' => $this->input->post('ulasan_bank_sampah', TRUE),
                'ulasan_nasabah' => $this->input->post('ulasan_nasabah', TRUE),
                'tgl_review' => $this->input->post('tgl_review', TRUE),
            );

            $this->Setoran_online_model->insert($data);
            $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">Berhasil Menambahkan Data </div>');
            redirect(site_url('setoran_online'));
        }
    }

    public function update($id)
    {
        $row = $this->Setoran_online_model->get_by_id($id);

        if ($row) {
            $data = array(
                'tittle'    => 'Setoran Online',
                'judul'     => 'List Setoran Online',
                'button' => 'Update',
                'action' => site_url('setoran_online/update_action'),
                'id_setoran_online' => set_value('id_setoran_online', $row->id_setoran_online),
                'id_nasabah_online' => set_value('id_nasabah_online', $row->id_nasabah_online),
                'id_bank_sampah' => set_value('id_bank_sampah', $row->id_bank_sampah),
                'id_status_setoran' => set_value('id_status_setoran', $row->id_status_setoran),
                'id_jenis_transaksi' => set_value('id_jenis_transaksi', $row->id_jenis_transaksi),
                'tgl_setoran' => set_value('tgl_setoran', $row->tgl_setoran),
                'id_reason' => set_value('id_reason', $row->id_reason),
                'nilai_nasabah_online' => set_value('nilai_nasabah_online', $row->nilai_nasabah_online),
                'nilai_bank_sampah' => set_value('nilai_bank_sampah', $row->nilai_bank_sampah),
                'ulasan_bank_sampah' => set_value('ulasan_bank_sampah', $row->ulasan_bank_sampah),
                'ulasan_nasabah' => set_value('ulasan_nasabah', $row->ulasan_nasabah),
                'tgl_review' => set_value('tgl_review', $row->tgl_review),
                'menu_aktif'            => 'setoran_online'
            );
            $this->load->view('setoran_online/setoran_online_form', $data);
        } else {
            $this->session->set_flashdata('message', '<div class="alert alert-warning" role="alert">Data Tidak Ditemukan</div>');
            redirect(site_url('setoran_online'));
        }
    }

    public function update_action()
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->update($this->input->post('id_setoran_online', TRUE));
        } else {
            $data = array(
                'id_nasabah_online' => $this->input->post('id_nasabah_online', TRUE),
                'id_bank_sampah' => $this->input->post('id_bank_sampah', TRUE),
                'id_status_setoran' => $this->input->post('id_status_setoran', TRUE),
                'id_jenis_transaksi' => $this->input->post('id_jenis_transaksi', TRUE),
                'tgl_setoran' => $this->input->post('tgl_setoran', TRUE),
                'id_reason' => $this->input->post('id_reason', TRUE),
                'nilai_nasabah_online' => $this->input->post('nilai_nasabah_online', TRUE),
                'nilai_bank_sampah' => $this->input->post('nilai_bank_sampah', TRUE),
                'ulasan_bank_sampah' => $this->input->post('ulasan_bank_sampah', TRUE),
                'ulasan_nasabah' => $this->input->post('ulasan_nasabah', TRUE),
                'tgl_review' => $this->input->post('tgl_review', TRUE),
            );

            $this->Setoran_online_model->update($this->input->post('id_setoran_online', TRUE), $data);
            $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">Berhasil Merubah Data</div>');
            redirect(site_url('setoran_online'));
        }
    }

    public function delete($id)
    {
        $row = $this->Setoran_online_model->get_by_id($id);

        if ($row) {
            $this->Setoran_online_model->delete($id);
            $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">Berhasil Menghapus Data</div>');
            redirect(site_url('setoran_online'));
        } else {
            $this->session->set_flashdata('message', '<div class="alert alert-warning" role="alert">Data Tidak Ditemukan</div>');
            redirect(site_url('setoran_online'));
        }
    }

    public function _rules()
    {
        $this->form_validation->set_rules('id_nasabah_online', 'id nasabah online', 'trim|required');
        $this->form_validation->set_rules('id_bank_sampah', 'id bank sampah', 'trim|required');
        $this->form_validation->set_rules('id_status_setoran', 'id status setoran', 'trim|required');
        $this->form_validation->set_rules('id_jenis_transaksi', 'id jenis transaksi', 'trim|required');
        $this->form_validation->set_rules('tgl_setoran', 'tgl setoran', 'trim|required');
        $this->form_validation->set_rules('id_reason', 'id reason', 'trim|required');
        $this->form_validation->set_rules('nilai_nasabah_online', 'nilai nasabah online', 'trim|required');
        $this->form_validation->set_rules('nilai_bank_sampah', 'nilai bank sampah', 'trim|required');
        $this->form_validation->set_rules('ulasan_bank_sampah', 'ulasan bank sampah', 'trim|required');
        $this->form_validation->set_rules('ulasan_nasabah', 'ulasan nasabah', 'trim|required');
        $this->form_validation->set_rules('tgl_review', 'tgl review', 'trim|required');

        $this->form_validation->set_rules('id_setoran_online', 'id_setoran_online', 'trim');
        $this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

    public function exportxl()
    {
        $q          = $this->input->get('q');
        $subject    = "Setoran_online";
        // $id         = $this->session->userdata('uid');
        $this->db->query('SET @no=0');
        $sql        =
            "SELECT @no:=@no+1 AS nomor,nama_nasabah_online,nama_bank_sampah,nama_status_setoran,nama_jenis_transaksi,tgl_setoran
              FROM `setoran_online` LEFT JOIN `bank_sampah` ON `bank_sampah`.`id_bank_sampah` = `setoran_online`.`id_bank_sampah` LEFT JOIN `nasabah_online` ON `nasabah_online`.`id_nasabah_online` = `setoran_online`.`id_nasabah_online` LEFT JOIN `status_setoran` ON `status_setoran`.`id_status_setoran` = `setoran_online`.`id_status_setoran` LEFT JOIN `jenis_transaksi` ON `jenis_transaksi`.`id_jenis_transaksi` = `setoran_online`.`id_jenis_transaksi` ORDER BY `tgl_setoran` desc";
        exportSQL('Setoran_online', $subject, $sql);
    }
}

/* End of file Setoran_online.php */
/* Location: ./application/controllers/Setoran_online.php */
/* Please DO NOT modify this information : */
/* Generated by Harviacode Codeigniter CRUD Generator 2020-03-19 07:28:31 */
/* http://harviacode.com */
