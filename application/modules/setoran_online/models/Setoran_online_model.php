<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Setoran_online_model extends CI_Model
{

    public $table = 'setoran_online';
    public $id = 'id_setoran_online';
    public $order = 'DESC';

    function __construct()
    {
        parent::__construct();
    }

    // get all
    function get_all()
    {
        $this->db->order_by($this->id, $this->order);
        return $this->db->get($this->table)->result();
    }

    // get data by id
    function get_by_id($id)
    {
        $this->db->join('bank_sampah', 'bank_sampah.id_bank_sampah = setoran_online.id_bank_sampah', 'left');
        $this->db->join('nasabah_online', 'nasabah_online.id_nasabah_online = setoran_online.id_nasabah_online', 'left');
        $this->db->join('status_setoran', 'status_setoran.id_status_setoran = setoran_online.id_status_setoran', 'left');
        $this->db->join('jenis_transaksi', 'jenis_transaksi.id_jenis_transaksi = setoran_online.id_jenis_transaksi', 'left');
        $this->db->join('reason', 'reason.id_reason = setoran_online.id_reason', 'left');

        $this->db->where($this->id, $id);
        return $this->db->get($this->table)->row();
    }

    // get total rows
    function total_rows($q = NULL,$awal,$akhir)
    {
        $this->db->join('bank_sampah', 'bank_sampah.id_bank_sampah = setoran_online.id_bank_sampah', 'left');
        $this->db->join('nasabah_online', 'nasabah_online.id_nasabah_online = setoran_online.id_nasabah_online', 'left');
        $this->db->join('status_setoran', 'status_setoran.id_status_setoran = setoran_online.id_status_setoran', 'left');
        $this->db->join('jenis_transaksi', 'jenis_transaksi.id_jenis_transaksi = setoran_online.id_jenis_transaksi', 'left');
        // $this->db->join('setoran_online_detail', 'setoran_online_detail.id_setoran_online = setoran_online.id_setoran_online');
        $this->db->where("(UNIX_TIMESTAMP(". $this->table .".tgl_setoran) BETWEEN UNIX_TIMESTAMP('" . $awal . "') AND UNIX_TIMESTAMP('" . $akhir  . " 23:59') ) ");

        $this->db->group_start();
        $this->db->like('id_setoran_online', $q);
        $this->db->or_like('nama_nasabah_online', $q);
        $this->db->or_like('nama_bank_sampah', $q);
        $this->db->or_like('nama_status_setoran', $q);
        $this->db->or_like('nama_jenis_transaksi', $q);
        $this->db->group_end();
        $this->db->from($this->table);
        return $this->db->count_all_results();
    }

    // get data with limit and search
    function get_limit_data($limit, $start = 0, $q = NUL,$awal,$akhir)
    {
        $this->db->join('bank_sampah', 'bank_sampah.id_bank_sampah = setoran_online.id_bank_sampah', 'left');
        $this->db->join('nasabah_online', 'nasabah_online.id_nasabah_online = setoran_online.id_nasabah_online', 'left');
        $this->db->join('status_setoran', 'status_setoran.id_status_setoran = setoran_online.id_status_setoran', 'left');
        $this->db->join('jenis_transaksi', 'jenis_transaksi.id_jenis_transaksi = setoran_online.id_jenis_transaksi', 'left');
        $this->db->where("(UNIX_TIMESTAMP(". $this->table .".tgl_setoran) BETWEEN UNIX_TIMESTAMP('" . $awal . "') AND UNIX_TIMESTAMP('" . $akhir  . " 23:59') ) ");
        $this->db->order_by($this->id, $this->order);
        $this->db->group_start();
        $this->db->like('id_setoran_online', $q);
        $this->db->or_like('nama_nasabah_online', $q);
        $this->db->or_like('nama_bank_sampah', $q);
        $this->db->or_like('nama_status_setoran', $q);
        $this->db->or_like('nama_jenis_transaksi', $q);
        $this->db->group_end();
        // $this->db->or_like('id_reason', $q);
        // $this->db->or_like('nilai_nasabah_online', $q);
        // $this->db->or_like('nilai_bank_sampah', $q);
        // $this->db->or_like('ulasan_bank_sampah', $q);
        // $this->db->or_like('ulasan_nasabah', $q);
        // $this->db->or_like('tgl_review', $q);
        $this->db->limit($limit, $start);
        return $this->db->get($this->table)->result();
    }
    function total_rows_detail($q = NULL, $id = '')
    {
        $this->db->join('kategori_sampah', 'kategori_sampah.id_kategori_sampah = setoran_online_detail.id_kategori_sampah', 'left');
        $this->db->join('jenis_sampah_bs', 'jenis_sampah_bs.id_jenis_sampah_bs = setoran_online_detail.id_jenis_sampah_bs', 'left');
        $this->db->join('jenis_sampah', 'jenis_sampah.id_jenis_sampah = jenis_sampah_bs.id_jenis_sampah', 'left');
        $this->db->join('merk_sampah', 'merk_sampah.id_merk = setoran_online_detail.id_merk', 'left');

        $this->db->where('id_setoran_online', $id);
        $this->db->group_start();
        $this->db->or_like('nama_kategori_sampah', $q);
        $this->db->or_like('nama_jenis_sampah', $q);
        $this->db->or_like('nama_merk', $q);
        $this->db->group_end();
        $this->db->from('setoran_online_detail');
        return $this->db->count_all_results();
    }

    // get data with limit and search
    function get_limit_data_detail($limit, $start = 0, $q = NULL, $id = '')
    {
        $this->db->join('kategori_sampah', 'kategori_sampah.id_kategori_sampah = setoran_online_detail.id_kategori_sampah', 'left');
        $this->db->join('jenis_sampah_bs', 'jenis_sampah_bs.id_jenis_sampah_bs = setoran_online_detail.id_jenis_sampah_bs', 'left');
        $this->db->join('jenis_sampah', 'jenis_sampah.id_jenis_sampah = jenis_sampah_bs.id_jenis_sampah', 'left');
        $this->db->join('merk_sampah', 'merk_sampah.id_merk = setoran_online_detail.id_merk', 'left');

        $this->db->where('id_setoran_online', $id);
        $this->db->group_start();
        $this->db->or_like('nama_kategori_sampah', $q);
        $this->db->or_like('nama_jenis_sampah', $q);
        $this->db->or_like('nama_merk', $q);
        $this->db->group_end();
        $this->db->limit($limit, $start);
        return $this->db->get('setoran_online_detail')->result();
    }

    // insert data
    function insert($data)
    {
        $this->db->insert($this->table, $data);
    }

    // update data
    function update($id, $data)
    {
        $this->db->where($this->id, $id);
        $this->db->update($this->table, $data);
    }

    // delete data
    function delete($id)
    {
        $this->db->where($this->id, $id);
        $this->db->delete($this->table);
    }
}

/* End of file Setoran_online_model.php */
/* Location: ./application/models/Setoran_online_model.php */
/* Please DO NOT modify this information : */
/* Generated by Harviacode Codeigniter CRUD Generator 2020-03-19 07:28:31 */
/* http://harviacode.com */
