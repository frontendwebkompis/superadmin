<script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>

<div class="row">
	<section class="col-lg connectedSortable ui-sortable">
		<div class="card card-info">
			<div class="card-header">
				<div class="card-title">
					<?php echo $title_card ?>
				</div>
			</div>
			<div class="card-body">
				<div class="tab-content p-0">

					<div class="row">
						<div class="col-md-5 mb-2">
							<?= filterDate($this->input->get('awal'), $this->input->get('akhir'), base_url('biller_pengepul')); ?>
						</div>
						<div class="col-md-3 offset-md-4 mb-2">
							<?= search(site_url('biller_pengepul/index'), site_url('biller_pengepul'), $q) ?>
						</div>
					</div>
					<div class="tab-content p-0" style="overflow:auto">
						<table class="table table-bordered table-striped table-condensed table-hover table-sm" style="margin-bottom: 10px">

							<tr>
								<th class="text-center" width="50px">No</th>
								<th class="text-center">Nama Pengepul</th>
								<th class="text-center">Tanggal</th>
								<th class="text-center">ID layanan tarik saldo online</th>
								<th class="text-center">Nama layanan tarik saldo online</th>
								<th class="text-center">Nomor Pelayanan</th>
								<th class="text-center">Amount</th>
							</tr><?php
									if ($total_rows == 0) {
										echo '<tr><td colspan="8" bgcolor="grey" style="color: white; font-weight: bold; text-align: center;">Data Tidak Ditemukan</td></tr>';
									} else {
										foreach ($biller_pengepul as $k) { ?>
									<tr>
										<td class="text-center"><?php echo ++$start ?></td>
										<td class="text-center"><?= $k->nama_pengepul ?></td>
										<td class="text-center"><?= fulldate($k->datetime) ?></td>
										<td class="text-center"><?= $k->id_layanan_tarik_saldo_online ?></td>
										<td class="text-center"><?= $k->nama_layanan_tarik_saldo_online ?></td>
										<td class="text-center"><?= $k->nomor_pelayanan ?></td>
										<td class="text-center"><?= $k->amount ?></td>

									</tr>
							<?php }
									} ?>
						</table>
					</div>
					<?= footer($total_rows, $pagination, '') ?>
				</div>
			</div>
		</div>
	</section>
</div>
<script type="text/javascript">
	setDateFilter('akhir', 'awal');

	function confirm(res) {
		Swal.fire({
			title: 'Anda Yakin Mau Menghapus?',
			text: "Tidak bisa dikembalikan jika sudah dihapus!",
			icon: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'Ya, Hapus!',
			cancelButtonText: 'Batal'
		}).then((result) => {
			if (result.value) {
				Swal.fire(
					'Terhapus!',
					'File sudah terhapus.',
					'success'
				)
				window.location = '<?php echo base_url() . 'biller_pengepul/del_sem/'; ?>' + res + '?id='+res;
			}
		});
	}
</script>
