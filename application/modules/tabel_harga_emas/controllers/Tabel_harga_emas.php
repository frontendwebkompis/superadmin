<?php

if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class Tabel_harga_emas extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
		if (cek_token()) {
			$this->load->model('Tabel_harga_emas_model');
			$this->load->library('form_validation');
		} else {
			logout();
		}
	}

	public function index()
	{
		$q = urldecode($this->input->get('q', TRUE));
		$start = intval($this->input->get('start'));

		if ($q <> '') {
			$config['base_url'] = base_url() . 'tabel_harga_emas?q=' . urlencode($q);
			$config['first_url'] = base_url() . 'tabel_harga_emas?q=' . urlencode($q);
		} else {
			$config['base_url'] = base_url() . 'tabel_harga_emas';
			$config['first_url'] = base_url() . 'tabel_harga_emas';
		}

		$config['per_page'] = 10;
		$config['page_query_string'] = TRUE;
		$config['total_rows'] = $this->Tabel_harga_emas_model->total_rows($q);
		$tabel_harga = $this->Tabel_harga_emas_model->get_limit_data($config['per_page'], $start, $q);
		// $ukuran_emas = $this->db->get('ukuran_emas')->result();

		$this->db->select('*');
		$this->db->from('ukuran_emas');
		$this->db->join('jenis_emas', 'ukuran_emas.id_jenis_emas = jenis_emas.id_jenis_emas');
		$this->db->order_by('id_ukuran_emas', 'ASC');
		$this->db->where('ukuran_emas.status_remove', 1);
		$ukuran_emas = $this->db->get()->result();

		$this->load->library('pagination');
		$this->pagination->initialize($config);

		$data = array(
			'tittle'                => 'Tabel Harga Emas',
			'judul'                 => 'Tabel Harga Emas',
			'title_card'            => 'List Harga Emas',
			'tabel_harga_emas'      => $tabel_harga,
			'ukuran_emas'           => $ukuran_emas,
			'q'                     => $q,
			'pagination'            => $this->pagination->create_links(),
			'total_rows'            => $config['total_rows'],
			'start'                 => $start,
			'action'                => base_url('tabel_harga/create_action'),
			'menu_aktif'            => 'tabel_harga_emas',
			'aktif_menu'            => 'list_harga'
		);
		$res['datakonten']  = $this->load->view('tabel_harga_emas/Tabel_harga_emas_list', $data, true);
		$this->load->view('layouts/main_view', $res);
	}

	public function ukuran_emas()
	{
		$q = urldecode($this->input->get('q', TRUE));
		$start = intval($this->input->get('start'));

		if ($q <> '') {
			$config['base_url'] = base_url() . 'tabel_harga_emas/ukuran_emas?q=' . urlencode($q);
			$config['first_url'] = base_url() . 'tabel_harga_emas/ukuran_emas?q=' . urlencode($q);
		} else {
			$config['base_url'] = base_url() . 'tabel_harga_emas/ukuran_emas';
			$config['first_url'] = base_url() . 'tabel_harga_emas/ukuran_emas';
		}

		$config['per_page'] = 10;
		$config['page_query_string'] = TRUE;
		$config['total_rows'] = $this->Tabel_harga_emas_model->total_rows_ukuran($q);
		$tabel_harga = $this->Tabel_harga_emas_model->get_limit_data_ukuran($config['per_page'], $start, $q);
		$jenis_emas = $this->db->get('jenis_emas')->result();

		$this->load->library('pagination');
		$this->pagination->initialize($config);

		$data = array(
			'tittle'                => 'Tabel Harga Emas',
			'judul'                 => 'Tabel Harga Emas',
			'title_card'            => 'List Harga Emas',
			'tabel_ukuran'          => $tabel_harga,
			'jenis_emas'            => $jenis_emas,
			'q'                     => $q,
			'pagination'            => $this->pagination->create_links(),
			'total_rows'            => $config['total_rows'],
			'start'                 => $start,
			'action'                => base_url('tabel_harga/create_action'),
			'menu_aktif'            => 'tabel_harga_emas',
			'aktif_menu'            => 'ukuran'
		);
		// $res['datakonten']  = $this->load->view('tabel_harga_emas/tabel_ukuran_emas', $data, true);
		$res['datakonten']  = $this->load->view('tabel_harga_emas/Tabel_ukuran_emas', $data, true);

		$this->load->view('layouts/main_view', $res);
	}

	public function ambilDataJenis($id)
	{
		$this->db->select('*');
		$this->db->from('ukuran_emas');
		$this->db->join('jenis_emas', 'jenis_emas.id_jenis_emas = ukuran_emas.id_jenis_emas');
		$this->db->where('id_ukuran_emas', $id);
		$data = $this->db->get()->row();
		echo json_encode($data);
	}

	public function input_action()
	{
		if ($_SERVER['REQUEST_METHOD'] != 'POST') {
			$this->session->set_flashdata('gagal', 'Halaman tidak ditemukan');
			redirect('tabel_harga_emas');
		}
		$this->form_validation->set_rules('ukuran_emas', 'ukuran emas', 'trim|required');
		$this->form_validation->set_rules('waktu', 'waktu', 'trim|required');
		$this->form_validation->set_rules('harga_distributor', 'harga distributor', 'trim|required');
		$this->form_validation->set_rules('harga_jual_nasabah', 'harga jual nasabah', 'trim|required');
		$this->form_validation->set_rules('harga_jual_bs', 'harga jual bs', 'trim|required');
		$this->form_validation->set_rules('keterangan', 'keterangan', 'trim|required');

		$ukuran_emas = $this->input->post('ukuran_emas');
		$data_ukuran = $this->db->where('id_ukuran_emas', $ukuran_emas)->where('status_aktif', 1)->where('status_remove', 1)->get('ukuran_emas')->row();
		if ($data_ukuran->id_jenis_emas == 3) {
			$this->form_validation->set_rules('buyback_serti_eye', 'buyback serti eye', 'trim|required');
			$this->form_validation->set_rules('buyback_serti_lama', 'buyback serti lama', 'trim|required');
		} else {
			$this->form_validation->set_rules('buyback', 'buyback', 'trim|required');
		}

		if ($this->form_validation->run() == FALSE) {
			$this->index();
		} else {
			$waktu = $this->input->post('waktu');
			$keterangan = $this->input->post('keterangan');
			$harga_distributor = $this->input->post('harga_distributor');
			$harga_jual_nasabah = $this->input->post('harga_jual_nasabah');
			$harga_jual_bs = $this->input->post('harga_jual_bs');
			$buyback = $this->input->post('buyback');
			$buyback_serti_eye = $this->input->post('buyback_serti_eye');
			$buyback_serti_lama = $this->input->post('buyback_serti_lama');
			$statusStok = $this->input->post('statusStok');
			$keterangan = $this->input->post('keterangan');

			if ($harga_jual_nasabah < $harga_distributor) {
				$this->session->set_flashdata('gagal', 'Harga jual nasabah tidak boleh kurang dari harga distributor');
				redirect('tabel_harga_emas');
			}

			$data = [
				'id_ukuran_emas'	=> $ukuran_emas,
				'waktu'				=> $waktu,
				'status_stok'		=> $statusStok,
				'keterangan'		=> $keterangan,
				'harga_distributor' => $harga_distributor,
				'harga_jualNasabah' => $harga_jual_nasabah,
				'harga_jualNasabah' => $harga_jual_nasabah,
				'harga_jualBS' 		=> $harga_jual_bs,
				'id_superadmin'		=> getID(),
				'status_remove'		=> 1,
				'status_aktif'		=> 1,
				'created_at'		=> date('Y-m-d H:i:s'),
				'update_at'		=> date('Y-m-d H:i:s'),
			];
			if ($data_ukuran->id_jenis_emas == 3) {
				try {

					// ------------------------------------------------------------------------
					// certi eye
					// ------------------------------------------------------------------------
					if (!is_dir("./assets/gambar/foto_emas/")) {
						mkdir("./assets/gambar/foto_emas/");
					}
					$config['upload_path'] = './assets/gambar/foto_emas/'; //path folder
					$config['allowed_types'] = 'gif|jpg|png|jpeg|bmp'; //type yang dapat diakses bisa anda sesuaikan
					$config['encrypt_name'] = TRUE; //Enkripsi nama yang terupload
					$this->load->library('upload', $config);
					if ($this->upload->do_upload('serti_eye', TRUE)) {
						$gbr = $this->upload->data();
						$upload = s3_upload('foto_emas/' . $gbr['file_name'], $gbr['full_path']);
						$gambar = $upload['ObjectURL'];
						$data['gambar'] = $gambar;
						@unlink($config['upload_path'] . $gbr['file_name']);
					} else {
						$this->session->set_flashdata('gagal', 'Upload sertifikat eye gagal.');
						redirect('tabel_harga_emas');
					}

					$prefix = "EMAS" . date('dmY');
					$row = $this->db->query("SELECT max(id_harga_emas) AS max FROM tabel_harga_emas WHERE id_harga_emas LIKE '$prefix%'")->row();
					$NoUrut = (int) substr($row->max, 12, 4);
					$NoUrut = $NoUrut + 1; //nomor urut +1
					$NoUrut = sprintf('%04d', $NoUrut);
					$fix = $prefix . $NoUrut;

					$data['buyback'] = $buyback_serti_eye;
					$data['jenis_sertifikat'] = 1;
					$data['id_harga_emas'] = $fix;
					$this->db->insert('tabel_harga_emas', $data);

					// ------------------------------------------------------------------------
					// certi lama
					// ------------------------------------------------------------------------
					if (!is_dir("./assets/gambar/foto_emas/")) {
						mkdir("./assets/gambar/foto_emas/");
					}
					$config['upload_path'] = './assets/gambar/foto_emas/'; //path folder
					$config['allowed_types'] = 'gif|jpg|png|jpeg|bmp'; //type yang dapat diakses bisa anda sesuaikan
					$config['encrypt_name'] = TRUE; //Enkripsi nama yang terupload
					$this->load->library('upload', $config);
					if ($this->upload->do_upload('serti_lama', TRUE)) {
						$gbr = $this->upload->data();
						$upload = s3_upload('foto_emas/' . $gbr['file_name'], $gbr['full_path']);
						$gambar = $upload['ObjectURL'];
						$data['gambar'] = $gambar;
						@unlink($config['upload_path'] . $gbr['file_name']);
					} else {
						$this->session->set_flashdata('gagal', 'Upload sertifikat eye gagal.');
						redirect('tabel_harga_emas');
					}
					$prefix = "EMAS" . date('dmY');
					$row = $this->db->query("SELECT max(id_harga_emas) AS max FROM tabel_harga_emas WHERE id_harga_emas LIKE '$prefix%'")->row();
					$NoUrut = (int) substr($row->max, 12, 4);
					$NoUrut = $NoUrut + 1; //nomor urut +1
					$NoUrut = sprintf('%04d', $NoUrut);
					$fix = $prefix . $NoUrut;

					$data['id_harga_emas'] = $fix;
					$data['buyback'] = $buyback_serti_lama;
					$data['jenis_sertifikat'] = 2;
					$this->db->insert('tabel_harga_emas', $data);

					$this->session->set_flashdata('berhasil', 'Berhasil menambahkan data');
					redirect('tabel_harga_emas');
				} catch (\Throwable $th) {
					$this->session->set_flashdata('gagal', 'Gagal menambahkan data');
					redirect('tabel_harga_emas');
				}
			} else {
				$prefix = "EMAS" . date('dmY');
				$row = $this->db->query("SELECT max(id_harga_emas) AS max FROM tabel_harga_emas WHERE id_harga_emas LIKE '$prefix%'")->row();
				$NoUrut = (int) substr($row->max, 12, 4);
				$NoUrut = $NoUrut + 1; //nomor urut +1
				$NoUrut = sprintf('%04d', $NoUrut);
				$fix = $prefix . $NoUrut;
				$data['id_harga_emas'] = $fix;
				$data['buyback'] = $buyback;

				// ------------------------------------------------------------------------
				// Foto emas
				// ------------------------------------------------------------------------
				if (!is_dir("./assets/gambar/foto_emas/")) {
					mkdir("./assets/gambar/foto_emas/");
				}
				$config['upload_path'] = './assets/gambar/foto_emas/'; //path folder
				$config['allowed_types'] = 'gif|jpg|png|jpeg|bmp'; //type yang dapat diakses bisa anda sesuaikan
				$config['encrypt_name'] = TRUE; //Enkripsi nama yang terupload
				$this->load->library('upload', $config);
				if ($this->upload->do_upload('foto_emas', TRUE)) {
					$gbr = $this->upload->data();
					$upload = s3_upload('foto_emas/' . $gbr['file_name'], $gbr['full_path']);
					$gambar = $upload['ObjectURL'];
					$data['gambar'] = $gambar;
					@unlink($config['upload_path'] . $gbr['file_name']);
				} else {
					$this->session->set_flashdata('gagal', 'Upload foto emas.');
					redirect('tabel_harga_emas');
				}

				$cek = $this->db->insert('tabel_harga_emas', $data);
				if ($cek) {
					$this->session->set_flashdata('berhasil', 'Berhasil menambahkan data');
					redirect('tabel_harga_emas');
				} else {
					$this->session->set_flashdata('gagal', 'Gagal menambahkan data');
					redirect('tabel_harga_emas');
				}
			}
		}
	}

	public function edit_action($id_harga_emas = null)
	{
		if ($_SERVER['REQUEST_METHOD'] != 'POST') {
			$this->session->set_flashdata('gagal', 'Halaman tidak ditemukan');
			redirect('tabel_harga_emas');
		}
		$this->form_validation->set_rules('ukuran_emas', 'ukuran emas', 'trim|required');
		$this->form_validation->set_rules('waktu', 'waktu', 'trim|required');
		$this->form_validation->set_rules('harga_distributor', 'harga distributor', 'trim|required');
		$this->form_validation->set_rules('harga_jual_nasabah', 'harga jual nasabah', 'trim|required');
		$this->form_validation->set_rules('harga_jual_bs', 'harga jual bs', 'trim|required');
		$this->form_validation->set_rules('keterangan', 'keterangan', 'trim|required');

		$ukuran_emas = $this->input->post('ukuran_emas');
		$data_ukuran = $this->db->where('id_ukuran_emas', $ukuran_emas)->where('status_aktif', 1)->where('status_remove', 1)->get('ukuran_emas')->row();
		$data_harga_emas = $this->db->where('id_harga_emas', $id_harga_emas)->where('status_aktif', 1)->where('status_remove', 1)->get('tabel_harga_emas')->row();
		if (empty($data_harga_emas)) {
			$this->session->set_flashdata('gagal', 'Data tidak ditemukan');
			redirect('tabel_harga_emas');
		}

		if ($data_ukuran->id_jenis_emas == 3) {
			if ($data_harga_emas->jenis_sertifikat == 1) {
				$this->form_validation->set_rules('buyback_serti_eye', 'buyback serti eye', 'trim|required');
			} elseif ($data_harga_emas->jenis_sertifikat == 2) {
				$this->form_validation->set_rules('buyback_serti_lama', 'buyback serti lama', 'trim|required');
			}
		} else {
			$this->form_validation->set_rules('buyback', 'buyback', 'trim|required');
		}


		if ($this->form_validation->run() == FALSE) {
			$this->index();
		} else {
			$waktu = $this->input->post('waktu');
			$keterangan = $this->input->post('keterangan');
			$harga_distributor = $this->input->post('harga_distributor');
			$harga_jual_nasabah = $this->input->post('harga_jual_nasabah');
			$harga_jual_bs = $this->input->post('harga_jual_bs');
			$buyback = $this->input->post('buyback');
			$statusStok = $this->input->post('statusStok');
			$buyback_serti_eye = $this->input->post('buyback_serti_eye');
			$buyback_serti_lama = $this->input->post('buyback_serti_lama');
			$keterangan = $this->input->post('keterangan');

			if ($harga_jual_nasabah < $harga_distributor) {
				$this->session->set_flashdata('gagal', 'Harga jual nasabah tidak boleh kurang dari harga distributor');
				redirect('tabel_harga_emas');
			}

			$data = [
				'id_ukuran_emas'	=> $ukuran_emas,
				'waktu'				=> $waktu,
				'status_stok'		=> $statusStok,
				'keterangan'		=> $keterangan,
				'harga_distributor' => $harga_distributor,
				'harga_jualNasabah' => $harga_jual_nasabah,
				'harga_jualNasabah' => $harga_jual_nasabah,
				'harga_jualBS' 		=> $harga_jual_bs,
				'update_at'		=> date('Y-m-d H:i:s'),
			];

			if ($data_ukuran->id_jenis_emas == 3) {
				if ($data_harga_emas->jenis_sertifikat == 1) {
					// ------------------------------------------------------------------------
					// certi eye
					// ------------------------------------------------------------------------
					if (!is_dir("./assets/gambar/foto_emas/")) {
						mkdir("./assets/gambar/foto_emas/");
					}
					$config['upload_path'] = './assets/gambar/foto_emas/'; //path folder
					$config['allowed_types'] = 'gif|jpg|png|jpeg|bmp'; //type yang dapat diakses bisa anda sesuaikan
					$config['encrypt_name'] = TRUE; //Enkripsi nama yang terupload
					$this->load->library('upload', $config);
					if ($_FILES['serti_eye_edit']['name']) {
						if ($this->upload->do_upload('serti_eye_edit', TRUE)) {
							$gbr = $this->upload->data();
							$upload = s3_upload('foto_emas/' . $gbr['file_name'], $gbr['full_path']);
							$gambar = $upload['ObjectURL'];
							$data['gambar'] = $gambar;
							@unlink($config['upload_path'] . $gbr['file_name']);
						} else {
							$this->session->set_flashdata('gagal', 'Upload sertifikat eye gagal.');
							redirect('tabel_harga_emas');
						}
					}

					$data['buyback'] = $buyback_serti_eye;
					$cek = $this->db->where('id_harga_emas', $id_harga_emas)->update('tabel_harga_emas', $data);
					if ($cek) {
						$this->session->set_flashdata('berhasil', 'Berhasil mengubah data');
						redirect('tabel_harga_emas');
					} else {
						$this->session->set_flashdata('gagal', 'Gagal mengubah data');
						redirect('tabel_harga_emas');
					}
				} elseif ($data_harga_emas->jenis_sertifikat = 2) {
					// ------------------------------------------------------------------------
					// certi lama
					// ------------------------------------------------------------------------
					if (!is_dir("./assets/gambar/foto_emas/")) {
						mkdir("./assets/gambar/foto_emas/");
					}
					$config['upload_path'] = './assets/gambar/foto_emas/'; //path folder
					$config['allowed_types'] = 'gif|jpg|png|jpeg|bmp'; //type yang dapat diakses bisa anda sesuaikan
					$config['encrypt_name'] = TRUE; //Enkripsi nama yang terupload
					$this->load->library('upload', $config);
					if ($_FILES['serti_lama_edit']['name']) {
						if ($this->upload->do_upload('serti_lama_edit', TRUE)) {
							$gbr = $this->upload->data();
							$upload = s3_upload('foto_emas/' . $gbr['file_name'], $gbr['full_path']);
							$gambar = $upload['ObjectURL'];
							$data['gambar'] = $gambar;
							@unlink($config['upload_path'] . $gbr['file_name']);
						} else {
							$this->session->set_flashdata('gagal', 'Upload sertifikat lama gagal.');
							redirect('tabel_harga_emas');
						}
					}
					$data['buyback'] = $buyback_serti_lama;
					$cek = $this->db->where('id_harga_emas', $id_harga_emas)->update('tabel_harga_emas', $data);
					if ($cek) {
						$this->session->set_flashdata('berhasil', 'Berhasil mengubah data');
						redirect('tabel_harga_emas');
					} else {
						$this->session->set_flashdata('gagal', 'Gagal mengubah data');
						redirect('tabel_harga_emas');
					}
				} else {
					echo "nothing";
				}
			} else {
				$data['buyback'] = $buyback;
				// ------------------------------------------------------------------------
				// Foto Emas
				// ------------------------------------------------------------------------
				if (!is_dir("./assets/gambar/foto_emas/")) {
					mkdir("./assets/gambar/foto_emas/");
				}
				$config['upload_path'] = './assets/gambar/foto_emas/'; //path folder
				$config['allowed_types'] = 'gif|jpg|png|jpeg|bmp'; //type yang dapat diakses bisa anda sesuaikan
				$config['encrypt_name'] = TRUE; //Enkripsi nama yang terupload
				$this->load->library('upload', $config);
				if ($_FILES['foto_emas_edit']['name']) {
					if ($this->upload->do_upload('foto_emas_edit', TRUE)) {
						$gbr = $this->upload->data();
						$upload = s3_upload('foto_emas/' . $gbr['file_name'], $gbr['full_path']);
						$gambar = $upload['ObjectURL'];
						$data['gambar'] = $gambar;
						@unlink($config['upload_path'] . $gbr['file_name']);
					} else {
						$this->session->set_flashdata('gagal', 'Upload sertifikat lama gagal.');
						redirect('tabel_harga_emas');
					}
				}
				$cek = $this->db->where('id_harga_emas', $id_harga_emas)->update('tabel_harga_emas', $data);
				if ($cek) {
					$this->session->set_flashdata('berhasil', 'Berhasil mengubah data');
					redirect('tabel_harga_emas');
				} else {
					$this->session->set_flashdata('gagal', 'Gagal mengubah data');
					redirect('tabel_harga_emas');
				}
			}
		}
	}

	public function hapus($id_harga_emas = null)
	{
		$data_harga_emas = $this->db->where('id_harga_emas', $id_harga_emas)->where('status_aktif', 1)->where('status_remove', 1)->get('tabel_harga_emas')->row();
		if (empty($data_harga_emas)) {
			$this->session->set_flashdata('gagal', 'Data tidak ditemukan');
			redirect('tabel_harga_emas');
		}
		$data = [
			'status_remove' => 0,
		];
		$cek = $this->db->where('id_harga_emas', $id_harga_emas)->update('tabel_harga_emas', $data);
		if ($cek) {
			$this->session->set_flashdata('berhasil', 'Berhasil menghapus data');
			redirect('tabel_harga_emas');
		} else {
			$this->session->set_flashdata('gagal', 'Gagal menghapus data');
			redirect('tabel_harga_emas');
		}
	}

	public function api_v_harga_emas($id_harga_emas = null)
	{
		$data = $this->db->where('id_harga_emas', $id_harga_emas)->get('v_harga_emas')->row();
		echo json_encode($data);
	}

	public function input_ukuran()
	{
		if ($_SERVER['REQUEST_METHOD'] != 'POST') {
			$this->session->set_flashdata('gagal', 'Halaman tidak ditemukan');
			redirect('tabel_harga_emas');
		}

		$this->form_validation->set_rules('jenis_emas', 'jenis emas', 'trim|required');
		$this->form_validation->set_rules('ukuran', 'ukuran', 'trim|required');

		if ($this->form_validation->run() == FALSE) {
			$eror = validation_errors();
			$this->session->set_flashdata('gagal', "Validasi gagal. " . $eror);
			$this->ukuran_emas();
		} else {
			$jenis_emas = $this->input->post('jenis_emas');
			$ukuran = $this->input->post('ukuran');

			$data = [
				'id_jenis_emas' => $jenis_emas,
				'ukuran_keping' => $ukuran,
				'status_aktif' => 1,
				'status_remove' => 1,
				'created_at' => date('Y-m-d H:i:s'),
				'update_at' => date('Y-m-d H:i:s'),
			];

			$cek = $this->db->insert('ukuran_emas', $data);
			if ($cek) {
				$this->session->set_flashdata('berhasil', 'Berhasil menambahkan data');
				redirect('tabel_harga_emas/ukuran_emas');
			} else {
				$this->session->set_flashdata('gagal', 'Gagal menambahkan data');
				redirect('tabel_harga_emas/ukuran_emas');
			}
		}
	}

	public function ukuran($id)
	{
		$this->db->where('id_ukuran_emas', $id);
		$this->db->join('jenis_emas', 'ukuran_emas.id_jenis_emas = jenis_emas.id_jenis_emas');
		$data = $this->db->get('ukuran_emas')->row();
		echo json_encode($data);
	}

	public function edit_ukuran($id)
	{
		$this->db->where('id_ukuran_emas', $id);
		$data = $this->db->get('ukuran_emas')->row();
		if ($data) {
			$data = array(
				'id_jenis_emas' => $this->input->post('jenis_emas'),
				'ukuran_keping' => $this->input->post('ukuran')
			);
			$data = $this->db->where('id_ukuran_emas', $id)->update('ukuran_emas', $data);
			if ($data) {
				$this->session->set_flashdata('berhasil', 'Data berhasil diubah');
				redirect('tabel_harga_emas/ukuran_emas');
			} else {
				$this->session->set_flashdata('gagal', 'Data tidak diubah');
				redirect('tabel_harga_emas/ukuran_emas');
			}
		} else {
			$this->session->set_flashdata('gagal', 'Data tidak ditemukan');
			redirect('tabel_harga_emas/ukuran_emas');
		}
	}

	public function edit_status_harga($id = null)
	{
		$data = $this->db->where('id_harga_emas', $id)->get('tabel_harga_emas')->row();
		if (!$data) {
			$json = [
				'status' => false,
				'message' => 'Data tidak ditemukan'
			];
			$this->output->set_status_header(400);
			echo json_encode($json);
			die();
		}

		$cek = $this->db->where('id_harga_emas', $id)->update('tabel_harga_emas', ['status_aktif' => !$data->status_aktif]);
		if (!$cek) {
			$json = [
				'status' => false,
				'message' => 'Gagal mengubah status'
			];
			$this->output->set_status_header(400);
			echo json_encode($json);
			die();
		}

		$json = [
			'status' => true,
			'message' => 'Berhasil mengubah status',
			'data' => $this->db->where('id_harga_emas', $id)->get('tabel_harga_emas')->row(),
		];

		$this->output->set_status_header(200);
		echo json_encode($json);
	}
	public function edit_status_stock($id = null)
	{
		$data = $this->db->where('id_harga_emas', $id)->get('tabel_harga_emas')->row();
		if (!$data) {
			$json = [
				'status' => false,
				'message' => 'Data tidak ditemukan'
			];
			$this->output->set_status_header(400);
			echo json_encode($json);
			die();
		}

		$cek = $this->db->where('id_harga_emas', $id)->update('tabel_harga_emas', ['status_stok' => !$data->status_stok]);
		if (!$cek) {
			$json = [
				'status' => false,
				'message' => 'Gagal mengubah status'
			];
			$this->output->set_status_header(400);
			echo json_encode($json);
			die();
		}

		$json = [
			'status' => true,
			'message' => 'Berhasil mengubah status',
			'data' => $this->db->where('id_harga_emas', $id)->get('tabel_harga_emas')->row(),
		];

		$this->output->set_status_header(200);
		echo json_encode($json);
	}
	public function edit_status_ukuran($id = null)
	{
		$data = $this->db->where('id_ukuran_emas', $id)->get('ukuran_emas')->row();
		if (!$data) {
			$json = [
				'status' => false,
				'message' => 'Data tidak ditemukan'
			];
			$this->output->set_status_header(400);
			echo json_encode($json);
			die();
		}

		$cek = $this->db->where('id_ukuran_emas', $id)->update('ukuran_emas', ['status_aktif' => !$data->status_aktif]);
		if (!$cek) {
			$json = [
				'status' => false,
				'message' => 'Gagal mengubah status'
			];
			$this->output->set_status_header(400);
			echo json_encode($json);
			die();
		}

		$json = [
			'status' => true,
			'message' => 'Berhasil mengubah status',
			'data' => $this->db->where('id_ukuran_emas', $id)->get('ukuran_emas')->row(),
		];

		$this->output->set_status_header(200);
		echo json_encode($json);
	}
}
/* End of file Tabel_harga.php */
/* Location: ./application/controllers/Tabel_harga.php */
