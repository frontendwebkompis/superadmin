<div class="row">
    <div class="col-12 text-center">
        <div class="btn-group btn-group-sm">
            <a href="<?= base_url("tabel_harga_emas") ?>" class="btn btn-sm <?php if($aktif_menu == 'list_harga'){
                echo 'btn-success';
            } else {
                echo 'btn-default';
            }  ?>">List harga emas</a>
            <a href="<?= base_url('tabel_harga_emas/ukuran_emas') ?>" class="btn btn-sm <?php if($aktif_menu == 'ukuran'){
                echo 'btn-success';
            } else {
                echo 'btn-default';
            }  ?>">Ukuran emas</a>
        </div>
    </div>
</div>