<div class="row">
	<section class="col-lg connectedSortable ui-sortable">
		<div class="card card-info">
			<div class="card-header">
				<div class="card-title">
					<?php echo $title_card ?>
				</div>
			</div>
			<div class="card-body">
				<div class="tab-content p-0">
                <?php $this->load->view('tabel_harga_emas/menu_button'); ?>
					<div class="row">
						<div class="col-md-5 mb-2">
                            <button class="btn btn-sm btn-<?= bgCard() ?> btn-tambah-harga-emas" data-toggle="modal" data-target="#modalEmas">Tambah Data</button>
						</div>
						<div class="col-md-3 offset-md-4 mb-2">
							<?= search(site_url('biller_pengepul/index'), site_url('biller_pengepul'), $q) ?>
						</div>
					</div>
					<div class="tab-content p-0" style="overflow:auto">
						<table class="table table-bordered table-striped table-condensed table-hover table-sm" style="margin-bottom: 10px">
							<tr>
								<th class="text-center" width="50px">No</th>
								<th class="text-center">Ukuran</th>
								<th class="text-center">Jenis Emas</th>
								<th class="text-center">Waktu</th>
								<th class="text-center">Harga Distributor</th>
								<th class="text-center">Harga Jual BS</th>
								<th class="text-center">Harga Jual Nasabah</th>
								<th class="text-center">Buyback</th>
								<th class="text-center">Stok</th>
								<th class="text-center">Status Aktif</th>
								<th class="text-center">Aksi</th>
							</tr><?php
									if ($total_rows == 0) {
										echo '<tr><td colspan="12" bgcolor="grey" style="color: white; font-weight: bold; text-align: center;">Data Tidak Ditemukan</td></tr>';
									} else {
										foreach ($tabel_harga_emas as $k) { ?>
									<tr>
										<td class="text-center"><?php echo ++$start ?></td>
										<td class="text-center"><?= $k->ukuran_keping ?></td>
										<td class="text-center"><?= $k->jenis_emas ?></td>
										<td class="text-center"><?= fullDateNoTime($k->waktu) ?></td>
										<td class="text-center"><?= format($k->harga_distributor) ?></td>
										<td class="text-center"><?= format($k->harga_jualBS )?></td>
										<td class="text-center"><?= format($k->harga_jualNasabah )?></td>
										<td class="text-center"><?= format($k->buyback) ?></td>
										<td class="text-center">
                                            <div class="form-group">
                                                <div class="custom-control custom-switch custom-switch-off-danger custom-switch-on-success" title="<?= $k->status_stok == '1' ? 'Ready' : 'Out of stok' ?>">
                                                    <input type="checkbox" class="custom-control-input input-cursor" id="switchStokStatus<?= $k->id_harga_emas ?>" <?= $k->status_stok == '1' ? 'checked' : '' ?> onclick="stockAction('<?= $k->id_harga_emas ?>')">
                                                    <label class="custom-control-label" for="switchStokStatus<?= $k->id_harga_emas ?>"></label>
                                                </div>
                                            </div>
                                        </td>
										<td class="text-center">
                                            <div class="form-group">
                                                <div class="custom-control custom-switch custom-switch-off-danger custom-switch-on-success">
                                                    <input type="checkbox" class="custom-control-input input-cursor" id="customSwitch3<?= $k->id_harga_emas ?>" <?= $k->status_aktif == '1' ? 'checked' : '' ?> onclick="cekStatus('<?= $k->id_harga_emas ?>')">
                                                    <label class="custom-control-label" for="customSwitch3<?= $k->id_harga_emas ?>"></label>
                                                </div>
                                            </div>
                                        </td>
                                        
                                        <td class="text-center">
                                            <div class="btn-group btn-group-sm">
                                                <button class="btn btn-sm btn-info" title="Detail Harga Emas" data-toggle="modal" data-target="#modalDetailEmas" onclick="detailEmas('<?= $k->id_harga_emas ?>')"><i class="fas fa-info-circle"></i></button>
                                                <button class="btn btn-sm btn-success" title="Edit Harga Emas" data-toggle="modal" data-target="#modalEmas" onclick="editEmas('<?= $k->id_harga_emas ?>')"><i class="fas fa-edit"></i></button>
                                                <button class="btn btn-sm btn-danger" title="Hapus Harga Emas" onclick="deleteEmas('<?= $k->id_harga_emas ?>')"><i class="fas fa-trash"></i></button>
                                            </div>
                                        </td>
									</tr>
							<?php }
									} ?>
						</table>
					</div>
					<?= footer($total_rows, $pagination, '') ?>
				</div>
			</div>
		</div>
	</section>
</div>

<div id="modalEmas" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-md" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title"></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body text-sm">
                <form action="" method="post" id="formEmas" role="form" enctype="multipart/form-data">
                <div class="row px-3">
                    <div class="col-12">
                        <div class="form-group">
                            <label for="">Jenis Dan Ukuran Emas</label>
                            <select name="ukuran_emas" id="ukuran_emas" class="form-control form-control-sm">
                                <option value="">Pilih jenis emas</option>
                                <?php foreach ($ukuran_emas as $data){ ?>
								<option value="<?php echo $data->id_ukuran_emas; ?>">
								<?php echo $data->ukuran_keping.' - '.$data->jenis_emas ?>
								</option>
								<?php }?>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="">Waktu</label>
                            <input type="text" class="form-control form-control-sm" name="waktu" id="waktu" placeholder="YYYY-MM-DD" value="" />
                        </div>
                        <div class="form-group">
                            <label for="">Harga Distributor</label>
                            <input type="text" class="form-control form-control-sm" name="harga_distributor" id="harga_distributor" placeholder="0" value="" onkeypress="return hanyaAngka(event)"/>
                        </div>
                        <div class="form-group">
                            <label for="">Harga Jual Nasabah</label>
                            <input type="text" class="form-control form-control-sm" name="harga_jual_nasabah" id="harga_jual_nasabah" placeholder="0" value="" onkeypress="return hanyaAngka(event)"/>
                        </div>
                        <div class="form-group">
                            <label for="">Harga Jual BS</label>
                            <input type="text" class="form-control form-control-sm" name="harga_jual_bs" id="harga_jual_bs" placeholder="0" value="" onkeypress="return hanyaAngka(event)" readonly/>
                        </div>
                        <div class="input-emas" style="display: none;">
                            <div class="form-group">
                                <label for="">Buyback</label>
                                <input type="text" class="form-control form-control-sm" name="buyback" id="buyback" placeholder="0" value="" onkeypress="return hanyaAngka(event)"/>
                            </div>
                            <div class="form-group foto_emas">
                                <label for="foto_emas">Foto Emas</label>
                                <input type="file" class="form-control form-control-sm" name="foto_emas" id="foto_emas" value=""/>
                            </div>
                            <div class="form-group foto_emas_edit" style="display: none">
                                <label for="foto_emas_edit">Foto Emas</label>
                                <input type="file" class="form-control form-control-sm" name="foto_emas_edit" id="foto_emas_edit" value=""/>
                            </div>
                        </div>
                        <div class="input-antam" style="display: none;">
                            <div class="form-group buyback-serti-eye">
                                <label for="">Buyback Certi Eye</label>
                                <input type="text" class="form-control form-control-sm" name="buyback_serti_eye" id="buyback_serti_eye" placeholder="0" value="" onkeypress="return hanyaAngka(event)"/>
                            </div>
                            <div class="form-group buyback-serti-lama">
                                <label for="">Buyback Certi Lama</label>
                                <input type="text" class="form-control form-control-sm" name="buyback_serti_lama" id="buyback_serti_lama" placeholder="0" value="" onkeypress="return hanyaAngka(event)"/>
                            </div>
                            <div class="form-group file-serti-eye">
                                <label for="serti_eye">Foto Emas Certi Eye</label>
                                <input type="file" class="form-control form-control-sm" name="serti_eye" id="serti_eye" placeholder="0" value=""/>
                            </div>
                            <div class="form-group file-serti-lama">
                                <label for="serti_lama">Foto Emas Certi Lama</label>
                                <input type="file" class="form-control form-control-sm" name="serti_lama" id="serti_lama" placeholder="0" value=""/>
                            </div>
                            <div class="form-group file-serti-eye-edit">
                                <label for="serti_eye">Foto Emas Certi Eye</label>
                                <input type="file" class="form-control form-control-sm" name="serti_eye_edit" id="serti_eye_edit" placeholder="0" value=""/>
                            </div>
                            <div class="form-group file-serti-lama-edit">
                                <label for="serti_lama">Foto Emas Certi Lama</label>
                                <input type="file" class="form-control form-control-sm" name="serti_lama_edit" id="serti_lama_edit" placeholder="0" value=""/>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="statusStok">Status Stok</label>
                            <select name="statusStok" id="statusStok" class="form-control form-control-sm">
                                <option value="">Pilih status stok</option>
                                <option value="1" selected>Ready</option>
                                <option value="0">Out of stock</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="keterangan">Keterangan</label>
                            <textarea name="keterangan" id="keterangan" rows="2" placeholder="Keterangan" class="form-control form-control-sm"></textarea>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <div class="col-md-12 text-center">
                    <button id="btn-submit" type="submit" class="btn btn-action-modal btn-sm btn-<?= bgCard() ?>">Simpan</button>
                    <button type="button" class="btn btn-action-modal btn-sm btn-secondary" data-dismiss="modal">Batal</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="modalDetailEmas" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title"></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body text-sm">
                <div class="row px-3">
                    <div class="col-sm-6">
                        <table class="table table-sm table-borderless">
                            <tr>
                                <td>Ukuran Keping</td>
                                <td>:</td>
                                <td id="ukuran_keping"><span class="badge badge-danger">Kosong</span></td>
                            </tr>
                            <tr>
                                <td>Jenis Emas</td>
                                <td>:</td>
                                <td id="jenis_emas"><span class="badge badge-danger">Kosong</span></td>
                            </tr>
                            <tr>
                                <td>Waktu</td>
                                <td>:</td>
                                <td id="waktu"><span class="badge badge-danger">Kosong</span></td>
                            </tr>
                            <tr>
                                <td>Harga Distributor</td>
                                <td>:</td>
                                <td id="harga_distributor"><span class="badge badge-danger">Kosong</span></td>
                            </tr>
                            <tr>
                                <td>Harga Jual BS</td>
                                <td>:</td>
                                <td id="harga_jual_bs"><span class="badge badge-danger">Kosong</span></td>
                            </tr>
                            <tr>
                                <td>Harga Jual Nasabah</td>
                                <td>:</td>
                                <td id="harga_jual_nasabah"><span class="badge badge-danger">Kosong</span></td>
                            </tr>
                            <tr>
                                <td>Buyback</td>
                                <td>:</td>
                                <td id="buyback"><span class="badge badge-danger">Kosong</span></td>
                            </tr>
                            <tr>
                                <td>Jenis Sertifikat</td>
                                <td>:</td>
                                <td id="jenis_sertifikat"><span class="badge badge-danger">Kosong</span></td>
                            </tr>
                            <tr>
                                <td>Status Aktif</td>
                                <td>:</td>
                                <td id="status_aktif"><span class="badge badge-danger">Kosong</span></td>
                            </tr>
                            <tr>
                                <td>Keterangan</td>
                                <td>:</td>
                                <td id="keterangan"><span class="badge badge-danger">Kosong</span></td>
                            </tr>
                        </table>
                    </div>
                    <div class="col-sm-6 text-center">
                        <label for="">Foto Emas</label><br>
                        <img id="foto_emas" class="image-detail-emas" src="<?= PATH_ASSETS ?>gambar/default/minigold.jpg" alt="">
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <div class="col-md-12 text-center">
                    <button type="button" class="btn btn-action-modal btn-sm btn-secondary" data-dismiss="modal">Batal</button>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    const modalEmas         = 'modalEmas'
    const modalDetailEmas   = 'modalDetailEmas'
    const formEmas          = 'formEmas'

    $(function(){
        formEmasValidation(formEmas)
        $('#'+modalEmas+' .file-serti-eye-edit').css({'display': 'none'})
        $('#'+modalEmas+' .file-serti-lama-edit').css({'display': 'none'})
    })

    function cekBuyback(val){
        if(parseInt(val) > parseInt($('#'+modalEmas+' #harga_distributor').val())){
            CekKonfirmasi('Harga buyback tidak boleh lebih dari harga distributor', '')
            return $('#'+modalEmas+' #harga_distributor').val()
        }
        return val
    }

    $('#'+modalEmas+' #buyback').keyup(function(e){
        $('#'+modalEmas+' #buyback').val(cekBuyback($('#'+modalEmas+' #buyback').val()))
    })

    $('#'+modalEmas+' #buyback_serti_eye').keyup(function(e){
        $('#'+modalEmas+' #buyback_serti_eye').val(cekBuyback($('#'+modalEmas+' #buyback_serti_eye').val()))
    })

    $('#'+modalEmas+' #buyback_serti_lama').keyup(function(e){
        $('#'+modalEmas+' #buyback_serti_lama').val(cekBuyback($('#'+modalEmas+' #buyback_serti_lama').val()))
    })

    function genHargaBS(){
        let harga_distri = 0
        let harga_nasabah = 0
        if($('#'+modalEmas+' #harga_distributor').val() == ''){
            harga_distri = 0
        } else {
            harga_distri = parseInt($('#'+modalEmas+' #harga_distributor').val())
        }
        if($('#'+modalEmas+' #harga_jual_nasabah').val() == ''){
            harga_nasabah   = 0
        } else {
            harga_nasabah   = parseInt($('#'+modalEmas+' #harga_jual_nasabah').val())
        }
        let hasil = parseInt((harga_distri + harga_nasabah)/2)
        $('#'+modalEmas+' #harga_jual_bs').val(hasil.toString())
    }

    $('#'+modalEmas+' #harga_distributor').keyup(function(e){
        genHargaBS()
    })

    $('#'+modalEmas+' #harga_jual_nasabah').keyup(function(e){
        genHargaBS()
    })

    $('#'+modalEmas).on('hidden.bs.modal', function(){
        $('#'+modalEmas+' #ukuran_emas').val('')
        $('#'+modalEmas+' #waktu').val('')
        $('#'+modalEmas+' #waktu').val('')
        $('#'+modalEmas+' #harga_distributor').val('')
        $('#'+modalEmas+' #harga_jual_nasabah').val('')
        $('#'+modalEmas+' #harga_jual_bs').val('')
        $('#'+modalEmas+' #buyback').val('')
        $('#'+modalEmas+' #buyback_serti_eye').val('')
        $('#'+modalEmas+' #buyback_serti_lama').val('')
        $('#'+modalEmas+' #serti_eye').val('')
        $('#'+modalEmas+' #serti_lama').val('')
        $('#'+modalEmas+' #statusStok').val('')
        $('#'+modalEmas+' #keterangan').val('')
        $('#'+modalEmas+' #ukuran_emas').removeClass('is-invalid')
        $('#'+modalEmas+' #waktu').removeClass('is-invalid')
        $('#'+modalEmas+' #waktu').removeClass('is-invalid')
        $('#'+modalEmas+' #harga_distributor').removeClass('is-invalid')
        $('#'+modalEmas+' #harga_jual_nasabah').removeClass('is-invalid')
        $('#'+modalEmas+' #harga_jual_bs').removeClass('is-invalid')
        $('#'+modalEmas+' #buyback').removeClass('is-invalid')
        $('#'+modalEmas+' #buyback_serti_eye').removeClass('is-invalid')
        $('#'+modalEmas+' #buyback_serti_lama').removeClass('is-invalid')
        $('#'+modalEmas+' #serti_eye').removeClass('is-invalid')
        $('#'+modalEmas+' #serti_lama').removeClass('is-invalid')
        $('#'+modalEmas+' #statusStok').removeClass('is-invalid')
        $('#'+modalEmas+' #keterangan').removeClass('is-invalid')
        $('#'+modalEmas+' .file-serti-eye-edit').css({'display': 'none'})
        $('#'+modalEmas+' .file-serti-lama-edit').css({'display': 'none'})
        $('#'+modalEmas+' .foto_emas').css({'display': 'block'})
        $('#'+modalEmas+' .foto_emas_edit').css({'display': 'none'})
    })

    $('#'+modalDetailEmas).on('hidden.bs.modal', function(){
        $('#'+modalDetailEmas+' #ukuran_keping').html('<span class="badge badge-danger">Kosong</span>')
        $('#'+modalDetailEmas+' #jenis_emas').html('<span class="badge badge-danger">Kosong</span>')
        $('#'+modalDetailEmas+' #waktu').html('<span class="badge badge-danger">Kosong</span>')
        $('#'+modalDetailEmas+' #harga_distributor').html('<span class="badge badge-danger">Kosong</span>')
        $('#'+modalDetailEmas+' #harga_jual_bs').html('<span class="badge badge-danger">Kosong</span>')
        $('#'+modalDetailEmas+' #harga_jual_nasabah').html('<span class="badge badge-danger">Kosong</span>')
        $('#'+modalDetailEmas+' #buyback').html('<span class="badge badge-danger">Kosong</span>')
        $('#'+modalDetailEmas+' #jenis_sertifikat').html('<span class="badge badge-danger">Kosong</span>')
        $('#'+modalDetailEmas+' #status_aktif').html('<span class="badge badge-danger">Kosong</span>')
        $('#'+modalDetailEmas+' #foto_emas').attr('src', base_url('assets/gambar/default/minigold.jpg'))
    })

    $(function(){
        $('#waktu').bootstrapMaterialDatePicker({
			format: 'YYYY-MM-DD',
			weekStart: 0,
			time: false
		}).on('change', function(e, date) {
		});
    })

    $('#'+modalEmas+' #ukuran_emas').on('change', function(e){
        startloading('#'+modalEmas+' .modal-content')

        $.ajax({
            url: base_url('tabel_harga_emas/ambilDataJenis/'+ $('#'+modalEmas+' #ukuran_emas').val()) ,
            dataType: "json",
            cache: false,
            success: function(res) {
                if(res.id_jenis_emas == '1' || res.id_jenis_emas == '2' || res.id_jenis_emas == '4' ){
                    $('#'+modalEmas+' .input-emas').css({'display': 'block'})
                    $('#'+modalEmas+' .input-antam').css({'display': 'none'})
                } else if(res.id_jenis_emas == '3') {
                    $('#'+modalEmas+' .input-emas').css({'display': 'none'})
                    $('#'+modalEmas+' .input-antam').css({'display': 'block'})
                }
                stoploading('#'+modalEmas+' .modal-content')
            }
        });
    })

    $('.btn-tambah-harga-emas').on('click', function(){
        $('#'+modalEmas+' .modal-title').html('Form Input Emas')
        $('#'+modalEmas+' #formEmas').attr('action', base_url('tabel_harga_emas/input_action'))
    })

    function editEmas(id){
        startloading('#'+modalEmas+' .modal-content')
        $('#'+modalEmas+' .modal-title').html('Edit Harga Emas')
        $('#'+modalEmas+' #formEmas').attr('action', base_url('tabel_harga_emas/edit_action/'+id))
        $.ajax({
            url: base_url('tabel_harga_emas/api_v_harga_emas/'+id) ,
            dataType: "json",
            cache: false,
            success: function(res) {
                $('#'+modalEmas+' #ukuran_emas').val(res.id_ukuran_emas)
                $('#'+modalEmas+' #waktu').val(res.waktu)
                $('#'+modalEmas+' #harga_distributor').val(res.harga_distributor)
                $('#'+modalEmas+' #harga_jual_bs').val(res.harga_jualBS)
                $('#'+modalEmas+' #harga_jual_nasabah').val(res.harga_jualNasabah)
                if(res.id_jenis_emas == '1' || res.id_jenis_emas == '2' || res.id_jenis_emas == '4'){
                    $('#'+modalEmas+' .input-emas').css({'display':'block'})
                    $('#'+modalEmas+' .input-antam').css({'display':'none'})
                    $('#'+modalEmas+' #buyback').val(res.buyback)
                    $('#'+modalEmas+' .foto_emas').css({'display': 'none'})
                    $('#'+modalEmas+' .foto_emas_edit').css({'display': 'block'})
                } else {
                    $('#'+modalEmas+' .input-emas').css({'display':'none'})
                    $('#'+modalEmas+' .input-antam').css({'display':'block'})
                    if(res.jenis_sertifikat == '2'){
                        $('#'+modalEmas+' #buyback_serti_lama').val(res.buyback)
                        $('#'+modalEmas+' .buyback-serti-eye').css({'display':'none'})
                        $('#'+modalEmas+' .file-serti-eye').css({'display':'none'})
                        $('#'+modalEmas+' .file-serti-eye-edit').css({'display':'none'})
                        $('#'+modalEmas+' .buyback-serti-lama').css({'display':'block'})
                        $('#'+modalEmas+' .file-serti-lama').css({'display':'none'})
                        $('#'+modalEmas+' .file-serti-lama-edit').css({'display':'block'})
                    } else if(res.jenis_sertifikat == '1'){
                        $('#'+modalEmas+' #buyback_serti_eye').val(res.buyback)
                        $('#'+modalEmas+' .buyback-serti-eye').css({'display':'block'})
                        $('#'+modalEmas+' .file-serti-eye').css({'display':'none'})
                        $('#'+modalEmas+' .file-serti-eye-edit').css({'display':'block'})
                        $('#'+modalEmas+' .buyback-serti-lama').css({'display':'none'})
                        $('#'+modalEmas+' .file-serti-lama').css({'display':'none'})
                        $('#'+modalEmas+' .file-serti-lama-edit').css({'display':'none'})
                    }
                }
                var stok = ''
                if(res.status_stok == '0' || res.status_stok == ''){
                    stok = '0'
                } else {
                    stok = '1'
                }
                $('#'+modalEmas+' #statusStok').val(stok)
                $('#'+modalEmas+' #keterangan').val(res.keterangan)
                stoploading('#'+modalEmas+' .modal-content')
            }
        });
    }

    function detailEmas(id){
        startloading('#'+modalDetailEmas+' .modal-content')
        $('#'+modalDetailEmas+' .modal-title').html('Detail Harga Emas')
        $.ajax({
            url: base_url('tabel_harga_emas/api_v_harga_emas/'+id) ,
            dataType: "json",
            cache: false,
            success: function(res) {
                $('#'+modalDetailEmas+' #ukuran_keping').html(res.ukuran_keping+' Gram')
                $('#'+modalDetailEmas+' #jenis_emas').html(res.jenis_emas)
                $('#'+modalDetailEmas+' #waktu').html(res.waktu)
                $('#'+modalDetailEmas+' #harga_distributor').html(formatKoin(res.harga_distributor))
                $('#'+modalDetailEmas+' #harga_jual_bs').html(formatKoin(res.harga_jualBS))
                $('#'+modalDetailEmas+' #harga_jual_nasabah').html(formatKoin(res.harga_jualNasabah))
                $('#'+modalDetailEmas+' #buyback').html(formatKoin(res.buyback))
                let sertifikat = ''
                if(res.jenis_sertifikat == '2'){
                    sertifikat = '<span class="badge badge-primary">Sertifikat Lama</span>'
                } else {
                    sertifikat = '<span class="badge badge-primary">Sertifikat Eye</span>'
                }
                $('#'+modalDetailEmas+' #jenis_sertifikat').html(sertifikat)
                let status = ''
                if(res.status_aktif == '1'){
                    status = '<span class="badge badge-success">Aktif</span>'
                } else {
                    status = '<span class="badge badge-danger">Tidak Aktif</span>'
                }
                $('#'+modalDetailEmas+' #status_aktif').html(status)
                $('#'+modalDetailEmas+' #keterangan').html(res.keterangan)
                let gambar = ''
                if(res.gambar == null || res.gambar == ''){
                    gambar = base_url('assets/gambar/default/minigold.jpg')
                } else {
                    gambar = res.gambar
                }
                $('#'+modalDetailEmas+' #foto_emas').attr('src', gambar)
                stoploading('#'+modalDetailEmas+' .modal-content')
            }
        });
    }

    function deleteEmas(id){
        delConf(base_url('tabel_harga_emas/hapus/'+id))
    }

    function formEmasValidation(id){
        $('#'+modalEmas+' #'+id).validate({
            rules: {
                ukuran_emas: {
                    required: true
                },
                waktu: {
                    required: true
                },
                harga_distributor: {
                    required: true,
                    number: true,
                },
                harga_jual_nasabah: {
                    required: true,
                    number: true
                },
                harga_jual_bs: {
                    required: true,
                    number: true
                },
                buyback: {
                    required: true,
                    number: true
                },
                buyback_serti_eye: {
                    required: true,
                    number: true,
                },
                buyback_serti_lama: {
                    required: true,
                    number: true,
                },
                serti_eye: {
                    required: true
                },
                serti_lama: {
                    required: true
                },
                statusStok: {
                    required: true
                },
                keterangan: {
                    required: true
                },
                foto_emas: {
                    required: true
                }
            },
            messages: {
                ukuran_emas: {
                    required: "Data tidak boleh kosong"
                },
                waktu: {
                    required: "Data tidak boleh kosong"
                },
                harga_distributor: {
                    required: "Data tidak boleh kosong",
                    min: "Harga tidak boleh kurang dari buyback"
                },
                harga_jual_nasabah: {
                    required: "Data tidak boleh kosong"
                },
                harga_jual_bs: {
                    required: "Data tidak boleh kosong"
                },
                buyback: {
                    required: "Data tidak boleh kosong",
                },
                buyback_serti_eye: {
                    required: "Data tidak boleh kosong",
                },
                buyback_serti_lama: {
                    required: "Data tidak boleh kosong",
                },
                serti_eye: {
                    required: "Data tidak boleh kosong"
                },
                serti_lama: {
                    required: "Data tidak boleh kosong"
                },
                statusStok: {
                    required: "Status stok tidak boleh kosong"
                },
                keterangan: {
                    required: "Data tidak boleh kosong"
                },
                foto_emas: {
                    required: "Foto tidak boleh kosong"
                }
            },
            errorElement: 'span',
            errorPlacement: function(error, element) {
                error.addClass('invalid-feedback');
                element.closest('.form-group').append(error);
            },
            highlight: function(element, errorClass, validClass) {
                $(element).addClass('is-invalid');
            },
            unhighlight: function(element, errorClass, validClass) {
                $(element).removeClass('is-invalid');
            },
            submitHandler: function(form) { 
                let harga_distributor= 0
                let harga_nasabah    = 0

                if($('#'+modalEmas+' #harga_jual_nasabah').val() == ''){
                    harga_nasabah = 0
                } else {
                    harga_nasabah = parseInt($('#'+modalEmas+' #harga_jual_nasabah').val())
                }

                if($('#'+modalEmas+' #harga_distributor').val() == ''){
                    harga_distributor = 0
                } else {
                    harga_distributor = parseInt($('#'+modalEmas+' #harga_distributor').val())
                }

                if(harga_nasabah < harga_distributor){
                    CekKonfirmasi('Harga nasabah tidak boleh kurang dari harga distributor', '')
                } else {
                    form.submit()        
                }
            }

        })
    }

    function cekStatus(id){
        startloading('body')
        $.ajax({
            url: base_url('tabel_harga_emas/edit_status_harga/'+id) ,
            dataType: "json",
            cache: false,
            success: function(res) {
                CekKonfirmasi('', 'Berhasil mengubah data')
                stoploading('body')
            }
        });
    }

    function stockAction(id){
        startloading('body')
        $.ajax({
            url: base_url('tabel_harga_emas/edit_status_stock/'+id) ,
            dataType: "json",
            cache: false,
            success: function(res) {
                CekKonfirmasi('', 'Berhasil mengubah data')
                stoploading('body')
            },
            error: function (request, status, error) {
                alert(request.responseText);
            }
        });
    }

</script>