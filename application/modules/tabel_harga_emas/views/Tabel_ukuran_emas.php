<div class="row">
	<section class="col-lg connectedSortable ui-sortable">
		<div class="card card-info">
			<div class="card-header">
				<div class="card-title">
					<?php echo $title_card ?>
				</div>
			</div>
			<div class="card-body">
				<div class="tab-content p-0">
                <?php $this->load->view('tabel_harga_emas/menu_button'); ?>
					<div class="row">
						<div class="col-md-5 mb-2">
                            <button class="btn btn-sm btn-<?= bgCard() ?> btn-tambah-ukuran" data-toggle="modal" data-target="#modalUkuran">Tambah Data</button>
						</div>
						<div class="col-md-3 offset-md-4 mb-2">
							<?= search(site_url('biller_pengepul/index'), site_url('biller_pengepul'), $q) ?>
						</div>
					</div>
					<div class="tab-content p-0" style="overflow:auto">
						<table class="table table-bordered table-striped table-condensed table-hover table-sm" style="margin-bottom: 10px">
							<tr>
								<th class="text-center" width="50px">No</th>
								<th class="text-center">Jenis Emas</th>
								<th class="text-center">Ukuran Keping</th>
								<th class="text-center">Status Aktif</th>
								<th class="text-center">Aksi</th>
							</tr><?php
									if ($total_rows == 0) {
										echo '<tr><td colspan="12" bgcolor="grey" style="color: white; font-weight: bold; text-align: center;">Data Tidak Ditemukan</td></tr>';
									} else {
										foreach ($tabel_ukuran as $k) { ?>
									<tr>
										<td class="text-center"><?php echo ++$start ?></td>
										<td class="text-center"><?= $k->jenis_emas ?></td>
										<td class="text-center"><?= $k->ukuran_keping ?></td>
										<td class="text-center">
                                            <?php if($k->status_aktif == '1'){
                                                echo '<span class="badge badge-success">Aktif</span>';
                                            } else {
                                                echo '<span class="badge badge-success">Tidak Aktif</span>';
                                            } ?>
                                        </td>
                                        <td class="text-center">
                                            <div class="btn-group btn-group-sm">
                                                <button class="btn btn-sm btn-info" data-toggle="modal" data-target="#modalDetailUkuran" onclick="detailUkuran('<?= $k->id_ukuran_emas ?>')"><i class="fas fa-info-circle"></i></button>
                                                <button class="btn btn-sm btn-success" data-toggle="modal" data-target="#modalUkuran" onclick="editUkuran('<?= $k->id_ukuran_emas ?>')"><i class="fas fa-edit"></i></button>
                                            </div>
                                        </td>
									</tr>
							<?php }
									} ?>
						</table>
					</div>
					<?= footer($total_rows, $pagination, '') ?>
				</div>
			</div>
		</div>
	</section>
</div>

<div id="modalUkuran" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-md" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title"></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body text-sm">
                <form action="" method="post" id="formUkuran" role="form" enctype="multipart/form-data">
                <div class="row px-3">
                    <div class="col-12">
                        <div class="form-group">
                            <label for="">Jenis Emas</label>
                            <select name="jenis_emas" id="jenis_emas" class="form-control form-control-sm">
                                <option value="">Pilih jenis emas</option>
                                <?php foreach ($jenis_emas as $data){ ?>
								<option value="<?php echo $data->id_jenis_emas; ?>">
								<?php echo $data->jenis_emas; ?>
								</option>
								<?php }?>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="ukuran">Ukuran</label>
                            <input type="number" step="0.01" class="form-control form-control-sm" name="ukuran" id="ukuran" placeholder="0" value="" />
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <div class="col-md-12 text-center">
                    <button type="submit" class="btn btn-action-modal btn-sm btn-<?= bgCard() ?>">Simpan</button>
                    <button type="button" class="btn btn-action-modal btn-sm btn-secondary" data-dismiss="modal">Batal</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="modalDetailUkuran" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-md" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title"></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body text-sm">
                <div class="row px-3">
                    <div class="col-12">
                        <table class="table table-sm table-borderless">
                            <tr>
                                <td class='font-weight-bold'>Jenis Emas</td>
                                <td>:</td>
                                <td id="detail_jenis"><?= kosong() ?></td>
                            </tr>
                            <tr>
                                <td class='font-weight-bold'>Ukuran Emas</td>
                                <td>:</td>
                                <td id="detail_ukuran"><?= kosong() ?></td>
                            </tr>
                            <tr>
                                <td class='font-weight-bold'>Status</td>
                                <td>:</td>
                                <td id="detail_status"><?= kosong() ?></td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <div class="col-md-12 text-center">
                    <button type="button" class="btn btn-action-modal btn-sm btn-secondary" data-dismiss="modal">Kembali</button>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    const modalUkuran = 'modalUkuran'
    const modalDetailUkuran = 'modalDetailUkuran'
    const formUkuran = 'formUkuran'

    $(function(){
        ukuranValidation(formUkuran)
    })

    $('#'+modalUkuran).on('hidden.bs.modal', function(){
        $('#'+modalUkuran+' #jenis_emas').val('')
        $('#'+modalUkuran+' #ukuran').val('')
        $('#'+modalUkuran+' #jenis_emas').removeClass('is-invalid')
        $('#'+modalUkuran+' #ukuran').removeClass('is-invalid')
    })

    $('#'+modalDetailUkuran).on('hidden.bs.modal', function(){
        $('#'+modalDetailUkuran+' #detail_jenis').html('<span class="badge badge-danger">Kosong</span>')
        $('#'+modalDetailUkuran+' #detail_ukuran').html('<span class="badge badge-danger">Kosong</span>')
        $('#'+modalDetailUkuran+' #detail_status').html('<span class="badge badge-danger">Kosong</span>')
    })

    $('.btn-tambah-ukuran').on('click', function(){
        $('#'+modalUkuran+' .modal-title').html('Form Tambah Ukuran')
        $('#'+modalUkuran+' #'+formUkuran).attr('action', base_url('tabel_harga_emas/input_ukuran'))
    })

    function detailUkuran(id){
        startloading('#'+modalDetailUkuran+' .modal-content')
        $('#'+modalDetailUkuran+' .modal-title').html('Detail Ukuran Emas')
        $.ajax({
            url: base_url('tabel_harga_emas/ukuran/'+id) ,
            dataType: "json",
            cache: false,
            success: function(res) {
                $('#'+modalDetailUkuran+' #detail_jenis').html(res.jenis_emas)
                $('#'+modalDetailUkuran+' #detail_ukuran').html(res.ukuran_keping)
                let status = ''
                if(res.status_aktif == '1'){
                    status = '<span class="badge badge-success">Aktif</span>';
                } else {
                    status = '<span class="badge badge-danger">Tidak Aktif</span>';
                }
                $('#'+modalDetailUkuran+' #detail_status').html(status)
                stoploading('#'+modalDetailUkuran+' .modal-content')
            }
        });
    }

    function editUkuran(id){
        startloading('#'+modalUkuran+' .modal-content')
        $('#'+modalUkuran+' .modal-title').html('Form Edit Ukuran')
        $('#'+modalUkuran+' #'+formUkuran).attr('action', base_url('tabel_harga_emas/edit_ukuran/'+id))
        $.ajax({
            url: base_url('tabel_harga_emas/ukuran/'+id) ,
            dataType: "json",
            cache: false,
            success: function(res) {
                $('#'+modalUkuran+' #jenis_emas').val(res.id_jenis_emas)
                $('#'+modalUkuran+' #ukuran').val(res.ukuran_keping)
                stoploading('#'+modalUkuran+' .modal-content')
            }
        });
    }

    function ukuranValidation(id){
        $('#'+modalUkuran+' #'+id).validate({
            rules: {
                jenis_emas: {
                    required: true
                },
                ukuran: {
                    required: true
                }
            },
            messages: {
                jenis_emas: {
                    required: 'Jenis tidak boleh kosong'
                },
                ukuran: {
                    required: 'Ukuran tidak boleh kosong'
                }
            },
            errorElement: 'span',
            errorPlacement: function(error, element) {
                error.addClass('invalid-feedback');
                element.closest('.form-group').append(error);
            },
            highlight: function(element, errorClass, validClass) {
                $(element).addClass('is-invalid');
            },
            unhighlight: function(element, errorClass, validClass) {
                $(element).removeClass('is-invalid');
            }
        })
    }
</script>
