<?php

if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class Tabel_harga_emas_model extends CI_Model
{

	public $table = 'tabel_harga_emas';
	public $id = 'id_harga_emas';
	public $order = 'DESC';

	function __construct()
	{
		parent::__construct();
	}

	// get all
	function get_all()
	{
		// $this->db->order_by($this->id, $this->order);
		// return $this->db->get($this->table)->result();
		$query = $this->db->query('SELECT tabel_harga.id_tabel_harga,kategori_sampah.nama_kategori_sampah,jenis_sampah.nama_jenis_sampah,tabel_harga.harga_nasional FROM tabel_harga 
        LEFT JOIN kategori_sampah ON kategori_sampah.id_kategori_sampah=tabel_harga.id_kategori_sampah
        LEFT JOIN jenis_sampah ON jenis_sampah.id_jenis_sampah=tabel_harga.id_jenis_sampah');

		return $query->result();
	}

	function get_kategori_all()
	{
		$this->db->select('kategori_sampah.*');
		$this->db->join('tabel_harga', 'tabel_harga.id_kategori_sampah = kategori_sampah.id_kategori_sampah', 'left outer');
		$this->db->where('tabel_harga.id_tabel_harga', NULL);
		$this->db->where('kategori_sampah.status_kategori_sampah_remove', 1);

		return $this->db->get('kategori_sampah')->result();
	}
	function get_kategori()
	{
		$this->db->select('kategori_sampah.*');
		$this->db->where('kategori_sampah.status_kategori_sampah_remove', 1);

		return $this->db->get('kategori_sampah')->result();
	}
	function get_jenis()
	{
		$this->db->select('jenis_sampah.*');
		$this->db->where('jenis_sampah.status_jenis_sampah_remove', 1);

		return $this->db->get('jenis_sampah')->result();
	}
	function get_provinsi()
	{

		return $this->db->get('provinsi')->result();
	}

	function get_provinsi_all($id)
	{
		$this->db->select('provinsi.*');
		$this->db->join('tabel_harga_detail', '(tabel_harga_detail.id_provinsi = provinsi.id_provinsi and tabel_harga_detail.id_tabel_harga="' . $id . '")', 'left outer');
		$this->db->where('tabel_harga_detail.id_tbl_harga_detail', NULL);
		return $this->db->get('provinsi')->result();
	}
	function get_cek_all()
	{
		// $this->db->select('provinsi.*');
		// $this->db->join('tabel_harga_detail', 'tabel_harga_detail.id_provinsi = provinsi.id_provinsi','left outer');
		// $this->db->where('tabel_harga_detail.id_tbl_harga_detail', NULL);
		return $this->db->get('provinsi')->result();
	}

	public function get_by_prefix($prefix = '')
	{
		# code..
		$query = $this->db->query("SELECT max(" . $this->id . ") AS max FROM " . $this->table . " WHERE " . $this->id . " LIKE '$prefix%'");
		return $query->row();
	}
	// get data by id
	function get_by_id($id)
	{
		//     $this->db->where($this->id, $id);
		//     return $this->db->get($this->table)->row();
		$query = $this->db->query('SELECT tabel_harga.id_tabel_harga,tabel_harga.id_kategori_sampah,kategori_sampah.nama_kategori_sampah,tabel_harga.harga_nasional, jenis_sampah.* FROM tabel_harga 
        LEFT JOIN kategori_sampah ON kategori_sampah.id_kategori_sampah=tabel_harga.id_kategori_sampah
        LEFT JOIN jenis_sampah ON jenis_sampah.id_jenis_sampah=tabel_harga.id_jenis_sampah 
        where tabel_harga.id_tabel_harga="' . $id . '"');


		return $query->row();
	}
	function get_by_id_detail($id)
	{
		$this->db->select('tabel_harga_detail.*,provinsi.id_provinsi');
		// $this->db->join('tabel_harga', 'tabel_harga.id_tabel_harga = tabel_harga_detail.id_tabel_harga_detail');
		$this->db->join('provinsi', 'tabel_harga_detail.id_provinsi = provinsi.id_provinsi');
		// $this->db->join('jenis_sampah', 'jenis_sampah.id_jenis_sampah = tabel_harga.id_jenis_sampah');

		$this->db->where('id_tbl_harga_detail', $id);

		$this->db->from('tabel_harga_detail');

		return $this->db->get()->row();
	}



	// ------------------------------------------------------------------------
	// Pagination
	// ------------------------------------------------------------------------
	// Edit bagian ini saja :*
	// ------------------------------------------------------------------------
	function _data_ukuran($q = null)
	{
		$this->db->join('jenis_emas', 'ukuran_emas.id_jenis_emas = jenis_emas.id_jenis_emas');
		$this->db->where('ukuran_emas.status_remove', 1);
		// $this->db->group_start();
		// $this->db->or_like('ukuran_emas.ukuran_keping', $q);
		// $this->db->group_end();
	}
	// get total rows
	function total_rows_ukuran($q = NULL)
	{
		$this->_data_ukuran($q);
		$this->db->from('ukuran_emas');
		return $this->db->count_all_results();
	}
	function get_limit_data_ukuran($limit, $start = 0, $q = NULL)
	{
		$this->db->order_by('id_ukuran_emas', $this->order);
		$this->db->select('*');
		$this->db->from('ukuran_emas');
		$this->_data_ukuran($q);
		$this->db->limit($limit, $start);
		return $this->db->get()->result();
	}
	// ------------------------------------------------------------------------
	// End Pagination
	// ------------------------------------------------------------------------








	// ------------------------------------------------------------------------
	// Pagination
	// ------------------------------------------------------------------------
	// Edit bagian ini saja :*
	// ------------------------------------------------------------------------
	function _data($q = null)
	{
		$this->db->join('ukuran_emas', $this->table . '.id_ukuran_emas = ukuran_emas.id_ukuran_emas');
		$this->db->join('jenis_emas', 'ukuran_emas.id_jenis_emas = jenis_emas.id_jenis_emas');
		$this->db->where($this->table . '.status_remove', 1);
		$this->db->group_start();
		$this->db->or_like('ukuran_emas.ukuran_keping', $q);
		$this->db->group_end();
	}

	function total_rows($q = NULL)
	{
		$this->_data($q);
		$this->db->from($this->table);
		return $this->db->count_all_results();
	}
	// get data with limit and search
	function get_limit_data($limit, $start = 0, $q = NULL)
	{
		$this->db->select("$this->table.*,ukuran_emas.ukuran_keping,jenis_emas.jenis_emas");
		$this->db->order_by($this->table.'.waktu', 'DESC');
		$this->db->from($this->table);
		$this->_data($q);
		$this->db->limit($limit, $start);
		return $this->db->get()->result();
	}
	// ------------------------------------------------------------------------
	// End Pagination
	// ------------------------------------------------------------------------








	// ------------------------------------------------------------------------
	// Pagination
	// ------------------------------------------------------------------------
	// Edit bagian ini saja :*
	// ------------------------------------------------------------------------
	function _data_detail($q = null, $id = null)
	{
		// $this->db->join('tabel_harga', 'tabel_harga.id_tabel_harga = tabel_harga_detail.id_tabel_harga_detail');
		$this->db->join('provinsi', 'tabel_harga_detail.id_provinsi = provinsi.id_provinsi');
		// $this->db->join('jenis_sampah', 'jenis_sampah.id_jenis_sampah = tabel_harga.id_jenis_sampah');

		$this->db->where('id_tabel_harga', $id);
		$this->db->group_start();
		$this->db->or_like('provinsi.nama_provinsi', $q);
		// $this->db->or_like('id_jenis_sampah', $q);
		$this->db->or_like('harga_provinsi', $q);
		$this->db->group_end();
	}
	// get total rows
	function total_rows_detail($q = NULL, $id)
	{
		$this->db->select('*');
		$this->_data_detail($q, $id);
		$this->db->from('tabel_harga_detail');
		return $this->db->count_all_results();
	}
	// get data with limit and search
	function get_limit_data_detail($limit, $start = 0, $q = NULL, $id)
	{
		$this->db->select('*');
		$this->db->from('tabel_harga_detail');
		$this->_data_detail($q, $id);
		$this->db->limit($limit, $start);
		return $this->db->get()->result();
	}
	// ------------------------------------------------------------------------
	// End Pagination
	// ------------------------------------------------------------------------





	// insert data
	function insert($data)
	{
		return $this->db->insert($this->table, $data);
	}
	// insert data
	function insertdetail($data)
	{
		return $this->db->insert('tabel_harga_detail', $data);
	}

	// update data
	function update($id, $data)
	{
		$this->db->where($this->id, $id);
		$this->db->update($this->table, $data);
	}
	// update data
	function updatedetail($id, $data)
	{
		$this->db->where('id_tbl_harga_detail', $id);
		return $this->db->update('tabel_harga_detail', $data);
	}

	// delete data
	function delete($id)
	{
		$this->db->where('id_tbl_harga_detail', $id);
		return $this->db->delete('tabel_harga_detail');
	}
}

/* End of file Tabel_harga_model.php */
/* Location: ./application/models/Tabel_harga_model.php */
/* Please DO NOT modify this information : */
/* Generated by Harviacode Codeigniter CRUD Generator 2020-02-10 09:13:21 */
/* http://harviacode.com */
