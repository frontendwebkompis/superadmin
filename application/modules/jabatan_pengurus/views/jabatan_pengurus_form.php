<div class="row">
	<section class="col-lg connectedSortable ui-sortable">
		<div class="card card-info">
            <div class="card-header">
                <div class="card-title">
                  <?php echo $title_card?>
                </div>
            </div>
			<div class="card-body">
				<div class="tab-content p-0">

        <form action="<?php echo $action; ?>" method="post" id="dataform">
	    <div class="form-group">
            <label for="varchar">Nama Jabatan Pengurus <?php echo form_error('nama_jabatan_pengurus') ?></label>
            <input type="text" class="form-control" name="nama_jabatan_pengurus" id="nama_jabatan_pengurus" placeholder="Nama Jabatan Pengurus" value="<?php echo $nama_jabatan_pengurus; ?>" />
        </div>
	    <input type="hidden" name="id_jabatan_pengurus" value="<?php echo $id_jabatan_pengurus; ?>" /> 
	    <?php
			if($button == 'Create'){ ?>
				<button type="submit" class="btn btn-info" style="width: 100px;">Tambah</button> 
			<?php } else { ?>
				<button type="submit" class="btn btn-info" style="width: 100px;">Simpan</button> 
		<?php
			}
		?>
	    <a href="<?php echo site_url('jabatan_pengurus') ?>" class="btn btn-default" style="width: 100px;">Batal</a>
		</form>
    	</div>
		</div>
	</section>
</div>

<script type="text/javascript">
	    $(document).ready(function () {
          $('#dataform').validate({
            rules: {
              nama_jabatan_pengurus: {
                required: true,
              }
            },
            messages: {
              nama_jabatan_pengurus: {
                required: "Nama Jabatan Tidak Boleh Kosong",
              }
            },
            errorElement: 'span',
            errorPlacement: function (error, element) {
              error.addClass('invalid-feedback');
              element.closest('.form-group').append(error);
            },
            highlight: function (element, errorClass, validClass) {
              $(element).addClass('is-invalid');
            },
            unhighlight: function (element, errorClass, validClass) {
              $(element).removeClass('is-invalid');
            },
            

          });
        });
</script>
