<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Jabatan_pengurus extends MX_Controller
{
    function __construct()
    {
        parent::__construct();
        if (cek_token()) {
            $this->load->model('Jabatan_pengurus_model');
            $this->load->library('form_validation');
        } else {
            logout();
        }
    }

    public function index()
    {
        $q = urldecode($this->input->get('q', TRUE));
        $start = intval($this->input->get('start'));

        if ($q <> '') {
            $config['base_url']     = base_url() . 'jabatan_pengurus?q=' . urlencode($q);
            $config['first_url']    = base_url() . 'jabatan_pengurus?q=' . urlencode($q);
        } else {
            $config['base_url']     = base_url() . 'jabatan_pengurus';
            $config['first_url']    = base_url() . 'jabatan_pengurus';
        }

        $config['per_page']             = 5;
        $config['page_query_string']    = TRUE;
        $config['total_rows']           = $this->Jabatan_pengurus_model->total_rows($q);
        $jabatan_pengurus               = $this->Jabatan_pengurus_model->get_limit_data($config['per_page'], $start, $q);

        $this->load->library('pagination');
        $this->pagination->initialize($config);

        $data = array(
            'tittle'                => 'Jabatan',
            'judul'                 => 'Jabatan',
            'title_card'            => 'List Jabatan',
            'jabatan_pengurus_data' => $jabatan_pengurus,
            'q'                     => $q,
            'pagination'            => $this->pagination->create_links(),
            'total_rows'            => $config['total_rows'],
            'start'                 => $start,
            'jabatan_aktif'         => '1',
            'menu_aktif'            => 'jabatan_pengurus'
        );
        $res['datakonten'] = $this->load->view('jabatan_pengurus/jabatan_pengurus_list', $data, true);
        $this->load->view('layouts/main_view', $res);
    }

    public function read($id)
    {
        $row = $this->Jabatan_pengurus_model->get_by_id($id);
        if ($row) {
            $data = array(
                'tittle'                    => 'Jabatan',
                'judul'                     => 'Jabatan List',
                'id_jabatan_pengurus'       => $row->id_jabatan_pengurus,
                'nama_jabatan_pengurus'     => $row->nama_jabatan_pengurus,
                'jabatan_aktif'             => '1',
                'menu_aktif'                => 'jabatan_pengurus'
            );
            $res['datakonten'] = $this->load->view('jabatan_pengurus/jabatan_pengurus_read', $data, true);
            $this->load->view('layouts/main_view', $res);
        } else {
            $this->session->set_flashdata('gagal', 'Record Not Found');
            redirect(site_url('jabatan_pengurus'));
        }
    }

    public function create()
    {
        $data = array(
            'tittle'                    => 'Jabatan',
            'judul'                     => 'Jabatan',
            'title_card'                => 'Tambah Data Jabatan',
            'button'                    => 'Create',
            'action'                    => site_url('jabatan_pengurus/create_action'),
            'id_jabatan_pengurus'       => set_value('id_jabatan_pengurus'),
            'nama_jabatan_pengurus'     => set_value('nama_jabatan_pengurus'),
            'jabatan_aktif'             => '1',
            'menu_aktif'                => 'jabatan_pengurus'
        );
        $res['datakonten'] = $this->load->view('jabatan_pengurus/jabatan_pengurus_form', $data, true);
        $this->load->view('layouts/main_view', $res);
    }

    public function create_action()
    {
        $this->_rules();
        $dt = new DateTime();
       
        if ($this->form_validation->run() == FALSE) {
            $this->create();
        } else {
            $data = array(
                'nama_jabatan_pengurus'             => $this->input->post('nama_jabatan_pengurus', TRUE),
                'id_superadmin'                     =>getID(),
                'status_jabatan_pengurus_remove'    => 1,
                'created_at'        => $dt->format('Y-m-d H:i:s'),

            );
            $this->Jabatan_pengurus_model->insert($data);
            $this->session->set_flashdata('berhasil', 'Create Record Success');
            redirect(site_url('jabatan_pengurus'));
        }
    }

    public function update($id)
    {
        $row = $this->Jabatan_pengurus_model->get_by_id($id);

        if ($row) {
            $data = array(
                'tittle'                    => 'Jabatan',
                'judul'                     => 'Jabatan',
                'title_card'                => 'Update Jabatan',
                'button'                    => 'Update',
                'action'                    => site_url('jabatan_pengurus/update_action'),
                'id_jabatan_pengurus'       => set_value('id_jabatan_pengurus', $row->id_jabatan_pengurus),
                'nama_jabatan_pengurus'     => set_value('nama_jabatan_pengurus', $row->nama_jabatan_pengurus),
                'jabatan_aktif'             => '1',
                'menu_aktif'                => 'jabatan_pengurus'
            );
            $res['datakonten'] = $this->load->view('jabatan_pengurus/jabatan_pengurus_form', $data, true);
            $this->load->view('layouts/main_view', $res);
        } else {
            $this->session->set_flashdata('gagal', 'Record Not Found');
            redirect(site_url('jabatan_pengurus'));
        }
    }

    public function update_action()
    {
        $this->_rules();
        $dt = new DateTime();
       
        if ($this->form_validation->run() == FALSE) {
            $this->update($this->input->post('id_jabatan_pengurus', TRUE));
        } else {
            $data = array(
                'nama_jabatan_pengurus' => $this->input->post('nama_jabatan_pengurus', TRUE),
                'id_superadmin' => getID(),
                'update_at'        => $dt->format('Y-m-d H:i:s'),
            );
            $this->Jabatan_pengurus_model->update($this->input->post('id_jabatan_pengurus', TRUE), $data);
            $this->session->set_flashdata('berhasil', 'Update Record Success');
            redirect(site_url('jabatan_pengurus'));
        }
    }

    public function del_sem($id)
    {
        $row = $this->Jabatan_pengurus_model->get_by_id($id);
           $dt = new DateTime();
        if ($row) {
            $data = array(
                'status_jabatan_pengurus_remove' => 0,
                'id_superadmin' => getID(),
                'update_at'        => $dt->format('Y-m-d H:i:s'),
            );
            $this->Jabatan_pengurus_model->update($id, $data);
            $this->session->set_flashdata('berhasil', 'Delete Record Success');
            redirect(site_url('jabatan_pengurus'));
        } else {
            $this->session->set_flashdata('gagal', 'Record Not Found');
            redirect(site_url('jabatan_pengurus'));
        }
    }
   

    public function _rules()
    {
        $this->form_validation->set_rules('nama_jabatan_pengurus', 'nama jabatan pengurus', 'trim|required');
        $this->form_validation->set_rules('id_jabatan_pengurus', 'id_jabatan_pengurus', 'trim');
        $this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

    public function excel()
    {
        $this->load->helper('exportexcel');
        $namaFile = "jabatan_pengurus.xls";
        $judul = "jabatan_pengurus";
        $tablehead = 0;
        $tablebody = 1;
        $nourut = 1;
        //penulisan header
        header("Pragma: public");
        header("Expires: 0");
        header("Cache-Control: must-revalidate, post-check=0,pre-check=0");
        header("Content-Type: application/force-download");
        header("Content-Type: application/octet-stream");
        header("Content-Type: application/download");
        header("Content-Disposition: attachment;filename=" . $namaFile . "");
        header("Content-Transfer-Encoding: binary ");

        xlsBOF();

        $kolomhead = 0;
        xlsWriteLabel($tablehead, $kolomhead++, "No");
        xlsWriteLabel($tablehead, $kolomhead++, "Nama Jabatan Pengurus");

        foreach ($this->Jabatan_pengurus_model->get_all() as $data) {
            $kolombody = 0;

            //ubah xlsWriteLabel menjadi xlsWriteNumber untuk kolom numeric
            xlsWriteNumber($tablebody, $kolombody++, $nourut);
            xlsWriteLabel($tablebody, $kolombody++, $data->nama_jabatan_pengurus);

            $tablebody++;
            $nourut++;
        }

        xlsEOF();
        exit();
    }
}

/* End of file Jabatan_pengurus.php */
/* Location: ./application/controllers/Jabatan_pengurus.php */
