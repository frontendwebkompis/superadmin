<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Jenis_sampah extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        if (cek_token()) {
            $this->load->model('Jenis_sampah_model');
            $this->load->library('form_validation');
            $this->load->library('upload');
        } else {
            logout();
        }
    }

    public function index()
    {
        $q = urldecode($this->input->get('q', TRUE));
        $start = intval($this->input->get('start'));

        if ($q <> '') {
            $config['base_url'] = base_url() . 'jenis_sampah/?q=' . urlencode($q);
            $config['first_url'] = base_url() . 'jenis_sampah/?q=' . urlencode($q);
        } else {
            $config['base_url'] = base_url() . 'jenis_sampah/';
            $config['first_url'] = base_url() . 'jenis_sampah/';
        }

        $config['per_page'] = 10;
        $config['page_query_string'] = TRUE;
        $config['total_rows'] = $this->Jenis_sampah_model->total_rows($q);
        $jenis_sampah = $this->Jenis_sampah_model->get_limit_data($config['per_page'], $start, $q);
        $this->load->library('pagination');
        $this->pagination->initialize($config);

        $data = array(
            'tittle'                => 'Jenis Sampah',
            'judul'                 => 'Jenis Sampah',
            'title_card'            => 'List Jenis Sampah',
            'jenis_sampah_data'     => $jenis_sampah,
            'q'                     => $q,
            'pagination'            => $this->pagination->create_links(),
            'total_rows'            => $config['total_rows'],
            'start'                 => $start,
            'jenis_sampah_aktif'    => '1',
            'menu_aktif'            => 'jenis_sampah'
        );
        $res['datakonten'] = $this->load->view('jenis_sampah/jenis_sampah_list', $data, true);
        $this->load->view('layouts/main_view', $res);
    }

    public function read($id)
    {
        $row = $this->Jenis_sampah_model->get_by_id($id);
        if ($row) {
            $q = urldecode($this->input->get('q', TRUE));
            $start = intval($this->input->get('start'));
    
            if ($q <> '') {
                $config['base_url'] = base_url() . 'jenis_sampah/read/'.$id.'?q=' . urlencode($q);
                $config['first_url'] = base_url() . 'jenis_sampah/read/'.$id.'?q=' . urlencode($q);
            } else {
                $config['base_url'] = base_url() . 'jenis_sampah/read/'.$id.'';
                $config['first_url'] = base_url() . 'jenis_sampah/read/'.$id.'';
            }
    
            $config['per_page'] = 10;
            $config['page_query_string'] = TRUE;
            $config['total_rows'] = $this->Jenis_sampah_model->total_rows_merk($q,$id);
            $merk_sampah = $this->Jenis_sampah_model->get_limit_data_merk($config['per_page'], $start, $q,$id);
            $this->load->library('pagination');
            $this->pagination->initialize($config);
    
            $data = array(
                'tittle'                     => 'Jenis Sampah',
                'judul'                     => 'Jenis Sampah',
                'title_card'                => 'Detail Jenis Sampah',
                'id_jenis_sampah'           => $row->id_jenis_sampah,
                'nama_kategori_sampah'      => $row->nama_kategori_sampah,
                'nama_jenis_sampah'         => $row->nama_jenis_sampah,
                'foto_jenis_sampah'         => $row->foto_jenis_sampah,
                'deskripsi_jenis_sampah'    => $row->deskripsi_jenis_sampah,
                'merk_sampah_data'          => $merk_sampah,
                'q'                         => $q,
                'pagination'                => $this->pagination->create_links(),
                'total_rows'                => $config['total_rows'],
                'start'                     => $start,
                'jenis_sampah_aktif'        => '1',
                'menu_aktif'                => 'jenis_sampah'
            );
            $res['datakonten'] = $this->load->view('jenis_sampah/jenis_sampah_read', $data, true);
            $this->load->view('layouts/main_view', $res);
        } else {
            $this->session->set_flashdata('gagal', 'Data Tidak Di Temukan');
            redirect(site_url('jenis_sampah'));
        }
    }

    public function create()
    {
        $data = array(
            'tittle'                    => 'Jenis Sampah',
            'judul'                     => 'Jenis Sampah',
            'title_card'                => 'Tambah Jenis Sampah',
            'button'                    => 'Create',
            'action'                    => site_url('jenis_sampah/create_action'),
            'id_jenis_sampah'           => set_value('id_jenis_sampah'),
            'id_kategori_sampah'        => set_value('id_kategori_sampah'),
            'nama_jenis_sampah'         => set_value('nama_jenis_sampah'),
            'foto_jenis_sampah'         => set_value('foto_jenis_sampah'),
            'deskripsi_jenis_sampah'    => set_value('deskripsi_jenis_sampah'),
            'jenis_sampah_aktif'        => '1',
            'menu_aktif'                => 'jenis_sampah'
        );
        $data['kategori_sampah'] = $this->Jenis_sampah_model->get_kategori_all();
        $res['datakonten'] = $this->load->view('jenis_sampah/jenis_sampah_form', $data, true);
        $this->load->view('layouts/main_view', $res);
    }

    public function create_action()
    {
        $dt = new DateTime();
        $this->_rules();
        $config['upload_path'] = './assets/gambar/jenis_sampah/'; //path folder
        $config['allowed_types'] = 'gif|jpg|png|jpeg|bmp'; //type yang dapat diakses bisa anda sesuaikan
        $config['encrypt_name'] = TRUE; //Enkripsi nama yang terupload
        $prefix = "JNS" . $this->input->post('id_kategori_sampah');
        // echo $gabung;
        $row = $this->Jenis_sampah_model->get_by_prefix($prefix);
        $NoUrut = (int) substr($row->max, 10, 4);
        $NoUrut = $NoUrut + 1; //nomor urut +1
        $NoUrut = sprintf('%04d', $NoUrut);
        $fix = $prefix . $NoUrut;
        $this->upload->initialize($config);
        if (!empty($_FILES['foto_jenis_sampah']['name'])) {

            if ($this->upload->do_upload('foto_jenis_sampah', TRUE)) {
                $gbr = $this->upload->data();
                //Compress Image
                $config['image_library']    = 'gd2';
                $config['source_image']     = './assets/gambar/jenis_sampah/' . $gbr['file_name'];
                $config['create_thumb']     = FALSE;
                $config['maintain_ratio']   = FALSE;
                $config['quality']          = '50%';
                $config['width']            = 600;
                $config['height']           = 400;
                $config['new_image']        = './assets/images/jenis_sampah/' . $gbr['file_name'];
                $this->load->library('image_lib', $config);
                $this->image_lib->resize();

                $upload = s3_upload('jenis_sampah/'.$gbr['file_name'], $gbr['full_path']);
                @unlink('assets/gambar/jenis_sampah/' . $gbr['file_name']);
            $gambar=$upload['ObjectURL'];
            }
        } else {
            echo "Image yang diupload kosong";
        }

        if ($this->form_validation->run() == FALSE) {
            $this->create();
        } else {
            $data = array(
                'id_jenis_sampah'               => $fix,
                'id_superadmin'                 => getID(),
                'status_jenis_sampah_remove'    => 1,
                'id_kategori_sampah'            => $this->input->post('id_kategori_sampah', TRUE),
                'nama_jenis_sampah'             => $this->input->post('nama_jenis_sampah', TRUE),
                'foto_jenis_sampah'             => $gambar,
                'deskripsi_jenis_sampah'        => $this->input->post('deskripsi_jenis_sampah', TRUE),
                'created_at'                    =>  $dt->format('Y-m-d H:i:s'),
            );

            $this->Jenis_sampah_model->insert($data);
            $this->session->set_flashdata('berhasil', 'Berhasil Menambahkan Data');
            redirect(site_url('jenis_sampah'));
        }
    }

    public function update($id)
    {
        $row = $this->Jenis_sampah_model->get_by_id($id);

        if ($row) {
            $data = array(
                'tittle'                    => 'Jenis Sampah',
                'judul'                     => 'Jenis Sampah',
                'title_card'                => 'Update Jenis Sampah',
                'button'                    => 'Update',
                'action'                    => site_url('jenis_sampah/update_action'),
                'id_jenis_sampah'           => set_value('id_jenis_sampah', $row->id_jenis_sampah),
                'id_kategori_sampah'        => set_value('id_kategori_sampah', $row->id_kategori_sampah),
                'nama_jenis_sampah'         => set_value('nama_jenis_sampah', $row->nama_jenis_sampah),
                'foto_jenis_sampah'         => set_value('foto_jenis_sampah', $row->foto_jenis_sampah),
                'deskripsi_jenis_sampah'    => set_value('deskripsi_jenis_sampah', $row->deskripsi_jenis_sampah),
                'jenis_sampah_aktif'        => '1',
                'menu_aktif'                => 'jenis_sampah'
            );
            $data['kategori_sampah'] = $this->Jenis_sampah_model->get_kategori_all();

            $res['datakonten'] = $this->load->view('jenis_sampah/jenis_sampah_form', $data, true);
            $this->load->view('layouts/main_view', $res);
        } else {
            $this->session->set_flashdata('gagal', 'Data Tidak Ditemukan');
            redirect(site_url('jenis_sampah'));
        }
    }

    public function update_action()
    {
        $this->_rules();

        $config['upload_path'] = './assets/gambar/jenis_sampah/'; //path folder
        $config['allowed_types'] = 'gif|jpg|png|jpeg|bmp'; //type yang dapat diakses bisa anda sesuaikan
        $config['encrypt_name'] = TRUE; //Enkripsi nama yang terupload


        $this->upload->initialize($config);
        if (!empty($_FILES['foto_jenis_sampahbaru']['name'])) {

            if ($this->upload->do_upload('foto_jenis_sampahbaru', TRUE)) {
                $gbr = $this->upload->data();
                //Compress Image
                $config['image_library'] = 'gd2';
                $config['source_image'] = './assets/gambar/jenis_sampah/' . $gbr['file_name'];
                $config['create_thumb'] = FALSE;
                $config['maintain_ratio'] = FALSE;
                $config['quality'] = '50%';
                $config['width'] = 600;
                $config['height'] = 400;
                $config['new_image'] = './assets/images/jenis_sampah/' . $gbr['file_name'];
                $this->load->library('image_lib', $config);
                $this->image_lib->resize();
                @unlink('assets/gambar/jenis_sampah/' . $this->input->post('foto_jenis_sampah'));
      del('jenis_sampah/'.basename($this->input->post('foto_jenis_sampah')));
              
                   $upload = s3_upload('jenis_sampah/'.$gbr['file_name'], $gbr['full_path']);
 @unlink('assets/gambar/jenis_sampah/' . $gbr['file_name']);
            $gambar=$upload['ObjectURL'];
            }
        } else {
            $gambar = $this->input->post('foto_jenis_sampah');
        }

        if ($this->form_validation->run() == FALSE) {
            $this->update($this->input->post('id_jenis_sampah', TRUE));
        } else {
            $data = array(
                'id_kategori_sampah'        => $this->input->post('id_kategori_sampah', TRUE),
                'nama_jenis_sampah'         => $this->input->post('nama_jenis_sampah', TRUE),
                'foto_jenis_sampah'         => $gambar,
                'deskripsi_jenis_sampah'    => $this->input->post('deskripsi_jenis_sampah', TRUE),
            );

            $this->Jenis_sampah_model->update($this->input->post('id_jenis_sampah', TRUE), $data);
            $this->session->set_flashdata('berhasil', 'Berhasil Merubah Data');
            redirect(site_url('jenis_sampah'));
        }
    }

    public function del_sem($id)
    {
        $row = $this->Jenis_sampah_model->get_by_id($id);

        if ($row) {
            $data = array(
                'status_jenis_sampah_remove' => 0,
            );
             
            $this->db->where('id_jenis_sampah', $id);
            $this->db->update('jenis_sampah', $data);
             del('jenis_sampah/'.basename($row->foto_jenis_sampah));
  
            @unlink('assets/gambar/jenis_sampah/' . $row->foto_jenis_sampah);

            $this->session->set_flashdata('berhasil', 'Berhasil Menghapus Data');
            redirect(site_url('jenis_sampah'));
        } else {
            $this->session->set_flashdata('gagal', 'Data Tidak Ditemukan');
            redirect(site_url('jenis_sampah'));
        }
    }
   
   public function cek()
   {
       # code...
    $cek="https://kompis-bank-sampah.s3.ap-southeast-1.amazonaws.com/jenis_sampah/14fd6785b9e075bb62b60680b6b4a393.png";
    // del(basename('jenis_sampah/'.$cek));
    echo "jenis_sampah/".basename($cek);
   }
    public function _rules()
    {
        $this->form_validation->set_rules('id_kategori_sampah', 'id kategori sampah', 'trim|required');
        $this->form_validation->set_rules('nama_jenis_sampah', 'nama jenis sampah', 'trim|required');
        $this->form_validation->set_rules('deskripsi_jenis_sampah', 'deskripsi jenis sampah', 'trim|required');

        $this->form_validation->set_rules('id_jenis_sampah', 'id_jenis_sampah', 'trim');
        $this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

    public function excel()
    {
        $this->load->helper('exportexcel');
        $namaFile = "jenis_sampah.xls";
        $judul = "jenis_sampah";
        $tablehead = 0;
        $tablebody = 1;
        $nourut = 1;
        //penulisan header
        header("Pragma: public");
        header("Expires: 0");
        header("Cache-Control: must-revalidate, post-check=0,pre-check=0");
        header("Content-Type: application/force-download");
        header("Content-Type: application/octet-stream");
        header("Content-Type: application/download");
        header("Content-Disposition: attachment;filename=" . $namaFile . "");
        header("Content-Transfer-Encoding: binary ");

        xlsBOF();

        $kolomhead = 0;
        xlsWriteLabel($tablehead, $kolomhead++, "No");
        xlsWriteLabel($tablehead, $kolomhead++, "Id Kategori Sampah");
        xlsWriteLabel($tablehead, $kolomhead++, "Nama Jenis Sampah");
        xlsWriteLabel($tablehead, $kolomhead++, "Foto Jenis Sampah");
        xlsWriteLabel($tablehead, $kolomhead++, "Deskripsi Jenis Sampah");
        xlsWriteLabel($tablehead, $kolomhead++, "Status Jenis Sampah");

        foreach ($this->Jenis_sampah_model->get_all() as $data) {
            $kolombody = 0;

            //ubah xlsWriteLabel menjadi xlsWriteNumber untuk kolom numeric
            xlsWriteNumber($tablebody, $kolombody++, $nourut);
            xlsWriteNumber($tablebody, $kolombody++, $data->id_kategori_sampah);
            xlsWriteLabel($tablebody, $kolombody++, $data->nama_jenis_sampah);
            xlsWriteLabel($tablebody, $kolombody++, $data->foto_jenis_sampah);
            xlsWriteLabel($tablebody, $kolombody++, $data->deskripsi_jenis_sampah);
            xlsWriteLabel($tablebody, $kolombody++, $data->status_jenis_sampah);

            $tablebody++;
            $nourut++;
        }

        xlsEOF();
        exit();
    }
  function get_image(){

        $img=$this->input->get('img');
        // echo getkonten($img);
        $cek='https://kompis-bank-sampah.s3.ap-southeast-1.amazonaws.com/'.$img;
         $row = $this->Jenis_sampah_model->get_by_poto($cek);
          if($row){
          echo getkonten($img);  
        }else{
              $this->session->set_flashdata('cek', 'Gambar Tidak Ditemukan');
                    redirect('profile');
        }
        }


       public function exportxl()
    {
        $q          = $this->input->get('q');
        $subject    = "Jenis_Sampah";
        // $id         = $this->session->userdata('uid');
           $this->db->query('SET @no=0');
        $sql        ="SELECT @no:=@no+1 AS nomor, kategori_sampah.nama_kategori_sampah,nama_jenis_sampah,deskripsi_jenis_sampah FROM `jenis_sampah` JOIN `kategori_sampah` ON `jenis_sampah`.`id_kategori_sampah` = `kategori_sampah`.`id_kategori_sampah` WHERE `status_jenis_sampah_remove` = 1";
        exportSQL('Jenis Sampah', $subject, $sql);
    }
}

/* End of file Jenis_sampah.php */
/* Location: ./application/controllers/Jenis_sampah.php */
/* Please DO NOT modify this information : */
/* Generated by Harviacode Codeigniter CRUD Generator 2020-02-10 08:05:20 */
/* http://harviacode.com */
