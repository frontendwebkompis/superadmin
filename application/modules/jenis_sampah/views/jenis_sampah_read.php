<script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>

<div class="row">
	<section class="col-lg connectedSortable ui-sortable">
		<div class="card card-info">
			<div class="card-header">
				<div class="card-title"><?php echo $title_card?></div>
			</div>
			<div class="card-body">
				<div class="tab-content p-0">
					<table class="table table-bordered table-striped table-condensed" style="margin-bottom: 20px">
						<tr>
							<td>Kategori Sampah</td>
							<td><?php echo $nama_kategori_sampah; ?></td>
						</tr>
						<tr>
							<td>Nama Jenis Sampah</td>
							<td><?php echo $nama_jenis_sampah; ?></td>
						</tr>
						<tr>
							<td>Foto Jenis Sampah</td>
							
							<td>
								<img src="<?php echo $foto_jenis_sampah ?>" alt="Belum Ada Gambar" style="height:100px">
							</td>
						</tr>
						<tr>
							<td>Deskripsi Jenis Sampah</td>
							<td><?php echo $deskripsi_jenis_sampah; ?></td>
						</tr>
					</table>
					<div>
						<a href="<?php echo site_url('jenis_sampah') ?>" class="btn btn-info btn-sm" style="width: 100px;">Batal</a>
					</div>
					</div>
					<div class="col-md-4 text-center">
                        <div style="margin-top: 10px" id="message">
                            <?php echo $this->session->userdata('message') <> '' ? $this->session->userdata('message') : ''; ?>
                        </div>
                    </div>

					<?php if(count($merk_sampah_data)==0){ 
						echo "<div class='text-center' style='margin-top: 10px'><span class='alert alert-default'><i>~Merk Belum Ada~</i></span></div>";
					}else{ ?>
					<table class="table table-bordered table-striped table-condensed" style="margin-top: 20px">
						<tr>
							<th style="text-align:center" width="20px">No</th>
						
							<th>Nama Merk</th>
							<th style="text-align:center">Aksi</th>
						</tr><?php foreach ($merk_sampah_data as $merk_sampah) { ?>
						<tr>
							<td style="text-align:center"><?php echo ++$start ?></td>
							
							<td><?php echo $merk_sampah->nama_merk ?></td>
							<td style="text-align:center" width="100px">
								<div class="btn-group">
									<a href="<?php echo site_url('merk_sampah/update/'.$merk_sampah->id_merk); ?>"
									data-toogle="tooltip" title="Update">
									<button type="button" class="btn btn-success btn-sm"><i class="far fa-edit"></i></button></a>
									<a href="#" class="btn btn-danger btn-sm" data-toggle="tooltip" data-placement="top" title="Hapus" onclick="confirm('<?=  $merk_sampah->id_merk ?>')" ><i class="fas fa-trash-alt"></i></a>
								</div>
							</td>
						</tr>
						<?php } ?>
					</table>
					<?php } ?>
							<?= footer($total_rows, $pagination, '') ?>
				</div>
			</div>
		</div>
	</section>
</div>

<script>
function confirm(res) {
    Swal.fire({
    title: 'Anda Yakin Mau Menghapus?',
    text: "Tidak bisa dikembalikan jika sudah dihapus!",
    icon: 'warning',
    showCancelButton: true,
    confirmButtonColor: '#3085d6',
    cancelButtonColor: '#d33',
    confirmButtonText: 'Ya, Hapus!',
    cancelButtonText: 'Batal'
    }).then((result) => {
    if (result.value) {
        Swal.fire(
        'Terhapus!',
        'File sudah terhapus.',
        'success'
        )
        window.location='<?php echo base_url().'merk_sampah/del_sem/'; ?>'+res;
    }
    });
}

</script>