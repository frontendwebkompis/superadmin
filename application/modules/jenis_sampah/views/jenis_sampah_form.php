<div class="row">
	<section class="col-lg connectedSortable ui-sortable">
		<div class="card card-info">
			<div class="card-header">
                <div class="card-title">
                  <?php echo $title_card?>
                </div>
            </div>
			<div class="card-body">
				<div class="tab-content p-0">

					<form id="dataform" action="<?php echo $action; ?>" method="post" enctype="multipart/form-data">
						<div class="form-group">
							<label for="varchar">Kategori Sampah <?php echo form_error('id_kategori_sampah') ?></label>
							<select required id="id_kategori_sampah" class="form-control" name="id_kategori_sampah">
								<option value="">Pilih Kategori</option>
								<?php foreach ($kategori_sampah as $kat){ ?>
								<option <?=  $kat->id_kategori_sampah ==  $id_kategori_sampah ? 'selected' : ''; ?>
								value="<?php echo $kat->id_kategori_sampah; ?>">
								<?php echo $kat->nama_kategori_sampah; ?>
								</option>
								<?php }?>
							</select>
						</div>
						<div class="form-group">
							<label for="varchar">Nama Jenis Sampah <?php echo form_error('nama_jenis_sampah') ?></label>
							<input required type="text" class="form-control" name="nama_jenis_sampah" id="nama_jenis_sampah"
								placeholder="Nama Jenis Sampah" value="<?php echo $nama_jenis_sampah; ?>" />
						</div>
						<div class="form-group">
							<label for="varchar">Foto Jenis Sampah <?php echo form_error('foto_jenis_sampah') ?></label>
							<?php if($foto_jenis_sampah !=""){?>
								<div class="row justify-content-center mb-2">
									<img class="img-fluid rounded" style="height:160px" src="<?php echo base_url(); ?>jenis_sampah/get_image?img=jenis_sampah/<?php echo basename($foto_jenis_sampah) ?>" alt="">	
								</div>
								<div class="row justify-content-center mb-2">
									<button type="button" class="btn btn-secondary btn-sm" onclick="cek()"><i class="fas fa-image"></i> Ganti Gambar</button>
								</div>
							<input type="file" style="display:none" class="form-control" accept="image/*" name="foto_jenis_sampahbaru" id="foto_jenis_sampahbaru">
							<input type="hidden" id="foto_jenis_sampah" accept="image/*" value="<?php echo $foto_jenis_sampah ?>"
								name="foto_jenis_sampah">
							<?php }else{ 
									if($button == 'Update'){ ?>
										<input type="file" class="form-control" accept="image/*" name="foto_jenis_sampahbaru" id="foto_jenis_sampahbaru">
								<?php
									} else { ?>
										<input required type="file" class="form-control" accept="image/*" name="foto_jenis_sampah" id="foto_jenis_sampah">
								<?php
									}
								?>
							<?php } ?>
						</div>
						<div class="form-group">
							<label for="varchar">Deskripsi Jenis Sampah
								<?php echo form_error('deskripsi_jenis_sampah') ?></label>
							<textarea required class="form-control" name="deskripsi_jenis_sampah" id="deskripsi_jenis_sampah"
								cols="30" rows="5"><?php echo $deskripsi_jenis_sampah; ?></textarea>
						</div>
						
						<input type="hidden" name="id_jenis_sampah" value="<?php echo $id_jenis_sampah; ?>" />
						 <div id="loader" class="alert alert-info" style="display:none">
						    <strong>Info!</strong> Sedang Proses....
						</div>
						<button id="ceksub" type="submit" class="btn btn-info" style="width: 100px;">Simpan
						</button>
						<a href="<?php echo site_url('jenis_sampah') ?>" class="btn btn-default" style="width: 100px;">Batal</a>
					</form>
					<script>
						function cek() {
							var x = document.getElementById("foto_jenis_sampahbaru");
							if (x.style.display === "none") {
								x.style.display = "block";
							} else {
								x.style.display = "none";
							}
						}

					</script>
				</div>
			</div>
		</div>
	</section>
</div>

<script type="text/javascript">

    $(document).ready(function () {
      $('#dataform').validate({
        rules: {
          id_kategori_sampah: {
            required: true,
          },
          nama_jenis_sampah: {
          	required: true,
          },
          foto_jenis_sampah: {
          	required: true,
          },
          deskripsi_jenis_sampah: {
          	required: true,
          }
        },
        messages: {
          id_kategori_sampah: {
            required: "Kategori Sampah Tidak Boleh Kosong",
          },
          nama_jenis_sampah: {
          	required: "Jenis Sampah Tidak Boleh Kosong",
          },
          foto_jenis_sampah: {
          	required: "Foto Jenis Sampah Tidak Boleh Kosong",
          },
          deskripsi_jenis_sampah: {
          	required: "Deskripsi Tidak Boleh Kosong",
          }
        },
        submitHandler: function(form) {
          	// $('#dataform').submit(); 
        	$('#loader').show(300); 
          	$('#ceksub').prop('disabled', true);
          	form.submit();
        },
        errorElement: 'span',
        errorPlacement: function (error, element) {
          error.addClass('invalid-feedback');
          element.closest('.form-group').append(error);
        },
        highlight: function (element, errorClass, validClass) {
          $(element).addClass('is-invalid');
        },
        unhighlight: function (element, errorClass, validClass) {
          $(element).removeClass('is-invalid');
        },
      });
    });
</script>