<script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>

<div class="row">
	<section class="col-lg connectedSortable ui-sortable">
		<div class="card card-info">
			<div class="card-header">
                <div class="card-title">
                  <?php echo $title_card?>
                </div>
            </div>
			<div class="card-body">
				<div class="tab-content p-0">
		
					<div class="row">
						<div class="col-md-4 mb-2">
							<?php echo anchor(site_url('jenis_sampah/create'),'Tambah Data', 'class="btn btn-info btn-sm"'); ?>
						</div>
						<div class="col-md-3 offset-md-5 mb-2">
							<?= search(site_url('jenis_sampah/index'), site_url('jenis_sampah'), $q) ?>
						</div>
					</div>
					<div class="tab-content p-0" style="overflow:auto">
					<table class="table table-bordered table-striped table-condensed table-hover table-sm" style="margin-bottom: 10px">
						<tr>
							<th class="text-center" width="50px">No</th>
							<th class="text-center" width="150px">Kategori</th>
							<th class="text-center">Nama Jenis</th>
							<th class="text-center">Foto Jenis</th>
							<th class="text-center">Deskripsi Jenis</th>
							<th class="text-center">Aksi</th>
						</tr><?php 
			            if($total_rows == 0){
			                echo '<tr><td colspan="11" bgcolor="grey" style="color: white; font-weight: bold; text-align: center;">Data Tidak Ditemukan</td></tr>';
			            } else {
						foreach ($jenis_sampah_data as $jenis_sampah) { ?>
						<tr>
							<td class="text-center"><?php echo ++$start ?></td>
							<td><?php echo $jenis_sampah->nama_kategori_sampah ?></td>
							<td><?php echo $jenis_sampah->nama_jenis_sampah ?></td>
							<td class="text-center">
								<?php
									if($jenis_sampah->foto_jenis_sampah == null){  ?>
										<span class="badge badge-danger">Belum ada gambar</span>
								<?php
									} else { ?>
									<!-- 	<img src="<?php echo base_url(); ?>jenis_sampah/get_image?img=jenis_sampah/<?php echo basename($jenis_sampah->foto_jenis_sampah) ?>" alt="Belum Ada Gambar" style="height:100px"> -->
										<img src="<?php echo $jenis_sampah->foto_jenis_sampah ?>" alt="Belum Ada Gambar" style="height:100px">
								<?php
									}
								?>
								
							</td>
							<td><?php echo $jenis_sampah->deskripsi_jenis_sampah ?></td>
							<td class="text-center" width="150px">
								<div class="btn-group">
									<a href="<?php echo site_url('jenis_sampah/read/'.$jenis_sampah->id_jenis_sampah); ?>"
									data-toogle="tooltip" title="Lihat">
									<button type="button" class="btn btn-info btn-sm"><i class="fas fa-info-circle"></i></button></a>
									<a href="<?php echo site_url('jenis_sampah/update/'.$jenis_sampah->id_jenis_sampah); ?>"
									data-toogle="tooltip" title="Update">
									<button type="button" class="btn btn-success btn-sm"><i class="far fa-edit"></i></button></a>
									<a href="#" class="btn btn-danger btn-sm" data-toggle="tooltip" data-placement="top" title="Hapus" onclick="confirm('<?=  $jenis_sampah->id_jenis_sampah ?>')" ><i class="fas fa-trash-alt"></i></a>
								</div>
							</td>
						</tr>
						<?php } 
						} ?>
					</table>
					</div>
					<?= footer($total_rows, $pagination, site_url('jenis_sampah/exportxl?q=' . $q)) ?>
				</div>
			</div>
		</div>
	</section>
</div>

<script>
function confirm(res) {
    Swal.fire({
    title: 'Anda Yakin Mau Menghapus?',
    text: "Tidak bisa dikembalikan jika sudah dihapus!",
    icon: 'warning',
    showCancelButton: true,
    confirmButtonColor: '#3085d6',
    cancelButtonColor: '#d33',
    confirmButtonText: 'Ya, Hapus!',
    cancelButtonText: 'Batal'
    }).then((result) => {
    if (result.value) {
        Swal.fire(
        'Terhapus!',
        'File sudah terhapus.',
        'success'
        )
        window.location='<?php echo base_url().'jenis_sampah/del_sem/'; ?>'+res;
    }
    });
}

// Get the modal
var modal = document.getElementById("myModal");

// Get the image and insert it inside the modal - use its "alt" text as a caption
var img = document.getElementById("myImg");
var modalImg = document.getElementById("img01");
var captionText = document.getElementById("caption");
img.onclick = function(){
  modal.style.display = "block";
  modalImg.src = this.src;
  captionText.innerHTML = this.alt;
}

// Get the <span> element that closes the modal
var span = document.getElementsByClassName("close")[0];

// When the user clicks on <span> (x), close the modal
span.onclick = function() { 
  modal.style.display = "none";
}
</script>