<div class="card card-<?= bgCard() ?>">
    <div class="card-header">
        <div class="card-title">
            <?= $titleCard ?>
        </div>
    </div>
    <div class="card-body">
        <div class="tab-content p-0">
            <div class="row">
                <div class="col-md-4 mb-2">
                
                </div>
                <div class="col-md-3 offset-md-5 mb-2">
                    <?= search(site_url('request_import/index'), site_url('request_import/index'), $q) ?>
                </div>
            </div>
            <div class="tab-content p-0" style="overflow:auto">
                <table class="table table-bordered table-striped table-condensed table-hover table-sm" style="margin-bottom: 10px">
                    <tr>
                        <th class='text-center' style="width: 60px;">No</th>
                        <th class='text-center'>Nama Bank Sampah</th>
                        <th class='text-center'>Tgl Upload</th>
                        <th class='text-center'>Tgl Update</th>
                        <th class='text-center'>File Nasabah</th>
                        <th class='text-center'>File Jenis</th>
                        <th class='text-center'>File Setoran </th>
                        <th class='text-center'>Status </th>
                        <th class='text-center' style="width: 150px;">Aksi</th>
                    </tr><?php
                            if ($total_rows == 0) {
                                echo '<tr><td colspan="11" bgcolor="grey" style="color: white; font-weight: bold; text-align: center;">Data Tidak Ditemukan</td></tr>';
                            } else {
                                foreach ($request_import_data as $data) {
                            ?>
                            <tr>
                                <td class='text-center'><?php echo ++$start ?></td>
                                <td><?php echo $data->nama_bank_sampah ?></td>
                                <td class='text-center'><?php echo fulldate($data->tgl_upload) ?></td>
                                <td class='text-center'><?php echo fulldate($data->tgl_update) ?></td>

                                <td><?php
                                    if($data->file_nasabah == null){  ?>
                                        <span class="badge badge-danger">Belum ada file</span>
                                <?php
                                    } else { ?>
                                      <a href="<?php echo $data->file_nasabah ?>" class="btn btn-success btn-sm" data-toggle="tooltip" title="Download Excel"><i class="fas fa-file-excel"></i> Download Excel</a>
                                <?php
                                    }
                                ?></td>
                                <td><?php
                                    if($data->file_jenis_sampah == null){  ?>
                                        <span class="badge badge-danger">Belum ada file</span>
                                <?php
                                    } else { ?>
                                           <a href="<?php echo $data->file_jenis_sampah ?>" class="btn btn-success btn-sm" data-toggle="tooltip" title="Download Excel"><i class="fas fa-file-excel"></i> Download Excel</a>
                                <?php
                                    }
                                ?></td>
                                <td><?php
                                    if($data->file_nasabah == null){  ?>
                                        <span class="badge badge-danger">Belum ada gambar</span>
                                <?php
                                    } else { ?>
                                        <a href="<?php echo $data->file_setoran_offline ?>" class="btn btn-success btn-sm" data-toggle="tooltip" title="Download Excel"><i class="fas fa-file-excel"></i> Download Excel</a>
                                <?php
                                    }
                                ?></td>
                                <td>  <?php if ($data->status == 0) { ?>
                                       <span class="badge badge-secondary">Belum di inputkan</span>
                                      <?php } ?> 
                                       <?php if ($data->status == 1) { ?>
                                       <span class="badge badge-primary">Sedang di proses</span>
                                      <?php } ?> 
                                      <?php if ($data->status == 2) { ?>
                                       <span class="badge badge-danger">Ditolak</span>
                                      <?php } ?>
                                    <?php if ($data->status == 3) { ?>
                                         <span class="badge badge-success">Sudah di inputkan</span>
                                    
                                    <?php } ?>
                                </td>
                                <td class='text-center'>
                                    <div class="btn-group">
                                       
                                        <?php if ($data->status == 0) { ?>
                                            <button class="btn btn-primary btn-sm" title="Proses" onclick="cekProses('<?= $data->id_request_setup_bs ?>')"><i class="fas fa-table"></i></button>
                                        <?php } ?>
                                        <?php if ($data->status == 1) { ?>
                                            <button class="btn btn-danger btn-sm" title="Tolak" onclick="cekNonAktifVersi('<?= $data->id_request_setup_bs ?>')"><i class="fas fa-book"></i></button> 
                                            <button class="btn btn-success btn-sm" title="Aktif" onclick="cekAktifVersi('<?= $data->id_request_setup_bs ?>')"><i class="fas fa-book"></i>
                                            </button>
                                        <?php } ?>
                                    </div>

                                </td>
                            </tr>
                    <?php
                                }
                            }
                    ?>
                </table>
            </div>
            <?= footer($total_rows, $pagination, '') ?>
        </div>
    </div>
</div>

<div id="modalCreateversi" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Form Tambah versi</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form action="<?= $action ?>" method="post" id="formTambahversi" role="form" enctype="multipart/form-data">
                    <div class="row ml-3 mr-3">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="varchar">Jenis Aplikasi</label>
                                <select name="status_aplikasi" id="status_aplikasi" class="form-control" required>
                                    <option value="">Pilih Jenis Aplikasi</option>
                                    <option value=0>Bank Sampah</option>
                                    <option value=1>Nasabah</option>
                                    <option value=2>Pengepul</option>

                                </select>
                            </div>
                            <div class="form-group">
                                <label for="varchar">Versi</label>
                                <input type="text" class="form-control" name="versi" id="versi" placeholder="Versi" value="" />
                            </div>
                            <div class="form-group">
                                <label for="varchar">Keterangan Versi</label>
                                <input type="text" class="form-control" name="keterangan_versi" id="keterangan_versi" placeholder="Keterangan Versi" value="" />
                            </div>

                        </div>
                    </div>
            </div>
            <div class="modal-footer">
                <div class="col-md-12 text-center">
                    <button type="submit" class="btn btn-<?= bgCard() ?>" style="width: 100px;">Simpan</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal" style="width: 100px;">Batal</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="modalEditversi" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Form Edit versi</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form action="<?= $action ?>" method="post" id="formEditversi" role="form" enctype="multipart/form-data">
                    <div class="row ml-3 mr-3">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="varchar">Jenis Aplikasi</label>
                                <select name="status_aplikasi" id="status_aplikasi" class="form-control" required>
                                    <option value="">Pilih Jenis Aplikasi</option>
                                    <option value=0>Bank Sampah</option>
                                    <option value=1>Nasabah</option>
                                    <option value=2>Pengepul</option>

                                </select>
                            </div>
                            <div class="form-group">
                                <label for="varchar">Versi</label>
                                <input type="text" class="form-control" name="versi" id="versi" placeholder="Versi" value="" />
                            </div>
                            <div class="form-group">
                                <label for="varchar">Keterangan Versi</label>
                                <input type="text" class="form-control" name="keterangan_versi" id="keterangan_versi" placeholder="Keterangan Versi" value="" />
                            </div>
                        </div>
                    </div>
            </div>
            <div class="modal-footer">
                <div class="col-md-12 text-center">
                    <button type="submit" class="btn btn-<?= bgCard() ?>" style="width: 100px;">Simpan</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal" style="width: 100px;">Batal</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<script>      

    function cekEditversi(id) {
        startloading('#modalEditversi .modal-content')
        $('#modalEditversi #formEditversi').attr('action', '<?= base_url('request_import/update_action/') ?>' + id)
        $.ajax({
            url: '<?php echo base_url() ?>request_import/ambilDataversi/' + id,
            dataType: "json",
            cache: false,
            success: function(res) {
                $('#modalEditversi #status_aplikasi').val(res.status_aplikasi)
                $('#modalEditversi #edit_keterangan_versi').val(res.id_keterangan_versi).trigger('change')
                $('#modalEditversi #keterangan_versi').val(res.keterangan_versi)
                $('#modalEditversi #versi').val(res.versi)
                stoploading('#modalEditversi .modal-content')
            }
        });
    }

    function cekNonAktifVersi(id) {
        swal({
                title: 'Yakin merubah status?',
                text: "Merubah informasi memberitahukan pada bank sampah",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#CD5C5C',
                cancelButtonColor: '#d33',
                cancelButtonText: 'Batal',
                confirmButtonText: 'Non Aktifkan',
                closeOnConfirm: false,
            },
            function(isConfirm) {
                if (isConfirm) {
                    window.location.href = "<?php echo site_url('request_import/nonaktif/'); ?>" + id;
                } else {

                }
            });
    }

    function cekAktifVersi(id) {
        swal({
                title: 'Yakin merubah status?',
                text: "Merubah informasi memberitahukan pada bank sampah",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#CD5C5C',
                cancelButtonColor: '#d33',
                cancelButtonText: 'Batal',
                confirmButtonText: 'Aktifkan',
                closeOnConfirm: false,
            },
            function(isConfirm) {
                if (isConfirm) {
                    window.location.href = "<?php echo site_url('request_import/aktif/'); ?>" + id;
                } else {

                }
            });
    } 
    function cekProses(id) {
        swal({
                title: 'Yakin merubah status?',
                text: "Merubah informasi memberitahukan pada bank sampah",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#CD5C5C',
                cancelButtonColor: '#d33',
                cancelButtonText: 'Batal',
                confirmButtonText: 'Proses',
                closeOnConfirm: false,
            },
            function(isConfirm) {
                if (isConfirm) {
                    window.location.href = "<?php echo site_url('request_import/proses/'); ?>" + id;
                } else {

                }
            });
    }

 
</script>