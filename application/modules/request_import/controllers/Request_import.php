<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Request_import extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        if (cek_token()) {
            $this->load->model('Request_import_model');
            $this->load->library('form_validation');
        } else {
            logout();
        }
    }

    public function index()
    {
        $q = urldecode($this->input->get('q', TRUE));
        $start = intval($this->input->get('start'));

        if ($q <> '') {
            $config['base_url'] = base_url() . 'request_import?q=' . urlencode($q);
            $config['first_url'] = base_url() . 'request_import?q=' . urlencode($q);
        } else {
            $config['base_url'] = base_url() . 'request_import';
            $config['first_url'] = base_url() . 'request_import';
        }

        $config['per_page'] = 10;
        $config['page_query_string'] = TRUE;
        $config['total_rows'] = $this->Request_import_model->total_rows($q);
        $request_import     = $this->Request_import_model->get_limit_data($config['per_page'], $start, $q);
        $kendaraan  = $this->db->get('jenis_kendaraan')->result();

        $this->load->library('pagination');
        $this->pagination->initialize($config);

        $data = array(
            'titleCard'                 => 'List Request Import',
            'tittle'                     => 'Request Import',
            'judul'                     => 'Request Import',
            'breadcrumbactive'          => '',
            'breadcrumb'                => 'Request Import',
            'cekaktif'                  => 'request_import',
            'link'                      => '',
            'request_import_data'            => $request_import,
            'q'                         => $q,
            'pagination'                => $this->pagination->create_links(),
            'total_rows'                => $config['total_rows'],
            'start'                     => $start,
            'action'                    => site_url('request_import/create_action'),
            'kategori_aktif'            => '1',
            'menu_aktif'                => 'request_import'
        );
        $data['kendaraan']  = $kendaraan;
        $data['datakonten'] = $this->load->view('request_import_list', $data, true);
        $this->load->view('layouts/main_view', $data);
    }

    public function detail($id)
    {
        $row = $this->Request_import_model->get_by_id($id);
        if ($row) {
            $data = array(
                'titleCard'                 => 'Data Dashboard',
                'title'                     => 'Dashboard | Mitra',
                'judul'                     => 'Dashboard',
                'breadcrumbactive'          => '',
                'breadcrumb'                => 'Dashboard',
                'cekaktif'                  => 'dashboard',
                'link'                      => '',
                'id_request_import' => $row->id_request_import,
                'nama_request_import' => $row->nama_request_import,
                'prefix_kat' => $row->prefix_kat,
                'created_at' => $row->created_at,
                'edited_at' => $row->edited_at,
            );
            $data['datakonten'] = $this->load->view('tb_request_import_detail', $data, true);
            $this->load->view('layouts/main_view', $data);
        } else {
            $this->session->set_flashdata('message', '<div class="alert alert-warning" role="alert">
  Data Tidak Di Temukan
</div>');
            redirect(site_url('request_import'));
        }
    }

    public function create()
    {
        $data = array(
            'titleCard'                 => 'Data Dashboard',
            'title'                     => 'Dashboard |',
            'judul'                     => 'Dashboard',
            'breadcrumbactive'          => '',
            'breadcrumb'                => 'Dashboard',
            'cekaktif'                  => 'dashboard',
            'link'                      => '',
            'button' => 'Create',
            'action' => site_url('request_import/create_action'),
            'id_request_import' => set_value('id_request_import'),
            'nama_request_import' => set_value('nama_request_import'),
            'prefix_kat' => set_value('prefix_kat'),
            'created_at' => set_value('created_at'),
            'edited_at' => set_value('edited_at'),
            'sts_rmv_request_import' => set_value('sts_rmv_request_import'),
        );
        $data['datakonten'] = $this->load->view('tb_request_import_form', $data, true);
        $this->load->view('layouts/main_view', $data);
    }

    public function create_action()
    {
        $this->_rules();
        $dt = new DateTime();
        // $prefix = "DRV".getID();
        // // echo $gabung;
        // $row    = $this->Request_import_model->get_by_prefix($prefix);
        // $NoUrut = (int) substr($row->max, 18, 4);
        // $NoUrut = $NoUrut + 1; //nomor urut +1
        // $NoUrut = sprintf('%04d', $NoUrut);
        // $fix    = $prefix . $NoUrut;
        if ($this->form_validation->run() == FALSE) {
            $this->session->set_flashdata('gagal', 'Gagal Menambahkan Data');
            redirect(site_url('request_import'));
        } else {
            $data = array(
                'keterangan_versi'           => $this->input->post('keterangan_versi', TRUE),
                'versi'    => $this->input->post('versi', TRUE),
                'status_aplikasi'        => $this->input->post('status_aplikasi', TRUE),
                'is_versi_active'        => 0,
                'sts_versi_rmv'        => 1,
                'tgl_buat'        => $dt->format('Y-m-d H:i:s'),
                'tgl_update'        => $dt->format('Y-m-d H:i:s'),
            );

            $this->Request_import_model->insert($data);
            $this->session->set_flashdata('berhasil', 'Berhasil Menambahkan Data');
            redirect(site_url('request_import'));
        }
    }

    public function update($id)
    {
        $row = $this->Request_import_model->get_by_id($id);

        if ($row) {
            $data = array(
                'titleCard'                 => 'Data Dashboard',
                'title'                     => 'Dashboard | Mitra',
                'judul'                     => 'Dashboard',
                'breadcrumbactive'          => '',
                'breadcrumb'                => 'Dashboard',
                'cekaktif'                  => 'dashboard',
                'link'                      => '',
                'button' => 'Update',
                'action' => site_url('request_import/update_action'),
                'id_request_import' => set_value('id_request_import', $row->id_request_import),
                'nama_request_import' => set_value('nama_request_import', $row->nama_request_import),
                'prefix_kat' => set_value('prefix_kat', $row->prefix_kat),
                'created_at' => set_value('created_at', $row->created_at),
                'edited_at' => set_value('edited_at', $row->edited_at),
                'sts_rmv_request_import' => set_value('sts_rmv_request_import', $row->sts_rmv_request_import),
            );
            $data['datakonten'] = $this->load->view('tb_request_import_form', $data, true);
            $this->load->view('layouts/main_view', $data);
        } else {
            $this->session->set_flashdata('message', '<div class="alert alert-warning" role="alert">Data Tidak Ditemukan</div>');
            redirect(site_url('request_import'));
        }
    }

    public function update_action($id)
    {
        $this->_rules();
        $dt = new DateTime();
        // if ($this->form_validation->run() == FALSE) {
        //     $this->update($id);
        // } else {
        $data = array(
            'keterangan_versi'           => $this->input->post('keterangan_versi', TRUE),
            'versi'    => $this->input->post('versi', TRUE),
            'status_aplikasi'        => $this->input->post('status_aplikasi', TRUE),
            'tgl_update'        => $dt->format('Y-m-d H:i:s'),
        );

        $this->Request_import_model->update($id, $data);
        $this->session->set_flashdata('berhasil', 'Berhasil Merubah Data');
        redirect(site_url('request_import'));
        // }
    }

    public function delete($id)
    {
        $row = $this->Request_import_model->get_by_id($id);
        $dt = new DateTime();
        if ($row) {
            $data = array(
                'is_versi_active' => 0,
                'sts_versi_rmv' => 0,
                'tgl_update'        => $dt->format('Y-m-d H:i:s'),
            );
            $this->Request_import_model->update($id, $data);
            $this->session->set_flashdata('berhasil', 'Berhasil Menghapus Data');
            redirect(site_url('request_import'));
        } else {
            $this->session->set_flashdata('gagal', 'Data Tidak Ditemukan');
            redirect(site_url('request_import'));
        }
    }
    public function nonaktif($id)
    {

        $row = $this->Request_import_model->get_by_id($id);

        $dt = new DateTime();
        if ($row) {
            
                $data = array(
                    'status' => 2,
                    'tgl_update'        => $dt->format('Y-m-d H:i:s'),
                );
                $this->Request_import_model->update($id, $data);
                $this->session->set_flashdata('berhasil', 'Berhasil Menonaktifkan Data');
                redirect(site_url('request_import'));
           
        } else {
            $this->session->set_flashdata('gagal', 'Data Tidak Ditemukan');
            redirect(site_url('request_import'));
        }
    }  
      public function proses($id)
    {

        $row = $this->Request_import_model->get_by_id($id);

        $dt = new DateTime();
        if ($row) {
            
                $data = array(
                    'status' => 1,
                    'tgl_update'        => $dt->format('Y-m-d H:i:s'),
                );
                $this->Request_import_model->update($id, $data);
                $this->session->set_flashdata('berhasil', 'Berhasil Menonaktifkan Data');
                redirect(site_url('request_import'));
           
        } else {
            $this->session->set_flashdata('gagal', 'Data Tidak Ditemukan');
            redirect(site_url('request_import'));
        }
    }
    public function aktif($id)
    {
        $row = $this->Request_import_model->get_by_id($id);
        $dt = new DateTime();
        if ($row) {
           
            $data = array(
                'status' => 3,
                'tgl_update'        => $dt->format('Y-m-d H:i:s'),
            );
            $this->Request_import_model->update($id, $data);
            $this->session->set_flashdata('berhasil', 'Berhasil Mengaktifkan Versi');
            redirect(site_url('request_import'));
        } else {
            $this->session->set_flashdata('gagal', 'Data Tidak Ditemukan');
            redirect(site_url('request_import'));
        }
    }

    public function _rules()
    {
        $this->form_validation->set_rules('versi', 'nama kategori produk', 'trim|required');
        $this->form_validation->set_rules('status_aplikasi', 'Jenis Kendaraan', 'trim|required');
        $this->form_validation->set_rules('keterangan_versi', 'Plat Kendaraan', 'trim|required');

        $this->form_validation->set_rules('id_versi', 'id_versi', 'trim');
        $this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

    public function ambilDataversi($id)
    {
        $this->db->where('id_versi', $id);
        $data = $this->db->get('request_import')->row();
        echo json_encode($data);
    }
}

/* End of file request_import.php */
/* Location: ./application/controllers/request_import.php */
/* Please DO NOT modify this information : */
/* Generated by Harviacode Codeigniter CRUD Generator 2020-07-15 09:43:37 */
/* http://harviacode.com */