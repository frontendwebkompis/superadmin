<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Laporan_setoran extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        if (cek_token()) {
        $this->load->model('Laporan_setoran_model');
        $this->load->library('form_validation');
    } else {
            logout();
        }
    }

    public function index(){
        $q = urldecode($this->input->get('q', TRUE));
        $start = intval($this->input->get('start'));
         $awal   = $this->input->get('awal') ? $this->input->get('awal') : date('y').'-01-01';
        $akhir  = $this->input->get('akhir') ? $this->input->get('akhir') : date('y-m-d');
      
        if ($q <> '') {
            $config['base_url'] = base_url() . 'laporan_setoran?q=' . urlencode($q);
            $config['first_url'] = base_url() . 'laporan_setoran?q=' . urlencode($q);
        } else {
            $config['base_url'] = base_url() . 'laporan_setoran';
            $config['first_url'] = base_url() . 'laporan_setoran';
        }

        $config['per_page'] = 15;
        $config['page_query_string'] = TRUE;
        $config['total_rows'] = $this->Laporan_setoran_model->total_rows($q,$awal,$akhir);
        $laporan_setoran     = $this->Laporan_setoran_model->get_limit_data($config['per_page'], $start, $q,$awal,$akhir);
     
        $this->load->library('pagination');
        $this->pagination->initialize($config);

        $data = array(
            'titleCard'                 => 'List Laporan Setoran',
            'tittle'                    => 'Laporan Setoran',
            'judul'                     => 'Laporan Setoran',
            'breadcrumbactive'          => '',
            'breadcrumb'                => 'Laporan Setoran',
            'cekaktif'                  => 'Laporan Setoran',
            'link'                      => '',
            'laporan_setoran_data'      => $laporan_setoran,
            'q'                         => $q,
            'pagination'                => $this->pagination->create_links(),
            'total_rows'                => $config['total_rows'],
            'start'                     => $start,
            'action'                    => site_url('laporan_setoran/create_action'),
            'kategori_aktif'            => '1',
            'menu_aktif'                => 'laporan_setoran',
            'tombol_aktif'              => 'pengepul'
        );
        $data['datakonten'] = $this->load->view('laporan_setoran_list', $data, true);
        $this->load->view('layouts/main_view', $data);
    }

    public function bank_sampah(){
        $q = urldecode($this->input->get('q', TRUE));
        $start = intval($this->input->get('start'));
             $awal   = $this->input->get('awal') ? $this->input->get('awal') : date('y').'-01-01';
        $akhir  = $this->input->get('akhir') ? $this->input->get('akhir') : date('y-m-d');
    
        if ($q <> '') {
            $config['base_url'] = base_url() . 'laporan_setoran/bank_sampah?q=' . urlencode($q);
            $config['first_url'] = base_url() . 'laporan_setoran/bank_sampah?q=' . urlencode($q);
        } else {
            $config['base_url'] = base_url() . 'laporan_setoran/bank_sampah';
            $config['first_url'] = base_url() . 'laporan_setoran/bank_sampah';
        }

        $config['per_page'] = 15;
        $config['page_query_string'] = TRUE;
        $config['total_rows'] = $this->Laporan_setoran_model->total_rows_bank_sampah($q,$awal,$akhir);
        $laporan_setoran     = $this->Laporan_setoran_model->get_limit_data_bank_sampah($config['per_page'], $start, $q,$awal,$akhir);
     
        $this->load->library('pagination');
        $this->pagination->initialize($config);

        $data = array(
            'titleCard'                 => 'List Versi App Superadmin',
            'tittle'                     => ' App Superadmin | ',
            'judul'                     => 'Versi App Superadmin',
            'breadcrumbactive'          => '',
            'breadcrumb'                => 'Versi App Superadmin',
            'cekaktif'                  => 'Versi App',
            'link'                      => '',
            'laporan_setoran_data'               => $laporan_setoran,
            'q'                         => $q,
            'pagination'                => $this->pagination->create_links(),
            'total_rows'                => $config['total_rows'],
            'start'                     => $start,
            'action'                    => site_url('laporan_setoran/create_action'),
            'kategori_aktif'            => '1',
             'menu_aktif'            => 'laporan_setoran',
             'tombol_aktif'            => 'bank_sampah'
        );
         $data['datakonten'] = $this->load->view('laporan_setoran_list', $data, true);
        $this->load->view('layouts/main_view', $data);
    }
    public function nasabah(){
        $q = urldecode($this->input->get('q', TRUE));
        $start = intval($this->input->get('start'));
             $awal   = $this->input->get('awal') ? $this->input->get('awal') : date('y').'-01-01';
        $akhir  = $this->input->get('akhir') ? $this->input->get('akhir') : date('y-m-d');
    
        if ($q <> '') {
            $config['base_url'] = base_url() . 'laporan_setoran/nasabah?q=' . urlencode($q);
            $config['first_url'] = base_url() . 'laporan_setoran/nasabah?q=' . urlencode($q);
        } else {
            $config['base_url'] = base_url() . 'laporan_setoran/nasabah';
            $config['first_url'] = base_url() . 'laporan_setoran/nasabah';
        }

        $config['per_page'] = 15;
        $config['page_query_string'] = TRUE;
        $config['total_rows'] = $this->Laporan_setoran_model->total_rows_nasabah($q,$awal,$akhir);
        $laporan_setoran     = $this->Laporan_setoran_model->get_limit_data_nasabah($config['per_page'], $start, $q,$awal,$akhir);
     
        $this->load->library('pagination');
        $this->pagination->initialize($config);

        $data = array(
            'titleCard'                 => 'List Versi App Superadmin',
            'tittle'                     => 'Versi App Superadmin | ',
            'judul'                     => 'Versi App Superadmin',
            'breadcrumbactive'          => '',
            'breadcrumb'                => 'Versi App Superadmin',
            'cekaktif'                  => 'Versi App',
            'link'                      => '',
            'laporan_setoran_data'               => $laporan_setoran,
            'q'                         => $q,
            'pagination'                => $this->pagination->create_links(),
            'total_rows'                => $config['total_rows'],
            'start'                     => $start,
            'action'                    => site_url('laporan_setoran/create_action'),
            'kategori_aktif'            => '1',
             'menu_aktif'            => 'laporan_setoran',
             'tombol_aktif'            => 'nasabah'
        );
         $data['datakonten'] = $this->load->view('laporan_setoran_list', $data, true);
        $this->load->view('layouts/main_view', $data);
    }

    public function detail($id) 
    {
        
        $row = $this->Laporan_setoran_model->get_by_id($id);

        if ($row) {
             $q = urldecode($this->input->get('q', TRUE));
              $start = intval($this->input->get('start'));
    
            if ($q <> '') {
                $config['base_url'] = base_url() . 'laporan_setoran/detail/'.$id.'?q=' . urlencode($q);
                $config['first_url'] = base_url() . 'laporan_setoran/detail/'.$id.'?q=' . urlencode($q);
            } else {
                $config['base_url'] = base_url() . 'laporan_setoran/detail/'.$id;
                $config['first_url'] = base_url() . 'laporan_setoran/detail/'.$id;
            }
        $config['per_page'] = 15;
        $config['page_query_string'] = TRUE;
        $config['total_rows'] = $this->Laporan_setoran_model->total_rows_detail($id,$q);
        $detail     = $this->Laporan_setoran_model->get_limit_detail_nasabah($id,$config['per_page'], $start, $q);
        $this->load->library('pagination');
            $this->pagination->initialize($config);
        $data = array(
        'titleCard'                 => 'Data Dashboard',
        'tittle'                     => 'Dashboard | Mitra',
        'judul'                     => 'Dashboard',
        'breadcrumbactive'          => '',
        'breadcrumb'                => 'Dashboard',
         'cekaktif'                  => 'dashboard',
         'link'                      => '',
		 'id_setoran_online'     => $row->id_setoran_online,
        'nama_nasabah_online'   => $row->nama_nasabah_online,
        'nama_bank_sampah'      => $row->nama_bank_sampah,
        'nama_status_setoran'   => $row->nama_status_setoran,
        'nama_jenis_transaksi'  => $row->nama_jenis_transaksi,
        'tgl_setoran'           => $row->tgl_setoran,
        'id_reason'             => $row->id_reason,
        'desc_reason'           => $row->desc_reason,
        'nilai_nasabah_online'  => $row->nilai_nasabah_online,
        'nilai_bank_sampah'     => $row->nilai_bank_sampah,
        'ulasan_bank_sampah'    => $row->ulasan_bank_sampah,
        'ulasan_nasabah'        => $row->ulasan_nasabah,
        'tgl_review'            => $row->tgl_review,
        'setoran_online_data'   => $detail,
         'total_rows'                => $config['total_rows'],
		'menu_aktif'            => 'laporan_setoran',
        'start'                 => $start,   
         'q'                     => $q,
         'pagination'            => $this->pagination->create_links(),
         'total_rows'            => $config['total_rows'],
            
              
	    );
        $data['datakonten'] = $this->load->view('laporan_setoran_detail', $data, true);
        $this->load->view('layouts/main_view', $data);
        } else {
          $this->session->set_flashdata('gagal', 'Data tidak Ditemukan');
            redirect(site_url('laporan_setoran'));
        }
    }

    public function create() 
    {
        $data = array(
                'titleCard'                 => 'Data Dashboard',
            'title'                     => 'Dashboard |',
            'judul'                     => 'Dashboard',
            'breadcrumbactive'          => '',
            'breadcrumb'                => 'Dashboard',
            'cekaktif'                  => 'dashboard',
            'link'                      => '',
                'button' => 'Create',
                'action' => site_url('laporan_setoran/create_action'),
            'id_laporan_setoran' => set_value('id_laporan_setoran'),
            'nama_laporan_setoran' => set_value('nama_laporan_setoran'),
            'prefix_kat' => set_value('prefix_kat'),
            'created_at' => set_value('created_at'),
            'edited_at' => set_value('edited_at'),
            'sts_rmv_laporan_setoran' => set_value('sts_rmv_laporan_setoran'),
	    );
         $data['datakonten'] = $this->load->view('tb_laporan_setoran_form', $data, true);
        $this->load->view('layouts/main_view', $data);

    }
    
    public function create_action(){
        $this->_rules();
        $dt = new DateTime();
        // $prefix = "DRV".getID();
        // // echo $gabung;
        // $row    = $this->laporan_setoran->get_by_prefix($prefix);
        // $NoUrut = (int) substr($row->max, 18, 4);
        // $NoUrut = $NoUrut + 1; //nomor urut +1
        // $NoUrut = sprintf('%04d', $NoUrut);
        // $fix    = $prefix . $NoUrut;
        if ($this->form_validation->run() == FALSE) {
            $this->session->set_flashdata('gagal', 'Gagal Menambahkan Data');
            redirect(site_url('laporan_setoran'));
        } else {
            $data = array(
                'laporan_setoran'           => $this->input->post('laporan_setoran',TRUE),
              
	        );

            $this->laporan_setoran->insert($data);
            $this->session->set_flashdata('berhasil', 'Berhasil Menambahkan Data');
            redirect(site_url('laporan_setoran'));
        }
    }
    
    public function update($id) 
    {
        $row = $this->laporan_setoran->get_by_id($id);

        if ($row) {
            $data = array(
             'titleCard'                 => 'Data Dashboard',
         'title'                     => 'Dashboard | Mitra',
        'judul'                     => 'Dashboard',
        'breadcrumbactive'          => '',
        'breadcrumb'                => 'Dashboard',
         'cekaktif'                  => 'dashboard',
         'link'                      => '',
                'button' => 'Update',
                'action' => site_url('laporan_setoran/update_action'),
		'id_laporan_setoran' => set_value('id_laporan_setoran', $row->id_laporan_setoran),
		'nama_laporan_setoran' => set_value('nama_laporan_setoran', $row->nama_laporan_setoran),
		'prefix_kat' => set_value('prefix_kat', $row->prefix_kat),
		'created_at' => set_value('created_at', $row->created_at),
		'edited_at' => set_value('edited_at', $row->edited_at),
		'sts_rmv_laporan_setoran' => set_value('sts_rmv_laporan_setoran', $row->sts_rmv_laporan_setoran),
	    );
               $data['datakonten'] = $this->load->view('tb_laporan_setoran_form', $data, true);
        $this->load->view('layouts/main_view', $data);

        } else {
            $this->session->set_flashdata('message', '<div class="alert alert-warning" role="alert">Data Tidak Ditemukan</div>');
            redirect(site_url('laporan_setoran'));
        }
    }
    
    public function update_action($id) 
    {
        $this->_rules();
        $dt = new DateTime();
        // if ($this->form_validation->run() == FALSE) {
        //     $this->update($id);
        // } else {
            $data = array(
		     'laporan_setoran'           => $this->input->post('laporan_setoran',TRUE),
         
	    );

            $this->laporan_setoran->update($id, $data);
         $this->session->set_flashdata('berhasil', 'Berhasil Merubah Data');
            redirect(site_url('laporan_setoran'));
        // }
    }
    
    public function delete($id) 
    {
        $row = $this->laporan_setoran->get_by_id($id);
        $dt = new DateTime();
        if ($row) {
              $data = array(
                 'is_versi_active' => 0,
                'sts_versi_rmv' => 0,
                 'tgl_update'        => $dt->format('Y-m-d H:i:s'),
            );
            $this->laporan_setoran->update($id, $data);
            $this->session->set_flashdata('berhasil', 'Berhasil Menghapus Data');
            redirect(site_url('laporan_setoran'));
        } else {
            $this->session->set_flashdata('gagal', 'Data Tidak Ditemukan');
            redirect(site_url('laporan_setoran'));
        }
    } 
    public function nonaktif($id) 
    {
        $row = $this->laporan_setoran->get_by_id($id);
        $dt = new DateTime();
        if ($row) {
              $data = array(
                'is_versi_active' => 0,
                 'tgl_update'        => $dt->format('Y-m-d H:i:s'),
            );
            $this->laporan_setoran->update($id, $data);
            $this->session->set_flashdata('berhasil', 'Berhasil Menghapus Data');
            redirect(site_url('laporan_setoran'));
        } else {
            $this->session->set_flashdata('gagal', 'Data Tidak Ditemukan');
            redirect(site_url('laporan_setoran'));
        }
    } 
     public function aktif($id) 
    {
        $row = $this->laporan_setoran->get_by_id($id);
        $dt = new DateTime();
        if ($row) {
              $data = array(
                'is_versi_active' => 1,
                 'tgl_update'        => $dt->format('Y-m-d H:i:s'),
            );
            $this->laporan_setoran->update($id, $data);
            $this->session->set_flashdata('berhasil', 'Berhasil Menghapus Data');
            redirect(site_url('laporan_setoran'));
        } else {
            $this->session->set_flashdata('gagal', 'Data Tidak Ditemukan');
            redirect(site_url('laporan_setoran'));
        }
    }

    public function _rules() 
    {
        $this->form_validation->set_rules('laporan_setoran', 'nama Jenis kendaraan', 'trim|required');
 
        $this->form_validation->set_rules('id_laporan_setoran', 'id_versi', 'trim');
        $this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

    public function ambilDataversi($id){
        $this->db->where('id_laporan_setoran', $id);
        $data = $this->db->get('laporan_setoran')->row();
        echo json_encode($data);
    }

}

/* End of file laporan_setoran.php */
/* Location: ./application/controllers/laporan_setoran.php */
/* Please DO NOT modify this information : */
/* Generated by Harviacode Codeigniter CRUD Generator 2020-07-15 09:43:37 */
/* http://harviacode.com */