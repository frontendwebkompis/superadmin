<div class="card card-<?= bgCard() ?>">
    <div class="card-header">
        <div class="card-title">
            <?= $titleCard ?>
        </div>
    </div>
    <div class="card-body">
        <div class="row">
            <div class="col-md-5 mb-2">
                <?= filterDate($this->input->get('awal'), $this->input->get('akhir'), base_url('laporan_setoran')) ?>
            </div>
            <div class="col-md-3 offset-md-4 mb-2">
                <?= search(site_url('laporan_setoran'), site_url('laporan_setoran'), $q) ?>
            </div>
        </div>
           
            <div class="tab-content p-0" style="overflow:auto">
                <table class="table table-bordered table-striped table-condensed table-hover table-sm" style="margin-bottom: 10px">
                    <tr>
                        <th class='text-center' style="width: 60px;">No</th>
                        <th class='text-center'>Tanggal Setoran Sampah </th>
                        <th class='text-center'>Nama Nasabah Online</th>
                        <th class='text-center'>Tanggal Penjemputan </th>
                        <th class='text-center'>Tanggal Review </th>
                        <th class='text-center' style="width: 120px;">Aksi</th>
                    </tr><?php
                    if ($total_rows == 0) {
                        echo '<tr><td colspan="11" bgcolor="grey" style="color: white; font-weight: bold; text-align: center;">Data Tidak Ditemukan</td></tr>';
                    } else {
                        foreach ($laporan_setoran_data as $laporan_setoran)
                        {
                            ?>
                    <tr>
                        <td class='text-center'><?php echo ++$start ?></td>
                        <td class='text-center'><?php echo fulldate($laporan_setoran->tgl_setoran) ?></td>
                        <td><?php echo $laporan_setoran->nama_nasabah_online ?></td>
                            <td><?php echo fulldate($laporan_setoran->waktu_penjemputan) ?></td>  
                            <td><?php echo fulldate($laporan_setoran->tgl_review) ?></td>
                        <td class='text-center'><div class="btn-group"><a href="<?php echo site_url('laporan_setoran/detail/'.$laporan_setoran->id_setoran_online); ?>"
                                    data-toogle="tooltip" title="Lihat"><button type="button" class="btn btn-info btn-sm"><i class="fas fa-info-circle"></i></button></a>
                            </div>
                                </td>
                    </tr>
                            <?php
                        }
                    }
                        ?>
                </table>
            </div>
            <?= footer($total_rows, $pagination, '') ?>
        </div>
    </div>
</div>

<div id="modalCreatelaporan_setoran" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Form Tambah laporan_setoran</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form action="<?= $action ?>" method="post" id="formTambahlaporan_setoran" role="form" enctype="multipart/form-data">
                <div class="row ml-3 mr-3">
                    <div class="col-md-12">
                       
                        <div class="form-group">
                            <label for="varchar">Nama Jenis Kendaraan</label>
                            <input type="text" class="form-control" name="nama_laporan_setoran" id="nama_laporan_setoran" placeholder="Nama Jenis Kendaraan" value="" />
                        </div>
                        
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <div class="col-md-12 text-center">
                    <button type="submit" class="btn btn-<?= bgCard() ?>" style="width: 100px;">Simpan</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal" style="width: 100px;">Batal</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="modalEditlaporan_setoran" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Form Edit laporan_setoran</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form action="<?= $action ?>" method="post" id="formEditlaporan_setoran" role="form" enctype="multipart/form-data">
                <div class="row ml-3 mr-3">
                    <div class="col-md-12">
                           <div class="form-group">
                            <label for="varchar">laporan_setoran</label>
                            <input type="text" class="form-control" name="nama_laporan_setoran" id="nama_laporan_setoran" placeholder="laporan_setoran" value="" />
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <div class="col-md-12 text-center">
                    <button type="submit" class="btn btn-<?= bgCard() ?>" style="width: 100px;">Simpan</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal" style="width: 100px;">Batal</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    $(function(){
        setDateFilter('akhir','awal')

        $('#modalCreatelaporan_setoran #formTambahlaporan_setoran').validate({
            rules: {
                nama_laporan_setoran : {
                    required : true
                },
            },
            messages: {
                nama_laporan_setoran : {
                    required : "Nama laporan_setoran Tidak Boleh Kosong"
                },
            },
            errorElement: 'span',
            errorPlacement: function(error, element) {
                error.addClass('invalid-feedback');
                element.closest('.form-group').append(error);
            },
            highlight: function(element, errorClass, validClass) {
                $(element).addClass('is-invalid');
            },
            unhighlight: function(element, errorClass, validClass) {
                $(element).removeClass('is-invalid');
            }
        })
        $('#modalEditlaporan_setoran #formEditlaporan_setoran').validate({
            rules: {
                nama_laporan_setoran : {
                    required : true
                },
            },
            messages: {
                nama_laporan_setoran : {
                    required : "Nama laporan_setoran Tidak Boleh Kosong"
                },
            },
            errorElement: 'span',
            errorPlacement: function(error, element) {
                error.addClass('invalid-feedback');
                element.closest('.form-group').append(error);
            },
            highlight: function(element, errorClass, validClass) {
                $(element).addClass('is-invalid');
            },
            unhighlight: function(element, errorClass, validClass) {
                $(element).removeClass('is-invalid');
            }
        })

        $('#modalCreatelaporan_setoran #keterangan_laporan_setoran').select2({
            theme: 'bootstrap'
        })

        $('#modalEditlaporan_setoran #edit_keterangan_laporan_setoran').select2({
            theme: 'bootstrap'
        })
    })

    $('#modalCreatelaporan_setoran').on('hidden.bs.modal', function(){
        $('#modalCreatelaporan_setoran #is_read_aplikasi').val('')
        $('#modalCreatelaporan_setoran #is_read_aplikasi').removeClass('is-invalid')
        $('#modalCreatelaporan_setoran #keterangan_laporan_setoran').val('').trigger('change')
        $('#modalCreatelaporan_setoran #keterangan_laporan_setoran').removeClass('is-invalid')
        $('#modalCreatelaporan_setoran #nama_laporan_setoran').val('')
        $('#modalCreatelaporan_setoran #nama_laporan_setoran').removeClass('is-invalid')
    })

    $('#modalEditlaporan_setoran').on('hidden.bs.modal', function(){
        $('#modalEditlaporan_setoran #is_read_aplikasi').val('')
        $('#modalEditlaporan_setoran #is_read_aplikasi').removeClass('is-invalid')
        $('#modalEditlaporan_setoran #edit_keterangan_laporan_setoran').val('').trigger('change')
        $('#modalEditlaporan_setoran #edit_keterangan_laporan_setoran').removeClass('is-invalid')
        $('#modalEditlaporan_setoran #laporan_setoran').val('')
        $('#modalEditlaporan_setoran #laporan_setoran').removeClass('is-invalid')
    })

    function cekEditlaporan_setoran(id){
        startloading('#modalEditlaporan_setoran .modal-content')
        $('#modalEditlaporan_setoran #formEditlaporan_setoran').attr('action', '<?= base_url('laporan_setoran/update_action/') ?>'+id)
        $.ajax({
            url: '<?php echo base_url() ?>laporan_setoran/ambilDatalaporan_setoran/' + id,
            dataType: "json",
            cache: false,
            success: function(res) {
                $('#modalEditlaporan_setoran #nama_laporan_setoran').val(res.nama_laporan_setoran)
               
                stoploading('#modalEditlaporan_setoran .modal-content')
            }
        });
    }
  
    function cekDeletelaporan_setoran(id){
        delConf(base_url('laporan_setoran/delete/'+id))
    }
</script>