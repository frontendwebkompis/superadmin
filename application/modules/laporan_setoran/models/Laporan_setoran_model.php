<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Laporan_setoran_model extends CI_Model
{

    public $vtable = 'v_setoran_online';
    public $table = 'setoran_online';
    public $id = 'id_setoran_online';
    public $vtable_nasabah = 'v_tarik_saldo_nasabah';
    public $vtable_bank = 'v_tarik_saldo_banksampah';
    public $table_peng = 'biller_pengepul';
    public $table_nas = 'biller_nasabah';
    public $table_bs = 'biller_banksampah';
   
    public $order = 'DESC';

    function __construct()
    {
        parent::__construct();
    }

    // get all
    function get_all()
    {
        $this->db->order_by($this->id, $this->order);
        return $this->db->get($this->table)->result();
    }
public function get_by_prefix($prefix = '')
    {
        $query = $this->db->query("SELECT max(".$this->id.") AS max FROM ".$this->table." WHERE ".$this->id." LIKE '$prefix%'");
        return $query->row();
    }
    // get data by id
    function get_by_id($id)
    {
        $this->db->join('bank_sampah', 'bank_sampah.id_bank_sampah = setoran_online.id_bank_sampah');
        $this->db->join('nasabah_online', 'nasabah_online.id_nasabah_online = setoran_online.id_nasabah_online');
        $this->db->join('status_setoran', 'status_setoran.id_status_setoran = setoran_online.id_status_setoran');
        $this->db->join('jenis_transaksi', 'jenis_transaksi.id_jenis_transaksi = setoran_online.id_jenis_transaksi');
        $this->db->join('reason', 'reason.id_reason = setoran_online.id_reason');
        $this->db->where($this->id, $id);
        return $this->db->get($this->table)->row();
    }
    
   
         // get data with limit and search
    function get_limit_detail_nasabah($id,$limit, $start = 0, $q = NULL) {
       $this->db->join('kategori_sampah', 'kategori_sampah.id_kategori_sampah = setoran_online_detail.id_kategori_sampah', 'left');
        $this->db->join('jenis_sampah', 'jenis_sampah.id_jenis_sampah = setoran_online_detail.id_jenis_sampah', 'left');
        $this->db->join('merk_sampah', 'merk_sampah.id_merk = setoran_online_detail.id_merk', 'left');

        $this->db->where('id_setoran_online', $id);
        $this->db->group_start();
        $this->db->or_like('nama_kategori_sampah', $q);
        $this->db->or_like('nama_jenis_sampah', $q);
        $this->db->or_like('nama_merk', $q);
        $this->db->group_end();
        $this->db->limit($limit, $start);
        return $this->db->get('setoran_online_detail')->result();
    } 
     // get total rows
    function total_rows_detail($id,$q = NULL) {
        $this->db->join('kategori_sampah', 'kategori_sampah.id_kategori_sampah = setoran_online_detail.id_kategori_sampah', 'left');
        $this->db->join('jenis_sampah', 'jenis_sampah.id_jenis_sampah = setoran_online_detail.id_jenis_sampah', 'left');
        $this->db->join('merk_sampah', 'merk_sampah.id_merk = setoran_online_detail.id_merk', 'left');

        $this->db->where('id_setoran_online', $id);
        $this->db->group_start();
        $this->db->or_like('nama_kategori_sampah', $q);
        $this->db->or_like('nama_jenis_sampah', $q);
        $this->db->or_like('nama_merk', $q);
        $this->db->group_end();
        $this->db->from('setoran_online_detail');
        return $this->db->count_all_results();
    }
      // get total rows
    function total_rows($q = NULL,$awal,$akhir) {
         $this->db->join('nasabah_online','nasabah_online.id_nasabah_online='. $this->table .'.id_nasabah_online');
        $this->db->where("(UNIX_TIMESTAMP(". $this->table .".tgl_setoran) BETWEEN UNIX_TIMESTAMP('" . $awal . "') AND UNIX_TIMESTAMP('" . $akhir  . " 23:59') ) ");
        $this->db->group_start();
        $this->db->or_like('nama_nasabah_online', $q);
        $this->db->group_end();
        // $this->db->group_by('id_pembelian_pengepul');
        $this->db->from($this->table);
        return $this->db->count_all_results();
    }

    // get data with limit and search
    function get_limit_data($limit, $start = 0, $q = NULL,$awal,$akhir) {
        $this->db->join('nasabah_online','nasabah_online.id_nasabah_online='. $this->table .'.id_nasabah_online');
        $this->db->where("(UNIX_TIMESTAMP(". $this->table .".tgl_setoran) BETWEEN UNIX_TIMESTAMP('" . $awal . "') AND UNIX_TIMESTAMP('" . $akhir  . " 23:59') ) ");
        $this->db->order_by('tgl_setoran', $this->order);
        $this->db->group_start();
        $this->db->or_like('nama_nasabah_online', $q);
        $this->db->group_end();
        // $this->db->group_by('id_pembelian_pengepul');
        $this->db->limit($limit, $start);
        return $this->db->get($this->table)->result();
    }

    // insert data
    function insert($data)
    {
        $this->db->insert($this->table, $data);
    }

    // update data
    function update($id, $data)
    {
        $this->db->where($this->id, $id);
        $this->db->update($this->table, $data);
    }

    // delete data
    function delete($id)
    {
        $this->db->where($this->id, $id);
        $this->db->delete($this->table);
    }

}

/* End of file Kategori_produk_model.php */
/* Location: ./application/models/Kategori_produk_model.php */
/* Please DO NOT modify this information : */
/* Generated by Harviacode Codeigniter CRUD Generator 2020-07-15 09:43:37 */
/* http://harviacode.com */