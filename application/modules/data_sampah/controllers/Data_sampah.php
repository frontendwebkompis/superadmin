<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Data_sampah extends CI_Controller
{

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Data_sampah_model');
		$this->load->library('form_validation');
	}

	public function index()
	{
		$model = $this->Data_sampah_model;
		$q = urldecode($this->input->get('q', TRUE));
		$start = intval($this->input->get('start'));

		if ($q <> '') {
			$config['base_url'] = base_url() . 'data_sampah/index.html?q=' . urlencode($q);
			$config['first_url'] = base_url() . 'data_sampah/index.html?q=' . urlencode($q);
		} else {
			$config['base_url'] = base_url() . 'data_sampah/index.html';
			$config['first_url'] = base_url() . 'data_sampah/index.html';
		}

		$config['per_page'] = 10;
		$config['page_query_string'] = TRUE;
		$config['total_rows'] = 0; //$model->total_rows($q);
		$data_sampah = []; //$model->get_limit_data($config['per_page'], $start, $q);

		$this->load->library('pagination');
		$this->pagination->initialize($config);

		$data = array(
			'tittle'    					=> 'Data Sampah',
			'titleCard'    					=> 'Data Sampah List',
			'judul'     					=> 'Data Sampah',
			'data_sampah_data' 			=> $data_sampah,
			'q' 							=> $q,
			'pagination' 					=> $this->pagination->create_links(),
			'total_rows' 					=> $config['total_rows'],
			'start' 						=> $start,
			'menu_aktif'					=> 'data_sampah'
		);
		$res['datakonten'] = $this->load->view('data_sampah/data_sampah_list', $data, true);
		$this->load->view('layouts/main_view', $res);
	}
}

/* End of file data_sampah.php */
