<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Kategori_sampah extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        if (cek_token()) {
            $this->load->model('Kategori_sampah_model');
            $this->load->library('form_validation');
        } else {
            logout();
        }
    }

    public function index()
    {
        $q = urldecode($this->input->get('q', TRUE));
        $start = intval($this->input->get('start'));

        if ($q <> '') {
            $config['base_url'] = base_url() . 'kategori_sampah/?q=' . urlencode($q);
            $config['first_url'] = base_url() . 'kategori_sampah/?q=' . urlencode($q);
        } else {
            $config['base_url'] = base_url() . 'kategori_sampah/';
            $config['first_url'] = base_url() . 'kategori_sampah/';
        }

        $config['per_page'] = 10;
        $config['page_query_string'] = TRUE;
        $config['total_rows'] = $this->Kategori_sampah_model->total_rows($q);
        $kategori_sampah = $this->Kategori_sampah_model->get_limit_data($config['per_page'], $start, $q);

        $this->load->library('pagination');
        $this->pagination->initialize($config);

        $data = array(
            'tittle'                => 'Kategori Sampah',
            'judul'                 => 'Kategori Sampah',
            'title_card'            => 'List Kategori Sampah',
            'kategori_sampah_data'  => $kategori_sampah,
            'q'                     => $q,
            'pagination'            => $this->pagination->create_links(),
            'total_rows'            => $config['total_rows'],
            'start'                 => $start,
            'kategori_sampah_aktif' => '1',
            'menu_aktif'            => 'kategori_sampah'
        );
        $res['datakonten'] = $this->load->view('kategori_sampah/kategori_sampah_list', $data, true);
        $this->load->view('layouts/main_view', $res);
    }

    public function read($id)
    {
        $row = $this->Kategori_sampah_model->get_by_id($id);
        if ($row) {
            $data = array(
                'tittle'                => 'Kategori Sampah',
                'judul'                 => 'Kategori Sampah',
                'title_card'            => 'Detail Kategori Sampah',
                'id_kategori_sampah'    => $row->id_kategori_sampah,
                'nama_kategori_sampah'  => $row->nama_kategori_sampah,
                'nama_treatment_sampah' => $row->nama_treatment_sampah,
                'kategori_sampah_aktif' => '1',
                'menu_aktif'            => 'kategori_sampah'
            );
            $res['datakonten'] = $this->load->view('kategori_sampah/kategori_sampah_read', $data, true);
            $this->load->view('layouts/main_view', $res);
        } else {
            $this->session->set_flashdata('gagal', 'Data Tidak Di Temukan');
            redirect(site_url('kategori_sampah'));
        }
    }

    public function create()
    {
        $treatment = $this->Kategori_sampah_model->get_treat_all();
        $data = array(
            'tittle'                        => 'Kategori Sampah',
            'judul'                         => 'Kategori Sampah',
            'title_card'                    => 'Tambah Kategori Sampah',
            'button'                        => 'Create',
            'treatment'                     => $treatment,
            'id_treatment_sampah'           => set_value('id_treatment_sampah'),
            'action'                        => site_url('kategori_sampah/create_action'),
            'id_kategori_sampah'            => set_value('id_kategori_sampah'),
            'nama_kategori_sampah'          => set_value('nama_kategori_sampah'),
            'kategori_sampah_aktif'         => '1',
            'menu_aktif'                    => 'kategori_sampah'
        );

        $res['datakonten'] = $this->load->view('kategori_sampah/kategori_sampah_form', $data, true);
        $this->load->view('layouts/main_view', $res);
    }

    public function create_action()
    {
        $this->_rules();
        $dt = new DateTime();
        $prefix = "KAS";
        // echo $gabung;
        $row = $this->Kategori_sampah_model->get_by_prefix($prefix);
        $NoUrut = (int) substr($row->max, 3, 4);
        $NoUrut = $NoUrut + 1; //nomor urut +1
        $NoUrut = sprintf('%04d', $NoUrut);
        $fix = $prefix . $NoUrut;
        if ($this->form_validation->run() == FALSE) {
            $this->create();
        } else {
            $data = array(
                'id_kategori_sampah'            => $fix,
                'nama_kategori_sampah'          => $this->input->post('nama_kategori_sampah', TRUE),
                'id_treatment_sampah'           => $this->input->post('id_treatment_sampah', TRUE),
                'id_superadmin'                 => getID(),
                'created_at'                    => @date('Y-m-d H:i:s'),
                'status_kategori_sampah_remove' => '1',
            );

            $this->Kategori_sampah_model->insert($data);
            $this->session->set_flashdata('berhasil', 'Berhasil Menambahkan Data');
            redirect(site_url('kategori_sampah'));
        }
    }

    public function update($id)
    {
        $row = $this->Kategori_sampah_model->get_by_id($id);
        $treatment = $this->Kategori_sampah_model->get_treat_all();


        if ($row) {
            $data = array(
                'tittle'                => 'Kategori Sampah',
                'judul'                 => 'Kategori Sampah',
                'title_card'            => 'Update Kategori Sampah',
                'button'                => 'Update',
                'treatment'             => $treatment,
                'action'                => site_url('kategori_sampah/update_action'),
                'id_kategori_sampah'    => set_value('id_kategori_sampah', $row->id_kategori_sampah),
                'id_treatment_sampah'   => set_value('id_treatment_sampah', $row->id_treatment_sampah),
                'nama_kategori_sampah'  => set_value('nama_kategori_sampah', $row->nama_kategori_sampah),
                'kategori_sampah_aktif' => '1',
                'menu_aktif'            => 'kategori_sampah'
            );
            $res['datakonten'] = $this->load->view('kategori_sampah/kategori_sampah_form', $data, true);
            $this->load->view('layouts/main_view', $res);
        } else {
            $this->session->set_flashdata('gagal', 'Data Tidak Ditemukan');
            redirect(site_url('kategori_sampah'));
        }
    }

    public function update_action()
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->update($this->input->post('id_kategori_sampah', TRUE));
        } else {
            $data = array(
                'nama_kategori_sampah'  => $this->input->post('nama_kategori_sampah', TRUE),
                'id_treatment_sampah'   => $this->input->post('id_treatment_sampah', TRUE),
                'id_superadmin'         => $this->session->userdata('uid'),
            );

            $this->Kategori_sampah_model->update($this->input->post('id_kategori_sampah', TRUE), $data);
            $this->session->set_flashdata('berhasil', 'Berhasil Merubah Data');
            redirect(site_url('kategori_sampah'));
        }
    }

    public function del_sem($id)
    {
        $row = $this->Kategori_sampah_model->get_by_id($id);

        if ($row) {
            $data = array(
                'status_kategori_sampah_remove' => 0,
                'id_superadmin' => $this->session->userdata('uid'),
            );

            $this->Kategori_sampah_model->update($id, $data);

            $this->session->set_flashdata('berhasil', 'Berhasil Menghapus Data');
            redirect(site_url('kategori_sampah'));
        } else {
            $this->session->set_flashdata('gagal', 'Data Tidak Ditemukan');
            redirect(site_url('kategori_sampah'));
        }
    }


    public function _rules()
    {
        $this->form_validation->set_rules('nama_kategori_sampah', 'nama kategori sampah', 'trim|required');

        $this->form_validation->set_rules('id_kategori_sampah', 'id_kategori_sampah', 'trim');
        $this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

    public function excel()
    {
        $this->load->helper('exportexcel');
        $namaFile = "kategori_sampah.xls";
        $judul = "kategori_sampah";
        $tablehead = 0;
        $tablebody = 1;
        $nourut = 1;
        //penulisan header
        header("Pragma: public");
        header("Expires: 0");
        header("Cache-Control: must-revalidate, post-check=0,pre-check=0");
        header("Content-Type: application/force-download");
        header("Content-Type: application/octet-stream");
        header("Content-Type: application/download");
        header("Content-Disposition: attachment;filename=" . $namaFile . "");
        header("Content-Transfer-Encoding: binary ");

        xlsBOF();

        $kolomhead = 0;
        xlsWriteLabel($tablehead, $kolomhead++, "No");
        xlsWriteLabel($tablehead, $kolomhead++, "Nama Kategori Sampah");

        foreach ($this->Kategori_sampah_model->get_all() as $data) {
            $kolombody = 0;

            //ubah xlsWriteLabel menjadi xlsWriteNumber untuk kolom numeric
            xlsWriteNumber($tablebody, $kolombody++, $nourut);
            xlsWriteLabel($tablebody, $kolombody++, $data->nama_kategori_sampah);

            $tablebody++;
            $nourut++;
        }

        xlsEOF();
        exit();
    }
    public function exportxl()
    {
        $q          = $this->input->get('q');
        $subject    = "Kategori";
        // $id         = $this->session->userdata('uid');
        $sql        ="SELECT * FROM `kategori_sampah` JOIN `treatment_sampah` ON `treatment_sampah`.`id_treatment_sampah` = `kategori_sampah`.`id_treatment_sampah` WHERE `status_kategori_sampah_remove` = 1";
        exportSQL('Kategori', $subject, $sql);
    }
}

/* End of file Kategori_sampah.php */
/* Location: ./application/controllers/Kategori_sampah.php */
/* Please DO NOT modify this information : */
/* Generated by Harviacode Codeigniter CRUD Generator 2020-02-07 19:00:53 */
/* http://harviacode.com */
