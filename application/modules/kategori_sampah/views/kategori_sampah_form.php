<!-- ---------------------------------Konten-------------------------------------------------- -->
<div class="row">
	<section class="col-lg connectedSortable ui-sortable">
		<div class="card card-info">
			<div class="card-header">
                <div class="card-title">
                  <?php echo $title_card?>
                </div>
            </div>
			<div class="card-body">
				<div class="tab-content p-0">

					<form action="<?php echo $action; ?>" method="post" id="dataform">
						<div class="form-group">
							<label for="varchar">Nama Kategori Sampah
								<?php echo form_error('nama_kategori_sampah') ?></label>
							<input type="text" class="form-control" name="nama_kategori_sampah"
								id="nama_kategori_sampah" placeholder="Nama Kategori Sampah"
								value="<?php echo $nama_kategori_sampah; ?>" />
						</div>
						<div class="form-group">
						<label for="varchar">Pilih Treatment Sampah</label>
							<select id="id_treatment_sampah" class="form-control" name="id_treatment_sampah">
								<option value="">Pilih Treatment Sampah </option>
								<?php foreach ($treatment as $treat){ ?>
								<option <?=  $treat->id_treatment_sampah ==  $id_treatment_sampah ? 'selected' : ''; ?>
								value="<?php echo $treat->id_treatment_sampah; ?>">
								<?php echo $treat->nama_treatment_sampah; ?>
								</option>
								<?php }?>
							</select>
						</label>	
						</div>
						<input type="hidden" name="id_kategori_sampah" value="<?php echo $id_kategori_sampah; ?>" />
											
						<button type="submit" class="btn btn-info" style="width: 100px;" id="btn-simpan">Simpan</button>
						<a href="<?php echo site_url('kategori_sampah') ?>" class="btn btn-default" style="width: 100px;">Batal</a>
					</form>

				</div>
			</div>
		</div>
	</section>
</div>

<script type="text/javascript">
    $(document).ready(function () {
      $('#dataform').validate({
        rules: {
          nama_kategori_sampah: {
            required: true,
          },
          id_treatment_sampah: {
          	required: true,
          }
        },
        messages: {
          nama_kategori_sampah: {
            required: "Kategori Sampah Tidak Boleh Kosong",
          },
          id_treatment_sampah: {
          	required: "Treatment Sampah Tidak Boleh Kosong",
          }
        },
        submitHandler: function(form) {
          $('#btn-simpan').prop('disabled', true);
          form.submit();
        },
        errorElement: 'span',
        errorPlacement: function (error, element) {
          error.addClass('invalid-feedback');
          element.closest('.form-group').append(error);
        },
        highlight: function (element, errorClass, validClass) {
          $(element).addClass('is-invalid');
        },
        unhighlight: function (element, errorClass, validClass) {
          $(element).removeClass('is-invalid');
        },
      });
    });
</script>
