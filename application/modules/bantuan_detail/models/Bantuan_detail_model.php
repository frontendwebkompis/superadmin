<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Bantuan_detail_model extends CI_Model
{

    public $table = 'bantuan';
    public $id = 'id_bantuan';
    public $order = 'DESC';

    function __construct()
    {
        parent::__construct();
    }

    // get all
    function get_all()
    {
        $this->db->order_by($this->id, $this->order);
        return $this->db->get($this->table)->result();
    }
    // get data by id
    function get_by_poto($id)
    {
        $this->db->where('capture_bantuan', $id);
        return $this->db->get($this->table)->row();
    } 
    function get_by_order($id)
    {
    
        return $this->db->select('*')->order_by('order_id',"desc")->where('id_menu_bantuan',$id)->limit(1)->get($this->table)->row();
    }
    // get data by id
    function get_by_id($id)
    {
        $this->db->select('bantuan.*,menu_bantuan.nama_menu_bantuan,menu_bantuan.id_menu_bantuan');
        $this->db->join('menu_bantuan', 'menu_bantuan.id_menu_bantuan = bantuan.id_menu_bantuan');

        $this->db->where($this->id, $id);
        return $this->db->get($this->table)->row();
    }
    public function get_by_prefix($prefix = '')
    {
        # code..
        $query = $this->db->query("SELECT max(" . $this->id . ") AS max FROM " . $this->table . " WHERE " . $this->id . " LIKE '$prefix%'");
        return $query->row();
    }

 
    // get total rows
    function total_rows($q = NULL, $id)
    {
        $this->db->join('menu_bantuan', 'menu_bantuan.id_menu_bantuan = bantuan.id_menu_bantuan');
        $this->db->where('bantuan.id_menu_bantuan', $id);
  $this->db->where('bantuan.status_bantuan_remove', 1);
        $this->db->group_start();
        $this->db->or_like('judul_bantuan', $q);
        $this->db->or_like('deskripsi_bantuan_detail', $q);
        $this->db->or_like('capture_bantuan', $q);
        $this->db->or_like('status_user', $q);

        // $this->db->or_like('status_bantuan_aktif', $q);
        $this->db->or_like('status_bantuan_remove', $q);
        $this->db->or_like('bantuan.created_at', $q);
        $this->db->or_like('bantuan.update_at', $q);
        $this->db->group_end();
        $this->db->from($this->table);
        return $this->db->count_all_results();
    }


function get_oder_all($id)
{
    
        $this->db->where('bantuan.id_menu_bantuan', $id);
          return $this->db->get($this->table)->result();
}

    // get data with limit and search
    function get_limit_data($limit, $start = 0, $q = NULL, $id)
    {
            $this->db->select('bantuan.*,menu_bantuan.id_menu_bantuan,menu_bantuan.nama_menu_bantuan');
        $this->db->join('menu_bantuan', 'menu_bantuan.id_menu_bantuan = bantuan.id_menu_bantuan');
        $this->db->where('bantuan.id_menu_bantuan', $id);
 $this->db->where('bantuan.status_bantuan_remove', 1);
        $this->db->order_by('order_id', 'asc');
        $this->db->group_start();
        $this->db->or_like('judul_bantuan', $q);
        $this->db->or_like('deskripsi_bantuan_detail', $q);
        $this->db->or_like('capture_bantuan', $q);
        $this->db->or_like('status_user', $q);

        // $this->db->or_like('status_bantuan_aktif', $q);
        $this->db->or_like('status_bantuan_remove', $q);
        $this->db->or_like('bantuan.created_at', $q);
        $this->db->or_like('bantuan.update_at', $q);
        $this->db->group_end();
        $this->db->limit($limit, $start);
        return $this->db->get($this->table)->result();
    }
      // get total rows
    function total_rows_nasabah($q = NULL, $id)
    {
        $this->db->join('menu_bantuan', 'menu_bantuan.id_menu_bantuan = bantuan.id_menu_bantuan');
        $this->db->where('bantuan.id_menu_bantuan', $id);
 $this->db->where('bantuan.status_user', 2);
        $this->db->group_start();
        $this->db->or_like('judul_bantuan', $q);
        $this->db->or_like('deskripsi_bantuan_detail', $q);
        $this->db->or_like('capture_bantuan', $q);
        $this->db->or_like('status_user', $q);
          $this->db->or_like('status_bantuan_remove', $q);
        $this->db->or_like('bantuan.created_at', $q);
        $this->db->or_like('bantuan.update_at', $q);
        $this->db->group_end();
        $this->db->from($this->table);
        return $this->db->count_all_results();
    }

     // get data with limit and search
    function get_limit_data_nasabah($limit, $start = 0, $q = NULL, $id)
    {
            $this->db->select('bantuan.*,menu_bantuan.id_menu_bantuan,menu_bantuan.nama_menu_bantuan');
      
        $this->db->join('menu_bantuan', 'menu_bantuan.id_menu_bantuan = bantuan.id_menu_bantuan');
        $this->db->where('bantuan.id_menu_bantuan', $id);
 $this->db->where('bantuan.status_user', 2);

        $this->db->order_by('order_id', 'asc');
        $this->db->group_start();
        $this->db->or_like('judul_bantuan', $q);
        $this->db->or_like('deskripsi_bantuan_detail', $q);
        $this->db->or_like('capture_bantuan', $q);
        $this->db->or_like('status_user', $q);

        $this->db->or_like('status_bantuan_remove', $q);
        $this->db->or_like('bantuan.created_at', $q);
        $this->db->or_like('bantuan.update_at', $q);
        $this->db->group_end();
        $this->db->limit($limit, $start);
        return $this->db->get($this->table)->result();
    }
      // get total rows
    function total_rows_pengepul($q = NULL, $id)
    {
        $this->db->join('menu_bantuan', 'menu_bantuan.id_menu_bantuan = bantuan.id_menu_bantuan');
        $this->db->where('bantuan.id_menu_bantuan', $id);
 $this->db->where('bantuan.status_user', 3);
        $this->db->group_start();
        $this->db->or_like('judul_bantuan', $q);
        $this->db->or_like('deskripsi_bantuan_detail', $q);
        $this->db->or_like('capture_bantuan', $q);
        $this->db->or_like('status_user', $q);
          $this->db->or_like('status_bantuan_remove', $q);
        $this->db->or_like('bantuan.created_at', $q);
        $this->db->or_like('bantuan.update_at', $q);
        $this->db->group_end();
        $this->db->from($this->table);
        return $this->db->count_all_results();
    }

     // get data with limit and search
    function get_limit_data_pengepul($limit, $start = 0, $q = NULL, $id)
    {
            $this->db->select('bantuan.*,menu_bantuan.id_menu_bantuan,menu_bantuan.nama_menu_bantuan');
      
        $this->db->join('menu_bantuan', 'menu_bantuan.id_menu_bantuan = bantuan.id_menu_bantuan');
        $this->db->where('bantuan.id_menu_bantuan', $id);
 $this->db->where('bantuan.status_user', 3);

        $this->db->order_by('order_id', 'asc');
        $this->db->group_start();
        $this->db->or_like('judul_bantuan', $q);
        $this->db->or_like('deskrispi_bantuan_detail', $q);
        $this->db->or_like('capture_bantuan', $q);
        $this->db->or_like('status_user', $q);

        // $this->db->or_like('status_bantuan_aktif', $q);
        $this->db->or_like('status_bantuan_remove', $q);
        $this->db->or_like('bantuan.created_at', $q);
        $this->db->or_like('bantuan.update_at', $q);
        $this->db->group_end();
        $this->db->limit($limit, $start);
        return $this->db->get($this->table)->result();
    }
   
    // insert data
    function insert($data)
    {
        $this->db->insert($this->table, $data);
    }

    // insert data
    function insertdetail($data)
    {
        $this->db->insert('bantuan_detail', $data);
    }

    // update data
    function update($id, $data)
    {
        $this->db->where($this->id, $id);
        $this->db->update($this->table, $data);
    }  

    // update data order
    function updateorder($id, $data)
    {
        $this->db->where('order_id', $id);
        $this->db->update($this->table, $data);
    }

    // delete data
    function delete($id)
    {
        $this->db->where($this->id, $id);
        $this->db->delete($this->table);
    }
}

/* End of file Bantuan_model.php */
/* Location: ./application/models/Bantuan_model.php */
/* Please DO NOT modify this information : */
/* Generated by Harviacode Codeigniter CRUD Generator 2020-03-17 08:02:29 */
/* http://harviacode.com */
