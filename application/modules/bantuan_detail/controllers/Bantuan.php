<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Bantuan extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Bantuan_model');
        $this->load->library('form_validation');
    }

    public function index()
    {
        $q = urldecode($this->input->get('q', TRUE));
        $start = intval($this->input->get('start'));
        
        if ($q <> '') {
            $config['base_url'] = base_url() . 'bantuan/index.html?q=' . urlencode($q);
            $config['first_url'] = base_url() . 'bantuan/index.html?q=' . urlencode($q);
        } else {
            $config['base_url'] = base_url() . 'bantuan/index.html';
            $config['first_url'] = base_url() . 'bantuan/index.html';
        }

        $config['per_page'] = 10;
        $config['page_query_string'] = TRUE;
        $config['total_rows'] = $this->Bantuan_model->total_rows($q);
        $bantuan = $this->Bantuan_model->get_limit_data($config['per_page'], $start, $q);

        $this->load->library('pagination');
        $this->pagination->initialize($config);

        $data = array(
            'bantuan_data' => $bantuan,
            'q' => $q,
            'pagination' => $this->pagination->create_links(),
            'total_rows' => $config['total_rows'],
            'start' => $start,
        );
        $this->load->view('bantuan/bantuan_list', $data);
    }

    public function detail($id) 
    {
        $row = $this->Bantuan_model->get_by_id($id);
        if ($row) {
            $data = array(
		'id_bantuan' => $row->id_bantuan,
		'id_menu_bantuan' => $row->id_menu_bantuan,
		'judul_bantuan' => $row->judul_bantuan,
		'deskrispi_bantuan_detail' => $row->deskrispi_bantuan_detail,
		'capture_bantuan' => $row->capture_bantuan,
		'status_user' => $row->status_user,
		'order_id' => $row->order_id,
		'status_bantuan_aktif' => $row->status_bantuan_aktif,
		'status_bantuan_remove' => $row->status_bantuan_remove,
		'created_at' => $row->created_at,
		'update_at' => $row->update_at,
	    );
            $this->load->view('bantuan/bantuan_detail', $data);
        } else {
            $this->session->set_flashdata('message', '<div class="alert alert-warning" role="alert">
  Data Tidak Di Temukan
</div>');
            redirect(site_url('bantuan'));
        }
    }

    public function create() 
    {
        $data = array(
            'button' => 'Create',
            'action' => site_url('bantuan/create_action'),
	    'id_bantuan' => set_value('id_bantuan'),
	    'id_menu_bantuan' => set_value('id_menu_bantuan'),
	    'judul_bantuan' => set_value('judul_bantuan'),
	    'deskrispi_bantuan_detail' => set_value('deskrispi_bantuan_detail'),
	    'capture_bantuan' => set_value('capture_bantuan'),
	    'status_user' => set_value('status_user'),
	    'order_id' => set_value('order_id'),
	    'status_bantuan_aktif' => set_value('status_bantuan_aktif'),
	    'status_bantuan_remove' => set_value('status_bantuan_remove'),
	    'created_at' => set_value('created_at'),
	    'update_at' => set_value('update_at'),
	);
        $this->load->view('bantuan/bantuan_form', $data);
    }
    
    public function create_action() 
    {
        $this->_rules();

     

        if ($this->form_validation->run() == FALSE) {
            $this->create();
        } else {
            $data = array(
		'id_menu_bantuan' => $this->input->post('id_menu_bantuan',TRUE),
		'judul_bantuan' => $this->input->post('judul_bantuan',TRUE),
		'deskrispi_bantuan_detail' => $this->input->post('deskrispi_bantuan_detail',TRUE),
		'capture_bantuan' => $this->input->post('capture_bantuan',TRUE),
		'status_user' => $this->input->post('status_user',TRUE),
		'order_id' => $this->input->post('order_id',TRUE),
		'status_bantuan_aktif' => $this->input->post('status_bantuan_aktif',TRUE),
		'status_bantuan_remove' => $this->input->post('status_bantuan_remove',TRUE),
		'created_at' => $this->input->post('created_at',TRUE),
		'update_at' => $this->input->post('update_at',TRUE),
	    );

            $this->Bantuan_model->insert($data);
            $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">Berhasil Menambahkan Data </div>');
            redirect(site_url('bantuan'));
        }
    }
    
    public function update($id) 
    {
        $row = $this->Bantuan_model->get_by_id($id);

        if ($row) {
            $data = array(
                'button' => 'Update',
                'action' => site_url('bantuan/update_action'),
		'id_bantuan' => set_value('id_bantuan', $row->id_bantuan),
		'id_menu_bantuan' => set_value('id_menu_bantuan', $row->id_menu_bantuan),
		'judul_bantuan' => set_value('judul_bantuan', $row->judul_bantuan),
		'deskrispi_bantuan_detail' => set_value('deskrispi_bantuan_detail', $row->deskrispi_bantuan_detail),
		'capture_bantuan' => set_value('capture_bantuan', $row->capture_bantuan),
		'status_user' => set_value('status_user', $row->status_user),
		'order_id' => set_value('order_id', $row->order_id),
		'status_bantuan_aktif' => set_value('status_bantuan_aktif', $row->status_bantuan_aktif),
		'status_bantuan_remove' => set_value('status_bantuan_remove', $row->status_bantuan_remove),
		'created_at' => set_value('created_at', $row->created_at),
		'update_at' => set_value('update_at', $row->update_at),
	    );
            $this->load->view('bantuan/bantuan_form', $data);
        } else {
            $this->session->set_flashdata('message', '<div class="alert alert-warning" role="alert">Data Tidak Ditemukan</div>');
            redirect(site_url('bantuan'));
        }
    }
    
    public function update_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->update($this->input->post('id_bantuan', TRUE));
        } else {
            $data = array(
		'id_menu_bantuan' => $this->input->post('id_menu_bantuan',TRUE),
		'judul_bantuan' => $this->input->post('judul_bantuan',TRUE),
		'deskrispi_bantuan_detail' => $this->input->post('deskrispi_bantuan_detail',TRUE),
		'capture_bantuan' => $this->input->post('capture_bantuan',TRUE),
		'status_user' => $this->input->post('status_user',TRUE),
		'order_id' => $this->input->post('order_id',TRUE),
		'status_bantuan_aktif' => $this->input->post('status_bantuan_aktif',TRUE),
		'status_bantuan_remove' => $this->input->post('status_bantuan_remove',TRUE),
		'created_at' => $this->input->post('created_at',TRUE),
		'update_at' => $this->input->post('update_at',TRUE),
	    );

            $this->Bantuan_model->update($this->input->post('id_bantuan', TRUE), $data);
            $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">Berhasil Merubah Data</div>');
            redirect(site_url('bantuan'));
        }
    }
    
    public function delete($id) 
    {
        $row = $this->Bantuan_model->get_by_id($id);

        if ($row) {
            $this->Bantuan_model->delete($id);
            $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">Berhasil Menghapus Data</div>');
            redirect(site_url('bantuan'));
        } else {
            $this->session->set_flashdata('message', '<div class="alert alert-warning" role="alert">Data Tidak Ditemukan</div>');
            redirect(site_url('bantuan'));
        }
    }

    public function _rules() 
    {
	$this->form_validation->set_rules('id_menu_bantuan', 'id menu bantuan', 'trim|required');
	$this->form_validation->set_rules('judul_bantuan', 'judul bantuan', 'trim|required');
	$this->form_validation->set_rules('deskrispi_bantuan_detail', 'deskrispi bantuan detail', 'trim|required');
	$this->form_validation->set_rules('capture_bantuan', 'capture bantuan', 'trim|required');
	$this->form_validation->set_rules('status_user', 'status user', 'trim|required');
	$this->form_validation->set_rules('order_id', 'order id', 'trim|required');
	$this->form_validation->set_rules('status_bantuan_aktif', 'status bantuan aktif', 'trim|required');
	$this->form_validation->set_rules('status_bantuan_remove', 'status bantuan remove', 'trim|required');
	$this->form_validation->set_rules('created_at', 'created at', 'trim|required');
	$this->form_validation->set_rules('update_at', 'update at', 'trim|required');

	$this->form_validation->set_rules('id_bantuan', 'id_bantuan', 'trim');
	$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

    public function excel()
    {
        $this->load->helper('exportexcel');
        $namaFile = "bantuan.xls";
        $judul = "bantuan";
        $tablehead = 0;
        $tablebody = 1;
        $nourut = 1;
        //penulisan header
        header("Pragma: public");
        header("Expires: 0");
        header("Cache-Control: must-revalidate, post-check=0,pre-check=0");
        header("Content-Type: application/force-download");
        header("Content-Type: application/octet-stream");
        header("Content-Type: application/download");
        header("Content-Disposition: attachment;filename=" . $namaFile . "");
        header("Content-Transfer-Encoding: binary ");

        xlsBOF();

        $kolomhead = 0;
        xlsWriteLabel($tablehead, $kolomhead++, "No");
	xlsWriteLabel($tablehead, $kolomhead++, "Id Menu Bantuan");
	xlsWriteLabel($tablehead, $kolomhead++, "Judul Bantuan");
	xlsWriteLabel($tablehead, $kolomhead++, "Deskrispi Bantuan Detail");
	xlsWriteLabel($tablehead, $kolomhead++, "Capture Bantuan");
	xlsWriteLabel($tablehead, $kolomhead++, "Status User");
	xlsWriteLabel($tablehead, $kolomhead++, "Order Id");
	xlsWriteLabel($tablehead, $kolomhead++, "Status Bantuan Aktif");
	xlsWriteLabel($tablehead, $kolomhead++, "Status Bantuan Remove");
	xlsWriteLabel($tablehead, $kolomhead++, "Created At");
	xlsWriteLabel($tablehead, $kolomhead++, "Update At");

	foreach ($this->Bantuan_model->get_all() as $data) {
            $kolombody = 0;

            //ubah xlsWriteLabel menjadi xlsWriteNumber untuk kolom numeric
            xlsWriteNumber($tablebody, $kolombody++, $nourut);
	    xlsWriteNumber($tablebody, $kolombody++, $data->id_menu_bantuan);
	    xlsWriteLabel($tablebody, $kolombody++, $data->judul_bantuan);
	    xlsWriteLabel($tablebody, $kolombody++, $data->deskrispi_bantuan_detail);
	    xlsWriteLabel($tablebody, $kolombody++, $data->capture_bantuan);
	    xlsWriteLabel($tablebody, $kolombody++, $data->status_user);
	    xlsWriteNumber($tablebody, $kolombody++, $data->order_id);
	    xlsWriteLabel($tablebody, $kolombody++, $data->status_bantuan_aktif);
	    xlsWriteLabel($tablebody, $kolombody++, $data->status_bantuan_remove);
	    xlsWriteLabel($tablebody, $kolombody++, $data->created_at);
	    xlsWriteLabel($tablebody, $kolombody++, $data->update_at);

	    $tablebody++;
            $nourut++;
        }

        xlsEOF();
        exit();
    }

}

/* End of file Bantuan.php */
/* Location: ./application/controllers/Bantuan.php */
/* Please DO NOT modify this information : */
/* Generated by Harviacode Codeigniter CRUD Generator 2020-03-17 08:02:29 */
/* http://harviacode.com */