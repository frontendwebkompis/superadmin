<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Bantuan_detail extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        if (cek_token()) {
            $this->load->model('Bantuan_detail_model');
            $this->load->model('Bantuan/Bantuan_model');
            $this->load->library('form_validation');
            $this->load->library('upload');
            $this->load->library('image_lib');
        } else {
            logout();
        }
    }

    public function tampil($id = '')
    {
        $row = $this->Bantuan_model->get_by_id($id);
        if ($row) {
            $q = urldecode($this->input->get('q', TRUE));
            $start = intval($this->input->get('start'));
            //   $id=$this->input->get('id');
            if ($q <> '') {
                $config['base_url'] = base_url() . 'bantuan_detail/tampil?q=' . urlencode($q);
                $config['first_url'] = base_url() . 'bantuan_detail/tampil?q=' . urlencode($q);
            } else {
                $config['base_url'] = base_url() . 'bantuan_detail/tampil';
                $config['first_url'] = base_url() . 'bantuan_detail/tampil';
            }

            $config['per_page'] = 10;
            $config['page_query_string'] = TRUE;
            $config['total_rows'] = $this->Bantuan_detail_model->total_rows($q, $id);
            $bantuan = $this->Bantuan_detail_model->get_limit_data($config['per_page'], $start, $q, $id);

            $this->load->library('pagination');
            $this->pagination->initialize($config);

            $data = array(
                'tittle'            => 'Bantuan',
                'judul'             => 'Bantuan',
                'title_card'        => 'List Bantuan',
                'bantuan_data'      => $bantuan,
                'q'                 => $q,
                'id'                => $id,
                'pagination'        => $this->pagination->create_links(),
                'total_rows'        => $config['total_rows'],
                'start'             => $start,
                'aktif'             => '1',
                'bantuan_aktif'     => '1',
                'menu_aktif'        => 'bantuan'
            );
            $res['datakonten'] = $this->load->view('bantuan_detail/bantuan_list', $data, true);
            $this->load->view('layouts/main_view', $res);
        } else {
            $this->session->set_flashdata('gagal', 'Data Tidak Ditemukan');
            redirect(site_url('bantuan'));
        }
    }
    public function nasabah($id = '')
    {
        $row = $this->Bantuan_model->get_by_id($id);
        if ($row) {
            $q = urldecode($this->input->get('q', TRUE));
            $start = intval($this->input->get('start'));
            //   $id=$this->input->get('id');
            if ($q <> '') {
                $config['base_url'] = base_url() . 'bantuan_detail/tampil?q=' . urlencode($q);
                $config['first_url'] = base_url() . 'bantuan_detail/tampil?q=' . urlencode($q);
            } else {
                $config['base_url'] = base_url() . 'bantuan_detail/tampil';
                $config['first_url'] = base_url() . 'bantuan_detail/tampil';
            }

            $config['per_page'] = 10;
            $config['page_query_string'] = TRUE;
            $config['total_rows'] = $this->Bantuan_detail_model->total_rows_nasabah($q, $id);
            $bantuan = $this->Bantuan_detail_model->get_limit_data_nasabah($config['per_page'], $start, $q, $id);

            $this->load->library('pagination');
            $this->pagination->initialize($config);

            $data = array(
                'tittle'    => 'Bantuan Nasabah',
                'judul'     => 'List Bantuan Nasabah',
                'bantuan_data' => $bantuan,
                'q' => $q,
                'id' => $id,
                'pagination' => $this->pagination->create_links(),
                'total_rows' => $config['total_rows'],
                'start' => $start,
                'aktif' => '2',
                'menu_aktif'    => 'bantuan'
            );
            $res['datakonten'] = $this->load->view('bantuan_detail/bantuan_list', $data, true);
            $this->load->view('layouts/main_view', $res);
        } else {
            $this->session->set_flashdata('gagal', 'Data Tidak Ditemukan');
            redirect(site_url('bantuan'));
        }
    }
    public function pengepul($id = '')
    {
        $row = $this->Bantuan_model->get_by_id($id);
        if ($row) {
            $q = urldecode($this->input->get('q', TRUE));
            $start = intval($this->input->get('start'));
            //   $id=$this->input->get('id');
            if ($q <> '') {
                $config['base_url'] = base_url() . 'bantuan_detail/pengepul?q=' . urlencode($q);
                $config['first_url'] = base_url() . 'bantuan_detail/pengepul?q=' . urlencode($q);
            } else {
                $config['base_url'] = base_url() . 'bantuan_detail/pengepul';
                $config['first_url'] = base_url() . 'bantuan_detail/pengepul';
            }

            $config['per_page'] = 10;
            $config['page_query_string'] = TRUE;
            $config['total_rows'] = $this->Bantuan_detail_model->total_rows_pengepul($q, $id);
            $bantuan = $this->Bantuan_detail_model->get_limit_data_pengepul($config['per_page'], $start, $q, $id);

            $this->load->library('pagination');
            $this->pagination->initialize($config);

            $data = array(
                'tittle'    => 'Bantuan Pengepul',
                'judul'     => 'List Bantuan Pengepul',
                'bantuan_data' => $bantuan,
                'q' => $q,
                'id' => $id,
                'pagination' => $this->pagination->create_links(),
                'total_rows' => $config['total_rows'],
                'start' => $start,
                'aktif' => '3',
                'menu_aktif'    => 'bantuan'
            );
            $res['datakonten'] = $this->load->view('bantuan_detail/bantuan_list', $data, true);
            $this->load->view('layouts/main_view', $res);
        } else {
            $this->session->set_flashdata('gagal', 'Data Tidak Ditemukan');
            redirect(site_url('bantuan'));
        }
    }
    public function detail($id)
    {
        $row = $this->Bantuan_detail_model->get_by_id($id);
        if ($row) {

            $q = urldecode($this->input->get('q', TRUE));
            $id_menu = $this->input->get('id_menu', TRUE);
            $start = intval($this->input->get('start'));
            //   $id=$this->input->get('id');
            if ($q <> '') {
                $config['base_url'] = base_url() . 'bantuan_detail/detail/' . $id . '?q=' . urlencode($q);
                $config['first_url'] = base_url() . 'bantuan_detail/detail/' . $id . '?q=' . urlencode($q);
            } else {
                $config['base_url'] = base_url() . 'bantuan_detail/detail/' . $id;
                $config['first_url'] = base_url() . 'bantuan_detail/detail/' . $id;
            }

            $config['per_page'] = 10;
            $config['page_query_string'] = TRUE;
            $config['total_rows'] = $this->Bantuan_detail_model->total_rows_detail($q, $id);
            $bantuan_detail = $this->Bantuan_detail_model->get_limit_data_detail($config['per_page'], $start, $q, $id);

            $this->load->library('pagination');
            $this->pagination->initialize($config);

            $data = array(
                'tittle'    => 'Bantuan',
                'judul'     => 'List Bantuan',
                'bantuan_detail' => $bantuan_detail,
                'q' => $q,
                'id' => $id,
                'id_menu' => $id_menu,
                'pagination' => $this->pagination->create_links(),
                'total_rows' => $config['total_rows'],
                'start' => $start,
                'id_bantuan' => $row->id_bantuan,
                'nama_menu_bantuan' => $row->nama_menu_bantuan,
                'judul_bantuan' => $row->judul_bantuan,
                'deskripsi_bantuan_detail' => $row->deskripsi_bantuan_detail,
                'capture_bantuan' => $row->capture_bantuan,
                'status_user' => $row->status_user,
                'menu_aktif'    => 'bantuan'
            );

            $res['datakonten'] = $this->load->view('bantuan_detail/bantuan_detail', $data, true);
            $this->load->view('layouts/main_view', $res);
        } else {
            $this->session->set_flashdata('gagal', 'Data Tidak Di Temukan');
            redirect(site_url('bantuan'));
        }
    }

    public function create($id = '')
    {
        $aktif = $this->input->get('aktif');
        $order = $this->Bantuan_detail_model->get_by_order($id);
        $data = array(
            'tittle'    => 'Bantuan',
            'judul'     => 'Bantuan',
            'title_card'     => 'List Bantuan',
            'button' => 'Create',
            'action' => site_url('bantuan_detail/create_action/' . $id . '?aktif=' . $aktif),
            'id_bantuan' => set_value('id_bantuan'),
            'id_menu_bantuan' => set_value('id_menu_bantuan'),
            'judul_bantuan' => set_value('judul_bantuan'),
            'deskripsi_bantuan_detail' => set_value('deskripsi_bantuan_detail'),
            'capture_bantuan' => set_value('capture_bantuan'),
            'id' => $id,
            'order_id' => set_value('order_id'),
            'status_bantuan_aktif' => set_value('status_bantuan_aktif'),
            'status_bantuan_remove' => set_value('status_bantuan_remove'),
            'created_at' => set_value('created_at'),
            'update_at' => set_value('update_at'),
            'order' => $order,
            'aktif' => $aktif,
            'bantuan_aktif' => '1',
            'menu_aktif'    => 'bantuan'
        );

        $res['datakonten'] = $this->load->view('bantuan_detail/bantuan_form', $data, true);
        $this->load->view('layouts/main_view', $res);
    }
    public function create_detail($id = '')
    {
        $id_menu = $this->input->get('id_menu');

        $data = array(
            'tittle'    => 'Bantuan',
            'judul'     => 'List Bantuan',
            'button' => 'Create',
            'action' => site_url('bantuan_detail/create_detail_action/' . $id . '?id_menu=' . $id_menu),
            'id_bantuan' => set_value('id_bantuan'),
            'id_menu_bantuan' => set_value('id_menu_bantuan'),
            'judul_bantuan' => set_value('judul_bantuan'),
            'deskripsi_bantuan_detail' => set_value('deskripsi_bantuan_detail'),
            'capture_bantuan' => set_value('capture_bantuan'),

        );

        $res['datakonten'] = $this->load->view('bantuan_detail/bantuan_detail_form', $data, true);
        $this->load->view('layouts/main_view', $res);
    }

    public function create_action($id = '')
    {
        $dt = new DateTime();
        $aktif = $this->input->get('aktif');
        $order = $this->Bantuan_detail_model->get_by_order($id);
        $cek = 0;
        if ($order) {
            $cek = $order->order_id + 1;
        } else {
            $cek = 1;
        }
        // echo $cek;
        $this->_rules();
        $prefix = "BNT";
        // echo $gabung;
        $row = $this->Bantuan_detail_model->get_by_prefix($prefix);
        $NoUrut = (int) substr($row->max, 3, 3);
        $NoUrut = $NoUrut + 1; //nomor urut +1
        $NoUrut = sprintf('%03d', $NoUrut);
        $fix = $prefix . $NoUrut;
        if (!is_dir("./assets/gambar/bantuan/" . $this->session->userdata('uid') . "/")) {
            mkdir("./assets/gambar/bantuan/" . $this->session->userdata('uid') . "/");
        }
        $config['upload_path'] = './assets/gambar/bantuan/'; //path folder
        $config['allowed_types'] = 'gif|jpg|png|jpeg|bmp'; //type yang dapat diakses bisa anda sesuaikan
        $config['encrypt_name'] = TRUE; //Enkripsi nama yang terupload

        $this->upload->initialize($config);
        if (!empty($_FILES['capture_bantuanbaru']['name'])) {

            if ($this->upload->do_upload('capture_bantuanbaru', TRUE)) {
                $gbr = $this->upload->data();
                //Compress Image
                $config['image_library'] = 'gd2';
                $config['source_image'] = './assets/gambar/bantuan/' . $gbr['file_name'];
                $config['create_thumb'] = FALSE;
                $config['maintain_ratio'] = FALSE;
                $config['quality'] = '50%';
                $config['width'] = 600;
                $config['height'] = 400;
                $config['new_image'] = './assets/images/bantuan/' . $gbr['file_name'];
                $this->load->library('image_lib', $config);
                $this->image_lib->resize();
                // $gambar = $gbr['file_name'];
                $upload = s3_upload('bantuan/' . $gbr['file_name'], $gbr['full_path']);
                $gambar = $upload['ObjectURL'];
                @unlink('assets/gambar/bantuan/' . $gbr['file_name']);
            }
        } else {
            echo "Image yang diupload kosong";
        }
        if ($this->form_validation->run() == FALSE) {
            $this->create();
        } else {
            $data = array(
                'id_bantuan' => $fix,
                'id_menu_bantuan' => $id,
                'judul_bantuan' => $this->input->post('judul_bantuan', TRUE),
                'deskripsi_bantuan_detail' => $this->input->post('deskripsi_bantuan_detail', TRUE),
                'capture_bantuan' => $gambar,
                // 'status_user' => $this->input->post('status_user', TRUE),
                'order_id' => $cek,
                'status_bantuan_remove' =>  1,
                'created_at' =>  $dt->format('Y-m-d H:i:s'),
                'update_at' =>  $dt->format('Y-m-d H:i:s'),
                // 'update_at' => $this->input->post('update_at', TRUE),
            );

            $this->Bantuan_detail_model->insert($data);
            $this->session->set_flashdata('berhasil', 'Berhasil Menambahkan Data');
            redirect(site_url('bantuan_detail/tampil/' . $id . '?aktif=' . $aktif));
        }
    }
    public function create_detail_action($id = '')
    {
        $id_menu = $this->input->get('id_menu', TRUE);
        // $this->_rules();
        if (!is_dir("./assets/gambar/bantuan_detail/" . $this->session->userdata('uid') . "/")) {
            mkdir("./assets/gambar/bantuan_detail/" . $this->session->userdata('uid') . "/");
        }
        $config['upload_path'] = './assets/gambar/bantuan_detail/'; //path folder
        $config['allowed_types'] = 'gif|jpg|png|jpeg|bmp'; //type yang dapat diakses bisa anda sesuaikan
        $config['encrypt_name'] = TRUE; //Enkripsi nama yang terupload

        $this->upload->initialize($config);
        if (!empty($_FILES['capture_bantuan']['name'])) {

            if ($this->upload->do_upload('capture_bantuan', TRUE)) {
                $gbr = $this->upload->data();
                //Compress Image
                $config['image_library'] = 'gd2';
                $config['source_image'] = './assets/gambar/bantuan_detail/' . $gbr['file_name'];
                $config['create_thumb'] = FALSE;
                $config['maintain_ratio'] = FALSE;
                $config['quality'] = '50%';
                $config['width'] = 600;
                $config['height'] = 400;
                $config['new_image'] = './assets/images/bantuan_detail/' . $gbr['file_name'];
                $this->load->library('image_lib', $config);
                $this->image_lib->resize();
                // $gambar = $gbr['file_name'];
                $upload = s3_upload('bantuan/' . $gbr['file_name'], $gbr['full_path']);
                $gambar = $upload['ObjectURL'];
            }
        } else {
            echo "Image yang diupload kosong";
        }

        // if ($this->form_validation->run() == FALSE) {
        //     $this->create();
        // } else {
        $data = array(
            'id_bantuan' => $id,
            'judul_bantuan_detail' => $this->input->post('judul_bantuan_detail', TRUE),
            'deskripsi_bantuan_detail' => $this->input->post('deskripsi_bantuan_detail', TRUE),
            'capture_bantuan' => $gambar,
        );
        $this->Bantuan_detail_model->insertdetail($data);
        $this->session->set_flashdata('berhasil', 'Berhasil Menambahkan Data');
        redirect(site_url('bantuan_detail/tampil/' . $id_menu));
        // }
    }

    public function update($id)
    {
        $row = $this->Bantuan_detail_model->get_by_id($id);
        $aktif = $this->input->get('aktif');

        if ($row) {
            $data = array(
                'aktif'                     => $aktif,
                'id'                        => $row->id_menu_bantuan,
                'tittle'                    => 'Bantuan',
                'judul'                     => 'Bantuan',
                'title_card'                => 'Update Bantuan',
                'button'                    => 'Update',
                'action'                    => site_url('bantuan_detail/update_action/' . $id . '?aktif=' . $aktif),
                'id_bantuan'                => set_value('id_bantuan', $row->id_bantuan),
                'judul_bantuan'             => set_value('judul_bantuan', $row->judul_bantuan),
                'deskripsi_bantuan_detail'  => set_value('deskrispi_bantuan_detail', $row->deskripsi_bantuan_detail),
                'capture_bantuan'           => set_value('capture_bantuan', $row->capture_bantuan),
                'order_id'                  => set_value('order_id', $row->order_id),
                'status_bantuan_remove'     => set_value('status_bantuan_remove', $row->status_bantuan_remove),
                'created_at'                => set_value('created_at', $row->created_at),
                'update_at'                 => set_value('update_at', $row->update_at),
                'bantuan_aktif'             => '1',
                'menu_aktif'    => 'bantuan'
                // 'status_user' => set_value('status_user', $row->status_user),
                // 'status_bantuan_aktif' => set_value('status_bantuan_aktif', $row->status_bantuan_aktif),
            );

            $res['datakonten'] = $this->load->view('bantuan_detail/bantuan_form', $data, true);
            $this->load->view('layouts/main_view', $res);
        } else {
            $this->session->set_flashdata('gagal', 'Data Tidak Ditemukan');
            redirect(site_url('bantuan_detail/tampil/' . $row->id_menu_bantuan . 'aktif=' . $aktif));
        }
    }

    public function update_action($id = '')
    {
        // $id = $this->input->get('id');
        $this->_rules();
        $aktif = $this->input->get('aktif');
        $row = $this->Bantuan_detail_model->get_by_id($id);

        $config['upload_path'] = './assets/gambar/bantuan/'; //path folder
        $config['allowed_types'] = 'gif|jpg|png|jpeg|bmp'; //type yang dapat diakses bisa anda sesuaikan
        $config['encrypt_name'] = TRUE; //Enkripsi nama yang terupload
        $dt = new DateTime();

        $this->upload->initialize($config);
        if (!empty($_FILES['capture_bantuanbaru']['name'])) {

            if ($this->upload->do_upload('capture_bantuanbaru', TRUE)) {
                $gbr = $this->upload->data();
                //Compress Image
                $config['image_library'] = 'gd2';
                $config['source_image'] = './assets/gambar/bantuan/' . $gbr['file_name'];
                $config['create_thumb'] = FALSE;
                $config['maintain_ratio'] = FALSE;
                $config['quality'] = '50%';
                $config['width'] = 600;
                $config['height'] = 400;
                $config['new_image'] = './assets/images/bantuan/' . $gbr['file_name'];
                $this->load->library('image_lib', $config);
                $this->image_lib->resize();
                @unlink('assets/gambar/bantuan/' . $this->input->post('capture_bantuan'));
                // $gambar = $gbr['file_name'];
                del('bantuan/' . basename($row->capture_bantuan));
                $upload = s3_upload('bantuan/' . $gbr['file_name'], $gbr['full_path']);
                $gambar = $upload['ObjectURL'];
            }
        } else {
            $gambar = $this->input->post('capture_bantuan');
        }
        if ($row) {
            if ($this->form_validation->run() == FALSE) {
                $this->update($this->input->post('id_bantuan', TRUE));
            } else {
                $data = array(
                    'judul_bantuan' => $this->input->post('judul_bantuan', TRUE),
                    'deskripsi_bantuan_detail' => $this->input->post('deskripsi_bantuan_detail', TRUE),
                    'capture_bantuan' => $gambar,
                    // 'status_user' => $this->input->post('status_user', TRUE),
                    'update_at' =>  $dt->format('Y-m-d H:i:s'),
                );
                $this->Bantuan_detail_model->update($this->input->post('id_bantuan', TRUE), $data);
                $this->session->set_flashdata('berhasil', 'Berhasil Merubah Data');
                redirect(site_url('bantuan_detail/tampil/' . $row->id_menu_bantuan . '?aktif=' . $aktif));
            }
        } else {
            $this->session->set_flashdata('gagal', 'Data Tidak Ditemukan');
            redirect(site_url('bantuan_detail/tampil/' . $row->id_menu_bantuan . '?aktif=' . $aktif));
        }
    }

    public function delete($id)
    {
        $iddata = $this->input->get('aktif');
        $row = $this->Bantuan_detail_model->get_by_id($id);
        $cekdata = $this->Bantuan_detail_model->get_oder_all($row->id_menu_bantuan);
        foreach ($cekdata as $key) {
            # code...
            if ($key->order_id > $row->order_id) {
                $ss = $key->order_id;
                $kurang = $ss - 1;
                $datas = array(
                    'order_id' => $kurang,
                );
                // echo $key->id_bantuan.'#'.$kurang.'#'.$ss.'<br>';
                $this->Bantuan_detail_model->update($key->id_bantuan, $datas);
            }
        }
        if ($row) {
            $data = array(
                'order_id' => 0,
                'status_bantuan_remove' => 0,
            );
            $this->Bantuan_detail_model->update($row->id_bantuan, $data);
            // $this->Bantuan_detail_model->delete($id);
            del('bantuan/' . basename($row->capture_bantuan));
            $this->session->set_flashdata('berhasil', 'Berhasil Menghapus Data');
            redirect(site_url('bantuan_detail/tampil/' . $row->id_menu_bantuan . '?aktif=' . $iddata));
        } else {
            $this->session->set_flashdata('gagal', 'Data Tidak Ditemukan');
            redirect(site_url('bantuan_detail/tampil/' . $row->id_menu_bantuan . '?aktif=' . $iddata));
        }
    }

    public function _rules()
    {
        // $this->form_validation->set_rules('id_menu_bantuan', 'id menu bantuan', 'trim|required');
        $this->form_validation->set_rules('judul_bantuan', 'judul bantuan', 'trim|required');
        $this->form_validation->set_rules('deskripsi_bantuan_detail', 'Deskripsi bantuan detail', 'trim|required');
        // $this->form_validation->set_rules('capture_bantuan', 'capture bantuan', 'trim|required');
        // $this->form_validation->set_rules('status_user', 'status user', 'trim|required');
        // $this->form_validation->set_rules('order_id', 'order id', 'trim|required');
        // // $this->form_validation->set_rules('status_bantuan_aktif', 'status bantuan aktif', 'trim|required');

        $this->form_validation->set_rules('id_bantuan', 'id_bantuan', 'trim');
        $this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }



    function get_image()
    {

        $img = $this->input->get('img');
        // echo getkonten($img);
        $cek = 'https://kompis-bank-sampah.s3.ap-southeast-1.amazonaws.com/' . $img;
        $row = $this->Bantuan_detail_model->get_by_poto($cek);
        if ($row) {
            echo getkonten($img);
        } else {
            $this->session->set_flashdata('cek', 'Gambar Tidak Ditemukan');
            redirect('profile');
        }
    }

    function cekup($id = null)
    {

        $row = $this->Bantuan_detail_model->get_by_id($id);
        if ($row) {
            $simpan = $row->order_id;
            $tambah = $simpan - 1;
            $data = array(
                'order_id' => $simpan,
            );
            $this->Bantuan_detail_model->updateorder($tambah, $data);

            $data = array(
                'order_id' => $tambah,
            );
            $this->Bantuan_detail_model->update($row->id_bantuan, $data);

            redirect('bantuan_detail/tampil/' . $row->id_menu_bantuan);
        } else {
            $this->session->set_flashdata('gagal', 'Data Tidak Di Temukan');
            redirect('bantuan_detail/tampil/' . $row->id_menu_bantuan);
        }
    }
    function cekdown($id = null)
    {

        $row = $this->Bantuan_detail_model->get_by_id($id);
        if ($row) {
            $simpan = $row->order_id;
            $tambah = $simpan + 1;
            $data = array(
                'order_id' => $simpan,
            );
            $this->Bantuan_detail_model->updateorder($tambah, $data);

            $data = array(
                'order_id' => $tambah,
            );
            $this->Bantuan_detail_model->update($row->id_bantuan, $data);
            redirect('bantuan_detail/tampil/' . $row->id_menu_bantuan);
        } else {
            $this->session->set_flashdata('gagal', 'Data Tidak Ditemukan');
            redirect('bantuan_detail/tampil/' . $row->id_menu_bantuan);
        }
    }
}

/* End of file Bantuan.php */
/* Location: ./application/controllers/Bantuan.php */
/* Please DO NOT modify this information : */
/* Generated by Harviacode Codeigniter CRUD Generator 2020-03-17 08:02:29 */
/* http://harviacode.com */
