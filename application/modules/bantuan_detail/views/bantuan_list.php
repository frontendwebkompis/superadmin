<script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>

<?php
$aktif=$this->input->get('aktif');
 ?>

<div class="row">
	<section class="col-lg connectedSortable ui-sortable">
		<div class="card card-info">
            <div class="card-header">
                <div class="card-title">
                  <?php echo $title_card?>
                </div>
              </div>
			<div class="card-body">
				<div class="tab-content p-0">

        <div class="row">
            <div class="col-md-5 mb-2">
                <?php echo anchor(site_url('bantuan_detail/create/'.$id.'?aktif='.$aktif),'Tambah Bantuan', 'class="btn btn-info btn-sm"'); ?>
                <?php if($aktif==3){ ?>
                    <a class="btn btn-secondary btn-sm" href="<?php echo base_url('bantuan/pengepul');?>">Kembali</a>
                <?php }elseif ($aktif==2) { ?>
                    <a class="btn btn-secondary btn-sm" href="<?php echo base_url('bantuan/nasabah');?>">Kembali</a>
                <?php  }else{ ?>
                    <a class="btn btn-secondary btn-sm" href="<?php echo base_url('bantuan');?>">Kembali</a>
                <?php } ?>
            </div>
            <div class="col-md-3 offset-md-4 mb-2">
                <?= search(site_url('bantuan_detail'), site_url('bantuan_detail'), $q) ?>
            </div>
        </div>
           <div class="tab-content p-0" style="overflow:auto">
            <table class="table table-bordered table-striped table-condensed table-hover table-sm mb-2">
        <tr>
            <th style="text-align:center" width="20px">No</th>
            <th class="text-center">Menu Bantuan</th>
            <th class="text-center">Judul Bantuan</th>
            <th class="text-center">Deskripsi Bantuan</th>
            <th class="text-center">Capture Bantuan</th>
            <th class="text-center">Order</th>
             <th class="text-center">Dibuat Tanggal</th>
            <th class="text-center">Update Tanggal</th>
            <th class="text-center">Aksi</th>
        </tr><?php
            foreach ($bantuan_data as $bantuan)
            {
                ?>
        <tr>
			<td class="text-center"><?php echo ++$start ?></td>
			<td class="text-center"><?php echo $bantuan->nama_menu_bantuan ?></td>
			<td class="text-center"><?php echo $bantuan->judul_bantuan ?></td>
			<td><?php echo $bantuan->deskripsi_bantuan_detail ?></td>
            <td class="text-center">
                <img width="50px" height="50px" src="<?= base_url()?>bantuan_detail/get_image?img=bantuan/<?php echo basename($bantuan->capture_bantuan) ?>" 
                alt="Capture Bantuan">
            </td>
			<td class="text-center">
                 <?php 
                     if(cekposisibantuan($id)->min!=$bantuan->order_id){
                        ?>
                <a href="<?php echo base_url(); ?>bantuan_detail/cekup/<?php echo($bantuan->id_bantuan) ?>"><i class="fas fa-arrow-circle-up"></i></a>
                <?php }?>
                <?php
                     if(cekposisibantuan($id)->max!=$bantuan->order_id){
                        ?>
                <a href="<?php echo base_url(); ?>bantuan_detail/cekdown/<?php echo($bantuan->id_bantuan) ?>"><i class="fas fa-arrow-circle-down"></i></a>
                <?php } ?>
            </td>
                		
			<td class="text-center"><?php echo fulldate($bantuan->created_at) ?> </td>
			<td class="text-center"><?php echo fulldate($bantuan->update_at) ?></td>
			<td style="text-align:center" width="100px">
                <div class="btn-group">
                    <a href="<?php echo site_url('bantuan_detail/update/'.$bantuan->id_bantuan.'?aktif='.$aktif); ?>"
                    data-toogle="tooltip" title="Update">
                    <button type="button" class="btn btn-success btn-sm"><i class="far fa-edit"></i></button></a>
                    <a href="#" class="btn btn-danger btn-sm" data-toggle="tooltip" data-placement="top" title="Hapus" onclick="confirm('<?=  $bantuan->id_bantuan ?>')" ><i class="fas fa-trash-alt"></i></a>
                </div>
			</td>
		</tr>
            <?php
            }
            ?>
        </table>
        </div>
        
            <?= footer($total_rows, $pagination, '') ?>
          
        
                    </div>
                </div>
            </div>
        </section>
    </div>   
</div>


<script>
function confirm(res) {
    Swal.fire({
    title: 'Anda Yakin Mau Menghapus?',
    text: "Tidak bisa dikembalikan jika sudah dihapus!",
    icon: 'warning',
    showCancelButton: true,
    confirmButtonColor: '#3085d6',
    cancelButtonColor: '#d33',
    confirmButtonText: 'Ya, Hapus!',
    cancelButtonText: 'Batal'
    }).then((result) => {
    if (result.value) {
        Swal.fire(
        'Terhapus!',
        'File sudah terhapus.',
        'success'
        )
        window.location='<?php echo base_url().'bantuan_detail/delete/'; ?>'+res+'?aktif=<?php $this->input->get('aktif'); ?>';
    }
    });
}
</script>