<div class="row">
	<!-- Left col -->
	<section class="col-lg connectedSortable ui-sortable">
		<!-- Custom tabs (Charts with tabs)-->
		<div class="card card-info">
            <div class="card-header">
                <div class="card-title">
                  <?php echo $title_card?>
                </div>
              </div>
			<div class="card-body">
				<div class="tab-content p-0">
                    <div class="row mx-3">
                        <div class="col-12">
                            <form id="dataform" action="<?php echo $action; ?>" method="post" enctype="multipart/form-data">

                                <div class="form-group">
                                    <label for="varchar">Judul Bantuan <?php echo form_error('judul_bantuan') ?></label>
                                    <input type="text" class="form-control" name="judul_bantuan" id="judul_bantuan" placeholder="Judul Bantuan" value="<?php echo $judul_bantuan; ?>" />
                                </div>
                                <div class="form-group">
                                    <label for="varchar">Deskripsi Bantuan  <?php echo form_error('deskripsi_bantuan_detail') ?></label>
                                    <div class="mb-3">
                                        <textarea class="textarea"  name="deskripsi_bantuan_detail" id="deskripsi_bantuan_detail" placeholder="Deskripsi Bantuan "  aria-multiline="true"  rows="5"   style="width: 100%; height: 400px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;"><?php echo $deskripsi_bantuan_detail; ?> </textarea>
                                    </div>
                                </div>
                                
                                <div class="form-group">
                                    <label for="varchar">Capture Bantuan <?php echo form_error('capture_bantuan') ?></label><br>
                                    <?php if ($capture_bantuan != "") { ?>
                                        <div class="text-center">
                                            <img style="max-width:300px;max-height:300px;" src="<?= base_url()?>bantuan_detail/get_image?img=bantuan/<?php echo basename($capture_bantuan) ?>" alt=""><br>
                                            <button type="button" class="btn btn-secondary btn-sm" onclick="cek()">Ganti Gambar</button>
                                        </div>
                                        <input type="file" style="display:none" accept="image/*" class="form-control mt-2" name="capture_bantuanbaru" id="capture_bantuanbaru">
                                        <input type="hidden" id="capture_bantuan" accept="image/*" value="<?php echo $capture_bantuan ?>" name="capture_bantuan">
                                    <?php } else { ?>
                                        <input required type="file" accept="image/*" class="form-control" name="capture_bantuanbaru" id="capture_bantuanbaru">
                                    <?php } ?>
                                </div>

                                <input type="hidden" name="id_bantuan" value="<?php echo $id_bantuan; ?>" />
                                    <div id="loader" class="alert alert-info" style="display:none">
                                        <strong>Info!</strong> Sedang Proses....
                                    </div>
                            
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12 text-center">
                            <button id="ceksub" type="submit" class="btn btn-info" style="width: 100px;">Simpan</button>
                            <a href="<?php echo site_url('bantuan_detail/tampil/'.$id.'?aktif='.$aktif) ?>" class="btn btn-secondary" style="width: 100px;">Batal</a>
                            </form>
                        </div>
                    </div>

                </div>
            </div><!-- /.card-body -->
		</div>
		<!-- /.card -->
	</section>
	<!-- /.Left col -->
</div>

<script>
    $('#ceksub').click(function () {
          $('#dataform').submit(); 
        $('#loader').show(300); 
          $('#ceksub').prop('disabled', true);
    }
        );
    function cek() {
        var x = document.getElementById("capture_bantuanbaru");
        if (x.style.display === "none") {
            x.style.display = "block";
        } else {
            x.style.display = "none";
        }
    }
     $(function () {
    // Summernote
    $('.textarea').summernote()
  })
</script>