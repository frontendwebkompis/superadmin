<div class="row">
	<!-- Left col -->
	<section class="col-lg connectedSortable ui-sortable">
		<!-- Custom tabs (Charts with tabs)-->
		<div class="card">
			<div class="card-body">
				<div class="tab-content p-0">

					<table class="table table-bordered table-striped table-condensed" style="margin-bottom: 20px">
						<tr>
							<td>Nama Bantuan</td><td><?php echo $nama_menu_bantuan; ?></td>
						</tr>
						
						<!-- <tr>
							<td></td>
							<td><a href="<?php echo site_url('brand') ?>" class="btn btn-default">Batal</a></td>
						</tr> -->
					</table>
					<div>
						<a href="<?php echo site_url('brand') ?>" class="btn btn-primary">Batal</a>
					</div>

				</div>
			</div><!-- /.card-body -->
		</div>
		<!-- /.card -->
	</section>
	<!-- /.Left col -->
</div>
