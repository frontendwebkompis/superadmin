
<div class="row">
	<!-- Left col -->
	<section class="col-lg connectedSortable ui-sortable">
		<!-- Custom tabs (Charts with tabs)-->
		<div class="card">
			<div class="card-body">
				<div class="tab-content p-0">

        <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data">
	  
	    <div class="form-group">
            <label for="varchar">Judul Bantuan Detail <?php echo form_error('judul_bantuan_detail') ?></label>
            <input type="text" class="form-control" name="judul_bantuan_detail" id="judul_bantuan_detail" placeholder="Judul Bantuan" value="<?php echo $judul_bantuan; ?>" />
        </div>
	    <div class="form-group">
            <label for="varchar">Deskripsi Bantuan Detail <?php echo form_error('deskrispi_bantuan_detail') ?></label>
            <input type="text" class="form-control" name="deskrispi_bantuan_detail" id="deskrispi_bantuan_detail" placeholder="Deskrispi Bantuan Detail" value="<?php echo $deskrispi_bantuan_detail; ?>" />
        </div>

        <div class="form-group">
            <label for="varchar">Capture Bantuan <?php echo form_error('capture_bantuan') ?></label>
            <?php if ($capture_bantuan != "") { ?>
            <img style="height:100px"
                src="<?php echo $capture_bantuan ?>" alt="">
            <button type="button" onclick="cek()">Ganti Gambar</button>
            <input type="file" style="display:none" accept="image/*" class="form-control" name="capture_bantuanbaru" id="capture_bantuanbaru">
            <input type="hidden" id="capture_bantuan" accept="image/*" value="<?php echo $capture_bantuan ?>" name="capture_bantuan">
            <?php }else{ ?>
            <input required type="file" accept="image/*" class="form-control" name="capture_bantuan" id="capture_bantuan">
            <?php } ?>
        </div>

            <button type="submit" class="btn btn-primary btn-sm">Simpan</button> 
            <a href="<?php echo site_url('bantuan_detail') ?>" class="btn btn-default btn-sm">Batal</a>
	    </form>
        
                </div>
            </div><!-- /.card-body -->
		</div>
		<!-- /.card -->
	</section>
	<!-- /.Left col -->
    
</div>

<script>
    function cek() {
        var x = document.getElementById("capture_bantuanbaru");
        if (x.style.display === "none") {
            x.style.display = "block";
        } else {
            x.style.display = "none";
        }
    }
</script>