
<div class="row">
	<!-- Left col -->
	<section class="col-lg connectedSortable ui-sortable">
		<!-- Custom tabs (Charts with tabs)-->
		<div class="card">
			<div class="card-body">
				<div class="tab-content p-0">

                <div class=card-body>
                <table class="table table-bordered table-striped table-condensed" style="margin-bottom: 10px">
                    <tr>
                        <td>Nama Menu Bantuan</td>
                        <td><?php echo $nama_menu_bantuan; ?></td>
                    </tr>
                    <tr>
                        <td>Judul Bantuan</td>
                        <td><?php echo $judul_bantuan; ?></td>
                    </tr>
                    <tr>
                        <td>Deskrispi Bantuan Detail</td>
                        <td><?php echo $deskripsi_bantuan_detail; ?></td>
                    </tr>
                    <tr>
                        <td>Capture Bantuan</td>
                        <td><img width="50px" height="50px" src="<?= base_url() ?>assets/gambar/bantuan/<?php echo $capture_bantuan ?>" alt=""></td>
                    </tr>
                   
                    <!-- <tr>
                        <td></td>
                        <td><a href="<?php echo site_url('bantuan_detail/tampil/') ?>" class="btn btn-default">Batal</a></td>
                    </tr> -->
                </table>
                <a href="<?php echo site_url('bantuan_detail/tampil/') ?>" class="btn btn-default btn-sm">Batal</a></td>
                </div>

            <!-- <div class="card-body"> -->
                
                <div class="card-body">
                <div class="row" style="margin-bottom: 10px">
						<div class="col-md-4">
                        <?php echo anchor(site_url('bantuan_detail/create_detail/' . $id . '?id_menu=' . $id_menu), 'Tambah Bantuan Detail', 'class="btn btn-primary btn-sm"'); ?>
						</div>
						<div class="col-md-4 text-center">
						</div>
						<div class="col-md-1 text-right">
						</div>
						<div class="col-md-3 text-right">
						</div>
                </div>
                <table class="table table-bordered table-striped table-condensed" style="margin-right: 10px" >
                    <tr>
                        <th>No</th>
                        <th>Judul Bantuan</th>
                        <th>Deskrispi Bantuan</th>
                        <th>Capture Bantuan</th>
                    </tr>
                            <?php
                            foreach ($bantuan_detail as $bantuan) {
                            ?>
                    <tr>
                        <td width="80px"><?php echo ++$start ?></td>
                        <td><?php echo $bantuan->judul_bantuan_detail ?></td>
                        <td><?php echo $bantuan->deskrispi_bantuan_detail ?></td>
                        <td><img width="50px" height="50px" src="<?= base_url()?>assets/gambar/bantuan_detail/<?php echo $bantuan->capture_bantuan ?>" alt=""></td>
                        <!-- <td style="text-align:center" width="100px">
                            <div class="btn-group">
                                <a href="<?php echo site_url('bantuan_detail/detail/'.$bantuan->id_bantuan); ?>"
                                data-toogle="tooltip" title="Lihat">
                                <button type="button" class="btn btn-info"><i class="fas fa-info-circle"></i></button></a>
                                <a href="<?php echo site_url('bantuan_detail/update/'.$bantuan->id_bantuan); ?>"
                                data-toogle="tooltip" title="Update">
                                <button type="button" class="btn btn-success"><i class="far fa-edit"></i></button></a>
                                <a href="#" class="btn btn-danger" data-toggle="tooltip" data-placement="top" title="Hapus" onclick="confirm('<?=  $bantuan->id_bantuan ?>')" ><i class="fas fa-trash-alt"></i></a>
                            </div>
			            </td> -->
                    </tr>
                    <?php } ?>
                </table>
                </div>
            <!-- </div> -->

                    </div>
                </div>
            </div>
        </div>
    </section>
</div>