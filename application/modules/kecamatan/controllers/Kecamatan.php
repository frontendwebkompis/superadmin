<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Kecamatan extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
		if (cek_token()) {
			$this->load->model('Kecamatan_model');
			$this->load->library('form_validation');
			$this->load->library('upload');
		} else {
			logout();
		}
	}

	public function index()
	{
		$model = $this->Kecamatan_model;
		$q = urldecode($this->input->get('q', TRUE));
		$start = intval($this->input->get('start'));

		if ($q <> '') {
			$config['base_url'] = base_url() . 'kecamatan/?q=' . urlencode($q);
			$config['first_url'] = base_url() . 'kecamatan/?q=' . urlencode($q);
		} else {
			$config['base_url'] = base_url() . 'kecamatan/';
			$config['first_url'] = base_url() . 'kecamatan/';
		}

		$config['per_page'] = 15;
		$config['page_query_string'] = TRUE;
		$config['total_rows'] = $model->total_rows($q);
		$cek = $model->get_limit_data($config['per_page'], $start, $q);

		$this->load->library('pagination');
		$this->pagination->initialize($config);

		$data = array(
			'tittle'        => 'kecamatan',
			'judul'         => 'kecamatan',
			'title_card'    => 'Menu kecamatan',
			'kecamatan' => $cek,
			'q'             => $q,
			'pagination'    => $this->pagination->create_links(),
			'total_rows'    => $config['total_rows'],
			'start'         => $start,
			'menu_aktif'	=> 'kecamatan',
		);
		$res['datakonten'] = $this->load->view('kecamatan/kecamatan_list', $data, true);
		$this->load->view('layouts/main_view', $res);
	}
}

/* End of file kecamatan.php */
