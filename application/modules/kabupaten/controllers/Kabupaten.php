<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Kabupaten extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
		if (cek_token()) {
			$this->load->model('Kabupaten_model');
			$this->load->library('form_validation');
			$this->load->library('upload');
		} else {
			logout();
		}
	}

	public function index()
	{
		$model = $this->Kabupaten_model;
		$q = urldecode($this->input->get('q', TRUE));
		$start = intval($this->input->get('start'));

		if ($q <> '') {
			$config['base_url'] = base_url() . 'kabupaten/?q=' . urlencode($q);
			$config['first_url'] = base_url() . 'kabupaten/?q=' . urlencode($q);
		} else {
			$config['base_url'] = base_url() . 'kabupaten/';
			$config['first_url'] = base_url() . 'kabupaten/';
		}

		$config['per_page'] = 15;
		$config['page_query_string'] = TRUE;
		$config['total_rows'] = $model->total_rows($q);
		$cek = $model->get_limit_data($config['per_page'], $start, $q);

		$this->load->library('pagination');
		$this->pagination->initialize($config);

		$data = array(
			'tittle'        => 'kabupaten',
			'judul'         => 'kabupaten',
			'title_card'    => 'Menu kabupaten',
			'kabupaten' => $cek,
			'q'             => $q,
			'pagination'    => $this->pagination->create_links(),
			'total_rows'    => $config['total_rows'],
			'start'         => $start,
			'menu_aktif'	=> 'kabupaten',
		);
		$res['datakonten'] = $this->load->view('kabupaten/kabupaten_list', $data, true);
		$this->load->view('layouts/main_view', $res);
	}
}

/* End of file kabupaten.php */
