<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Kabupaten_model extends CI_Model
{
	public $table = 'kabupaten';
	function total_rows($q = NULL)
	{
		$this->db->group_start();
		$this->db->or_like("nama_kabupaten", $q);
		$this->db->group_end();

		$this->db->from($this->table);
		return $this->db->count_all_results();
	}

	// get data with limit and search
	function get_limit_data($limit, $start = 0, $q = NULL)
	{
		$this->db->order_by('id_kabupaten', 'asc');
		$this->db->group_start();
		$this->db->or_like("nama_kabupaten", $q);
		$this->db->group_end();
		$this->db->limit($limit, $start);
		return $this->db->get($this->table)->result();
	}
}

/* End of file Biller_bank_sampah_model.php */
