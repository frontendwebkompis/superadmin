<!doctype html>
<html>
    <head>
        <title>Tambah Status_bank_sampah</title>
        <link rel="stylesheet" href="<?php echo base_url('assets/bootstrap/css/bootstrap.min.css') ?>"/>
        <style>
            body{
                padding: 15px;
            }
        </style>
    </head>
    <body>
        <h2 style="margin-top:0px">Pengolahan Data Status_bank_sampah </h2>
        <form action="<?php echo $action; ?>" method="post">
	    <div class="form-group">
            <label for="varchar">Ket Status Bank Sampah <?php echo form_error('ket_status_bank_sampah') ?></label>
            <input type="text" class="form-control" name="ket_status_bank_sampah" id="ket_status_bank_sampah" placeholder="Ket Status Bank Sampah" value="<?php echo $ket_status_bank_sampah; ?>" />
        </div>
	    <input type="hidden" name="id_status_bank_sampah" value="<?php echo $id_status_bank_sampah; ?>" /> 
	    <button type="submit" class="btn btn-primary">Simpan</button> 
	    <a href="<?php echo site_url('status_bank_sampah') ?>" class="btn btn-default">Batal</a>
	</form>
    </body>
</html>