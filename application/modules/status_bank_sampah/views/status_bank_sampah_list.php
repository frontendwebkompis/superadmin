<!doctype html>
<html>
    <head>
        <title>Crud Data Status_bank_sampah</title>
        <link rel="stylesheet" href="<?php echo base_url('assets/bootstrap/css/bootstrap.min.css') ?>"/>
        <style>
            body{
                padding: 15px;
            }
        </style>
    </head>
    <body>
        <h2 style="margin-top:0px"> List Status_bank_sampah</h2>
        <div class="row" style="margin-bottom: 10px">
            <div class="col-md-4">
                <?php echo anchor(site_url('status_bank_sampah/create'),'Tambah Data', 'class="btn btn-primary"'); ?>
            </div>
            <div class="col-md-4 text-center">
                <div style="margin-top: 8px" id="message">
                    <?php echo $this->session->userdata('message') <> '' ? $this->session->userdata('message') : ''; ?>
                </div>
            </div>
            <div class="col-md-1 text-right">
            </div>
            <div class="col-md-3 text-right">
                <form action="<?php echo site_url('status_bank_sampah/index'); ?>" class="form-inline" method="get">
                    <div class="input-group">
                        <input type="text" class="form-control" name="q" value="<?php echo $q; ?>">
                        <span class="input-group-btn">
                            <?php if ($q <> '') { ?>
                                <a href="<?php echo site_url('status_bank_sampah'); ?>" class="btn btn-default">Reset</a>
                                <?php } ?>
                            <button class="btn btn-primary" type="submit">Pencarian</button>
                        </span>
                    </div>
                </form>
            </div>
        </div>
        <table class="table table-bordered" style="margin-bottom: 10px">
            <tr>
                <th>No</th>
                <th>Ket Status Bank Sampah</th>
                <th style="text-align:center">Aksi</th>
            </tr>
            <?php foreach ($status_bank_sampah_data as $status_bank_sampah) { ?>
            <tr>
                <td width="80px"><?php echo ++$start ?></td>
                <td><?php echo $status_bank_sampah->ket_status_bank_sampah ?></td>
                <td style="text-align:center" width="200px">
                    <?php 
                    echo anchor(site_url('status_bank_sampah/read/'.$status_bank_sampah->id_status_bank_sampah),'Lihat'); 
                    echo ' | '; 
                    echo anchor(site_url('status_bank_sampah/update/'.$status_bank_sampah->id_status_bank_sampah),'Update'); 
                    echo ' | '; 
                    echo anchor(site_url('status_bank_sampah/delete/'.$status_bank_sampah->id_status_bank_sampah),'Hapus','onclick="javasciprt: return confirm(\'Yakin Anda ingin Menghapus ini ?\')"'); 
                    ?>
                </td>
		    </tr>
            <?php } ?>
        </table>
        <?= footer($total_rows, $pagination, site_url('status_bank_sampah/excel')) ?>
    </body>
</html>