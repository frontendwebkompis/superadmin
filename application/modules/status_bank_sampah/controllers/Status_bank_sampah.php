<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Status_bank_sampah extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Status_bank_sampah_model');
        $this->load->library('form_validation');
    }

    public function index()
    {
        $q = urldecode($this->input->get('q', TRUE));
        $start = intval($this->input->get('start'));
        
        if ($q <> '') {
            $config['base_url'] = base_url() . 'status_bank_sampah/?q=' . urlencode($q);
            $config['first_url'] = base_url() . 'status_bank_sampah/?q=' . urlencode($q);
        } else {
            $config['base_url'] = base_url() . 'status_bank_sampah/';
            $config['first_url'] = base_url() . 'status_bank_sampah/';
        }

        $config['per_page'] = 10;
        $config['page_query_string'] = TRUE;
        $config['total_rows'] = $this->Status_bank_sampah_model->total_rows($q);
        $status_bank_sampah = $this->Status_bank_sampah_model->get_limit_data($config['per_page'], $start, $q);

        $this->load->library('pagination');
        $this->pagination->initialize($config);

        $data = array(
            'status_bank_sampah_data' => $status_bank_sampah,
            'q' => $q,
            'pagination' => $this->pagination->create_links(),
            'total_rows' => $config['total_rows'],
            'start' => $start,
        );
        $this->load->view('status_bank_sampah/status_bank_sampah_list', $data);
    }

    public function read($id) 
    {
        $row = $this->Status_bank_sampah_model->get_by_id($id);
        if ($row) {
            $data = array(
            'id_status_bank_sampah' => $row->id_status_bank_sampah,
            'ket_status_bank_sampah' => $row->ket_status_bank_sampah,
            );
            $this->load->view('status_bank_sampah/status_bank_sampah_read', $data);
        } else {
            $this->session->set_flashdata('message', 
            '<div class="alert alert-warning" role="alert">Data Tidak Di Temukan</div>');
            redirect(site_url('status_bank_sampah'));
        }
    }

    public function create() 
    {
        $data = array(
            'button' => 'Create',
            'action' => site_url('status_bank_sampah/create_action'),
            'id_status_bank_sampah' => set_value('id_status_bank_sampah'),
            'ket_status_bank_sampah' => set_value('ket_status_bank_sampah'),
        );
        $this->load->view('status_bank_sampah/status_bank_sampah_form', $data);
    }
    
    public function create_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->create();
        } else {
            $data = array(
            'ket_status_bank_sampah' => $this->input->post('ket_status_bank_sampah',TRUE),
            );

            $this->Status_bank_sampah_model->insert($data);
            $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">Berhasil Menambahkan Data </div>');
            redirect(site_url('status_bank_sampah'));
        }
    }
    
    public function update($id) 
    {
        $row = $this->Status_bank_sampah_model->get_by_id($id);

        if ($row) {
            $data = array(
            'button' => 'Update',
            'action' => site_url('status_bank_sampah/update_action'),
            'id_status_bank_sampah' => set_value('id_status_bank_sampah', $row->id_status_bank_sampah),
            'ket_status_bank_sampah' => set_value('ket_status_bank_sampah', $row->ket_status_bank_sampah),
            );
            $this->load->view('status_bank_sampah/status_bank_sampah_form', $data);
        } else {
            $this->session->set_flashdata('message', '<div class="alert alert-warning" role="alert">Data Tidak Ditemukan</div>');
            redirect(site_url('status_bank_sampah'));
        }
    }
    
    public function update_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->update($this->input->post('id_status_bank_sampah', TRUE));
        } else {
            $data = array(
            'ket_status_bank_sampah' => $this->input->post('ket_status_bank_sampah',TRUE),
            );

            $this->Status_bank_sampah_model->update($this->input->post('id_status_bank_sampah', TRUE), $data);
            $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">Berhasil Merubah Data</div>');
            redirect(site_url('status_bank_sampah'));
        }
    }
    
    public function delete($id) 
    {
        $row = $this->Status_bank_sampah_model->get_by_id($id);

        if ($row) {
            $this->Status_bank_sampah_model->delete($id);
            $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">Berhasil Menghapus Data</div>');
            redirect(site_url('status_bank_sampah'));
        } else {
            $this->session->set_flashdata('message', '<div class="alert alert-warning" role="alert">Data Tidak Ditemukan</div>');
            redirect(site_url('status_bank_sampah'));
        }
    }

    public function _rules() 
    {
	$this->form_validation->set_rules('ket_status_bank_sampah', 'ket status bank sampah', 'trim|required');
	$this->form_validation->set_rules('id_status_bank_sampah', 'id_status_bank_sampah', 'trim');
	$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

    public function excel()
    {
        $this->load->helper('exportexcel');
        $namaFile = "status_bank_sampah.xls";
        $judul = "status_bank_sampah";
        $tablehead = 0;
        $tablebody = 1;
        $nourut = 1;
        //penulisan header
        header("Pragma: public");
        header("Expires: 0");
        header("Cache-Control: must-revalidate, post-check=0,pre-check=0");
        header("Content-Type: application/force-download");
        header("Content-Type: application/octet-stream");
        header("Content-Type: application/download");
        header("Content-Disposition: attachment;filename=" . $namaFile . "");
        header("Content-Transfer-Encoding: binary ");

        xlsBOF();

        $kolomhead = 0;
        xlsWriteLabel($tablehead, $kolomhead++, "No");
	    xlsWriteLabel($tablehead, $kolomhead++, "Ket Status Bank Sampah");

	foreach ($this->Status_bank_sampah_model->get_all() as $data) {
            $kolombody = 0;

            //ubah xlsWriteLabel menjadi xlsWriteNumber untuk kolom numeric
            xlsWriteNumber($tablebody, $kolombody++, $nourut);
	        xlsWriteLabel($tablebody, $kolombody++, $data->ket_status_bank_sampah);

	    $tablebody++;
            $nourut++;
        }

        xlsEOF();
        exit();
    }

}

/* End of file Status_bank_sampah.php */
/* Location: ./application/controllers/Status_bank_sampah.php */
