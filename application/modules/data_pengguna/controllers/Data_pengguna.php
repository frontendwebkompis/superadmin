<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Data_pengguna extends CI_Controller
{

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Data_pengguna_model');
		$this->load->library('form_validation');
	}

	public function index()
	{
		$model = $this->Data_pengguna_model;
		$q = urldecode($this->input->get('q', TRUE));
		$start = intval($this->input->get('start'));

		if ($q <> '') {
			$config['base_url'] = base_url() . 'data_pengguna/index.html?q=' . urlencode($q);
			$config['first_url'] = base_url() . 'data_pengguna/index.html?q=' . urlencode($q);
		} else {
			$config['base_url'] = base_url() . 'data_pengguna/index.html';
			$config['first_url'] = base_url() . 'data_pengguna/index.html';
		}

		$config['per_page'] = 10;
		$config['page_query_string'] = TRUE;
		$config['total_rows'] = 0; //$model->total_rows($q);
		$data_pengguna = []; //$model->get_limit_data($config['per_page'], $start, $q);

		$this->load->library('pagination');
		$this->pagination->initialize($config);

		$data = array(
			'tittle'    					=> 'Data Pengguna',
			'titleCard'    					=> 'Data Pengguna List',
			'judul'     					=> 'Data Pengguna',
			'data_pengguna_data' 			=> $data_pengguna,
			'q' 							=> $q,
			'pagination' 					=> $this->pagination->create_links(),
			'total_rows' 					=> $config['total_rows'],
			'start' 						=> $start,
			'menu_aktif'					=> 'data_pengguna'
		);
		$res['datakonten'] = $this->load->view('data_pengguna/data_pengguna_list', $data, true);
		$this->load->view('layouts/main_view', $res);
	}
}

/* End of file Data_pengguna.php */
