<?php

function availableMenuStatus(){
  return false;
}

function sekolahMenuStatus(){
  return false;
}

function filterDate($awal, $akhir, $link){
  $data = '<form action="" method="get">
            <div class="input-group input-group-sm">
              <div class="input-group input-group-sm" style="width: 130px;" data-toggle="tooltip" title="Dari Tanggal">
                  <input type="text" class="form-control" name="awal" id="awal" placeholder="Dari" value="'.$awal.'">
                  <span class="input-group-append">
                      <span class="input-group-text"><i id="icon-awal" class="fas fa-calendar-alt"></i></span>
                  </span>
              </div>&nbsp;
              <div class="input-group input-group-sm" style="width: 130px;" data-toggle="tooltip" title="Sampai Tanggal">
                <input type="text" class="form-control" name="akhir" id="akhir" placeholder="Sampai" value="'.$akhir.'" style="overflow: auto;">
                  <span class="input-group-append">
                      <span class="input-group-text"><i id="icon-akhir" class="fas fa-calendar-alt"></i></span>
                  </span>
              </div>&nbsp;
              <div class="btn-group btn-group-sm">
              ';
              if(!empty($awal || $akhir)){
                $data = $data.'<a href="'.$link.'" class="btn btn-secondary btn-sm" title="Clear"><i class="fas fa-redo"></i></a>';
              }
              $data = $data.'<button type="submit" class="btn btn-success btn-sm" title="Search"><i class="fas fa-search"></i></button>
              </div>
              </div>
            </form>';
  return $data;
}

function getDateLink(){
  return '?'.md5(date('dmYHis'));
}

function footer($row, $pagination, $export){
   $data = '<div class="row">
              <div class="col-md-4 py-2">
            '.totalrow($row).'
              </div>
              <div class="col-md-8 text-right">
                '.$pagination.'
              </div>
            </div>';
  if(!empty($export)){
    $data = $data.'
            <hr class="o-mt-5">
            <div class="row o-mt-5">
                <div class="col-sm-12">
                    <a href="'.$export.'" class="btn btn-success btn-sm" data-toggle="tooltip" title="Export Excel"><i class="fas fa-file-excel"></i> Export Excel</a>
                </div>
            </div>
          ';
  }
  return $data;
}

function search($action, $reset, $q){
  $data = '<form action="'.$action.'" method="get">
  <div class="input-group input-group-sm">
      <input type="text" name="q" class="form-control o-search-input" placeholder="Search" value="'.$q.'">
      <span class="input-group-append">';
  if($q <> ''){
      $data = $data.'<a href="'.$reset.'" class="btn btn-secondary btn-sm" title="Clear"><i class="fas fa-redo"></i></a>';
  }
  $data = $data.'<button type="submit" class="btn btn-info btn-sm" title="Search"><i class="fas fa-search"></i></button>
      </span>
      </div>
      </form>';
  return $data;
}

function kosong(){
  return '<span class="badge badge-danger">Kosong</span>';
}

function totalrow($row){
  return '<span class="font-weight-bold" title="Jumlah record '.$row.'">Jumlah Record : '.$row.'</span>';
}

function SetTimeOut(){
    return 60000;
}

function fulldate($dt)
{
  if($dt === '0000-00-00 00:00:00'){
    return 'Belum Disetting';
  }
    $date = new DateTime($dt);

    return $date->format('d M Y, G:i');
}
function fullDateNoTime($dt)
{
  if($dt === '0000-00-00'){
    return 'Belum Disetting';
  }
    $date = new DateTime($dt);

    return $date->format('d M Y');
}
function bgCard()
{
    return 'info';
}
function bgSecondary()
{
    return 'secondary';
}
function getID()
{
    $CI = &get_instance();
    $id = $CI->session->userdata('uidsuper');
    return $id;
}

function username(){
  $ci = &get_instance();
  $ci->db->where('id_superadmin', $ci->session->userdata('uidsuper'));
  $row  = $ci->db->get('superadmin')->row();
  return $row->username;
}

function cek_token()
{
  $ci = &get_instance();
  if ($ci->session->userdata('token') != "") {
    $ci->db->where('id_superadmin', $ci->session->userdata('uidsuper'));
    $query = $ci->db->get('superadmin');
    $row = $query->row();
    if ($row->token === $ci->session->userdata('token'))
      return true;
    else
      return false;
  } else {
    return false;
  }
}
function cekin($id)
{
  $ci = &get_instance();
  $ci->db->where('id_tabel_harga', $id);
  $query = $ci->db->get('kategori')->result();
  return $query;
}
function clean($string) {
   $string = str_replace(' ', '_', $string);// Replaces all spaces with hyphens.
   return preg_replace('/[^A-Za-z0-9\-]/', '', $string); // Removes special chars.
}

function logout()
{
  $ci = &get_instance();
  $ci->session->sess_destroy();
  redirect('login');
}

function berat($angka)
{
    $hasil_berat = number_format($angka, 0, '', '.') . " <b>Kg</b>";
    return $hasil_berat;
}

function rupiah($angka)
{
  // $hasil_rupiah = "Rp " . number_format($angka, 2, ',', '.');
  // return $hasil_rupiah;
  $hasil_rupiah = number_format($angka, 0, '', '.').' Koin';
  return $hasil_rupiah;
}

function format($angka)
{
  // $hasil_rupiah = "Rp " . number_format($angka, 2, ',', '.');
  // return $hasil_rupiah;
  $hasil_rupiah = number_format($angka, 0, '', '.');
  return $hasil_rupiah;
}

function cekposisi($cek=null)
{
 $ci = &get_instance();
  $ci->db->select('max(order_id) max, min(order_id) min');
  $ci->db->where('status_remove',1);
    $ci->db->where('status_user', $cek);
  $query = $ci->db->get('menu_bantuan')->row();
return $query;
}
function cekposisibantuan($id=null,$cek=null)
{
 $ci = &get_instance();
  $ci->db->select('max(order_id) max, min(order_id) min');
    $ci->db->where('id_menu_bantuan', $id);
    $ci->db->where('status_bantuan_remove',1);
  $query = $ci->db->get('bantuan')->row();
return $query;
}

function exportSQL($table, $subject = 'file',$sql='')
    {
      $ci = &get_instance();
      $ci->load->library('excel');
      $result     = $ci->db->query($sql);
      $ci->excel->setActiveSheetIndex(0);
      $fields = $result->list_fields();
      $alphabet = 'ABCDEFGHIJKLMOPQRSTUVWXYZ';
      $alphabet_arr = str_split($alphabet);
      $column = [];
  
      foreach ($alphabet_arr as $alpha) {
          $column[] =  $alpha;
      }
  
      foreach ($alphabet_arr as $alpha) {
          foreach ($alphabet_arr as $alpha2) {
              $column[] =  $alpha . $alpha2;
          }
      }
      foreach ($alphabet_arr as $alpha) {
          foreach ($alphabet_arr as $alpha2) {
              foreach ($alphabet_arr as $alpha3) {
                  $column[] =  $alpha . $alpha2 . $alpha3;
              }
          }
      }
  
      foreach ($column as $col) {
          $ci->excel->getActiveSheet()->getColumnDimension($col)->setWidth(30);
      }
  
      $col_total = $column[count($fields) - 1];
  
      //styling
      $ci->excel->getActiveSheet()->getStyle('A1:' . $col_total . '1')->applyFromArray(
          array(
              'fill' => array(
                  'type' => PHPExcel_Style_Fill::FILL_SOLID,
                  'color' => array('rgb' => 'DA3232')
              ),
              'alignment' => array(
                  'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                  'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
              )
          )
      );
  
      $phpColor = new PHPExcel_Style_Color();
      $phpColor->setRGB('FFFFFF');
  
      $ci->excel->getActiveSheet()->getStyle('A1:' . $col_total . '1')->getFont()->setColor($phpColor);
      $ci->excel->getActiveSheet()
      ->getStyle('A1')
      ->getNumberFormat()
      ->setFormatCode(
          PHPExcel_Style_NumberFormat::FORMAT_TEXT
      );
  
      $ci->excel->getActiveSheet()->getRowDimension(1)->setRowHeight(40);
  
      $ci->excel->getActiveSheet()->getStyle('A1:' . $col_total . '1')
          ->getAlignment()->setWrapText(true);
  
      $col = 0;
      foreach ($fields as $field) {
  
          $ci->excel->getActiveSheet()->setCellValueByColumnAndRow($col, 1, ucwords(str_replace('_', ' ', $field)));
          $col++;
      }
  
      $row = 2;
      foreach ($result->result() as $data) {
          $col = 0;
          foreach ($fields as $field) {
              $ci->excel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $data->$field);
              $col++;
          }
  
          $row++;
      }
  
      $baris= $row - 1;
  
      //set border
      $styleArray = array(
          'borders' => array(
              'allborders' => array(
                  'style' => PHPExcel_Style_Border::BORDER_THIN
              )
          )
      );
      $ci->excel->getActiveSheet()->getStyle('A1:' . $col_total . '' . $baris)->applyFromArray($styleArray);
  
      $ci->excel->getActiveSheet()->setTitle(ucwords($subject));
  
      header('Content-Type: application/vnd.ms-excel');
          // header('Content-Type: application/xlsx');
  
      header('Content-Disposition: attachment;filename=' . ucwords($subject) . '-' . date('Y-m-d') . '.xls');
     
      header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
      header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT');
      header('Cache-Control: cache, must-revalidate');
      header('Pragma: public');
  header('Cache-Control: max-age=0');
      $objWriter = PHPExcel_IOFactory::createWriter($ci->excel, 'Excel5');
      $objWriter->save('php://output');
    }