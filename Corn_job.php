<?php

// change db host production || development
// ------------------------------------------------------------------------
if ($_SERVER["HTTP_HOST"] == "localhost") {
    define('ENVIRONMENT', 'development');
    define('DBCEK', 'kompis-db-devtest');
} else {
    if (trim($suburl) == "superadmin") {
        define('DBt', $suburl);
        define('ENVIRONMENT', 'production');
        define('DBCEK', 'kompis-db-prod');
    } else {
        define('ENVIRONMENT', 'testing');
        define('DBCEK', 'kompis-db-devtest');
    }
}
// connection
// ------------------------------------------------------------------------
$koneksi = new mysqli('kompis-db-devtest.cywe3yl7yico.ap-southeast-1.rds.amazonaws.com', 'admin', 'NewKompis2020', 'db_kompis');
if ($koneksi->connect_errno) {
    echo "Failed to connect to MySQL: " . $koneksi->connect_error;
    exit();
}

 $dt = new DateTime();
// calculation saldo_nasabah_online
// ------------------------------------------------------------------------
$list_saldo_nasabah_online = $koneksi->query('SELECT * FROM v_list_saldo_nasabah_online');
while ($value = $list_saldo_nasabah_online->fetch_object()) {
    if ($value->is_transfer == 0 && $value->aktif <> 0) {
        $nasabah_online_detail = $koneksi->query("SELECT * from nasabah_online_detail where id_nasabah_online='$value->id_nasabah_online' ");
        $bs_detail = $koneksi->query("SELECT * from bank_sampah_detail where id_bank_sampah='$value->id_bank_sampah' ");
        $data_nasabah_online_detail = $nasabah_online_detail->fetch_object();
        $data_bs_detail = $bs_detail->fetch_object();
        $penambahan_saldo = $data_nasabah_online_detail->saldo_nasabah_online + ($value->aktif*85/100);
        $penambahan_saldo_bs = $data_bs_detail->saldo_bank_sampah + ($value->aktif*10/100);
        $koneksi->query("UPDATE nasabah_online_detail SET saldo_nasabah_online=$penambahan_saldo where id_nasabah_online='$value->id_nasabah_online' ");
        $koneksi->query("UPDATE bank_sampah_detail SET saldo_bank_sampah=$penambahan_saldo_bs where id_bank_sampah='$value->id_bank_sampah' ");
        $koneksi->query("UPDATE setoran_online SET is_transfer=1 where id_setoran_online='$value->id_setoran_online' ");
    }
}

// calculation saldo_nasabah_offline
// ------------------------------------------------------------------------
$list_saldo_nasabah_offline = $koneksi->query('SELECT * FROM v_list_saldo_nasabah_offline where is_transfer=0 and aktif is not null');
while ($value = $list_saldo_nasabah_offline->fetch_object()) {
    if ($value->is_transfer == 0 && $value->aktif <> 0) {
        $nasabah_offline = $koneksi->query("SELECT * from nasabah_offline where id_nasabah_offline='".$value->id_nasabah_offline."' ");
         $bs_detail = $koneksi->query("SELECT * from bank_sampah_detail where id_bank_sampah='".$value->id_bank_sampah."' ");
       $data_bs_detail = $bs_detail->fetch_object();
      
        $data_nasabah_offline = $nasabah_offline->fetch_object();
       
        $penambahan_saldo = $data_nasabah_offline->saldo_nasabah_offline + ($value->aktif*85/100);
           $penambahan_saldo_bs = $data_bs_detail->saldo_bank_sampah + ($value->aktif*10/100);
       $koneksi->query("UPDATE bank_sampah_detail SET saldo_bank_sampah=$penambahan_saldo_bs where id_bank_sampah='".$value->id_bank_sampah."' ");
      
        $koneksi->query("UPDATE nasabah_offline SET saldo_nasabah_offline=$penambahan_saldo where id_nasabah_offline='".$value->id_nasabah_offline."' ");
        $koneksi->query("UPDATE setoran_offline SET is_transfer=1 where id_setoran_offline='".$value->id_setoran_offline."' ");
    }
}
$listcancel = $koneksi->query('SELECT * FROM v_pembelian_pengepul_cancel_by_cronjob where cancel=1 order by tgl_pembelian_sampah');
$dt = new DateTime();
while ($cancel = $listcancel->fetch_object()) {
      $listpembeliandetail = $koneksi->query("SELECT * from v_pembelian_pengepul where id_pembelian_pengepul='".$cancel->id_pembelian_pengepul."' ");
    while ($detail = $listpembeliandetail->fetch_object()) {
         $getlistpenjualan = $koneksi->query("SELECT * from penjualan_bank_sampah_detail where  where id_penjualan_bank_sampah='".$detail->id_penjualan_bank_sampah."' ");
        $stokbaru=$getlistpenjualan->total_berat+$detail->stok_dibeli;
       $koneksi->query("UPDATE penjualan_bank_sampah_detail SET total_berat='$stokbaru' where id_penjualan_bank_sampah='".$getlistpenjualan->id_penjualan_bank_sampah."' "); 

    }
      $koneksi->query("UPDATE pembelian_pengepul SET id_status_pembelian_pengepul=8,id_reason=13,update_at='".$dt->format('Y-m-d H:i:s')."',reason_pembatalan='Pengepul melewati batas waktu pembayaran' where id_pembelian_pengepul='$cancel->id_pembelian_pengepul' ");
   echo $cancel->id_pembelian_pengepul."<br>"; 
}
  
  
// show data
// ------------------------------------------------------------------------
// echo "<pre>";
// print_r($data);
// echo "</pre>";
